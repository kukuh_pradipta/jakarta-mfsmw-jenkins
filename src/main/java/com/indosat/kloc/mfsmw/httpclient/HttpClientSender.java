/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 20, 2014 
 * Time       : 1:41:47 PM 
 */
package com.indosat.kloc.mfsmw.httpclient;

import static org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Component
public class HttpClientSender {

	private static Log log = LogFactory.getLog(HttpClientSender.class);

	@Autowired
	private RestTemplate restTemplate;

	public ResponseEntity<String> sendGet(String url) {
		ResponseEntity<String> response = null;
		log.info("=========Sending GET request==============");
		log.debug("Sending GET request: " + url);
		enableSSL();
		try {
			if (url.contains("https")) {
				HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
				DefaultHttpClient httpClient = (DefaultHttpClient) requestFactory.getHttpClient();
				TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
					@Override
					public boolean isTrusted(X509Certificate[] certificate, String authType) {
						return true;
					}
				};
				SSLSocketFactory sf = new SSLSocketFactory(acceptingTrustStrategy, ALLOW_ALL_HOSTNAME_VERIFIER);
				httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", 28078, sf));
				response = new RestTemplate(requestFactory).exchange(url, HttpMethod.GET, null, String.class);
			} else {
				response = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
			}

			log.info("response.getStatusCode(): " + response.getStatusCode().value());
			log.info("response.getBody(): " + response.getBody());
		} catch (HttpStatusCodeException e) {
			// TODO: handle HttpStatusCodeException
			// Server Response: 40x , 50x
			log.error("HttpStatusCodeException found.....\n" + url + "\n" + e);
		} catch (ResourceAccessException e) {
			// TODO: handle ResourceAccessException
			// I/O error, including marshaling
			log.error("ResourceAccessException found.....\n" + url + "\n" + e);
		} catch (Exception e) {
			// TODO: handle exception
			// other error
			log.error("Exception......\n" + url + "\n" + e);
		}

		return response;
	}

	public ResponseEntity<String> sendPost(String url, String data, List<MediaType> acceptableMediaTypes) {
		ResponseEntity<String> response = null;
		log.info("Sending POST request: " + url + " \nData:\n" + data);

		try {
			// Prepare header
			HttpHeaders headers = new HttpHeaders();
			headers.add("sender", "middleware-indosat-dompetku");
			headers.setAccept(acceptableMediaTypes);
			
			headers.setContentType(acceptableMediaTypes.get(0));

			// Pass the new person and header
			HttpEntity<String> entity = new HttpEntity<String>(data, headers);

			response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			log.info("response.getStatusCode(): " + response.getStatusCode().value());
			log.info("response.getBody(): " + response.getBody());
		} catch (HttpStatusCodeException e) {
			// TODO: handle HttpStatusCodeException
			// Server Response: 40x , 50x
			e.printStackTrace();
			log.error("HttpStatusCodeException found.....\n" + url + "\n" + e);
		} catch (ResourceAccessException e) {
			// TODO: handle ResourceAccessException
			// I/O error, including marshaling
			e.printStackTrace();
			log.error("ResourceAccessException found.....\n" + url + "\n" + e);
		} catch (Exception e) {
			// TODO: handle exception
			// other error
			e.printStackTrace();
			log.error("Exception......\n" + url + "\n" + e);
		}

		return response;
	}

	public ResponseEntity<String> sendPostByPassCertificate(String url, String data,
			List<MediaType> acceptableMediaTypes) {
		ResponseEntity<String> response = null;
		log.info("Sending POST request: " + url + " \nData:\n" + data);
		enableSSL();
		try {
			// Prepare header
			HttpHeaders headers = new HttpHeaders();
			headers.add("sender", "middleware-indosat-dompetku");
			headers.setAccept(acceptableMediaTypes);

			// Pass the new person and header
			HttpEntity<String> entity = new HttpEntity<String>(data, headers);

			// bypass cert
			RestTemplate passSignRest = restTemplate;
			ignoreCertAndHostVerification(passSignRest);
			response = passSignRest.exchange(url, HttpMethod.POST, entity, String.class);
			log.info("response.getStatusCode(): " + response.getStatusCode().value());
			log.info("response.getBody(): " + response.getBody());
		} catch (HttpStatusCodeException e) {
			// TODO: handle HttpStatusCodeException
			// Server Response: 40x , 50x
			e.printStackTrace();
			log.error("HttpStatusCodeException found.....\n" + url + "\n" + e);
		} catch (ResourceAccessException e) {
			// TODO: handle ResourceAccessException
			// I/O error, including marshaling
			e.printStackTrace();
			log.error("ResourceAccessException found.....\n" + url + "\n" + e);
		} catch (Exception e) {
			// TODO: handle exception
			// other error
			e.printStackTrace();
			log.error("Exception......\n" + url + "\n" + e);
		}

		return response;
	}

	public ResponseEntity<String> sendPost(String url, MultiValueMap<String, String> parameters,
			List<MediaType> acceptableMediaTypes) {
		ResponseEntity<String> response = null;
		log.info("Sending POST request: " + url);
		try {
			// Prepare header
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(acceptableMediaTypes);
			// Pass the new object and header
			HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<MultiValueMap<String, String>>(
					parameters, headers);
			response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
			log.info("response.getStatusCode(): " + response.getStatusCode().value());
			if(url.contains("ektp"))
				log.info("response.getBody(): " + response.getStatusCode().getReasonPhrase());
			else
				log.info("response.getBody(): " + response.getBody());
		} catch (HttpStatusCodeException e) {
			// TODO: handle HttpStatusCodeException
			// Server Response: 40x , 50x
			e.printStackTrace();
			log.error("HttpStatusCodeException found.....\n" + url + "\n" + e);
			logErrorParameters(e, url, parameters);
		} catch (ResourceAccessException e) {
			// TODO: handle ResourceAccessException
			// I/O error, including marshaling
			e.printStackTrace();
			log.error("ResourceAccessException found.....\n" + url + "\n" + e);
			logErrorParameters(e, url, parameters);
		} catch (Exception e) {
			// TODO: handle exception
			// other error
			e.printStackTrace();
			log.error("Exception......\n" + e);
			logErrorParameters(e, url, parameters);
		}

		return response;
	}

	private void logErrorParameters(Exception e, String url, MultiValueMap<String, String> parameters) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Error Found ***********************\n").append("URL: ").append(url).append("\n");
		for (String key : new ArrayList<String>(parameters.keySet())) {
			for (String value : parameters.remove(key))
				stringBuilder.append(key).append(":").append(value).append("\n");
		}
		stringBuilder.append(e).append("\n");
		log.error(stringBuilder.toString());

	}

	private void enableSSL() {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
		}
	}

	// disable SSL
	public static void ignoreCertAndHostVerification(RestTemplate restTemplate) throws Exception {
		SSLContext sslContext = SSLContext.getInstance("SSL");
		javax.net.ssl.TrustManager[] trustManagers = { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				// TODO Auto-generated method stub
				return null;
			}
			public void checkServerTrusted(X509Certificate[] arg0, String arg1)throws CertificateException {
				// TODO Auto-generated method stub
			}

			public void checkClientTrusted(X509Certificate[] arg0, String arg1)throws CertificateException {
				// TODO Auto-generated method stub
			}
		}
		};
		sslContext.init(null, trustManagers, new SecureRandom());
		SSLSocketFactory sslFactory = new SSLSocketFactory(sslContext, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		Scheme scheme = new Scheme("https", 443, sslFactory);
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.getHttpClient().getConnectionManager().getSchemeRegistry().register(scheme);
		restTemplate.setRequestFactory(factory);
	}

	public static HttpsURLConnection ignoreCertAndHostVerification(String urlStr) throws Exception {
		SSLContext sslContext = SSLContext.getInstance("SSL");
		
		HttpsURLConnection httpsConnection = null;
		
		javax.net.ssl.TrustManager[] trustManagers = { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				// TODO Auto-generated method stub
				return null;
			}
			public void checkServerTrusted(X509Certificate[] arg0, String arg1)throws CertificateException {
				// TODO Auto-generated method stub
			}

			public void checkClientTrusted(X509Certificate[] arg0, String arg1)throws CertificateException {
				// TODO Auto-generated method stub
			}
		}
		};
		sslContext.init(null, trustManagers, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
        URL url = new URL(urlStr);
        httpsConnection = (HttpsURLConnection) url.openConnection();
        httpsConnection.setHostnameVerifier(new TrustAllHosts());
        httpsConnection.connect();
        return httpsConnection;
	}
	
	private static class TrustAllHosts implements HostnameVerifier {
		@Override
		public boolean verify(String arg0, SSLSession arg1) {
			return false;
		}
	}
	
}
