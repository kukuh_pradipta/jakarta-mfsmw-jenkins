/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 25, 2015 
 * Time       : 5:21:43 PM 
 */
package com.indosat.kloc.mfsmw.scheduler;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.indosat.kloc.mfsmw.dao.SMSOutboxDao;
import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.model.SMSOutbox;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;

@Service("sMSResendScheduler")
public class SMSResendScheduler {

	private static Log log = LogFactory.getLog(SMSResendScheduler.class);
	
	@Autowired
	private HttpClientSender httpClientSender;
	@Autowired
	private SMSOutboxDao smsOutboxDao;
	@Autowired
	private DataStore dataStore;
	
	@Value("${app.backup.url.smsgw}")
	private String backupSmsGwURL;
	
	@Scheduled(fixedDelay=180000)//3 minutes
	public void process(){
		log.debug("Running SMS DR Monitoring ***********");
		String  startDate = Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss", -6);
		String endDate = Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss", -3);
		List<SMSOutbox> list = smsOutboxDao.getByDate(startDate, endDate);
		for(SMSOutbox obj : list){
			int id = smsOutboxDao.checkByDR(obj.getMessageId(), 10);
			if(id == 0){// DR not found
				log.info("DR["+obj.getMessageId()+"] not found, try to sending with backup link");
				if(!dataStore.isIsatPrefix(obj.getMsisdn())){//non indosat
					String smsgwURL = this.backupSmsGwURL;
					smsgwURL = smsgwURL.replace(Constant.MSISDN_REPLACE_TAG, obj.getMsisdn());
					smsgwURL = smsgwURL.replace(Constant.SMS_REPLACE_TAG, obj.getSms());
					httpClientSender.sendGet(smsgwURL);
				}
			}
		}
	}
	
}
