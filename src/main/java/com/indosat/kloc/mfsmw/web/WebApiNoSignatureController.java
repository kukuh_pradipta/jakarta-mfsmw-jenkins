/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 19, 2014 
 * Time       : 3:19:43 PM 
 */
package com.indosat.kloc.mfsmw.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.pojo.AgentData;
import com.indosat.kloc.mfsmw.pojo.Parameter;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.processor.KycProcessor;
import com.indosat.kloc.mfsmw.service.FinancialService;
import com.indosat.kloc.mfsmw.soap.SessionHandler;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.SignatureBuilder;
import com.indosat.kloc.mfsmw.util.Utils;

@Controller
@RequestMapping(value="webapinosign/{serviceName}")
public class WebApiNoSignatureController {

	private static Log log = LogFactory.getLog(WebApiNoSignatureController.class);
	
	@Autowired
	private SessionHandler sessionHandler;
	@Autowired
	private FinancialService financialService;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private KycProcessor kycProcessor;
	
	
	@RequestMapping(method = RequestMethod.POST)
	public void doPost(HttpServletRequest request, HttpServletResponse response, Response rs,
			@PathVariable("serviceName") String serviceName,
			@RequestParam(value=Constant.USERID_PARAM,required=false) String userId,
			@RequestParam(value=Constant.TO_PARAM, required=false) String to,
			@RequestParam(value=Constant.PIN_PARAM, required=false) String pin,
			@RequestParam(value=Constant.INITIATOR_PARAM, required=false) String initiator) {

		if(initiator.startsWith("62")){
			initiator = "0"+initiator.substring(2);
		}
		
		HashMap<String, String> parameterMap = new HashMap<String, String>();
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = (String)params.nextElement();
		 String value = request.getParameter(paramName);
		 if(paramName.equals(Constant.SIGNATURE_PARAM))
			 value = "XXXXXX";
		 else if(paramName.equals(Constant.PIN_PARAM))
			 value = "XXXXXX";
		 log.debug("["+serviceName+"] Attribute Name - "+paramName+", Value - "+value);
		 parameterMap.put(paramName, request.getParameter(paramName));
		}
		
		Parameter parameter = new Parameter();
		parameter.setKeyValueMap(parameterMap);
		
		String result = "";
		ApiUser apiUser = dataStore.getApiUser(userId);
		String signature = SignatureBuilder.build(initiator, pin, apiUser.getPassKey());
		
		if (apiUser != null) {
			switch (serviceName.toLowerCase()) {
			case Constant.SERVICE_FORGET_PASS:
				rs = financialService.forgetPassword(userId, to);
				break;
			}
			if (apiUser.getResponseType() == Constant.API_USER_STATUS_RESPONSE_TYPE_XML) {
				result = Utils.convertToXml(rs, Response.class);
			} else {
				result = Utils.convertToJSON(rs);
			}
		} else {
			rs.setStatus(Constant.RC_INVALID_USERID);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			result = Utils.convertToXml(rs, Response.class);
		}
		try {
			PrintWriter out = response.getWriter();
			log.info("Result: " + result);
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	@RequestMapping(method = RequestMethod.GET)
	public void doGet(HttpServletResponse response, Response rs, HttpServletRequest request,
			@PathVariable("serviceName") String serviceName,
			@RequestParam(value=Constant.USERID_PARAM,required=false) String userId,
			@RequestParam(value=Constant.TO_PARAM, required=false) String to,
			@RequestParam(value=Constant.PIN_PARAM, required=false) String pin,
			@RequestParam(value=Constant.INITIATOR_PARAM, required=false) String initiator){
		
		if(initiator.startsWith("62")){
			initiator = "0"+initiator.substring(2);
		}
		
		HashMap<String, String> parameterMap = new HashMap<String, String>();
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = (String)params.nextElement();
		 String value = request.getParameter(paramName);
		 if(paramName.equals(Constant.SIGNATURE_PARAM))
			 value = "XXXXXX";
		 if(paramName.equals(Constant.PIN_PARAM))
			 value = "XXXXXX";
		 log.debug("["+serviceName+"] Attribute Name - "+paramName+", Value - "+value);
		 parameterMap.put(paramName, request.getParameter(paramName));
		}
		
		Parameter parameter = new Parameter();
		parameter.setKeyValueMap(parameterMap);
		
		long m1 = System.currentTimeMillis();
		
		String result = "";
		ApiUser apiUser = dataStore.getApiUser(userId);
		String signature = SignatureBuilder.build(initiator, pin, apiUser.getPassKey());
		if(apiUser !=null){
			switch (serviceName.toLowerCase()) {
			case Constant.SERVICE_PATH_LOGIN:
				rs = financialService.doLogin(userId, signature);
				break;
			case Constant.SERVICE_PATH_EKTP_KYC:
//				rs = financialService.doKYCKTP(userId, signature, parameter);
				rs = financialService.doKYCKTPTemp(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_EKTP_KYC_BY_AGENT:
				rs = financialService.doKYCKTPByAgent(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_EKTP_KYC_BY_AGENT_REFERENCE:
				rs = financialService.doKYCKTPByAgentWithReference(userId, signature, parameter);
				break;
			}
		}else{
			rs.setStatus(Constant.RC_INVALID_USERID);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			result = Utils.convertToXml(rs, Response.class);
		}
		
		if(serviceName.toLowerCase().equalsIgnoreCase(Constant.SERVICE_PATH_LOGIN)){
			result = "<result><status>[%STATUS%]</status><trxid>[%TRANSACTIONID%]</trxid><msg>[%ERROR_MESSAGE%]</msg></result>";
			result = result.replace(Constant.STATUS_REPLACE_TAG, String.valueOf(rs.getStatus()));
			result = result.replace(Constant.TRANSACTIONID_REPLACE_TAG, rs.getTrxid());
			result = result.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, rs.getMsg());	
		}else{
			if(!Utils.isNullorEmptyString(rs.getAgentData().getMotherMaidenName())){
				AgentData ad = rs.getAgentData();
				ad.setMotherMaidenName("XXXXXX");
				ad.setNo_kk("XXXXXX");
				rs.setAgentData(ad);
			}
			if (apiUser.getResponseType() == Constant.API_USER_STATUS_RESPONSE_TYPE_XML) {
				result = Utils.convertToXml(rs, Response.class);
			} else {
				result = Utils.convertToJSON(rs);
			}
		}
		
		try {
			PrintWriter out = response.getWriter();
			log.info("Result: "+ result);
			out.write(result);
			long m = System.currentTimeMillis() - m1;
			log.info("response time: "+ Long.toString(m) + " ms");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
