/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 20, 2014 
 * Time       : 10:36:35 AM 
 */
package com.indosat.kloc.mfsmw.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.processor.biller.IPAYProcessor;
import com.indosat.kloc.mfsmw.processor.biller.IndosmartAirtimeProcessor;
import com.indosat.kloc.mfsmw.processor.biller.KCJProcessor;
import com.indosat.kloc.mfsmw.processor.biller.MbillerIndosmartAirtimeProcessor;
import com.indosat.kloc.mfsmw.processor.biller.NextMediaProcessor;
import com.indosat.kloc.mfsmw.processor.biller.OrangeTVProcessor;
import com.indosat.kloc.mfsmw.util.Constant;

@Controller
public class MBillerController {

	private static Log log = LogFactory.getLog(MBillerController.class);
	
	@Autowired
	private OrangeTVProcessor orangeTVProcessor;
	@Autowired
	private KCJProcessor kcjProcessor;
	@Autowired
	private IPAYProcessor ipayProcessor;
	@Autowired
	private NextMediaProcessor nextMediaProcessor;
	@Autowired
	private IndosmartAirtimeProcessor airtimeProcessor;
	@Autowired
	private MbillerIndosmartAirtimeProcessor mbillerIndosmartAirtimeProcessor; 
	
	@RequestMapping(produces = MediaType.APPLICATION_XML_VALUE, value="mbiller/request.html")
	public void goGet(HttpServletRequest request, HttpServletResponse  response,
			@RequestParam(value=Constant.FUNCTION_PARAM, required = false) String function){
		
		log.info("Incoming MBillerController request.........................................................................");
			
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = (String)params.nextElement();
		 log.info("Attribute Name - "+paramName+", Value - "+request.getParameter(paramName));
		}
		String xmlResult = "";
		if(function.equalsIgnoreCase(Constant.BILLER_GAMES_INQUIRY_FUNCTION)){
			xmlResult = ipayProcessor.inquiry(request);
		}else if(function.equalsIgnoreCase(Constant.BILLER_GAMES_PAYMENT_FUNCTION)){
			
		}else if(function.equalsIgnoreCase(Constant.BILLER_KCJ_INQUIRY_FUNCTION)){
			xmlResult = kcjProcessor.inquiry(request);	
		}else if(function.equalsIgnoreCase(Constant.BILLER_KCJ_PAYMENT_FUNCTION)){
			xmlResult = kcjProcessor.payment(request); 	
		}else if(function.equalsIgnoreCase(Constant.BILLER_ORANGE_TV_INQUIRY_FUNCTION)){
			xmlResult = orangeTVProcessor.inquiry(request);
		}else if(function.equalsIgnoreCase(Constant.BILLER_ORANGE_TV_TOPUP_FUNCTION)){
			xmlResult = orangeTVProcessor.payment(request);
		}else if(function.equalsIgnoreCase(Constant.BILLER_NEXT_MEDIA_INQURY_FUNCTION)){
			xmlResult = nextMediaProcessor.inquiry(request);
		}else if(function.equalsIgnoreCase(Constant.BILLER_NEXT_MEDIA_PAYMENT_FUNCTION)){
			xmlResult = nextMediaProcessor.payment();
		}else if(function.equalsIgnoreCase(Constant.BILLER_AIRTIME_INQUIRY)){
			xmlResult = airtimeProcessor.inquiry(request);
		}else if(function.equalsIgnoreCase(Constant.BILLER_AIRTIME_TOPUP)){
			xmlResult = airtimeProcessor.topup(request);
		}else if(function.equalsIgnoreCase(Constant.MBILLER_AIRTIME_INQUIRY)){
			xmlResult = mbillerIndosmartAirtimeProcessor.inquiry(request);
		}else if(function.equalsIgnoreCase(Constant.MBILLER_AIRTIME_TOPUP)){
			xmlResult = mbillerIndosmartAirtimeProcessor.topup(request);
		}
		
		try {
			PrintWriter out = response.getWriter();
			out.write(xmlResult);
			log.info("xmlResult: "+xmlResult);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
