/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 12, 2014 
 * Time       : 8:51:01 PM 
 */
package com.indosat.kloc.mfsmw.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.soap.SessionHandler;
import com.indosat.kloc.mfsmw.soap.UTIBAHandler;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;

//@Controller
public class PushUSSDController {

	private static Log log = LogFactory.getLog(PushUSSDController.class);
	
	@Autowired
	private UTIBAHandler utibaHandler;
	@Autowired
	private SessionHandler sessionHandler;
	@Autowired
	private DataStore dataStore;
	
	@RequestMapping("merchant/do_payment")
	public void doPayment(HttpServletRequest request, HttpServletResponse response, Response rs,
			@RequestParam(value=Constant.EXT_REF_PARAM,required=false) String extRef,
			@RequestParam(value=Constant.SIGNATURE_PARAM,required=false) String signature,
			@RequestParam(value=Constant.AMOUNT_PARAM,required=false) String amount,
			@RequestParam(value=Constant.MSISDN_PARAM,required=false) String to,
			@RequestParam(value=Constant.USERID_PARAM,required=false) String userId) throws Exception{
	
		log.info("Incoming dopayment request.........................................................................");
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = (String)params.nextElement();
		 log.info("Attribute Name - "+paramName+", Value - "+request.getParameter(paramName));
		}
		//log.info("param: "+request.getQueryString());
		if(extRef == null || signature == null || amount == null || to == null || userId == null){
			rs.setStatus(Constant.RC_INVALID_PARAMETERS);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
		}else if(!Utils.validateMSISDN(to)){
			rs.setStatus(Constant.RC_INVALID_MSISDN_FORMAT);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_MSISDN_FORMAT));
			rs.setExtRef(extRef);
		}else{
			rs.setExtRef(extRef);
			ApiUser apiUser = dataStore.getApiUser(userId);
			if(apiUser != null){// merchant found.
				signature = signature.replace(" ", "+");
				String unpackSign = Utils.validateSignature(signature, apiUser.getPassKey());
				
				log.info("unpackSign: "+unpackSign);
				if(!Utils.isNullorEmptyString(unpackSign)){
					String sessionId = (String) request.getSession().getAttribute(apiUser.getUserId());
					log.info("get sessionId from session: "+sessionId);
					if(sessionId == null){
						sessionId = sessionHandler.getSessionId();
						int validateSession = sessionHandler.validateSession(sessionId, unpackSign.split("\\|")[1].substring(6), 
								new StringBuilder(unpackSign.split("\\|")[1].substring(0, 6)).reverse().toString());
						
						if(validateSession == Constant.RC_SUCCESS){
							request.getSession().setAttribute(apiUser.getUserId(), sessionId);
							rs = utibaHandler.sell(sessionId, to, amount, apiUser.getServiceType(), extRef,apiUser);
						}else{
							rs.setStatus(validateSession);
							rs.setMsg(dataStore.getErrorMsg(validateSession));
						} 
					}else{
						rs = utibaHandler.sell(sessionId, to, amount, apiUser.getServiceType(), extRef,apiUser);
					}
				}else{
					rs.setStatus(Constant.RC_INVALID_SIGNATURE);
					rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
				}
			}else{
				rs.setStatus(Constant.RC_INVALID_USERID);
				rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			}
			
			
		}
		
		
		try {
			PrintWriter out = response.getWriter();
			out.write(Utils.convertToXml(rs, Response.class));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	@RequestMapping("merchant/do_payment_check")
	public void doPaymentCheck(HttpServletRequest request, HttpServletResponse response, Response rs,
			@RequestParam(value=Constant.EXT_REF_PARAM, required = false) String extRef,
			@RequestParam(value=Constant.SIGNATURE_PARAM, required = false) String signature,
			@RequestParam(value=Constant.USERID_PARAM, required = false) String userId){
		
		log.info("Incoming dopayment check request.........................................................................");
		log.debug("RS:"+rs);
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = (String)params.nextElement();
		 log.info("Attribute Name - "+paramName+", Value - "+request.getParameter(paramName));
		}
		
		if(extRef == null || signature == null || userId == null){
			rs.setStatus(Constant.RC_INVALID_PARAMETERS);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
		}else{
			ApiUser apiUser = dataStore.getApiUser(userId);
			if(apiUser != null){
				signature = signature.replace(" ", "+");
				String unpackSign = Utils.validateSignature(signature, apiUser.getPassKey());
				log.info("unpackSign: "+unpackSign);
				if(!Utils.isNullorEmptyString(unpackSign)){
					String sessionId = (String) request.getSession().getAttribute(apiUser.getUserId());
					log.info("get sessionId from session: "+sessionId);
					if(sessionId == null){
						sessionId = sessionHandler.getSessionId();
						int validateSession = sessionHandler.validateSession(sessionId, unpackSign.split("\\|")[1].substring(6), 
								new StringBuilder(unpackSign.split("\\|")[1].substring(0, 6)).reverse().toString());
						
						if(validateSession == Constant.RC_SUCCESS){
							request.getSession().setAttribute(apiUser.getUserId(), sessionId);
							rs = utibaHandler.checkTransaction(sessionId, extRef,apiUser);
						}else{
							rs.setStatus(validateSession);
							rs.setMsg(dataStore.getErrorMsg(validateSession));
						} 
					}else{
						rs = utibaHandler.checkTransaction(sessionId, extRef,apiUser);
					}
				}else{
					rs.setStatus(Constant.RC_INVALID_SIGNATURE);
					rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
				}
			}else{
				rs.setStatus(Constant.RC_INVALID_USERID);
				rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			}
		}

		
		
		try {

			PrintWriter out = response.getWriter();
			out.write(Utils.convertToXml(rs, Response.class));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
