/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Nov 26, 2014 
 * Time       : 3:26:57 PM 
 */
package com.indosat.kloc.mfsmw.web;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.dao.UserDao;
import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.model.User;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.sender.MailSenderHandler;
import com.indosat.kloc.mfsmw.service.FinancialService;
import com.indosat.kloc.mfsmw.soap.UTIBAHandler;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DESedeEncryption;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;

import umarketscws.KeyValuePairMap;
import umarketscws.RegisterESubscriberRequestType;

@Controller
public class EmailActivationController {

	private static Log log = LogFactory.getLog(EmailActivationController.class);
	
	@Autowired
	private UserDao  userDao;
	@Autowired
	private UTIBAHandler utibaHandler;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private FinancialService financialService;
	@Autowired
	private MailSenderHandler mailHandler;
	@Value("${app.email.subject.transaction}")
	private String subjectEmailTransaction;
	@Value("${app.email.body.registration}")
	private String bodyEmailTransaction;
	@Value("${app.email.body.activation.success}")
	private String bodyEmailActivationSuccess;
	@Value("${app.email.body.activation.success.generatedpin}")
	private String bodyEmailActivationSuccessGeneratedPin;
	
	@Value("${app.url.activation.success}")
	private String redirectURL;
	
	@RequestMapping("activate")
	public String activation(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="key", required = false) String key) throws Exception{
		
		
			HashMap<String, String> parameterMap = new HashMap<String, String>();
			Enumeration<?> params = request.getParameterNames(); 
			while(params.hasMoreElements()){
				 String paramName = (String)params.nextElement();
				 String paramValue = request.getParameter(paramName);
				 if(paramName.equals(Constant.SIGNATURE_PARAM))
					 paramValue = "XXXXXX";
				 log.debug("[activation] Attribute Name - "+paramName+", Value - "+paramValue);
				 parameterMap.put(paramName, request.getParameter(paramName));
			}
			
			DESedeEncryption encryption = new DESedeEncryption("OTt_ygRfn_UyR6_yh_76YrTU");
			String value = encryption.decrypt(key);
			String data[] = value.split("\\|");
			String username = data[0];
			String msisdn = data[1];
			String pin = data[2];
			String type ="";
			//For generatedPin Registration
			if(data.length>=4)
				if(!Utils.isNullorEmptyString(data[3]))
					type = data[3];
			String textBody = "";
			
			String url= redirectURL;
			
			User user = userDao.get(username);
			
			if(user.getStatus() == 0){
				
				umarketscws.KeyValuePairMap kvMap = new KeyValuePairMap();
				
				if(!Utils.isNullorEmptyString(user.getFirstName())){
					umarketscws.KeyValuePair kvFirstName = new umarketscws.KeyValuePair();
					kvFirstName.setKey(Constant.FIRSTNAME_PARAM);
					kvFirstName.setValue(user.getFirstName());
					kvMap.getKeyValuePair().add(kvFirstName);
				}
				
				if(!Utils.isNullorEmptyString(user.getLastName())){
					umarketscws.KeyValuePair kvLastName = new umarketscws.KeyValuePair();
					kvLastName.setKey(Constant.LASTNAME_PARAM);
					kvLastName.setValue(user.getLastName());
					kvMap.getKeyValuePair().add(kvLastName);
				}
				
				if(!Utils.isNullorEmptyString(user.getDob())){
					umarketscws.KeyValuePair kv0 = new umarketscws.KeyValuePair();
					kv0.setKey(Constant.DOB_PARAM);
					kv0.setValue(user.getDob());
					kvMap.getKeyValuePair().add(kv0);
				}
				
				if(!Utils.isNullorEmptyString(user.getGender())){
					umarketscws.KeyValuePair kv1 = new umarketscws.KeyValuePair();
					kv1.setKey(Constant.GENDER_PARAM);
					kv1.setValue(user.getGender());
					kvMap.getKeyValuePair().add(kv1);
				}
				
				if(!Utils.isNullorEmptyString(user.getMotherMaidenName())){
					umarketscws.KeyValuePair kv2 = new umarketscws.KeyValuePair();
					kv2.setKey("alt_id");
					kv2.setValue(user.getMotherMaidenName());
					kvMap.getKeyValuePair().add(kv2);
				}
				
				if(!Utils.isNullorEmptyString(user.getAddress())){
					umarketscws.KeyValuePair kv3 = new umarketscws.KeyValuePair();
			   		kv3.setKey(Constant.ADDRESS_PARAM);
			   		kv3.setValue(user.getAddress());
			   		kvMap.getKeyValuePair().add(kv3);
				}
				
				if(!Utils.isNullorEmptyString(user.getIdType())){
					umarketscws.KeyValuePair kv4 = new umarketscws.KeyValuePair();
			   		kv4.setKey(Constant.ID_TYPE_PARAM);
			   		kv4.setValue(user.getIdType());
			   		kvMap.getKeyValuePair().add(kv4);
				}
		   		
				if(!Utils.isNullorEmptyString(user.getIdNo())){
					umarketscws.KeyValuePair kv5 = new umarketscws.KeyValuePair();
			   		kv5.setKey(Constant.ID_NUMBER_PARAM);
			   		kv5.setValue(user.getIdNo());
			   		kvMap.getKeyValuePair().add(kv5);
				}
		   		
		   		umarketscws.KeyValuePair kv6 = new umarketscws.KeyValuePair();
		   		kv6.setKey("primary_group");
		   		kv6.setValue("subscribers");
		   		
		   		umarketscws.KeyValuePair kv7 = new umarketscws.KeyValuePair();
		   		kv7.setKey("suppress_pin_expiry");
		   		kv7.setValue("1");
		   		
		   		umarketscws.KeyValuePair kv8 = new umarketscws.KeyValuePair();
				kv8.setKey("suppress_sms");
				kv8.setValue("1");
		   		
		   		kvMap.getKeyValuePair().add(kv6);
		   		kvMap.getKeyValuePair().add(kv7);
		   		kvMap.getKeyValuePair().add(kv8);
				//Response rsp = utibaHandler.register(username, pin, user.getFirstName(), user.getMsisdn(), kvMap);
				
				RegisterESubscriberRequestType requestType = new RegisterESubscriberRequestType();
				requestType.setSessionid(dataStore.getGlobalSessionId());
				requestType.setEMailId(username);
				requestType.setMsisdn(user.getMsisdn());
				requestType.setFirstname(user.getFirstName());
				requestType.setLastname(user.getLastName());
				requestType.setPasswords(pin);
				//requestType.setExtraParams(kvMap);
				
				if(!Utils.isNullorEmptyString(user.getIdType())){
					if(user.getIdType().equals("1")){
						requestType.setIdtype("ktp");
					}else if(user.getIdType().equals("2")){
						requestType.setIdtype("sim");
					}else{
						requestType.setIdtype("kitas");
					}
				}
		   		
				if(!Utils.isNullorEmptyString(user.getIdNo())){
					requestType.setIdnumber(user.getIdNo());
				}
				
				if(!Utils.isNullorEmptyString(user.getDob())){
					
				}
				
				if(!Utils.isNullorEmptyString(user.getGender())){
					if(user.getGender().equals("1"))
						requestType.setGender("Male");
					else
						requestType.setGender("Female");
				}
				
//				if(!Utils.isNullorEmptyString(user.getMotherMaidenName())){
//					requestType.setMmm(user.getMotherMaidenName());
//				}
				
				if(!Utils.isNullorEmptyString(user.getAddress())){
					requestType.setAddress(user.getAddress());
				}
				
				Response rsp = utibaHandler.eRegister(requestType);
				int channel = Constant.CHANNEL_TYPE_WEB;
				ApiUser apiUser = new ApiUser();
				apiUser.setChannelType(channel);
				apiUser.setUserId("dompetkuplus");
						
				if(rsp.getStatus() == Constant.RC_SUCCESS){
					if(!Utils.isNullorEmptyString(type)){
						if(type.equalsIgnoreCase("generatedPin")){
							String name = user.getFirstName() +" "+user.getLastName();
							
							utibaHandler.updateProfile(dataStore.getGlobalSessionId(), username, name, user.getMsisdn(), null, kvMap,apiUser);
							userDao.updateStatus(username, Constant.USER_STATUS_ACTIVE);
							Response agentResp = utibaHandler.getAgentByReferenceRequest(username,apiUser);
							url = url.replace(Constant.STATUS_REPLACE_TAG, "0");
							url = url.replace(Constant.EMAIL_REPLACE_TAG, username);
							textBody = bodyEmailActivationSuccessGeneratedPin;
							textBody = textBody.replace(Constant.NAME_REPLACE_TAG, user.getFirstName() + " "+ user.getLastName());
							textBody = textBody.replace(Constant.INITIATOR_REPLACE_TAG, username);
							textBody = textBody.replace(Constant.ACCOUNTID_REPLACE_TAG, agentResp.getAgentData().getAccountId());
							textBody = textBody.replace(Constant.PIN_REPLACE_TAG, pin);
							mailHandler.sendEmailNotification(username, subjectEmailTransaction, textBody);
						}
					}else{
						String name = user.getFirstName() +" "+user.getLastName();
						utibaHandler.updateProfile(dataStore.getGlobalSessionId(), username, name, user.getMsisdn(), null, kvMap, apiUser);
						userDao.updateStatus(username, Constant.USER_STATUS_ACTIVE);
						Response agentResp = utibaHandler.getAgentByReferenceRequest(username,apiUser);
						url = url.replace(Constant.STATUS_REPLACE_TAG, "0");
						url = url.replace(Constant.EMAIL_REPLACE_TAG, username);
						textBody = bodyEmailActivationSuccess;
						textBody = textBody.replace(Constant.NAME_REPLACE_TAG, user.getFirstName() + " "+ user.getLastName());
						textBody = textBody.replace(Constant.INITIATOR_REPLACE_TAG, username);
						textBody = textBody.replace(Constant.ACCOUNTID_REPLACE_TAG, agentResp.getAgentData().getAccountId());
					}
				}else{
					url = url.replace(Constant.STATUS_REPLACE_TAG, "1");
					url = url.replace(Constant.EMAIL_REPLACE_TAG, username);
					textBody = bodyEmailTransaction;
					textBody = textBody.replace("[%DATE%]", Utils.getCurrentDate("dd/MM/yyyy HH:mm:ss"));
					textBody = textBody.replace("[%TRANSACTION_TYPE%]", "Aktivasi");
					textBody = textBody.replace("[%STATUS%]", "Gagal, "+dataStore.getErrorMsg(rsp.getStatus()));
				}
					
			}else{
				url = url.replace(Constant.STATUS_REPLACE_TAG, "1");
				url = url.replace(Constant.EMAIL_REPLACE_TAG, username);
				textBody = bodyEmailTransaction;
				textBody = textBody.replace("[%DATE%]", Utils.getCurrentDate("dd/MM/yyyy HH:mm:ss"));
				textBody = textBody.replace("[%TRANSACTION_TYPE%]", "Aktifasi");
				textBody = textBody.replace("[%STATUS%]", "Gagal, User Telah Aktif");
			}
			
			//mailHandler.sendEmailNotification(username, subjectEmailTransaction, textBody);
		
		return "redirect:"+url;
	}
	
}
