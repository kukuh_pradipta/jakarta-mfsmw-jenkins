/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 17, 2014 
 * Time       : 8:14:58 PM 
 */
package com.indosat.kloc.mfsmw.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.processor.MerchantProcessor;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;

@Controller
public class MerchantController {

	private static Log log = LogFactory.getLog(MerchantController.class);
	
	@Autowired
	private MerchantProcessor processor;
	@Autowired
	private DataStore dataStore;
	
	/**
	 * DO PAYMENT FOR USSD PUSH
	 * 
	 * @param request
	 * @param response
	 * @param rs
	 * @param extRef
	 * @param signature
	 * @param amount
	 * @param to
	 * @param userId
	 * @throws Exception
	 */
	@RequestMapping("merchant/do_payment")
	public void doPayment(HttpServletRequest request, HttpServletResponse response, Response rs,
			@RequestParam(value=Constant.EXT_REF_PARAM,required=false) String extRef,
			@RequestParam(value=Constant.SIGNATURE_PARAM,required=false) String signature,
			@RequestParam(value=Constant.AMOUNT_PARAM,required=false) String amount,
			@RequestParam(value=Constant.MSISDN_PARAM,required=false) String to,
			@RequestParam(value=Constant.USERID_PARAM,required=false) String userId) throws Exception{
	
		log.info("Incoming dopayment request.........................................................................");
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = (String)params.nextElement();
		 log.info("Attribute Name - "+paramName+", Value - "+request.getParameter(paramName));
		}
		
		if(Utils.isNullorEmptyString(extRef) || Utils.isNullorEmptyString(signature) 
				|| Utils.isNullorEmptyString(amount)  || Utils.isNullorEmptyString(to)  
				|| Utils.isNullorEmptyString(userId) ){
			
			rs.setStatus(Constant.RC_INVALID_PARAMETERS);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
		}else if(!Utils.validateMSISDN(to)){
			rs.setStatus(Constant.RC_INVALID_MSISDN_FORMAT);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_MSISDN_FORMAT));
			rs.setExtRef(extRef);
		}else{
			rs = processor.doProcess(Constant.SERVICE_DO_PAYMENT, request);
		}
		
		
		try {
			ApiUser apiUser = dataStore.getApiUser(userId);
			String result = Utils.buildResponse(apiUser.getResponseType(), rs, Response.class);
			PrintWriter out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * DO PAYMENT CHECK FOR UUSD PUSH
	 * 
	 * @param request
	 * @param response
	 * @param rs
	 * @param extRef
	 * @param signature
	 * @param userId
	 */
	@RequestMapping("merchant/do_payment_check")
	public void doPaymentCheck(HttpServletRequest request, HttpServletResponse response, Response rs,
			@RequestParam(value=Constant.EXT_REF_PARAM, required = false) String extRef,
			@RequestParam(value=Constant.SIGNATURE_PARAM, required = false) String signature,
			@RequestParam(value=Constant.USERID_PARAM, required = false) String userId){
		
		log.info("Incoming dopayment check request.........................................................................");
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = (String)params.nextElement();
		 log.info("Attribute Name - "+paramName+", Value - "+request.getParameter(paramName));
		}
		
		if(Utils.isNullorEmptyString(extRef) || Utils.isNullorEmptyString(signature)  || Utils.isNullorEmptyString(userId)){
			rs.setStatus(Constant.RC_INVALID_PARAMETERS);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
		}else{
			rs = processor.doProcess(Constant.SERVICE_DO_PAYMENT_CHECK, request);
		}

		try {

			ApiUser apiUser = dataStore.getApiUser(userId);
			String result = Utils.buildResponse(apiUser.getResponseType(), rs, Response.class);
			PrintWriter out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * TRANSFER TOKEN
	 * 
	 * @param request
	 * @param response
	 * @param rs
	 * @param transid
	 * @param signature
	 * @param amount
	 * @param msisdn
	 * @param userId
	 * @param couponId
	 */
	@RequestMapping("merchant/do_transfer_token")
	public void doTransferTokenCheck(HttpServletRequest request, HttpServletResponse response, Response rs,
			@RequestParam(value=Constant.TRANSID_PARAM,required=false) String transid,
			@RequestParam(value=Constant.SIGNATURE_PARAM,required=false) String signature,
			@RequestParam(value=Constant.AMOUNT_PARAM,required=false) String amount,
			@RequestParam(value=Constant.MSISDN_PARAM,required=false) String msisdn,
			@RequestParam(value=Constant.USERID_PARAM,required=false) String userId,
			@RequestParam(value=Constant.COUPONID_PARAM, required =false) String couponId){
		
		log.info("Incoming do transfer token request.........................................................................");
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = (String)params.nextElement();
		 log.info("Attribute Name - "+paramName+", Value - "+request.getParameter(paramName));
		}
		
		if(Utils.isNullorEmptyString(transid) || Utils.isNullorEmptyString(signature) 
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(msisdn)  
				|| Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(couponId)){
			
			rs.setStatus(Constant.RC_INVALID_PARAMETERS);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			
		}else if(!Utils.validateMSISDN(msisdn)){
			rs.setStatus(Constant.RC_INVALID_MSISDN_FORMAT);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_MSISDN_FORMAT));
			rs.setExtRef(transid);
		}else{
			rs = processor.doProcess(Constant.SERVICE_DO_TRANSFER_TOKEN, request);
		}

		try {

			ApiUser apiUser = dataStore.getApiUser(userId);
			String result = Utils.buildResponse(apiUser.getResponseType(), rs, Response.class);
			PrintWriter out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * CHECK EXTERNAL TRANSACTION
	 * 
	 * @param request
	 * @param response
	 * @param rs
	 * @param transid
	 * @param signature
	 * @param userId
	 */
	@RequestMapping("merchant/get_trx_status")
	public void getTransactionStatus(HttpServletRequest request, HttpServletResponse response, Response rs,
			@RequestParam(value=Constant.TRANSID_PARAM, required = false) String transid,
			@RequestParam(value=Constant.SIGNATURE_PARAM, required = false) String signature,
			@RequestParam(value=Constant.USERID_PARAM, required = false) String userId){
		
		log.info("Incoming chek transaction status request.........................................................................");
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = (String)params.nextElement();
		 log.info("Attribute Name - "+paramName+", Value - "+request.getParameter(paramName));
		}
		
		if(Utils.isNullorEmptyString(transid) || Utils.isNullorEmptyString(signature)  || Utils.isNullorEmptyString(userId)){
			rs.setStatus(Constant.RC_INVALID_PARAMETERS);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
		}else{
			rs = processor.doProcess(Constant.SERVICE_GET_TRX_STATUS, request);
		}

		try {

			ApiUser apiUser = dataStore.getApiUser(userId);
			String result = Utils.buildResponse(apiUser.getResponseType(), rs, Response.class);
			PrintWriter out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping("merchant/do_cashin")
	public void doCashin(HttpServletRequest request, HttpServletResponse response, Response rs,
			@RequestParam(value=Constant.AMOUNT_PARAM,required=false) String amount,
			@RequestParam(value=Constant.MSISDN_PARAM,required=false) String msisdn,
			@RequestParam(value=Constant.SIGNATURE_PARAM, required = false) String signature,
			@RequestParam(value=Constant.USERID_PARAM, required = false) String userId){
		
		log.info("Incoming cashin request.........................................................................");
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = (String)params.nextElement();
		 log.info("Attribute Name - "+paramName+", Value - "+request.getParameter(paramName));
		}
		
		if(Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(msisdn)  
				|| Utils.isNullorEmptyString(signature) || Utils.isNullorEmptyString(userId) ){
			rs.setStatus(Constant.RC_INVALID_PARAMETERS);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
		}else{
			rs = processor.doProcess(Constant.SERVICE_CASHIN, request);
		}
		
		try {
			ApiUser apiUser = dataStore.getApiUser(userId);
			String result = Utils.buildResponse(apiUser.getResponseType(), rs, Response.class);
			PrintWriter out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping("merchant/do_cashout_new")
	public void doCashout(HttpServletRequest request, HttpServletResponse response, Response rs,
			@RequestParam(value=Constant.TRANSID_PARAM,required=false) String transid,
			@RequestParam(value=Constant.SIGNATURE_PARAM,required=false) String signature,
			@RequestParam(value=Constant.AMOUNT_PARAM,required=false) String amount,
			@RequestParam(value=Constant.MSISDN_PARAM,required=false) String msisdn,
			@RequestParam(value=Constant.USERID_PARAM,required=false) String userId,
			@RequestParam(value=Constant.COUPONID_PARAM, required =false) String couponId){
		
		log.info("Incoming cashin request.........................................................................");
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = (String)params.nextElement();
		 log.info("Attribute Name - "+paramName+", Value - "+request.getParameter(paramName));
		}
		
		if(Utils.isNullorEmptyString(transid) || Utils.isNullorEmptyString(signature) 
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(msisdn)  
				|| Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(couponId)){
			
			rs.setStatus(Constant.RC_INVALID_PARAMETERS);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			
		}else{
			rs = processor.doProcess(Constant.SERVICE_CASHOUT, request);
		}
		
		try {
			ApiUser apiUser = dataStore.getApiUser(userId);
			String result = Utils.buildResponse(apiUser.getResponseType(), rs, Response.class);
			PrintWriter out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping("merchant/do_reversal")
	public void doReversal(HttpServletRequest request, HttpServletResponse response, Response rs,
			@RequestParam(value=Constant.AMOUNT_PARAM,required=false) String amount,
			@RequestParam(value=Constant.TRANSID_PARAM, required = false) String transId,
			@RequestParam(value=Constant.SIGNATURE_PARAM, required = false) String signature,
			@RequestParam(value=Constant.USERID_PARAM, required = false) String userId){
		
		log.info("Incoming cashin request.........................................................................");
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = (String)params.nextElement();
		 log.info("Attribute Name - "+paramName+", Value - "+request.getParameter(paramName));
		}
		
		if(Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(transId)  
				|| Utils.isNullorEmptyString(signature) || Utils.isNullorEmptyString(userId) ){
			rs.setStatus(Constant.RC_INVALID_PARAMETERS);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
		}else{
			rs = processor.doProcess(Constant.SERVICE_REVERSAL, request);
		}
		
		try {
			ApiUser apiUser = dataStore.getApiUser(userId);
			String result = Utils.buildResponse(apiUser.getResponseType(), rs, Response.class);
			//String result = Utils.convertToXml(rs, Response.class);
			log.info("result: \n"+result);
			PrintWriter out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
