/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jun 30, 2015 
 * Time       : 10:38:51 AM 
 */
package com.indosat.kloc.mfsmw.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.pojo.Parameter;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.processor.BankProcessor;
import com.indosat.kloc.mfsmw.processor.PayproMicroappsProcessor;
import com.indosat.kloc.mfsmw.processor.Wallet2CashProcessor;
import com.indosat.kloc.mfsmw.service.FinancialService;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.SignatureBuilder;
import com.indosat.kloc.mfsmw.util.Utils;

@Controller
public class BankController {

	private static Log log = LogFactory.getLog(BankController.class);

	@Autowired
	private FinancialService financialService;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private BankProcessor bankProcessor;

	@RequestMapping(value = "/niaga/notification", method = RequestMethod.POST)
	public void handler(HttpServletRequest request, HttpServletResponse response, @RequestBody String body) {
		String result = "";
		String responseFailedXML = "<response><notification><status>[%STATUS%]</status><msg>[%MESSAGE%]</msg></notification></response>";
		Response resp = new Response();

		HashMap<String, String> parameterMap = new HashMap<String, String>();
		Enumeration<?> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = (String) params.nextElement();
			String value = request.getParameter(paramName);
			log.info("Attribute Name - " + paramName + ", Value - " + value);
			parameterMap.put(paramName, request.getParameter(paramName));
		}

		log.info("Incoming request from Partner Response.........................................\nBODY:\n" + body);
		if (body.contains("<request>")) {
			if (body.contains("<notification>")) {
				String userid = Utils.getXmlTagValue(body, "<userid>");
				String signature = Utils.getXmlTagValue(body, "<signature>");
//				if (userid.equalsIgnoreCase("niaga_h2h")) {
					result = bankProcessor.SendNotificationNiaga(body);
//				}else {
//					resp.setStatus(Constant.RC_INVALID_USERID);
//					resp.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
//					log.error("Status:"+Constant.RC_INVALID_USERID+"/n"
//							+ "Msg:"+dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
//				}
			}
		} else {
			if (body.contains("<request>")) {
				if (body.contains("<notification>")) {
					result = responseFailedXML;
					result = result.replace(Constant.STATUS_REPLACE_TAG, String.valueOf(Constant.RC_SYSTEM_ERROR));
					result = result.replace(Constant.MESSAGE_REPLACE_TAG, dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
				}
			}
		}

		// Parameter parameter = new Parameter();
		// parameter.setKeyValueMap(parameterMap);
		// String result = "REQUEST IN PROGRESS";
		// ApiUser apiUser = dataStore.getApiUser(userId);
		// if(apiUser !=null){
		// String signature = SignatureBuilder.build(initiator, pin,
		// apiUser.getPassKey());
		// if(serviceName.equals(Constant.SERVICE_PAATH_CREATE_COUPON_REMITANCE)){
		// wallet2CashProcessor.create(userId, signature, parameter);
		// }else
		// if(serviceName.equals(Constant.SERVICE_PATH_CANCEL_COUPON_REMITANCE)){
		// wallet2CashProcessor.cancel(userId, initiator, signature, parameter);
		// }
		// }else{
		// rs.setStatus(Constant.RC_INVALID_USERID);
		// rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		// result = Utils.convertToXml(rs, Response.class);
		// }

		try {
			PrintWriter out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void main(String args[]){
		String body = "<request>"
				+ "<notification>"
				+ "<signature>1234567890123456</signature>"
				+ "<userid>niaga_h2h</userid>"
				+ "<extRef>VA-098746376</extRef>"
				+ "<vaNo>1234567890123456</vaNo>"
				+ "<trxid>1490709204</trxid>"
				+ "<amount>100000</amount>"
				+ "<merchantid>dealoka</merchantid>"
				+ "</notification>"
				+ "</request>";
		String userid = Utils.getXmlTagValue(body, "<userid>");
		String signature = Utils.getXmlTagValue(body, "<signature>");
		System.out.println(userid+"-"+signature);
		if (userid.equalsIgnoreCase("niaga_h2h")) {
			System.out.println("True");
		}
		
	}

}
