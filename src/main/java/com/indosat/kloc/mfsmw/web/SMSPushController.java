/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Apr 24, 2015 
 * Time       : 10:29:59 AM 
 */
package com.indosat.kloc.mfsmw.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.dao.SMSOutboxDao;
import com.indosat.kloc.mfsmw.dao.TransactionDMTDao;
import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.model.SMSOutbox;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.soap.SessionHandler;
import com.indosat.kloc.mfsmw.soap.UTIBAHandler;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;

@Controller
@RequestMapping(value = "smsgw/push/{serviceName}")
public class SMSPushController {

	private static Log log = LogFactory.getLog(SMSPushController.class);

	@Autowired
	private DataStore dataStore;
	@Autowired
	private SMSOutboxDao outboxDao;
	@Autowired
	private UTIBAHandler utibaHandler;
	@Autowired
	private SessionHandler sessionHandler;
	@Autowired
	private TransactionDMTDao transactionDMTDao;

	@Value("${app.external.url.smsgw}")
	private String externalSmsGwURL;
	@Value("${app.utiba.url.smsgw}")
	private String isatCDMSURL;
	@Autowired
	private HttpClientSender httpClientSender;

	@Value("${app.xml.pattern.sms.resp}")
	private String xmlResponse;

	@RequestMapping(method = RequestMethod.GET)
	public void smsPush(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("serviceName") String serviceName,
			@RequestParam(value = "uid", required = false) String userId,
			@RequestParam(value = "pwd", required = false) String key,
			@RequestParam(value = "msisdn", required = false) String msisdn,
			@RequestParam(value = "serviceid", required = false) String serviceid,
			@RequestParam(value = "transid", required = false) String transid,
			@RequestParam(value = "smstype", required = false) String smstype,
			@RequestParam(value = "sms", required = false) String sms) {

		String smsResponse = this.xmlResponse;
		String smsgwURL = "";

		Enumeration<?> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = (String) params.nextElement();
			String value = request.getParameter(paramName);
			log.debug("[" + serviceName + "] Attribute Name - " + paramName + ", Value - " + value);
		}
		

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(key) || Utils.isNullorEmptyString(msisdn)
				|| Utils.isNullorEmptyString(sms) || Utils.isNullorEmptyString(transid)) {
			smsResponse = smsResponse.replace(Constant.STATUS_REPLACE_TAG,
					String.valueOf(Constant.RC_INVALID_PARAMETERS));
			smsResponse = smsResponse.replace(Constant.TRANSACTIONID_REPLACE_TAG, Utils.generateNumber(8));
			smsResponse = smsResponse.replace(Constant.SMS_REPLACE_TAG,
					dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
		} else {

			ResponseEntity<String> re = null;
			String status = null;
			String messageId = null;
			ApiUser apiUser = dataStore.getApiUser(userId);

			if (apiUser != null && apiUser.getPassKey().equals(key)) {
				msisdn = msisdn.replace("+", "");
				if (msisdn.startsWith("0"))
					msisdn = "62" + msisdn.substring(1);

				log.debug("[" + msisdn + "]IS INDOSAT : " + dataStore.isIsatPrefix(msisdn));
				
				//=========TEMPORARY ADDED FOR SMS PROMO
				
				if(serviceName.equalsIgnoreCase("promo")){
					if (dataStore.isIsatPrefix(msisdn)) {
						smsgwURL = this.isatCDMSURL;
						smsgwURL = smsgwURL.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
						smsgwURL = smsgwURL.replace(Constant.SMS_REPLACE_TAG, sms);
						smsgwURL = smsgwURL.replace(Constant.TRANSACTIONID_REPLACE_TAG, transid);
						re = httpClientSender.sendGet(smsgwURL);
						if (re == null) {
							for (int i = 0; i < 3; i++) {
								re = httpClientSender.sendGet(smsgwURL);
								if (re != null)
									break;
							}
						}
						smsResponse = re.getBody();
						status = Utils.getXmlTagValue(smsResponse, "<STATUS>");
					}else {
						smsgwURL = this.externalSmsGwURL;
						smsgwURL = smsgwURL.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
						smsgwURL = smsgwURL.replace(Constant.SMS_REPLACE_TAG, sms);
						messageId = msisdn + Utils.generateNumber(5);
						smsgwURL = smsgwURL.replace(Constant.TRANSACTIONID_REPLACE_TAG, messageId);
						re = httpClientSender.sendGet(smsgwURL);
						if (re == null) {
							for (int i = 0; i < 3; i++) {
								re = httpClientSender.sendGet(smsgwURL);
								if (re != null)
									break;
							}
						}
						if (re != null && re.getBody().contains("&")) {
							String respBody = re.getBody();
							for (String obj : respBody.split("&")) {
								String val[] = obj.split("=");
								for (String obj2 : val) {
									if (obj2.equalsIgnoreCase("Status"))
										status = val[1];
									if (obj2.equalsIgnoreCase("MessageId"))
										messageId = val[1];
								}
							}
						} else {
							status = re.getBody();
						}

						if (status.equals("1") || status.equals("000")) {
							smsResponse = smsResponse.replace(Constant.STATUS_REPLACE_TAG, "0");
							smsResponse = smsResponse.replace(Constant.SMS_REPLACE_TAG,
									"Message processed successfully");
						} else {
							smsResponse = smsResponse.replace(Constant.STATUS_REPLACE_TAG, "-1");
							smsResponse = smsResponse.replace(Constant.SMS_REPLACE_TAG, "Fail to process");
						}

						smsResponse = smsResponse.replace(Constant.TRANSACTIONID_REPLACE_TAG, transid);

					}
				}else{
					
					String initiator = "";
					String smsTransId = Utils.getTrxIdFromSMS(sms);
					for (int i = 0; i < 3; i++) {
						initiator = transactionDMTDao.getWallet2CashByTransId(smsTransId);
						if (!Utils.isNullorEmptyString(initiator)) {
							break;
						} else {
							log.debug("initiator not found waiting from db[" + i + "]");
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					
					if(!Utils.isNullorEmptyString(initiator)){
						log.info("SEND WALLET2CASH TRANSACTION msisdn["+msisdn+"] transid["+transid+"]");
						if (dataStore.isIsatPrefix(msisdn)) {
							smsgwURL = this.isatCDMSURL;
							smsgwURL = smsgwURL.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
							smsgwURL = smsgwURL.replace(Constant.SMS_REPLACE_TAG, sms);
							smsgwURL = smsgwURL.replace(Constant.TRANSACTIONID_REPLACE_TAG, transid);
							re = httpClientSender.sendGet(smsgwURL);
							if (re == null) {
								for (int i = 0; i < 3; i++) {
									re = httpClientSender.sendGet(smsgwURL);
									if (re != null)
										break;
								}
							}
							smsResponse = re.getBody();
							status = Utils.getXmlTagValue(smsResponse, "<STATUS>");
						}else {
							smsgwURL = this.externalSmsGwURL;
							smsgwURL = smsgwURL.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
							smsgwURL = smsgwURL.replace(Constant.SMS_REPLACE_TAG, sms);
							messageId = msisdn + Utils.generateNumber(5);
							smsgwURL = smsgwURL.replace(Constant.TRANSACTIONID_REPLACE_TAG, messageId);
							re = httpClientSender.sendGet(smsgwURL);
							if (re == null) {
								for (int i = 0; i < 3; i++) {
									re = httpClientSender.sendGet(smsgwURL);
									if (re != null)
										break;
								}
							}
							if (re != null && re.getBody().contains("&")) {
								String respBody = re.getBody();
								for (String obj : respBody.split("&")) {
									String val[] = obj.split("=");
									for (String obj2 : val) {
										if (obj2.equalsIgnoreCase("Status"))
											status = val[1];
										if (obj2.equalsIgnoreCase("MessageId"))
											messageId = val[1];
									}
								}
							} else {
								status = re.getBody();
							}

							if (status.equals("1") || status.equals("000")) {
								smsResponse = smsResponse.replace(Constant.STATUS_REPLACE_TAG, "0");
								smsResponse = smsResponse.replace(Constant.SMS_REPLACE_TAG,
										"Message processed successfully");
							} else {
								smsResponse = smsResponse.replace(Constant.STATUS_REPLACE_TAG, "-1");
								smsResponse = smsResponse.replace(Constant.SMS_REPLACE_TAG, "Fail to process");
							}

							smsResponse = smsResponse.replace(Constant.TRANSACTIONID_REPLACE_TAG, transid);

						}
					}else{
						smsResponse = smsResponse.replace(Constant.STATUS_REPLACE_TAG, "0");
						smsResponse = smsResponse.replace(Constant.SMS_REPLACE_TAG, "SMS Blocked By Middleware");
						smsResponse = smsResponse.replace(Constant.TRANSACTIONID_REPLACE_TAG, transid);
					}
					
					
				}
				
				//comment due to terminate SMS Service
				/*String initiator = "";
				String smsTransId = Utils.getTrxIdFromSMS(sms);

				for (int i = 0; i < 3; i++) {
					initiator = transactionDMTDao.getInitiatorByTransId(smsTransId);
					if (!Utils.isNullorEmptyString(initiator)) {
						break;
					} else {
						log.debug("initiator not found waiting from db[" + i + "]");
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				log.debug("smsTransId[" + smsTransId + "] initiator[" + initiator + "]");
				if (initiator.startsWith("01") || initiator.startsWith("11") || initiator.startsWith("dmt_syb") || initiator.startsWith("dmt_pegadaian")) {
					// untuk alfamat gak usah kirim sms
					smsResponse = smsResponse.replace(Constant.STATUS_REPLACE_TAG, "0");
					smsResponse = smsResponse.replace(Constant.SMS_REPLACE_TAG, "SMS Blocked By Middleware due to restricted agent");
					smsResponse = smsResponse.replace(Constant.TRANSACTIONID_REPLACE_TAG, transid);
				} else {
					if (dataStore.isIsatPrefix(msisdn)) {
						smsgwURL = this.isatCDMSURL;
						smsgwURL = smsgwURL.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
						smsgwURL = smsgwURL.replace(Constant.SMS_REPLACE_TAG, sms);
						smsgwURL = smsgwURL.replace(Constant.TRANSACTIONID_REPLACE_TAG, transid);
						re = httpClientSender.sendGet(smsgwURL);
						if (re == null) {
							for (int i = 0; i < 3; i++) {
								re = httpClientSender.sendGet(smsgwURL);
								if (re != null)
									break;
							}
						}
						smsResponse = re.getBody();
						status = Utils.getXmlTagValue(smsResponse, "<STATUS>");
					} else {
						smsgwURL = this.externalSmsGwURL;
						smsgwURL = smsgwURL.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
						smsgwURL = smsgwURL.replace(Constant.SMS_REPLACE_TAG, sms);
						messageId = msisdn + Utils.generateNumber(5);
						smsgwURL = smsgwURL.replace(Constant.TRANSACTIONID_REPLACE_TAG, messageId);
						re = httpClientSender.sendGet(smsgwURL);
						if (re == null) {
							for (int i = 0; i < 3; i++) {
								re = httpClientSender.sendGet(smsgwURL);
								if (re != null)
									break;
							}
						}
						if (re != null && re.getBody().contains("&")) {
							String respBody = re.getBody();
							for (String obj : respBody.split("&")) {
								String val[] = obj.split("=");
								for (String obj2 : val) {
									if (obj2.equalsIgnoreCase("Status"))
										status = val[1];
									if (obj2.equalsIgnoreCase("MessageId"))
										messageId = val[1];
								}
							}
						} else {
							status = re.getBody();
						}

						if (status.equals("1") || status.equals("000")) {
							smsResponse = smsResponse.replace(Constant.STATUS_REPLACE_TAG, "0");
							smsResponse = smsResponse.replace(Constant.SMS_REPLACE_TAG,
									"Message processed successfully");
						} else {
							smsResponse = smsResponse.replace(Constant.STATUS_REPLACE_TAG, "-1");
							smsResponse = smsResponse.replace(Constant.SMS_REPLACE_TAG, "Fail to process");
						}

						smsResponse = smsResponse.replace(Constant.TRANSACTIONID_REPLACE_TAG, transid);

					}
				}*/

				// ====== SMS Transaction Log
				try {
					Response getTransactionByIdResp = utibaHandler.getTransactionById(
							sessionHandler.getValidGlobalSessionId(), Integer.valueOf(transid),
							Constant.SERVICE_GET_COUPON_DETAILS, apiUser);
					SMSOutbox outbox = new SMSOutbox();
					outbox.setTransId(String.valueOf(getTransactionByIdResp.getCouponDetails().getCouponTransId()));
					outbox.setMsisdn(msisdn);
					outbox.setSms(sms);
					outbox.setResponseBody(re == null ? "null" : re.getBody());
					outbox.setStatus(status != null ? Integer.valueOf(status) : Constant.RC_SYSTEM_ERROR);
					outbox.setMessageId(messageId);
					outboxDao.save(outbox);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				
				// ===================================

			} else {
				smsResponse = smsResponse.replace(Constant.STATUS_REPLACE_TAG,
						String.valueOf(Constant.RC_LOGIN_FAILED));
				smsResponse = smsResponse.replace(Constant.TRANSACTIONID_REPLACE_TAG, Utils.generateNumber(8));
				smsResponse = smsResponse.replace(Constant.SMS_REPLACE_TAG,
						dataStore.getErrorMsg(Constant.RC_LOGIN_FAILED));
			}
		}

		try {
			PrintWriter out = response.getWriter();
			log.info("smsResponse: " + smsResponse);
			out.write(smsResponse);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
