/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Dec 2, 2014 
 * Time       : 1:18:46 PM 
 */
package com.indosat.kloc.mfsmw.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.service.FinancialService;
import com.indosat.kloc.mfsmw.soap.SessionHandler;
import com.indosat.kloc.mfsmw.util.DESedeEncryption;
import com.indosat.kloc.mfsmw.util.GeneratePIN;
import com.indosat.kloc.mfsmw.util.Utils;

//@Controller
public class SecurePageController {

	
	@Autowired
	private GeneratePIN generatePIN;
	@Autowired
	private SessionHandler sessionHandler;
	@Autowired
	private FinancialService financialService; 
	
	private  final String userId="web_api_test";
	
	@RequestMapping(value="checkout")
	public String checkoutPage(){
		
		return "securepage/checkout";
	}
	
	@RequestMapping(value="securepage/login", method=RequestMethod.GET)
	public String doGetLogin(
			@RequestParam(value="username", required=false) String username,
			@RequestParam(value="password", required=false) String password,
			@RequestParam(value="actionBtn", required=false) String actionBtn){
		
		return "securepage/login";
	}
	
	@RequestMapping(value="securepage/login", method=RequestMethod.POST)
	public String doPostLogin(HttpServletRequest request,ModelMap modelMap,
			@RequestParam(value="username", required=false) String username,
			@RequestParam(value="password", required=false) String password,
			@RequestParam(value="actionBtn", required=false) String actionBtn){
		
		
		StringBuilder builder = new StringBuilder();
		//StringBuilder reversePin = new StringBuilder(123456).reverse();
		builder.append(Utils.getCurrentDate("HHmmsss")).append(password).append("|").append(new StringBuilder(password).reverse()).append(username);
		DESedeEncryption encryption = null;
		try {
			encryption = new DESedeEncryption("Th1s_0nLy_F0uR_t3sT1nG__");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println(builder.toString());
//		System.out.println(encryption.encrypt(builder.toString()));
		String signature = encryption.encrypt(builder.toString());
			Response response = financialService.doLogin(userId, signature);
			if(response.getStatus() != null && response.getStatus() != 0)
				return "redirect:login?error=true";
		
			Response response2 = financialService.balanceCheck(userId, username);
//			System.out.println("response2: "+response2.toString());
			response.setName(response2.getName());
			response.setBalance(Utils.convertStringtoCurrency(response2.getBalance()));
			modelMap.addAttribute("username",username);
			modelMap.addAttribute("response",response);
			modelMap.addAttribute("signature",signature);
		return "securepage/do_payment";
	}
	
	@RequestMapping(value="securepage/register", method=RequestMethod.GET)
	public String doGetRegistration(){
		return "securepage/register";
	}
	
	/*@RequestMapping(value="securepage/register", method=RequestMethod.POST)
	public String doPostRegistration(HttpServletRequest request, ModelMap modelMap,
			@RequestParam(value="username", required=false) String username,
			@RequestParam(value="password", required=false) String password){
		StringBuilder builder = new StringBuilder();
		//StringBuilder reversePin = new StringBuilder(123456).reverse();
		builder.append(Utils.getCurrentDate("HHmmsss")).append(password).append("|").append(new StringBuilder(password).reverse()).append(username);
		DESedeEncryption encryption = null;
		try {
			encryption = new DESedeEncryption("Th1s_0nLy_F0uR_t3sT1nG__");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(builder.toString());
		System.out.println(encryption.encrypt(builder.toString()));
		String signature = encryption.encrypt(builder.toString());
		Response response = financialService.register(userId, signature, 
				request.getParameter(Constant.FIRSTNAME_PARAM), 
				request.getParameter(Constant.LASTNAME_PARAM),
				request.getParameter(Constant.DOB_PARAM), 
				request.getParameter(Constant.GENDER_PARAM), 
				request.getParameter(Constant.MOTHER_NAME_PARAM), 
				request.getParameter(Constant.ADDRESS_PARAM), 
				request.getParameter(Constant.ID_TYPE_PARAM), 
				request.getParameter(Constant.ID_NUMBER_PARAM), 
				request.getParameter(Constant.MSISDN_PARAM));
		
		System.out.println(response.toString());
		
		modelMap.put("message", response.getMsg());
		
		if(response.getStatus() != null && response.getStatus() !=0)
			return "securepage/register";
		
		return "securepage/success-register";
	}*/
	
	@RequestMapping(value="securepage/do_payment", method=RequestMethod.GET)
	public String doGetpayment(){
		return "securepage/do_payment";
	}
	
	@RequestMapping(value="securepage/do_payment", method=RequestMethod.POST)
	public String doPostpayment(
			@RequestParam(value="signature", required=false) String signature,
			@RequestParam(value="amount", required=false) String amount,
			@RequestParam(value="to", required=false) String to){
		
		//Response response= financialService.merchantTransfer(userId, signature, "53000", "cipika", "");
		//System.out.println(response.toString());
		return "securepage/success";
	}
	
	
}
