/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : May 27, 2015 
 * Time       : 1:23:33 PM 
 */
package com.indosat.kloc.mfsmw.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.pojo.Parameter;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.sender.MailSenderHandler;
import com.indosat.kloc.mfsmw.service.FinancialService;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.SignatureBuilder;
import com.indosat.kloc.mfsmw.util.Utils;

@Controller
@RequestMapping(value="dmt/{serviceName}")
public class AlfamartDMTController {
	
	private static Log log = LogFactory.getLog(AlfamartDMTController.class);

	@Autowired
	private DataStore dataStore;
	
	@Autowired
	private FinancialService financialService;
	@Autowired
	private MailSenderHandler mailSenderHandler;
	@Value("${app.alert.email}")
	private String emailList;
	
	private long responseTime = 0l;
	
	@RequestMapping(method = RequestMethod.POST)
	public void doPost(HttpServletRequest request, HttpServletResponse response, Response rs,
			@PathVariable("serviceName") String serviceName,
			@RequestParam(value=Constant.PIN_PARAM, required=false) String pin,
			@RequestParam(value=Constant.INITIATOR_PARAM, required=false) String initiator,
			@RequestParam(value=Constant.USERID_PARAM,required=false) String userId,
			@RequestParam(value=Constant.EXT_REF_PARAM,required=false) String extRef,
			@RequestParam(value=Constant.TRANSID_PARAM,required=false) String transid,
			@RequestParam(value=Constant.PUBLIC_KEY_PARAM, required=false) String publicKey,
			@RequestParam(value=Constant.EXTRA_PARAM , required=false) String extraParam){
		
		Enumeration<?> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			log.debug("Header :: key["+key+"] value["+value+"]");
		}
		
		HashMap<String, String> parameterMap = new HashMap<String, String>();
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
			 String paramName = (String)params.nextElement();
			 String value = request.getParameter(paramName);
			 if (paramName.equals(Constant.SIGNATURE_PARAM)){
				 value = "XXXXXX";
			 }else if(paramName.equals(Constant.PIN_PARAM)){
				 value = "XXXXXX";
			 }else if(paramName.equals(Constant.PUBLIC_KEY_PARAM)){
				 value = "XXXXXX";
			 }
			 
			 log.debug("["+serviceName+"] Attribute Name - "+paramName+", Value - "+value);
			 parameterMap.put(paramName, request.getParameter(paramName));
		}
		
		Parameter parameter = new Parameter();
		parameter.setKeyValueMap(parameterMap);
		
		String result = "";
		ApiUser apiUser = dataStore.getApiUser(userId);
		if(apiUser != null){
			long StartTime =new Date().getTime();
			String signature = SignatureBuilder.build(initiator, pin, publicKey);
			
			parameterMap.put(Constant.SIGNATURE_PARAM, signature);
			
			if(serviceName.equals(Constant.SERVICE_PAATH_CREATE_COUPON_REMITANCE)){
				rs = financialService.createCouponRemitance(userId, signature, parameter);
			}else if(serviceName.equals(Constant.SERVICE_PATH_GET_COUPON_REMITANCE)){
				rs = financialService.getCouponRemitance(userId, signature, parameter);
			}else if(serviceName.equals(Constant.SERVICE_PATH_CANCEL_COUPON_REMITANCE)){
				rs = financialService.cancelCouponRemitance(userId, signature, parameter);
			}else if(serviceName.equals(Constant.SERVICE_PATH_COUPON_REMITANCE)){
				rs = financialService.remitCoupon(userId, signature, parameter);
			}else if(serviceName.equals(Constant.SERVICE_PATH_INQUIRY_CREATE_COUPON_REMIT)){
				rs = financialService.inquiryCouponRemit(userId, signature, parameter);
			}else if(serviceName.equals(Constant.SERVICE_PATH_COMMIT_COUPON_REMIT)){
				rs = financialService.commitCouponRemit(userId, signature, transid, extRef);
			}else if(serviceName.equals(Constant.SERVICE_PATH_GET_TRANSACTION_BY_EXT_REF)){
				rs = financialService.checkTransaction(userId, signature, extRef);
			}else if(serviceName.equals(Constant.SERVICE_PATH_GET_USER_INFO_BY_IDENTITY)){
				rs = financialService.getDMTUserByIdentityID(userId, signature, 
						request.getParameter(Constant.ID_TYPE_PARAM), request.getParameter(Constant.ID_NUMBER_PARAM));
			}else if(serviceName.equals(Constant.SERVICE_PATH_RESEND_SMS_COUPON_REMIT)){
				rs = financialService.resendSMSCouponRemit(userId, signature, transid);
			}
			
//			rs.setStatus(Constant.RC_TIMEOUT);

			if(apiUser.getResponseType() == Constant.API_USER_STATUS_RESPONSE_TYPE_XML){
				result = Utils.convertToXml(rs, Response.class);
			}else{
				result = Utils.convertToJSON(rs);
			}
			
			responseTime = new Date().getTime()-StartTime;
			if(rs.getStatus() == Constant.RC_TIMEOUT){
				mailSenderHandler.sendEmailNotification(this.emailList, "TIMEOUT ALERT", "DMT TIMEOUT | TIME["+Utils.getCurrentDate("dd/MM/yyyy hh:mm:ss")+"]"+" On Service:"+serviceName.toLowerCase()
				+"<br /><b>Response Time:"+responseTime+"</b>");
			}
			
		}else{
			rs.setStatus(Constant.RC_INVALID_USERID);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			result = Utils.convertToXml(rs, Response.class);
		}
		
		try {
			PrintWriter out = response.getWriter();
			log.info("Result: "+ result+" responseTime: "+responseTime+" ms");
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
