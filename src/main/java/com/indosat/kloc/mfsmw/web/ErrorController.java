/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 19, 2014 
 * Time       : 10:42:50 AM 
 */
package com.indosat.kloc.mfsmw.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;

@Controller
public class ErrorController {

	@Autowired
	private DataStore dataStore;
	
	@RequestMapping("general-error.html")
	public void getGeneralError(HttpServletResponse response, Response rs){
		
		rs.setStatus(Constant.RC_SYSTEM_ERROR);
		rs.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));

		try {
			PrintWriter out = response.getWriter();
			out.write(Utils.convertToXml(rs, Response.class));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
