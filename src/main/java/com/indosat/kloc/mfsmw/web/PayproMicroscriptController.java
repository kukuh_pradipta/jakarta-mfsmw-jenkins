/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 19, 2014 
 * Time       : 3:19:43 PM 
 */
package com.indosat.kloc.mfsmw.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.pojo.AgentData;
import com.indosat.kloc.mfsmw.pojo.Parameter;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.processor.KycProcessor;
import com.indosat.kloc.mfsmw.processor.PayproMicroappsProcessor;
import com.indosat.kloc.mfsmw.sender.MailSenderHandler;
import com.indosat.kloc.mfsmw.service.FinancialService;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.SignatureBuilder;
import com.indosat.kloc.mfsmw.util.Utils;

@Controller
@RequestMapping(value = "paypro_microapps/{serviceName}")
public class PayproMicroscriptController {

	private static Log log = LogFactory.getLog(PayproMicroscriptController.class);

	@Autowired
	private PayproMicroappsProcessor ppmaProcessor;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private KycProcessor kycProcessor;
	@Autowired
	private MailSenderHandler mailSenderHandler;
	@Value("${app.alert.email}")
	private String emailList;

	private long responseTime = 0l;

	@RequestMapping(method = RequestMethod.POST)
	public void doPost(HttpServletRequest request, HttpServletResponse response, Response rs,
			@PathVariable("serviceName") String serviceName,
			@RequestParam(value = Constant.TO_PARAM, required = false) String to,
			@RequestParam(value = Constant.SIGNATURE_PARAM, required = true) String signature,
			@RequestParam(value = Constant.USERID_PARAM, required = true) String userId,
			@RequestParam(value = Constant.TRXID_PARAM, required = false) String trxid,
			@RequestParam(value = Constant.AMOUNT_PARAM, required = false) String amount,
			@RequestParam(value = Constant.QR_STRING_PARAM, required = false) String qr,
			@RequestParam(value = Constant.EXT_REF_PARAM, required = false) String extRef,
			@RequestParam(value = Constant.REFERENCE_PARAM, required = false) String reference,
			@RequestParam(value = Constant.EXTRA_PARAM, required = false) String extraParam) {

		Enumeration<?> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			log.info("Header :: key[" + key + "] value[" + value + "]");
		}

		HashMap<String, String> parameterMap = new HashMap<String, String>();
		Enumeration<?> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = (String) params.nextElement();
			String value = request.getParameter(paramName);
			if (paramName.equals(Constant.SIGNATURE_PARAM))
				value = "XXXXXX";
			else if (paramName.equals(Constant.PIN_PARAM))
				value = "XXXXXX";
			log.info("[" + serviceName + "] Attribute Name - " + paramName + ", Value - " + value);
			parameterMap.put(paramName, request.getParameter(paramName));
		}

		Parameter parameter = new Parameter();
		parameter.setKeyValueMap(parameterMap);

		String result = "";
		ApiUser apiUser = dataStore.getApiUser(userId);

		if (apiUser != null) {
			long StartTime = new Date().getTime();
			switch (serviceName.toLowerCase()) {
			case Constant.SERVICE_PATH_QR_CHECK:
				rs = ppmaProcessor.qrCheck(qr);
				break;

			case Constant.SERVICE_PATH_QR_INQ:
				if(Utils.isNullorEmptyString(amount)){
					rs = ppmaProcessor.qrInquiry(signature, userId, qr,null);
				}else{
					rs = ppmaProcessor.qrInquiry(signature, userId, qr,amount);
				}
				break;
			case Constant.SERVICE_PATH_QR_PAY:
				rs = ppmaProcessor.qrPayment(signature, userId, trxid);
				break;

			case Constant.SERVICE_PATH_VOUCHER_INQ:
				rs = ppmaProcessor.voucherInquiry(signature, userId, reference, extRef);
				break;

			case Constant.SERVICE_PATH_VOUCHER_PAY:
				rs = ppmaProcessor.voucherPayment(signature, userId, trxid, extRef);
				break;

			}

			if (apiUser.getResponseType() == Constant.API_USER_STATUS_RESPONSE_TYPE_XML) {
				result = Utils.convertToXml(rs, Response.class);
			} else {
				result = Utils.convertToJSON(rs);
			}

			responseTime = new Date().getTime() - StartTime;
			if (rs.getStatus() == Constant.RC_TIMEOUT) {
				mailSenderHandler.sendEmailNotification(this.emailList, "TIMEOUT ALERT",
						"Middleware TIMEOUT | TIME[" + Utils.getCurrentDate("dd/MM/yyyy hh:mm:ss") + "]"
								+ " On Service:" + serviceName.toLowerCase() + "<br /><b>Response Time:" + responseTime
								+ "</b>");
			}

		} else {
			rs.setStatus(Constant.RC_INVALID_USERID);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			result = Utils.convertToJSON(rs);
		}
		try {
			PrintWriter out = response.getWriter();
			log.info("Result: " + result + "--ResponseTime:" + responseTime);
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@RequestMapping(method = RequestMethod.GET)
	public void doGet(HttpServletResponse response, Response rs, HttpServletRequest request,
			@PathVariable("serviceName") String serviceName,
			@RequestParam(value = Constant.TO_PARAM, required = false) String to,
			@RequestParam(value = Constant.SIGNATURE_PARAM, required = true) String signature,
			@RequestParam(value = Constant.USERID_PARAM, required = true) String userId,
			@RequestParam(value = Constant.TRXID_PARAM, required = false) String trxid,
			@RequestParam(value = Constant.AMOUNT_PARAM, required = false) String amount,
			@RequestParam(value = Constant.QR_STRING_PARAM, required = false) String qr,
			@RequestParam(value = Constant.EXT_REF_PARAM, required = false) String extRef,
			@RequestParam(value = Constant.REFERENCE_PARAM, required = false) String reference,
			@RequestParam(value = Constant.EXTRA_PARAM, required = false) String extraParam) {

		HashMap<String, String> parameterMap = new HashMap<String, String>();
		Enumeration<?> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = (String) params.nextElement();
			String value = request.getParameter(paramName);
			if (paramName.equals(Constant.SIGNATURE_PARAM))
				value = "XXXXXX";
			if (paramName.equals(Constant.PIN_PARAM))
				value = "XXXXXX";
			log.debug("[" + serviceName + "] Attribute Name - " + paramName + ", Value - " + value);
			parameterMap.put(paramName, request.getParameter(paramName));
		}

		Parameter parameter = new Parameter();
		parameter.setKeyValueMap(parameterMap);

		long m1 = System.currentTimeMillis();

		String result = "";
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			long StartTime = new Date().getTime();

			switch (serviceName.toLowerCase()) {
			case Constant.SERVICE_PATH_QR_CHECK:
				rs = ppmaProcessor.qrCheck(qr);
				break;

			case Constant.SERVICE_PATH_QR_INQ:
				if(Utils.isNullorEmptyString(amount)){
					rs = ppmaProcessor.qrInquiry(signature, userId, qr,null);
				}else{
					rs = ppmaProcessor.qrInquiry(signature, userId, qr,amount);
				}
				break;
			case Constant.SERVICE_PATH_QR_PAY:
				rs = ppmaProcessor.qrPayment(signature, userId, trxid);
				break;

			case Constant.SERVICE_PATH_VOUCHER_INQ:
				rs = ppmaProcessor.voucherInquiry(signature, userId, reference, extRef);
				break;

			case Constant.SERVICE_PATH_VOUCHER_PAY:
				rs = ppmaProcessor.voucherPayment(signature, userId, trxid, extRef);
				break;

			}

			responseTime = new Date().getTime() - StartTime;
			if (rs.getStatus() == Constant.RC_TIMEOUT) {
				mailSenderHandler.sendEmailNotification(this.emailList, "TIMEOUT ALERT",
						"Middleware TIMEOUT | TIME[" + Utils.getCurrentDate("dd/MM/yyyy hh:mm:ss") + "]"
								+ " On Service:" + serviceName.toLowerCase() + "<br /><b>Response Time:" + responseTime
								+ "</b>");
			}

			if (apiUser != null) {
				if (apiUser.getResponseType() == Constant.API_USER_STATUS_RESPONSE_TYPE_XML) {
					result = Utils.convertToXml(rs, Response.class);
				} else {
					result = Utils.convertToJSON(rs);
				}
			} else {
				result = Utils.convertToJSON(rs);
			}

		} else {
			rs.setStatus(Constant.RC_INVALID_USERID);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			result = Utils.convertToXml(rs, Response.class);
		}

		try {
			PrintWriter out = response.getWriter();
			log.info("Result: " + result);
			out.write(result);
			long m = System.currentTimeMillis() - m1;
			log.info("response time: " + Long.toString(m) + " ms");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
