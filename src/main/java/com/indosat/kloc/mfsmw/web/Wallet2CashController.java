/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jun 30, 2015 
 * Time       : 10:38:51 AM 
 */
package com.indosat.kloc.mfsmw.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.pojo.Parameter;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.processor.Wallet2CashProcessor;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.SignatureBuilder;
import com.indosat.kloc.mfsmw.util.Utils;

@Controller
public class Wallet2CashController {
	
	private static Log log = LogFactory.getLog(Wallet2CashController.class);
	
	@Autowired
	private Wallet2CashProcessor wallet2CashProcessor;
	@Autowired
	private DataStore dataStore;
	
	@RequestMapping(value="/wallet2cash/{serviceName}", method = RequestMethod.GET)
	public void doCreate(HttpServletRequest request, HttpServletResponse response, Response rs,
			@PathVariable("serviceName") String serviceName,
			@RequestParam(value=Constant.USERID_PARAM,required=false) String userId,
			@RequestParam(value=Constant.TO_PARAM, required=false) String to,
			@RequestParam(value=Constant.PIN_PARAM, required=false) String pin,
			@RequestParam(value=Constant.INITIATOR_PARAM, required=false) String initiator){
		
		HashMap<String, String> parameterMap = new HashMap<String, String>();
		Enumeration<?> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
			 String paramName = (String)params.nextElement();
			 String value = request.getParameter(paramName);
			 if(paramName.equals(Constant.SIGNATURE_PARAM))
				 value = "XXXXXX";
			 log.debug(" Attribute Name - "+paramName+", Value - "+value);
			 parameterMap.put(paramName, request.getParameter(paramName));
		}
		
		Parameter parameter = new Parameter();
		parameter.setKeyValueMap(parameterMap);
		String result = "REQUEST IN PROGRESS";
		ApiUser apiUser = dataStore.getApiUser(userId);
		if(apiUser !=null){
			String signature = SignatureBuilder.build(initiator, pin, apiUser.getPassKey());
			if(serviceName.equals(Constant.SERVICE_PAATH_CREATE_COUPON_REMITANCE)){
				wallet2CashProcessor.create(userId, signature, parameter);
			}else if(serviceName.equals(Constant.SERVICE_PATH_CANCEL_COUPON_REMITANCE)){
				wallet2CashProcessor.cancel(userId, initiator, signature, parameter);
			}
		}else{
			rs.setStatus(Constant.RC_INVALID_USERID);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			result = Utils.convertToXml(rs, Response.class);
		}
		
		
		try {
			PrintWriter out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
}
