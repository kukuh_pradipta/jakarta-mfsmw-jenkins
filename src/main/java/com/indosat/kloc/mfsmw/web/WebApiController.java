/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 19, 2014 
 * Time       : 3:19:43 PM 
 */
package com.indosat.kloc.mfsmw.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.pojo.AgentData;
import com.indosat.kloc.mfsmw.pojo.Parameter;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.processor.KycProcessor;
import com.indosat.kloc.mfsmw.processor.PayproMicroappsProcessor;
import com.indosat.kloc.mfsmw.sender.MailSenderHandler;
import com.indosat.kloc.mfsmw.service.FinancialService;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.SignatureBuilder;
import com.indosat.kloc.mfsmw.util.Utils;

@Controller
@RequestMapping(value = "webapi/{serviceName}")
public class WebApiController {

	private static Log log = LogFactory.getLog(WebApiController.class);

	@Autowired
	private FinancialService financialService;
	@Autowired
	private PayproMicroappsProcessor ppmaProcessor;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private KycProcessor kycProcessor;
	@Autowired
	private MailSenderHandler mailSenderHandler;
	@Value("${app.alert.email}")
	private String emailList;
	
	private long responseTime = 0l;

	@RequestMapping(method = RequestMethod.POST)
	public void doPost(HttpServletRequest request, HttpServletResponse response, Response rs,
			@PathVariable("serviceName") String serviceName,
			@RequestParam(value = Constant.TO_PARAM, required = false) String to,
			@RequestParam(value = Constant.SIGNATURE_PARAM, required = false) String signature,
			@RequestParam(value = Constant.USERID_PARAM, required = false) String userId,
			@RequestParam(value = Constant.EXT_REF_PARAM, required = false) String extRef,
			@RequestParam(value = Constant.AMOUNT_PARAM, required = false) String amount,
			@RequestParam(value = Constant.COUPONID_PARAM, required = false) String couponId,
			@RequestParam(value = Constant.TRANSID_PARAM, required = false) String transid,
			@RequestParam(value = Constant.TARGET_PARAM, required = false) String target,
			@RequestParam(value = Constant.OPERATOR_NAME_PARAM, required = false) String operatorName,
			@RequestParam(value = Constant.COUNT_PARAM, required = false) Integer count,
			@RequestParam(value = Constant.EXTRA_PARAM, required = false) String extraParam,
			@RequestParam(value = Constant.TRXID_PARAM, required = false) String trxid,
			@RequestParam(value = Constant.QR_STRING_PARAM, required = false) String qr,
			@RequestParam(value = Constant.REFERENCE_PARAM, required = false) String reference,
			@RequestParam(value = Constant.LOWERLIMIT_PARAM, required = false) String lowerLimit,
			@RequestParam(value = Constant.MSISDN_PARAM, required = false) String msisdn) {
		
		//add decoding for qr unicode
//		if(!Utils.isNullorEmptyString(qr)) {
//			try {
//				String s = qr;
//				String s2 = URLDecoder.decode(s,"UTF-8");
//				qr = new String(s2.getBytes("ISO-8859-1"), "UTF-8");
//			}catch(Exception e) {
//				
//			}
//			log.debug(qr);
//		}
		//============================

		Enumeration<?> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			log.info("Header :: key[" + key + "] value[" + value + "]");
		}

		HashMap<String, String> parameterMap = new HashMap<String, String>();
		Enumeration<?> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = (String) params.nextElement();
			String value = request.getParameter(paramName);
			if(paramName.equals(Constant.SIGNATURE_PARAM))
				 value = "XXXXXX";
			else if(paramName.equals(Constant.PIN_PARAM))
				 value = "XXXXXX";
			else if(paramName.equals(Constant.IDPHOTO_PARAM))
				 value = "XXXXXX";
			else if(paramName.equals(Constant.IDPHOTOUSER_PARAM))
				 value = "XXXXXX";
			log.info("[" + serviceName + "] Attribute Name - " + paramName + ", Value - " + value);
			parameterMap.put(paramName, request.getParameter(paramName));
		}

		Parameter parameter = new Parameter();
		parameter.setKeyValueMap(parameterMap);

		String result = "";
		ApiUser apiUser = dataStore.getApiUser(userId);
//		List<String> apiIPs = dataStore.getIPApiUser(userId);
//		List<String> apiLimits = dataStore.getLimitApiUser(userId);

//		if (!apiIPs.contains(request.getRemoteAddr())) {
//			rs.setStatus(Constant.RC_INVALID_USERID);
//			rs.setMsg("IP of API User is not registered");
//			result = Utils.convertToXml(rs, Response.class);
//		}
//
//		if (!apiLimits.contains(serviceName)) {
//			rs.setStatus(Constant.RC_INVALID_USERID);
//			rs.setMsg("IP of API User is not registered");
//			result = Utils.convertToXml(rs, Response.class);
//		}
		
		if (apiUser != null) {
			long StartTime =new Date().getTime();
			switch (serviceName.toLowerCase()) {
			case Constant.SERVICE_PATH_LOGIN:
				rs = financialService.doLogin(userId, signature);
				break;
			case Constant.SERVICE_PATH_OTP_REQUEST:
				rs = financialService.requestOTP(userId, signature, count);
				break;
			case Constant.SERVICE_PATH_REGISTER:
				rs = financialService.register(userId, signature, parameterMap);
				break;
			case Constant.SERVICE_PATH_UPDATE_PROFILE:
				rs = financialService.updateProfile(userId, signature, parameterMap);
				break;
			case Constant.SERVICE_PATH_MOBILE_AGENT_KYC_UPGRADE:
				rs = financialService.mobileAgnetkycUpgrade(userId, signature, parameterMap);
				break;
			case Constant.SERVICE_PATH_KYC_UPGRADE:
				rs = financialService.kycUpgrade(userId, signature, parameterMap);
				break;
			case Constant.SERVICE_PATH_HISTORY_TRANSACTION:
				rs = financialService.doHistoryTransaction(userId, to, count);
				break;
			case Constant.SERVICE_PATH_GET_AGENT_BY_REF:
				rs = financialService.doUserInquiry(userId, signature, to);
				break;
			case Constant.SERVICE_PATH_DO_PAYMENT:
				rs = financialService.sell(userId, signature, to, amount, extRef, extraParam);
				break;
			case Constant.SERVICE_PATH_DO_PAYMENT_PARAM:
				rs = financialService.sellWithParam(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATAH_DO_PAYMENT_CHECK:
				rs = financialService.checkTransaction(userId, signature, extRef);
				break;
			case Constant.SERVICE_PATH_COUPON_TRANSFER:
				rs = financialService.couponTransfer(userId, signature, to, extRef, amount, couponId);
				break;
			case Constant.SERVICE_PATH_CREATE_COUPON:
				rs = financialService.createCoupon(userId, signature);
				break;
			case Constant.SERVICE_PATH_CHECK_BALANCE:
				rs = financialService.balanceCheck(userId, to);
				break;
			case Constant.SERVICE_PATH_MERCHANT_BUY:
				rs = financialService.merchantTransfer(userId, signature, amount, to, parameterMap);
				break;
			case Constant.SERVICE_PATH_MONEY_TRANSFER:
				rs = financialService.moneyTransferExtParam(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_AIRTIME_INQUIRY:
				rs = financialService.creditAirtimeInquiry(userId, signature, to, amount, extRef);
				break;
			case Constant.SERVICE_PATH_AIRTIME_COMMIT:
				//transid --> transid when inquiry
				rs = financialService.commitCreditAirtime(userId, signature, Integer.valueOf(transid), extRef,
						operatorName);
				break;
			case Constant.SERVICE_PATH_MATRIX_INQUIRY:
				rs = financialService.matrixBillPay(userId, signature, to, target);
				break;
			case Constant.SERVICE_PATH_CONFIRM:
				rs = financialService.confirm(userId, signature, Integer.valueOf(transid));
				break;
			case Constant.SERVICE_PATH_CONFIRM_NOID:
				rs = financialService.confirmNoTransid(userId, signature);
				break;
			case Constant.SERVICE_PATH_QUERY_BILLPAY:
				rs = financialService.queryBillPay(userId, signature, target, to, parameterMap);
				break;
			case Constant.SERVICE_PATH_BILLPAY:
				rs = financialService.billPay(userId, signature, target, to, amount, String.valueOf(transid),
						parameterMap);
				break;
			case Constant.SERVICE_PATH_CHANGE_PIN:
				rs = financialService.changePin(userId, signature, request.getParameter(Constant.PIN_PARAM));
				break;
			case Constant.SERVICE_PATH_BANK_TRF:
				rs = financialService.bankTransfer(userId, signature, request.getParameter(Constant.BANK_CODE_PARAM),
						request.getParameter(Constant.BANK_ACC_NO), amount);
				break;
			case Constant.SERVICE_PATH_P2P_TRF:
				rs = financialService.p2pTransfer(userId, signature, to, amount);
				break;
			case Constant.SERVICE_FORGET_PASS:
				rs = financialService.forgetPassword(userId, to);
				break;
			case Constant.SERVICE_PATH_OTP_LIFE_REQUEST:
				rs = financialService.OTPCounter(userId, to);
				break;
			case Constant.SERVICE_RESET_PIN:
				rs = financialService.resetPin(userId, signature);
				break;
			case Constant.SERVICE_PATH_KYC_REQUEST:
				kycProcessor.verify(parameterMap);
				break;
			case Constant.SERVICE_PATH_MBILLER_INQUIRY:
				rs = financialService.billerInq(userId, signature, parameterMap);
				break;
			case Constant.SERVICE_PATH_MBILLER_PAY:
				rs = financialService.billerPay(userId, signature, parameterMap);
				break;
			case Constant.SERVICE_PAATH_CREATE_COUPON_REMITANCE:
				rs = financialService.createCouponRemitance(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_GET_COUPON_REMITANCE:
				rs = financialService.getCouponRemitance(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_CANCEL_COUPON_REMITANCE:
				rs = financialService.cancelCouponRemitance(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_COUPON_REMITANCE:
				rs = financialService.remitCoupon(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_INQUIRY_CREATE_COUPON_REMIT:
				rs = financialService.inquiryCouponRemit(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_COMMIT_COUPON_REMIT:
				rs = financialService.commitCouponRemit(userId, signature, transid, extRef);
				break;
			case Constant.SERVICE_PATH_GET_TRANSACTION_BY_EXT_REF:
				rs = financialService.checkTransaction(userId, signature, extRef);
				break;
			case Constant.SERVICE_PATH_GET_USER_INFO_BY_IDENTITY:
				rs = financialService.getDMTUserByIdentityID(userId, signature,
						request.getParameter(Constant.ID_TYPE_PARAM), request.getParameter(Constant.ID_NUMBER_PARAM));
				break;
			case Constant.SERVICE_PATH_RESEND_SMS_COUPON_REMIT:
				rs = financialService.resendSMSCouponRemit(userId, signature, transid);
				break;
			case Constant.SERVICE_PATH_MERCHANT_CASHIN:
				rs = financialService.cashin(userId, signature, to, amount, extRef, false);
				break;
			case Constant.SERVICE_PATH_INQUIRY_KYC:
				rs = financialService.inquiryKYC(userId, signature, to, request.getParameter(Constant.TYPE_PARAM));
				break;
			case Constant.SERVICE_PATH_DO_KYC_BY_AGENT:
				rs = financialService.doKYCByAgent(userId, signature, parameterMap);
				break;
			case Constant.SERVICE_PATH_DO_NFC_SETTLEMENT:
				rs = financialService.nfcSettlement(userId, signature, parameterMap);
				break;
			case Constant.SERVICE_PATH_REVERSAL:
				rs = financialService.reversal(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_WALLET_TO_CASH_INQUIRY:// wallet to cash Inquiry
				rs = financialService.wallet2CashInquiry(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_WALLET_TO_CASH:// wallet to cash
				rs = financialService.wallet2Cash(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_INQUIRY_RESET_PIN:
				rs = financialService.inquiryResetPINByAgent(userId, signature, to);
				break;
			case Constant.SERVICE_PATH_COMMIT_RESET_PIN:
				rs = financialService.resetPinByAgent(userId, signature, parameterMap);
				break;
			case Constant.SERVICE_PATH_RESEND_EMAIL_ACTIVATION:
				rs = financialService.resendEmailActivation(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_TRANS_EXT_REF:
				rs = financialService.transQueryExt(userId, signature, extRef);
				break;
			case Constant.SERVICE_PATH_SELL_POINT:
				rs = financialService.sellPoint(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_COMPANION_CARD:
				rs = financialService.companionCard(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_SMS_KEYWORD:
				rs = financialService.smsKeyword(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_AJ_INQ:
				rs = financialService.ajInquiry(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_AJ_TRF:
				rs = financialService.ajTransfer(userId, signature, parameter);
				break;
//			case Constant.SERVICE_PATH_EKTP:
//				rs = financialService.eKTP(parameter);
//				break;
			case Constant.SERVICE_PATH_EKTP_KYC:
//				rs = financialService.doKYCKTP(userId, signature, parameter);
				rs = financialService.doKYCKTPTemp(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_EKTP_KYC_BY_AGENT:
				rs = financialService.doKYCKTPByAgent(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_EKTP_KYC_BY_AGENT_REFERENCE:
				rs = financialService.doKYCKTPByAgentWithReference(userId, signature, parameter);
				break;
//			case Constant.SERVICE_PATH_SELL_NO_CONFIRM:
//				rs = financialService.sellNoConfirm(userId, signature, to, amount, extRef);
//				break;
			case Constant.SERVICE_PATH_SIMPLE_REG:
				rs = financialService.simpleReg(userId, signature, parameterMap);
				break;
			case Constant.SERVICE_PATH_HOLD:
				rs = financialService.hold(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_HOLD_CONFIRM:
				rs = financialService.holdConfirm(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_HOLD_RELEASE:
				rs = financialService.holdRelease(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_ONEBILL_REG:
				rs = financialService.oneBillReg(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_ONEBILL_UPDATE:
				rs = financialService.oneBillUpdate(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_ONEBILL_GETLIST:
				rs = financialService.oneBillGetList(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_ONEBILL_UNREG:
				rs = financialService.oneBillUnreg(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_DPLUS_REGISTER_GENERATEPIN:
				rs = financialService.registerGeneratePin(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_GET_TRX_BY_TRXID:
				rs = financialService.getTrxByTrxid(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_FORGOTPIN:
				rs = financialService.forgotPin(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_KYC_NOKTP:
				rs = financialService.doKYCNoKTP(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_CHECKTRX_EXTREF:
				rs = financialService.transQueryExtExtra(userId, signature, extRef);
				break;
			case Constant.SERVICE_PATH_REVERSAL_EXTREF:
				rs = financialService.reversalExtRef(userId, signature, extRef, amount);
				break;
			case Constant.SERVICE_PATH_QR_CHECK:
				rs = ppmaProcessor.qrCheck(qr);
				break;

			case Constant.SERVICE_PATH_QR_INQ:
				if(Utils.isNullorEmptyString(amount)){
					rs = ppmaProcessor.qrInquiry(signature, userId, qr,null);
				}else{
					rs = ppmaProcessor.qrInquiry(signature, userId, qr,amount);
				}
				break;
			case Constant.SERVICE_PATH_QR_PAY:
				rs = ppmaProcessor.qrPayment(signature, userId, trxid);
				break;

			case Constant.SERVICE_PATH_VOUCHER_INQ:
				rs = ppmaProcessor.voucherInquiry(signature, userId, reference, extRef);
				break;

			case Constant.SERVICE_PATH_VOUCHER_PAY:
				rs = ppmaProcessor.voucherPayment(signature, userId, trxid, extRef);
				break;
				
			case Constant.SERVICE_PATH_QR_OFFLINE_REQ:
				rs = financialService.qrOfflineRequest(userId, signature, parameter);
				break;
				
			case Constant.SERVICE_PATH_CHECK_CHILD:
				rs = financialService.checkChild(userId, signature, parameter);
				break;
				
			case Constant.SERVICE_PATH_CHECK_POINT_DOMPETKU:
				rs = financialService.checkPointDompetku(userId, signature, parameter);
				break;
				
			case Constant.SERVICE_PATH_LOCK_BALANCE:
				rs = financialService.lockAmount(userId, signature, lowerLimit, msisdn);
				break;
			}

			if(!Utils.isNullorEmptyString(rs.getAgentData().getMotherMaidenName())){
				AgentData ad = rs.getAgentData();
				ad.setMotherMaidenName("XXXXXX");
				ad.setNo_kk("XXXXXX");
				rs.setAgentData(ad);
			}
			if (apiUser.getResponseType() == Constant.API_USER_STATUS_RESPONSE_TYPE_XML) {
				result = Utils.convertToXml(rs, Response.class);
			} else {
				result = Utils.convertToJSON(rs);
			}

			responseTime = new Date().getTime()-StartTime;
			if(rs.getStatus() == Constant.RC_TIMEOUT){
				mailSenderHandler.sendEmailNotification(this.emailList, "TIMEOUT ALERT", "Middleware TIMEOUT | TIME["+Utils.getCurrentDate("dd/MM/yyyy hh:mm:ss")+"]"+" On Service:"+serviceName.toLowerCase()
				+"<br /><b>Response Time:"+responseTime+"</b>");
			}
			
		} else {
			rs.setStatus(Constant.RC_INVALID_USERID);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			result = Utils.convertToXml(rs, Response.class);
		}
		try {
			PrintWriter out = response.getWriter();
			log.info("Result: " + result+"--ResponseTime:"+responseTime);
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@RequestMapping(method = RequestMethod.GET)
	public void doGet(HttpServletResponse response, Response rs, HttpServletRequest request,
			@PathVariable("serviceName") String serviceName,
			@RequestParam(value = Constant.USERID_PARAM, required = false) String userId,
			@RequestParam(value = Constant.SIGNATURE_PARAM, required = false) String signature,
			@RequestParam(value = Constant.TO_PARAM, required = false) String to,
			@RequestParam(value = Constant.PIN_PARAM, required = false) String pin,
			@RequestParam(value = Constant.INITIATOR_PARAM, required = false) String initiator,
			@RequestParam(value = Constant.TRXID_PARAM, required = false) String trxid,
			@RequestParam(value = Constant.QR_STRING_PARAM, required = false) String qr,
			@RequestParam(value = Constant.REFERENCE_PARAM, required = false) String reference) {

		HashMap<String, String> parameterMap = new HashMap<String, String>();
		Enumeration<?> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = (String) params.nextElement();
			String value = request.getParameter(paramName);
			if(paramName.equals(Constant.SIGNATURE_PARAM))
				 value = "XXXXXX";
			 if(paramName.equals(Constant.PIN_PARAM))
				 value = "XXXXXX";
			log.debug("[" + serviceName + "] Attribute Name - " + paramName + ", Value - " + value);
			parameterMap.put(paramName, request.getParameter(paramName));
		}

		Parameter parameter = new Parameter();
		parameter.setKeyValueMap(parameterMap);

		long m1 = System.currentTimeMillis();

		String result = "";
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			long StartTime =new Date().getTime();
			if (serviceName.equals(Constant.SERVICE_PATH_LOGIN)) {
				rs = financialService.doLogin(userId, signature);
				result = "<result><status>[%STATUS%]</status><trxid>[%TRANSACTIONID%]</trxid><msg>[%ERROR_MESSAGE%]</msg></result>";
				result = result.replace(Constant.STATUS_REPLACE_TAG, String.valueOf(rs.getStatus()));
				result = result.replace(Constant.TRANSACTIONID_REPLACE_TAG, rs.getTrxid());
				result = result.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, rs.getMsg());
			} else if (serviceName.equals(Constant.SERVICE_PATH_CHECK_BALANCE)) {
				rs = financialService.balanceCheck(userId, to);
				if (rs.getAgentData().isOutlet() == false)
					rs.setStatus(Constant.RC_RESTRICTED_CHANNEL);

				result = "<result>" + "<common_response>" + "<response_code>[%STATUS%]</response_code>"
						+ "<response_message>[%ERROR_MESSAGE%]</response_message>" + "</common_response>" + "<trx_info>"
						+ "<trx_id>[%TRANSACTIONID%]</trx_id>" + "<currentBalance>[%BALANCE%]</currentBalance>"
						+ "</trx_info>" + "</result>";

				result = result.replace(Constant.STATUS_REPLACE_TAG, String.valueOf(rs.getStatus()));
				result = result.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, rs.getMsg());
				result = result.replace(Constant.TRANSACTIONID_REPLACE_TAG, rs.getTrxid());
				result = result.replace(Constant.BALANCE_REPLACE_TAG, rs.getBalance());

			} else if (serviceName.equals(Constant.SERVICE_PATH_WALLET_TO_CASH)) {// wallet to cash
				signature = SignatureBuilder.build(initiator, pin, apiUser.getPassKey());
				rs = financialService.wallet2Cash(userId, signature, parameter);
				result = rs.toString();
			} else if (serviceName.equals(Constant.SERVICE_PATH_SMS_KEYWORD)) {
				signature = SignatureBuilder.build(initiator, pin, apiUser.getPassKey());
				rs = financialService.smsKeyword(userId, signature, parameter);
				result = rs.toString();
			} else if (serviceName.equals(Constant.SERVICE_PATH_EKTP_KYC)) {
//				signature = SignatureBuilder.build(initiator, pin, apiUser.getPassKey());
//				rs = financialService.doKYCKTP(userId, signature, parameter);
				rs = financialService.doKYCKTPTemp(userId, signature, parameter);
				if(!Utils.isNullorEmptyString(rs.getAgentData().getMotherMaidenName())){
					AgentData ad = rs.getAgentData();
					ad.setMotherMaidenName("XXXXXX");
					ad.setNo_kk("XXXXXX");
					rs.setAgentData(ad);
				}
				result = rs.toString();
			} else if (serviceName.equals(Constant.SERVICE_PATH_EKTP_KYC_STRICT)) {
//				signature = SignatureBuilder.build(initiator, pin, apiUser.getPassKey());
//				rs = financialService.doKYCKTPStrict(userId, signature, parameter);
				rs = financialService.doKYCKTPStrictTemp(userId, signature, parameter);
				if(!Utils.isNullorEmptyString(rs.getAgentData().getMotherMaidenName())){
					AgentData ad = rs.getAgentData();
					ad.setMotherMaidenName("XXXXXX");
					ad.setNo_kk("XXXXXX");
					rs.setAgentData(ad);
				}
				result = rs.toString();
			}else if (serviceName.equals(Constant.SERVICE_PATH_INQUIRY_RESET_PIN)) {
//				signature = SignatureBuilder.build(initiator, pin, apiUser.getPassKey());
				rs = financialService.inquiryResetPINByAgent(userId, signature, to);
				result = rs.toString();
			}else if (serviceName.equals(Constant.SERVICE_PATH_COMMIT_RESET_PIN)) {
//				signature = SignatureBuilder.build(initiator, pin, apiUser.getPassKey());
				rs = financialService.resetPinByAgent(userId, signature, parameterMap);
				result = rs.toString();
			}else if (serviceName.equals(Constant.SERVICE_PATH_CHECK_POINT_DOMPETKU)) {
				signature = SignatureBuilder.build(initiator, pin, apiUser.getPassKey());
				rs = financialService.checkPointDompetku(userId, signature, parameter);
				result = rs.toString();
			}
			
			responseTime = new Date().getTime()-StartTime;
			if(rs.getStatus() == Constant.RC_TIMEOUT){
				mailSenderHandler.sendEmailNotification(this.emailList, "TIMEOUT ALERT", "Middleware TIMEOUT | TIME["+Utils.getCurrentDate("dd/MM/yyyy hh:mm:ss")+"]"+" On Service:"+serviceName.toLowerCase()
				+"<br /><b>Response Time:"+responseTime+"</b>");
			}
			
		} else {
			rs.setStatus(Constant.RC_INVALID_USERID);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			result = Utils.convertToXml(rs, Response.class);
		}

		try {
			PrintWriter out = response.getWriter();
			log.info("Result: " + result);
			out.write(result);
			long m = System.currentTimeMillis() - m1;
			log.info("response time: " + Long.toString(m) + " ms");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
