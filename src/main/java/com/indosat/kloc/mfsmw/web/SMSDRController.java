/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 11, 2015 
 * Time       : 6:55:29 PM 
 */
package com.indosat.kloc.mfsmw.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.dao.SMSOutboxDao;

@Controller
public class SMSDRController {

	private static Log log = LogFactory.getLog(SMSDRController.class);
	
	@Autowired
	private SMSOutboxDao smsOutboxDao; 

	@RequestMapping(value = "sms/dr/receive")
	public void doGetIMXDR(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="messageid", required=false) String messageId,
			@RequestParam(value="status", required=false) String status,
			@RequestBody String body) {
		Enumeration<?> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = (String) params.nextElement();
			String value = request.getParameter(paramName);
			log.debug("[paramName Name] - " + paramName + ", Value - " + value);
		}
		log.info("Receiving SMS Delivery Report *******************\n" + body);
		
		smsOutboxDao.saveDR(messageId, status);
		
		try {
			PrintWriter out = response.getWriter();
			out.write("OK");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
