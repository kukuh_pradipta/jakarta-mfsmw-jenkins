/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 19, 2014 
 * Time       : 3:19:43 PM 
 */
package com.indosat.kloc.mfsmw.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.pojo.AgentData;
import com.indosat.kloc.mfsmw.pojo.Parameter;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.processor.BillergwProcessor;
import com.indosat.kloc.mfsmw.processor.KycProcessor;
import com.indosat.kloc.mfsmw.processor.OTPPayproProcessor;
import com.indosat.kloc.mfsmw.sender.MailSenderHandler;
import com.indosat.kloc.mfsmw.service.FinancialService;
import com.indosat.kloc.mfsmw.soap.SessionHandler;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;

@Controller
@RequestMapping(value = "webapiinternal/{serviceName}")
public class WebApiInternalController {

	private static Log log = LogFactory.getLog(WebApiInternalController.class);

	@Autowired
	private SessionHandler sessionHandler;
	@Autowired
	private FinancialService financialService;
	@Autowired
	private BillergwProcessor billerGwService;
	@Autowired
	private OTPPayproProcessor otpPayproProcessor;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private KycProcessor kycProcessor;
	@Autowired
	private MailSenderHandler mailSenderHandler;
	
	@Value("${app.alert.email}")
	private String emailList;
	
	private long responseTime = 0l;

	@RequestMapping(method = RequestMethod.POST)
	public void doPost(HttpServletRequest request, HttpServletResponse response, Response rs,
			@RequestBody String body,
			@PathVariable("serviceName") String serviceName,
			@RequestParam(value = Constant.TO_PARAM, required = false) String to,
			@RequestParam(value = Constant.SIGNATURE_PARAM, required = false) String signature,
			@RequestParam(value = Constant.USERID_PARAM, required = false) String userId,
			@RequestParam(value = Constant.EXT_REF_PARAM, required = false) String extRef,
			@RequestParam(value = Constant.AMOUNT_PARAM, required = false) String amount,
			@RequestParam(value = Constant.COUPONID_PARAM, required = false) String couponId,
			@RequestParam(value = Constant.TRANSID_PARAM, required = false) String transid,
			@RequestParam(value = Constant.TARGET_PARAM, required = false) String target,
			@RequestParam(value = Constant.OPERATOR_NAME_PARAM, required = false) String operatorName,
			@RequestParam(value = Constant.COUNT_PARAM, required = false) Integer count,
			@RequestParam(value = Constant.EXTRA_PARAM, required = false) String extraParam) {

		Enumeration<?> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			log.debug("Header :: key[" + key + "] value[" + value + "]");
		}

		HashMap<String, String> parameterMap = new HashMap<String, String>();
		Enumeration<?> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = (String) params.nextElement();
			String value = request.getParameter(paramName);
			if (paramName.equals(Constant.SIGNATURE_PARAM))
				value = "XXXXXX";
			else if(paramName.equals(Constant.PIN_PARAM))
				 value = "XXXXXX";
			log.debug("[" + serviceName + "] Attribute Name - " + paramName + ", Value - " + value);
			parameterMap.put(paramName, request.getParameter(paramName));
		}

		Parameter parameter = new Parameter();
		parameter.setKeyValueMap(parameterMap);

		String result = "";
		ApiUser apiUser = dataStore.getApiUser(userId);
		
		//for asyncbiller
		switch (serviceName.toLowerCase()) {
			case "horekreatif":
				result = billerGwService.asyncHoreKreatif(body);
		}
		
		if (apiUser != null) {
			long StartTime =new Date().getTime();
			switch (serviceName.toLowerCase()) {
			case Constant.SERVICE_PATH_LOGIN:
				rs = financialService.doLogin(userId, signature);
				break;
			case Constant.SERVICE_PATH_CHECK_BALANCE:
				rs = financialService.balanceCheck(userId, to);
				break;
			case Constant.SERVICE_PATH_REVERSAL:
				rs = financialService.reversal(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_MERCHANT_CASHIN:
				rs = financialService.cashin(userId, signature, to, amount, extRef, false);
				break;
			case Constant.SERVICE_PATH_QUERY_BILLPAY:
				rs = financialService.queryBillPay(userId, signature, target, to, parameterMap);
				break;
			case Constant.SERVICE_PATH_BILLPAY:
				rs = financialService.billPay(userId, signature, target, to, amount, String.valueOf(transid),
						parameterMap);
				break;
			case Constant.SERVICE_PATH_EKTP:
				rs = financialService.eKTP(parameter);
				break;
			case Constant.SERVICE_PATH_EKTP_KYC:
//				rs = financialService.doKYCKTP(userId, signature, parameter);
				rs = financialService.doKYCKTPTemp(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_EKTP_KYC_BY_AGENT:
				rs = financialService.doKYCKTPByAgent(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_EKTP_KYC_BY_AGENT_REFERENCE:
				rs = financialService.doKYCKTPByAgentWithReference(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_EKTP_CONFIRM:
				rs = financialService.eKTPConfirm(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_EKTP_KYC_ONLY_NAME:
				rs = financialService.eKTPConfirmResultNameOnly(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_NUSANTARA_REG:
				rs = financialService.nusantaraReg(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_NUSANTARA_RESET_PIN:
				rs = financialService.resetPinNusantara(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_NUSANTARA_REFF_MAPPING:
				rs = financialService.nusantaraReferralMapping(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_MODIFIER:
				rs = financialService.updateUserBecomeModifier(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_PROG_INFO:
				rs = financialService.programInformer(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_REMOVE_AGENT:
				rs = financialService.removeAgent(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_MAP_UNMAP:
				rs = financialService.mapUnmap(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_UPDATEADMIN:
				rs = financialService.updateAdmin(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_SELL_NO_CONFIRM:
				rs = financialService.sellNoConfirm(userId, signature, to, amount, extRef);
				break;
			case Constant.SERVICE_PATH_DIRECTHITUTIBA:
				rs = financialService.directHitUtiba(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_GET_TRANSACTION_BY_EXT_REF:
				rs = financialService.checkTransactionById(userId, signature, extRef);
				break;
			case Constant.SERVICE_PATH_CHECK_GROUPING_USER:
				rs = financialService.checkGroupingName(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_REG_FIREBASE:
				rs = financialService.registerFirebase(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_HIT_BILLERGW:
				rs = financialService.billerGwTool(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_GET_WHITELABEL_SEQUENCE:
				rs = financialService.getWhitelabelSequence(userId, parameter);
				break;
			case Constant.SERVICE_PATH_SET_WHITELABEL_SEQUENCE:
				rs = financialService.setWhitelabelSequence(userId, signature, parameter);
				break;
			//user Initiator and PIN of Merchant in userID parameter
			case Constant.SERVICE_PATH_ADD_MERCHANT_USERID:
				rs = financialService.createNewMerchant(userId, signature, parameter);
				break;
			case "get_otp_token":
				result = otpPayproProcessor.getToken(parameter);
				break;
			case "verify_otp_token":
				result = otpPayproProcessor.verifyToken(parameter);
				break;
			// Test onebill
			case "onebill_inq":
				rs = financialService.oneBillInq(userId, signature, parameter);
				break;
			case "onebill_pay":
				rs = financialService.oneBillPay(userId, signature, parameter);
				break;
			}
			
			if(!Utils.isNullorEmptyString(rs.getAgentData().getMotherMaidenName())){
				AgentData ad = rs.getAgentData();
				ad.setMotherMaidenName("XXXXXX");
				ad.setNo_kk("XXXXXX");
				rs.setAgentData(ad);
			}
			
			if(!serviceName.toLowerCase().equalsIgnoreCase("get_otp_token") || !serviceName.toLowerCase().equalsIgnoreCase("verify_otp_token")){
				if (apiUser.getResponseType() == Constant.API_USER_STATUS_RESPONSE_TYPE_XML) {
					result = Utils.convertToXml(rs, Response.class);
				} else {
					result = Utils.convertToJSON(rs);
				}
			}

			responseTime = new Date().getTime()-StartTime;
			if(rs.getStatus() == Constant.RC_TIMEOUT){
				mailSenderHandler.sendEmailNotification(this.emailList, "TIMEOUT ALERT", "Middleware TIMEOUT | TIME["+Utils.getCurrentDate("dd/MM/yyyy hh:mm:ss")+"]"+" On Service:"+serviceName.toLowerCase()
				+"<br /><b>Response Time:"+responseTime+"</b>");
			}
		} else {
			rs.setStatus(Constant.RC_INVALID_USERID);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			result = Utils.convertToXml(rs, Response.class);
		}
		try {
			PrintWriter out = response.getWriter();
			log.info("Result: " + result);
			out.write(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@RequestMapping(method = RequestMethod.GET)
	public void doGet(HttpServletResponse response, Response rs, HttpServletRequest request,
			@PathVariable("serviceName") String serviceName,
			@RequestParam(value = Constant.USERID_PARAM, required = false) String userId,
			@RequestParam(value = Constant.SIGNATURE_PARAM, required = false) String signature,
			@RequestParam(value = Constant.TO_PARAM, required = false) String to,
			@RequestParam(value = Constant.PIN_PARAM, required = false) String pin,
			@RequestParam(value = Constant.INITIATOR_PARAM, required = false) String initiator) {

		HashMap<String, String> parameterMap = new HashMap<String, String>();
		Enumeration<?> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = (String) params.nextElement();
			String value = request.getParameter(paramName);
			if (paramName.equals(Constant.SIGNATURE_PARAM))
				value = "XXXXXX";
			log.debug("[" + serviceName + "] Attribute Name - " + paramName + ", Value - " + value);
			parameterMap.put(paramName, request.getParameter(paramName));
		}

		Parameter parameter = new Parameter();
		parameter.setKeyValueMap(parameterMap);

		long m1 = System.currentTimeMillis();

		String result = "";
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
//			if (serviceName.equals(Constant.SERVICE_PATH_LOGIN)) {
//				rs = financialService.doLogin(userId, signature);
//				result = "<result><status>[%STATUS%]</status><trxid>[%TRANSACTIONID%]</trxid><msg>[%ERROR_MESSAGE%]</msg></result>";
//				result = result.replace(Constant.STATUS_REPLACE_TAG, String.valueOf(rs.getStatus()));
//				result = result.replace(Constant.TRANSACTIONID_REPLACE_TAG, rs.getTrxid());
//				result = result.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, rs.getMsg());
//			} else if (serviceName.equals(Constant.SERVICE_PATH_SMS_KEYWORD)) {
//				signature = SignatureBuilder.build(initiator, pin, apiUser.getPassKey());
//				rs = financialService.smsKeyword(userId, signature, parameter);
//				result = rs.toString();
//			}
			switch (serviceName.toLowerCase()) {
			case Constant.SERVICE_PATH_EKTP_KYC_ONLY_NAME:
				rs = financialService.eKTPConfirmResultNameOnly(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_REG_FIREBASE:
				rs = financialService.registerFirebase(userId, signature, parameter);
				break;
			case Constant.SERVICE_PATH_EKTP_KYC:
//				rs = financialService.doKYCKTP(userId, signature, parameter);
				rs = financialService.doKYCKTPTemp(userId, signature, parameter);
				break;	
			}
			AgentData ad = rs.getAgentData();
			ad.setMotherMaidenName("XXXXXX");
			ad.setNo_kk("XXXXXX");
			if (apiUser.getResponseType() == Constant.API_USER_STATUS_RESPONSE_TYPE_XML) {
				rs.setAgentData(ad);
				result = Utils.convertToXml(rs, Response.class);
			} else {
				rs.setAgentData(ad);
				result = Utils.convertToJSON(rs);
			}
		} else {
			rs.setStatus(Constant.RC_INVALID_USERID);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			result = Utils.convertToXml(rs, Response.class);
		}

		try {
			PrintWriter out = response.getWriter();
			log.info("Result: " + result);
			out.write(result);
			long m = System.currentTimeMillis() - m1;
			log.info("response time: " + Long.toString(m) + " ms");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
