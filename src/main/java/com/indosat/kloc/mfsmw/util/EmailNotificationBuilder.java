/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Feb 13, 2015 
 * Time       : 2:54:38 PM 
 */
package com.indosat.kloc.mfsmw.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EmailNotificationBuilder {

	@Autowired
	private DataStore dataStore;
	
	@Value("${app.email.body.transaction}")
	private String bodyEmailTransaction;
	@Value("${app.email.body.transfer.receive}")
	private String bodyEmailReceiveTransfer;
	@Value("${app.email.body.ajsuccess}")
	private String bodyEmailAj;
	@Value("${app.firebase.body.ajtransfer}")
	private String bodyFirebaseAj;
	@Value("${app.firebase.header.ajtransfer}")
	private String shortBodyFirebaseAj;
	
	public String transaction(String trxTypeINA, String trxTypeEN, String to, String amount, String transid, int status){
		
		String text = bodyEmailTransaction;
		if(amount == null){
			text = text.replace("<tr><td>Nominal</td><td>: [%AMOUNT%]</td></tr>", "");
			text = text.replace("<tr><td>Amount</td><td>: [%AMOUNT%]</td></tr>", "");
		}
			
		text = text.replace(Constant.DATE_REPLACE_TAG, Utils.getCurrentDate("dd/MM/yyyy HH:mm:ss"));
		text = text.replace(Constant.TRANSACTION_TYPE_REPLACE_TAG_INA, trxTypeINA);
		text = text.replace(Constant.TRANSACTION_TYPE_REPLACE_TAG_EN, trxTypeEN);
		text = text.replace(Constant.TO_REPLACE_TAG, to);
		
		if(!Utils.isNullorEmptyString(amount))
			text = text.replace(Constant.AMOUNT_REPLACE_TAG, Utils.convertStringtoCurrency(amount));
		
		text = text.replace(Constant.REFERENCEID_REPLACE_TAG, transid);
		text = text.replace(Constant.STATUS_REPLACE_TAG, dataStore.getErrorMsg(status));
		
		return text;
	}
	
	public String transactionAJ(String trxTypeINA, String trxTypeEN, String to,String to_name, String from,String from_name,String amount, String transid,String rrn, String statusMsg,  String bankCode, String bankName,String nameRek){
		
		String text = bodyEmailAj;
		if(amount == null){
			text = text.replace("<tr><td>Nominal</td><td>: [%AMOUNT%]</td></tr>", "");
			text = text.replace("<tr><td>Amount</td><td>: [%AMOUNT%]</td></tr>", "");
		}
			
		text = text.replace(Constant.DATE_REPLACE_TAG, Utils.getCurrentDate("dd/MM/yyyy HH:mm:ss"));
		text = text.replace(Constant.TRANSACTION_TYPE_REPLACE_TAG_INA, trxTypeINA);
		text = text.replace(Constant.TRANSACTION_TYPE_REPLACE_TAG_EN, trxTypeEN);
		text = text.replace(Constant.TO_REPLACE_TAG, to);
		text = text.replace(Constant.TO_NAME_REPLACE_TAG, to_name);
		text = text.replace(Constant.FROM_REPLACE_TAG, from);
		text = text.replace(Constant.FROM_NAME_REPLACE_TAG, from_name);
		text = text.replace(Constant.BANK_NAME_REPLACE_TAG, bankName);
		text = text.replace(Constant.BANK_CODE_REPLACE_TAG, bankCode);
		text = text.replace(Constant.NAME_REPLACE_TAG, nameRek);
		
		if(!Utils.isNullorEmptyString(amount))
			text = text.replace(Constant.AMOUNT_REPLACE_TAG, Utils.convertStringtoCurrency(amount));
		
		text = text.replace(Constant.REFERENCEID_REPLACE_TAG, transid);
		text = text.replace(Constant.RRN_REPLACE_TAG, rrn);
		text = text.replace(Constant.STATUS_REPLACE_TAG, statusMsg);
		
		return text;
	}
	
	
	public String transactionReceiver(String trxTypeINA, String trxTypeEN, String to, String amount, String transid, int status){
		
		String text = bodyEmailReceiveTransfer;
		text = text.replace("[%DATE%]", Utils.getCurrentDate("dd/MM/yyyy HH:mm:ss"));
		text = text.replace(Constant.TRANSACTION_TYPE_REPLACE_TAG_INA, "Menerima Uang");
		text = text.replace(Constant.TRANSACTION_TYPE_REPLACE_TAG_EN, "Received Money");
		text = text.replace("[%FROM%]", to);
		text = text.replace("[%AMOUNT%]", Utils.convertStringtoCurrency(amount));
		text = text.replace("[%REF%]", transid);
		text = text.replace("[%STATUS%]", dataStore.getErrorMsg(status));
		
		return text;
	}
	
	public String firebaseTransferToBankMsg(String type,String status,String amount,String fromname,String toname,String torek,String bankname,String rrn, String ref){
		String text="";
		if(type.equalsIgnoreCase("full")){
			text = bodyFirebaseAj;
			text = text.replace(Constant.STATUS_REPLACE_TAG,status);
			text = text.replace(Constant.DATETIME_REPLACE_TAG,Utils.getCurrentDate("dd/MM/yyyy HH:mm:ss"));
			text = text.replace(Constant.AMOUNT_REPLACE_TAG,amount);
			text = text.replace(Constant.FROM_NAME_REPLACE_TAG,fromname);
			text = text.replace(Constant.TO_NAME_REPLACE_TAG,toname);
			text = text.replace(Constant.TO_REPLACE_TAG,torek);
			text = text.replace(Constant.BANK_NAME_REPLACE_TAG,bankname);
			text = text.replace(Constant.RRN_REPLACE_TAG,rrn);
			text = text.replace(Constant.REFERENCEID_REPLACE_TAG,ref);
		}else if(type.equalsIgnoreCase("short")){
			text = shortBodyFirebaseAj;
			text = text.replace(Constant.STATUS_REPLACE_TAG,status);
			text = text.replace(Constant.DATETIME_REPLACE_TAG,Utils.getCurrentDate("dd/MM/yyyy HH:mm:ss"));
			text = text.replace(Constant.AMOUNT_REPLACE_TAG,amount);
			text = text.replace(Constant.REFERENCEID_REPLACE_TAG,ref);
		}
		return text;
	}
}
