/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 5, 2015 
 * Time       : 11:23:50 AM 
 */
package com.indosat.kloc.mfsmw.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.pojo.AuthResponse;
import com.indosat.kloc.mfsmw.soap.SessionHandler;
import com.indosat.kloc.mfsmw.soap.UTIBAHandler;

@Component
public class AuthenticationRequest {

	@Autowired
	private UTIBAHandler utibaHandler;
	@Autowired
	private SessionHandler sessionHandler;
	@Autowired
	private DataStore dataStore;
	
	public AuthResponse validate(String userId, String signature){
		
		AuthResponse response = new AuthResponse();
	
		ApiUser apiUser = dataStore.getApiUser(userId);
		if(apiUser != null){
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if(getCredential != null){
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if(validateSession == Constant.RC_SUCCESS){
					response.setStatus(Constant.RC_SUCCESS);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
					response.setInitiator(getCredential[0]);
					response.setPassword(getCredential[1]);
					response.setSessionId(sessionId);
				}else{
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			}else{
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		}else{
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		
		return response;
	}
	
	
}
