/*
 * Project Name: Mobile Financial Service Middleware
 * Created By  : Megi Jaka Permana
 * Created Date: May 2, 2014 6:19:45 PM
 */
package com.indosat.kloc.mfsmw.util;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import sun.misc.BASE64Encoder;

/**
 * Aes encryption
 */
public class AESEncryption {
	
	public static String encrypt(String strToEncrypt,String key) {
		String res;
		//Create key and cipher
	    Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
	    try{
		    Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		    // encrypt the text
		    cipher.init(Cipher.ENCRYPT_MODE, aesKey);
		    byte[] encrypted = cipher.doFinal(strToEncrypt.getBytes());
		    String encryptedValue = new BASE64Encoder().encode(encrypted);
//		    String utfval = URLEncoder.encode(encryptedValue, "UTF-8");
		    res = encryptedValue;
	    }catch(Exception ex){
	    	ex.printStackTrace();
	    	res = null;
	    }
		return res;
	}
	
	public static String decrypt(String strToDecrypt,String key) {
		String res;
		//Create key and cipher
		 Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
		    try{
			    Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			    cipher.init(Cipher.DECRYPT_MODE, aesKey);
		        byte[] decodedValue = new Base64().decode(strToDecrypt);
		        byte[] decValue = cipher.doFinal(decodedValue);
		        String decryptedValue = new String(decValue);
//			    byte[] encrypted = cipher.doFinal(strToDecrypt.trim().getBytes());
//			    String encryptedValue = new BASE64Encoder().encode(encrypted);
////			    String utfval = URLEncoder.encode(encryptedValue, "UTF-8");
			    res = decryptedValue;
		    }catch(Exception ex){
		    	ex.printStackTrace();
		    	res = null;
		    }
		return res;
	}

	public static void main(String args[]) {
		final String strToEncrypt = "61basuki1";
		final String strPssword = "XGat3_Domp3tku_+";
		String encrypt = encrypt(strToEncrypt,strPssword);
		String decrypt = decrypt("00LUh8gXPLWeOMjUo53HEg==", strPssword);
//		System.out.println(encrypt);
//		System.out.println(decrypt);
//		System.out.println(decrypt("ui%2FCIDQFjSENHDmYR8rSNQ%3D%3D",strPssword));
	}

}