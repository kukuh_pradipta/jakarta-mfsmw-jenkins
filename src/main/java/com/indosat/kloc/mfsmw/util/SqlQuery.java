/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 23, 2015 
 * Time       : 10:04:03 AM 
 */
package com.indosat.kloc.mfsmw.util;

public class SqlQuery {

	public static final String KYC_RAW_DATA_GET_ALL = "SELECT * FROM kyc_raw_data WHERE(1=1) ";
	public static final String KYC_DATA_SAVE = "INSERT INTO kyc_data(msisdn, name, idno, dob, motherMaidenName, gender) "
			+ "VALUES(?,?,?,?,?,?)";
	
	public static final String M_BILLER_TRANSACTION_GET_ALL = "SELECT * FROM m_biller_transaction WHERE (1=1) ";
	public static final String SMS_NOTIFICATION_TEMPLATE_GET_ALL = "SELECT * FROM sms_notification_template WHERE(1=1) ";
	
	public static final String PREFIX_REFERENCE_GET_ALL = "SELECT * FROM prefix_reference WHERE(1=1) ";
	
	//============ m_biller_transaction ===============
		public static final String M_BILLER_TRANSACTION_SAVE = "INSERT INTO m_biller_transaction"
				+ " (transId, refTransId, walletTransId, billerTransId, channelTransId, productId, type, initiator, destination, destinationName, amount, price, fee, details, msg, dateTime, responseTime, responseCode) "
				+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";	
	//====================================
		
	public static final String ERROR_CODE_GET_ALL = "SELECT * FROM error_code WHERE(1=1) ";	
	
	//======= DMT=======================================
	public static final String REMIT_COUPON_INQ_SAVE = "INSERT INTO remit_coupon_inq(sessionId, transId, extRef, "
			+ "storeName, amount, recipientPhoneNumber, recipientName, recipientSourceOfFund, recipientPurpose, "
			+ "recipientAddress, recipientProvince, recipientCity, senderIdType, senderIdNo, senderName, senderPhoneNumber, status, trxTime, uniqueCode, note) "
			+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public static final String REMIT_COUPON_GET_BY_TRANSID_AND_EXTREF = "SELECT * FROM remit_coupon_inq WHERE transId =? AND  extRef=? AND STATUS = 0 ";
	public static final String REMIT_COUPON_UPDATE_STATUS = "UPDATE remit_coupon_inq SET STATUS=1 ,trxTime=? WHERE transId =? AND  extRef=? ";
	
	public static final String REMIT_USER_SAVE = "INSERT INTO dmt.remittance_user"
			+ "(idNo, idType, idExp, name, phoneNumber, gender, address, province, city, pob, dob, occupation, nationality, aggreeReg) "
			+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public static final String REMIT_USER_UPDATE = "UPDATE dmt.remittance_user SET idExp=?, "
			+ "phoneNumber=?, gender=?, address=?, province=?, city=?, pob=?, dob=?, occupation=?, nationality=?, aggreeReg=? "
			+ "WHERE idType=? AND idNo=? ";
			
	
	public static final String REMIT_USER_GET_ALL = "SELECT * FROM dmt.remittance_user WHERE(1=1) ";
	
	public static final String REMIT_SAVE_TRANSACTION = "INSERT INTO dmt.transaction(parentId, transId, extRef, initiator, "
			+ "couponId, storeName, recipientPhoneNumber, recipientName, recipientSourceOfFund, recipientPurpose, recipientAddress, recipientProvince, recipientCity, "
			+ "senderIdType, senderIdNo, senderPhoneNumber, amount, fee, total, statusMessage, status, trxTime, type, uniqueCode, note, respTime) "
			+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
	
	//========== SMS OUTBOX
	public static final String SMS_OUTBOX_SAVE = "INSERT INTO dmt.sms_outbox(transid, msisdn, sms, responseBody, messageId, status) VALUES(?,?,?,?,?,?)";
	public static final String SMS_OUTBOX_GET_BY_TRANSID = "SELECT * FROM dmt.sms_outbox WHERE transid=?";
	public static final String SMS_OUTBOX_UPDATE_COUNTER = "UPDATE dmt.sms_outbox SET counter=counter-1 WHERE transid=?";
	public static final String SMS_OUTBOX_GET = " SELECT msisdn, sms, messageId FROM dmt.sms_outbox WHERE(1=1) ";
	
	//========== SMS DR
	public static final String SMS_DR_SAVE = "INSERT INTO dmt.sms_dr(messageId, status) VALUES(?,?) ";
	public static final String SMS_DR_GET = "SELECT id FROM dmt.sms_dr WHERE messageId=? AND status=? ";
	
	//====== DMT
	public static final String TRX_FEE_GET_FEE = "SELECT fee from dmt.trx_fee WHERE ? between min AND max";
	public static final String TRX_H2H_FEE_GET_FEE = "SELECT fee from dmt.h2h_trx_fee WHERE ? between min AND max and agent=?";
	public static final String DMT_TRAX_GET_BY_EXTREF = "SELECT transid, extRef, trxTime, amount, status, couponId FROM dmt.transaction WHERE extRef=? AND type !=0 order by trxTime desc limit 1 ";
	public static final String DMT_AGENT_SELECT_ALL = "SELECT initiator, storeId FROM dmt.agent WHERE status=1 ";
	public static final String DMT_PARTNER_SELECT_ALL = "SELECT id, initiator, name, ip_address, isAllowed, status FROM dmt.remittance_partner WHERE(1=1) AND status=1 ";
	public static final String DMT_GET_INITIATOR_BY_TRANSID = "SELECT initiator FROM dmt.transaction where transid=? and status=0 and type=0 limit 1";
	public static final String DMT_GET_WALLET2CASH_TRX_BY_TRANSID= "SELECT initiator FROM dmt.transaction where transId=? AND type in(4,6) limit 1";
	
	//====== OverhandlingWallet
	public static final String EMPLOYEE_UPDATE_STATUS = "UPDATE employee_isat SET virtualAmount=? ,mainAmount=? WHERE mainWallet =? ";
	public static final String REMITTANCE_GET_ALL = "SELECT * FROM remittance_scheduler";
	public static final String REMITUSER_UPDATE_STATUS = "UPDATE remittance_scheduler SET savingAmount=? ,mainAmount=? WHERE msisdn =? ";
	public static final String REMITUSER_DELETE = "DELETE FROM remittance_scheduler WHERE msisdn =? ";
	public static final String REMITUSER_INSERT = "INSERT INTO wallet_overhandler.remittance_scheduler(msisdn) VALUES(?)";

	//====== User Wallet
	public static final String USERWALLET_SAVE_TRANSACTION = "INSERT INTO mfsmw.user_wallet(msisdn,name,idno,idtype,countrycode,gender,dob,address,wallet_type,status,encrypted_text,encrypted_code,created_time,updated_time) "
			+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
	
}
