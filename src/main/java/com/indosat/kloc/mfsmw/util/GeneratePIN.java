/**
 * Project    : Mobile Financial Service Middleware 
 * Created By : Megi Jaka Permana
 * Date       : Jun 25, 2014 
 * Time       : 10:36:59 AM 
 */
package com.indosat.kloc.mfsmw.util;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GeneratePIN {

	@Value("${app.utiba.initiator}")
	private String initiator;
	@Value("${app.utiba.pin}")
	private String password;

	
	public String generate(String sessionId, String username, String password){
		String pin = username.toLowerCase() +password;
		try {
			pin = AeSimpleSha1.SHA1(pin).toLowerCase();
			pin = sessionId + pin;
			pin = AeSimpleSha1.SHA1(pin).toUpperCase();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return pin;
	}
	
	/*public String generate(String initiator, String password) throws Exception{
		StringBuilder builder = new StringBuilder();
		StringBuilder reversePin = new StringBuilder(password).reverse();
		builder.append(Utils.getCurrentDate("HHmmsss")).append(password).append("|").append(reversePin).append(initiator);
		DESedeEncryption encryption = new DESedeEncryption(publicKey);
		return encryption.encrypt(builder.toString());
	}*/
	
	
	public static void main(String[] args) throws Exception {
		//GeneratePIN pin = new GeneratePIN();
		//System.out.println(pin.generate("1UT3UKBGYNNRTB5W34P5", "SOAPCLIENT", "secret"));
		StringBuilder builder = new StringBuilder();
		//StringBuilder reversePin = new StringBuilder(123456).reverse();
		builder.append(Utils.getCurrentDate("HHmmsss")).append(123456).append("|").append("654321").append("outlet_1");
		DESedeEncryption encryption = new DESedeEncryption("Th1s_0nLy_F0uR_t3sT1nG__");
//		System.out.println(builder.toString());
//		System.out.println(encryption.encrypt(builder.toString()));
	}
	
	
}
