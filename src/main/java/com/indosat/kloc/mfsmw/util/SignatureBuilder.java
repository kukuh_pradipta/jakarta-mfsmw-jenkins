/**
 * Project    : Dompetku - Domestic Money Transfer
 * Created By : Megi Jaka Permana
 * Date       : Apr 13, 2015 
 * Time       : 10:37:35 AM 
 */
package com.indosat.kloc.mfsmw.util;


public class SignatureBuilder {

	public static String build(String username, String password, String key){
		try {
			StringBuilder builder = new StringBuilder();
			StringBuilder reversePin = new StringBuilder(password).reverse();
			builder.append(Utils.getCurrentDate(Utils.getCurrentDate("HHmmsss"))).append(password).append("|").append(reversePin).append("|").append(username);
			DESedeEncryption encryption = new DESedeEncryption(key);
			return encryption.encrypt(builder.toString());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
			return null;
		}
		
	}
	
}
