/**
 * Project    : OTT Wallet
 * Created By : Megi Jaka Permana
 * Date       : Feb 12, 2015 
 * Time       : 10:19:19 AM 
 */
package com.indosat.kloc.mfsmw.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class AeSimpleSha2 {

	public static String SHA2(String text) throws NoSuchAlgorithmException{
		
		 MessageDigest md = MessageDigest.getInstance("SHA-256");
		 md.update(text.getBytes());
		 byte byteData[] = md.digest();
		 //convert the byte to hex format method 1
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < byteData.length; i++) {
	        	sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }
	     return sb.toString();   
	}
	
}
