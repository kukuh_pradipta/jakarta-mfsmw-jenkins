/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jun 27, 2014 
 * Time       : 3:08:51 PM 
 */
package com.indosat.kloc.mfsmw.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.indosat.kloc.mfsmw.dao.ApiUserDao;
import com.indosat.kloc.mfsmw.dao.BillerDao;
import com.indosat.kloc.mfsmw.dao.DenomDao;
import com.indosat.kloc.mfsmw.dao.ErrorCodeDao;
import com.indosat.kloc.mfsmw.dao.ExtraTransDataDao;
import com.indosat.kloc.mfsmw.dao.NotificationDao;
import com.indosat.kloc.mfsmw.dao.OTPCounterDao;
import com.indosat.kloc.mfsmw.dao.PrefixDao;
import com.indosat.kloc.mfsmw.dao.ProductDao;
import com.indosat.kloc.mfsmw.dao.ReferralTypeDao;
import com.indosat.kloc.mfsmw.dao.RemittanceAgentDao;
import com.indosat.kloc.mfsmw.dao.TargetBillerDao;
import com.indosat.kloc.mfsmw.dao.UserWalletDao;
import com.indosat.kloc.mfsmw.dao.WhitelabelSequenceDao;
import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.model.Biller;
import com.indosat.kloc.mfsmw.model.Denom;
import com.indosat.kloc.mfsmw.model.ErrorCode;
import com.indosat.kloc.mfsmw.model.ExtraTransData;
import com.indosat.kloc.mfsmw.model.IndosmartErrorCode;
import com.indosat.kloc.mfsmw.model.MBiller;
import com.indosat.kloc.mfsmw.model.OTPCounter;
import com.indosat.kloc.mfsmw.model.Prefix;
import com.indosat.kloc.mfsmw.model.PrefixReference;
import com.indosat.kloc.mfsmw.model.Product;
import com.indosat.kloc.mfsmw.model.RefKecamatan;
import com.indosat.kloc.mfsmw.model.ReferralType;
import com.indosat.kloc.mfsmw.model.RemittancePartner;
import com.indosat.kloc.mfsmw.model.SmsNotificationTemplate;
import com.indosat.kloc.mfsmw.model.WhitelabelSequence;
import com.indosat.kloc.mfsmw.pojo.Target;
import com.indosat.kloc.mfsmw.processor.OTPWriteHandler;
import com.indosat.kloc.mfsmw.soap.SessionHandler;

@Component
public class DataStore {

	private static Log log = LogFactory.getLog(DataStore.class);
	
	@Autowired
	private NotificationDao notificationDao;
	@Autowired
	private ProductDao productDao;
	@Autowired
	private TargetBillerDao targetDao;
	@Autowired
	private SessionHandler sessionHandler;
	@Autowired
	private ApiUserDao apiUserDao;
	@Autowired
	private BillerDao billerDao;
	@Autowired
	private ExtraTransDataDao extraTransDataDao;
	@Autowired
	private ErrorCodeDao errorCodeDao;
	@Autowired
	private PrefixDao prefixDao;
	@Autowired
	private DenomDao denomDao;
	@Autowired
	private OTPCounterDao otpCounterDao;
	@Autowired
	private TaskExecutor sendExecutor;
	@Autowired
	private RemittanceAgentDao remittanceAgentDao; 
	@Autowired
	private WhitelabelSequenceDao whitelabelSequenceDao; 
	@Autowired
	private UserWalletDao userWalletDao; 
	@Autowired
	private ReferralTypeDao referralTypeDao;
	
	@Value("${app.otp.counter}")
	private int otpCounter;
	
	private String globalNFCSessionId;
	private String globalSessionId;
	private String isatPrefix[];
	private Map<String, ApiUser> apiUserMap = new HashMap<String, ApiUser>();
	private Map<String, ApiUser> apiMerchantMap = new HashMap<String, ApiUser>();
	private HashMap<String,List<String>> apiIPUserMap = new HashMap<String,List<String>>();
	private HashMap<String,List<String>> apiLimitUserMap = new HashMap<String,List<String>>();

	private Map<Integer, String> errorMap = new HashMap<Integer, String>();
	private Map<String, String> orangeTvErrorMap = new HashMap<String, String>();
	private Map<String, String> indosmartErrorMap = new HashMap<String, String>();
	private HashMap<String, String> compCarderrorMap = new HashMap<String, String>();
	private Map<String, Biller> billerMap = new HashMap<String, Biller>();
	private List<Prefix> prefixList = new LinkedList<Prefix>();
	private List<PrefixReference> prefixRefList = new LinkedList<PrefixReference>();
	private Map<Integer, HashMap<String, String>> denomMap = new HashMap<Integer, HashMap<String,String>>();
	private Map<String, Integer> otpCounterMap = new HashMap<String, Integer>();
	private Map<String, Target> targetMap = new HashMap<String, Target>();
	private Map<Integer, MBiller> mbillerMap = new HashMap<Integer, MBiller>();
	private Map<Integer, Product> productTargetIdMap= new HashMap<Integer, Product>();
	private Map<Integer, Product> productReferenceIdMap= new HashMap<Integer, Product>();
	private Map<String, Product> productProductCodeMap= new HashMap<String, Product>();
	private Map<Integer, Product> productMap= new HashMap<Integer, Product>();
	private Map<Integer, String> smsNotifMap = new HashMap<Integer, String>();	
	private Map<String, RemittancePartner> remittancePartnerMap = new HashMap<String, RemittancePartner>();
	private Multimap<String, WhitelabelSequence> whitelabelMap = ArrayListMultimap.create();
	private List<String> kecamatanCodeList = new LinkedList<String>();
	private Map<String, ReferralType> referallTypeMap = new HashMap<String, ReferralType>();
	
	//TEMP
	
	@Scheduled(fixedRate=3600000)
	private void init(){
		log.info("populate data to data stores......");
		setGlobalSessionId();
		loadApiUserMap();
		loadErrorCode();
		loadBiller();
		loadOrangeTvErrorMap();
		loadIndosmartErrorMap();
		loadPrefixList();
		loadTarget();
		loadMbillerMap();
		loadProduct();
		loadSMSNotification();
		loadPrefixReference();
		loadCompCardErrorMessage();
		loadRemittancePartner();
		loadKecamatanList();
		loadReferralType();
	}
	
	@Scheduled(cron="0 0 0 * * ?")
	private void resetOTPCounter(){
		log.info("Reset All OTP From Object Object........");
		synchronized (otpCounterMap) {
			otpCounterMap.clear();
			otpCounterDao.removeAll();
		}
		
	}
	
	@PostConstruct
	private void loadOTPCounter(){
		List<OTPCounter> list = otpCounterDao.getAll();
		synchronized (otpCounterMap) {
			for(OTPCounter obj: list){
				otpCounterMap.put(obj.getUsername(), obj.getCounter());
			}
		}
		
		log.info("Load OTP Counter From DB, Size: "+ otpCounterMap.size());
	}
	
	private void loadPrefixReference(){
		synchronized (prefixRefList) {
			prefixRefList = prefixDao.getAllPrefixReference();
		}
		
		log.info("Load Prefix Reference size: "+prefixRefList.size());
	}
	
	public String getCompCardErrorMessage(String code){
		return compCarderrorMap.get(code);
	}
	
	public List<PrefixReference> getPrefixRefList(){
		return prefixRefList;
	}
	
	private void loadSMSNotification(){
		List<SmsNotificationTemplate> list = notificationDao.getAll();
		synchronized (smsNotifMap) {
			for(SmsNotificationTemplate obj: list)
				smsNotifMap.put(obj.getId(), obj.getMessage());
		}
		
			
		log.info("Load SMS Notification Template Size: "+list.size());
	}
	
	public String getSMSNotificationByTitle(String title){
		List<SmsNotificationTemplate> list = notificationDao.getAllByTitle(title);
//		log.debug(list.toString());
		return list.get(0).getMessage();
	}
	
	public String getSMSNotificationTemplate(int id){
		return smsNotifMap.get(id);
	}
	
	private void loadProduct(){
		
		List<Product> products = productDao.getAllActive();
		
		for(Product obj : products){
			
			synchronized (productTargetIdMap) {
				productTargetIdMap.put(obj.getTargetId(), obj);
			}
			
			synchronized (productReferenceIdMap) {
				productReferenceIdMap.put(obj.getProductRefId(), obj);
			}
			
			synchronized (productProductCodeMap) {
				if(obj.getProductCode()!=null)
					productProductCodeMap.put(obj.getProductCode(), obj);
			}
			
			synchronized (productMap) {
				productMap.put(obj.getId(), obj);
			}
			
		}
		
		log.info("Load Product from DB size: "+products.size());
	}
	
	public Product getProductByTargetId(int targetId){
		return productTargetIdMap.get(targetId);
	}
	
	public Product getProductByProductCode(String productCode){
		return productProductCodeMap.get(productCode);
	}
	
	public Product getProductByReferenceId(int productRefId){
		return productReferenceIdMap.get(productRefId);
	}
	
	public Product getProductById(int id){
		return productMap.get(id);
	}
	
	private void loadMbillerMap(){
		List<MBiller> mBillers = billerDao.getAllMBiller();
		synchronized (mbillerMap) {
			for(MBiller obj: mBillers)
				mbillerMap.put(obj.getId(), obj);
		}
		
		log.info("Load MBillercfrom DB, Size: "+ mbillerMap.size());
	}
	
	public MBiller getMbiller(int billerId){
		return mbillerMap.get(billerId);
	}
	
	public Target getTarget(String target){
		return targetMap.get(target);
	}
	
	public Integer getOtpCounter(String username){
		Integer counter = otpCounterMap.get(username);
		if(counter == null){
			OTPWriteHandler otpWriteHandler = new OTPWriteHandler();
			otpWriteHandler.setCounterDao(otpCounterDao);
			otpWriteHandler.setOperationType(Constant.OPERATION_TYPE_INSERT);
			otpWriteHandler.setUsername(username);
			sendExecutor.execute(otpWriteHandler);
			otpCounterMap.put(username, otpCounter);
			counter = otpCounter;
		}
		
		return counter;
			
	}
	
	public void updateOTPCounter(String username, Integer counter){
		otpCounterMap.put(username, counter);
		OTPWriteHandler otpWriteHandler = new OTPWriteHandler();
		otpWriteHandler.setCounterDao(otpCounterDao);
		otpWriteHandler.setOperationType(Constant.OPERATION_TYPE_UPDATE);
		otpWriteHandler.setUsername(username);
		otpWriteHandler.setCounter(counter);
		sendExecutor.execute(otpWriteHandler);
	}
	
	public ApiUser getApiUser(String userId){
		if(apiUserMap.size() == 0)
			loadApiUserMap();
		if(apiMerchantMap.size() == 0)
			loadApiUserMap();
		
		ApiUser res = apiUserMap.get(userId);
		if(res==null){
			res = apiMerchantMap.get(userId);
		}
		return res;
		//return apiUserMap.get(userId);
	}
	
//	public ApiUser getApiUser(String userId){
//		if(apiUserMap.size() == 0)
//			loadApiUserMap();
//		return apiUserMap.get(userId);
//	}
	
	public List<String> getIPApiUser(String userId){
		if(apiIPUserMap.size() == 0)
			loadApiUserMap();
		return apiIPUserMap.get(userId);
	}
	
	public List<String> getLimitApiUser(String userId){
		if(apiLimitUserMap.size() == 0)
			loadApiUserMap();
		return apiLimitUserMap.get(userId);
	}
	
	public String getErrorMsg(int id){
		if(errorMap.get(id) == null)
			return "UNKNOW ERROR";
		return errorMap.get(id);
	}
	
	public Biller getBiller(String name){
		if(billerMap.isEmpty())
			this.loadBiller();
		return billerMap.get(name.toUpperCase());
	}
	
	public String getORTVErrorMessage(String errorCode){
		return orangeTvErrorMap.get(errorCode);
	}
	
	public String getIndosmartErrorMessage(String errorCode){
		return indosmartErrorMap.get(errorCode);
	}
	
	public Prefix getTelcoIdByPrefix(String msisdn){
		Prefix res = new Prefix(); 
		if(prefixList.size() == 0)
			this.loadPrefixList();
		synchronized (prefixList) {
			for(Prefix obj: prefixList){
				String [] prefixArr = obj.getPrefixNo().split("\\,");
				for(String prefix : prefixArr){
					//System.out.println("msisdn: "+msisdn+ " prefix.trim(): "+prefix.trim());
					if(msisdn.startsWith(prefix.trim())){
						res = obj;
						return res;
					}
				}
			}
		}
		
		return res;
	}
	
	public HashMap<String, String> getDenomMap(int prefixId){
		synchronized (denomMap) {
			return denomMap.get(prefixId);
		}
	}
	
	public boolean isIsatPrefix(String msisdn){
		for(String obj : this.isatPrefix){
			if(msisdn.startsWith(obj))
				return true;
		}
		return false;
	}
	
	private void loadTarget(){
		List<Target> list =targetDao.getAll();
		synchronized (targetMap) {
			for(Target obj: list){
				targetMap.put(obj.getType(), obj);
			}
		}
		log.info("Load Target From DB: Size["+list.size()+"]");
	}
	
	private void loadPrefixList(){
		synchronized (prefixList) {
			prefixList = prefixDao.getAll();
			//System.out.println("prefixList.size(): "+prefixList.size());
			for(Prefix prefix : prefixList){
				List<Denom> denomList = denomDao.getAll(prefix.getId());
				HashMap<String, String> denomMap = new HashMap<String, String>();
				for(Denom denom: denomList){
					denomMap.put(denom.getDenom(), denom.getPrice());
				}
				this.denomMap.put(prefix.getId(), denomMap);
				
				if(prefix.getName().equalsIgnoreCase("INDOSAT")){
					this.isatPrefix = prefix.getPrefixNo().replace("0", "62").split(",");
				}
			}
		}
	}
	
	private void loadApiUserMap(){
		List<ApiUser> list = apiUserDao.getAll();
		List<ApiUser> listMerchant = apiUserDao.getAllMerchantUser();
//		List<String> ips = new ArrayList<String>();
//		List<String> apis = new ArrayList<String>();
		synchronized (apiUserMap) {
			for(ApiUser obj: list){
//				for(String ip: obj.getIpList().split(",")){
//					ips.add(ip);
//				}
//				for(String apiLimit: obj.getApiList().split(",")){
//					apis.add(apiLimit);
//				}
				apiUserMap.put(obj.getUserId(), obj);
//				apiIPUserMap.put(obj.getUserId(), ips);
//				apiLimitUserMap.put(obj.getUserId(), apis);
			}
		}
		synchronized (apiMerchantMap) {
			for(ApiUser obj: listMerchant){
				apiMerchantMap.put(obj.getUserId(), obj);
			}
		}
		log.info("Load Api User size: "+list.size());
		log.info("Load Api Merchant User size: "+list.size());
	}
	
//	private void loadApiUserMap(){
//		List<ApiUser> list = apiUserDao.getAll();
////		List<String> ips = new ArrayList<String>();
////		List<String> apis = new ArrayList<String>();
//		synchronized (apiUserMap) {
//			for(ApiUser obj: list){
////				for(String ip: obj.getIpList().split(",")){
////					ips.add(ip);
////				}
////				for(String apiLimit: obj.getApiList().split(",")){
////					apis.add(apiLimit);
////				}
//				apiUserMap.put(obj.getUserId(), obj);
////				apiIPUserMap.put(obj.getUserId(), ips);
////				apiLimitUserMap.put(obj.getUserId(), apis);
//			}
//		}
//		
//		log.info("Load Api User size: "+list.size());
//	}
	
	private void loadBiller(){
		synchronized (billerMap) {
			List<Biller> list = billerDao.getAll();
			for(Biller obj: list){
				List<ExtraTransData> extdqbp = extraTransDataDao.getAllByTemplateId(obj.getId(), Constant.EXT_TRANS_DATA_TYPE_QUERY_BILLPAY);
				//System.out.println("obj.getId():"+obj.getId()+" extdqbp" +extdqbp.size());
				List<ExtraTransData> extdbp = extraTransDataDao.getAllByTemplateId(obj.getId(), Constant.EXT_TRANS_DATA_TYPE_BILLPAY);
				//System.out.println("obj.getId():"+obj.getId()+" extdbp" +extdbp.size());
				//System.out.println();
				obj.setExtTransDataQueryBillpay(extdqbp);
				obj.setExtTransDataBillpay(extdbp);
				billerMap.put(obj.getName().toUpperCase(), obj);
			}
			log.info("Get billerMap Size: "+billerMap.size());
		}
	}

	public String getGlobalSessionId() {
		if(globalSessionId == null)
			setGlobalSessionId();
		return globalSessionId;
	}

	public void setGlobalSessionId() {
		this.globalSessionId = sessionHandler.getValidGlobalSessionId();
	}
	
	public String getGlobalNFCSessionId(){
		if(globalNFCSessionId == null)
			setGlobalNFCSessionId();
		return globalNFCSessionId;
	}
	
	public void setGlobalNFCSessionId() {
		this.globalNFCSessionId = sessionHandler.getValidGlobalNFCSession();
	}
	
	public void loadErrorCode(){
		
		List<ErrorCode> list = errorCodeDao.getList();
		synchronized (errorMap) {
			for(ErrorCode obj : list)
				errorMap.put(obj.getCode(), obj.getDescription());
		}
		
		log.info("Load ERROR CODE size:"+list.size());
		
		/*errorMap.put(Constant.RC_SYSTEM_ERROR, "system error");
		errorMap.put(Constant.RC_TIMEOUT, "System Error : End Point Failed");
		errorMap.put(Constant.RC_BAD_REQUEST, "Auth Bad Request");
		errorMap.put(Constant.RC_PIN_MUST_6_DIGIT, "PIN must be 6 digits");
		errorMap.put(Constant.RC_INVALID_SIGNATURE, "invalid signature");
		errorMap.put(Constant.RC_INVALID_USERID, "invalid userid");
		errorMap.put(Constant.RC_INVALID_PREFIX_NUMBER, "invalid prefix number");
		errorMap.put(Constant.RC_INVALID_MSISDN_FORMAT, "invalid msisdn format");
		errorMap.put(Constant.RC_INVALID_PARAMETERS, "invalid parameter");
		errorMap.put(Constant.RC_INVALID_REQUEST, "invalid request");
		errorMap.put(Constant.RC_SUCCESS, "Success");
		errorMap.put(Constant.RC_SESSION_INVALID, "Session Invalid");
		errorMap.put(Constant.RC_SESSION_EXPIRED, "Session Expired");
		errorMap.put(Constant.RC_AMOUNT_TO_SMALL, "Amount Too Small");
		errorMap.put(Constant.RC_AMOUNT_TO_BIG, "Amount Too Big");
		errorMap.put(Constant.RC_REVERSAL_FAILED, "Reversal Failed");
		errorMap.put(Constant.RC_COUPON_WRITE_FAILED, "Coupon write failed");
		errorMap.put(Constant.RC_COUPON_IN_PROGRESS, "Coupon In Progress");
		errorMap.put(Constant.RC_COUPON_NO_EXIST, "Coupon Doesn't Exists");
		errorMap.put(Constant.RC_INVALID_COUPON_USER, "Invalid Coupon User");
		errorMap.put(Constant.RC_COUPON_FUNDS_TEMP_UNAVAILABLE, "Coupon Funds Temporary Unavailable");
		errorMap.put(Constant.RC_COUPON_CANCELED, "Coupon Cancelled");
		errorMap.put(Constant.RC_COUPONID_EXIST, "Coupon Id Exists");
		errorMap.put(Constant.RC_INSUFFISIENT_FUNDS, "Insufficient Funds");
		errorMap.put(Constant.RC_TRANSACTION_RECOVERED, "Transaction Recovered");
		errorMap.put(Constant.RC_WALLET_BALANCE_EXCEEDED, "Wallet Balance Exceeded");
		errorMap.put(Constant.RC_WALLET_CAP_EXCEEDED, "Wallet Cap Exceeded");
		errorMap.put(Constant.RC_REQUEST_EXPIRED, "Request Expired");
		errorMap.put(Constant.RC_MFS_SYSTEM_ERROR, "System Error");
		errorMap.put(Constant.RC_DUPLICATE_EXTERNAL_REFERENCE, "Duplicate External Reference");
		errorMap.put(Constant.RC_MFS_BAD_REQUEST, "Bad Request");
		errorMap.put(Constant.RC_AUTH_BAD_TOKEN, "Auth Bad Token");
		errorMap.put(Constant.RC_AUTH_RETRY_EXECEEDED, "Auth Retry Exceed");
		errorMap.put(Constant.RC_AUTH_EXPIRED, "Auth Expired");
		errorMap.put(Constant.RC_AUTH_BAD_PASSWORD, "Auth Bad Password");
		errorMap.put(Constant.RC_AUTH_SAME_PASSWORD, "Auth Same Password");
		errorMap.put(Constant.RC_TRANSACTIN_FAILED, "Transaction Failed");
		errorMap.put(Constant.RC_TRANSACTION_NOT_FOUND, "Requested transaction cannot be found");
		errorMap.put(Constant.RC_AGENT_NOTFOUND, "The Agent attempting to access the system could not be found");
		errorMap.put(Constant.RC_USER_ALREADY_EXIST, "User already exist");
		errorMap.put(Constant.RC_OTP_EXCEEDED, "Request OTP Exceeded");
		errorMap.put(Constant.RC_AGENT_ALREADY_REGISTERED, "User already registered");
		errorMap.put(Constant.RC_AGENT_SUSPENDED, "Agent suspended.");
		errorMap.put(Constant.RC_AGENT_UPGRADE_SUSPENDED, "Cannot update the Agent, restricted status");
		errorMap.put(Constant.RC_RESTRICTED_CHANNEL, "Restricted Channel.");*/
		
	}
	
	private void loadOrangeTvErrorMap(){
		synchronized (orangeTvErrorMap) {
			orangeTvErrorMap.put("999", "Success");
			orangeTvErrorMap.put("333", "Orange ID tidak dikenal");
			orangeTvErrorMap.put("222", "Denominasi tidak dikenal");
			orangeTvErrorMap.put("111", "PIN/hw tidak dikenal");
			orangeTvErrorMap.put("555", "Tipe transaksi tidak dikenal");
			orangeTvErrorMap.put("777", "Duplikat SN atau SN bernilai kosong");
		}
	}
	
	private void loadIndosmartErrorMap(){
		List<IndosmartErrorCode> list = errorCodeDao.getAll(); 
		synchronized (indosmartErrorMap) {
			for(IndosmartErrorCode obj: list){
				indosmartErrorMap.put(obj.getCode(), obj.getDescription());
			}
		}
	}
	
	public void loadCompCardErrorMessage(){
		compCarderrorMap.put("00", "Sukses");
		compCarderrorMap.put("01", "Kartu Dompetku Anda tidak aktif.");
		compCarderrorMap.put("02", "Kartu Dompetku Anda diblokir.");
		compCarderrorMap.put("03", "Anda tidak memiliki kartu.");
		compCarderrorMap.put("04", "Kartu Dompetku Anda telah kadaluarsa.");
		compCarderrorMap.put("05", "Prosess gagal.");
		compCarderrorMap.put("08", "Kartu Dompetku Anda sedang disiapkan.");
		compCarderrorMap.put("09", "Kartu Anda sudah diterima dan statusnya aktif.");
		compCarderrorMap.put("14", "Anda Tidak memiliki kartu.");
		compCarderrorMap.put("80", "Permintaan kartu anda masih dalam proses.");
		compCarderrorMap.put("88", "Anda telah memiliki Kartu Dompetku.");
		
		log.info("loadCompCardErrorMessage:"+compCarderrorMap.size());
	}
	
	public void loadRemittancePartner(){
		List<RemittancePartner> list = remittanceAgentDao.getAllRemittancePartner();
		synchronized (remittancePartnerMap) {
			for(RemittancePartner obj: list)
				remittancePartnerMap.put(obj.getInitiator(), obj);
		}
		log.info("loadRemittancePartner:"+remittancePartnerMap.size());
	}
	
	public RemittancePartner getRemittancePartner(String initiator){
		return remittancePartnerMap.get(initiator);
	}
	
	//TEMP
	public void loadKecamatanList(){
		List<RefKecamatan> list = userWalletDao.getKecamatans();
		synchronized (kecamatanCodeList) {
			for(RefKecamatan obj: list){
				String last = obj.getLokasi_kode().substring(obj.getLokasi_kode().lastIndexOf(".") + 1);
				kecamatanCodeList.add(obj.getLokasi_kode().split(last)[0].replace(".",""));
			}
		}
		log.info("loadKecamatanList:"+kecamatanCodeList.size());
	}
	
	public boolean checkKecamatanCode(String kodeKecamatan){
		return kecamatanCodeList.contains(kodeKecamatan);
	}
	
	public boolean checkTglLahirKTP(String tgllahir){
		String date = tgllahir;
		if(Integer.parseInt(tgllahir.substring(0, 2))>40){
			String monthYear = tgllahir.substring(2,6);
			int tgl = Integer.parseInt(tgllahir.substring(0, 2))-40;
			date = StringUtils.leftPad(String.valueOf(tgl), 2, "0")+monthYear;
		}
		return Utils.isValidDateEKTP(date);
	}

	private void loadReferralType(){
		List<ReferralType> list = referralTypeDao.getAllReferralType();
//		List<String> ips = new ArrayList<String>();
//		List<String> apis = new ArrayList<String>();
		synchronized (referallTypeMap) {
			for(ReferralType obj: list){
//				for(String ip: obj.getIpList().split(",")){
//					ips.add(ip);
//				}
//				for(String apiLimit: obj.getApiList().split(",")){
//					apis.add(apiLimit);
//				}
				referallTypeMap.put(obj.getReferralNo(), obj);
//				apiIPUserMap.put(obj.getUserId(), ips);
//				apiLimitUserMap.put(obj.getUserId(), apis);
			}
		}
		log.info("Referral Type Load: "+list.size());
	}
	
	/**
	 * @param reff_no
	 * @return ReferallType object
	 */
	public ReferralType getReffTypeByReffNo(String reff_no) {
		if(referallTypeMap.size() == 0)
			loadReferralType();;
		return referallTypeMap.get(reff_no);
	}
	
//	public void loadWhitelabelSequence(){
//		List<WhitelabelSequence> list = whitelabelSequenceDao.getWhitelabelSequenceAll();
//		synchronized (whitelabelMap) {
//			for(WhitelabelSequence obj: list)
//				whitelabelMap.put(obj.getMsisdn_part(), obj);
//		}
//		log.info("loadRemittancePartner:"+remittancePartnerMap.size());
//	}
	
//	public List<WhitelabelSequence> getWhitelabelSequences(String partMsisdn){
//		List<WhitelabelSequence> res = new ArrayList<WhitelabelSequence>();
//		Collection<WhitelabelSequence> getSpesific = whitelabelMap.get(partMsisdn);
//		for (WhitelabelSequence obj : getSpesific) {
//			res.add(obj);
//		}
//		return res;
//	}
}
