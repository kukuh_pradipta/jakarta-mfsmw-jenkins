/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Feb 16, 2015 
 * Time       : 3:22:33 PM 
 */
package com.indosat.kloc.mfsmw.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class ImageHandler {

	private static Log log = LogFactory.getLog(ImageHandler.class);
	
	@Value("${app.image.path}")
	private String imagePath;
	
	@Async
	public void writeImage(String username, int type, String picture){
		
		log.debug("Write Image username["+username+"] type");
		String fileName = Utils.buildImageFilename(username, type);
		try {
			FileOutputStream imageOutFile = new FileOutputStream(this.imagePath+"/"+fileName);
			// Converting a Base64 String into Image byte array
	        byte[] imageByteArray = Utils.decodeImage(picture);
	        imageOutFile.write(imageByteArray);
	        imageOutFile.close();
//			PrintWriter out = new PrintWriter(this.imagePath+"/"+fileName+".txt");
//			out.print(picture);
//			out.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public static String getImageType(String base64) {
        String[] header = base64.split("[;]");
        if(header == null || header.length == 0) return null;
        return header[0].split("[/]")[1];
    }
	
	public static String removeBase64Header(String base64) {
        if(base64 == null) return  null;
        return base64.trim().replaceFirst("data[:]image[/]([a-z])+;base64,", "");
    }
	
}
