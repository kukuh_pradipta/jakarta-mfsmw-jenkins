
/*
 * Project Name: Mobile Financial Service Middleware
 * Created By  : Megi Jaka Permana
 * Created Date: May 2, 2014 6:19:45 PM
 */
package com.indosat.kloc.mfsmw.util;

import java.io.UnsupportedEncodingException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.codec.binary.Base64;

public class DESedeEncryption {

	private static final String UNICODE_FORMAT = "UTF8";
	public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
	private KeySpec myKeySpec;
	private SecretKeyFactory mySecretKeyFactory;
	private Cipher cipher;
	byte[] keyAsBytes;
	private String myEncryptionKey;
	private String myEncryptionScheme;
	SecretKey key;

	public DESedeEncryption(String publicKey) {
		myEncryptionKey = publicKey;
		myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;
		try {
			keyAsBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
			myKeySpec = new DESedeKeySpec(keyAsBytes);
			mySecretKeyFactory = SecretKeyFactory.getInstance(myEncryptionScheme);
			cipher = Cipher.getInstance(myEncryptionScheme);
			key = mySecretKeyFactory.generateSecret(myKeySpec);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Method To Encrypt The String
	 */
	public String encrypt(String unencryptedString) {
		String encryptedString = null;
		try {
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
			byte[] encryptedText = cipher.doFinal(plainText);
			encryptedString = new String(Base64.encodeBase64(encryptedText));
			// BASE64Encoder base64encoder = new BASE64Encoder();
			// encryptedString = base64encoder.encode(encryptedText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encryptedString;
	}

	/**
	 * Method To Decrypt An Encrypt String
	 */
	public String decrypt(String encryptedString) {
		String decryptedText = null;
		try {
			cipher.init(Cipher.DECRYPT_MODE, key);
			// BASE64Decoder base64decoder = new BASE64Decoder();
			// byte[] encryptedText =
			// base64decoder.decodeBuffer(encryptedString);
			byte[] encryptedText = Base64.decodeBase64(encryptedString);
			byte[] plainText = cipher.doFinal(encryptedText);
			decryptedText = bytes2String(plainText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return decryptedText;
	}

	/**
	 * Returns String From An Array Of Bytes
	 */
	private static String bytes2String(byte[] bytes) {
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			stringBuffer.append((char) bytes[i]);
		}
		return stringBuffer.toString();
	}

	public static void main(String[] args) throws Exception {
		System.out.println("085721067790".substring("085721067790".length()-4, "085721067790".length()));
		DESedeEncryption encryption = new DESedeEncryption("qr0fFl1n3PerT@MiN4P4yPr0");
		System.out.println(encryption.encrypt("permana"));
//		System.out.println(javaEnc.encrypt("141215123456|654321|081572020080"));
//		System.out.println(phpEnc.encrypt("141215289374|473982indahwisata"));

	}

}
