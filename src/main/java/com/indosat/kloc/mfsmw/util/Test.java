/**
4 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Apr 20, 2016 
 * Time       : 1:46:43 PM 
 */
package com.indosat.kloc.mfsmw.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URL;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.lang.RandomStringUtils;
import org.simmetrics.StringMetric;
import org.simmetrics.metrics.StringMetrics;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.apache.commons.codec.binary.Base64;

import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.model.WhitelabelSequence;

public class Test {

	/**
	 * runs a simple example.
	 *
	 * @param args
	 *            two strings required for comparison
	 * @throws FileNotFoundException
	 *
	 * @see this.usage() in the same source file for more details on the usage
	 *      instructions
	 */

	public static void main(final String[] args) throws FileNotFoundException {
//		String address = "JL.CIKONDANG NO.4 RT.1/12, CIGADUNG, CIBEUNYING KALER, KOTA BANDUNG, JAWA BARAT";
//		//address = acc.getAddress();
//		String addressExt = "";
//			if(address.contains(".")){
//				address = address.replace(".", " ");
//				if(address.contains(",")){
//					address = address.replace(",", " ");
//					if(address.contains("/")){
//						address = address.replace("/", " ");
//						if(address.length()>50){
//							address = address.substring(0, 50);
//							if(address.length()>=100){
//								addressExt = address.substring(50,100);
//								System.out.println("More than 100");
//							}else{
//								addressExt = address.substring(50,address.length());
//								System.out.println("Less than 100");
//							}
//						}
//					}
//				}
//			}
//		System.out.println(address);
//		System.out.println(addressExt);
//		
		String x = "[1100, 192162, 226164, 250162]";
		System.out.println(x.contains("1105"));
		
		
//		String x = "08551436505**1000**123456**1506654789**36505";
//		System.out.println(x.split("\\*\\*")[0]);
//		System.out.println(x.split("\\*\\*")[1]);
//		System.out.println(x.split("\\*\\*")[2]);
//		System.out.println(x.split("\\*\\*")[3]);
//		System.out.println(x.split("\\*\\*")[4]);
		// String x = "Jln.sultan agung,ruko festival,medan
		// satria,bekasi,Tidak,01010001,3309040602840003,3000000,> IDR
		// 3.000.000,.,+6287772581004,12";
		// String test = "08551436505";
		// System.out.println(test.substring(test.length()-5));
		//
		//
		//
		// String x = "2";
		// System.out.println(x.matches("/[1,2,8]/"));
		// String y =
		// "{\"status\":1,\"timestamp\":1489554365,\"tn\":\"5786915\",\"reg_id\":\"isat-000101\",\"sn\":\"66510315199339\",\"message\":\"0899999999.Berhasil
		// SN
		// :999999999999.XYZ25.\",\"harga\":\"25000\",\"saldo\":\"1514399\"}";
		// System.out.println(Utils.getJSONValue(y, "reg_id"));
		// String x = "Jln. Tamansari Gobras Kp. Sumurdago Rt/Rw : 003/014 Kel.
		// Mulyasari Kec. Tamansari Kota
		// Tasikmalaya,Tidak,19941123,3278072311940003,200000,> IDR 3.000.0";
		// if(x.length()>40){
		// String y = x.substring(0, 40);
		// System.out.println(y.substring(0, y.lastIndexOf(" ")));
		// }else{
		// System.out.println(x);
		// }
		// System.out.println("Length:"+x.split(",").length);
		// System.out.println(x.split(",")[x.split(",").length-1]);
		// System.out.println(x.split(",")[x.split(",").length-2]);
		// System.out.println(x.split(",")[x.split(",").length-3]);
		// System.out.println(x.split(",")[x.split(",").length-4]);
		// System.out.println(x.split(",")[x.split(",").length-5]);
		// System.out.println(x.split(",")[x.split(",").length-6]);
		// System.out.println(x.split(",")[x.split(",").length-7]);
		// System.out.println(x.split(",")[x.split(",").length-8]);
		//
		// System.out.println(x.split(","+x.split(",")[x.split(",").length-8])[0]);

		// 12
		// +6287772581004
		// .
		// > IDR 3.000.000
		// 3000000
		// 3309040602840003
		// 01010001
		// Tidak
		// Jln.sultan agung,ruko festival,medan satria,bekasi

		// long responseTime = 0l;
		// long StartTime =new Date().getTime();
		//
		// int x = 50;
		// PrintWriter out = new PrintWriter( "result.txt" );
		// for (int i = 0; i < x; i++) {
		// HttpClientSender sender = new HttpClientSender();
		// String url = "https://mapi.dompetku.com/webapi/balance_check";
		// MultiValueMap<String, String> parameters = new
		// LinkedMultiValueMap<String, String>();
		// parameters.add("userid", "dompetku_developer");
		// parameters.add("to", "085721067790");
		// // Prepare acceptable media type
		// List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		// acceptableMediaTypes.add(MediaType.APPLICATION_XML);
		// ResponseEntity<String> response = sender.sendPost(url, parameters,
		// acceptableMediaTypes);
		//
		// responseTime = new Date().getTime()-StartTime;
		//
		// //out.println( text );
		// out.println("Sent:"+url);
		// out.println("Sent Result:"+response.getBody());
		// out.println("Sent Status:"+response.getStatusCode());
		// out.println("TIME Execute:"+responseTime);
		// out.println("=============================================================================");
		//
		// }
		//
		//
		//// DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		////
		//// long milliSeconds= Long.parseLong("1488614595811");
		//// System.out.println(milliSeconds);
		////
		//// Calendar calendar = Calendar.getInstance();
		//// calendar.setTimeInMillis(milliSeconds);
		//// System.out.println(formatter.format(calendar.getTime()));
		////
		//// System.out.println("18000.0".split("\\.")[0]);
		// String x = "Kota Kasablanka Jl.Casablanca
		// No.22,Ya,10051990,3273180407900001,5000000,> IDR 3,000,000,Hompi
		// Pimpah Alaihim,021564537382,12";
		// //x.split(",", 2);
		//// System.out.println(x.split(",", 2)[3]);
		// System.out.println(RandomStringUtils.randomAlphanumeric(24));
		//// if(x.contains("+")){
		// x = "0"+x.substring(3);
		// System.out.println(x);
		// }
	}

	
}
