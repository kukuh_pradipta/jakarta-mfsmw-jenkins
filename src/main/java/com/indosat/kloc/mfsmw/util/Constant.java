/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jun 13, 2014 
 * Time       : 3:44:47 PM 
 */
package com.indosat.kloc.mfsmw.util;

public class Constant {
	
	public static final int USER_STATUS_INITIAL = 0;
	public static final int USER_STATUS_ACTIVE = 1;
	public static final int USER_STATUS_REQUEST_TO_KYC = 2;
	public static final int USER_STATUS_KYC = 3;
	
	public static final int WALLET_TYPE_REG = 1;
	public static final int WALLET_TYPE_KYC_REQUEST = 2;
	public static final int WALLET_TYPE_PREMIUM = 3;
	
	public static final int OPERATION_TYPE_INSERT = 1;
	public static final int OPERATION_TYPE_UPDATE = 2;
	
	public static final int UTIBA_ACCOUNT_STATUS_REGISTERED = 3;
	public static final int UTIBA_ACCOUNT_STATUS_SUSPEND = 8;
	
	//extra trans_data type
	public static final int EXT_TRANS_DATA_TYPE_QUERY_BILLPAY = 0;
	public static final int EXT_TRANS_DATA_TYPE_BILLPAY = 1;
	
	public static final int UTIBA_BILLPAY_QUERY_SUCCESS_STATE = 2;
	
	//ArtaJasa Xgate
	public static final String ENC_KEY = "XGat3_Domp3tku_+";
	
	//DMT Request Type
	public static final int DMT_REQUEST_TYPE_CREATE_COUPON_INQUIRY = 0;
	public static final int DMT_REQUEST_TYPE_CREATE_COUPON_COMMIT = 1;
	public static final int DMT_REQUEST_TYPE_INQUIRY_COUPON_REMIT = 2;
	public static final int DMT_REQUEST_TYPE_COUPON_REMIT = 3;
	public static final int DMT_REQUEST_TYPE_CREATE_COUPON = 4;
	public static final int DMT_REQUEST_TYPE_CANCEL_COUPON_REMIT = 5;
	public static final int DMT_REQUEST_TYPE_WALLET2CASH = 6;
	
	//service type
	public static final int SERVICE_DO_PAYMENT = 1;
	public static final int SERVICE_DO_PAYMENT_CHECK = 2;
	public static final int SERVICE_DO_TRANSFER_TOKEN = 3;
	public static final int SERVICE_GET_TRX_STATUS = 4;
	public static final int SERVICE_CASHIN = 5;
	public static final int SERVICE_CASHIN_SUPPRESS_SMS = 6;
	public static final int SERVICE_REVERSAL = 7;
	public static final int SERVICE_CASHOUT = 8;
	public static final int SERVICE_CREATE_COUPON = 9;
	public static final int SERVICE_GET_BALANCE = 10;
	public static final int SERVICE_GET_USER_INFO = 11;
	public static final int SERVICE_BILLPAY = 12;
	public static final int SERVICE_QUERY_BILLPAY = 13;
	public static final int SERVICE_AIRTIME_INQUIRY = 14;
	public static final int SERVICE_MERCHANT_TRANSFER = 15;
	public static final int SERVICE_MONEY_TRANSFER = 16;
	public static final int SERVICE_COMMIT_AIRTIME = 17;
	public static final int SERVICE_CONFIRM= 18;
	public static final int SERVICE_MATRIX_INQUIRY = 19;
	public static final int SERVICE_LOGIN = 20;
	public static final int SERVICE_REGISTER = 21;
	public static final int SERVICE_GET_COUPON_DETAILS = 22;
	public static final int SERVICE_INBOUND_REMITTANCE = 23;
	
	//PARAMETER NAME
	public static final String ID_NUMBER_PARAM = "idno";
	//public static final String ID_NUMBER_PARAM = "id_num";
	public static final String ID_TYPE_PARAM = "idtype";
	//public static final String ID_TYPE_PARAM = "id_type";
	public static final String ADDRESS_PARAM = "address";
	public static final String NAME_PARAM = "name";
	public static final String MOTHER_NAME_PARAM = "mothername";
	//public static final String MOTHER_NAME_PARAM = "mother";
	public static final String GENDER_PARAM = "gender";
	public static final String DOB_PARAM = "dob";
	//public static final String DOB_PARAM = "birthdate";
	public static final String FIRSTNAME_PARAM = "firstname";
	public static final String LASTNAME_PARAM = "lastname";
	public static final String MSISDN_PARAM = "msisdn";
	public static final String EMAIL_PARAM = "email";
	public static final String EXT_REF_PARAM = "extRef";
	public static final String COMP_NAME_PARAM = "company_name";
	public static final String CONTACT_NO_PARAM = "contact_no";
	public static final String SIGNATURE_PARAM= "signature";
	public static final String PUBLIC_KEY_PARAM= "publicKey";
	public static final String AMOUNT_PARAM = "amount";
	public static final String USERID_PARAM = "userid";
	public static final String TRANSID_PARAM = "transid";
	public static final String COUPONID_PARAM = "couponid";
	public static final String PASSWORD_PARAM = "password";
	public static final String OUTLETID_PARAM = "outletid";
	public static final String TRANSACTIONID_PARAM = "transactionId";
	public static final String INQUIRY_PARAM = "inquiry";
	public static final String GAME_VOUCHER_PARAM = "gamevoucher";
	public static final String INITIATOR_PARAM = "initiator";
	public static final String DATETIME_PARAM = "DateTime";
	public static final String TARGET_PARAM = "target";
	public static final String REFERENCE_PARAM = "reference";
	public static final String LOWERLIMIT_PARAM = "lowerlimit";
	public static final String FUNCTION_PARAM = "function";
	public static final String PAYMENT_PARAM = "payment";
	public static final String PIN_PARAM = "pin";
	public static final String RECIPIENT_PARAM = "recipient";
	public static final String SENDER_PARAM = "sender";
	public static final String SENDER_ID_PARAM = "senderId";
	public static final String DENOM_PARAM = "denom";
	public static final String TO_PARAM = "to";
	public static final String PRICE_PARAM = "price";
	public static final String OPERATOR_NAME_PARAM="operatorName";
	public static final String COUNT_PARAM = "count";
	public static final String EXTRA_PARAM = "extraParam";
	public static final String BANK_CODE_PARAM = "bankCode";
	public static final String BANK_NAME_PARAM = "bankName";
	public static final String BANK_ACC_NO = "bankAccNo";
	public static final String PROFILEPIC_PARAM = "profilepic";
	public static final String IDPHOTO_PARAM = "idphoto";
	public static final String IDPHOTOUSER_PARAM = "idphotouser";
	public static final String KYC_REQUEST_PARAM = "kyc_request";
	public static final String PRODUCT_ID_PARAM = "productid";
	public static final String OTP_PARAM = "otp";
	public static final String SUPPRESS_SMS_PARAM = "suppress_sms";
	public static final String TYPE_PARAM = "type";
	public static final String KEYWORD_PARAM = "keyword";
	public static final String ACCID_PARAM = "accid";
	public static final String ISSNAME_PARAM = "issname";
	public static final String DESTNAME_PARAM = "destname";
	public static final String TOKEN_PARAM = "token";
	public static final String POSTCODE_PARAM = "postcode";
	public static final String VOUCHER_CODE_PARAM = "vcode";
	public static final String BILLER_PARAM = "biller";
	public static final String DATE_PARAM = "date";
	public static final String VIRTUALWALLET_PARAM = "vwallet";
	public static final String GROUPID_PARAM = "groupid";
	public static final String SLEEP_PARAM = "sleep";
	public static final String IDPELANGGAN_PARAM = "idpelanggan";
	public static final String TRXID_PARAM = "trxid";
	public static final String COUNTRYCODE_PARAM = "countrycode";
	public static final String CITY_PARAM = "city";
	public static final String PROVINCE_PARAM = "province";
	public static final String POB_PARAM = "pob";
	//Add By Ryan for new Merchant
	public static final String CHANNEL_PARAM = "channel";
	public static final String IP_PARAM = "ip";
	public static final String API_LIST_PARAM = "apilist";
	//QRCode
	public static final String QR_STRING_PARAM = "qr";
	public static final String QR_TRX_EXT_REF_PARAM = "trx_ext_ref";
	public static final String QR_TIMESTAMP_PARAM = "timestamp";
	public static final String QR_DIGEST_PARAM = "transactionDigest";
	public static final String TERMID_PARAM = "terminal_id";
	public static final String OPERID_PARAM = "operator_id";
	public static final String MERCHANT_NUMBER_PARAM = "merchant_number";
	public static final String REFF_NO_PARAM = "reff_no";
	public static final String REFF_CODE_PARAM = "reff_code";
	public static final String DEVICE_ID = "deviceid";
	
	//companion card
	public static final String COMP_CARD_GET_STATUS = "MSISDNStatus";
	public static final String COMP_CARD_DELIVERY_REQUEST = "DeliveryReq";
	public static final String COMP_CARD_DELIVERY_STATUS = "DeliveryStatus";
	public static final String COMP_CARD_ACTIVATION = "ActivateCard";
	public static final String COMP_CARD_DEACTIVATION = "DeactivateCard";
	
	public static final String COMP_CARD_SERVICE_NAME = "serviceName";
	
	//remittance
	public static final String UNIQUE_CODE_PARAM = "uniqueCode";
	public static final String NOTE_PARAM = "note";
	public static final String PARENT_ID_PARAM = "parentId";
	public static final String STORE_NAME_PARAM = "storeName";
	public static final String RECIPIENT_PHONE_NUMBER_PARAM = "recipientPhoneNumber";
	public static final String RECIPIENT_NAME_PARAM = "recipientName";
	public static final String RECIPIENT_SOURCE_OF_FUND_PARAM = "recipientSourceOfFund";
	public static final String RECIPIENT_PURPOSE_PARAM = "recipientPurpose";
	public static final String RECIPIENT_ID_TYPE_PARAM = "recipientIdType";
	public static final String RECIPIENT_ID_NO_PARAM = "recipientIdNo";
	public static final String RECIPIENT_ID_EXP_PARAM = "recipientIdExp";
	public static final String RECIPIENT_GENDER_PARAM = "recipientGender";
	public static final String RECIPIENT_ADDRESS_PARAM = "recipientAddress";
	public static final String RECIPIENT_PROVINCE_PARAM = "recipientProvince";
	public static final String RECIPIENT_CITY_PARAM = "recipientCity";
	public static final String RECIPIENT_PLACE_OF_BIRTH = "recipientPlaceOfBirth";
	public static final String RECIPIENT_DATE_OF_BIRTH = "recipientDateOfBirth";
	public static final String RECIPIENT_OCCUPATION = "recipientOccupation";
	public static final String RECIPIENT_NATIONALITY = "recipientNationality";
	public static final String RECIPIENT_AGGREE_TO_REGISTER_DOMPETKU = "recipientAggreeToReg";
	
	public static final String SENDER_NAME_PARAM = "senderName";
	public static final String SENDER_PHONE_NUMBER_PARAM = "senderPhoneNumber";
	public static final String SENDER_ID_TYPE_PARAM = "senderIdType";
	public static final String SENDER_ID_NO_PARAM = "senderIdNo";
	public static final String SENDER_ID_EXP_PARAM = "senderIdExp";
	public static final String SENDER_GENDER_PARAM = "senderGender";
	public static final String SENDER_ADDRESS_PARAM = "senderAddress";
	public static final String SENDER_PROVINCE_PARAM = "senderProvince";
	public static final String SENDER_CITY_PARAM = "senderCity";
	public static final String SENDER_PLACE_OF_BIRTH = "senderPlaceOfBirth";
	public static final String SENDER_DATE_OF_BIRTH = "senderDateOfBirth";
	public static final String SENDER_OCCUPATION = "senderOccupation";
	public static final String SENDER_NATIONALITY = "senderNationality";
	public static final String SENDER_AGGREE_TO_REGISTER_DOMPETKU  = "senderAggreeToReg";
	
	public static final String STORE_NAME_PARAM_LITE = "strName";
	public static final String RECIPIENT_PHONE_NUMBER_PARAM_LITE = "rHP";
	public static final String RECIPIENT_NAME_PARAM_LITE = "rName";
	public static final String RECIPIENT_SOURCE_OF_FUND_PARAM_LITE = "rSF";
	public static final String RECIPIENT_PURPOSE_PARAM_LITE = "rP";
	public static final String RECIPIENT_ID_TYPE_PARAM_LITE = "rIdType";
	public static final String RECIPIENT_ID_NO_PARAM_LITE = "rIdNo";
	public static final String RECIPIENT_ID_EXP_PARAM_LITE = "rIdExp";
	public static final String RECIPIENT_GENDER_PARAM_LITE = "rGender";
	public static final String RECIPIENT_ADDRESS_PARAM_LITE = "rAddr";
	public static final String RECIPIENT_PROVINCE_PARAM_LITE = "rProv";
	public static final String RECIPIENT_CITY_PARAM_LITE = "rCity";
	public static final String RECIPIENT_PLACE_OF_BIRTH_LITE = "rpob";
	public static final String RECIPIENT_DATE_OF_BIRTH_LITE = "rdob";
	public static final String RECIPIENT_OCCUPATION_LITE = "rOcc";
	public static final String RECIPIENT_NATIONALITY_LITE = "rNat";
	public static final String RECIPIENT_AGGREE_TO_REGISTER_DOMPETKU_LITE = "rATReg";
	
	public static final String SENDER_NAME_PARAM_LITE = "sName";
	public static final String SENDER_PHONE_NUMBER_PARAM_LITE = "sHP";
	public static final String SENDER_ID_TYPE_PARAM_LITE = "sIdType";
	public static final String SENDER_ID_NO_PARAM_LITE = "sIdNo";
	public static final String SENDER_ID_EXP_PARAM_LITE = "sIdExp";
	public static final String SENDER_GENDER_PARAM_LITE = "sGender";
	public static final String SENDER_ADDRESS_PARAM_LITE = "sAddr";
	public static final String SENDER_PROVINCE_PARAM_LITE = "sProv";
	public static final String SENDER_CITY_PARAM_LITE = "sCity";
	public static final String SENDER_PLACE_OF_BIRTH_LITE = "spob";
	public static final String SENDER_DATE_OF_BIRTH_LITE = "sdob";
	public static final String SENDER_OCCUPATION_LITE = "sOcc";
	public static final String SENDER_NATIONALITY_LITE = "sNat";
	
	
	
	
	
	
	//PATH VARIABLE
	public static final String SERVICE_PATH_LOGIN = "login";
	public static final String SERVICE_PATH_REGISTER = "register";
	public static final String SERVICE_PATH_UPDATE_PROFILE = "update_profile";
	public static final String SERVICE_PATH_KYC_UPGRADE = "upgrade_kyc";
	public static final String SERVICE_PATH_MOBILE_AGENT_KYC_UPGRADE = "new_upgrade_kyc";
	public static final String SERVICE_PATH_HISTORY_TRANSACTION = "history_transaction";
	public static final String SERVICE_PATH_GET_BALANCE = "balance";
	public static final String SERVICE_PATH_GET_AGENT_BY_REF = "user_inquiry";
	public static final String SERVICE_PATH_COUPON_TRANSFER = "coupon_transfer";
	public static final String SERVICE_PATH_DO_PAYMENT = "do_payment";
	public static final String SERVICE_PATH_DO_PAYMENT_PARAM = "do_payment_param";
	public static final String SERVICE_PATAH_DO_PAYMENT_CHECK = "do_payment_check";
	public static final String SERVICE_PATH_GET_TRANSACTION_BY_EXT_REF = "transaction_check";
	public static final String SERVICE_PATH_GET_USER_INFO_BY_IDENTITY  = "get_user_info_by_identity";
	public static final String SERVICE_PATH_RESEND_SMS_COUPON_REMIT = "resend_coupon_remit";
	public static final String SERVICE_PATH_RESEND_EMAIL_ACTIVATION = "resend_email_activation";
	
	public static final String SERVICE_PATH_CREATE_COUPON = "create_coupon";
	public static final String SERVICE_PATH_CHECK_BALANCE = "balance_check";
	public static final String SERVICE_PATH_MERCHANT_BUY = "merchant_transfer";
	public static final String SERVICE_PATH_MONEY_TRANSFER = "money_transfer";
	public static final String SERVICE_PATH_AIRTIME_INQUIRY = "airtime_inquiry";
	public static final String SERVICE_PATH_AIRTIME_COMMIT = "airtime_commit";
	public static final String SERVICE_PATH_MATRIX_INQUIRY = "matrix_inquiry";
	public static final String SERVICE_PATH_CONFIRM = "confirm";
	public static final String SERVICE_PATH_CONFIRM_NOID = "confirm_noid";
	public static final String SERVICE_PATH_QUERY_BILLPAY = "query_billpay";
	public static final String SERVICE_PATH_BILLPAY= "billpay";
	public static final String SERVICE_PATH_CHANGE_PIN = "change_pin";
	public static final String SERVICE_PATH_BANK_TRF = "bank_transfer";
	public static final String SERVICE_PATH_P2P_TRF = "p2p_transfer";
	public static final String SERVICE_PATH_MBILLER_INQUIRY ="mbiller_inquiry";
	public static final String SERVICE_PATH_MBILLER_PAY = "mbiller_pay";
	public static final String SERVICE_PAATH_CREATE_COUPON_REMITANCE = "create_coupon_remit";
	public static final String SERVICE_PATH_GET_COUPON_REMITANCE = "get_coupon_remit";
	public static final String SERVICE_PATH_CANCEL_COUPON_REMITANCE = "cancel_coupon_remit";
	public static final String SERVICE_PATH_COUPON_REMITANCE = "coupon_remit";
	
	public static final String SERVICE_PATH_INQUIRY_CREATE_COUPON_REMIT = "inquriy_coupon_remit";
	public static final String SERVICE_PATH_COMMIT_COUPON_REMIT = "commit_coupon_remit";
	
	public static final String SERVICE_PATH_WALLET_TO_CASH = "wallet2cash";
	public static final String SERVICE_PATH_WALLET_TO_CASH_INQUIRY = "wallet2cash_inquiry";
	
	public static final String SERVICE_PATH_JATIS_SMS_PUSH = "jatis_mobile";
	public static final String SERVICE_PATH_ISAT_CDMS_PUSH = "isat";
	
	public static final String SERVICE_PATH_OTP_LIFE_REQUEST = "get_otp_life";
	public static final String SERVICE_PATH_OTP_REQUEST = "get_otp";
	public static final String SERVICE_FORGET_PASS = "forget_pass";
	public static final String SERVICE_RESET_PIN = "reset_pin";
	
	public static final String SERVICE_PATH_KYC_REQUEST = "verify_kyc";
	
	public static final String SERVICE_PATH_MERCHANT_CASHIN = "merchant_cashin";
	
	public static final String SERVICE_PATH_INQUIRY_KYC = "inquiry_kyc";
	public static final String SERVICE_PATH_DO_KYC_BY_AGENT = "do_kyc_by_agent";
	
	public static final String SERVICE_PATH_DO_NFC_SETTLEMENT = "do_nfc_settlement";
	
	public static final String SERVICE_PATH_REVERSAL = "do_reversal";
	
	public static final String SERVICE_PATH_INQUIRY_RESET_PIN = "inquiry_reset_pin";
	public static final String SERVICE_PATH_COMMIT_RESET_PIN = "commit_reset_pin";
	
	public static final String SERVICE_PATH_TRANS_EXT_REF = "trans_ext_ref";
	
	public static final String SERVICE_PATH_SELL_POINT = "sell_p";
	
	public static final String SERVICE_PATH_SELL_NO_CONFIRM = "sell_no_confirm";
	
	public static final String SERVICE_PATH_SIMPLE_REG = "s_register";
	
	public static final String SERVICE_PATH_COMPANION_CARD = "comp_card";
	
	public static final String SERVICE_PATH_SMS_KEYWORD = "sms_keyword";
	//Add by ryanbhuled
	public static final String SERVICE_PATH_AJ_INQ= "aj_inq";
	public static final String SERVICE_PATH_AJ_TRF= "aj_trf";
	public static final String SERVICE_PATH_ACA_INSURANCE= "aca_insurance";
	public static final String SERVICE_PATH_EKTP= "ektp";
	public static final String SERVICE_PATH_EKTP_CONFIRM= "ektp_confirm";
	public static final String SERVICE_PATH_EKTP_KYC= "kyc_ektp";
	public static final String SERVICE_PATH_EKTP_KYC_PHOTO= "kyc_ektp_photo";
	public static final String SERVICE_PATH_EKTP_KYC_STRICT= "kyc_ektp_strict";
	public static final String SERVICE_PATH_EKTP_KYC_BY_AGENT= "kyc_ektp_by_agent";
	public static final String SERVICE_PATH_EKTP_KYC_BY_AGENT_REFERENCE= "kyc_ektp_by_agent_reff";
	public static final String SERVICE_PATH_EKTP_KYC_ONLY_NAME= "kyc_ektp_nameonly";
	public static final String SERVICE_PATH_HOLD= "transaction_reserve";
	public static final String SERVICE_PATH_HOLD_CONFIRM= "transaction_confirm";
	public static final String SERVICE_PATH_HOLD_RELEASE= "transaction_release";
	public static final String SERVICE_PATH_ONEBILL_REG= "onebill_reg";
	public static final String SERVICE_PATH_ONEBILL_UPDATE= "onebill_update";
	public static final String SERVICE_PATH_ONEBILL_GETLIST= "onebill_getlist";
	public static final String SERVICE_PATH_ONEBILL_UNREG= "onebill_unreg";
	public static final String SERVICE_PATH_NUSANTARA_REG= "dnus_reg";
	public static final String SERVICE_PATH_NUSANTARA_RESET_PIN= "dnus_reset_pin";
	public static final String SERVICE_PATH_NUSANTARA_REFF_MAPPING= "dnus_reff_map";
	public static final String SERVICE_PATH_DMT_MOBILE_PICKUP= "dmt_mobiepickup";
	public static final String SERVICE_PATH_MODIFIER= "update_modifier";
	public static final String SERVICE_PATH_PROG_INFO= "prog_informer";
	public static final String SERVICE_PATH_DPLUS_REGISTER_GENERATEPIN= "reg_generate_pin";
	public static final String SERVICE_PATH_REMOVE_AGENT= "remove_agent";
	public static final String SERVICE_PATH_MAP_UNMAP= "map_unmap";
	public static final String SERVICE_PATH_UPDATEADMIN= "update_admin";
	public static final String SERVICE_PATH_DIRECTHITUTIBA= "utiba_hit";
	public static final String SERVICE_PATH_CHECK_POINT_DOMPETKU= "check_point";
	public static final String SERVICE_PATH_CHECK_GROUPING_USER= "check_grouping";
	public static final String SERVICE_PATH_REG_FIREBASE= "reg_firebase";
	public static final String SERVICE_PATH_HIT_BILLERGW= "billergw";
	public static final String SERVICE_PATH_GET_TRX_BY_TRXID= "get_trx_detail";
	public static final String SERVICE_PATH_FORGOTPIN= "forgotpin";
	public static final String SERVICE_PATH_KYC_NOKTP= "kycnoktp";
	public static final String SERVICE_PATH_GET_WHITELABEL_SEQUENCE= "get_whitelabel_sequence";
	public static final String SERVICE_PATH_SET_WHITELABEL_SEQUENCE= "set_whitelabel_sequence";
	public static final String SERVICE_PATH_CHECKTRX_EXTREF= "check_trx_extref";
	public static final String SERVICE_PATH_GETDETAILTRX= "get_detail_trx";
	public static final String SERVICE_PATH_REVERSAL_EXTREF= "rev_extref";
	public static final String SERVICE_PATH_CHECK_CHILD= "check_child";
	//Internal
	public static final String SERVICE_PATH_ADD_MERCHANT_USERID= "add_merchant";
	
	//Microsript
	public static final String SERVICE_PATH_QR_CHECK= "qrcheck";
	public static final String SERVICE_PATH_QR_INQ= "qrinq";
	public static final String SERVICE_PATH_QR_PAY= "qrpay";
	public static final String SERVICE_PATH_QR_OFFLINE_REQ= "offline_qr_payment";
	public static final String SERVICE_PATH_VOUCHER_INQ= "voucherinq";
	public static final String SERVICE_PATH_VOUCHER_PAY= "voucherpay";
	
	//BlueBird Lock Balance
	public static final String SERVICE_PATH_LOCK_BALANCE= "lock_balance";
	
	//REPlACE TAG
	public static final String RC_CODE_REPLACE_TAG = "[%RC_CODE%]";
	public static final String REFERENCE_REPLACE_TAG = "[%REFERENCE%]";
	public static final String AMOUNT_REPLACE_TAG = "[%AMOUNT%]";
	public static final String TRANSACTIONID_REPLACE_TAG = "[%TRANSACTIONID%]";
	public static final String PRICE_REPLACE_TAG = "[%PRICE%]";
	public static final String GAME_VOUCHER_REPLACE_TAG = "[%GAME_VOUCHER%]";
	public static final String MSISDN_REPLACE_TAG = "[%MSISDN%]";
	public static final String ERROR_MESSAGE_REPLACE_TAG = "[%ERROR_MESSAGE%]";
	public static final String MESSAGE_REPLACE_TAG = "[%MESSAGE%]";
	public static final String TRX_ID_REPLACE_TAG = "[%TRX_ID%]";
	public static final String DENOM_REPLACE_TAG = "[%DENOM%]";
	public static final String FEE_REPLACE_TAG = "[%FEE%]";
	public static final String VOUCHER_REPLACE_TAG = "[%VOUCHER%]";
	public static final String EXT_REF_ID = "[%EXTREF%]";
	public static final String DATE_REPLACE_TAG = "[%DATE%]";
	public static final String NAME_REPLACE_TAG = "[%NAME%]";
	public static final String CARDNO_REPLACE_TAG = "[%CARDNO%]";
	public static final String OPERATOR_REPLACE_TAG = "[%OPERATOR%]";
	public static final String PIN_REPLACE_TAG = "[%PIN%]";
	public static final String BANK_CODE_REPLACE_TAG = "[%BANK_CODE%]";
	public static final String ACC_NO_REPLACE_TAG = "[%ACC_NO%]";
	public static final String DATETIME_REPLACE_TAG = "[%DATETIME%]";
	public static final String SMS_REPLACE_TAG = "[%SMS%]";
	public static final String COUPON_REPLACE_TAG = "[%COUPON%]";
	public static final String TRANSACTION_TYPE_REPLACE_TAG_INA = "[%TRANSACTION_TYPE_INA%]";
	public static final String TRANSACTION_TYPE_REPLACE_TAG_EN = "[%TRANSACTION_TYPE_EN%]";
	public static final String TO_REPLACE_TAG = "[%TO%]";
	public static final String TO_NAME_REPLACE_TAG = "[%TO_NAME%]";
	public static final String REFERENCEID_REPLACE_TAG ="[%REF%]";
	public static final String STATUS_REPLACE_TAG = "[%STATUS%]";
	public static final String INITIATOR_REPLACE_TAG = "[%INITIATOR%]";
	public static final String ACCOUNTID_REPLACE_TAG = "[%ACCOUNT_ID%]";
	public static final String EMAIL_REPLACE_TAG = "[%EMAIL%]";
	public static final String DOB_REPLACE_TAG = "[%DOB%]";
	public static final String TYPE_REPLACE_TAG = "[%TYPE%]";
	public static final String BALANCE_REPLACE_TAG = "[%BALANCE%]";
	public static final String BALANCE_VIRTUAL_REPLACE_TAG = "[%BALANCE_VIRTUAL%]";
	public static final String DETAILS_REPLACE_TAG = "[%DETAILS%]";
	public static final String ADDRESS_REPLACE_TAG = "[%ADDRESS%]";
	public static final String POSCODE_REPLACE_TAG = "[%POSCODE%]";
	public static final String FROM_REPLACE_TAG = "[%FROM%]";
	public static final String FROM_NAME_REPLACE_TAG = "[%FROM_NAME%]";
	//Added By Ryanbhuled
	public static final String UNIX_TIME_REPLACE_TAG = "[%UNIXTIME%]";
	public static final String DEST_NAME_REPLACE_TAG = "[%DEST_NAME%]";
	public static final String ISS_NAME_REPLACE_TAG = "[%ISS_NAME%]";
	public static final String PRODUCT_CODE_REPLACE_TAG = "[%PRODUCT_CODE%]";
	public static final String OUTLET_CODE_REPLACE_TAG = "[%OUTLET_CODE%]";
	public static final String XML_METHOD_REPLACE_TAG = "[%XML_METHOD%]";
	public static final String BANK_NAME_REPLACE_TAG = "[%BANK_NAME%]";
	public static final String RRN_REPLACE_TAG = "[%RRN%]";
	
	//BILLER FUNCTION
	public static final String BILLER_GAMES_INQUIRY_FUNCTION = "isGameVoucherValid";
	public static final String BILLER_GAMES_PAYMENT_FUNCTION = "isGameVoucherValid";
	public static final String BILLER_KCJ_INQUIRY_FUNCTION = "isKCJValid";
	public static final String BILLER_KCJ_PAYMENT_FUNCTION = "creditKCJ";
	public static final String BILLER_ORANGE_TV_INQUIRY_FUNCTION = "buyOrangeInq";
	public static final String BILLER_ORANGE_TV_TOPUP_FUNCTION = "buyOrangePay";
	public static final String BILLER_NEXT_MEDIA_INQURY_FUNCTION = "openNexMediaInq";
	public static final String BILLER_NEXT_MEDIA_PAYMENT_FUNCTION = "openNexMediaPay";
	public static final String BILLER_AIRTIME_INQUIRY = "isAirtimeAccountValid";
	public static final String BILLER_AIRTIME_TOPUP = "creditAirtime";
	public static final String MBILLER_AIRTIME_INQUIRY = "airtimeInquiry";
	public static final String MBILLER_AIRTIME_TOPUP = "topupAirtime";
	
	//RESPONSE CODE
	
	public static final int RC_SYSTEM_ERROR = -99;
	public static final int RC_TIMEOUT = -98;
	public static final int RC_LOGIN_FAILED = -97;
	public static final int RC_DB_ERROR = -96;
	public static final int RC_CONNECTION_ERROR = -95;
	public static final int RC_PARS_XML_RESP_ERROR = -94;
	public static final int RC_KYC_NOT_VALID = -19;
	public static final int RC_PRODUCT_NOT_FOUND = -18;
	public static final int RC_TARGET_NOTFOUND = -17;
	public static final int RC_RESTRICTED_CHANNEL = -16;
	public static final int RC_AGENT_UPGRADE_SUSPENDED = -15;
	public static final int RC_AGENT_SUSPENDED = -14;
	public static final int RC_OTP_EXCEEDED = -13;
	public static final int RC_USER_ALREADY_EXIST = -12;
	public static final int RC_AGENT_NOTFOUND = -11;
	public static final int RC_DENOM_NOTFOUND = -10;
	public static final int RC_BILLER_NOTFOUND = -9;
	public static final int RC_BAD_REQUEST = -8;
	public static final int RC_PIN_MUST_6_DIGIT = -7;
	public static final int RC_INVALID_SIGNATURE = -6;
	public static final int RC_INVALID_USERID = -5;
	public static final int RC_INVALID_PREFIX_NUMBER = -4;
	public static final int RC_INVALID_MSISDN_FORMAT = -3;
	public static final int RC_INVALID_PARAMETERS = -2;
	public static final int RC_INVALID_REQUEST = -1;
	public static final int RC_SUCCESS = 0;
	public static final int RC_SESSION_INVALID=2;
	public static final int RC_SESSION_EXPIRED = 3;
	public static final int RC_AGENT_ALREADY_REGISTERED =18;
	public static final int RC_AMOUNT_TO_SMALL = 23;
	public static final int RC_AMOUNT_TO_BIG = 24;
	public static final int RC_REVERSAL_FAILED = 75;
	public static final int RC_BILLER_GENERAL_ERROR = 2242;
	public static final int RC_TRANSACTION_NOT_FOUND = 400;
	public static final int RC_COUPON_WRITE_FAILED = 540;
	public static final int RC_COUPON_IN_PROGRESS = 541;
	public static final int RC_COUPON_NO_EXIST = 542;
	public static final int RC_INVALID_COUPON_USER = 543;
	public static final int RC_COUPON_FUNDS_TEMP_UNAVAILABLE = 544;
	public static final int RC_COUPON_CANCELED = 545;
	public static final int RC_COUPONID_EXIST = 546;
	public static final int RC_INSUFFISIENT_FUNDS = 1001;
	public static final int RC_TRANSACTION_RECOVERED = 1002;
	public static final int RC_WALLET_BALANCE_EXCEEDED = 1003;
	public static final int RC_WALLET_CAP_EXCEEDED = 1004;
	public static final int RC_REQUEST_EXPIRED = 1005;
	public static final int RC_MFS_SYSTEM_ERROR = 1006;
	public static final int RC_DUPLICATE_EXTERNAL_REFERENCE = 1007;
	public static final int RC_MFS_BAD_REQUEST = 1008;
	public static final int RC_ALREADY_ON_GROUP = 1009;
	public static final int RC_UNKNOWN_ERROR = 1010;
	public static final int RC_AUTH_BAD_TOKEN = 1011;
	public static final int RC_AUTH_RETRY_EXECEEDED = 1012;
	public static final int RC_AUTH_EXPIRED = 1013;
	public static final int RC_AUTH_BAD_PASSWORD = 1014;
	public static final int RC_AUTH_SAME_PASSWORD = 1015;
	public static final int RC_TRANSACTIN_FAILED = 1099;
	//EKTP
	public static final int RC_EKTP_SUCCESS = 4400;
	public static final int RC_EKTP_NOT_EXISTS = 4401;
	public static final int RC_EKTP_NOT_VALID = 4402;
	public static final int RC_EKTP_USED = 4403;
	public static final int RC_EKTP_UNKNOWN_ERROR = 4404;
	public static final int RC_CLOSING_DOMPETKU = -9999;
	public static final int RC_JOIN_PARENT_PARENT_NOTVALID = 4405;
	
	public static final String SUCCESS_TEXT = "SUCCESS";
	
	//api_user
	public static final int API_USER_STATUS_INACTIVE = 0;
	public static final int API_USER_STATUS_ACTIVE = 1;
	
	public static final int API_USER_STATUS_RESPONSE_TYPE_XML = 1;
	public static final int API_USER_STATUS_RESPONSE_TYPE_JSON = 2;
	
	public static final int CHANNEL_TYPE_MOBILE = 1;
	public static final int CHANNEL_TYPE_WEB = 2;
	public static final int CHANNEL_TYPE_UMB = 3;
	public static final int CHANNEL_TYPE_PLATFORM = 4;
	
	public static final int GROUPID_DOMPETKU = 1;
	public static final int GROUPID_MYNT = 2;
	
	public static final int SERVICE_TYPE_WEB_API = 1;
	public static final int SERVICE_TYPE_SOAP = 2;
	
	public static final String CHANNEL_STR = "channel";
	
	public static final String TRANSACTION_TEXT_TYPE_SEND_MONEY_INA = "Transfer Uang";
	public static final String TRANSACTION_TEXT_TYPE_SEND_MONEY_EN = "Money Transfer";
	public static final String TRANSACTION_TEXT_TYPE_RECEIVE_MONEY = "Menerima Uang";
	public static final String TRANSACTION_TEXT_TYPE_ECOMMERCE = "E-Commerce";
	public static final String TRANSACTION_TEXT_TYPE_BILL_PAYMENT = "Bill Payment";
	public static final String TRANSACTION_RESET_PASSWORD = "Reset Password";
	
	public static final int PRODUCT_STATUS_ACTIVE = 1;
	
	public static final int TRANSACTION_TYPE_INQUIRY = 1;
	public static final int TRANSACTION_TYPE_PAY = 2;
	
	public static final int TYPE_PHOTO_ID_ONLY = 1;
	public static final int TYPE_PHOTO_ID_AND_PHOTO = 2;
	
	
	//SQL QUERY
	public static final String API_USER_QUERY_GET_ALL = "SELECT * FROM api_users WHERE(1=1) ";
	public static final String API_MERCHANT_USER_QUERY_GET_ALL = "SELECT * FROM api_merchant_users WHERE(1=1) ";
	public static final String TRANSACTION_INSERT = "INSERT INTO transaction(userId, channelType, serviceType, "
			+ "initiator, extRef, ref, trxDateTime, responseCode) "
			+ "VALUES(?,?,?,?,?,?,?,?)";
	public static final String BILLER_TRANSACTION_INSERT = "INSERT INTO "
			+ "biller_transaction(billerName, function, transid, extRef, amount, dateTime, msisdn, initiator, responseCode) "
			+ "VALUES(?,?,?,?,?,?,?,?,?)";
	public static final String BILLER_TRANSACTION_GET_ALL = "SELECT * FROM biller_transaction WHERE transid=?";
	
	//==================== m_biller ==============
	public static final String MBILLER_GET_ALL = "SELECT * FROM m_biller WHERE(1=1)";
	//============================================
	
	public static final String BILLER_GET_ALL = "SELECT * FROM biller WHERE(1=1) ";
	public static final String EXT_TRANS_DATA_GET_ALL = "SELECT * FROM extra_trans_data WHERE(1=1) ";
	
	public static final String INDOSMART_ERROR_CODE_GET_ALL = "SELECT * FROM indosmart_error_code";
	
	public static final String PREFIX_GET_ALL = "SELECT * FROM prefix";
	
	public static final String DENOM_GET_ALL = "SELECT * FROM denom WHERE(1=1) ";
	
	public static final String EMPLOYEE_GET_ALL = "SELECT * FROM employee_isat";
	
	public static final String USER_SAVE = "INSERT INTO user(username, status, createdDate) VALUES(?,?,?)" ;
	public static final String USER_GET =  "SELECT * FROM user WHERE username=?";
	public static final String USER_GET_USERNAME =  "SELECT username FROM user WHERE id=?";
	
	public static final String OTP_COUNTER_GET_ALL = "SELECT * FROM otp_counter ";
	public static final String OTP_COUNTER_GET = "SELECT counter FROM otp_counter WHERE username=? ";
	public static final String OTP_COUNTER_SAVE = "INSERT INTO otp_counter(username) VALUES(?) ";
	public static final String OTP_COUNTER_UPDATE = "UPDATE otp_counter SET counter =? WHERE username=? ";
	public static final String OTP_COUNTER_DELETE = "DELETE FROM otp_counter ";
	
	public static final String OTP_PAYPRO_GET_ALL = "SELECT * FROM otp_user_paypro";
	public static final String OTP_PAYPRO_UPDATE = "UPDATE otp_user_paypro SET otp_sessid=? , otp_date_start=?, otp_date_end=?, otp_request_id=?, otp_sms_result=? WHERE otp_msisdn=? ";
	public static final String OTP_PAYPRO_UPDATE_FLAG = "UPDATE otp_user_paypro SET otp_date_end=?, otp_flag=1 WHERE otp_sessid=? ";
	public static final String OTP_PAYPRO_DELETE = "DELETE FROM otp_user_paypro WHERE otp_request_id =? AND otp_sessid=? AND otp_flag=0 ";
	
	public static final String RESET_PASS_TOKEN_SAVE = "INSERT INTO reset_pass_token(initiator, token, createdDateTime, updatedDateTime) "
			+ " VALUES(?,?,?,?)";
	public static final String REET_PASS_GET_TOKEN = "SELECT token from reset_pass_token WHERE initiator=? AND status = 0 ";
	
	public static final String SAVE_PHOTO = "INSERT INTO photo(username, profile, identity) VALUES(?, ?, ?) ";
	
	//TARGET
	public static final String TARGET_GET_ALL = "SELECT * FROM target WHERE(1=1) ";
	
	//============ product ===============
	public static final String PRODUCT_GET_ALL = "SELECT * FROM product WHERE(1=1) ";
	//============= whitelabel sequence ================
	public static final String WHITELABEL_SEQUENCE_GET_ALL = "SELECT partner_code,msisdn,msisdn_part FROM whitelabel_sequence";
	public static final String WHITELABEL_SEQUENCE_SPESIFIC = "SELECT partner_code,msisdn,msisdn_part FROM whitelabel_sequence WHERE msisdn_part=? AND partner_code=? ORDER BY id";
	public static final String WHITELABEL_SEQUENCE_SPESIFIC_MSISDN = "SELECT partner_code,msisdn,msisdn_part FROM whitelabel_sequence WHERE msisdn=? AND partner_code=? ORDER BY id";
	
	//============= userWallet =================
	public static final String USER_WALLET_GET =  "SELECT * FROM user_wallet WHERE msisdn=?";
	public static final String GET_KECAMATANS =  "SELECT lokasi_kode FROM ref_kecamatan";
	public static final String REFERRAL_GET_RECEIVER="SELECT * FROM referral WHERE receiver=?";
	public static final String REFERRAL_GET_DATA_BY_DEVICEID_AND_REFERRAL_CODE="SELECT * FROM referral WHERE referralCode=? and deviceId =?";
	public static final String REFERRAL_GET_DATA_BY_DEVICEID_ONLY="SELECT * FROM referral WHERE deviceId =?";
	
	//===============ReferralType=============	
	public static final String REFERALL_TYPE_GET_ALL = "SELECT * FROM referral_type WHERE(1=1) ";
}
