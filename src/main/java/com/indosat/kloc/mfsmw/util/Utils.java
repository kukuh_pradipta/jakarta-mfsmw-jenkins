/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jun 16, 2014 
 * Time       : 10:20:15 AM 
 */
package com.indosat.kloc.mfsmw.util;

import java.io.IOException;
import java.io.StringBufferInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Utils {
	private static Log log = LogFactory.getLog(Utils.class);
	
	public static long CURRENT_TIME = System.nanoTime();
	
	public static String getTrxIdFromSMS(String sms){
		String transid="";
		transid = StringUtils.substringBetween(sms, "Ref ", ".");
		System.out.println(transid.length());
		return transid;
	}
	
	public static HashMap<String,String> stringToHashMap(String value) {
		
		value = StringUtils.substringBetween(value, "{", "}");
		String[] keyValuePairs = value.split(",");              //split the string to creat key-value pairs
		HashMap<String,String> map = new HashMap<>();               

		for(String pair : keyValuePairs)                        //iterate over the pairs
		{
		    String[] entry = pair.split(":");                   //split the pairs to get key and value 
		    map.put(entry[0].trim().replace("\"", ""), entry[1].replace("\"", ""));          //add them to the hashmap and trim whitespaces
		}
		
		return map;
	}

	public static String generate(String sessionId, String username, String password){
		String pin = username.toLowerCase() +password;
		try {
			pin = AeSimpleSha1.SHA1(pin).toLowerCase();
			pin = sessionId + pin;
			pin = AeSimpleSha1.SHA1(pin).toUpperCase();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return pin;
	}
	
	public static boolean checkContains(String words,String key){
		Boolean res = false;
		try{
			res = words.toLowerCase().contains(key.toLowerCase());
		}catch(NullPointerException ex){
			res = false;
		}
		return res;
	}
	
	public static void main(String[] args) throws IOException {
//		System.out.println(checkContains("TELKOM SPEEDY", "speEdy"));
		System.out.println(Utils.convertDateFromMilis("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", "1490150579117"));
		String x = "{\"merchant_code\":\"outlet_1\",\"dlk_code\":\"MDKL_1488777670_44713109\",\"timestamp\":\"1488777670\",\"cashier\":\"Sidnei Budiman\",\"cashier_id\":\"58970527b1d7a871442c41d6\",\"amount\":\"26000\",\"phone_id\":\"352919063212281\"}";
		System.out.println(Utils.getJSONValue(x, "cashier_id"));
	}

	public static String getTransactionId() {
		String trxId = String.valueOf(System.nanoTime());
		return StringUtils.mid(trxId, 0, 16);
	}

	public static Object convertXmlToObject(String xml, Class<?> type) {
		Object object = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(type);
			Unmarshaller u = jc.createUnmarshaller();
			object = (Object) u.unmarshal(new StringReader(xml));
		} catch (JAXBException e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return object;
	}

	public static String buildImageFilename(String username, int type) {
		StringBuilder fileNameBuilder = new StringBuilder();
		fileNameBuilder.append(username).append("|").append(type);
		String fileName = "";
		try {
			fileName = AeSimpleSha1.SHA1(fileNameBuilder.toString());
			fileName = AeSimpleSha2.SHA2(fileName);
			fileName = fileName + ".png";
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fileName;
	}

	public static byte[] decodeImage(String imageDataString) {
		return Base64.decodeBase64(imageDataString);
	}

	public static boolean isValidDate(String inDate) {

		if (inDate == null)
			return false;

		// set the format to use as a constructor argument
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");

		if (inDate.trim().length() != dateFormat.toPattern().length())
			return false;

		dateFormat.setLenient(false);

		try {
			// parse the inDate parameter
			dateFormat.parse(inDate.trim());
		} catch (ParseException pe) {
			return false;
		}
		return true;
	}

	public static boolean isValidEmailAddress(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}
	
	public static boolean isNumber(String num) {
		boolean result = true;
		try {
			Double.parseDouble(num);
		} catch (NumberFormatException ex) {
			result = false;
		}
		return result;
	}
	
	public static boolean isValidEmailAddress2(String email) {
		boolean result = true;
		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
		try{
			Matcher m = p.matcher(email);
			boolean matchFound = m.matches();
			if (!matchFound) {
				result=false;	    
			}
		}catch(Exception ex){
			result = false;
		}
		return result;
	}
	

	public static String getCurrentTime() {
		return String.valueOf(new Date().getTime());
	}

	public static String convertParamVal(String paramVal) {
		String parameters = "";
		if (!Utils.isNullorEmptyString(paramVal))
			parameters = paramVal.replace("|", "=").replace(",", "&");

//		System.out.println("parameters: " + parameters);
		return parameters;
	}

	/**
	 * Format String to currency ( #,###,###.00)
	 * <p/>
	 * sample:10000 = 10,000.00
	 */
	public static String convertStringtoCurrency(String value) {
		Double result = 0.0;
		if (value != null) {
			NumberFormat nf = new DecimalFormat("#,###,###");
			result = Double.parseDouble(value);
			value = nf.format(result);

			/*
			 * DecimalFormat kursIndonesia = (DecimalFormat)
			 * DecimalFormat.getCurrencyInstance(); DecimalFormatSymbols
			 * formatRp = new DecimalFormatSymbols();
			 * formatRp.setCurrencySymbol("IDR ");
			 * formatRp.setMonetaryDecimalSeparator(',');
			 * formatRp.setGroupingSeparator('.');
			 * kursIndonesia.setDecimalFormatSymbols(formatRp); value =
			 * kursIndonesia.format(Double.valueOf(value));
			 */

		}
		return value;
	}

	public static String generateNumber(int lenght) {

		Random random = new Random();
		char[] digits = new char[lenght];
		digits[0] = (char) (random.nextInt(9) + '1');
		for (int i = 1; i < lenght; i++) {
			digits[i] = (char) (random.nextInt(10) + '0');
		}
		return new String(digits);
	}

	public static String getXmlTagValue(String xml, String el) {
		// <sid>lintaselas_5000_wp</sid>
		if (!xml.contains(el))
			return null;
		int pos1 = xml.indexOf(el) + el.length();
		int pos2 = xml.indexOf("</", pos1);
		return xml.substring(pos1, pos2);
	}

	public static String getCurrentDate(String format) {
		DateFormat df = new SimpleDateFormat(format);
		String formDate = "";
		Date newDate = new Date();
		formDate = df.format(newDate);
		return formDate;
	}

	public static String getCurrentDate(String format, int minute) {
		Calendar cal = Calendar.getInstance(); // creates calendar
		cal.setTime(new Date()); // sets calendar time/date
		cal.add(Calendar.MINUTE, minute); // adds minutes
		cal.getTime(); // returns new date object, one hour in the future
						// 2015-06-20 15:36:21
		SimpleDateFormat fmt = new SimpleDateFormat(format);
		String formatted = fmt.format(cal.getTime());
		return formatted;
	}

	public static String formatDate(Date date, String format) {
		DateFormat df = new SimpleDateFormat(format);
		String formDate = "";
		formDate = df.format(date);
		return formDate;
	}
	
	public static String formatDate(String orignialFormat, String resultFormat, String datetime) {
		String formattedTime="";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(orignialFormat);
			SimpleDateFormat output = new SimpleDateFormat(resultFormat);
			Date d = sdf.parse(datetime);
			formattedTime = output.format(d);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
//			System.out.println("error to parsing date: "+datetime);
			e.printStackTrace();
		}
		return formattedTime;
	}

	public static Date getExpiredTokenTime(int hour) {
		Calendar cal = Calendar.getInstance(); // creates calendar
		cal.setTime(new Date()); // sets calendar time/date
		cal.add(Calendar.HOUR_OF_DAY, hour); // adds one hour
		return cal.getTime(); // returns new date object, one hour in the future
	}

	public static boolean validateMSISDN(String msisdn) {
		if (msisdn.startsWith("0") && msisdn.length() > 8)
			return true;
		return false;
	}

	public static boolean isNullorEmptyString(String value) {

		return value == null || value.length() == 0 || value == "";
	}

	public static HashMap<String, String> populate(String paramVal) {
		HashMap<String, String> hm = null;
		try{
			hm = new HashMap<String, String>();
			String text[] = paramVal.split("\\,");
			for (String obj : text) {
				String[] parameters = obj.split("\\|");
				hm.put(parameters[0], parameters[1]);
			}
		}catch(ArrayIndexOutOfBoundsException ex){
			ex.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return hm;
	}

	public static String convertToXml(Object source, Class<?> type) {
		String result;
		StringWriter sw = new StringWriter();
		try {
			JAXBContext carContext = JAXBContext.newInstance(type);
			Marshaller carMarshaller = carContext.createMarshaller();
			carMarshaller.marshal(source, sw);
			result = sw.toString();
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	public static String convertToJSON(Object source) {
		Gson gson = new Gson();
		return gson.toJson(source);
	}

	public static JsonObject getJsonData(String jsonString) {
		JsonObject res = new Gson().fromJson(jsonString, JsonObject.class);
		return res;
	}

	public static String buildResponse(int responseType, Object source, Class<?> type) {
		String result;
		if (responseType == Constant.API_USER_STATUS_RESPONSE_TYPE_JSON) {
			result = convertToJSON(source);
		} else {
			result = convertToXml(source, source.getClass());
		}
		return result;
	}

	public static String unpackSignature(String signature, String passKey) {

		try {
			DESedeEncryption encryption = new DESedeEncryption(passKey);
			return encryption.decrypt(signature);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public static String validateSignature(String signature, String passKey) {
		String unpackSign = unpackSignature(signature, passKey);
		if (unpackSign != null) {
			if (unpackSign.split("\\|").length == 2) {
				return unpackSign;
			}
		}
		return "";

	}

	public static String getJSONValue(String stringJson, String key) {
		JsonParser parser = new JsonParser();
		JsonElement jsonElement = parser.parse(stringJson);
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		return jsonObject.get(key).getAsString();
	}
	
	public static String getJSONobject(String stringJson, String key) {
		JsonParser parser = new JsonParser();
		JsonElement jsonElement = parser.parse(stringJson);
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		try {
			return jsonObject.get(key).toString();
		}catch (Exception e) {
			return null;
		}
	}
	
	public static String convertDateFromMilis(String format,String milis){
    	DateFormat formatter = new SimpleDateFormat(format);

    	long milliSeconds= Long.parseLong(milis);
    	System.out.println(milliSeconds);

    	Calendar calendar = Calendar.getInstance();
    	calendar.setTimeInMillis(milliSeconds);
    	return formatter.format(calendar.getTime()); 
	}
	
	
	public static String convertDateFromOtherFormat(String formatOld,String formatNew, String date){
		long epoch;
		try {
			epoch = new java.text.SimpleDateFormat(formatOld).parse(date).getTime() / 1000;
			String dateNew = new java.text.SimpleDateFormat(formatNew).format(new java.util.Date (epoch*1000));
			return dateNew;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static HashMap<String, String> iterateJSONtoMap(JsonObject object) {
		HashMap<String, String> res = new HashMap<>();
		for (Entry<String, JsonElement> entry : object.entrySet()) {
			JsonElement je = entry.getValue();
			if(je.isJsonObject())
			{
				for (Entry<String, JsonElement> entry2 : je.getAsJsonObject().entrySet()) {
					JsonElement je2 = entry2.getValue();
					res.put(entry2.getKey(), je2.getAsString());
				}
			}else{
				res.put(entry.getKey(), je.getAsString());
			}
		}
		return res;
	}
	
	public static HashMap<String, String> iterateXMLtoMap(String xml) {
		HashMap<String, String> res = new HashMap<>();
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
			Document document = docBuilder.parse(new StringBufferInputStream(xml));
			NodeList nodeList = document.getElementsByTagName("*");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					res.put(node.getNodeName(), node.getTextContent());
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.toString());
			e.printStackTrace();
			res = null;
		}
		return res;
	}

	/**
	 * 
	 * @param signature
	 * @param passKey
	 * @return array of string, index [0] = initiator, index[1] = pin
	 */
	public static String[] getUsernameAndPassFromSignature(String signature, String passKey) {
		signature = signature.replace(" ", "+");
		signature = unpackSignature(signature, passKey);
		if (!isNullorEmptyString(signature)) {
			String[] splitSign = signature.split("\\|");
			if (splitSign.length == 3) {
				return new String[] { splitSign[2], StringUtils.reverse(splitSign[1]) };
			}
			/*
			 * if(splitSign.length == 2 && splitSign[1].length() > 10){ return
			 * new String[]{splitSign[1].substring(6),
			 * StringUtils.reverse(splitSign[1].substring(0, 6))}; }
			 */
		}
		return null;
	}
	
	public static boolean isValidDateEKTP(String inDate) {

		if (inDate == null)
			return false;

		// set the format to use as a constructor argument
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyy");

		if (inDate.trim().length() != dateFormat.toPattern().length())
			return false;

		dateFormat.setLenient(false);

		try {
			// parse the inDate parameter
			dateFormat.parse(inDate.trim());
		} catch (ParseException pe) {
			return false;
		}
		return true;
	}
	
	public static Boolean isTimeStampValid(String formatDate,String inputString)
	{ 
		log.debug("Check timestamp Valid ["+inputString+"]");
	    SimpleDateFormat format = new java.text.SimpleDateFormat(formatDate);
	    try{
	       format.parse(inputString);
	       return true;
	    }
	    catch(ParseException e)
	    {
	        return false;
	    }
	}
	
	public static long deltaTime(long timeStartEpoch, long timeEndEpoch){
		long difference = timeEndEpoch- timeStartEpoch;
		return difference;
	}
}
