/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 18, 2014 
 * Time       : 4:36:31 PM 
 */
package com.indosat.kloc.mfsmw.util;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.indosat.kloc.mfsmw.model.BillerTransaction;
import com.indosat.kloc.mfsmw.model.MBillerTransaction;
import com.indosat.kloc.mfsmw.model.Transaction;
import com.indosat.kloc.mfsmw.model.UserWallet;
import com.indosat.kloc.mfsmw.pojo.DMTTransaction;

@Component
public class TrxHistList {

	private static Log log = LogFactory.getLog(TrxHistList.class);
	
	private List<Transaction> list = new LinkedList<Transaction>();
	private List<BillerTransaction> bList = new LinkedList<BillerTransaction>();
	private List<MBillerTransaction> mbtList = new LinkedList<MBillerTransaction>();
	private List<DMTTransaction> dmtList = new LinkedList<DMTTransaction>();
	private List<UserWallet> userList = new LinkedList<UserWallet>();
	
	public void add(MBillerTransaction trx){
		synchronized (mbtList) {
			mbtList.add(trx);
		}
	}

	private void add(Transaction trx) {
		synchronized (list) {
			list.add(trx);
		}
	}
	
	private void addBT(BillerTransaction trx){
		synchronized (bList) {
			bList.add(trx);
		}
	}
	
	private void addDMT(DMTTransaction trx){
		synchronized (dmtList) {
			dmtList.add(trx);
		}
	}
	
	private void addUserWallet(UserWallet user){
		synchronized (userList) {
			userList.add(user);
		}
	}
	
	
	public void writeBillTrxHist(String billerName, String function, String transId, 
			String extRef, String amount, String msisdn, String initiator, String responseCode ){
		//billerName, function, transid, extRef, amount, dateTime, responseCode
		try {
			BillerTransaction trx = new BillerTransaction();
			trx.setBillerName(billerName);
			trx.setFunction(function);
			trx.setTransId(transId);
			trx.setExtRef(extRef);
			trx.setAmount(amount);
			if(Utils.isNullorEmptyString(responseCode))
				trx.setResponseCode("-99");
			else
				trx.setResponseCode(responseCode);
			
			trx.setMsisdn(msisdn);
			trx.setInitiator(initiator);
			this.addBT(trx);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
		
	}

	public void writeHist(int userId, int channelType, int serviceType,
			String initiator, String extRef, String ref, int respCode) {
		Transaction trx = new Transaction();
		trx.setUserId(userId);
		trx.setChannelType(channelType);
		trx.setServiceType(serviceType);
		trx.setInitiator(initiator);
		trx.setExtRef(extRef);
		trx.setRef(ref == null ? 0 : Integer.valueOf(ref));
		trx.setResponseCode(respCode);
		this.add(trx);
	}
	
	public void writeDMTHist(DMTTransaction transaction){
		this.addDMT(transaction);
	}
	
	public void writeUserWalletList(UserWallet user){
		this.addUserWallet(user);
	}
	
	public List<DMTTransaction> popAllDMT(){
		synchronized (dmtList) {
			List<DMTTransaction> outList = new LinkedList<DMTTransaction>(dmtList);
			dmtList.clear();
			return outList;
		}
	}
	
	public List<MBillerTransaction> popAllMBT(){
		synchronized (mbtList) {
			List<MBillerTransaction> outList = new LinkedList<MBillerTransaction>(mbtList);
			mbtList.clear();
			return outList;
		}
	}

	public List<Transaction> popAll() {
		synchronized (list) {
			LinkedList<Transaction> outlist = new LinkedList<Transaction>(list);
			list.clear();
			return outlist;
		}
	}
	
	public List<BillerTransaction> popAllBT() {
		synchronized (bList) {
			LinkedList<BillerTransaction> outlist = new LinkedList<BillerTransaction>(bList);
			bList.clear();
			return outlist;
		}
	}

	public List<UserWallet> popAllUserWallet() {
		synchronized (userList) {
			LinkedList<UserWallet> outlist = new LinkedList<UserWallet>(userList);
			userList.clear();
			return outlist;
		}
	}

}
