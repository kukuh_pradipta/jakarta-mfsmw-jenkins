/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Feb 10, 2015 
 * Time       : 11:33:25 AM 
 */
package com.indosat.kloc.mfsmw.sender;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.model.MBillerTransaction;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.pojo.TransactionMail;
import com.indosat.kloc.mfsmw.soap.UTIBAHandler;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;

@Component
public class SmsClientSender {

	private static Log log = LogFactory.getLog(SmsClientSender.class);
	
	@Autowired
	private HttpClientSender httpClientSender;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private UTIBAHandler utibaHandler;
	@Autowired
	private MailSenderHandler mailHandler;
	
	@Value("${app.utiba.url.smsgw}")
	private String smsGwURL;
	@Value("${app.dplus.url.smsgw}")
	private String smsGwDPLusURL;
	@Value("${app.sms.mynt.trx.notification}")
	private String myntTrxPattern;
	@Value("${app.utiba.url.smsgw.charged}")
	private String smsGwURLCharged;
	@Value("${app.mmd.url.smsgw}")
	private String smsGWMMD;
	
	//@Async
	public void send(int templateId, String type, String currentDate, String transId, MBillerTransaction trx, ApiUser apiUser){
		try {
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Error Sending SMS notification transId[transId]");
		}
		String sms = "";
		String initiator = trx.getInitiator();
		sms = dataStore.getSMSNotificationTemplate(templateId);
		if(!Utils.isNullorEmptyString(type))
			sms = sms.replace(Constant.TYPE_REPLACE_TAG, type);
		if(!Utils.isNullorEmptyString(trx.getDestinationName()))
			sms = sms.replace(Constant.NAME_REPLACE_TAG, trx.getDestinationName());
		
		sms = sms.replace(Constant.DETAILS_REPLACE_TAG, trx.getDetails());
		sms = sms.replace(Constant.TO_REPLACE_TAG, trx.getDestination());
		if(!Utils.isNullorEmptyString(trx.getPrice()) && !Utils.isNullorEmptyString(trx.getFee()))
			sms = sms.replace(Constant.AMOUNT_REPLACE_TAG, Utils.convertStringtoCurrency(String.valueOf(Integer.valueOf(trx.getPrice()) - Integer.valueOf(trx.getFee()))));
		sms = sms.replace(Constant.FEE_REPLACE_TAG, Utils.convertStringtoCurrency(trx.getFee()));
		sms = sms.replace(Constant.PRICE_REPLACE_TAG, Utils.convertStringtoCurrency(trx.getPrice()));
		sms = sms.replace(Constant.DATE_REPLACE_TAG, currentDate);
		sms = sms.replace(Constant.BALANCE_REPLACE_TAG, Utils.convertStringtoCurrency(utibaHandler.getAgentByReferenceRequest(trx.getInitiator(), apiUser).getBalance()));
		sms = sms.replace(Constant.REFERENCEID_REPLACE_TAG, transId);
		
		if(initiator.startsWith("0"))
			initiator = "62"+initiator.substring(1);
		
		String url = this.smsGwURL;
		url = url.replace(Constant.SMS_REPLACE_TAG, sms);
		url = url.replace(Constant.TRANSACTIONID_REPLACE_TAG, String.valueOf(new Date().getTime()));
		url = url.replace(Constant.MSISDN_REPLACE_TAG, initiator);
		httpClientSender.sendGet(url);
		
	}
	
	
	@Async
	public void send(String sms, String msisdn){
		if(msisdn.startsWith("0"))
			msisdn = "62"+msisdn.substring(1);
		
		String url = this.smsGwURL;
		url = url.replace(Constant.SMS_REPLACE_TAG, sms);
		url = url.replace(Constant.TRANSACTIONID_REPLACE_TAG, String.valueOf(new Date().getTime()));
		url = url.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
		httpClientSender.sendGet(url);
	}
	
	@Async
	public void sendAutoChoose(String sms, String msisdn){

		if(msisdn.startsWith("0"))
			msisdn = "62"+msisdn.substring(1);
		if(dataStore.isIsatPrefix(msisdn)){
			String url = this.smsGwURL;
			url = url.replace(Constant.SMS_REPLACE_TAG, sms);
			url = url.replace(Constant.TRANSACTIONID_REPLACE_TAG, String.valueOf(new Date().getTime()));
			url = url.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
			httpClientSender.sendGet(url);
		}else{
			if(msisdn.startsWith("62"))
				msisdn = "0"+msisdn.substring(2);
			mailHandler.sendFirebaseNotification(msisdn, Utils.getCurrentDate("dd-MM-yyyy HH:mm:ss"), sms, "NOTIFICATION");
		}
	}
	
	@Async
	public void sendByPutu(String sms, String msisdn){
		if(msisdn.startsWith("0"))
		msisdn = "62"+msisdn.substring(1);
	
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		parameters.add(Constant.FUNCTION_PARAM, "sms789");
		parameters.add(Constant.TRANSACTIONID_PARAM, String.valueOf(new Date().getTime()));
		parameters.add("message", sms);
		parameters.add(Constant.MSISDN_PARAM, msisdn);
		
		ResponseEntity<String> re = httpClientSender.sendPost("https://api.dompetku.com:7443/", parameters, acceptableMediaTypes);
	}
	
	public void sendForDPlus(String sms, String msisdn){
		if(msisdn.startsWith("0"))
			msisdn = "62"+msisdn.substring(1);
		
		String url = this.smsGwDPLusURL;
		if(dataStore.isIsatPrefix(msisdn)){
			url = this.smsGwURL;
		}
		url = url.replace(Constant.SMS_REPLACE_TAG, sms);
		url = url.replace(Constant.TRANSACTIONID_REPLACE_TAG, String.valueOf(new Date().getTime()));
		url = url.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
		httpClientSender.sendGet(url);
	}

	@Async
	public void sendDPlusNotificaton(String initiator, TransactionMail trx, ApiUser apiUser){
		String text = myntTrxPattern;
		text = text.replace(Constant.STATUS_REPLACE_TAG, dataStore.getErrorMsg(trx.getStatus()));
		text = text.replace(Constant.TRANSACTION_TYPE_REPLACE_TAG_INA, trx.getTransactionTypeINA());
		if(trx.getAmount() == null)
			text = text.replace("Rp [%AMOUNT%]",  "");
		else
			text = text.replace(Constant.AMOUNT_REPLACE_TAG,  Utils.convertStringtoCurrency(trx.getAmount()));
		
		text = text.replace(Constant.TO_REPLACE_TAG, trx.getDestination());
		text = text.replace(Constant.DATE_REPLACE_TAG, Utils.getCurrentDate("dd/MM/yyyy HH:mm:ss"));
		text = text.replace(Constant.REFERENCEID_REPLACE_TAG, trx.getTransId());
		
		Response response = utibaHandler.getAgentByReferenceRequest(initiator, apiUser);
		if(response.getStatus() == Constant.RC_SUCCESS){
			if(!Utils.isNullorEmptyString(response.getAgentData().getSmsAddress()))
				send(text, response.getAgentData().getSmsAddress());
		}
		
	}
	
	@Async
	public void sendPointDompetku(String point, String transid, String msisdn){
		String smsTxt = "Poin Anda pada "+Utils.getCurrentDate("dd/MM/yy HH:mm")+" sebesar "+point+". Ref "+transid;
		if(msisdn.startsWith("0"))
			msisdn = "62"+msisdn.substring(1);
		String url = this.smsGwURLCharged;
		url = url.replace(Constant.SMS_REPLACE_TAG, smsTxt);
		url = url.replace(Constant.TRANSACTIONID_REPLACE_TAG, String.valueOf(new Date().getTime()));
		url = url.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
		httpClientSender.sendGet(url);
	}
	
	public String sendUsingMMD(String sms, String msisdn){
		if(msisdn.startsWith("+62")){
			msisdn = "0"+msisdn.substring(3);
		}else if(msisdn.startsWith("62")){
			msisdn = "0"+msisdn.substring(2);
		}else if(msisdn.startsWith("+")){
			msisdn = msisdn.substring(1);
		}
		String url = this.smsGWMMD;
		url = url.replace(Constant.SMS_REPLACE_TAG, sms);
		url = url.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
		ResponseEntity<String> res = httpClientSender.sendGet(url);
		return res.getBody();
	}
	
}
