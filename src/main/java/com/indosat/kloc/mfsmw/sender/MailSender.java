/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Dec 2, 2014 
 * Time       : 4:27:37 PM 
 */
package com.indosat.kloc.mfsmw.sender;

import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

public class MailSender implements Runnable{

private static Log log = LogFactory.getLog(MailSender.class);
	
	private JavaMailSender mailSender;
	private SimpleMailMessage mailMessage;
	private String destination;
	private String textMessage;
	private String subject;
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("================Sending Email Notification===============");
		log.info("destination: "+destination+"\nsubject: "+subject+"\ntextMessage: "+textMessage);
		try {
			  MimeMessage message = mailSender.createMimeMessage();
			  MimeMessageHelper helper = new MimeMessageHelper(message, true);
	          helper.setFrom(mailMessage.getFrom());
	          helper.setTo(destination.toString().split(","));
	          helper.setSubject(subject);
	          helper.setText(textMessage, true);
	          mailSender.send(message);
		} catch (javax.mail.MessagingException me) {
			// TODO: handle exception
			log.error("********************* Exception while sending: "+me);
		}
	}
	
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void setMailMessage(SimpleMailMessage mailMessage) {
		this.mailMessage = mailMessage;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setTextMessage(String textMessage) {
		this.textMessage = textMessage;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
}
