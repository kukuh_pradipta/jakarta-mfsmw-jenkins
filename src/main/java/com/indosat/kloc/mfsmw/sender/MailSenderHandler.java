/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Dec 7, 2014 
 * Time       : 6:50:43 PM 
 */
package com.indosat.kloc.mfsmw.sender;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.pojo.TransactionMail;
import com.indosat.kloc.mfsmw.soap.UTIBAHandler;
import com.indosat.kloc.mfsmw.util.EmailNotificationBuilder;
import com.indosat.kloc.mfsmw.util.Utils;

@Component
public class MailSenderHandler {

	private static Log log = LogFactory.getLog(MailSenderHandler.class);
	
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private SimpleMailMessage mailMessage;
	@Autowired
	private TaskExecutor sendExecutor;
	@Autowired
	private EmailNotificationBuilder emailNotificationBuilder;
	@Autowired
	private UTIBAHandler utibaHandler;
	@Autowired
	private HttpClientSender httpClientSender;
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${app.notif.firebase.url}")
	private String firebaseURL;
	
	public void sendEmailNotification(String destination,String subject, String emailText){
		log.debug("Sending E-mail confirmation: destination["+destination+"] subject["+subject+"]\n"+emailText);
		
		MailSender sender = new MailSender();
		sender.setMailMessage(mailMessage);
		sender.setMailSender(mailSender);
		sender.setDestination(destination);
		sender.setSubject(subject);
		sender.setTextMessage(emailText);
		sendExecutor.execute(sender);
		
	}
	
	public void sendEmailNotification(String destination,String subject, TransactionMail transactionMail, ApiUser apiUser){
		log.debug(transactionMail.toString());
		String destName = utibaHandler.getAgentByReferenceRequest(transactionMail.getDestination(), apiUser).getName();
		String emailText = emailNotificationBuilder.transaction(
				transactionMail.getTransactionTypeINA(), transactionMail.getTransactionTypeEN(), destName==null?transactionMail.getDestination():destName, 
				transactionMail.getAmount(), transactionMail.getTransId(), transactionMail.getStatus());
		
		log.debug("Sending E-mail confirmation: destination["+destination+"] subject["+subject+"]\n"+emailText);
		
		MailSender sender = new MailSender();
		sender.setMailMessage(mailMessage);
		sender.setMailSender(mailSender);
		sender.setDestination(destination);
		sender.setSubject(subject);
		sender.setTextMessage(emailText);
		sendExecutor.execute(sender);
		
	}
	
	public void sendEmailNotificationToReceiver(String destination,String subject, TransactionMail transactionMail, ApiUser apiUser){
		String destName = utibaHandler.getAgentByReferenceRequest(transactionMail.getDestination(), apiUser).getName();
		String emailText = emailNotificationBuilder.transactionReceiver(
				transactionMail.getTransactionTypeINA(), transactionMail.getTransactionTypeEN(), destName==null?transactionMail.getDestination():destName, 
				transactionMail.getAmount(), transactionMail.getTransId(), transactionMail.getStatus());
		
		log.debug("Sending E-mail confirmation: destination["+destination+"] subject["+subject+"]\n"+emailText);
		
		MailSender sender = new MailSender();
		sender.setMailMessage(mailMessage);
		sender.setMailSender(mailSender);
		sender.setDestination(destination);
		sender.setSubject(subject);
		sender.setTextMessage(emailText);
		sendExecutor.execute(sender);
		
	}
	
	public void sendFirebaseNotification(String type,String msisdn,String trxId,String msg,String msgShort){
		log.info("Send Notif to Firebase:["+msisdn+"]~["+trxId+"]~["+msg+"]");
		//http://10.138.71.2:8080/MFSP/v1/pushBankTransferInfo
		HashMap<String,String> data = new HashMap<String,String>();
		data.put("userId", msisdn);
		if(Utils.isNullorEmptyString(trxId)){
			trxId = "44444444";
		}
		data.put("trxId", trxId);
		data.put("fullContentType", "html");
		data.put("shortContent", msgShort);
		data.put("fullContent", msg);
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add("sender", "middleware-indosat-dompetku");
		headers.add("Content-Type", "application/json");
		ResponseEntity<String> response = null;
		
		String urlDetail = "pushgeneraldetail";
		if(!Utils.isNullorEmptyString(type)){
			if(type.equalsIgnoreCase("BANK TRANSFER")){
				urlDetail = "pushBankTransferInfo";
			}else{
				urlDetail = "pushgeneraldetail";
				data.put("headerTitle", type);
			}
		}else{
			data.put("headerTitle", "");
		}
		
		try{
			String dataJson = Utils.convertToJSON(data);
			log.info("Parameter send to Firebase["+dataJson+"]");
			HttpEntity<String> entity = new HttpEntity<String>(dataJson, headers);
			response = restTemplate.exchange(firebaseURL+urlDetail, HttpMethod.POST, entity, String.class);
			log.info("Response send Firebase["+response+"]");
		}catch(Exception ex){
			log.error("ERROR ["+ExceptionUtils.getFullStackTrace(ex)+"]");
		}
		log.info("=============================End===========================");
	}
	
	@Async
	public void sendFirebaseNotification(String msisdn,String trxId,String msg,String msgShort){
		log.info("Send Notif to Firebase:["+msisdn+"]~["+trxId+"]~["+msg+"]");
		//http://10.138.71.2:8080/MFSP/v1/pushBankTransferInfo
		HashMap<String,String> data = new HashMap<String,String>();
		data.put("userId", msisdn);
		data.put("trxId", trxId);
		data.put("headerTitle", msgShort);
		data.put("fullContentType", "html");
		data.put("shortContent", msgShort);
		data.put("fullContent", msg);

		String dataJson = Utils.convertToJSON(data);
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
		ResponseEntity<String> response = httpClientSender.sendPost(firebaseURL+"pushgeneraldetail", dataJson, acceptableMediaTypes);
		log.info("Sent notif result:"+response.getBody());
		log.info("=============================End===========================");
	}
	
}
