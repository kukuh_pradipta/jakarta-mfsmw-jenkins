/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 10, 2016 
 * Time       : 2:48:53 PM 
 */
package com.indosat.kloc.mfsmw.sender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.Utils;

@Component
public class AlertHandler {

	@Autowired
	private HttpClientSender httpClientSender;
	@Value("${app.utiba.url.smsgw}")
	private String isatCDMSURL;
	@Value("${app.alert.sms}")
	private String msisdn;
	
	@Async
	public void sendSMSAlert(String service){
		String smsText = "Timeout found at["+Utils.getCurrentDate("dd/MM/yyyy hh:mm:ss")+"] for service["+service+"]";
		String[] to = msisdn.split(",");
		for(String number:to){
			String smsgwURL = "";
			smsgwURL = this.isatCDMSURL;
			smsgwURL = smsgwURL.replace(Constant.MSISDN_REPLACE_TAG, number);
			smsgwURL = smsgwURL.replace(Constant.SMS_REPLACE_TAG, smsText);
			smsgwURL = smsgwURL.replace(Constant.TRANSACTIONID_REPLACE_TAG, Utils.getCurrentTime());
			httpClientSender.sendGet(smsgwURL);
		}
	}
	
}
