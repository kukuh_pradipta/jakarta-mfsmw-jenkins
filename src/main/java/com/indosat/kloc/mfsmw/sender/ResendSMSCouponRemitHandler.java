/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : May 12, 2015 
 * Time       : 5:03:25 PM 
 */
package com.indosat.kloc.mfsmw.sender;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;

@Component
public class ResendSMSCouponRemitHandler {

	private static Log log = LogFactory.getLog(ResendSMSCouponRemitHandler.class);
	
	@Autowired
	private DataStore dataStore;
	@Autowired
	private HttpClientSender httpClientSender;
	@Value("${app.external.url.smsgw}")
	private String externalSmsGwURL;
	@Value("${app.utiba.url.smsgw}")
	private String isatCDMSURL;
	
	public void send(String msisdn, String sms, String transid){
		
		log.info("Resend SMS Coupon Remit msisdn["+msisdn+"] sms["+sms+"] transid["+transid+"]");
		
		String smsgwURL = "";
		
		msisdn = msisdn.replace("+", "");
		if(msisdn.startsWith("0"))
			msisdn = "62"+msisdn.substring(1);
		
		log.debug("["+msisdn+"]IS INDOSAT : "+dataStore.isIsatPrefix(msisdn));
		
		if(dataStore.isIsatPrefix(msisdn)){
			smsgwURL = this.isatCDMSURL;
			smsgwURL = smsgwURL.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
			smsgwURL = smsgwURL.replace(Constant.SMS_REPLACE_TAG, sms);
			smsgwURL = smsgwURL.replace(Constant.TRANSACTIONID_REPLACE_TAG, transid);
			httpClientSender.sendGet(smsgwURL);
		}else{
			smsgwURL = this.externalSmsGwURL;
			smsgwURL = smsgwURL.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
			smsgwURL = smsgwURL.replace(Constant.SMS_REPLACE_TAG, sms);
			httpClientSender.sendGet(smsgwURL);
		}
	}
	
}
