/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 25, 2015 
 * Time       : 4:30:13 PM 
 */
package com.indosat.kloc.mfsmw.processor.biller;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.indosat.kloc.mfsmw.dao.TransactionDao;
import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.model.BillerTransaction;
import com.indosat.kloc.mfsmw.model.Prefix;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.TrxHistList;
import com.indosat.kloc.mfsmw.util.Utils;

@Service
public class MbillerIndosmartAirtimeProcessor {

private static Log log = LogFactory.getLog(MbillerIndosmartAirtimeProcessor.class);
	
	@Autowired
	private DataStore dataStore;
	@Autowired
	private TrxHistList histList;
	@Autowired
	private HttpClientSender httpClientSender;
	@Autowired
	private TransactionDao transactionDao;
	
	@Value("${app.biller.url.indosmart}")
	private String url;
	@Value("${app.biller.response.indosmart.inquiry}")
	private String xmlInquiry;
	@Value("${app.biller.indosmart.xml.topup}")
	private String xmlTopup;
	
	@Value("${app.mbiller.response.indosmart.inquiry}")
	private String xmlInquiryResponse;
	@Value("${app.mbiller.response.indosmart.topup}")
	private String xmlTopupResponse;
	@Value("${app.mbiller.response.indosmart.pending}")
	private String xmlPendingResponse;
	
	@Value("${app.mbiller.response.failed}")
	private String xmlResponseFailed;
	
	
	public String inquiry(HttpServletRequest request){

		String function = request.getParameter(Constant.FUNCTION_PARAM);
		String transId = request.getParameter(Constant.TRANSID_PARAM);
		String amount = request.getParameter(Constant.AMOUNT_PARAM);
		String reference = request.getParameter(Constant.EXT_REF_PARAM);
		String msisdn = request.getParameter(Constant.TO_PARAM);
		String initiator = request.getParameter(Constant.INITIATOR_PARAM);
		String xmlResult = "";
		
		log.info("Incoming request IndosmartAirtime Inquiry: transId:"+transId+" amount:"+amount+" reference:"+reference +" msisdn:"+msisdn);
		
		if(Utils.isNullorEmptyString(amount)
				|| Utils.isNullorEmptyString(transId) || Utils.isNullorEmptyString(msisdn)){
			xmlResult = xmlResponseFailed;
			xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
			xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transId==null?"":transId);
			xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Missing parameters");
			xmlResult = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><result>"+xmlResult+"</result>";
		}else{
			Prefix prefix = dataStore.getTelcoIdByPrefix(msisdn);
			if(prefix != null){
				HashMap<String, String> denomMap = dataStore.getDenomMap(prefix.getId());
				String price = denomMap.get(amount); 
				if(price != null){
					xmlResult = xmlInquiryResponse;
					xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transId);
					xmlResult = xmlResult.replace(Constant.AMOUNT_REPLACE_TAG, amount);
					xmlResult = xmlResult.replace(Constant.PRICE_REPLACE_TAG, price);
					histList.writeBillTrxHist(IndosmartAirtimeProcessor.class.getSimpleName(), function, transId, reference, amount, msisdn, initiator, String.valueOf(Constant.RC_SUCCESS));
				}else{
					xmlResult = xmlResponseFailed;
					xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, String.valueOf(Constant.RC_DENOM_NOTFOUND));
					xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transId==null?"":transId);
					xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Denom not found");
					xmlResult = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><result>"+xmlResult+"</result>";
				}
			
			}else{
				xmlResult = xmlResponseFailed;
				xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
				xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transId==null?"":transId);
				xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Prefix not found");
				xmlResult = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><result>"+xmlResult+"</result>";
			}
			
		}
		
		return xmlResult;
		
	}
	
	public String topup(HttpServletRequest request) {

		String function = request.getParameter(Constant.FUNCTION_PARAM);
		String transId = request.getParameter(Constant.TRANSID_PARAM);
		String amount = request.getParameter(Constant.AMOUNT_PARAM);
		String msisdn = request.getParameter(Constant.TO_PARAM);
		String initiator = request.getParameter(Constant.INITIATOR_PARAM);
		String price = request.getParameter(Constant.PRICE_PARAM);
		String xmlResult = "";
		
		Prefix prefix = dataStore.getTelcoIdByPrefix(msisdn);
		if (prefix != null) {

			//check duplicate transaction id
			
			BillerTransaction bt = transactionDao.getBillerTransaction(transId);
			if(bt != null){
				log.info("Duplicate Transaction found transId["+transId+"]");
				log.info(bt.toString());
				if(bt.getResponseCode().equals("0") || bt.getResponseCode().equals("00") || bt.getResponseCode().equals("61") || bt.getResponseCode().equals("62")){
					xmlResult =  xmlTopupResponse;
					xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transId);
					xmlResult = xmlResult.replace(Constant.AMOUNT_REPLACE_TAG, amount);
					xmlResult = xmlResult.replace(Constant.PRICE_REPLACE_TAG, price);
					xmlResult = xmlResult.replace(Constant.OPERATOR_REPLACE_TAG, prefix.getName());
					xmlResult = xmlResult.replace(Constant.EXT_REF_ID, bt.getExtRef());
				}/*else if(bt.getResponseCode() == 13 || bt.getResponseCode() == 55 || bt.getResponseCode() == 56
						|| bt.getResponseCode() == 59 || bt.getResponseCode() == 60 || bt.getResponseCode() == 68 
						|| bt.getResponseCode() == 99 ){
					xmlResult = this.buildXmlPending(transId, amount, price, prefix.getName());
				}*/else{
					xmlResult = xmlResponseFailed;
					xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
					xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transId==null?"":transId);
					xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, dataStore.getIndosmartErrorMessage(String.valueOf(bt.getResponseCode())));
					xmlResult = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><creditAirtimeResponse>"+xmlResult+"</creditAirtimeResponse>";
				}
				
				return xmlResult;
				
			}
			
			//=============
			
			
			
				String data = xmlTopup;
				data = data.replace(Constant.TRANSACTIONID_REPLACE_TAG, transId);
				data = data.replace(Constant.DATE_REPLACE_TAG,	Utils.getCurrentDate("yyyyMMdd'T'HH:mm:ss"));
				data = data.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
				data = data.replace(Constant.AMOUNT_REPLACE_TAG, amount);
				data = data.replace(Constant.CARDNO_REPLACE_TAG, prefix.getCardNo());
				// Prepare acceptable media type
				List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
				acceptableMediaTypes.add(MediaType.APPLICATION_XML);
				ResponseEntity<String> response = httpClientSender.sendPost(url, data, acceptableMediaTypes);
				
				if(response == null){
					/*xmlResult = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><creditAirtimeResponse><ResultCode>00</ResultCode>"
							+ "<TransactionId>[%TRANSACTIONID%]</TransactionId>"
							+ "<ErrorMessage>Transaksi sedang diproses</ErrorMessage>"
							+ "<extraParameters><keyValuePair><key>amount</key><value>[%AMOUNT%]</value></keyValuePair><keyValuePair><key>price</key><value>[%PRICE%]</value></keyValuePair><keyValuePair><key>operator</key><value>[%OPERATOR%]</value></keyValuePair></extraParameters></creditAirtimeResponse>";
					xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transId);
					xmlResult = xmlResult.replace(Constant.AMOUNT_REPLACE_TAG, amount);
					xmlResult = xmlResult.replace(Constant.PRICE_REPLACE_TAG, price);
					xmlResult = xmlResult.replace(Constant.OPERATOR_REPLACE_TAG, prefix.getName());*/
					return this.buildXmlPending(transId, amount, price, prefix.getName());
				}
				
				if (response.getStatusCode().value() == 200) {
					String xml = response.getBody();
					String responseCode ="";
					String extReference="";
					
					try {
						
						DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
						DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
						InputSource is = new InputSource(new StringReader(xml));
						Document doc = dBuilder.parse(is);
						doc.getDocumentElement().normalize();
						NodeList nList = doc.getElementsByTagName("member");
						for (int temp = 0; temp < nList.getLength(); temp++) {
							Node nNode = nList.item(temp);
							if (nNode.getNodeType() == Node.ELEMENT_NODE) {
								
								Element eElement = (Element) nNode;
								
								if(eElement.getElementsByTagName("name").item(0).getTextContent().equalsIgnoreCase("ResponseCode")){
									responseCode = eElement.getElementsByTagName("value").item(0).getTextContent();
								}
								
								if(eElement.getElementsByTagName("name").item(0).getTextContent().equalsIgnoreCase("ReferenceNo")){
									extReference = eElement.getElementsByTagName("value").item(0).getTextContent();
								}
								//System.out.println("name : " + eElement.getElementsByTagName("name").item(0).getTextContent());
								//System.out.println("value : " + eElement.getElementsByTagName("value").item(0).getTextContent());
							}
						}
					
						log.info("responseCode: "+responseCode);
							
						
					} catch (ParserConfigurationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						log.info("ParserConfigurationException: "+e);
						return  this.buildXmlPending(transId, amount, price, prefix.getName());
					} catch (SAXException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						log.info("SAXException: "+e);
						return xmlResult = this.buildXmlPending(transId, amount, price, prefix.getName());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						log.info("IOException: "+e);
						return this.buildXmlPending(transId, amount, price, prefix.getName());
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
						log.info("Exception: "+e);
						histList.writeBillTrxHist(IndosmartAirtimeProcessor.class.getSimpleName(), function, transId, extReference, amount, msisdn, initiator, responseCode);
						return this.buildXmlPending(transId, amount, price, prefix.getName());
					}
					
					if(Utils.isNullorEmptyString(responseCode)){// failed
						/*log.info("Error Response, No response code");
						xmlResult = xmlResponseFailed;
						xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
						xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transId==null?"":transId);
						xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Response Error From Partner");
						xmlResult = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><creditAirtimeResponse>"+xmlResult+"</creditAirtimeResponse>";
						responseCode ="-99";
						System.out.println("error found on xml: "+xmlResult);*/
						
						//inquiry bro
						
						log.info("Error Response, No response code");
						
						xmlResult = this.buildXmlPending(transId, amount, price, prefix.getName());
								
					}else{
						if(responseCode.trim().equals("0") || responseCode.trim().equals("00") 
								|| responseCode.trim().equals("61") || responseCode.trim().equals("62")){//success
							xmlResult =  xmlTopupResponse;
							xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transId);
							xmlResult = xmlResult.replace(Constant.AMOUNT_REPLACE_TAG, amount);
							xmlResult = xmlResult.replace(Constant.PRICE_REPLACE_TAG, price);
							xmlResult = xmlResult.replace(Constant.OPERATOR_REPLACE_TAG, prefix.getName());
							xmlResult = xmlResult.replace(Constant.EXT_REF_ID, extReference);
						}/*else if(responseCode.trim().equals("13") || responseCode.trim().equals("55") || responseCode.trim().equals("56")
								|| responseCode.trim().equals("59") || responseCode.trim().equals("60") || responseCode.trim().equals("68") 
								|| responseCode.trim().equals("99")){
								
							xmlResult = this.buildXmlPending(transId, amount, price, prefix.getName());
							
						}*/else{
							xmlResult = xmlResponseFailed;
							xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
							xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transId==null?"":transId);
							xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, dataStore.getIndosmartErrorMessage(responseCode.trim()));
							xmlResult = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><creditAirtimeResponse>"+xmlResult+"</creditAirtimeResponse>";
						}
					}
					
					histList.writeBillTrxHist(IndosmartAirtimeProcessor.class.getSimpleName(), function, transId, extReference, amount, msisdn, initiator, responseCode);
					
				} else {
					/*xmlResult = xmlResponseFailed;
					xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG,	"2242");
					xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transId == null ? "" : transId);
					xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Connection error");
					xmlResult = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><creditAirtimeResponse>" + xmlResult + "</creditAirtimeResponse>";*/
					xmlResult = this.buildXmlPending(transId, amount, price, prefix.getName());
					histList.writeBillTrxHist(IndosmartAirtimeProcessor.class.getSimpleName(), function, transId, "", amount, msisdn, initiator, String.valueOf(Constant.RC_TIMEOUT));
				}

		}
		
		if(Utils.isNullorEmptyString(xmlResult))
			this.buildXmlPending(transId, amount, price, prefix.getName());
		
		return xmlResult;
	}
	
	
	private String buildXmlPending(String transId, String amount, String price, String operatorName){
		String xmlResponse = this.xmlPendingResponse;
		xmlResponse = xmlResponse.replace(Constant.TRANSACTIONID_REPLACE_TAG, transId);
		xmlResponse = xmlResponse.replace(Constant.AMOUNT_REPLACE_TAG, amount);
		xmlResponse = xmlResponse.replace(Constant.PRICE_REPLACE_TAG, price);
		xmlResponse = xmlResponse.replace(Constant.OPERATOR_REPLACE_TAG, operatorName);
		return xmlResponse;
	}
	
}
