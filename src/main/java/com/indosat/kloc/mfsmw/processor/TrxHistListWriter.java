/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 18, 2014 
 * Time       : 5:08:20 PM 
 */
package com.indosat.kloc.mfsmw.processor;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.indosat.kloc.mfsmw.dao.TransactionDao;
import com.indosat.kloc.mfsmw.model.BillerTransaction;
import com.indosat.kloc.mfsmw.model.MBillerTransaction;
import com.indosat.kloc.mfsmw.model.Transaction;
import com.indosat.kloc.mfsmw.model.UserWallet;
import com.indosat.kloc.mfsmw.pojo.DMTTransaction;
import com.indosat.kloc.mfsmw.util.TrxHistList;


@Service
public class TrxHistListWriter {

private static final Log log = LogFactory.getLog(TrxHistListWriter.class);
	
	@Autowired
	private TransactionDao dao;
	
	@Autowired
	private TrxHistList trxHistList;
	
	@Scheduled(fixedRate=1000)
	public void saveMessage(){
		saveTrx();
		saveBillerTransaction();
		saveMBillerTransaction();
		saveDMTTransaction();
		saveUserWallet();
	}
	
	private void saveTrx(){
		List<Transaction> list = trxHistList.popAll();
		if(list != null && list.size()>0){
			try{
				long m1 = System.currentTimeMillis();
				dao.saveAll(list);
				long m = System.currentTimeMillis() - m1;
				log.info("saving  Transaction " + list.size()  + " records, for " + Long.toString(m) + " ms");
			}
			catch(Exception e){
				log.error("Failed Saving  Transaction "+e.toString());
				e.printStackTrace();
			}
		}
	}
	
	private void saveBillerTransaction(){
		List<BillerTransaction> bList = trxHistList.popAllBT();
		if(bList != null && bList.size()>0){
			try{
				long m1 = System.currentTimeMillis();
				dao.saveAllBillerTrx(bList);
				long m = System.currentTimeMillis() - m1;
				log.info("saving  Biller Transaction " + bList.size()  + " records, for " + Long.toString(m) + " ms");
			}
			catch(Exception e){
				log.error("Failed Saving Biller Transaction "+e.toString());
				e.printStackTrace();
			}
		}
	}
	
	private void saveMBillerTransaction(){
		List<MBillerTransaction> list = trxHistList.popAllMBT();
		if(list != null && list.size()>0){
			try{
				long m1 = System.currentTimeMillis();
				dao.saveAllMBillerTrx(list);
				long m = System.currentTimeMillis() - m1;
				log.info("saving  MBiller Transaction " + list.size()  + " records, for " + Long.toString(m) + " ms");
			}
			catch(Exception e){
				log.error("Failed Saving Biller Transaction "+e.toString());
				e.printStackTrace();
			}
		}
	}
	
	private void saveDMTTransaction(){
		List<DMTTransaction> list = trxHistList.popAllDMT();
		if(list != null && list.size()>0){
			try {
				long m1 = System.currentTimeMillis();
				dao.saveAllDMTTransaction(list);
				long m = System.currentTimeMillis() - m1;
				log.info("saving  DMT Transaction " + list.size()  + " records, for " + Long.toString(m) + " ms");
			} catch (Exception e) {
				// TODO: handle exception
				log.error("Failed Saving DMT Transaction "+e.toString());
				e.printStackTrace();
			}
		}
	}
	
	private void saveUserWallet(){
		List<UserWallet> list = trxHistList.popAllUserWallet();
		if(list != null && list.size()>0){
			try {
				long m1 = System.currentTimeMillis();
				dao.saveAllUserWallet(list);
				long m = System.currentTimeMillis() - m1;
				log.info("saving  UserWallet Batch " + list.size()  + " records, for " + Long.toString(m) + " ms");
			} catch (Exception e) {
				// TODO: handle exception
				log.error("Failed Saving User Wallet "+e.toString());
				e.printStackTrace();
			}
		}
	}
	
}
