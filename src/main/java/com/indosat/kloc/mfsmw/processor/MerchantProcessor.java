/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 17, 2014 
 * Time       : 8:19:00 PM 
 */
package com.indosat.kloc.mfsmw.processor;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.soap.SessionHandler;
import com.indosat.kloc.mfsmw.soap.UTIBAHandler;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.TrxHistList;
import com.indosat.kloc.mfsmw.util.Utils;

@Service
public class MerchantProcessor {
	
	private static Log log = LogFactory.getLog(MerchantProcessor.class);
	
	@Autowired
	private UTIBAHandler utibaHandler;
	@Autowired
	private SessionHandler sessionHandler;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private TrxHistList histList;

	public Response doProcess(int serviceType, HttpServletRequest request){
		
		String userId 	 = request.getParameter(Constant.USERID_PARAM);
		String signature = request.getParameter(Constant.SIGNATURE_PARAM);
		String msisdn 	 = request.getParameter(Constant.MSISDN_PARAM);
		String amount 	 = request.getParameter(Constant.AMOUNT_PARAM);
		String extRef 	 = request.getParameter(Constant.EXT_REF_PARAM);
		String couponId	 = request.getParameter(Constant.COUPONID_PARAM);
		String outletId  = request.getParameter(Constant.OUTLETID_PARAM); 
		String transId   = request.getParameter(Constant.TRANSID_PARAM);
		Response rs 	 = new Response();
		ApiUser apiUser = dataStore.getApiUser(userId);
		
		if(apiUser != null){// merchant found.
			signature = signature.replace(" ", "+");
			String unpackSign = Utils.validateSignature(signature, apiUser.getPassKey());
			log.info("unpackSign: "+unpackSign);
			if(!Utils.isNullorEmptyString(unpackSign)){
				String sessionId = (String) request.getSession().getAttribute(apiUser.getUserId());
				log.info("get sessionId from session: "+sessionId);
				String initiator = unpackSign.split("\\|")[1].substring(6);
				String pin = new StringBuilder(unpackSign.split("\\|")[1].substring(0, 6)).reverse().toString();
				if(sessionId == null){
					sessionId = sessionHandler.getSessionId();
					int validateSession = sessionHandler.validateSession(sessionId, initiator, pin);
					
					if(validateSession == Constant.RC_SUCCESS){
						log.info("revalidate session and restore apiUser.getUserId(): "+apiUser.getUserId()+ "sessionId: "+sessionId);
						request.getSession().setAttribute(apiUser.getUserId(), sessionId);
						rs = doProcess(serviceType, request);
					}else{
						rs.setStatus(validateSession);
						rs.setMsg(dataStore.getErrorMsg(validateSession));
					} 
				}else{
					
					switch (serviceType) {
					
					case Constant.SERVICE_DO_PAYMENT:
						rs = utibaHandler.sell(sessionId, msisdn, amount, apiUser.getServiceType(), extRef, apiUser);
						break;
					case Constant.SERVICE_DO_PAYMENT_CHECK:
						rs = utibaHandler.checkTransaction(sessionId, extRef, apiUser);
						break;
					case Constant.SERVICE_DO_TRANSFER_TOKEN:
						rs = utibaHandler.couponTransfer(Constant.SERVICE_DO_TRANSFER_TOKEN,sessionId, msisdn, apiUser.getServiceType(), transId, amount, couponId, outletId, apiUser);
						break;
					case Constant.SERVICE_GET_TRX_STATUS:
						rs= utibaHandler.checkTransaction(sessionId, transId, apiUser);
						break;
					case Constant.SERVICE_CASHIN:
						rs = utibaHandler.cashin(sessionId, msisdn, amount, userId, transId, false, apiUser);
						break;
					case Constant.SERVICE_CASHIN_SUPPRESS_SMS:
						rs = utibaHandler.cashin(sessionId, msisdn, amount, userId, transId, true, apiUser);
						break;
					case Constant.SERVICE_REVERSAL:
						rs = utibaHandler.reversal(sessionId, transId, amount, null, apiUser);
						break;
					case Constant.SERVICE_CASHOUT:
						rs = utibaHandler.couponTransfer(Constant.SERVICE_CASHOUT, sessionId, msisdn, apiUser.getServiceType(), transId, amount, couponId, outletId, apiUser);
						break;
					}
					
					if(rs.getCommonResponse() != null){
						if(rs.getCommonResponse().getResponseCode() == Constant.RC_SESSION_EXPIRED){
							log.info("sessionid expired, trying to get the new one...");
							request.getSession().removeAttribute(apiUser.getUserId());
							rs = doProcess(serviceType, request);
						}
					}else{
						if(rs.getStatus() == Constant.RC_SESSION_EXPIRED){
							log.info("sessionid expired, trying to get the new one...");
							request.getSession().removeAttribute(apiUser.getUserId());
							rs = doProcess(serviceType, request);
						}
					}
					
					int ref =0;
					int respCode = 999;
					if(rs.getCommonResponse() != null){
						ref = rs.getCommonResponse().getTrxid();
						respCode = rs.getCommonResponse().getResponseCode();
					}else{
						ref = Integer.valueOf(rs.getTrxid());
						respCode = rs.getStatus();
					}
					
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), serviceType, initiator, 
							Utils.isNullorEmptyString(extRef)?transId:extRef, String.valueOf(ref) , respCode);
				}
			}else{
				rs.setStatus(Constant.RC_INVALID_SIGNATURE);
				rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		}else{
			rs.setStatus(Constant.RC_INVALID_USERID);
			rs.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		
		return  rs;
	}
	
}
