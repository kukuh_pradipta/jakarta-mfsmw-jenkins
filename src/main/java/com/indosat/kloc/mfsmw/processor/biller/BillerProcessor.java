/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Nov 24, 2014 
 * Time       : 11:05:27 AM 
 */
package com.indosat.kloc.mfsmw.processor.biller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.pojo.MBillerRequest;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.service.FinancialService;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;

@Service
public class BillerProcessor {

	private static Log log = LogFactory.getLog(BillerProcessor.class);
	
	@Autowired
	private HttpClientSender httpClientSender;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private FinancialService finService;
	
	@Value("${app.billergw.url.api}")
	private String billerGwApiURL;
	@Value("${app.billergw.url.tool}")
	private String billerGwToolURL;
	
	
	public Response process(MBillerRequest request, HashMap<String, String> parameterMap){
		log.debug("biller process: "+ request.toString());
		Response response = new Response();
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
		acceptableMediaTypes.add(MediaType.APPLICATION_XML);
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		
		parameters.add(Constant.AMOUNT_PARAM, request.getPrice());
		parameters.add(Constant.TRANSID_PARAM, request.getTransId());
		parameters.add(Constant.FUNCTION_PARAM, request.getFunction());
		parameters.add(Constant.REFERENCE_PARAM, request.getProductId());
		parameters.add(Constant.INITIATOR_PARAM, request.getInitiator());
		parameters.add(Constant.TO_PARAM, request.getTo());
		parameters.add(Constant.PRICE_PARAM, request.getPrice());
		parameters.add(Constant.EXT_REF_PARAM, request.getExtRef());
		
		for (Map.Entry<String, String> entry : parameterMap.entrySet()) {
		    parameters.add(entry.getKey(), entry.getValue());
		}
		
		ResponseEntity<String> re = httpClientSender.sendPost(request.getUrl(), parameters, acceptableMediaTypes);
		if(re == null){
			response.setStatus(Constant.RC_CONNECTION_ERROR);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_CONNECTION_ERROR));
			return response;
		}
		
		response = (Response) Utils.convertXmlToObject(re.getBody(), Response.class);
		if(response == null){
			response = new Response();
			response.setStatus(Constant.RC_PARS_XML_RESP_ERROR);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_PARS_XML_RESP_ERROR));
			return response;
		}
		
		return response;
	} 
	
	//Adding by Ryan directly hit to billergw
	public Response processBillerGw(HashMap<String, String> parameterMap){
		
		log.debug("billerGW process: "+ parameterMap.toString());
		Response response = new Response();
		String url="";
		
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		
		for (Map.Entry<String, String> entry : parameterMap.entrySet()) {
		    parameters.add(entry.getKey(), entry.getValue());
		}
		//otype 1:api 2:tools
		if(parameterMap.get("otype").equals("1")){
			url = billerGwApiURL;
		}else if(parameterMap.get("otype").equals("2")){
			url = billerGwToolURL;
		}
		
		ResponseEntity<String> re = httpClientSender.sendPost(url, parameters, acceptableMediaTypes);
		if(re == null){
			response.setStatus(Constant.RC_CONNECTION_ERROR);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_CONNECTION_ERROR));
			return response;
		}
		
		response.setStatus(Integer.parseInt(Utils.getJSONValue(re.getBody(), "status")));
		response.setMsg(Utils.getJSONValue(re.getBody(), "msg"));
		return response;
	} 
	
}
