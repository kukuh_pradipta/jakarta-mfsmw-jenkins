/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Sep 9, 2014 
 * Time       : 9:51:55 AM 
 */
package com.indosat.kloc.mfsmw.processor.biller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.TrxHistList;
import com.indosat.kloc.mfsmw.util.Utils;

@Service
public class KCJProcessor {
	
	private static Log log = LogFactory.getLog(KCJProcessor.class);
	
	@Autowired
	private HttpClientSender httpClientSender;
	@Autowired
	private TrxHistList histList;
	
	@Value("${app.biller.url.kcj.inquiry}")
	private String kcjBillerInquiryURL;
	@Value("${app.biller.url.kcj.payment}")
	private String kcjBillerPaymentURL;
	@Value("${app.biller.response.kcj.inquiry.success}")
	private String xmlResponseKCJInquirySuccess;
	@Value("${app.biller.response.kcj.payment.success}")
	private String xmlResponseKCJPaymentSuccess;
	@Value("${app.biller.response.failed}")
	private String xmlResponseFailed;
	

	public String inquiry(HttpServletRequest request){
		
		String function = request.getParameter(Constant.FUNCTION_PARAM);
		String reference = request.getParameter(Constant.REFERENCE_PARAM);
		String amount = request.getParameter(Constant.AMOUNT_PARAM);
		String transactionId = request.getParameter(Constant.TRANSACTIONID_PARAM);
		String msisdn = request.getParameter(Constant.MSISDN_PARAM);
		String xmlResult = "";
		
		log.info("Incoming request KCJ Inquiry: transactionId:"+transactionId+" denom:"+amount+" reference:"+reference);
		
		if(Utils.isNullorEmptyString(reference) || Utils.isNullorEmptyString(amount) 
				|| Utils.isNullorEmptyString(transactionId)){
			xmlResult = xmlResponseFailed;
			xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
			xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId==null?"":transactionId);
			xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Missing parameters");
			xmlResult = "<isKCJValidResponse>"+xmlResult+"</isKCJValidResponse>";
		}else{
			String apiURL = kcjBillerInquiryURL;
			apiURL = apiURL.replace(Constant.REFERENCE_REPLACE_TAG, reference);
			apiURL = apiURL.replace(Constant.AMOUNT_REPLACE_TAG, amount);
			ResponseEntity<String> re = httpClientSender.sendGet(apiURL);
			if(re.getStatusCode().value() == 200){
				String kcjXmlResult = re.getBody();
				if(Utils.getXmlTagValue(kcjXmlResult, "<status>") != null 
						&& Utils.getXmlTagValue(kcjXmlResult, "<status>").equals("0")){//success
					
					xmlResult = xmlResponseKCJInquirySuccess;
					xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId);
					xmlResult = xmlResult.replace(Constant.AMOUNT_REPLACE_TAG, amount);
					xmlResult = xmlResult.replace(Constant.PRICE_REPLACE_TAG, Utils.getXmlTagValue(kcjXmlResult, "<nominal>"));
					xmlResult = xmlResult.replace(Constant.REFERENCE_REPLACE_TAG, reference);
					
				}
				
				/*histList.writeBillTrxHist(KCJProcessor.class.getSimpleName(), function, transactionId, "0", amount, 
						Utils.getXmlTagValue(kcjXmlResult, "<status>")==null?Constant.RC_BILLER_GENERAL_ERROR:Constant.RC_SUCCESS,msisdn);*/
			}
		}
		
		return xmlResult;
	}
	
	public String payment(HttpServletRequest request){
		
		String apiURL = kcjBillerPaymentURL;
		String xmlResult = "";
		
		String function = request.getParameter(Constant.FUNCTION_PARAM);
		String reference = request.getParameter(Constant.REFERENCE_PARAM);
		String transactionId = request.getParameter(Constant.TRANSACTIONID_PARAM);
		String msisdn = request.getParameter(Constant.MSISDN_PARAM);
		String amount = request.getParameter(Constant.AMOUNT_PARAM);
		
		log.info("Incoming request KCJ payment: transactionId:"+transactionId+" denom:"+amount+" reference:"+reference+ " msisdn:"+msisdn);
		
		if(Utils.isNullorEmptyString(reference) || Utils.isNullorEmptyString(transactionId)
				|| Utils.isNullorEmptyString(msisdn) || Utils.isNullorEmptyString(amount)){
			xmlResult = xmlResponseFailed;
			xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
			xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId==null?"":transactionId);
			xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Missing parameters");
			xmlResult = "<creditKCJResponse>"+xmlResult+"</creditKCJResponse>";
		}else{
			apiURL = apiURL.replace(Constant.REFERENCE_REPLACE_TAG, reference);
			apiURL = apiURL.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId);
			apiURL = apiURL.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
			ResponseEntity<String> re = httpClientSender.sendGet(apiURL);
			
			if(re.getStatusCode().value() == 200){
				String kcjXmlResult = re.getBody();
				if(Utils.getXmlTagValue(kcjXmlResult, "<status>") != null 
						&& Utils.getXmlTagValue(kcjXmlResult, "<status>").equals("0")){//success
					xmlResult = xmlResponseKCJPaymentSuccess;
					xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId);
					xmlResult = xmlResult.replace(Constant.AMOUNT_REPLACE_TAG, amount);
					xmlResult = xmlResult.replace(Constant.PRICE_REPLACE_TAG, amount);
					xmlResult = xmlResult.replace(Constant.REFERENCE_REPLACE_TAG, reference);
				}
				
				/*histList.writeBillTrxHist(KCJProcessor.class.getSimpleName(), function, transactionId, "0", amount, 
						Utils.getXmlTagValue(kcjXmlResult, "<status>")==null?Constant.RC_BILLER_GENERAL_ERROR:Constant.RC_SUCCESS, msisdn);*/
			}
		}
		
		return xmlResult;
	}
	
}
