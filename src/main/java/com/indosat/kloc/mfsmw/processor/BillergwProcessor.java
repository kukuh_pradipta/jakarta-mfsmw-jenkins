/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 23, 2015 
 * Time       : 11:46:57 AM 
 */
package com.indosat.kloc.mfsmw.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;

@Service
public class BillergwProcessor {

	private static Log log = LogFactory.getLog(BillergwProcessor.class);
	
	@Autowired
	private HttpClientSender httpSender;
	
	@Value("${app.billergw.async.url}")
	private String billergwURL="http://localhost:8080/billergw/biller/result";
	
	public String asyncHoreKreatif(String body){
		log.info("ASYNC Response from Hore Kreatif ["+body+"]");
		String url = billergwURL+"/horekreatif";
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
		ResponseEntity<String> responseHttp = httpSender.sendPost(url, body, acceptableMediaTypes);
		return responseHttp.getBody();
	}
	
}
