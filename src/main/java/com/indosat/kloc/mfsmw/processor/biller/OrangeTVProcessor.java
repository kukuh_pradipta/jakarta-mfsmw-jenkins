/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Sep 8, 2014 
 * Time       : 11:43:23 AM 
 */
package com.indosat.kloc.mfsmw.processor.biller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.TrxHistList;
import com.indosat.kloc.mfsmw.util.Utils;

@Service
public class OrangeTVProcessor {
	
	private static Log log = LogFactory.getLog(OrangeTVProcessor.class);
	
	@Autowired
	private HttpClientSender httpClientSender;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private TrxHistList histList;
	
	@Value("${app.biller.url.orangetv}")
	private String url;
	@Value("${app.biller.fee.orangetv}")
	private Integer fee;
	@Value("${app.biller.denom.orange}")
	private String denom;
	@Value("${app.biller.pin.orangetv}")
	private String pin;
	@Value("${app.biller.hw.orangetv}")
	private String hw;
	@Value("${app.biller.response.orangetv.inquiry}")
	private String xmlInquiry;
	@Value("${app.biller.response.orangetv.payment}")
	private String xmlpayment;
	@Value("${app.biller.response.failed}")
	private String xmlResponseFailed;
	
	public String inquiry(HttpServletRequest request){
		
		String function = request.getParameter(Constant.FUNCTION_PARAM);
		String transactionId = request.getParameter(Constant.TRANSACTIONID_PARAM);
		String denom = request.getParameter(Constant.DENOM_PARAM);
		String reference = request.getParameter(Constant.REFERENCE_PARAM);
		String msisdn = request.getParameter(Constant.MSISDN_PARAM);
		String xmlResult=xmlResponseFailed;
		
		log.info("Incoming request OrangeTV Inquiry: transactionId:"+transactionId+" denom:"+denom+" reference:"+reference);
		
		if(!Utils.isNullorEmptyString(denom) && !Utils.isNullorEmptyString(transactionId) 
				&& !Utils.isNullorEmptyString(reference)){
			int price = 0;
			String amount = "";
			String productDenom [] = this.denom.split(",");
			for(String obj: productDenom){
				String denomination [] = obj.split("\\|");
				if(denomination[0].equals(denom)){
					price = Integer.valueOf(denomination[1])+this.fee;
					amount = denomination[1];
					break;
				}
			}
			
			if(price > 0){
				xmlResult = xmlInquiry;
				xmlResult = xmlResult.replace(Constant.DENOM_REPLACE_TAG, denom);
				xmlResult = xmlResult.replace(Constant.AMOUNT_REPLACE_TAG, amount);
				xmlResult = xmlResult.replace(Constant.PRICE_REPLACE_TAG, String.valueOf(price));
				xmlResult = xmlResult.replace(Constant.FEE_REPLACE_TAG, String.valueOf(this.fee));
			}else{
				xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, String.valueOf(Constant.RC_BILLER_GENERAL_ERROR));
				xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Denom tidak dikenal");
				xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId==null?"":transactionId);
				xmlResult = "<BuyOrangeInqResponse>"+xmlResult+"</BuyOrangeInqResponse>";
			}
			
			//histList.writeBillTrxHist(OrangeTVProcessor.class.getSimpleName(), function, transactionId, "0", denom, price > 0? Constant.RC_SUCCESS:Constant.RC_BILLER_GENERAL_ERROR, msisdn);
			
		}else{
			xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, String.valueOf(Constant.RC_BILLER_GENERAL_ERROR));
			xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId==null?"":transactionId);
			xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Missing Parameters");
			xmlResult = "<BuyOrangeInqResponse>"+xmlResult+"</BuyOrangeInqResponse>";
		}
		
		
		
		return xmlResult;
	}
	
	public String payment(HttpServletRequest request){
		
		String function = request.getParameter(Constant.FUNCTION_PARAM);
		String transactionId = request.getParameter(Constant.TRANSACTIONID_PARAM);
		String denom = request.getParameter(Constant.DENOM_PARAM);
		String reference = request.getParameter(Constant.REFERENCE_PARAM);
		String msisdn = request.getParameter(Constant.MSISDN_PARAM);
		String xmlResult=xmlResponseFailed;
		
		if(!Utils.isNullorEmptyString(denom) && !Utils.isNullorEmptyString(transactionId) 
				&& !Utils.isNullorEmptyString(reference)){
			
			StringBuilder urlbuilder = new StringBuilder(this.url);
			urlbuilder.append("?tipe=trx").append("&snmpotv=").append(transactionId);
			urlbuilder.append("&orangeid=").append(reference).append("&denom=").append(denom);
			urlbuilder.append("&pin=").append(this.pin).append("&hw=").append(this.hw);
			
			String url = urlbuilder.toString();
			log.debug(url);
			ResponseEntity<String> re = httpClientSender.sendGet(url);
			if(re.getStatusCode().value() == 200){
				String xmlOTResult = re.getBody();
				if(Utils.getXmlTagValue(xmlOTResult, "<result>") != null 
						&& Utils.getXmlTagValue(xmlOTResult, "<result>").equals("999")){
					xmlResult = xmlpayment;
					xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId);
					xmlResult = xmlResult.replace(Constant.VOUCHER_REPLACE_TAG, Utils.getXmlTagValue(xmlOTResult, "<snorangeid>"));
					xmlResult = xmlResult.replace(Constant.EXT_REF_ID, Utils.getXmlTagValue(xmlOTResult, "<snmitraorangeid>"));
				}else{
					xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
					xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId == null ? "" : transactionId);
					xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, dataStore.getORTVErrorMessage(Utils.getXmlTagValue(xmlOTResult, "<result>")));
					xmlResult = "<BuyOrangeInqResponse>" + xmlResult+ "</BuyOrangeInqResponse>";
				}
				
				/*histList.writeBillTrxHist(OrangeTVProcessor.class.getSimpleName(), function, transactionId, 
						Utils.getXmlTagValue(xmlOTResult, "<snorangeid>")==null?"0":Utils.getXmlTagValue(xmlOTResult, "<snorangeid>"), 
								denom, Utils.getXmlTagValue(xmlOTResult, "<result>")==null?Constant.RC_BILLER_GENERAL_ERROR:Integer.valueOf(Utils.getXmlTagValue(xmlOTResult, "<result>")), msisdn);*/
			}else{
				xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
				xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId == null ? "" : transactionId);
				xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Connection error");
				xmlResult = "<BuyOrangeInqResponse>" + xmlResult+ "</BuyOrangeInqResponse>";
			}
			
			return xmlResult;
		}
		
		xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
		xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId == null ? "" : transactionId);
		xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Missing Parameters");
		xmlResult = "<BuyOrangeInqResponse>" + xmlResult+ "</BuyOrangeInqResponse>";
		
		return xmlResult;
	}
	
}
