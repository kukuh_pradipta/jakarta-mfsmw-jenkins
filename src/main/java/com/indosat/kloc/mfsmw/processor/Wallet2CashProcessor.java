/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jun 30, 2015 
 * Time       : 2:30:36 PM 
 */
package com.indosat.kloc.mfsmw.processor;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.indosat.kloc.mfsmw.pojo.Parameter;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.sender.SmsClientSender;
import com.indosat.kloc.mfsmw.service.FinancialService;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;

@Component
public class Wallet2CashProcessor {

	private static Log log = LogFactory.getLog(Wallet2CashProcessor.class);
	
	@Autowired
	private FinancialService financialService;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private SmsClientSender smsClientSender;
	
	@Value("${app.sms.notificatin.trx.failed}")
	private String smsTextFailed;
	
	@Async
	public void create(String userId, String signature, Parameter parameter){
		financialService.wallet2Cash(userId, signature, parameter);
	}
	
	@Async
	public void cancel(String userId, String initiator, String signature, Parameter parameter){
		Response getCouponResponse = financialService.getCouponRemitance(userId, signature, parameter);
		log.info("getCouponResponse:"+getCouponResponse.toString());
		if(getCouponResponse.getStatus() == Constant.RC_SUCCESS){
			if(!initiator.equals(getCouponResponse.getCouponDetails().getSenderPhoneNumber())){
				String smsText = smsTextFailed;
				smsText = smsText.replace(Constant.DATE_REPLACE_TAG, Utils.getCurrentDate("dd/MM/yy HH:mm"));
				smsText = smsText.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Anda tidak diperbolehkan membatalkan kode MTCN ini.");
				smsClientSender.send(smsText, initiator);
			}else{
				Response cancelResponse = financialService.cancelCouponRemitance(userId, signature, parameter);
				if(cancelResponse.getStatus() == Constant.RC_SUCCESS){
					HashMap<String, String> param = parameter.getKeyValueMap();
					String couponId = param.get(Constant.COUPONID_PARAM);
					smsClientSender.send("Kode Pencairan "+couponId+" dibatalkan dan sudah tidak berlaku. Ref "+cancelResponse.getTrxid(), initiator);
				}
			}
		}else{
			String smsText = smsTextFailed;
			smsText = smsText.replace(Constant.DATE_REPLACE_TAG, Utils.getCurrentDate("dd/MM/yy HH:mm"));
			smsText = smsText.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Kode MTCN tidak tersedia/pernah digunakan.");
			smsClientSender.send(smsText, initiator);
		}
	}
	
}
