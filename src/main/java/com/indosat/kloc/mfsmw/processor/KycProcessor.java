/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 23, 2015 
 * Time       : 11:46:57 AM 
 */
package com.indosat.kloc.mfsmw.processor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.indosat.kloc.mfsmw.dao.KycDao;
import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.model.KycRawData;
import com.indosat.kloc.mfsmw.pojo.AgentData;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.sender.SmsClientSender;
import com.indosat.kloc.mfsmw.soap.UTIBAHandler;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;

@Service
public class KycProcessor {

	private static Log log = LogFactory.getLog(KycProcessor.class);

	@Autowired
	private UTIBAHandler utibaHandler;
	@Autowired
	private KycDao kycDao;
	@Autowired
	private SmsClientSender smsClientSender;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private HttpClientSender httpSender;

	@Value("${app.sms.kyc.verify}")
	private String smsVerifyText;
	@Value("${app.sms.kyc.verify.blank}")
	private String smsVerifyBlank;
	@Value("${app.utiba.agentid.wallet.nonkyc}")
	private long nonKycAgentId;
	@Value("${app.ektp.soap.url}")
	private String ektpURL;
	@Value("${app.ektp.soap.request}")
	private String ektpXMLReq;

	@Async
	public void verify(HashMap<String, String> parameterMap) {

		String msisdn = parameterMap.get(Constant.MSISDN_PARAM);

		if (Utils.isNullorEmptyString(msisdn))
			return;

		ApiUser apiUser = new ApiUser();
		apiUser.setChannelType(Constant.CHANNEL_TYPE_WEB);
		apiUser.setUserId("dompetkuplus");
		Response response = utibaHandler.getAgentByReferenceRequest(msisdn, apiUser);
		if (response.getAgentData().getWalletType() != 3) {
			String sms = "";
			KycRawData data = kycDao.getByMSISDN(msisdn);
			if (data != null) {
				sms = smsVerifyText;
				sms = sms.replace(Constant.NAME_REPLACE_TAG, data.getName());
				sms = sms.replace(Constant.DOB_REPLACE_TAG, data.getDob());
			} else {
				sms = smsVerifyBlank;
			}
			smsClientSender.send(sms, msisdn);
		}
	}

	@Async
	public void confirm(HashMap<String, String> parameterMap) {

		String msisdn = parameterMap.get(Constant.MSISDN_PARAM);
		ApiUser apiUser = new ApiUser();
		apiUser.setChannelType(Constant.CHANNEL_TYPE_WEB);
		apiUser.setUserId("dompetkuplus");
		Response response = utibaHandler.getAgentByReferenceRequest(msisdn, apiUser);
		if (response.getAgentData().getWalletType() != 3) {
			String message = parameterMap.get(Constant.EXT_REF_PARAM);

			if (Utils.isNullorEmptyString(message)) {

			} else {
				String messages[] = message.split("#");
				if (messages.length != 3) {

				} else {
					String name = messages[0];
					String dob = messages[1];
					String idNo = messages[2];
				}
			}
		}
	}

	public Response ektpHit(String msisdn, String idno, String keyword, String trxid) {
		Response res = new Response();
		HttpsURLConnection httpsConnection = null;

		log.debug("Request EKTP SOAP [" + msisdn + "][" + idno + "][" + keyword + "]");
		final boolean isHttps = ektpURL.toLowerCase().startsWith("https");
		try {
			if (isHttps) {
				httpsConnection = HttpClientSender.ignoreCertAndHostVerification(ektpURL);
			}
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			// Preparing soap request
			String soapRequest = ektpXMLReq;
			soapRequest = soapRequest.replace(Constant.ACCOUNTID_REPLACE_TAG, idno);
			soapRequest = soapRequest.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
			if(StringUtils.isNumeric(keyword)){
				soapRequest = soapRequest.replace(Constant.REFERENCE_REPLACE_TAG, "<KK>"+keyword+"</KK>");
			}else{
				soapRequest = soapRequest.replace(Constant.REFERENCE_REPLACE_TAG, "<MM_NAME>"+keyword+"</MM_NAME>");
			}
			soapRequest = soapRequest.replace(Constant.TRX_ID_REPLACE_TAG, trxid);

			log.debug("Request EKTP SOAP [" + msisdn + "][" + soapRequest + "]");
			SOAPMessage soapResponse = soapConnection.call(createSoapFormat(soapRequest), ektpURL);
			// Process the SOAP Response
			String resSEV = soapMessageToString(soapResponse);
			log.debug("Result SOAP [" + msisdn + "][" + resSEV + "]");
			if(Utils.getXmlTagValue(resSEV,"<val:responseCode>").equals("0")){
				AgentData ad = new AgentData();
				ad.setReligion(Utils.getXmlTagValue(resSEV,"<val:agama>"));
				ad.setAddress(Utils.getXmlTagValue(resSEV,"<val:alamat>"));
				ad.setBloodType(Utils.getXmlTagValue(resSEV,"<val:golongan_darah>"));
				ad.setGender(Utils.getXmlTagValue(resSEV,"<val:jenis_kelamin>"));
				ad.setCity(Utils.getXmlTagValue(resSEV,"<val:kabupaten>"));
				ad.setKecamatan(Utils.getXmlTagValue(resSEV,"<val:kecamatan>"));
				ad.setKelurahan(Utils.getXmlTagValue(resSEV,"<val:kelurahan>"));
				ad.setPostalCode(Utils.getXmlTagValue(resSEV,"<val:kode_pos>"));
				String namalkp = Utils.getXmlTagValue(resSEV,"<val:nama_lengkap>");
				if (namalkp.split(" ").length > 1) {
					ad.setFirstName(namalkp.split(" ")[0]);
					ad.setLastName(namalkp.replace(namalkp.split(" ")[0] + " ", ""));
				} else {
					ad.setFirstName(namalkp);
					ad.setLastName(null);
				}
				ad.setMotherMaidenName(Utils.getXmlTagValue(resSEV,"<val:nama_lengkap_ibu>"));
				ad.setNo_kk(Utils.getXmlTagValue(resSEV,"<val:no_kk>"));
				ad.setIdNo(Utils.getXmlTagValue(resSEV,"<val:nik>"));
				ad.setOccupation(Utils.getXmlTagValue(resSEV,"<val:pekerjaan>"));
				ad.setLastEducation(Utils.getXmlTagValue(resSEV,"<val:pendidikan_akhir>"));
				ad.setProvince(Utils.getXmlTagValue(resSEV,"<val:propinsi>"));
				ad.setDateOfBirth(Utils.getXmlTagValue(resSEV,"<val:tgl_lahir>"));
				ad.setPlaceOfBirth(Utils.getXmlTagValue(resSEV,"<val:tempat_lahir>"));
				ad.setNo_rt(Utils.getXmlTagValue(resSEV,"<val:no_rt>"));
				ad.setNo_rw(Utils.getXmlTagValue(resSEV,"<val:no_rw>"));
				res.setAgentData(ad);
				res.setStatus(Constant.RC_SUCCESS);
				res.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
			}else if(Utils.getXmlTagValue(resSEV,"<val:responseCode>").equals("-1")){
				log.error("ERROR EKTP [" + Utils.getXmlTagValue(resSEV,"<val:responseCode>") + "]["+Utils.getXmlTagValue(resSEV,"<val:remark>")+"]");
				res.setStatus(Constant.RC_EKTP_NOT_VALID);
				res.setMsg(dataStore.getErrorMsg(Constant.RC_EKTP_NOT_VALID));
				return res;
			}else{
				log.error("ERROR EKTP [" + Utils.getXmlTagValue(resSEV,"<val:responseCode>") + "]["+Utils.getXmlTagValue(resSEV,"<val:remark>")+"]");
				res.setStatus(Constant.RC_EKTP_UNKNOWN_ERROR);
				res.setMsg(dataStore.getErrorMsg(Constant.RC_EKTP_UNKNOWN_ERROR));
				return res;
			}
			soapConnection.close();
			if (isHttps) {
				httpsConnection.disconnect();
			}
		} catch (Exception ex) {
			log.error("ERROR EKTP [" + ExceptionUtils.getFullStackTrace(ex) + "]");
			res.setStatus(Constant.RC_EKTP_UNKNOWN_ERROR);
			res.setMsg(dataStore.getErrorMsg(Constant.RC_EKTP_UNKNOWN_ERROR));
			return res;
		}

		return res;
	}

	
	// UTILS
	private static SOAPMessage createSoapFormat(String xml) throws Exception {
		MessageFactory factory = MessageFactory.newInstance();
		SOAPMessage message = factory.createMessage(new MimeHeaders(),
				new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8"))));
		System.out.print("Request SOAP Message = ");
		message.writeTo(System.out);
		System.out.println();
		return message;
	}
	
	public static String soapMessageToString(SOAPMessage message) {
		String result = null;
		if (message != null) {
			ByteArrayOutputStream baos = null;
			try {
				baos = new ByteArrayOutputStream();
				message.writeTo(baos);
				result = baos.toString();
			} catch (Exception e) {
			} finally {
				if (baos != null) {
					try {
						baos.close();
					} catch (IOException ioe) {
					}
				}
			}
		}
		return result;
	} 
}
