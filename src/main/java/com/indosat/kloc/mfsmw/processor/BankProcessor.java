/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 23, 2015 
 * Time       : 11:46:57 AM 
 */
package com.indosat.kloc.mfsmw.processor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.indosat.kloc.mfsmw.dao.KycDao;
import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.model.KycRawData;
import com.indosat.kloc.mfsmw.pojo.AgentData;
import com.indosat.kloc.mfsmw.pojo.AuthResponse;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.sender.SmsClientSender;
import com.indosat.kloc.mfsmw.service.FinancialServiceImpl;
import com.indosat.kloc.mfsmw.soap.SessionHandler;
import com.indosat.kloc.mfsmw.soap.UTIBAHandler;
import com.indosat.kloc.mfsmw.util.AeSimpleSha2;
import com.indosat.kloc.mfsmw.util.AuthenticationRequest;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.SignatureBuilder;
import com.indosat.kloc.mfsmw.util.Utils;
import com.paypro.mfs.microapps.service.QRService;
import com.paypro.mfs.microapps.service.QRServiceImpl;

@Service
public class BankProcessor {

	private static Log log = LogFactory.getLog(BankProcessor.class);

	@Autowired
	private UTIBAHandler utibaHandler;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private SessionHandler sessionHandler;
	@Autowired
	private HttpClientSender httpSender;
	@Autowired
	private FinancialServiceImpl financialService;
	@Autowired
	private SmsClientSender smsClientSender;
	@Autowired
	private AuthenticationRequest authenticationRequest;
	@Autowired
	private QRService qrService;
	
	@Value("${app.qr.dealoka.notifurl}")
	private String urlDealoka;
	@Value("${app.qr.dealoka.merclabel}")
	private String merchantLabel;
	@Value("${app.qr.dealoka.mercid}")
	private String merchantId;
	@Value("${app.qr.dealoka.merckey}")
	private String merchantkey;
	private String dlkCode;


	String responseSuccessXML = "<response><notification><status>[%STATUS%]</status><trxid>[%TRX_ID%]</trxid><msg>[%MESSAGE%]</msg><amount>[%AMOUNT%]</amount><vaNo>[%REFERENCE%]</vaNo><merchantid>[%OUTLET_CODE%]</merchantid></notification></response>";
	String responseFailedXML = "<response><notification><status>[%STATUS%]</status><msg>[%MESSAGE%]</msg></notification></response>";

	public String SendNotificationNiaga(String body) {
		String res = null;
		res = responseFailedXML;
		res = res.replace(Constant.STATUS_REPLACE_TAG, String.valueOf(Constant.RC_SYSTEM_ERROR));
		res = res.replace(Constant.MESSAGE_REPLACE_TAG, dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
		
		Response response = new Response();
		String signature = Utils.getXmlTagValue(body, "<signature>");
		String userid = Utils.getXmlTagValue(body, "<userid>");
		String extref = Utils.getXmlTagValue(body, "<extRef>");
		String vaNo = Utils.getXmlTagValue(body, "<vaNo>");
		String trxid = Utils.getXmlTagValue(body, "<trxid>");
		String amount = Utils.getXmlTagValue(body, "<amount>");
		String merchantid = Utils.getXmlTagValue(body, "<merchantid>");
		String status = Utils.getXmlTagValue(body, "<status>");
		String statusMessage = Utils.getXmlTagValue(body, "<statusMessage>");
		String dealokaId = null;
		log.debug("Request SendNotificationNiaga [" + trxid + "][" + merchantid + "]");
		ApiUser apiUser = dataStore.getApiUser(userid);
		if (apiUser != null) {
			AuthResponse authResponse = authenticationRequest.validate(userid, signature);
			if (authResponse.getStatus() == Constant.RC_SUCCESS) {
//					response = utibaHandler.checkTransaction(sessionHandler.getValidGlobalSessionId(), extref, apiUser);
//					response = utibaHandler.getTransactionById(authResponse.getSessionId(), Integer.parseInt(trxid), 999998,apiUser);
					response = qrService.checkTrxMPQR(trxid, vaNo);
					if(response.getStatus()==Constant.RC_SUCCESS){
						res = responseSuccessXML;
						res = res.replace(Constant.STATUS_REPLACE_TAG, String.valueOf(response.getStatus()));
						res = res.replace(Constant.MESSAGE_REPLACE_TAG, dataStore.getErrorMsg(response.getStatus()));
						res = res.replace(Constant.TRX_ID_REPLACE_TAG, String.valueOf(response.getTrxid()));
						res = res.replace(Constant.AMOUNT_REPLACE_TAG, amount);
						res = res.replace(Constant.REFERENCE_REPLACE_TAG, vaNo);
						res = res.replace(Constant.OUTLET_CODE_REPLACE_TAG, merchantid);
						try {
							Response hitDealoka = hitDealokaNotification(merchantid, amount, trxid,status,statusMessage);
							if(hitDealoka.getStatus()==Constant.RC_SUCCESS){
								log.info("Hit Notification Dealoka SUCCESS");
								dealokaId = hitDealoka.getExtRef();
							}else{
								log.error("Hit Notification Dealoka FAILED");
								dealokaId = hitDealoka.getExtRef();
							}
						} catch (NoSuchAlgorithmException e) {
							log.error("Hit Notification Dealoka ERROR ["+e.getMessage()+"]");
							log.error(ExceptionUtils.getFullStackTrace(e));
						}
					}else{
						res = responseFailedXML;
						res = res.replace(Constant.STATUS_REPLACE_TAG, String.valueOf(response.getStatus()));
						res = res.replace(Constant.MESSAGE_REPLACE_TAG, dataStore.getErrorMsg(response.getStatus()));
						log.error("Status:"+response.getStatus()+"/n"
								+ "Msg:"+dataStore.getErrorMsg(response.getStatus()));
					}
					qrService.recordNotifMPQR(userid, extref, vaNo, trxid, amount, merchantid, dealokaId);
				} else {
					res = responseFailedXML;
					res = res.replace(Constant.STATUS_REPLACE_TAG, String.valueOf(authResponse.getStatus()));
					res = res.replace(Constant.MESSAGE_REPLACE_TAG, dataStore.getErrorMsg(authResponse.getStatus()));
					log.error("Status:"+authResponse.getStatus()+"/n"
							+ "Msg:"+dataStore.getErrorMsg(authResponse.getStatus()));
				}
		} else {
			res = responseFailedXML;
			res = res.replace(Constant.STATUS_REPLACE_TAG, String.valueOf(Constant.RC_INVALID_USERID));
			res = res.replace(Constant.MESSAGE_REPLACE_TAG, dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			log.error("Status:"+Constant.RC_INVALID_USERID+"/n"
					+ "Msg:"+dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return res;
	}
	
	private Response hitDealokaNotification(String msisdnMerchant,String amount,String transid, String status, String statusMessage) throws NoSuchAlgorithmException{
		Response res = new Response();
		res.setStatus(Constant.RC_SYSTEM_ERROR);
		res.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
		
		String url = urlDealoka+"api_notification_mc_callback_url";
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		parameters.add("token", generateDealokaToken(msisdnMerchant, amount, transid));
		parameters.add("amount", amount);
		parameters.add("wallet_id", msisdnMerchant);
		parameters.add("transid", transid);
		parameters.add("merchant_label", merchantLabel);
		parameters.add("dlk_code", dlkCode);
		parameters.add("status", status);
		parameters.add("status_message", statusMessage);
		try{
		ResponseEntity<String> responseHttp = httpSender.sendPost(url, parameters, acceptableMediaTypes);
		String resultDealoka = Utils.getJSONValue(responseHttp.getBody(),"message_action");
			if(responseHttp.getStatusCode()==HttpStatus.OK){
				if(resultDealoka.contains("SUCCESS")){
					res.setStatus(Constant.RC_SUCCESS);
					res.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
				}
			}
			res.setExtRef(Utils.getJsonData(resultDealoka).get("message_id").getAsString());
		}catch(Exception ex){
			log.error("HIT Dealoka Error ["+ex.getMessage()+"]");
		}
		return res;
	}
	
	private String generateDealokaToken(String msisdnMerchant,String amount,String transid) throws NoSuchAlgorithmException{
		String res = null;
		//merchant_label  + %|% + merchant_id + %|% + dlk_code + %|% + merchant_key + %|% + str(sequence)  
		//DLK_<epoch_timestamp>_<5 digit random number> 
		//TOKEN = hashlib.sha256(mercToken.encode(ascii)).hexdigest()
		//str(dlk_code) + str(wallet_id) + str(amount) + str(transid) 
		String epochtime = String.valueOf(System.currentTimeMillis()/1000);
		dlkCode = "DLK_"+epochtime+"_"+Utils.generateNumber(5);
		String seq = dlkCode+msisdnMerchant+amount+transid;
		String formula = merchantLabel + "%|%" + merchantId  + "%|%" + dlkCode + "%|%" +merchantkey+ "%|%" + seq;
		res = AeSimpleSha2.SHA2(formula);
		return res;
	}
	
	//Util
//	private boolean checkSHA1Valid(String sha){
//		return sha.matches("[a-fA-F0-9]{40}");
//	}
}
