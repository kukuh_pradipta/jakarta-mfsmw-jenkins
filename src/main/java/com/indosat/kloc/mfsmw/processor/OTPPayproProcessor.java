/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 23, 2015 
 * Time       : 11:46:57 AM 
 */
package com.indosat.kloc.mfsmw.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.google.gson.JsonObject;
import com.indosat.kloc.mfsmw.dao.OTPPayproDao;
import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.model.OTPPaypro;
import com.indosat.kloc.mfsmw.pojo.Parameter;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.sender.SmsClientSender;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;
import com.indosat.kloc.mfsmw.web.SMSPushController;

@Service
public class OTPPayproProcessor {
	
	private static Log log = LogFactory.getLog(OTPPayproProcessor.class);
	
	@Autowired
	private SmsClientSender smsSender;
	@Autowired
	private OTPPayproDao otpPayproDao;
	
	public String getToken(Parameter parameter) {
		String msisdn = parameter.getKeyValueMap().get("number");
		String apikey = parameter.getKeyValueMap().get("api_key");
		String apisecret = parameter.getKeyValueMap().get("api_secret");
		
		HashMap<String, String> resMap = new HashMap<>();
		if (!apikey.equalsIgnoreCase("ab2c3e22") || !apisecret.equalsIgnoreCase("c2f0c5ef1f3792a0")) {
			resMap.put("status", "-97");
			resMap.put("request_id", null);
			resMap.put("result", "API Key or API SECRET not Valid");
			log.info("GET TOKEN ["+msisdn+"]["+resMap.get("result")+"]");
			return Utils.convertToJSON(resMap);
		}
		
		if (!Utils.isNullorEmptyString(msisdn)) {
			if(msisdn.substring(0,1).equals("0")){
				msisdn = "62"+msisdn.substring(1,msisdn.length());
			}else if(msisdn.substring(0,1).equals("+")){
				msisdn = msisdn.substring(1,msisdn.length());
			}
		}else{
			resMap.put("status", "-99");
			resMap.put("request_id", null);
			resMap.put("result", "Msisdn Cannot be Null	");
			log.info("GET TOKEN ["+msisdn+"]["+resMap.get("result")+"]");
			return Utils.convertToJSON(resMap);
		}
		
		String sessid = Utils.generateNumber(4);
		String msg = otpPayproDao.getConfigOTP("sms.otp.message");
		msg = msg.replace(Constant.COUPON_REPLACE_TAG, sessid);
		String resSMS = smsSender.sendUsingMMD(msg, msisdn);
		if(Utils.isNullorEmptyString(resSMS)){
			resMap.put("status", "-98");
			resMap.put("request_id", null);
			resMap.put("result", "3rd Party Error");
			log.info("GET TOKEN ["+msisdn+"]["+resMap.get("result")+"]");
			return Utils.convertToJSON(resMap);
		}else{
			String reqid = resSMS.split("MessageID=")[1].split(", Recipient=")[0];
			resMap.put("status", "0");
			resMap.put("request_id", resSMS);
			resMap.put("result", "Success");
			log.info("GET TOKEN ["+msisdn+"]["+resMap.get("result")+"]");
			if(otpPayproDao.checkMSISDN(msisdn)!=null){
				otpPayproDao.updateOTP(msisdn, sessid, reqid, resSMS);
			}else{
				OTPPaypro otp = new OTPPaypro();
				otp.setOtp_msisdn(msisdn);
				otp.setOtp_request_id(reqid);
				otp.setOtp_sessid(sessid);
				otp.setOtp_sms_result(resSMS);
				otp.setOtp_date_start(Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss"));
				otp.setOtp_date_end(Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss"));
				otp.setOtp_flag("0");
				otpPayproDao.insertOTP(otp);
			}
			return Utils.convertToJSON(resMap);
		}
	}

	public String verifyToken(Parameter parameter) {
		String reqid = parameter.getKeyValueMap().get("request_id");
		String sessid = parameter.getKeyValueMap().get("code");
		String apikey = parameter.getKeyValueMap().get("api_key");
		String apisecret = parameter.getKeyValueMap().get("api_secret");
		
		HashMap<String, String> resMap = new HashMap<>();
		if (!apikey.equalsIgnoreCase("ab2c3e22") || !apisecret.equalsIgnoreCase("c2f0c5ef1f3792a0")) {
			resMap.put("status", "-97");
			resMap.put("request_id", null);
			resMap.put("result", "API Key or API SECRET not Valid");
			log.info("VERIFY TOKEN ["+reqid+"]["+resMap.get("result")+"]");
			return Utils.convertToJSON(resMap);
		}
		
		if (Utils.isNullorEmptyString(sessid)) {
			resMap.put("status", "-95");
			resMap.put("request_id", null);
			resMap.put("result", "Verification Code Cannot be Null");
			log.info("VERIFY TOKEN ["+reqid+"]["+resMap.get("result")+"]");
			return Utils.convertToJSON(resMap);
		}else{
			String datenow = Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss");
			int menit = otpPayproDao.checkMinute(sessid, reqid, datenow);
			if(menit==0){
				resMap.put("status", "6");
				resMap.put("request_id", null);
				resMap.put("result", "Wrong Verification Code");
				log.info("VERIFY TOKEN ["+reqid+"]["+resMap.get("result")+"]");
				return Utils.convertToJSON(resMap);
			}else{
				if(menit<3){
					otpPayproDao.updateFlagOTP(sessid, datenow);
					resMap.put("status", "0");
					resMap.put("request_id", reqid);
					resMap.put("result", "Success");
					log.info("VERIFY TOKEN ["+reqid+"]["+resMap.get("result")+"]");
					return Utils.convertToJSON(resMap);
				}else{
					otpPayproDao.deleteOTP(reqid, sessid);
					resMap.put("status", "17");
					resMap.put("request_id", null);
					resMap.put("result", "Verification Code Expired");
					log.info("VERIFY TOKEN ["+reqid+"]["+resMap.get("result")+"]");
					return Utils.convertToJSON(resMap);
				}
			}
		}
	}
}
