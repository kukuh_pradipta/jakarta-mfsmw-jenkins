/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 5, 2015 
 * Time       : 11:52:30 AM 
 */
package com.indosat.kloc.mfsmw.processor;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.indosat.kloc.mfsmw.dao.BillerDao;
import com.indosat.kloc.mfsmw.dao.TransactionDao;
import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.model.MBiller;
import com.indosat.kloc.mfsmw.model.MBillerTransaction;
import com.indosat.kloc.mfsmw.model.Prefix;
import com.indosat.kloc.mfsmw.model.PrefixReference;
import com.indosat.kloc.mfsmw.model.Product;
import com.indosat.kloc.mfsmw.pojo.MBillerRequest;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.pojo.Target;
import com.indosat.kloc.mfsmw.processor.biller.BillerProcessor;
import com.indosat.kloc.mfsmw.sender.SmsClientSender;
import com.indosat.kloc.mfsmw.service.FinancialService;
import com.indosat.kloc.mfsmw.soap.BillerSessionHandler;
import com.indosat.kloc.mfsmw.soap.UTIBAHandler;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DESedeEncryption;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.SignatureBuilder;
import com.indosat.kloc.mfsmw.util.TrxHistList;
import com.indosat.kloc.mfsmw.util.Utils;

@Component
public class MultiBillerProcessor {

	private static Log log = LogFactory.getLog(MultiBillerProcessor.class);

	@Autowired
	private DataStore dataStore;
	@Autowired
	private BillerProcessor billerProcessor;
	@Autowired
	private TrxHistList trxHistList;
	@Autowired
	private BillerDao billerDao;
	@Autowired
	private UTIBAHandler utibaHandler;
	@Autowired
	private SmsClientSender smsClientSender;
	@Autowired
	private BillerSessionHandler billerSessionHandler;
	@Autowired
	private TransactionDao transactionDao;
	@Autowired
	private FinancialService financialService;

	@Value("${app.sms.notification.trx.pending}")
	private String pendingNotification;
	@Value("${app.utiba.obill.wallet.initiator}")
	private String obInit;
	@Value("${app.utiba.obill.wallet.pin}")
	private String obPinEnc;

	private String encKey = "0n3b1ll_R3ff3rnC3_K3yMf5";

	public Response inquiry(String initiator, HashMap<String, String> parameterMap) {
		Response response = new Response();
		Product product = new Product();
		long startTime = System.currentTimeMillis();
		String to = parameterMap.remove(Constant.TO_PARAM);
		String amount = parameterMap.remove(Constant.AMOUNT_PARAM);
		String target = parameterMap.remove(Constant.TARGET_PARAM);
		String extRef = parameterMap.remove(Constant.EXT_REF_PARAM);
		String reference = parameterMap.remove(Constant.REFERENCE_PARAM);

		log.info("MBiller Inquiry Request initiator[" + initiator + "] target[" + target + "] to[" + to + "] amount["
				+ amount + "] extRef[" + extRef + "] reference[" + reference + "]");

		if (Utils.isNullorEmptyString(to) || Utils.isNullorEmptyString(target)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		if (target.contains("prefix")) {
			List<PrefixReference> prefixRefList = dataStore.getPrefixRefList();
			for (PrefixReference obj : prefixRefList) {
				String[] prefixArr = obj.getPrefix().split("\\,");
				for (String prefix : prefixArr) {
					// System.out.println("msisdn: "+msisdn+ " prefix.trim():
					// "+prefix.trim());
					if (to.startsWith(prefix.trim()) && obj.getDenom().equals(amount)) {
						reference = obj.getReference();
						break;
					}
				}
			}

			if (Utils.isNullorEmptyString(reference)) {
				response.setStatus(Constant.RC_DENOM_NOTFOUND);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_DENOM_NOTFOUND));
				return response;
			}

			log.debug("msisdn[" + to + "] denom[" + amount + "] reference[" + reference + "]");

		}

		if (!Utils.isNullorEmptyString(reference) && StringUtils.isNumeric(reference)) {
			product = dataStore.getProductByReferenceId(Integer.valueOf(reference));
		} else {
			Target tg = dataStore.getTarget(target);
			if (tg == null) {
				response.setStatus(Constant.RC_TARGET_NOTFOUND);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_TARGET_NOTFOUND));
				return response;
			}
			product = dataStore.getProductByTargetId(tg.getId());
		}

		if (product == null) {
			response.setStatus(Constant.RC_PRODUCT_NOT_FOUND);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_PRODUCT_NOT_FOUND));
			return response;
		}

		String transId = Utils.getTransactionId();
		MBiller mBiller = dataStore.getMbiller(product.getBillerId());
		log.debug("MBiller: " + mBiller.toString());
		log.debug("Product: " + product.toString());
		MBillerRequest request = new MBillerRequest();
		request.setUrl(mBiller.getUrl());
		request.setAmount(amount);
		request.setTransId(transId);
		request.setFunction(mBiller.getFuncInq());
		request.setTo(to);
		request.setInitiator(initiator);
		request.setProductId(product.getProductCode());
		request.setExtRef(extRef);

		response = billerProcessor.process(request, parameterMap);
		long responseTime = System.currentTimeMillis() - startTime;

		// ============= Write MBiller Transaction ===================
		MBillerTransaction trx = new MBillerTransaction();
		trx.setTransId(transId);
		trx.setWalletTransId(null);
		trx.setBillerTransId(response.getExtRef());
		trx.setChannelTransId(extRef);
		trx.setProductId(product.getId());
		trx.setType(Constant.TRANSACTION_TYPE_INQUIRY);
		trx.setInitiator(initiator);
		trx.setDestination(to);
		trx.setDestinationName(response.getName());
		trx.setFee(response.getFee());
		trx.setAmount(response.getStatus() == Constant.RC_SUCCESS
				? String.valueOf(Integer.valueOf(response.getPrice()) - Integer.valueOf(response.getFee())) : "0");
		trx.setPrice(response.getPrice());
		trx.setResponseTime(responseTime);
		trx.setResponseCode(response.getStatus());
		trx.setMsg(response.getMsg());
		// trxHistList.add(trx);
		transactionDao.saveMbillerTrx(trx);
		// =================================================================

		return response;
	}

	public Response pay(String initiator, HashMap<String, String> parameterMap) {

		long startTime = System.currentTimeMillis();
		String currentDate = Utils.getCurrentDate("dd/MM/yy HH:mm");
		Response response = new Response();
		Product product = new Product();
		String inqTransId = parameterMap.get(Constant.TRANSID_PARAM);
		String to = parameterMap.remove(Constant.TO_PARAM);
		String amount = parameterMap.remove(Constant.AMOUNT_PARAM);
		String target = parameterMap.remove(Constant.TARGET_PARAM);
		String extRef = parameterMap.remove(Constant.EXT_REF_PARAM);
		String suppressSMS = parameterMap.remove(Constant.SUPPRESS_SMS_PARAM);
		int channel = dataStore.getApiUser("onebill").getChannelType();
		ApiUser apiUser = new ApiUser();
		apiUser.setChannelType(channel);
		apiUser.setUserId("onebill");

		log.info("MBiller Pay Request initiator[" + initiator + "] inquiryTransId[" + inqTransId + "] target[" + target
				+ "] to[" + to + "] amount[" + amount + "] extRef[" + extRef + "] ");

		MBillerTransaction trx = billerDao.getByTransId(inqTransId);
		if (trx == null) {
			response.setStatus(Constant.RC_TRANSACTION_NOT_FOUND);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_TRANSACTION_NOT_FOUND));
			return response;
		}

		product = dataStore.getProductById(trx.getProductId());
		if (product == null) {
			response.setStatus(Constant.RC_PRODUCT_NOT_FOUND);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_PRODUCT_NOT_FOUND));
			return response;
		}

		Target tg = dataStore.getTarget(target);

		if (target.contains("prefix")) {// airtime nih
			amount = trx.getPrice();// karena confirm beli pulsa, gak ada
									// parameter amount.
			to = trx.getDestination();
		}

		if (!initiator.equals(trx.getInitiator()) || tg.getId() != product.getTargetId()
				|| !to.equals(trx.getDestination()) || !amount.equals(trx.getPrice())) {

			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;

		}

		MBiller mBiller = dataStore.getMbiller(product.getBillerId());
		String transId = Utils.getTransactionId();
		String sessionId = billerSessionHandler.getSession(mBiller.getInitiator());
		// Response utibaResp = utibaHandler.sellNoConfirm(sessionId, initiator,
		// trx.getPrice(), inqTransId, target);
		Response utibaResp = utibaHandler.sellNoConfirmOnebill(sessionId, initiator, trx.getPrice(), inqTransId, target,
				to,apiUser);
		if (utibaResp.getStatus() == Constant.RC_SUCCESS) {
			log.debug("MBiller: " + mBiller.toString());
			log.debug("Product: " + product.toString());
			MBillerRequest request = new MBillerRequest();
			request.setUrl(mBiller.getUrl());
			request.setAmount(trx.getAmount());
			request.setPrice(trx.getPrice());
			request.setTransId(transId);
			request.setFunction(mBiller.getFuncPay());
			request.setTo(to);
			request.setInitiator(initiator);
			request.setProductId(product.getProductCode());
			request.setExtRef(extRef);

			response = billerProcessor.process(request, parameterMap);
			if (response.getStatus() == Constant.RC_SUCCESS) {
				trx.setDetails(response.getDetailTagihan());
				if (!Utils.isNullorEmptyString(suppressSMS) && suppressSMS.equalsIgnoreCase("yes")) {
					// do nothing
				} else {
					smsClientSender.send(product.getSmsNotificationTemplateId(), tg.getDescription(), currentDate,
							utibaResp.getTrxid(), trx,apiUser);
				}
			} else if (response.getStatus() == Constant.RC_BILLER_GENERAL_ERROR) {
				utibaHandler.reversal(sessionId, utibaResp.getTrxid(), trx.getPrice(), initiator,apiUser);
			} else {
				String sms = pendingNotification;
				sms = sms.replace(Constant.TYPE_REPLACE_TAG, tg.getDescription());
				sms = sms.replace(Constant.AMOUNT_REPLACE_TAG, Utils.convertStringtoCurrency(trx.getPrice()));
				sms = sms.replace(Constant.DATE_REPLACE_TAG, currentDate);
				sms = sms.replace(Constant.REFERENCEID_REPLACE_TAG, utibaResp.getTrxid());
				sms = sms.replace(Constant.TO_REPLACE_TAG, trx.getDestination());
				if (!Utils.isNullorEmptyString(suppressSMS) && suppressSMS.equalsIgnoreCase("yes")) {
					// do nothing
				} else {
					smsClientSender.send(sms, initiator);
				}

			}

			response.setTrxid(utibaResp.getTrxid());

		} else if (utibaResp.getStatus() == Constant.RC_SESSION_EXPIRED
				&& utibaResp.getResultNameSpace().equalsIgnoreCase("session")) {
			int result = billerSessionHandler.validateSession(mBiller.getInitiator(), mBiller.getPin());
			if (result == Constant.RC_SUCCESS) {
				parameterMap.put(Constant.TO_PARAM, to);
				parameterMap.put(Constant.AMOUNT_PARAM, amount);
				parameterMap.put(Constant.TARGET_PARAM, target);
				parameterMap.put(Constant.EXT_REF_PARAM, extRef);
				parameterMap.put(Constant.SUPPRESS_SMS_PARAM, suppressSMS);
				response = this.pay(initiator, parameterMap);
			} else {
				response.setStatus(Constant.RC_SESSION_INVALID);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_SESSION_INVALID));
			}
		} else {
			response.setStatus(utibaResp.getStatus());
			response.setMsg(dataStore.getErrorMsg(utibaResp.getStatus()));
			response.setTrxid(utibaResp.getTrxid());
		}

		long responseTime = System.currentTimeMillis() - startTime;

		// ============= Write MBiller Transaction ===================
		MBillerTransaction mbtrx = new MBillerTransaction();
		mbtrx.setTransId(transId);
		mbtrx.setRefTransId(inqTransId);
		mbtrx.setWalletTransId(utibaResp.getTrxid());
		mbtrx.setBillerTransId(response.getExtRef());
		mbtrx.setChannelTransId(extRef);
		mbtrx.setProductId(product.getId());
		mbtrx.setType(Constant.TRANSACTION_TYPE_PAY);
		mbtrx.setInitiator(initiator);
		mbtrx.setDestination(to);
		mbtrx.setDestinationName(trx.getDestinationName());
		mbtrx.setPrice(amount);
		mbtrx.setAmount(trx.getAmount());
		mbtrx.setFee(trx.getFee());
		mbtrx.setResponseTime(responseTime);
		mbtrx.setResponseCode(response.getStatus());
		mbtrx.setDetails(response.getDetailTagihan());
		mbtrx.setMsg(response.getMsg());
		trxHistList.add(mbtrx);
		// =================================================================

		return response;
	}

	public Response inquiryBillpay(String userid, String key, HashMap<String, String> parameterMap) {
		Response response = new Response();
		Product product = new Product();
		long startTime = System.currentTimeMillis();
		Target tg = null;
		String to = parameterMap.get(Constant.TO_PARAM);
		String amount = parameterMap.get(Constant.AMOUNT_PARAM);
		String target = parameterMap.get(Constant.TARGET_PARAM);
		String extRef = parameterMap.get(Constant.EXT_REF_PARAM);
		String reference = parameterMap.get(Constant.REFERENCE_PARAM);
		String initiator = parameterMap.get(Constant.INITIATOR_PARAM);
		Prefix telco;

		log.info("MBiller Inquiry Billpay Request init[" + initiator + "] target[" + target + "] to[" + to + "] amount["
				+ amount + "] extRef[" + extRef + "] reference[" + reference + "]");

		if (Utils.isNullorEmptyString(to) || Utils.isNullorEmptyString(target)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		if (!Utils.isNullorEmptyString(reference) && StringUtils.isNumeric(reference)) {
			log.debug("Get Biller");
			product = dataStore.getProductByProductCode(reference);
		} else {
			tg = dataStore.getTarget(target);
			if (tg == null) {
				response.setStatus(Constant.RC_TARGET_NOTFOUND);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_TARGET_NOTFOUND));
				return response;
			}
			product = dataStore.getProductByTargetId(tg.getId());
		}

		if (product == null) {
			response.setStatus(Constant.RC_PRODUCT_NOT_FOUND);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_PRODUCT_NOT_FOUND));
			return response;
		}

		try {
			DESedeEncryption encryptionKey = new DESedeEncryption(encKey);
			String signature = SignatureBuilder.build(obInit, encryptionKey.decrypt(obPinEnc), key);
			if (tg != null) {
				parameterMap.put("reference", product.getProductCode());
				if (!Utils.isNullorEmptyString(amount)) {
					parameterMap.put("amount", amount);
				} else {
					parameterMap.put("amount", product.getAmount());
				}

			}
			
			if(tg.getType().equalsIgnoreCase("matrix")){
				response = financialService.matrixBillPay(userid, signature, to, product.getProductCode());
//			}else if(Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(to).getName())){
//				telco = dataStore.getTelcoIdByPrefix(to);
//				if(!telco.getName().equalsIgnoreCase("indosat")){
//					response = financialService.creditAirtimeInquiry(userid, signature, to, amount, extRef);
//				}
			}else{
				response = financialService.queryBillPay(userid, signature, target, to, parameterMap);
			}
			log.info("Response:" + response);
		} catch (Exception ex) {
			response.setStatus(Constant.RC_SYSTEM_ERROR);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
			return response;
		}
		long responseTime = System.currentTimeMillis() - startTime;
		// ============= Write MBiller Transaction ===================
		MBillerTransaction trx = new MBillerTransaction();
		trx.setTransId(response.getTrxid());
		trx.setWalletTransId(null);
		trx.setBillerTransId(response.getExtRef());
		trx.setChannelTransId(extRef);
		trx.setProductId(product.getId());
		trx.setType(Constant.TRANSACTION_TYPE_INQUIRY);
		trx.setInitiator(initiator);
		trx.setDestination(to);
		trx.setDestinationName(response.getName());
		if(Utils.isNullorEmptyString(response.getFee())){
			response.setFee("0");
		}
		trx.setFee(response.getFee());
		trx.setAmount(response.getStatus() == Constant.RC_SUCCESS
				? String.valueOf(Integer.valueOf(response.getPrice()) - Integer.valueOf(response.getFee())) : "0");
		trx.setPrice(response.getPrice());
		trx.setResponseTime(responseTime);
		trx.setResponseCode(response.getStatus());
		trx.setMsg(response.getMsg());
		// trxHistList.add(trx);
		transactionDao.saveMbillerTrx(trx);
		// =================================================================
		return response;
	}

	public Response paymentBillpay(String userid, String key, HashMap<String, String> parameterMap) {
		long startTime = System.currentTimeMillis();
		String currentDate = Utils.getCurrentDate("dd/MM/yy HH:mm");
		DESedeEncryption encryptionKey = new DESedeEncryption(encKey);
		Response response = new Response();
		Product product = new Product();
		String inqTransId = parameterMap.get(Constant.TRANSID_PARAM);
		String initiator = parameterMap.get(Constant.INITIATOR_PARAM);
		String to = parameterMap.get(Constant.TO_PARAM);
		String amount = parameterMap.get(Constant.AMOUNT_PARAM);
		String target = parameterMap.get(Constant.TARGET_PARAM);
		String extRef = parameterMap.get(Constant.EXT_REF_PARAM);
		String suppressSMS = parameterMap.get(Constant.SUPPRESS_SMS_PARAM);
		int channel = dataStore.getApiUser("onebill").getChannelType();
		ApiUser apiUser = new ApiUser();
		apiUser.setChannelType(channel);
		apiUser.setUserId("onebill");
		
		Prefix telco;

		log.info("MBiller Pay Request from init[" + initiator + "] InqId[" + inqTransId + "] target[" + target + "] to["
				+ to + "] amount[" + amount + "] extRef[" + extRef + "] ");

		MBillerTransaction trx = billerDao.getByTransId(inqTransId);
		if (trx == null) {
			response.setStatus(Constant.RC_TRANSACTION_NOT_FOUND);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_TRANSACTION_NOT_FOUND));
			return response;
		}

		product = dataStore.getProductById(trx.getProductId());
		if (product == null) {
			response.setStatus(Constant.RC_PRODUCT_NOT_FOUND);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_PRODUCT_NOT_FOUND));
			return response;
		}

		Target tg = dataStore.getTarget(target);

		if (tg.getId() != product.getTargetId() || !to.equals(trx.getDestination()) || !amount.equals(trx.getPrice())) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;

		}

		String transId = Utils.getTransactionId();
		String sessionId = billerSessionHandler.getSession(obInit);
		// Response utibaResp = utibaHandler.sellNoConfirm(sessionId, initiator,
		// trx.getPrice(), inqTransId, target);
		Response sellResp = utibaHandler.sellNoConfirmOnebill(sessionId, initiator, trx.getPrice(), inqTransId, target,
				to,apiUser);
		if (sellResp.getStatus() == Constant.RC_SUCCESS) {
			log.debug("Product: " + product.toString());
			String signature = SignatureBuilder.build(obInit, encryptionKey.decrypt(obPinEnc), key);
			// Re-Inquiry to make sure no billpay timeout from utiba
			Response responseInq = new Response();
			Response reversalRes = new Response();
			try {
				if (tg != null) {
					parameterMap.put("reference", product.getProductCode());
					parameterMap.put("price", trx.getPrice());
					amount = trx.getAmount();
				}
				if(tg.getType().equalsIgnoreCase("matrix")){
					responseInq = financialService.matrixBillPay(userid, signature, to, product.getProductCode());
//				}else if(Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(to).getName())){
//					telco = dataStore.getTelcoIdByPrefix(to);
//					if(!telco.getName().equalsIgnoreCase("indosat")){
//						response = financialService.creditAirtimeInquiry(userid, signature, to, amount, extRef);
//					}
				}else{
					responseInq = financialService.queryBillPay(userid, signature, target, to, parameterMap);
				}
				log.info("Response:" + responseInq);
			} catch (Exception ex) {
				response.setStatus(Constant.RC_SYSTEM_ERROR);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
				return response;
			}
			// if Re-Inquiry Failed
			if (responseInq.getStatus() != Constant.RC_SUCCESS) {
				reversalRes = utibaHandler.reversal(sessionId, sellResp.getTrxid(), trx.getPrice(), initiator,apiUser);
				log.info("Reversal Msisdn[" + initiator + "] Status[" + reversalRes.getStatus() + "]");
				response.setStatus(Constant.RC_SYSTEM_ERROR);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
				return response;
			}

			if(tg.getType().equalsIgnoreCase("matrix")){
				response = financialService.confirm(userid, signature, Integer.parseInt(responseInq.getTrxid()));
//			}else if(Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(to).getName())){
//				telco = dataStore.getTelcoIdByPrefix(to);
//				if(!telco.getName().equalsIgnoreCase("indosat")){
//					response = financialService.commitCreditAirtime(userid, signature, Integer.valueOf(responseInq.getTrxid()), transId, telco.getName());
//				}
			}else{
				response = financialService.billPay(userid, signature, target, to, amount, transId, parameterMap);
			}
			if (response.getStatus() == Constant.RC_SUCCESS) {
				transId = response.getTrxid();
				trx.setDetails(response.toString());
//				if (!Utils.isNullorEmptyString(suppressSMS) && suppressSMS.equalsIgnoreCase("yes")) {
//					// do nothing
//				} else {
					if (product.getPriority() == 1) {
						String smsMsg = dataStore.getSMSNotificationTemplate(product.getSmsNotificationTemplateId());
						Response resDetailTrx = financialService.checkTransactionById(userid, signature, response.getTrxid());
						log.debug(resDetailTrx.toString());
						smsMsg = smsMsg.replace(Constant.DETAILS_REPLACE_TAG, resDetailTrx.getVoucherCode());
						smsMsg = smsMsg.replace(Constant.BALANCE_REPLACE_TAG, Utils.convertStringtoCurrency(utibaHandler.getAgentByReferenceRequest(trx.getInitiator(),apiUser).getBalance()));
						smsMsg = smsMsg.replace(Constant.REFERENCEID_REPLACE_TAG, response.getTrxid());
						smsClientSender.send(smsMsg, initiator);
					} else {
						smsClientSender.send(product.getSmsNotificationTemplateId(), tg.getDescription(), currentDate,
								sellResp.getTrxid(), trx,apiUser);
					}
//				}
			} else if (response.getStatus() == Constant.RC_BILLER_GENERAL_ERROR) {
				utibaHandler.reversal(sessionId, sellResp.getTrxid(), trx.getPrice(), initiator,apiUser);
				log.info("Reversal Msisdn[" + initiator + "] Status[" + reversalRes.getStatus() + "]");
			} else {
				String sms = pendingNotification;
				sms = sms.replace(Constant.TYPE_REPLACE_TAG, tg.getDescription());
				sms = sms.replace(Constant.AMOUNT_REPLACE_TAG, Utils.convertStringtoCurrency(trx.getPrice()));
				sms = sms.replace(Constant.DATE_REPLACE_TAG, currentDate);
				sms = sms.replace(Constant.REFERENCEID_REPLACE_TAG, sellResp.getTrxid());
				sms = sms.replace(Constant.TO_REPLACE_TAG, trx.getDestination());
				if (!Utils.isNullorEmptyString(suppressSMS) && suppressSMS.equalsIgnoreCase("yes")) {
					// do nothing
				} else {
					smsClientSender.send(sms, initiator);
				}
			}
			response.setTrxid(sellResp.getTrxid());
		} else if (sellResp.getStatus() == Constant.RC_SESSION_EXPIRED
				&& sellResp.getResultNameSpace().equalsIgnoreCase("session")) {
			int result = billerSessionHandler.validateSession(obInit, encryptionKey.decrypt(obPinEnc));
			if (result == Constant.RC_SUCCESS) {
				parameterMap.put(Constant.TO_PARAM, to);
				parameterMap.put(Constant.AMOUNT_PARAM, amount);
				parameterMap.put(Constant.TARGET_PARAM, target);
				parameterMap.put(Constant.EXT_REF_PARAM, extRef);
				parameterMap.put(Constant.SUPPRESS_SMS_PARAM, suppressSMS);
				response = this.pay(initiator, parameterMap);
			} else {
				response.setStatus(Constant.RC_SESSION_INVALID);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_SESSION_INVALID));
			}
		} else {
			response.setStatus(sellResp.getStatus());
			response.setMsg(dataStore.getErrorMsg(sellResp.getStatus()));
			response.setTrxid(sellResp.getTrxid());
		}
		long responseTime = System.currentTimeMillis() - startTime;

		// ============= Write MBiller Transaction ===================
		MBillerTransaction mbtrx = new MBillerTransaction();
		mbtrx.setTransId(transId);
		mbtrx.setRefTransId(inqTransId);
		mbtrx.setWalletTransId(sellResp.getTrxid());
		mbtrx.setBillerTransId(response.getExtRef());
		mbtrx.setChannelTransId(extRef);
		mbtrx.setProductId(product.getId());
		mbtrx.setType(Constant.TRANSACTION_TYPE_PAY);
		mbtrx.setInitiator(initiator);
		mbtrx.setDestination(to);
		mbtrx.setDestinationName(trx.getDestinationName());
		mbtrx.setPrice(amount);
		mbtrx.setAmount(trx.getAmount());
		mbtrx.setFee(trx.getFee());
		mbtrx.setResponseTime(responseTime);
		mbtrx.setResponseCode(response.getStatus());
		mbtrx.setDetails(response.getDetailTagihan());
		mbtrx.setMsg(response.getMsg());
		trxHistList.add(mbtrx);
		// =================================================================
		return response;

	}

}
