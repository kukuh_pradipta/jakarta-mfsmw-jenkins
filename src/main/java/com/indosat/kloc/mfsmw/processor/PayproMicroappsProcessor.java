/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 23, 2015 
 * Time       : 11:46:57 AM 
 */
package com.indosat.kloc.mfsmw.processor;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.google.gson.JsonObject;
import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.service.FinancialServiceImpl;
import com.indosat.kloc.mfsmw.util.AsymmetricCryptography;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DESedeEncryption;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;

@Service
public class PayproMicroappsProcessor {

	private static Log log = LogFactory.getLog(PayproMicroappsProcessor.class);

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private HttpClientSender httpSender;
	@Autowired
	private DataStore dataStore;
	
	@Value("${app.microscript.url}")
	private String msUrl;
//	@Value("${app.qr.privpath}")
	private String privPath="/WEB-INF/keypair/privateKey";
//	@Value("${app.qroffline.key.enc}")
//	private String keyEnc;
//	private String saltQROffline = "qr0fFl1n3PerT@MiN4P4yPr0";
	
	public Response voucherInquiry(String signature,String userid, String product, String extRef){
		Response res = new Response();
		JsonObject detailTrxJson;
		String url = msUrl+"voucherEngine";
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		HashMap<String,String> detailTrx = null;
		acceptableMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		parameters.add("function", "inqVoucher");
		parameters.add(Constant.SIGNATURE_PARAM, signature);
		parameters.add(Constant.USERID_PARAM, userid);
		parameters.add("key", dataStore.getApiUser(userid).getPassKey());
		parameters.add(Constant.REFERENCE_PARAM, product);
		parameters.add(Constant.EXT_REF_PARAM, extRef);
		
		ResponseEntity<String> responseHttp = httpSender.sendPost(url, parameters, acceptableMediaTypes);
		String resString = responseHttp.getBody();

		//MAPPING RESULT
		res.setStatus(Integer.parseInt(Utils.getJSONValue(resString, "status")));
		res.setMsg(Utils.getJSONValue(resString, "msg"));
		if(Utils.getJsonData(resString).get("amount")!=null)
			res.setAmount(Utils.getJsonData(resString).get("amount").getAsString());
		if(Utils.getJsonData(resString).get("detailTrx")!=null){
			detailTrx = new HashMap<String,String>();
			detailTrxJson = Utils.getJsonData(resString).get("detailTrx").getAsJsonObject();
			HashMap<String,String> jsonIterate = Utils.iterateJSONtoMap(detailTrxJson);
			Iterator it = jsonIterate.entrySet().iterator();
		    while (it.hasNext()) {
		    	Map.Entry pair = (Map.Entry)it.next();
		    	detailTrx.put(String.valueOf(pair.getKey()),String.valueOf(pair.getValue()));
		        it.remove(); // avoids a ConcurrentModificationException
		    }
		}
		if(Utils.getJsonData(resString).get("trxid")!=null){
			res.setTrxid(Utils.getJSONValue(resString, "trxid"));
		}if(Utils.getJsonData(resString).get("detailTagihan")!=null){
			res.setDetailTagihan(Utils.getJSONValue(resString, "detailTagihan"));
		}
//	    detailTrx.remove("trx_ext_ref");
		res.setDetailTrx(detailTrx);
		return res;
	}
	
	public Response voucherPayment(String signature,String userid, String trxid, String extRef){
		Response res = new Response();
		String url = msUrl+"voucherEngine";
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		parameters.add("function", "payVoucher");
		parameters.add(Constant.SIGNATURE_PARAM, signature);
		parameters.add(Constant.USERID_PARAM, userid);
		parameters.add("key", dataStore.getApiUser(userid).getPassKey());
		parameters.add(Constant.TRXID_PARAM, trxid);
		parameters.add(Constant.EXT_REF_PARAM, extRef);
		
		ResponseEntity<String> responseHttp = httpSender.sendPost(url, parameters, acceptableMediaTypes);
		String resString = responseHttp.getBody();

		//MAPPING RESULT
		res.setStatus(Integer.parseInt(Utils.getJSONValue(resString, "status")));
		res.setMsg(Utils.getJSONValue(resString, "msg"));
		if(Utils.getJsonData(resString).get("trxid")!=null){
			res.setTrxid(Utils.getJSONValue(resString, "trxid"));
		}
		if(Utils.getJsonData(resString).get("voucherCode")!=null){
			res.setVoucherCode(Utils.getJSONValue(resString, "voucherCode"));
		}
		HashMap<String,String> det = new HashMap<>();
		if(Utils.getJsonData(resString).get("detailTrx").getAsJsonObject().get("voucherType")!=null){
			det.put("voucherType", Utils.getJsonData(resString).get("detailTrx").getAsJsonObject().get("voucherType").getAsString());
		}
		if(Utils.getJsonData(resString).get("detailTrx").getAsJsonObject().get("url")!=null){
			det.put("url", Utils.getJsonData(resString).get("detailTrx").getAsJsonObject().get("url").getAsString());
		}
		res.setDetailTrx(det);
		return res;
	}
	
	public Response qrInquiry(String signature,String userid, String qrString,String amount){
		Response res = new Response();
		JsonObject detailTrxJson;
		String url = msUrl+"qrEngine";
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		HashMap<String,String> detailTrx = null;
		acceptableMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		parameters.add("function", "inqQr");
		parameters.add("signature", signature);
		parameters.add("userid", userid);
		parameters.add("key", dataStore.getApiUser(userid).getPassKey());
		parameters.add("qr", qrString);
		if(!Utils.isNullorEmptyString(amount)){
			parameters.add("amount", amount);
		}
		
		ResponseEntity<String> responseHttp = httpSender.sendPost(url, parameters, acceptableMediaTypes);
		String resString = responseHttp.getBody();

		//MAPPING RESULT
		res.setStatus(Integer.parseInt(Utils.getJSONValue(resString, "status")));
		res.setMsg(Utils.getJSONValue(resString, "msg"));
		if(Utils.getJsonData(resString).get("amount")!=null)
			res.setAmount(Utils.getJsonData(resString).get("amount").getAsString());
		if(Utils.getJsonData(resString).get("detailTrx")!=null){
			detailTrx = new HashMap<String,String>();
			detailTrxJson = Utils.getJsonData(resString).get("detailTrx").getAsJsonObject();
			HashMap<String,String> jsonIterate = Utils.iterateJSONtoMap(detailTrxJson);
			Iterator it = jsonIterate.entrySet().iterator();
		    while (it.hasNext()) {
		    	Map.Entry pair = (Map.Entry)it.next();
		    	detailTrx.put(String.valueOf(pair.getKey()),String.valueOf(pair.getValue()));
		        it.remove(); // avoids a ConcurrentModificationException
		    }
		}
		if(Utils.getJsonData(resString).get("trxid")!=null){
			res.setTrxid(Utils.getJSONValue(resString, "trxid"));
		}
//	    detailTrx.remove("trx_ext_ref");
		res.setDetailTrx(detailTrx);
		return res;
	}
	
	public Response qrPayment(String signature,String userid, String trx_ext_ref){
		Response res = new Response();
		String url = msUrl+"qrEngine";
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		parameters.add("function", "payQr");
		parameters.add("signature", signature);
		parameters.add("userid", userid);
		parameters.add("key", dataStore.getApiUser(userid).getPassKey());
		parameters.add("extRef", trx_ext_ref);
		
		ResponseEntity<String> responseHttp = httpSender.sendPost(url, parameters, acceptableMediaTypes);
		String resString = responseHttp.getBody();

		//MAPPING RESULT
		res.setStatus(Integer.parseInt(Utils.getJSONValue(resString, "status")));
		res.setMsg(Utils.getJSONValue(resString, "msg"));
		if(Utils.getJsonData(resString).get("trxid")!=null){
			res.setTrxid(Utils.getJSONValue(resString, "trxid"));
		}
		return res;
	}
	
	public Response qrCheck(String qr){
		Response res = new Response();
		String url = msUrl+"qrEngine";
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		parameters.add("function", "checkQr");
		parameters.add("qr", qr);
		
		ResponseEntity<String> responseHttp = httpSender.sendPost(url, parameters, acceptableMediaTypes);
		String resString = responseHttp.getBody();

		//MAPPING RESULT
		res.setStatus(Integer.parseInt(Utils.getJSONValue(resString, "status")));
		res.setMsg(Utils.getJSONValue(resString, "msg"));
		res.setAmount(Utils.getJSONValue(resString, "amount"));
		if(Utils.getJsonData(resString).get("name")!=null){
			res.setName(Utils.getJSONValue(resString, "name"));	
		}
		
		String detailTrx = Utils.getJSONobject(resString, "detailTrx");
		if(!Utils.isNullorEmptyString(detailTrx))
			res.setDetailTrx(Utils.stringToHashMap(detailTrx));
		
		return res;
	}
	
	public Boolean qrOfflineCheck(String amount, String msisdn, String timestamp, String digest){
		log.debug("Check qrOffline Valid ["+msisdn+"]");
		Boolean res = false;
//		DESedeEncryption crypt = new DESedeEncryption(saltQROffline);
		//08551436505**1000**1506654789**36505
		String digestCreate = DigestUtils.sha1Hex(msisdn+"**"+amount+"**"+timestamp+"**"+msisdn.substring(msisdn.length()-5));
		if(digest.equals(digestCreate)){
			res = true;
		}
		return res;
	}
	
	public Map<String,String> qrOfflineParser(String digest) throws Exception{
		log.debug("Check qrOffline");
		Map<String,String> res = null;
		AsymmetricCryptography crypt = new AsymmetricCryptography();
//		DESedeEncryption crypt = new DESedeEncryption(saltQROffline);
		//08551436505**1000**123456**1506654789**36505
		//User Account Number(MSISDN)**transaction amount**USERPIN**epoch time**salt
		Resource resource = applicationContext.getResource(privPath);
		PrivateKey privateKey = crypt.getPrivate(resource.getFile());
		String d = crypt.decryptText(digest, privateKey);
		if(!Utils.isNullorEmptyString(d)){
			res = new HashMap<String, String>();
			res.put("msisdn", d.split("\\*\\*")[0]);
			res.put("amount", d.split("\\*\\*")[1]);
			res.put("secret", d.split("\\*\\*")[2]);
			res.put("time", d.split("\\*\\*")[3]);
			res.put("salt", d.split("\\*\\*")[4]);
		}
		return res;
	}
}
