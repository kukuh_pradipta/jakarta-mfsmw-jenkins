/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jan 12, 2015 
 * Time       : 9:45:47 AM 
 */
package com.indosat.kloc.mfsmw.processor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.indosat.kloc.mfsmw.dao.OTPCounterDao;
import com.indosat.kloc.mfsmw.util.Constant;

public class OTPWriteHandler implements Runnable{

	private static Log log = LogFactory.getLog(OTPWriteHandler.class);
	
	private OTPCounterDao counterDao;
	private String username;
	private int operationType;
	private int counter;
	

	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("OTPWriteHandler: operation["+operationType+"] username["+username+"] counter["+counter+"]");
		if(operationType == Constant.OPERATION_TYPE_INSERT){
			counterDao.save(username);
		}else if(operationType == Constant.OPERATION_TYPE_UPDATE){
			counterDao.updateCounter(username, counter);
		}
		
	}


	public void setCounterDao(OTPCounterDao counterDao) {
		this.counterDao = counterDao;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public void setOperationType(int operationType) {
		this.operationType = operationType;
	}


	public void setCounter(int counter) {
		this.counter = counter;
	}
	
	

}
