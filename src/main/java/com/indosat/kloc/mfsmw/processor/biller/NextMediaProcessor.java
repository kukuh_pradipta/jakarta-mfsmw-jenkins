/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Sep 15, 2014 
 * Time       : 2:25:19 PM 
 */
package com.indosat.kloc.mfsmw.processor.biller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.TrxHistList;
import com.indosat.kloc.mfsmw.util.Utils;

@Service
public class NextMediaProcessor {
	
	private static Log log = LogFactory.getLog(NextMediaProcessor.class);
	
	@Autowired
	private HttpClientSender httpClientSender;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private TrxHistList histList;
	
	@Value("${app.biller.url.nextmedia}")
	private String url;
	@Value("${app.biller.response.failed}")
	private String xmlResponseFailed;
	
	@Value("${app.biller.response.nextmedia.inquiry}")
	private String inqResp;
	
	public String inquiry(HttpServletRequest request){
		
		String function = request.getParameter(Constant.FUNCTION_PARAM);
		String transactionId = request.getParameter(Constant.TRANSACTIONID_PARAM);
		String reference = request.getParameter(Constant.REFERENCE_PARAM);
		String xmlResult=xmlResponseFailed;
		
		log.info("Incoming request NextMedia Inquiry: transactionId:"+transactionId+" reference:"+reference);
		
		if(Utils.isNullorEmptyString(reference) || Utils.isNullorEmptyString(transactionId)){
			xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
			xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId==null?"":transactionId);
			xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Missing parameters");
			xmlResult = "<openNexMediaInqResponse>"+xmlResult+"</openNexMediaInqResponse>";
		}else{
			StringBuilder urlBuilder = new StringBuilder(url).append("?");
			urlBuilder.append("c=1&").append("id="+reference);
			ResponseEntity<String> re = httpClientSender.sendGet(urlBuilder.toString());
			if(re.getStatusCode().value() == 200){
				String nextMediaResult = re.getBody();
				if(Utils.getXmlTagValue(nextMediaResult, "<response>") != null && 
						Utils.getXmlTagValue(nextMediaResult, "<response>").equals("00")){
					/*<openNexMediaInqResponse>
					 * <ResultCode>00</ResultCode><IsValid>TRUE</IsValid>
					 * <TransactionId>[%TRANSACTIONID%]</TransactionId><ErrorMessage>Sukses</ErrorMessage>
					 * <CustomerMessage>Sukses</CustomerMessage><extraParameters>
					 * <keyValuePair><key>amount</key><value>[%AMOUNT%]</value></keyValuePair><keyValuePair><key>admin</key><value>[%FEE%]</value></keyValuePair><keyValuePair><key>namapelanggan</key><value>[%NAME%]</value></keyValuePair><keyValuePair><key>idpelanggan</key><value>nexmedia:[%REFERENCE%]</value></keyValuePair><keyValuePair><key>jumlahbulan</key><value>1</value></keyValuePair><keyValuePair><key>bulantahun</key><value>[%DATE%]</value></keyValuePair><keyValuePair><key>price</key><value>[%PRICE%]</value></keyValuePair></extraParameters></openNexMediaInqResponse>*/
					xmlResult = inqResp;
					xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId);
					
				}else{
					xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
					xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId==null?"":transactionId);
					xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "DATA TIDAK DITEMUKAN");
					xmlResult = "<openNexMediaInqResponse>"+xmlResult+"</openNexMediaInqResponse>";
				}
				
			}else{
				xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
				xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId==null?"":transactionId);
				xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Connection Problem");
				xmlResult = "<openNexMediaInqResponse>"+xmlResult+"</openNexMediaInqResponse>";
			}
		}
		
		
		
		
		return xmlResult;
	}
	
	public String payment(){
		String xmlResult=xmlResponseFailed;
		
		return xmlResult;
	}
}
