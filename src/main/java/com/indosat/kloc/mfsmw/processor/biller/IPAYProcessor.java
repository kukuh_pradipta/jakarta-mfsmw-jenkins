/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Sep 9, 2014 
 * Time       : 10:30:22 AM 
 */
package com.indosat.kloc.mfsmw.processor.biller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.TrxHistList;
import com.indosat.kloc.mfsmw.util.Utils;

@Service
public class IPAYProcessor {

	private static Log log = LogFactory.getLog(IPAYProcessor.class);

	@Autowired
	private HttpClientSender httpClientSender;
	@Autowired
	private TrxHistList histList;
	
	@Value("${app.biller.url.ipay}")
	private String gameBillerURL;
	
	@Value("${app.biller.response.failed}")
	private String xmlResponseFailed;
	@Value("${app.biller.response.game.success}")
	private String xmlResponseGameSuccess;
	
	public String inquiry(HttpServletRequest request){
		
		String function = request.getParameter(Constant.FUNCTION_PARAM);
		String reference = request.getParameter(Constant.REFERENCE_PARAM);
		String amount = request.getParameter(Constant.AMOUNT_PARAM);
		String transactionId = request.getParameter(Constant.TRANSACTIONID_PARAM);
		String gamevoucher = request.getParameter(Constant.GAME_VOUCHER_PARAM);
		String msisdn = request.getParameter(Constant.MSISDN_PARAM);
		String xmlResult = "";
		
		log.info("Incoming request IPAY Inquiry: transactionId:"+transactionId+" denom:"+amount+" reference:"+reference + " gamevoucher:"+gamevoucher);
		
		if(Utils.isNullorEmptyString(reference) || Utils.isNullorEmptyString(amount)
				|| Utils.isNullorEmptyString(transactionId) || Utils.isNullorEmptyString(gamevoucher)){
			xmlResult = xmlResponseFailed;
			xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
			xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId==null?"":transactionId);
			xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Missing parameters");
			xmlResult = "<isGameVoucherValidResponse>"+xmlResult+"</isGameVoucherValidResponse>";
		}else{
			String apiURL = gameBillerURL;
			apiURL = apiURL.replace(Constant.REFERENCE_REPLACE_TAG, reference);
			apiURL = apiURL.replace(Constant.AMOUNT_REPLACE_TAG, amount);
			
			ResponseEntity<String> re = httpClientSender.sendGet(apiURL);
			String status = Utils.getJSONValue(re.getBody(), "status");
			String price = Utils.getJSONValue(re.getBody(), "price");
			if(status.equalsIgnoreCase("false")){
				xmlResult = xmlResponseFailed;
				xmlResult = xmlResult.replace(Constant.RC_CODE_REPLACE_TAG, "2242");
				xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId==null?"":transactionId);
				xmlResult = xmlResult.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Voucher not found");
				xmlResult = "<isGameVoucherValidResponse>"+xmlResult+"</isGameVoucherValidResponse>";
			}else{
				xmlResult = xmlResponseGameSuccess;
				xmlResult = xmlResult.replace(Constant.TRANSACTIONID_REPLACE_TAG, transactionId);
				xmlResult = xmlResult.replace(Constant.AMOUNT_REPLACE_TAG, amount);
				xmlResult = xmlResult.replace(Constant.PRICE_REPLACE_TAG, price);
				xmlResult = xmlResult.replace(Constant.GAME_VOUCHER_REPLACE_TAG, gamevoucher);
			}
			
			/*histList.writeBillTrxHist(IPAYProcessor.class.getSimpleName(), function, transactionId, "0", amount, 
					status !=null && status.equalsIgnoreCase("false")?Constant.RC_BILLER_GENERAL_ERROR:Constant.RC_SUCCESS, msisdn);*/
		}
		
		
		
		return xmlResult;
		
	}
	
}
