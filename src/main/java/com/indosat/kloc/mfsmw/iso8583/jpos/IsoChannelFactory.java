/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 31, 2014 
 * Time       : 2:47:44 PM 
 */
package com.indosat.kloc.mfsmw.iso8583.jpos;

import java.io.FileNotFoundException;

import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.channel.NACChannel;
import org.jpos.iso.packager.GenericPackager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class IsoChannelFactory {

	private byte tpdu[] =new byte[5];
	ISOChannel isoChannel;
	
	@Value("${app.iso.server.ip}")
	private String ipAddress;
	@Value("${app.iso.server.port}")
	private Integer port;
	
	
	public ISOChannel getAltoISOChannel() throws ISOException, FileNotFoundException{
		if(isoChannel == null){
			isoChannel = new NACChannel(ipAddress, port, new GenericPackager("packager/iso93ascii.xml"),tpdu);
		}
		
		return isoChannel;
	}
	
}
