/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 31, 2014 
 * Time       : 3:24:50 PM 
 */
package com.indosat.kloc.mfsmw.iso8583.jpos;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.channel.ASCIIChannel;
import org.jpos.iso.packager.GenericPackager;
import org.jpos.util.LogSource;
import org.jpos.util.Logger;
import org.jpos.util.SimpleLogListener;

public class ClientExample {
	
	
	public static void main(String[] args) throws ISOException, IOException {
		
		ISOChannel channel = new ASCIIChannel("localhost", 12345,  new GenericPackager("src/packager/iso93ascii.xml"));	
		
		ISOMsg msg = new ISOMsg();
		msg.setMTI("1800");
		msg.set(3, "123456");
		msg.set(7, new SimpleDateFormat("yyyyMMdd").format(new Date()));
		msg.set(11, "000001");
        msg.set(12, new SimpleDateFormat("HHmmss").format(new Date()));
        msg.set(13, new SimpleDateFormat("MMdd").format(new Date()));
        msg.set(48, "Tutorial ISO 8583 Dengan Java");
        msg.set(70, "001");
		
		Logger logger = new Logger();
		logger.addListener (new SimpleLogListener (System.out));
		((LogSource)channel).setLogger (logger, "sample"+"-channel");
		channel.connect ();
		channel.send (msg);	
		ISOMsg result = channel.receive ();
		channel.disconnect ();
	}

}
