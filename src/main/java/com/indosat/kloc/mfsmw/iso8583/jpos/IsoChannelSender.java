/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 31, 2014 
 * Time       : 2:52:51 PM 
 */
package com.indosat.kloc.mfsmw.iso8583.jpos;

import java.io.IOException;

import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.LogSource;
import org.jpos.util.Logger;
import org.jpos.util.SimpleLogListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IsoChannelSender {

	@Autowired
	private IsoChannelFactory channelFactory;
	
	public ISOMsg send(ISOMsg msg, String merchantName) throws ISOException, IOException{
		
		Logger logger = new Logger();
		logger.addListener (new SimpleLogListener (System.out));
		ISOChannel  channel = channelFactory.getAltoISOChannel();
		((LogSource)channel).setLogger (logger, merchantName+"-channel");
		channel.connect ();
		channel.send (msg);	
		ISOMsg result = channel.receive ();
		channel.disconnect ();
		
		return result;
	}
	
}
