/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 31, 2014 
 * Time       : 1:09:11 PM 
 */
package com.indosat.kloc.mfsmw.iso8583.jpos;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOServer;
import org.jpos.iso.ISOSource;
import org.jpos.iso.ServerChannel;
import org.jpos.iso.channel.ASCIIChannel;
import org.jpos.iso.packager.GenericPackager;

public class AltoJPOSServerHandler implements ISORequestListener {
	
	public static void main(String[] args) throws ISOException {
		
		String hostname="localhost";
		int portNumber = 12345;
		//create ISOPackager
		ISOPackager packager = new GenericPackager("src/packager/iso93ascii.xml");
		//create channel
		ServerChannel channel = new ASCIIChannel(hostname, portNumber, packager);
		//create server
		ISOServer server = new ISOServer(portNumber, channel, null);
        server.addISORequestListener(new AltoJPOSServerHandler());
 
        new Thread(server).start();
 
//        System.out.println("ISO service ready on port: [" + portNumber+"]");
		
	}

	@Override
	public boolean process(ISOSource isoSrc, ISOMsg isoMsg) {
		// TODO Auto-generated method stub
		 try {
//	            System.out.println("Server menerima koneksi dari ["+((BaseChannel)isoSrc).getSocket().getInetAddress().getHostAddress()+"]");
	            if (isoMsg.getMTI().equalsIgnoreCase("800")) {
	                    acceptNetworkMsg(isoSrc, isoMsg);
	            }
	        } catch (IOException ex) {
	            Logger.getLogger(AltoJPOSServerHandler.class.getName()).log(Level.SEVERE, null, ex);
	        } catch (ISOException ex) {
	            Logger.getLogger(AltoJPOSServerHandler.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        return false;
	}
	
	 private void acceptNetworkMsg(ISOSource isoSrc, ISOMsg isoMsg) throws ISOException, IOException {
//	        System.out.println("Accepting Network Management Request");
	        ISOMsg reply = (ISOMsg) isoMsg.clone();
	        reply.setMTI("810");
	        reply.set(39, "00");
	 
	        isoSrc.send(reply);
	    }

}
