/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 8, 2014 
 * Time       : 9:40:23 AM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.RemittanceUser;

public interface RemittanceSchedulerDao {

	public List<RemittanceUser> getAll();
	public int updateUser(RemittanceUser remitUser);
	public int deleteUser(String msisdn);
	public int save(String msisdn);
	
}
