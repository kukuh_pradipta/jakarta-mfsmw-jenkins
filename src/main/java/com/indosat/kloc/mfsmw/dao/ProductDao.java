/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 12, 2015 
 * Time       : 3:11:10 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.Product;

public interface ProductDao {

	public List<Product> getAllActive();
	
}
