/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 23, 2015 
 * Time       : 10:01:29 AM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.KycDao;
import com.indosat.kloc.mfsmw.model.KycRawData;
import com.indosat.kloc.mfsmw.util.SqlQuery;

@Repository
public class KycDaoImpl implements KycDao {

	private static Log log = LogFactory.getLog(KycDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
		
	@Override
	public KycRawData getByMSISDN(String msisdn) {
		try {
			return jdbcTemplate.query(SqlQuery.KYC_RAW_DATA_GET_ALL, BeanPropertyRowMapper.newInstance(KycRawData.class)).get(0);
		} catch (DataAccessException e) {
			// TODO: handle exception
			log.error("Error getMSISDN("+msisdn+"):" + e);
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void save(KycRawData data) {
		// TODO Auto-generated method stub
		try {
			//msisdn, name, idno, motherMaidenName, gender
			jdbcTemplate.update(SqlQuery.KYC_DATA_SAVE, data.getMsisdn(), data.getName(), 
					data.getIdNo(), data.getDob(), data.getMotherMaidenName(), data.getGender()); 
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("error save: "+data.toString()+"\n"+e);
		}
	}
	

}
