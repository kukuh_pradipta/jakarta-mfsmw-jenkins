/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jan 10, 2015 
 * Time       : 9:52:52 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.OTPCounterDao;
import com.indosat.kloc.mfsmw.model.OTPCounter;
import com.indosat.kloc.mfsmw.util.Constant;

@Repository
public class OTPCounterDaoImpl implements OTPCounterDao {

	private static Log log = LogFactory.getLog(OTPCounterDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public int getCounter(String username) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.queryForInt(Constant.OTP_COUNTER_GET, username);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
			return -1;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
			return Constant.RC_DB_ERROR;
		}
	}

	@Override
	public void save(String username) {
		// TODO Auto-generated method stub
		try {
			jdbcTemplate.update(Constant.OTP_COUNTER_SAVE, username);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
		
	}

	@Override
	public void removeAll() {
		// TODO Auto-generated method stub
		try {
			jdbcTemplate.update(Constant.OTP_COUNTER_DELETE);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
	}

	@Override
	public void updateCounter(String username, int counter) {
		// TODO Auto-generated method stub
		try {
			jdbcTemplate.update(Constant.OTP_COUNTER_UPDATE, counter, username);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
	}

	@Override
	public List<OTPCounter> getAll() {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(Constant.OTP_COUNTER_GET_ALL,  BeanPropertyRowMapper.newInstance(OTPCounter.class));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.equals(e);
		}
		return null;
	}

}
