/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 24, 2015 
 * Time       : 6:47:03 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.SmsNotificationTemplate;

public interface NotificationDao {

	public List<SmsNotificationTemplate> getAll();
	public List<SmsNotificationTemplate> getAllByTitle(String title);
	
}
