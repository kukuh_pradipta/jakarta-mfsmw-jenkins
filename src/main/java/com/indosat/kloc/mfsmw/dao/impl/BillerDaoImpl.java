/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 19, 2014 
 * Time       : 2:46:47 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.BillerDao;
import com.indosat.kloc.mfsmw.model.Biller;
import com.indosat.kloc.mfsmw.model.MBiller;
import com.indosat.kloc.mfsmw.model.MBillerTransaction;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.SqlQuery;
@Repository
public class BillerDaoImpl implements BillerDao{
	
	private static Log log = LogFactory.getLog(BillerDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Biller> getAll(){
		try {
			return jdbcTemplate.query(Constant.BILLER_GET_ALL, BeanPropertyRowMapper.newInstance(Biller.class));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public List<MBiller> getAllMBiller() {
		
		try {
			return jdbcTemplate.query(Constant.MBILLER_GET_ALL, BeanPropertyRowMapper.newInstance(MBiller.class));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public MBillerTransaction getByTransId(String transId) {
		// TODO Auto-generated method stub
		try {
			StringBuilder sqlBuilder = new StringBuilder(SqlQuery.M_BILLER_TRANSACTION_GET_ALL);
			return jdbcTemplate.queryForObject(sqlBuilder.append(" AND transId=?").toString(), 
					BeanPropertyRowMapper.newInstance(MBillerTransaction.class), transId);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Error Found with transId["+transId+"] \n"+e);
		}
		return null;
	}
	
}
