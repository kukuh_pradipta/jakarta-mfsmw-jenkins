/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Feb 16, 2015 
 * Time       : 2:37:46 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.Photo;
import com.indosat.kloc.mfsmw.model.WhitelabelSequence;

public interface WhitelabelSequenceDao {

	public int insert(WhitelabelSequence obj);
	public List<WhitelabelSequence> getWhitelabelSequenceAll();
	public List<WhitelabelSequence> getWhitelabelSequenceSpesific(String partMsisdn,String partnerCode);
	public List<WhitelabelSequence> getWhitelabelSequenceSpesificMsisdn(String msisdn,String partnerCode);
	
}
