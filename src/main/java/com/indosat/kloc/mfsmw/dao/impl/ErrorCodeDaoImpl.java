/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 1, 2014 
 * Time       : 10:53:34 AM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.ErrorCodeDao;
import com.indosat.kloc.mfsmw.model.ErrorCode;
import com.indosat.kloc.mfsmw.model.IndosmartErrorCode;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.SqlQuery;

@Repository
public class ErrorCodeDaoImpl implements ErrorCodeDao {

	private static Log log = LogFactory.getLog(ErrorCodeDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<IndosmartErrorCode> getAll() {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(Constant.INDOSMART_ERROR_CODE_GET_ALL, BeanPropertyRowMapper.newInstance(IndosmartErrorCode.class));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public List<ErrorCode> getList() {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(SqlQuery.ERROR_CODE_GET_ALL, BeanPropertyRowMapper.newInstance(ErrorCode.class));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		return null;
	}

}
