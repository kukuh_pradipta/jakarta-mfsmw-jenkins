/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 22, 2018 
 * Time       : 2:59:02 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.ReferralTypeDao;
import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.model.ReferralType;
import com.indosat.kloc.mfsmw.util.Constant;

@Repository
public class ReferralTypeDaoImpl implements ReferralTypeDao {


	private static Log log = LogFactory.getLog(ApiUserDaoImpl.class);

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	
	@Override
	public List<ReferralType> getAllReferralType() {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(Constant.REFERALL_TYPE_GET_ALL,
					BeanPropertyRowMapper.newInstance(ReferralType.class));
		} catch (DataAccessException e) {
			// TODO: handle exception
			log.error("Error :" + e);
			e.printStackTrace();
			return null;
		}
	}

}
