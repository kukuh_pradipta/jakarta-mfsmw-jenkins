/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 6, 2014 
 * Time       : 12:12:42 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.Prefix;
import com.indosat.kloc.mfsmw.model.PrefixReference;

public interface PrefixDao {

	public List<Prefix> getAll();
	
	public List<PrefixReference> getAllPrefixReference();
	
}
