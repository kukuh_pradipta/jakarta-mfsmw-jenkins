/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Feb 16, 2015 
 * Time       : 2:37:46 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import com.indosat.kloc.mfsmw.model.Photo;

public interface PhotoDao {

	public void save(Photo photo);
	public Photo get(String username);
	public void update(Photo photo);
	
}
