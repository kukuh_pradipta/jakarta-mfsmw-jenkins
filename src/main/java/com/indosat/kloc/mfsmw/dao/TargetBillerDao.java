/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 5, 2015 
 * Time       : 12:33:59 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.pojo.Target;

public interface TargetBillerDao {

	public List<Target> getAll();
}
