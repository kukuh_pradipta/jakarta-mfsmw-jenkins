/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : May 12, 2015 
 * Time       : 11:35:32 AM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.SMSOutboxDao;
import com.indosat.kloc.mfsmw.model.SMSOutbox;
import com.indosat.kloc.mfsmw.util.SqlQuery;

@Repository
public class SMSOutboxDaoImpl implements SMSOutboxDao{

	private static Log log = LogFactory.getLog(SMSOutboxDaoImpl.class);
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void save(SMSOutbox outbox) {
		// TODO Auto-generated method stub
		try {
			log.debug("Save SMSOutbox: "+outbox.toString());
			jdbcTemplate.update(SqlQuery.SMS_OUTBOX_SAVE, outbox.getTransId(), 
					outbox.getMsisdn(), outbox.getSms(), outbox.getResponseBody(), outbox.getMessageId(),outbox.getStatus());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(outbox.toString()+"\n"+e);
		}
		
	}

	@Override
	public SMSOutbox getByTransId(String transId) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.queryForObject(SqlQuery.SMS_OUTBOX_GET_BY_TRANSID, 
					BeanPropertyRowMapper.newInstance(SMSOutbox.class), transId);
		}catch(EmptyResultDataAccessException e){
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e+"\n TransId"+transId);
		}
		return null;
	}

	@Override
	public void updateCounter(String transId) {
		// TODO Auto-generated method stub
		try {
			jdbcTemplate.update(SqlQuery.SMS_OUTBOX_UPDATE_COUNTER, transId);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e+"\n TransId:"+transId);
		} 
	}

	@Override
	public void saveDR(String messageId, String status) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		try {
				log.debug("Save saveDR: messageId["+messageId+"] status["+status+"]");
				jdbcTemplate.update(SqlQuery.SMS_DR_SAVE, messageId, status);
			} catch (Exception e) {
			// TODO: handle exceptio
				e.printStackTrace();
				log.error(messageId+"\n"+e);
			}
	}

	@Override
	public List<SMSOutbox> getByDate(String startDate, String endDate) {
		// TODO Auto-generated method stub
		StringBuilder sqlBuilder = new StringBuilder(SqlQuery.SMS_OUTBOX_GET);
		sqlBuilder.append(" AND responseBody='000' AND createdTime BETWEEN '").append(startDate).append("' AND '").append(endDate).append("'");
		log.debug("getSMSOUTBOXByDate query: "+sqlBuilder.toString());
		try {
			return jdbcTemplate.query(sqlBuilder.toString(), BeanPropertyRowMapper.newInstance(SMSOutbox.class));
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}

	@Override
	public int checkByDR(String messageId, int status) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.queryForInt(SqlQuery.SMS_DR_GET, messageId, status);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Error found messageId["+messageId+"]");
		}
		return 0;
	}

}
