/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 8, 2014 
 * Time       : 9:40:34 AM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.EmployeeIsatDao;
import com.indosat.kloc.mfsmw.model.EmployeeIsat;
import com.indosat.kloc.mfsmw.util.Constant;

@Repository
public class EmployeeIsatDaoImpl implements EmployeeIsatDao {

	private static Log log = LogFactory.getLog(EmployeeIsatDaoImpl.class);

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<EmployeeIsat> getAll() {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(Constant.EMPLOYEE_GET_ALL, BeanPropertyRowMapper.newInstance(EmployeeIsat.class));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int updateEmployee(EmployeeIsat employeeIsat) {
		// TODO Auto-generated method stub
		try {
			log.error("EMPLOYEE:"+employeeIsat.toString());
			if (employeeIsat.getMainWallet() != null)
				return jdbcTemplate.update(com.indosat.kloc.mfsmw.util.SqlQuery.EMPLOYEE_UPDATE_STATUS,
						employeeIsat.getVirtualAmount(), employeeIsat.getMainAmount(), employeeIsat.getMainWallet());
			else
				return -1;
		} catch (Exception e) {
			log.error("EMPLOYEE:"+employeeIsat.toString());
			// TODO: handle exception
			log.error("Error saving Registartion " + employeeIsat.toString() + " \n" + e);
			e.printStackTrace();
			return -1;
		}

	}

}
