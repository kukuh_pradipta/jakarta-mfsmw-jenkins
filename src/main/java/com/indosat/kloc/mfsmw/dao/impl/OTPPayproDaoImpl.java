/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jan 10, 2015 
 * Time       : 9:52:52 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.OTPPayproDao;
import com.indosat.kloc.mfsmw.model.OTPPaypro;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.Utils;

@Repository
public class OTPPayproDaoImpl implements OTPPayproDao {

	private static Log log = LogFactory.getLog(OTPPayproDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert simpleJdbcInsert;

	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("otp_user_paypro");
	}

	@Override
	public OTPPaypro checkMSISDN(String msisdn) {
		try {
			StringBuilder queryBuilder = new StringBuilder(Constant.OTP_PAYPRO_GET_ALL);
			queryBuilder.append(" WHERE otp_msisdn='"+msisdn+"' ");
			return jdbcTemplate.queryForObject(queryBuilder.toString(),  BeanPropertyRowMapper.newInstance(OTPPaypro.class));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.equals(e);
		}
		return null;
	}
	
	@Override
	public String getConfigOTP(String name) {
		try {
			StringBuilder queryBuilder = new StringBuilder("SELECT * FROM otp_sms_config");
			queryBuilder.append(" WHERE name='"+name+"' ");
			return jdbcTemplate.queryForObject(queryBuilder.toString(),  BeanPropertyRowMapper.newInstance(String.class));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.equals(e);
		}
		return null;
	}

	//SELECT TIMESTAMPDIFF(MINUTE,(SELECT otp_date_start FROM otp_user_paypro WHERE otp_request_id='59225DC5.req' AND otp_sessid='8719'),'2017-05-24 10:29:10') AS menit
	@Override
	public Integer checkMinute(String sessid, String requestid, String datereq) {
		try {
			StringBuilder queryBuilder = new StringBuilder("SELECT TIMESTAMPDIFF(MINUTE,(SELECT otp_date_start FROM otp_user_paypro WHERE otp_request_id='"+requestid+"' AND otp_sessid='"+sessid+"'),'"+datereq+"')");
			return jdbcTemplate.queryForObject(queryBuilder.toString(),  BeanPropertyRowMapper.newInstance(Integer.class));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.equals(e);
		}
		return null;
	}
	
	//INSERT INTO otp_user_paypro VALUES('000000','$msisdn','$sessid','$now','$now',0,'$req_id','$sms_result')
	@Override
	public int insertOTP(OTPPaypro otp) {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>(7);
			parameters.put("otp_msisdn", otp.getOtp_msisdn());
			parameters.put("otp_sessid", otp.getOtp_sessid());
			parameters.put("otp_date_start", otp.getOtp_date_start());
			parameters.put("otp_date_end", otp.getOtp_date_end());
			parameters.put("otp_flag", otp.getOtp_flag());
			parameters.put("otp_request_id", otp.getOtp_request_id());
			parameters.put("otp_sms_result", otp.getOtp_sms_result());
			Number id = simpleJdbcInsert.execute(parameters); //executeAndReturnKey(parameters);
			return id.intValue();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Found Error: "+otp.toString()+"\n"+e);
			return Constant.RC_DB_ERROR;
		}
	}

	//UPDATE otp_user_paypro SET otp_sessid='$sessid', otp_date_start='$now', otp_date_end='$now', otp_request_id='$req_id', otp_sms_result='$sms_result' WHERE otp_msisdn='$msisdn'
	@Override
	public int updateOTP(String msisdn, String sessid, String requestid, String smsresult) {
		try {
			log.info("Update OTP Paypro[" + msisdn + "] requestid[" + requestid + "]");
			return jdbcTemplate.update(Constant.OTP_PAYPRO_UPDATE, sessid,Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss"), Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss"),requestid,smsresult, msisdn);
		} catch (Exception e) {
			log.error("Failed to update OTP Paypro[" + msisdn + "]");
			e.printStackTrace();
		}
		return -99;
	}

	//UPDATE otp_user_paypro SET otp_date_end='$opt_date_end', otp_flag=1 WHERE otp_sessid='$sessid'
	@Override
	public int updateFlagOTP(String sessid, String date_end) {
		try {
			log.info("Flagging OTP Paypro[" + sessid + "] requestid[" + date_end + "]");
			return jdbcTemplate.update(Constant.OTP_PAYPRO_UPDATE_FLAG, date_end,sessid);
		} catch (Exception e) {
			log.error("Failed to update OTP Paypro[" + sessid + "]");
			e.printStackTrace();
		}
		return -99;
	}

	//DELETE FROM otp_user_paypro WHERE otp_request_id='$request_id' AND otp_sessid='$sessid' AND otp_flag=0
	@Override
	public int deleteOTP(String requestid, String sessid) {
		try {
			if (requestid != null&&sessid!=null)
				return jdbcTemplate.update(Constant.OTP_PAYPRO_DELETE,requestid,sessid);
			else
				return -1;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

}
