/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Apr 21, 2015 
 * Time       : 12:00:19 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import com.indosat.kloc.mfsmw.model.RemitCouponInq;

public interface RemitCouponInqDao {

	public int save(RemitCouponInq remitCouponInq);
	
	public RemitCouponInq getByTransId(String transId, String extRef);
	
	public void updateStatus(String transId, String extRef);
	
}
