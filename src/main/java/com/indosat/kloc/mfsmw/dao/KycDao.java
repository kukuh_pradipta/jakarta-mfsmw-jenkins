/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 23, 2015 
 * Time       : 9:48:25 AM 
 */
package com.indosat.kloc.mfsmw.dao;

import com.indosat.kloc.mfsmw.model.KycRawData;

public interface KycDao {

	public KycRawData getByMSISDN(String msisdn);
	public void save(KycRawData data);
	
}
