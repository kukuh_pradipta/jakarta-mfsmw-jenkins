/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jun 3, 2015 
 * Time       : 9:51:56 AM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.RemittancePartner;
import com.indosat.kloc.mfsmw.pojo.RemittanceAgent;

public interface RemittanceAgentDao {

	public List<RemittanceAgent> getAll();
	public List<RemittancePartner> getAllRemittancePartner();
	
}
