/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Apr 29, 2015 
 * Time       : 3:26:33 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.RemittanceUserDao;
import com.indosat.kloc.mfsmw.pojo.RemittanceUser;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.SqlQuery;

@Repository
public class RemittanceUserDaoImpl implements RemittanceUserDao {


	private static Log log = LogFactory.getLog(RemittanceUserDaoImpl.class);
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public int save(RemittanceUser user) {
		// TODO Auto-generated method stub
		try {
			//idNo, idType, idExp, name, phoneNumber, gender, address, pob, dob, occupation, nationality
			return jdbcTemplate.update(SqlQuery.REMIT_USER_SAVE, 
					user.getIdNo(), user.getIdType(), user.getIdExp(), user.getName(), 
					user.getPhoneNumber(), user.getGender(), user.getAddress(), user.getProvince(), user.getCity(), user.getPob(), 
					user.getDob(), user.getOccupation(), user.getNationality(), user.getAggreeReg());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e.toString()+" \n"+e);
		}
		return Constant.RC_DB_ERROR;
	}
	
	@Override
	public int update(RemittanceUser user) {
		//System.out.println("Update RemittanceUser: "+user.toString() );
		log.debug("Update RemittanceUser: "+user.toString());
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.update(SqlQuery.REMIT_USER_UPDATE, user.getIdExp(), user.getPhoneNumber(), user.getGender(),
					user.getAddress(), user.getProvince(), user.getCity(), user.getPob(), user.getDob(),
					user.getOccupation(), user.getNationality(), user.getAggreeReg(), user.getIdType(), user.getIdNo());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e.toString()+" \n"+e);
		}
		return Constant.RC_DB_ERROR;
	}

	@Override
	public RemittanceUser getById(String idType, String idNo) {
		// TODO Auto-generated method stub
		try {
			StringBuilder sqlBuilder = new StringBuilder(SqlQuery.REMIT_USER_GET_ALL).append(" AND idType=? AND idNo=? ");
			return jdbcTemplate.queryForObject(sqlBuilder.toString(), BeanPropertyRowMapper.newInstance(RemittanceUser.class),idType, idNo);
		} catch(EmptyResultDataAccessException e){
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("idType:"+idType+" idNo:"+idNo);
		}
		return null;
	}

}
