/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 15, 2014 
 * Time       : 10:17:04 AM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.ApiUserDao;
import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.Utils;

@Repository
public class ApiUserDaoImpl implements ApiUserDao {

	private static Log log = LogFactory.getLog(ApiUserDaoImpl.class);

	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert insertMerchantUser;

	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.insertMerchantUser = new SimpleJdbcInsert(dataSource).withTableName("api_merchant_users");
	}

	@Override
	public List<ApiUser> getAll() {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(Constant.API_USER_QUERY_GET_ALL,
					BeanPropertyRowMapper.newInstance(ApiUser.class));
		} catch (DataAccessException e) {
			// TODO: handle exception
			log.error("Error :" + e);
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ApiUser> getAllMerchantUser() {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(Constant.API_MERCHANT_USER_QUERY_GET_ALL,
					BeanPropertyRowMapper.newInstance(ApiUser.class));
		} catch (DataAccessException e) {
			// TODO: handle exception
			log.error("Error :" + e);
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int saveNewMerchant(ApiUser user) {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>(18);
			parameters.put("userid", user.getUserId());
			parameters.put("passkey", user.getPassKey());
			parameters.put("active", user.getActive());
			parameters.put("service_type", user.getServiceType());
			parameters.put("response_type", user.getResponseType());
			parameters.put("channel_type", user.getChannelType());
			parameters.put("group_id", user.getGroupId());
			parameters.put("iplist", user.getIpList());
			parameters.put("apilist", user.getApiList());
			Number id = insertMerchantUser.execute(parameters);
			return id.intValue();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Found Error: "+user.toString()+"\n"+e);
			return Constant.RC_DB_ERROR;
		}
	}

}
