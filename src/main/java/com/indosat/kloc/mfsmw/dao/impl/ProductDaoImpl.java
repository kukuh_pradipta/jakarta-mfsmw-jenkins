/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 12, 2015 
 * Time       : 3:12:31 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.ProductDao;
import com.indosat.kloc.mfsmw.model.Product;
import com.indosat.kloc.mfsmw.util.Constant;

@Repository
public class ProductDaoImpl implements ProductDao {

	private static Log log = LogFactory.getLog(ProductDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Product> getAllActive() {
		// TODO Auto-generated method stub
		try {
			StringBuilder sqlBuilder = new StringBuilder(Constant.PRODUCT_GET_ALL).append("AND status="+Constant.PRODUCT_STATUS_ACTIVE);
			return jdbcTemplate.query(sqlBuilder.toString(), BeanPropertyRowMapper.newInstance(Product.class));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}

}
