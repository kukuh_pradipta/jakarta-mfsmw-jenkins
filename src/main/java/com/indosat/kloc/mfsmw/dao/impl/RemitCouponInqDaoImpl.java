/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Apr 21, 2015 
 * Time       : 12:02:56 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.Date;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.RemitCouponInqDao;
import com.indosat.kloc.mfsmw.model.RemitCouponInq;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.SqlQuery;

@Repository
public class RemitCouponInqDaoImpl implements RemitCouponInqDao{

	private static Log log = LogFactory.getLog(RemitCouponInqDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public int save(RemitCouponInq o) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.update(SqlQuery.REMIT_COUPON_INQ_SAVE, o.getSessionId(), o.getTransId(), o.getExtRef(), o.getStoreName(), 
					o.getAmount(), o.getRecipientPhoneNumber(), o.getRecipientName(), o.getRecipientSourceOfFund(), o.getRecipientPurpose(),
					o.getRecipientAddress(), o.getRecipientProvince(), o.getRecipientCity(), o.getSenderIdType(), o.getSenderIdNo(), 
					o.getSenderName(), o.getSenderPhoneNumber(), 0, new Date(), o.getUniqueCode(), o.getNote());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e.toString()+" \n"+e);
		}
		return Constant.RC_DB_ERROR;
	}

	@Override
	public RemitCouponInq getByTransId(String transId, String extRef) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.queryForObject(SqlQuery.REMIT_COUPON_GET_BY_TRANSID_AND_EXTREF, 
					BeanPropertyRowMapper.newInstance(RemitCouponInq.class),transId, extRef);
		} catch(EmptyResultDataAccessException e){
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("transId:"+transId+" extRef:"+extRef);
		}
		return null;
	}

	@Override
	public void updateStatus(String transId, String extRef) {
		// TODO Auto-generated method stub
		try {
			jdbcTemplate.update(SqlQuery.REMIT_COUPON_UPDATE_STATUS,new Date(), transId, extRef);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("transId:"+transId+" extRef:"+extRef);
		}
		
	}

}
