/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jun 2, 2015 
 * Time       : 8:06:10 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import com.indosat.kloc.mfsmw.pojo.TransactionDMT;

public interface TransactionDMTDao {

	public TransactionDMT getByExtRef(String extRef);
	public TransactionDMT getByTransId(String transId);
	public String getInitiatorByTransId(String transId);
	public String getWallet2CashByTransId(String transId);
	
}
