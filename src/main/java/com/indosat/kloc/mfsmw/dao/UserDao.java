/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Nov 24, 2014 
 * Time       : 3:49:47 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import com.indosat.kloc.mfsmw.model.User;

public interface UserDao {

	public String save(User user);
	public User get(String username);
	public void updateStatus(String username, int status);
	public String getUserByAccountID(String accountID);
	
}
