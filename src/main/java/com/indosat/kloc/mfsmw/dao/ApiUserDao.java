/**
 * Project    : USSD Menu Browser Gateway  
 * Created By : Megi Jaka Permana
 * Date       : Aug 15, 2014 
 * Time       : 10:16:37 AM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.model.MBillerTransaction;

public interface ApiUserDao {

	List<ApiUser> getAll() ;
	List<ApiUser> getAllMerchantUser() ;
	public int saveNewMerchant(ApiUser user);
	
}
