/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : May 12, 2015 
 * Time       : 11:34:47 AM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.SMSOutbox;

public interface SMSOutboxDao {

	public void save(SMSOutbox outbox);
	public SMSOutbox getByTransId(String transId);
	public void updateCounter(String transId);
	
	public void saveDR(String messageId, String status);
	
	public List<SMSOutbox> getByDate(String startDate, String endDate);
	
	public int checkByDR(String messageId, int status);
	
}
