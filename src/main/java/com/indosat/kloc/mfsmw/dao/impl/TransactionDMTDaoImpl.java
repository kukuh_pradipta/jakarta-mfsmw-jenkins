/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jun 2, 2015 
 * Time       : 8:06:21 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.TransactionDMTDao;
import com.indosat.kloc.mfsmw.pojo.TransactionDMT;
import com.indosat.kloc.mfsmw.util.SqlQuery;

@Repository
public class TransactionDMTDaoImpl implements TransactionDMTDao{

	private static final Log log = LogFactory.getLog(TransactionDMTDaoImpl.class);

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public TransactionDMT getByExtRef(String extRef) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.queryForObject(SqlQuery.DMT_TRAX_GET_BY_EXTREF, 
					BeanPropertyRowMapper.newInstance(TransactionDMT.class), extRef);
		}catch(EmptyResultDataAccessException e){
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Error Found: extRef["+extRef+"]"+e);
		}
		return null;
	}

	@Override
	public TransactionDMT getByTransId(String transId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInitiatorByTransId(String transId) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.queryForObject(SqlQuery.DMT_GET_INITIATOR_BY_TRANSID, String.class, transId);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
		return "";
	}

	@Override
	public String getWallet2CashByTransId(String transId) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.queryForObject(SqlQuery.DMT_GET_WALLET2CASH_TRX_BY_TRANSID, String.class, transId);
		} catch (Exception e) {
			// TODO: handle exception
//			e.printStackTrace();
			log.error(e);
		}
		return "";
	}

}
