/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Akhilesh
 * Date       : 19-Oct-2017 
 * Time       : 2:15:11 PM 
 */
package com.indosat.kloc.mfsmw.dao;
import com.indosat.kloc.mfsmw.model.Referral;

public interface ReferralDao {
	public String save(Referral referral);
	public String registerWithReferral(String sender, String receiver, String referralcode, String deviceID);
	public Referral getDataByReferralCodeAndDeviceID(String referralCode, String deviceID);
	public Referral getDataByDeviceID(String referralCode, String deviceID);
	public Referral getReceiver(String receiver);
}
