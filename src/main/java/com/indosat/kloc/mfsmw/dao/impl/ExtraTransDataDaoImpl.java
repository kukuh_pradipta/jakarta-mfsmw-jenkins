/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 18, 2014 
 * Time       : 12:05:16 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.ExtraTransDataDao;
import com.indosat.kloc.mfsmw.model.ExtraTransData;
import com.indosat.kloc.mfsmw.util.Constant;

@Repository
public class ExtraTransDataDaoImpl implements ExtraTransDataDao {

	private static Log log = LogFactory.getLog(ExtraTransDataDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<ExtraTransData> getAllByTemplateId(int billerId, int type){
		try {
			String query = Constant.EXT_TRANS_DATA_GET_ALL+" AND billerId=? AND extType=? ";
			return jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(ExtraTransData.class), billerId, type);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		
		return null;
	}

}
