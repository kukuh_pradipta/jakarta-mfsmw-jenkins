/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Akhilesh
 * Date       : 19-Oct-2017 
 * Time       : 2:17:31 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.dao.ReferralDao;
import com.indosat.kloc.mfsmw.model.Referral;
import com.indosat.kloc.mfsmw.model.User;

@Repository
public class ReferralDaoImpl implements ReferralDao {
	private static Log log = LogFactory.getLog(ReferralDaoImpl.class);

	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert simpleJdbcInsert;

	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("referral")
				.usingGeneratedKeyColumns("id");
	}

	@Override
	public String save(Referral referral) {

		// TODO Auto-generated method stub
		log.info("Save referral : " + referral.toString());
		try {
			Map<String, Object> parameters = new HashMap<String, Object>(13);
			parameters.put("sender", referral.getSender());
			parameters.put("receiver", referral.getReceiver());
			parameters.put("referralCode", referral.getReferralCode());
			parameters.put("deviceId", referral.getDeviceID());
			Number id = simpleJdbcInsert.executeAndReturnKey(parameters);
			return id.toString();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}

	@Override
	public Referral getReceiver(String receiver) {
		// TODO Auto-generated method stub
		log.info("get Receiver: " + receiver);
		try {
			return jdbcTemplate.queryForObject(Constant.REFERRAL_GET_RECEIVER,
					BeanPropertyRowMapper.newInstance(Referral.class), receiver);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Referral getDataByReferralCodeAndDeviceID(String referralCode, String deviceID) {
		// TODO Auto-generated method stub
		log.info("get data by referral code: "+referralCode+" and device id: " + deviceID);
		try {
			return jdbcTemplate.queryForObject(Constant.REFERRAL_GET_DATA_BY_DEVICEID_AND_REFERRAL_CODE,
					BeanPropertyRowMapper.newInstance(Referral.class), referralCode, deviceID);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String registerWithReferral(String sender, String receiver, String referralcode, String deviceID) {
		String strResult = "-1";
		log.info("register with referral code: sender[" + sender + "]     receiver[" + receiver + "] referral code["+ referralcode+ "]");
		Referral referral = getReceiver(receiver);
		//make referral become register log of referral
		referral = null;
		if (referral == null) {
			referral = new Referral();
			referral.setSender(sender);
			referral.setReceiver(receiver);
			referral.setReferralCode(referralcode);
			referral.setDeviceID(deviceID);
			strResult = save(referral);
		}
		return strResult;
	}

	@Override
	public Referral getDataByDeviceID(String referralCode, String deviceID) {
		// TODO Auto-generated method stub
		log.info("[getDataByDeviceID] referral code: "+referralCode+" and device id: " + deviceID);
		try {
			return jdbcTemplate.queryForObject(Constant.REFERRAL_GET_DATA_BY_DEVICEID_ONLY,
					BeanPropertyRowMapper.newInstance(Referral.class), deviceID);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
}
