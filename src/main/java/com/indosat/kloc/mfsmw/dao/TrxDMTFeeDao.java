/**
 * Project    : Dompetku - Domestic Money Transfer
 * Created By : Megi Jaka Permana
 * Date       : May 15, 2015 
 * Time       : 6:00:45 PM 
 */
package com.indosat.kloc.mfsmw.dao;

public interface TrxDMTFeeDao {

	public String getFee(String amount);
	public String getFee(String amount, String agent);
	
	
}
