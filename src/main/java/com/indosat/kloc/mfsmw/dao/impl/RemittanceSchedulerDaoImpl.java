/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 8, 2014 
 * Time       : 9:40:34 AM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.RemittanceSchedulerDao;
import com.indosat.kloc.mfsmw.model.RemittanceUser;
import com.indosat.kloc.mfsmw.util.SqlQuery;


@Repository
public class RemittanceSchedulerDaoImpl implements RemittanceSchedulerDao{

	private static Log log = LogFactory.getLog(RemittanceSchedulerDaoImpl.class);

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public List<RemittanceUser> getAll() {
		try {
			return jdbcTemplate.query(SqlQuery.REMITTANCE_GET_ALL, BeanPropertyRowMapper.newInstance(RemittanceUser.class));
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
		}
		return null;
	}


	public int updateUser(RemittanceUser remitUser) {
		try {
			//log.info("EMPLOYEE:"+employeeIsat.toString());
			if (remitUser.getMsisdn() != null)
				return jdbcTemplate.update(SqlQuery.REMITUSER_UPDATE_STATUS,
						remitUser.getSavingAmount(), remitUser.getMainAmount(), remitUser.getMsisdn());
			else
				return -1;
		} catch (Exception e) {
			log.error("REMIT USER:"+remitUser.toString());
			log.error("Error saving Registartion " + remitUser.toString() + " \n" + e);
			e.printStackTrace();
			return -1;
		}
	}

	public int deleteUser(String msisdn) {
		try {
			if (msisdn != null)
				return jdbcTemplate.update(SqlQuery.REMITUSER_DELETE,msisdn);
			else
				return -1;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public int save(String msisdn) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.update(SqlQuery.REMITUSER_INSERT, msisdn);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return -1;
		}
		
	}
}
