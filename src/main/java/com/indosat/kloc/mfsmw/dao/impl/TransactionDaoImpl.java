/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 18, 2014 
 * Time       : 5:19:39 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.TransactionDao;
import com.indosat.kloc.mfsmw.model.BillerTransaction;
import com.indosat.kloc.mfsmw.model.MBillerTransaction;
import com.indosat.kloc.mfsmw.model.Transaction;
import com.indosat.kloc.mfsmw.model.UserWallet;
import com.indosat.kloc.mfsmw.pojo.DMTTransaction;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.SqlQuery;
import com.indosat.kloc.mfsmw.util.Utils;

@Repository
public class TransactionDaoImpl implements TransactionDao {

	private static final Log log = LogFactory.getLog(TransactionDaoImpl.class);

	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert simpleJdbcInsert;

	@Autowired
	public void setDataSource(final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("m_biller_transaction");
	}
	
	@Override
	public int saveMbillerTrx(MBillerTransaction trx){
		try {
			Map<String, Object> parameters = new HashMap<String, Object>(18);
			parameters.put("transId", trx.getTransId());
			parameters.put("refTransId", trx.getRefTransId());
			parameters.put("walletTransId",  trx.getWalletTransId());
			parameters.put("billerTransId", trx.getBillerTransId());
			parameters.put("channelTransId", trx.getChannelTransId());
			parameters.put("productId", trx.getProductId());
			parameters.put("type", trx.getType());
			parameters.put("initiator", trx.getInitiator());
			parameters.put("destination", trx.getDestination());
			parameters.put("destinationName", trx.getDestinationName());
			parameters.put("amount", trx.getAmount());
			parameters.put("price", trx.getPrice());
			parameters.put("fee", trx.getFee());
			parameters.put("details", trx.getDetails());
			parameters.put("msg", trx.getMsg());
			parameters.put("dateTime",  Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss"));
			parameters.put("responseTime", trx.getResponseTime());
			parameters.put("responseCode", trx.getResponseCode());
			Number id = simpleJdbcInsert.execute(parameters); //executeAndReturnKey(parameters);
			return id.intValue();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Found Error: "+trx.toString()+"\n"+e);
			return Constant.RC_DB_ERROR;
		}
	}
	
	@Override
	public BillerTransaction getBillerTransaction(String transId){
		
		try {
			StringBuilder queryBuilder = new StringBuilder(Constant.BILLER_TRANSACTION_GET_ALL);
			queryBuilder.append(" AND function='creditAirtime' ");
			return jdbcTemplate.queryForObject(queryBuilder.toString(),  BeanPropertyRowMapper.newInstance(BillerTransaction.class), transId);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	@Override
	public void saveAll(final List<Transaction> list) {
		// TODO Auto-generated method stub
		try {
			
			this.jdbcTemplate.batchUpdate(Constant.TRANSACTION_INSERT,	new BatchPreparedStatementSetter() {
						@Override
						public int getBatchSize() {
							// TODO Auto-generated method stub
							return list.size();
						}

						@Override
						public void setValues(PreparedStatement ps, int i)
								throws SQLException {
							// TODO Auto-generated method stub
							Transaction trx = list.get(i);
							//date, msisdn, serviceId, serviceType, responseCode, refNo
							int idx = 0;
							
							ps.setInt(++idx, trx.getUserId());
							ps.setInt(++idx, trx.getChannelType());
							ps.setInt(++idx, trx.getServiceType());
							ps.setString(++idx, trx.getInitiator());
							ps.setString(++idx, trx.getExtRef());
							ps.setInt(++idx, trx.getRef());
							ps.setString(++idx, Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss"));
							ps.setInt(++idx, trx.getResponseCode());
							
						}
					});	
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("saveAll Batch Update Error: " + e.getMessage());
		}
	}

	@Override
	public void saveAllBillerTrx(final List<BillerTransaction> list) {
		// TODO Auto-generated method stub
		try {
			this.jdbcTemplate.batchUpdate(Constant.BILLER_TRANSACTION_INSERT,	new BatchPreparedStatementSetter() {
				@Override
				public int getBatchSize() {
					// TODO Auto-generated method stub
					return list.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i)
						throws SQLException {
					// TODO Auto-generated method stub
					BillerTransaction trx = list.get(i);
					//billerName, function, transid, extRef, amount, dateTime, responseCode
					int idx = 0;
					ps.setString(++idx, trx.getBillerName());
					ps.setString(++idx, trx.getFunction());
					ps.setString(++idx, trx.getTransId());
					ps.setString(++idx, trx.getExtRef());
					ps.setString(++idx, trx.getAmount());
					ps.setString(++idx, Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss"));
					ps.setString(++idx, trx.getMsisdn());
					ps.setString(++idx, trx.getInitiator());
					ps.setString(++idx, trx.getResponseCode());
					
				}

			});
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("saveAllBillerTrx Batch Update Error: " + e.getMessage());
		}
	}

	@Override
	public void saveAllMBillerTrx(final List<MBillerTransaction> list) {
		// TODO Auto-generated method stub
		try {
			this.jdbcTemplate.batchUpdate(SqlQuery.M_BILLER_TRANSACTION_SAVE,	new BatchPreparedStatementSetter() {
				@Override
				public int getBatchSize() {
					// TODO Auto-generated method stub
					return list.size();
				}

				@Override
				public void setValues(PreparedStatement ps, int i)
						throws SQLException {
					// TODO Auto-generated method stub
					MBillerTransaction trx = list.get(i);
					int idx = 0;
					ps.setString(++idx, trx.getTransId());
					ps.setString(++idx, trx.getRefTransId());
					ps.setString(++idx, trx.getWalletTransId());
					ps.setString(++idx, trx.getBillerTransId());
					ps.setString(++idx, trx.getChannelTransId());
					ps.setInt(++idx, trx.getProductId());
					ps.setInt(++idx, trx.getType());
					ps.setString(++idx, trx.getInitiator());
					ps.setString(++idx, trx.getDestination());
					ps.setString(++idx, trx.getDestinationName());
					ps.setString(++idx, trx.getAmount());
					ps.setString(++idx, trx.getPrice());
					ps.setString(++idx, trx.getFee());
					ps.setString(++idx, trx.getDetails());
					ps.setString(++idx, trx.getMsg());
					ps.setString(++idx, Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss"));
					ps.setLong(++idx, trx.getResponseTime());
					ps.setInt(++idx, trx.getResponseCode());
				}

			});
		} catch (Exception e) {
			// TODO: handle exception\e.printStackTrace();
			log.error("saveAllMBillerTrx Batch Update Error: " + e.getMessage());
		}
	}

	@Override
	public void saveAllDMTTransaction(final List<DMTTransaction> list) {
		// TODO Auto-generated method stub
		try {
			this.jdbcTemplate.batchUpdate(SqlQuery.REMIT_SAVE_TRANSACTION,new BatchPreparedStatementSetter() {
						@Override
						public int getBatchSize() {
							// TODO Auto-generated method stub
							return list.size();
						}
						@Override
						public void setValues(PreparedStatement ps, int i)
								throws SQLException {
							// TODO Auto-generated method stub
							
							DMTTransaction trx = list.get(i);
							log.info("Saving dmt transaction "+trx.toString());
							int idx = 0;
							ps.setString(++idx, trx.getParentId());
							ps.setString(++idx, trx.getTransId());
							ps.setString(++idx, trx.getExtRef());
							ps.setString(++idx, trx.getInitiator());
							ps.setString(++idx, trx.getCouponId());
							ps.setString(++idx, trx.getStoreName());
							ps.setString(++idx, trx.getRecipientPhoneNumber());
							ps.setString(++idx, trx.getRecipientName());
							ps.setString(++idx, trx.getRecipientSourceOfFund());
							ps.setString(++idx, trx.getRecipientPurpose());
							ps.setString(++idx, trx.getRecipientAddress());
							ps.setString(++idx, trx.getRecipientProvince());
							ps.setString(++idx, trx.getRecipientCity());
							
							ps.setString(++idx, trx.getSenderIdType());
							ps.setString(++idx, trx.getSenderIdNo());
							ps.setString(++idx, trx.getSenderPhoneNumber());
							
							ps.setString(++idx, trx.getAmount());
							ps.setString(++idx, trx.getFee());
							ps.setString(++idx, trx.getTotal());
							ps.setString(++idx, trx.getStatusMessage());
							ps.setInt(++idx, trx.getStatus());
							ps.setString(++idx,Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss"));
							ps.setInt(++idx, trx.getType());
							ps.setString(++idx, trx.getUniqueCode());
							ps.setString(++idx, trx.getNote());
							ps.setLong(++idx, trx.getRespTime());
							
							
						}

					});
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("saveAll Batch Update Error: " + e.getMessage());
		}
		
	}

	@Override
	public void saveAllUserWallet(final List<UserWallet> list) {
		// TODO Auto-generated method stub
		try {
			this.jdbcTemplate.batchUpdate(SqlQuery.USERWALLET_SAVE_TRANSACTION,new BatchPreparedStatementSetter() {
						@Override
						public int getBatchSize() {
							// TODO Auto-generated method stub
							return list.size();
						}
						@Override
						public void setValues(PreparedStatement ps, int i)
								throws SQLException {
							// TODO Auto-generated method stub
							
							UserWallet trx = list.get(i);
							log.info("Saving User Wallet "+trx.toString());
							int idx = 0;
							ps.setString(++idx, trx.getMsisdn());
							ps.setString(++idx, trx.getName());
							ps.setString(++idx, trx.getIdno());
							ps.setString(++idx, trx.getIdtype());
							ps.setString(++idx, trx.getCountrycode());
							ps.setString(++idx, trx.getGender());
							ps.setString(++idx, trx.getDob());
							ps.setString(++idx, trx.getAddress());
							ps.setString(++idx, trx.getWallet_type());
							ps.setString(++idx, trx.getStatus());
							ps.setString(++idx, trx.getEncrypted_text());
							ps.setString(++idx, trx.getEncrypted_code());
							ps.setString(++idx, Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss"));
							ps.setString(++idx, Utils.getCurrentDate("yyyy-MM-dd HH:mm:ss"));
						}

					});
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("saveAll Batch Update Error: " + e.getMessage());
		}
		
	}

}
