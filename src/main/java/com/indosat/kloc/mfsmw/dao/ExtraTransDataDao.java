/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 18, 2014 
 * Time       : 12:02:56 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.ExtraTransData;

public interface ExtraTransDataDao {

	public List<ExtraTransData> getAllByTemplateId(int billerId, int type);
	
}
