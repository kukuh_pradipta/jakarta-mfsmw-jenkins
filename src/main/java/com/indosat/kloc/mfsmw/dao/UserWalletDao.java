/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Nov 24, 2014 
 * Time       : 3:49:47 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.EkycWorkaround;
import com.indosat.kloc.mfsmw.model.RefKecamatan;
import com.indosat.kloc.mfsmw.model.UserWallet;

public interface UserWalletDao {

	public String save(UserWallet user);
	public UserWallet get(String msisdn);
	public List<UserWallet> getList(String msisdn);
	public void updateStatus(UserWallet user);
	//Temp
	public String saveEkycWorkaround(EkycWorkaround user);
	public List<RefKecamatan> getKecamatans();
}
