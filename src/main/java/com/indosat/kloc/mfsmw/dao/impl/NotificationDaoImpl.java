/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 24, 2015 
 * Time       : 6:47:33 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.NotificationDao;
import com.indosat.kloc.mfsmw.model.SmsNotificationTemplate;
import com.indosat.kloc.mfsmw.util.SqlQuery;

@Repository
public class NotificationDaoImpl implements NotificationDao {

	private static Log log = LogFactory.getLog(NotificationDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<SmsNotificationTemplate> getAll() {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(SqlQuery.SMS_NOTIFICATION_TEMPLATE_GET_ALL, BeanPropertyRowMapper.newInstance(SmsNotificationTemplate.class));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}
	
	@Override
	public List<SmsNotificationTemplate> getAllByTitle(String title) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(SqlQuery.SMS_NOTIFICATION_TEMPLATE_GET_ALL+"AND title='"+title+"'", BeanPropertyRowMapper.newInstance(SmsNotificationTemplate.class));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}
}
