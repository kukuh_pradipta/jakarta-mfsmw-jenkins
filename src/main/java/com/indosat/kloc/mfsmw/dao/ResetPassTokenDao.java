/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jan 28, 2015 
 * Time       : 2:08:42 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import com.indosat.kloc.mfsmw.model.ResetPassToken;

public interface ResetPassTokenDao {

	public String getToken(String username);
	public void save(ResetPassToken token);
	
}
