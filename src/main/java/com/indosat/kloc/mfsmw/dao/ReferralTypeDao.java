/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 22, 2018 
 * Time       : 2:58:07 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.ReferralType;

public interface ReferralTypeDao {

	List<ReferralType> getAllReferralType();
	
}
