/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Apr 29, 2015 
 * Time       : 3:25:07 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import com.indosat.kloc.mfsmw.pojo.RemittanceUser;

public interface RemittanceUserDao {
	
	public int save(RemittanceUser user);
	public int update(RemittanceUser user);
	public RemittanceUser getById(String idType, String idNo);
	
}
