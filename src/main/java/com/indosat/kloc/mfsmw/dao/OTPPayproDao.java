/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jan 10, 2015 
 * Time       : 9:54:19 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.OTPCounter;
import com.indosat.kloc.mfsmw.model.OTPPaypro;

public interface OTPPayproDao {
	
	public OTPPaypro checkMSISDN(String msisdn);
	public Integer checkMinute(String sessid, String requestid, String datereq);
	public int insertOTP(OTPPaypro otp);
	public int updateOTP(String msisdn, String sessid, String requestid, String smsresult);
	public int updateFlagOTP(String sessid, String date_end);
	public int deleteOTP(String requestid, String sessid);
	public String getConfigOTP(String name);
	
}
