/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jun 3, 2015 
 * Time       : 9:52:11 AM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.RemittanceAgentDao;
import com.indosat.kloc.mfsmw.model.RemittancePartner;
import com.indosat.kloc.mfsmw.pojo.RemittanceAgent;
import com.indosat.kloc.mfsmw.util.SqlQuery;

@Repository
public class RemittanceAgentDaoImpl implements RemittanceAgentDao {

	private static Log log = LogFactory.getLog(RemittanceAgentDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<RemittanceAgent> getAll() {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(SqlQuery.DMT_AGENT_SELECT_ALL, 
					BeanPropertyRowMapper.newInstance(RemittanceAgent.class));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}

	@Override
	public List<RemittancePartner> getAllRemittancePartner() {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(SqlQuery.DMT_PARTNER_SELECT_ALL, BeanPropertyRowMapper.newInstance(RemittancePartner.class));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}
	
}
