/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Nov 24, 2014 
 * Time       : 3:55:22 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.UserDao;
import com.indosat.kloc.mfsmw.model.User;
import com.indosat.kloc.mfsmw.util.Constant;

@Repository
public class UserDaoImpl implements UserDao{

	private static Log log = LogFactory.getLog(UserDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert simpleJdbcInsert;
	
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("user").usingGeneratedKeyColumns("id");
	}
	
	@Override
	public void updateStatus(String username, int status){
		try {
			jdbcTemplate.update("UPDATE user SET status=? WHERE username=?", status, username);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	@Override
	public String save(User user) {
		// TODO Auto-generated method stub
		log.info("Save user : "+user.toString());
		try {
			Map<String, Object> parameters = new HashMap<String, Object>(13);
			parameters.put("username", user.getUsername());
			parameters.put("firstName", user.getFirstName());
			parameters.put("lastName", user.getLastName());
			parameters.put("dob", user.getDob());
			parameters.put("gender", user.getGender());
			parameters.put("motherMaidenName", user.getMotherMaidenName());
			parameters.put("address", user.getAddress());
			parameters.put("idType", user.getIdType());
			parameters.put("idNo", user.getIdNo());
			parameters.put("msisdn", user.getMsisdn());
			parameters.put("status", user.getStatus());
			parameters.put("encryptedText", user.getEncryptedText());
			parameters.put("createdDate", new Date());
			Number id = simpleJdbcInsert.executeAndReturnKey(parameters);
			return id.toString();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}

	@Override
	public User get(String username) {
		// TODO Auto-generated method stub
		log.info("getUserby Username: "+username);
		try {
			return jdbcTemplate.queryForObject(Constant.USER_GET, BeanPropertyRowMapper.newInstance(User .class), username);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
			return null;
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
	}
	
	@Override
	public String getUserByAccountID(String accountID) {
		// TODO Auto-generated method stub
		log.info("getUserby accountID: "+accountID);
		try {
			return jdbcTemplate.queryForObject(Constant.USER_GET_USERNAME, String.class, accountID);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

}
