/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Feb 16, 2015 
 * Time       : 2:39:41 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.PhotoDao;
import com.indosat.kloc.mfsmw.model.Photo;

@Repository
public class PhotoDaoImpl implements PhotoDao{
	
	private static Log log = LogFactory.getLog(PhotoDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void save(Photo photo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Photo get(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Photo photo) {
		// TODO Auto-generated method stub
		
	}

}
