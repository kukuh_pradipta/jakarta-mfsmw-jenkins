/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 8, 2014 
 * Time       : 9:40:34 AM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.DenomDao;
import com.indosat.kloc.mfsmw.model.Denom;
import com.indosat.kloc.mfsmw.util.Constant;

@Repository
public class DenomDaoImpl implements DenomDao {

private static Log log = LogFactory.getLog(DenomDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Denom> getAll(int prefixId) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(Constant.DENOM_GET_ALL+ "AND prefixId="+prefixId, BeanPropertyRowMapper.newInstance(Denom.class));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		return null;
	}

}
