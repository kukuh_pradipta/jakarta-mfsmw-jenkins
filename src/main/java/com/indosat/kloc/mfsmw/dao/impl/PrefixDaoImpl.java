/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 6, 2014 
 * Time       : 12:13:26 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.PrefixDao;
import com.indosat.kloc.mfsmw.model.Prefix;
import com.indosat.kloc.mfsmw.model.PrefixReference;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.SqlQuery;

@Repository
public class PrefixDaoImpl implements PrefixDao{
	
	private static Log log = LogFactory.getLog(PrefixDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Prefix> getAll() {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(Constant.PREFIX_GET_ALL, BeanPropertyRowMapper.newInstance(Prefix.class));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<PrefixReference> getAllPrefixReference() {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.query(SqlQuery.PREFIX_REFERENCE_GET_ALL, BeanPropertyRowMapper.newInstance(PrefixReference.class));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		
		return null;
	}

}
