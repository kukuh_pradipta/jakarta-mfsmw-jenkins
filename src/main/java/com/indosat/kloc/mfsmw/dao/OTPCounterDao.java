/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jan 10, 2015 
 * Time       : 9:54:19 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.OTPCounter;

public interface OTPCounterDao {

	public List<OTPCounter> getAll();
	public int getCounter(String username);
	public void save(String username);
	public void updateCounter(String username, int counter);
	public void removeAll();
	
}
