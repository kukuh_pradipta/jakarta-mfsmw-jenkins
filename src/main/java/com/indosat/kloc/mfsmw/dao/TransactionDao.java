/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 18, 2014 
 * Time       : 5:18:36 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.BillerTransaction;
import com.indosat.kloc.mfsmw.model.MBillerTransaction;
import com.indosat.kloc.mfsmw.model.Transaction;
import com.indosat.kloc.mfsmw.model.UserWallet;
import com.indosat.kloc.mfsmw.pojo.DMTTransaction;

public interface TransactionDao {
	public int saveMbillerTrx(MBillerTransaction trx);
	public BillerTransaction getBillerTransaction(String transId);
	public void saveAll(List<Transaction> list);
	public void saveAllBillerTrx(List<BillerTransaction> list);
	public void saveAllMBillerTrx(List<MBillerTransaction> list);
	
	public void saveAllDMTTransaction(List<DMTTransaction> list);
	public void saveAllUserWallet(List<UserWallet> list);
	
}
