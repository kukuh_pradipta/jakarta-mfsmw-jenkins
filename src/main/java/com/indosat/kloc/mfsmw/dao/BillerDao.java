/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 19, 2014 
 * Time       : 2:46:36 PM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.Biller;
import com.indosat.kloc.mfsmw.model.MBiller;
import com.indosat.kloc.mfsmw.model.MBillerTransaction;

public interface BillerDao {
	public List<Biller> getAll();
	public List<MBiller> getAllMBiller();
	public MBillerTransaction getByTransId(String transId);
}
