/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Nov 24, 2014 
 * Time       : 3:55:22 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.UserWalletDao;
import com.indosat.kloc.mfsmw.model.EkycWorkaround;
import com.indosat.kloc.mfsmw.model.RefKecamatan;
import com.indosat.kloc.mfsmw.model.UserWallet;
import com.indosat.kloc.mfsmw.util.Constant;

@Repository
public class UserWalletDaoImpl implements UserWalletDao {

	private static Log log = LogFactory.getLog(UserWalletDaoImpl.class);

	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert simpleJdbcInsert;
	private SimpleJdbcInsert simpleJdbcEkycWorkaround;

	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("user_wallet").usingGeneratedKeyColumns("id");
		this.simpleJdbcEkycWorkaround = new SimpleJdbcInsert(dataSource).withTableName("ekyc_workaround");
	}

	@Override
	public void updateStatus(UserWallet user) {
		try {
			jdbcTemplate.update(
					"UPDATE user_wallet SET name=?,idno=?,idtype=?,countrycode=?,gender=?,dob=?,address=?,wallet_type=?,"
							+ "status=?,encrypted_text=?,encrypted_code=?,updated_time=? WHERE msisdn=?",
					user.getName(), user.getIdno(), user.getIdtype(), user.getCountrycode(), user.getGender(),
					user.getDob(), user.getAddress(), user.getWallet_type(), user.getStatus(), user.getEncrypted_text(),
					user.getEncrypted_code(), user.getUpdated_time(), user.getMsisdn());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public String save(UserWallet user) {
		// TODO Auto-generated method stub
		log.info("Save user wallet : " + user.toString());
		try {
			Map<String, Object> parameters = new HashMap<String, Object>(13);
			parameters.put("msisdn", user.getMsisdn());
			parameters.put("name", user.getName());
			parameters.put("idno", user.getIdno());
			parameters.put("idtype", user.getIdtype());
			parameters.put("countrycode", user.getCountrycode());
			parameters.put("gender", user.getGender());
			parameters.put("dob", user.getDob());
			parameters.put("address", user.getAddress());
			parameters.put("wallet_type", user.getWallet_type());
			parameters.put("status", user.getStatus());
			parameters.put("encrypted_text", user.getAddress());
			parameters.put("encrypted_code", user.getAddress());
			parameters.put("created_time", new Date());
			parameters.put("updated_time", new Date());
			Number id = simpleJdbcInsert.execute(parameters);
			return id.toString();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Found Error: " + user.toString() + "\n" + e);
			return String.valueOf(Constant.RC_DB_ERROR);
		}
	}
	
	

	@Override
	public UserWallet get(String msisdn) {
		// TODO Auto-generated method stub
		log.info("getUserby MSISDN: " + msisdn);
		try {
			return jdbcTemplate.queryForObject(Constant.USER_WALLET_GET,
					BeanPropertyRowMapper.newInstance(UserWallet.class), msisdn);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Found Error: " + msisdn + "\n" + e);
			return null;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Found Error: " + msisdn + "\n" + e);
			return null;
		}

	}

	@Override
	public List<UserWallet> getList(String msisdn) {
		List<UserWallet> customers  = jdbcTemplate.query(Constant.USER_WALLET_GET,
				new BeanPropertyRowMapper(UserWallet.class),msisdn);
//		log.debug(customers.toString());
//		log.debug(customers.size());
		return customers;
	}

	@Override
	public String saveEkycWorkaround(EkycWorkaround user) {
		// TODO Auto-generated method stub
		log.info("Save EKYC Workaround : " + user.toString());
		try {
			Map<String, Object> parameters = new HashMap<String, Object>(13);
			parameters.put("msisdn", user.getMsisdn());
			parameters.put("idno", user.getIdno());
			parameters.put("keyword", user.getKeyword());
			parameters.put("created_date", new Date());
			int id = simpleJdbcEkycWorkaround.execute(parameters);
			if(id>0){
				log.info("Save EKYC Workaround : Success");
			}
			return String.valueOf(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Found Error: " + user.toString() + "\n" + e);
			return String.valueOf(Constant.RC_DB_ERROR);
		}
	}
	
	@Override
	public List<RefKecamatan> getKecamatans() {
		List<RefKecamatan> kecamatan  = jdbcTemplate.query(Constant.GET_KECAMATANS,new BeanPropertyRowMapper(RefKecamatan.class));
		return kecamatan;
	}
	
}
