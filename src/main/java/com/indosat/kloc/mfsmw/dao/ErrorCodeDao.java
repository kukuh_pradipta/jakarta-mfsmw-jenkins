/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 1, 2014 
 * Time       : 10:53:20 AM 
 */
package com.indosat.kloc.mfsmw.dao;

import java.util.List;

import com.indosat.kloc.mfsmw.model.ErrorCode;
import com.indosat.kloc.mfsmw.model.IndosmartErrorCode;

public interface ErrorCodeDao {

	public List<IndosmartErrorCode> getAll();
	
	public List<ErrorCode> getList();
	
}
