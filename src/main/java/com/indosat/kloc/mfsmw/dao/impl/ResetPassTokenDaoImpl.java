/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jan 28, 2015 
 * Time       : 2:09:22 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.Date;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.ResetPassTokenDao;
import com.indosat.kloc.mfsmw.model.ResetPassToken;
import com.indosat.kloc.mfsmw.util.Constant;

@Repository
public class ResetPassTokenDaoImpl implements ResetPassTokenDao{
	
private static Log log = LogFactory.getLog(PrefixDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void save(ResetPassToken token) {
		// TODO Auto-generated method stub
		try {
			jdbcTemplate.update(Constant.RESET_PASS_TOKEN_SAVE, token.getInitiator(), token.getToken(), new Date(), new Date());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
	}

	@Override
	public String getToken(String initiator) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.queryForObject(Constant.REET_PASS_GET_TOKEN, String.class, initiator);
		} catch(EmptyResultDataAccessException e){
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}

}
