/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 5, 2015 
 * Time       : 12:34:17 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.TargetBillerDao;
import com.indosat.kloc.mfsmw.pojo.Target;
import com.indosat.kloc.mfsmw.util.Constant;

@Repository
public class TargetBillerDaoImpl implements TargetBillerDao{

	private static Log log = LogFactory.getLog(TargetBillerDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(@Qualifier("dataSource") final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Target> getAll(){
		try {
			return jdbcTemplate.query(Constant.TARGET_GET_ALL, BeanPropertyRowMapper.newInstance(Target.class));
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e);
			e.printStackTrace();
		}
		
		return null;
	}
	
}
