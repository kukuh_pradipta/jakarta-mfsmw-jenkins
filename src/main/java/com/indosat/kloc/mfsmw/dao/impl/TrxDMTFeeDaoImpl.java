/**
 * Project    : Dompetku - Domestic Money Transfer
 * Created By : Megi Jaka Permana
 * Date       : May 15, 2015 
 * Time       : 6:00:54 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.TrxDMTFeeDao;
import com.indosat.kloc.mfsmw.util.SqlQuery;

@Repository
public class TrxDMTFeeDaoImpl implements TrxDMTFeeDao{

	private static final Log log = LogFactory.getLog(TrxDMTFeeDaoImpl.class);

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public String getFee(String amount) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.queryForObject(SqlQuery.TRX_FEE_GET_FEE, String.class, amount);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e+"amount:"+amount);
		}
		return null;
	}

	@Override
	public String getFee(String amount, String agent) {
		// TODO Auto-generated method stub
		try {
			return jdbcTemplate.queryForObject(SqlQuery.TRX_H2H_FEE_GET_FEE, String.class, amount, agent);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error(e+"amount:"+amount);
		}
		return null;
	}

}
