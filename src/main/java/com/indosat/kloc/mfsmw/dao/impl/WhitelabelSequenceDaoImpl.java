/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Feb 16, 2015 
 * Time       : 2:39:41 PM 
 */
package com.indosat.kloc.mfsmw.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.indosat.kloc.mfsmw.dao.PhotoDao;
import com.indosat.kloc.mfsmw.dao.WhitelabelSequenceDao;
import com.indosat.kloc.mfsmw.model.Biller;
import com.indosat.kloc.mfsmw.model.BillerTransaction;
import com.indosat.kloc.mfsmw.model.Photo;
import com.indosat.kloc.mfsmw.model.WhitelabelSequence;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.Utils;

@Repository
public class WhitelabelSequenceDaoImpl implements WhitelabelSequenceDao{
	
	private static Log log = LogFactory.getLog(WhitelabelSequenceDaoImpl.class);
	
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert whitelabelJdbcInsert;

	@Autowired
	public void setDataSource(final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.whitelabelJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("whitelabel_sequence");
	}


	@Override
	public int insert(WhitelabelSequence obj) {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>(18);
			parameters.put("partner_code", obj.getPartner_code());
			parameters.put("msisdn", obj.getMsisdn());
			parameters.put("msisdn_part",  obj.getMsisdn_part());
			Number id = whitelabelJdbcInsert.execute(parameters);
			return id.intValue();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("Found Error: "+obj.toString()+"\n"+e);
			return Constant.RC_DB_ERROR;
		}
	}

	@Override
	public List<WhitelabelSequence> getWhitelabelSequenceSpesific(String partMsisdn,String partnerCode) {
		List<WhitelabelSequence> result = new ArrayList<WhitelabelSequence>();
		try {
			result = jdbcTemplate.query(
					Constant.WHITELABEL_SEQUENCE_SPESIFIC,
				    new Object[] {partMsisdn,partnerCode},
				    new RowMapper<WhitelabelSequence>() {
				        public WhitelabelSequence mapRow(ResultSet rs, int rowNum) throws SQLException {
				        	WhitelabelSequence c = new WhitelabelSequence();
				            c.setPartner_code(rs.getString(1));
				            c.setMsisdn(rs.getString(2));
				            c.setMsisdn_part(rs.getString(3));
				            return c;
				        }
				    });
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			return result;
		}
	}

	@Override
	public List<WhitelabelSequence> getWhitelabelSequenceAll() {
		try {
			return jdbcTemplate.query(Constant.WHITELABEL_SEQUENCE_GET_ALL, BeanPropertyRowMapper.newInstance(WhitelabelSequence.class));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<WhitelabelSequence> getWhitelabelSequenceSpesificMsisdn(String msisdn,String partnerCode) {
		List<WhitelabelSequence> result = new ArrayList<WhitelabelSequence>();
		try {
			result = jdbcTemplate.query(
					Constant.WHITELABEL_SEQUENCE_SPESIFIC_MSISDN,
				    new Object[] {msisdn,partnerCode},
				    new RowMapper<WhitelabelSequence>() {
				        public WhitelabelSequence mapRow(ResultSet rs, int rowNum) throws SQLException {
				        	WhitelabelSequence c = new WhitelabelSequence();
				            c.setPartner_code(rs.getString(1));
				            c.setMsisdn(rs.getString(2));
				            c.setMsisdn_part(rs.getString(3));
				            return c;
				        }
				    });
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			return result;
		}
	}

	
}
