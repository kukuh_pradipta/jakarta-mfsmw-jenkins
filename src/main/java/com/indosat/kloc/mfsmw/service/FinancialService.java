/**
 * <h1>FinancialService Interface</h1>
 * This interface class will list all of service in MFSMW
 * and convert it into WSDL format by using '@webservice' annotation
 * from JAX-WS.
 * This interface method implemented on FinancialServiceImpl class
 * 
 * @author Megi Jaka Permana
 * @since Sep 6, 2014 
 */
package com.indosat.kloc.mfsmw.service;

import java.util.HashMap;

import javax.jws.WebParam;
import javax.jws.WebService;

import com.indosat.kloc.mfsmw.pojo.Parameter;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.util.Constant;

@WebService
public interface FinancialService {
	
	/**
	 * <b>Generate transactionID from Utiba Core and can be use as long utiba session is same for each transaction</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>DMT via SOAP</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 */
	public Response getNextID(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature);
	
	/**
	 * <b>Sent SMS with keyword to SSP</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PLN Webtool via API : [URL]/webapi/sms_keyword</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  <li>String keyword</li>
		  <li>String msisdn</li>
		</ul>
	 */
	public Response smsKeyword(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Some utilities for companion card. Companion card is ATM Card for Dompetku/Paypro.
	 * This API will connect to artajasa, to do some operation in Companion Card</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Dompetku apps : [URL]/webapi/sms_keyword</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  <li>String msisdn</li>
		  <li>String serviceName<br>
		  	  Available service:<br>
		  	  	COMP_CARD_GET_STATUS = "MSISDNStatus";
				COMP_CARD_DELIVERY_REQUEST = "DeliveryReq";
				COMP_CARD_DELIVERY_STATUS = "DeliveryStatus";
				COMP_CARD_ACTIVATION = "ActivateCard";
				COMP_CARD_DEACTIVATION = "DeactivateCard";
		  </li>	
		</ul>
	 */
	public Response companionCard(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>SELL Request using loyalty point</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Dompetku apps : [URL]/webapi/sell_p</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  <li>String to</li>
		  <li>String amount</li>
		  <li>String extRef</li>
		</ul>
	 */
	public Response sellPoint(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Querying transaction based on extRef from partner</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Online Merchant : [URL]/webapi/trans_ext_ref</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param extRef transaction reference of partner<br>
	 */
	public Response transQueryExt(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "extRef") String extRef);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Dompetku+ (Payment Gateway) Utilities: For resend Email Activation</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Dompetku+ : [URL]/webapi/resend_email_activation</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  <li>String email</li>
		</ul>
	 */
	public Response resendEmailActivation(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Settling transaction from NFC Card (P2P transfer on backend)</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>NFC Card : [URL]/webapi/do_nfc_settlement</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  <li>String amount</li>
		  <li>String extRef</li>
		</ul>
	 */
	public Response nfcSettlement(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);
	
	/**
	 * <b>Updating user data on UTIBA, and unmap user from "NON-KYC" grouping</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Apps,UMB : [URL]/webapi/do_kyc_by_agent</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameterMap hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String firstName</li>
			<li>String lastName</li>
			<li>String idType</li>
			<li>String idNumber</li>
			<li>String address</li>
			<li>String idPhoto</li>
			<li>String dob</li>
			<li>String motherMaidenName</li>
			<li>String gender</li>
			<li>String to</li>
			<li>String otp</li>
			<li>String type</li>
		</ul>
	 */
	public Response doKYCByAgent(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);
	
	/**
	 * 
	 * <b>This function used by Agent to KYC the customer. Agent will input the account that want to register as a KYC. This function will validate first the B number, if status of the account is non-KYC, system will send the SMS OTP to the B Number. B number need to tell the OTP to the Agent</b>
	 * <b>it's allso can be use to update the profile of the B number.</b>
	 * @param userId
	 * @param signature
	 * @param msisdn
	 * @param type 1:Update Profile otherwise OTP
	 * @return
	 */
	public Response inquiryKYC(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "msisdn") String msisdn,
			@WebParam(name = "type") String type);
	
	/**
	 * 
	 * This function used to validate the customer number before reset PIN. customer will get the SMS OTP, and those OTP will be shared to Agent
	 * 
	 * @param userId
	 * @param signature
	 * @param msisdn
	 * @return
	 */
	public Response inquiryResetPINByAgent(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "msisdn") String msisdn);
	
	/**
	 * 
	 * This function used to reset the Customer PIN via Agent.
	 * 
	 * @param userId
	 * @param signature
	 * @param parameterMap
	 * @return
	 */
	public Response resetPinByAgent(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);
	
	/**
	 * 
	 * <b>CASH 2 CASH</b>
	 * <br>
	 * This function used by DMT Web Portal to resend the SMS MTCN.
	 * 
	 * @param userId
	 * @param signature
	 * @param transId
	 * @return
	 */
	public Response resendSMSCouponRemit(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "transId") String transId);
	
	/**
	 * <b>CASH 2 CASH<b>
	 * <br>
	 * This function used to get the details of the customer by input the Identity Number.
	 * 
	 * @param userId
	 * @param signature
	 * @param idType
	 * @param idNo
	 * @return
	 */
	public Response getDMTUserByIdentityID(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "idType") String idType,
			@WebParam(name = "idNo") String idNo);
	
	/**
	 * 
	 * this function used to check status of the transaction
	 * 
	 * @param userId
	 * @param signature
	 * @param transId
	 * @return
	 */
	public Response getTransactionById(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "transid") String transId);
	
	/**
	 * 
	 * <b>CASH 2 CASH</b>
	 * <br>
	 * 
	 * This function used to initiate cash2cash transaction. Middleware will store the KYC information of the sender.
	 * 
	 * @param userId
	 * @param signature
	 * @param parameter (Please see the implementation class to get the details of the parameters)
	 * @return
	 */
	public Response inquiryCouponRemit(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * 
	 * <b>CASH2CASH</b>
	 * </br>
	 * this function used to confirm the inquiryCouponRemit.
	 * 
	 * @param userId
	 * @param signature
	 * @param transId
	 * @param extRef
	 * @return
	 * MTCN
	 */
	public Response commitCouponRemit(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "transid") String transId,
			@WebParam(name = "extRef") String extRef);
	
	
	/**
	 * 
	 * <b>CASH2CASH</b>
	 * </br>
	 * this function used to cancel the MTCN.
	 * @param userId
	 * @param signature
	 * @param couponId
	 * @param initiator
	 * @return
	 */
	public Response cancelCoupon(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "couponId") String couponId, String initiator);
	
	/**
	 *<b>CASH2CASH</b>
	 *</br>
	 * This function used to redeem the MTCN. Receiver will be KYC and Middleware will store the data.
	 *  
	 * @param userId
	 * @param signature
	 * @param parameter (Please see implementation class for parameter's details)
	 * @return
	 */
	public Response remitCoupon(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 *<b>Cancel MTCN of CASH2CASH/Remmitance</b>
	 *</br>
	 * This function used to cancel the MTCN and make it unavailable to use.
	 *  
	 * @param userId
	 * @param signature
	 * @param parameter (Please see implementation class for parameter's details)
	 * @return
	 */
	public Response cancelCouponRemitance(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 *<b>Get MTCN of CASH2CASH/Remmitance</b>
	 *</br>
	 * This function used to Inquiry MTCN of C2C.
	 *  
	 * @param userId
	 * @param signature
	 * @param parameter (Please see implementation class for parameter's details)
	 * @return
	 */
	public Response getCouponRemitance(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 *<b>Create MTCN for CASH2CASH/Remmitance</b>
	 *</br>
	 * This function used to redeem the MTCN. Receiver will be KYC and Middleware will store the data.
	 *  
	 * @param userId
	 * @param signature
	 * @param parameter (Please see implementation class for parameter's details)
	 * @return
	 */
	public Response createCouponRemitance(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Create MTCN for wallet2Cash</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UMB, ALL APPS: [URL]/webapi/wallet2cash</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter some of them NOT MANDATORY:<br>
	 * <ul>
			<li>String extRef</li>
			<li>String storeName</li>
			<li>String amount</li>
			<li>String recipientPhoneNumber</li>
			<li>String recipientName</li>
		</ul>
	 */
	public Response wallet2Cash(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Special Agent Inquiry for Wallet2Cash</b>
	 * <br>
	 * This API will inquiry agent data, and filtering eligible user for do Wallet2Cash
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UMB: [URL]/webapi/wallet2cash_inquiry</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter some of them NOT MANDATORY:<br>
	 * <ul>
			<li>String extRef</li>
			<li>String storeName</li>
			<li>String amount</li>
			<li>String recipientPhoneNumber</li>
			<li>String recipientName</li>
		</ul>
	 */
	public Response wallet2CashInquiry(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Move customer balance to "Onebill Temp Wallet" so it can be use by Onebill/Autodebit</b>
	 * <br>
	 * This API will move customer balance to "Onebill Temp Wallet"
	 * <br>
	 * Use at:
	 * <ul>
		  <li>ONEBILL Microapps : SOAP</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param param hashmap that consist of parameter that need to be use for inquiry billpayment automatic<br>
	 */
	public Response oneBillSell(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Inquiry Billpay mechanism that used for Onebill/Autodebit</b>
	 * <br>
	 * This API will inquiry customer billpayment, from "Onebill Temp Wallet"
	 * <br>
	 * Use at:
	 * <ul>
		  <li>ONEBILL Microapps : SOAP</li>
		  <li>INTERNAL TESTING: [URL]/webapiinternal/onebill_inq</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param param hashmap that consist of parameter that need to be use for inquiry billpayment automatic<br>
	 */
	public Response oneBillInq(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Payment Billpay mechanism that used for Onebill/Autodebit</b>
	 * <br>
	 * This API will pay customer billpayment, from "Onebill Temp Wallet"
	 * <br>
	 * Use at:
	 * <ul>
		  <li>ONEBILL Microapps : SOAP</li>
		  <li>INTERNAL TESTING: [URL]/webapiinternal/onebill_pay</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param param hashmap that consist of parameter that need to be use for confirm billpayment automatic<br>
	 */
	public Response oneBillPay(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Payment Billpay mechanism for MultiBiller</b>
	 * <br>
	 * This API will call mbiller object in this project. MBiller object will handling payment to some biller based on its service (electricity, Water, ETC)
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UMB, Dompetku: [URL]/webapi/mbiller_inquiry</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameterMap hashmap that consist of parameter that need to be used by MBiller<br>
	 */
	public Response billerInq(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Confirm Billpay mechanism for MultiBiller</b>
	 * <br>
	 * This API will call mbiller object in this project. MBiller object will handling payment to some biller based on its service (electricity, Water, ETC)
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UMB, Dompetku: [URL]/webapi/mbiller_pay</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameterMap hashmap that consist of parameter that need to be used by MBiller<br>
	 */
	public Response billerPay(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Get Dompetku+ OTP Request counter status</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Dompetku +</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 */
	public Response OTPCounter(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Forget Password request for Dompetku +</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Dompetku +</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param to id requested to be reset
	 */
	public Response forgetPassword(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "to") String to);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>P2P Transfer between TCASH,XL Tunai, Paypro</b>
	 * <br>
	 * Using P2P API made by Putu (PHP Programmer). Obsolote and unused
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UNUSEDr</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param to receiver number<br>
	 * @param amount amount to be transfered<br>
	 */
	public Response p2pTransfer(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "to") String to,
			@WebParam(name = "amount") String amount);
	
	/**
	 * <b>Initiate Bank Transfer via UTIBA. Agent will sent TOKEN By UTIBA to initiate bank transfer.</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UMB, Dompetku: [URL]/webapi/bank_transfer</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param bankCode target bank code<br>
	 * @param accountNo target bank_account number<br>
	 * @param amount amount to be transfered<br>
	 */
	public Response bankTransfer(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "bankCode") String bankCode,
			@WebParam(name = "accountNo") String accountNo,
			@WebParam(name = "amount") String amount);
	
	/**
	 * <b>Authorize customer credential</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UMB, ALL APPS: [URL]/webapi/login</li>
		  <li>UMB, ALL APPS: [URL]/webapiinternal/login</li>
		  <li>Dompetku +: securepage</li>
		  <li>DMT: soap</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param newPin agent newPIN<br>
	 */
	public Response doLogin(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature);
	
	/**
	 * <b>Change customer PIN. Old PIN pass via signature</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Dompetku & MobileAgentAPPS & PAYPRO: [URL]/webapi/change_pin</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param newPin agent newPIN<br>
	 */
	public Response changePin(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "newpPin") String newPin);
	
	/**
	 * <b>Reset agent PIN, and will be sent via SMS/AppsNotif/Email</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UMB,Dompetku+,Dompetku: [URL]/webapi/reset_pin</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 */
	public Response resetPin(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature);
	
	/**
	 * <b>Registering user to be reguler agent in UTIBA, only with msisdn, firstname, dob</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Dompetku & MobileAgentAPPS: [URL]/webapi/s_register</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter some of them NOT MANDATORY:<br>
	 * <ul>
			<li>String firstName</li>
			<li>String lastName</li>
			<li>String dob</li>
			<li>String gender</li>
			<li>String motherMaidenName</li>
			<li>String address</li>
			<li>String idType</li>
			<li>String idNo</li>
			<li>String msisdn</li>
		</ul>
	 */
	public Response simpleReg(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);
	
	/**
	 * <b>Registering user to be reguler agent in UTIBA</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Dompetku & MobileAgentAPPS: [URL]/webapi/register</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String firstName</li>
			<li>String lastName</li>
			<li>String dob</li>
			<li>String gender</li>
			<li>String motherMaidenName</li>
			<li>String address</li>
			<li>String idType</li>
			<li>String idNo</li>
			<li>String msisdn</li>
			<li>String email</li>
			<li>String countryCode</li>
		</ul>
	 */
	public Response register(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Registering agent as OUTLET</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UNUSED</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String firstName</li>
			<li>String address</li>
			<li>String msisdn</li>
			<li>String initiator</li>
		</ul>
	 */
	public Response registerOutlet(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Update agent data in UTIBA</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Dompetku & MobileAgentAPPS: [URL]/webapi/update_profile</li>
		  <li>INTERNAL METHOD</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String firstName</li>
			<li>String lastName</li>
			<li>String idType</li>
			<li>String idNumber</li>
			<li>String address</li>
			<li>String profilePic</li>
			<li>String idPhoto</li>
			<li>String dob</li>
			<li>String motherMaidenName</li>
			<li>String gender</li>
			<li>String msisdn</li>
			<li>String kycRequest</li>
			<li>String email</li>
		</ul>
	 */
	public Response updateProfile(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Register/Update agent, and flagged to our database as "KYC Request", Initiate by MobileAgent</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Dompetku & MobileAgentAPPS: [URL]/webapi/new_upgrade_kyc</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String firstName</li>
			<li>String lastName</li>
			<li>String msisdn</li>
			<li>String idType</li>
			<li>String idNumber</li>
			<li>String address</li>
			<li>String idPhoto</li>
			<li>String dob</li>
			<li>String motherMaidenName</li>
			<li>String gender</li>
		</ul>
	 */
	public Response mobileAgnetkycUpgrade(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Update agent data, and flagged to our database as "KYC Request"</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Dompetku: [URL]/webapi/upgrade_kyc</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String firstName</li>
			<li>String lastName</li>
			<li>String msisdn</li>
			<li>String idType</li>
			<li>String idNumber</li>
			<li>String address</li>
			<li>String idPhoto</li>
			<li>String dob</li>
			<li>String motherMaidenName</li>
			<li>String gender</li>
		</ul>
	 */
	public Response kycUpgrade(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);
	
	/**
	 * <b>Check agent transaction history</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO APPS, Dompetku, UMB, PARTNER : [URL]/webapi/history_transaction</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param to agent to be inquiried
	 * @param count count rows of transaction to be showed
	 */
	public Response doHistoryTransaction(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "to") String to,
			@WebParam(name = "count") int count);
	
	/**
	 * <b>Check Available balance of agent</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO APPS, Dompetku, UMB, PARTNER : [URL]/webapi/balance_check</li>
		  <li>PAYPRO APPS, Dompetku, UMB, PARTNER : [URL]/webapiinternal/balance_check</li>
		  <li>Dompetku + Securepage</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param to agent to be inquiried
	 */
	public Response balanceCheck(
			@WebParam(name = "userId")String userId, 
			@WebParam(name = "to") String to);
	
	/**
	 * <b>Get detail info of Agent</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO APPS, Dompetku, UMB, PARTNER : [URL]/webapi/user_inquiry</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param to agent to be inquiried
	 */
	public Response doUserInquiry(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "to") String to);

	/**
	 * <b>Payment/Purchase with confirmation (Using SELL Method) --> PUSH USSD</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PARTNER for PUSH USSD : [URL]/webapi/do_payment</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param extRef partner transaction id
	 * @param extraParam adding parameter for info
	 * @param to agent to be deducted
	 * @param amount amount to be deducted
	 */
	public Response sell(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "to") String to, 
			@WebParam(name = "amount") String amount,
			@WebParam(name = "extRef") String extRef,
			@WebParam(name = "extraParam") String extraParam);
	
	/**
	 * <b>Payment/Purchase without confirmation (Using SELL Method)</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PARTNER : [URL]/webapiinternal/sell_no_confirm</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param extRef partner transaction id
	 * @param to agent to be deducted
	 * @param amount amount to be deducted
	 */
	public Response sellNoConfirm(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "to") String to, 
			@WebParam(name = "amount") String amount,
			@WebParam(name = "extRef") String extRef);

	/**
	 * <b>Check DMT Transaction</b>
	 * <br>
	 * Only DB select transaction. UTIBA used for authorize account
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PARTNER : [URL]/webapi/do_payment_check</li>
		  <li>DMT PORTAL: [URL]/dmt/do_payment_check</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param extRef partner transaction id
	 */
	public Response checkTransaction(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "extRef") String extRef);

	/**
	 * <b>Cashout using Token for payment or other</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PARTNER : [URL]/webapi/coupon_transfer</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param to target/receiver wallet
	 * @param amount amount to be topupped
	 * @param extRef partner transaction id
	 * @param token token/coupon to release amount 
	 */
	public Response couponTransfer(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature, 
			@WebParam(name = "to") String to,
			@WebParam(name = "extRef") String extRef, 
			@WebParam(name = "amount") String amount,
			@WebParam(name = "token") String token);

	/**
	 * <b>Cashout using Token via merchant</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>DMT/Simponi via SOAP</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param to target/receiver wallet
	 * @param amount amount to be topupped
	 * @param extRef partner transaction id
	 * @param token token/coupon to release amount 
	 */
	public Response cashout(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature, 
			@WebParam(name = "to") String to,
			@WebParam(name = "extRef") String extRef, 
			@WebParam(name = "amount") String amount,
			@WebParam(name = "token") String token);

	/**
	 * <b>Topup Paypro Wallet via Merchant</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UMB, PARTNER : [URL]/webapi/merchant_cashin</li>
		  <li>UMB, PARTNER : [URL]/webapiinternal/merchant_cashin</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param to target/receiver wallet
	 * @param amount amount to be topupped
	 * @param extRef partner transaction id
	 * @param supressSMS configure sms notification 
	 */
	public Response cashin(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "to") String to, 
			@WebParam(name = "amount") String amount,
			@WebParam(name = "extRef") String extRef, 
			@WebParam(name = "supressSms") boolean supressSms);

	/**
	 * <b>Reversal transaction that happen in UTIBA</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO,UMB, PARTNER : [URL]/webapi/do_reversal</li>
		  <li>PAYPRO,UMB, PARTNER : [URL]/webapiinternal/do_reversal</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String amount</li>
			<li>String transid --> Transaction ID Of UTIBA who need to be reversed</li>
			<li>String recipient</li>
		</ul>
	 */
	public Response reversal(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature, 
			@WebParam(name = "parameter") Parameter parameter);

	/**
	 * @param userId
	 * @param signature
	 * @param parameter
	 * @return
	 */
	public Response reversalExtraParam(String userId, String signature, Parameter parameter);
	
	/**
	 * <b>Check agent available balance</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>SOAP</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 */
	public Response balance(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature);

	/**
	 * <b>Generate token/coupon for payment,cashout</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO,UMB, PARTNER : [URL]/webapi/create_coupon</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 */
	public Response createCoupon(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature);
	
	/**
	 * <b>Generate token/coupon with expired time/hour</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>INTERNAL METHOD: KYC Request,Reset PIN Request</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param hour limit expired time
	 */
	public Response createCouponByHour(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "hour") int hour);

	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Token OTP Generator For Dompetku+ </b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>DOMPETKU+ : [URL]/webapi/get_otp</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param count counter for OTP Request
	 */
	public Response requestOTP(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "count") int count);

	/**
	 * <b>Merchant Payment, using P2P Mechanism</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO,UMB, PARTNER : [URL]/webapi/merchant_transfer</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param amount amount to be paid to merchant
	 * @param to merchant name
	 * @param parameterMap hashmap that consist of other parameter
	 */
	public Response merchantTransfer(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature, 
			@WebParam(name = "amount") String amount,
			@WebParam(name = "to") String to,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);
	
	/**
	 * <b>Merchant Payment Special for QR, using P2P Mechanism</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Use for Paypro Microapps --> Using SOAP</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String amount</li>
			<li>String to</li>
		</ul>
	 */
	public Response qrMerchantTransfer(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature, 
			@WebParam(name = "parameter") Parameter parameter);
	
	/**
	 * <b>P2P Transfer</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>SOAP</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param amount hashstring that contains initiator,pin and timestamp
	 * @param to hashstring that contains initiator,pin and timestamp
	 * @param extRef hashstring that contains initiator,pin and timestamp
	 */
	public Response moneyTransfer(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature, 
			@WebParam(name = "amount") String amount,
			@WebParam(name = "to") String to,
			@WebParam(name = "extRef") String extRef);
	
	/**
	 * <b>P2P Transfer with possibility to adding extra parameter</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO,UMB, PARTNER : [URL]/webapi/money_transfer</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String amount</li>
			<li>String to</li>
			<li>String extRef</li>
			<li>.... And other parameter</li>
		</ul>
	 */
	public Response moneyTransferExtParam(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature, 
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Inquiry Transaction for Airtime credit. Biller set on UTIBA</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO,UMB, PARTNER : [URL]/webapi/airtime_inquiry</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param to recepient of airtime credit
	 * @param amount denomination of airtime credit
	 * @param extRef partner transaction id
	 */
	public Response creditAirtimeInquiry(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "to") String to, 
			@WebParam(name = "amount") String amount,
			@WebParam(name = "extRef") String extRef);

	/**
	 * <b>Confirm Transaction for Airtime credit. Biller set on UTIBA</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO,UMB, PARTNER : [URL]/webapi/airtime_commit</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param serviceTransactionId UTIBA inquiry transId
	 * @param transactionId Partner transId
	 * @param operatorName airtime operator name (Indosat,telkomsel, etc)
	 */
	public Response commitCreditAirtime(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "serviceTransactionId") int serviceTransactionId, 
			@WebParam(name = "transactionId") String transactionId,
			@WebParam(name = "operatorName") String operatorName);

	/**
	 * <b>Confirm Transaction for Matrix Billpayment</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO,UMB, PARTNER : [URL]/webapi/confirm</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param transId UTIBA inquiry transId
	 */
	public Response confirm(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature, 
			@WebParam(name = "transId") int transId);

	/**
	 * <b>Inquiry Transaction for Matrix Billpayment</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO,UMB, PARTNER : [URL]/webapi/matrix_inquiry</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param target billpay billername
	 * @param to billpay account
	 */
	public Response matrixBillPay(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature, 
			@WebParam(name = "to") String to,
			@WebParam(name = "target") String target);

	/*public Response queryBillPay(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "to") String to, 
			@WebParam(name = "amount") String amount, 
			@WebParam(name = "target") String target);*/
	
	/**
	 * <b>Inquiry Transaction for Billpay Transaction, check billpay amount etc</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO,UMB, PARTNER : [URL]/webapi/query_billpay</li>
		  <li>INTERNAL : [URL]/webapiinternal/query_billpay</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param target billpay billername
	 * @param to billpay account
	 * @param parameterMap hashmap that consist of another parameter<br>
	 */
	public Response queryBillPay(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "target")String target, 
			@WebParam(name = "to")String to,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);

	/**
	 * <b>Commit Transaction for Billpay Transaction</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO,UMB, PARTNER : [URL]/webapi/billpay</li>
		  <li>INTERNAL : [URL]/webapiinternal/billpay</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param target billpay billername
	 * @param to billpay account
	 * @param amount denomination if billpay have it
	 * @param transId UTIBA inquiry transId
	 * @param parameterMap hashmap that consist of another parameter<br>
	 */
	public Response billPay(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature, 
			@WebParam(name = "target") String target,
			@WebParam(name = "to") String to, 
			@WebParam(name = "amount") String amount, 
			@WebParam(name = "transId") String transId,
			@WebParam(name = "parameterMap") HashMap<String, String> parameterMap);
	
	//Added By Ryanbhuled
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Upgrade agent to become Trusted, so agent can do Special Trx in Utiba (ex:/adjustment, join parent,etc)</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>INTERNAL : [URL]/webapiinternal/update_modifier</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String msisdn</li>
			<li>String status --> delayed time before processing request</li>
		</ul>
	 */
	public Response updateUserBecomeModifier(			
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Mapping and send SMS to agent, to give info of promotion</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>INTERNAL : [URL]/webapiinternal/prog_informer</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String msisdn</li>
			<li>String sleepTime</li> --> delayed time before processing request
			<li>String type</li> --> Send SMS or not
		</ul>
	 */
	public Response programInformer(			
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Registering or Update user data, and send new PIN generated by system to their Email</b>
	 * <br>
	 * No UTIBA financial transaction.
	 * <br>
	 * Use at:
	 * <ul>
		  <li>DOMPETKU+ : [URL]/webapi/reg_generate_pin</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String firstName</li>
			<li>String lastName</li>
			<li>String dob</li>
			<li>String gender</li>
			<li>String motherMaidenName</li>
			<li>String address</li>
			<li>String idType</li>
			<li>String idNo</li>
			<li>String email</li>
			<li>String msisdn</li>
		</ul>
	 */
	public Response registerGeneratePin(			
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Remove Agent from UTIBA Core</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>INTERNAL : [URL]/webapiinternal/remove_agent</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String msisdn</li>
		</ul>
	 */
	public Response removeAgent(			
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Check loyalty point(saved on UTIBA) of an Agent</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>DOMPETKU APPS : [URL]/webapi/check_point</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String msisdn</li>
		</ul>
	 */
	public Response checkPointDompetku(			
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Checking "list group" of an agent </b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>INTERNAL : [URL]/webapiinternal/check_grouping</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String msisdn</li>
		</ul>
	 */
	public Response checkGroupingName(			
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	// ============AJ Transfer to Bank======================
	/**
	 * <b>INQUIRY bankTransfer transaction to XGATE</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO APPS : [URL]/webapi/aj_inq</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String accTarget</li>
			<li>String amount</li>
			<li>String bankCode</li>
		</ul>
	 */
	public Response ajInquiry(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>CONFIRM bankTransfer transaction to XGATE</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO APPS : [URL]/webapi/aj_trf</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String token</li>
			<li>String accTarget</li>
			<li>String amount</li>
			<li>String bankCode</li>
			<li>String bankName</li>
			<li>String issName</li>
			<li>String destName</li>
			<li>String transID</li>
		</ul>
	 */
	public Response ajTransfer(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
//	public Response ajInquirySimulator(
//			@WebParam(name = "userId") String userId,
//			@WebParam(name = "signature") String signature,
//			@WebParam(name = "param") Parameter parameter);
//	
//	public Response ajTransferSimulator(
//			@WebParam(name = "userId") String userId,
//			@WebParam(name = "signature") String signature,
//			@WebParam(name = "param") Parameter parameter);
	
	// ============EKtp======================
//	public Response eKTP(
//			@WebParam(name = "userId") String userId,
//			@WebParam(name = "signature") String signature,
//			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Create connection to ADMINDUK,Strict Filter (+ MaidenName or NoKK)</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Internal Application : [URL]/webapiinternal/ektp</li>
		  <li>ALL KTP METHOD IN THIS MFSMW</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String idNumber</li>
		  	<li>String keyword --> MotherMaidenName or NoKK</li>
			<li>String msisdn</li>
		</ul>
	 */
	public Response eKTP(
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Check Data EKTP, WITH connection to ADMINDUK,Strict Filter (+ MaidenName or NoKK)</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Internal Application : [URL]/webapiinternal/ektp_confirm</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String idNumber</li>
		  	<li>String keyword --> MotherMaidenName or NoKK</li>
			<li>String msisdn</li>
			<li>String type --> If using "1", one KTP only for one user</li>
		</ul>
	 */
	public Response eKTPConfirm(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>KYC using EKTP, WITH connection to ADMINDUK,Strict Filter (+ MaidenName or NoKK), and Initiate by user</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UNUSED : [URL]/webapi/kyc_ektp_by_agent</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String idNumber</li>
		  	<li>String keyword --> MotherMaidenName or NoKK</li>
			<li>String msisdn</li>
			<li>String type --> If using "1", one KTP only for one user</li>
		</ul>
	 */
	public Response doKYCKTP(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>KYC using EKTP, WITH connection to ADMINDUK,Strict Filter (+ MaidenName or NoKK), and Initiate by agent</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>DOMPETKU : [URL]/webapi/kyc_ektp_by_agent</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String idNumber</li>
		  	<li>String keyword --> MotherMaidenName or NoKK</li>
			<li>String msisdn</li>
			<li>String type --> If using "1", one KTP only for one user</li>
		</ul>
	 */
	public Response doKYCKTPByAgent(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>KYC using EKTP, WITH connection to ADMINDUK,Strict Filter (+ MaidenName or NoKK), and do some Mapping and join Parent</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UMB : [URL]/webapi/kyc_ektp_by_agent_reff</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String idNumber</li>
		  	<li>String keyword --> MotherMaidenName or NoKK</li>
			<li>String msisdn</li>
			<li>String type --> If using "1", one KTP only for one user</li>
		</ul>
	 */
	public Response doKYCKTPByAgentWithReference(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>KYC using EKTP, WITH connection to ADMINDUK,Strict Filter (+ MaidenName or NoKK)</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>INDOSAT Internal apps : [URL]/webapiinternal/kyc_ektp_nameonly</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String idNumber</li>
		</ul>
	 */
	public Response eKTPConfirmResultNameOnly(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>KYC using EKTP, WITH connection to ADMINDUK,Strict Filter (+ MaidenName or NoKK)</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UNUSED : [URL]/webapi/kyc_ektp</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String idNumber</li>
		  	<li>String keyword --> MotherMaidenName or NoKK</li>
			<li>String msisdn</li>
			<li>String type --> If using "1", one KTP only for one user</li>
		</ul>
	 */
	public Response doKYCKTPStrict(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>KYC using EKTP, without connecting to ADMINDUK and use local filter</b>
	 * <br>Local Filter:
	 * <ul>
	 * 		<li>Check Kecamatan Number</li>
	 * 		<li>Check DOB</li>
	 * 		<li>Check Gender</li>
	 * </ul>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UMB and PAYPRO APPS : [URL]/webapi/kyc_ektp</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String idNumber</li>
		  	<li>String keyword --> MotherMaidenName or NoKK</li>
			<li>String msisdn</li>
			<li>String type --> If using "1", one KTP only for one user</li>
		</ul>
	 */
	public Response doKYCKTPTemp(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>KYC using EKTP, without connecting to ADMINDUK,Strict Filter (+ MaidenName or NoKK) and use local filter</b>
	 * <br>Local Filter:
	 * <ul>
	 * 		<li>Check Kecamatan Number</li>
	 * 		<li>Check DOB</li>
	 * 		<li>Check Gender</li>
	 * 		<li>Check Keyword (Mothermaidename & NoKK)</li>
	 * </ul>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UMB : [URL]/webapi/kyc_ektp_strict</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String idNumber</li>
		  	<li>String keyword --> MotherMaidenName or NoKK</li>
			<li>String msisdn</li>
			<li>String type --> If using "1", one KTP only for one user</li>
		</ul>
	 */
	public Response doKYCKTPStrictTemp(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	// ============API HOLD BALANCE======================
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Hold agent balance, put to partner wallet, using sell no confirm</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UNUSED : [URL]/webapi/transaction_reserve</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String amount</li>
		  	<li>String extRef</li>
			<li>String to --> agent to deduct</li>
		</ul>
	 */
	public Response hold(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Revesal hold transaction, Re-sell with new amount, using reversal + sell no confirm</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UNUSED : [URL]/webapi/transaction_confirm</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String extRefHold --> transid of HOLD Trx</li>
		  	<li>String amount</li>
		  	<li>String extRef</li>
			<li>String to --> agent to deduct</li>
		</ul>
	 */
	public Response holdConfirm(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Release hold transaction, using reversal</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UNUSED : [URL]/webapi/transaction_release</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String extRefHold --> transid of HOLD Trx</li>
		</ul>
	 */
	public Response holdRelease(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	// ============One Bill======================
	/**
	 * <b>Registering user to Onebill/Autodebit Function</b>
	 * No UTIBA function, just call onebill microapps via HTTP Get
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO APPS : [URL]/webapi/onebill_unreg</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String extRef</li>
			<li>String msisdn</li>
			<li>String to</li>
			<li>String date
			<li>String biller</li>
		</ul>
	 */
	public Response oneBillReg(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Update Onebill/Autodebit date for agent, based on msisdn & product</b>
	 * No UTIBA function, just call onebill microapps via HTTP Get
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO APPS : [URL]/webapi/onebill_update</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String extRef</li>
			<li>String msisdn</li>
			<li>String to</li>
			<li>String date</li>
			<li>String biller</li>
			<li>String amount</li>
		</ul>
	 */
	public Response oneBillUpdate(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);

	/**
	 * <b>Get list of product of Onebill/Autodebit Function that registered by an Agent based on msisdn</b>
	 * No UTIBA function, just call onebill microapps via HTTP Get
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO APPS : [URL]/webapi/onebill_unreg</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
			<li>String msisdn</li>
		</ul>
	 */
	public Response oneBillGetList(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);

	/**
	 * <b>Unregistering user from Onebill/Autodebit Function</b>
	 * No UTIBA function, just call onebill microapps via HTTP Get
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO APPS : [URL]/webapi/onebill_unreg</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String extRef</li>
			<li>String msisdn</li>
			<li>String to</li>
			<li>String date
			<li>String biller</li>
		</ul>
	 */
	public Response oneBillUnreg(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	// ============D-NUSANTARA======================
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Register an agent and update it into Premium account</b>
	 * Use at:
	 * <ul>
		  <li>NUSANTARA-Branchlessbanking : [URL]/webapiinternal/dnus_reg</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String name</li>
			<li>String idType</li>
			<li>String idNumber</li>
			<li>String address</li>
			<li>String dob</li>
			<li>String motherMaidenName</li>
			<li>String msisdn</li>
			<li>String email</li>
			<li>String type</li>
			<li>String groupid</li>
		</ul>
	 */
	public Response nusantaraReg(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Reset pin with new pin determined by user/initiator</b>
	 * Use at:
	 * <ul>
		  <li>NUSANTARA-Branchlessbanking : [URL]/webapiinternal/dnus_reset_pin</li>
		  <li>INTERNAL API : [URL]/webapiinternal/dnus_reset_pin</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String msisdn</li>
		  	<li>String newPin</li>
		</ul>
	 */
	public Response resetPinNusantara(			
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);

	/**
	 * <b>Insurance function</b>
	 * <ul>
	 * 		<li>Mapping on special group on utiba. Group identified each product/partner</li>
	 * 		<li>If it DBS product, will send special SMS</li>
	 * </ul>
	  * Use at:
	 * <ul>
		  <li>PAYPRO APPS : [URL]/webapiinternal/dnus_reff_map</li>
		  <li>INTERNAL API : [URL]/webapiinternal/dnus_reff_map</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String name</li>
		  	<li>String group</li>
		  	<li>String msisdn</li>
		</ul>
	 */
	public Response nusantaraReferralMapping(			
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Map or Unmapping agent from Utiba Group</b>
	  * Use at:
	 * <ul>
		  <li>INTERNAL API : [URL]/webapiinternal/map_unmap</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String msisdn</li>
		  	<li>String type (map/unmap)</li>
		  	<li>String groupid</li>
		</ul>
	 */
	public Response mapUnmap(			
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Modify agent on Utiba, only to change its category (ex:/merchant,etc)</b>
	  * Use at:
	 * <ul>
		  <li>Dompetku : [URL]/webapiinternal/update_admin</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String msisdn</li>
		  	<li>String name</li>
		  	<li>String category</li>
		  	<li>String isOutlet</li>
		</ul>
	 */
	public Response updateAdmin(			
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Direct hit UTIBA, using Utiba SOAP format as parameter</b>
	  * Use at:
	 * <ul>
		  <li>UNUSED : [URL]/webapiinternal/utiba_hit</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 */
	public Response directHitUtiba(				
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Get detail data of transaction, using PIPE (|) as delimiter on transaction detail</b>
	  * Use at:
	 * <ul>
		  <li>Dompetku : [URL]/webapiinternal/transaction_check</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param extRef UTIBA Transaction ID 
	 */
	public Response checkTransactionById(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "extRef") String extRef);
	
	/**
	 * <b>Registering user to Firebase Notification Engine</b>
	 * Firebase used by non-sms-supported user.
	 * Use at:
	 * <ul>
		  <li>Paypro & Dompetku : [URL]/webapiinternal/reg_firebase</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String trxid</li>
		  	<li>String deviceId --> get from android/ios device</li>
		</ul>
	 */	
	public Response registerFirebase(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Hit Outbound Billergw, to get non-trx functionality, such as for check PLN Token</b>
	 * This API only eligible for internal purpose
	 * Use at:
	 * <ul>
		  <li>INTERNAL API : [URL]/webapiinternal/billergw</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String msisdn</li>
		  	<li>And Another params, depends on what function need to call on outbound mw ...</li>
		</ul>
	 */	
	public Response billerGwTool(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Get detail transaction based on UTIBA trxid</b>
	 * All of transaction was categorized based on "Special" param, such as billername etc
	 * Use at:
	 * <ul>
		  <li>Paypro : [URL]/webapi/get_trx_detail</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String trxid</li>
		</ul>
	 */	
	public Response getTrxByTrxid(			
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "trxid") Parameter parameter);
	
	/**
	 * <h2>DEPRECATED</h2>
	 * <b>Reset pin user challenge by Keyword (mother maiden name/no_kk) and noktp</b>
	 * Use at:
	 * <ul>
		  <li>Dompetku : [URL]/webapi/forgotpin</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String msisdn</li>
			<li>String ektp</li>
			<li>String keyword (mothermaidenname or no_kk)</li>
		</ul>
	 */	
	public Response forgotPin(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Upgrade user to Premium (KYC) without KTP</b>
	 * Can use KTP,SIM,etc <br>
	 * encCode :Encoded String of msisdn | mothermaidenname<br>
	 * encText :Encoded String of msisdn | idphoto | idanduserphoto
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Paypro Apps : [URL]/webapi/kycnoktp</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  	<li>String msisdn</li>
			<li>String name</li>
			<li>String idno</li>
			<li>String idtype</li>
			<li>String countrycode</li>
			<li>String gender</li>
			<li>String dob</li>
			<li>String address</li>
			<li>String idphoto</li>
			<li>String idphotouser</li>
			<li>String keyword (mothermaidenname or no_kk)</li>
		</ul>
	 */	
	public Response doKYCNoKTP(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Get Sequence of whitelabel number based on partner code</b>
	 * No UTIBA function, just pure Database select
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Dompetku : [URL]/webapi/get_whitelabel_sequence</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  <li>String partnerCode</li>
		  <li>String partMsisdn (prefix for differentiate each partner ex:/Danamon 99906)</li>
		</ul>
	 */
	public Response getWhitelabelSequence(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Sequence management to differentiate "SAME" user number in each whitelabel partner</b>
	 * No UTIBA function, just pure Database select
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Dompetku : [URL]/webapi/set_whitelabel_sequence</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  <li>String partnerCode</li>
		  <li>String msisdn</li>
		  <li>String partMsisdn (prefix for differentiate each partner ex:/Danamon 99906)</li>
		</ul>
	 */
	public Response setWhitelabelSequence(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Check Trx based on External Reference with detail trx</b>
	 * Gives detail transaction consist of Result,TrxDate, TransID, Amont
	 * <br>
	 * Use at:
	 * <ul>
		  <li>API : [URL]/webapi/check_trx_extref</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param extRef Partner Transaction ID 
	 */
	public Response transQueryExtExtra(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "extRef") String extRef);
	
	/**
	 * <b>Get detail data of transaction,and put detail parameter on map</b>
	 * <br>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param transid UTIBA Transaction ID 
	 */
	public Response getDetailTrx(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "transid") String transid);
	
	/**
	 * <b>Reversal transaction based on external reference(Partner Transaction ID)</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>Kartuku : [URL]/webapi/rev_extref</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param extRef Partner Transaction ID 
	 * @param amount amount to reversal (Optional)
	 */
	public Response reversalExtRef(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "extRef") String extref,
			@WebParam(name = "amount") String amount);
	
	/**
	 * <b>doAdjustment wallet: Move balance from one wallet to another wallet</b>
	 * Only can be used by Eligible initiator in Utiba
	 * <br>
	 * Use at:
	 * <ul>
		  <li>wallet overhandler : VIA SOAP</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  <li>String amount</li>
		  <li>String source</li>
		  <li>String target</li>
		  <li>String desc</li>
		</ul>
	 */
	public Response adjustWallet(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Create merchant: Input data in MFSMW Database, and create merchant on Utiba Core</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>API MANAGER : [URL]/webapi/add_merchant</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  <li>Integer responseType</li>
		  <li>Integer channelType</li>
		  <li>String ipList</li>
		  <li>String apiList</li>
		</ul>
	 */
	public Response createNewMerchant(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Check Digest String sent by QR Scanner, validate, if Ok Deducting customer wallet. Initiate by Merchant/Partner</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PERTAMINA : [URL]/webapi/offline_qr_payment</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  <li>String extRef</li>
		  <li>String amount</li>
		  <li>String msisdn</li>
		  <li>String timestamp</li>
		  <li>String digest</li>
		</ul>
	 */
	public Response qrOfflineRequest(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "signature") String signature,
			@WebParam(name = "param") Parameter parameter);
	
	/**
	 * <b>Send Notification to Customer, Separate it using sms and Firebase</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>UMBGW : SOAP
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param parameter hashmap that consist of parameter:<br>
	 * <ul>
		  <li>String msisdn</li>
		  <li>String msg</li>
		</ul>
	 */
	public Response sendNotification(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "param") Parameter parameter);
	
	// ============DPU======================
//	public Response dmtMobilePickup(
//			@WebParam(name = "userId") String userId,
//			@WebParam(name = "signature") String signature,
//			@WebParam(name = "param") Parameter parameter);
	
	// ============Overhandling Limit======================
	public Response overHandlingWallet(String source,String target,Integer limit);	
	// public Response getTransactionById(@WebParam(name = "userId") String
	// userId, @WebParam(name = "signature") String signature, int id, int
	// requestType);

	/**
	 * <b>Confirm Transaction for ONLINE Payment</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PAYPRO,UMB, PARTNER : [URL]/webapi/confirmNoTransid</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 */
	public Response confirmNoTransid(String userId, String signature);

	/**
	 * <b>Payment/Purchase with confirmation (Using SELL Method) With Parameter --> PUSH USSD</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PARTNER for PUSH USSD : [URL]/webapi/do_payment_param</li>
		</ul>
	 * @param userId id for partner that use webapi. Refered to table api_users & api_merchant_users in database
	 * @param signature hashstring that contains initiator,pin and timestamp
	 * @param extRef partner transaction id
	 * @param extraParam adding parameter for info
	 * @param to agent to be deducted
	 * @param amount amount to be deducted
	 */
	public Response sellWithParam(String userId, String signature, Parameter parameter);

	/**
	 *  * <b>Check Child</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PARTNER for PUSH USSD : [URL]/webapi/check_child</li>
		</ul>
	 * @param userId
	 * @param signature
	 * @param parameter
	 * @return
	 */
	Response checkChild(String userId, String signature, Parameter parameter);
	
	/**
	 *  * <b>Lock lower limit for Blue Bird</b>
	 * <br>
	 * Use at:
	 * <ul>
		  <li>PARTNER for Locking Balance : [URL]/webapi/lock_balance</li>
		</ul>
	 * @param userId
	 * @param signature
	 * @param lowerlimit
	 * @return
	 */
	Response lockAmount(String userId, String signature, String lowerLimit, String msisdn);
}
