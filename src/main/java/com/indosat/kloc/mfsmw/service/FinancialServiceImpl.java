/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Sep 6, 2014 
 * Time       : 8:52:14 PM 
 */
package com.indosat.kloc.mfsmw.service;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.simmetrics.StringMetric;
import org.simmetrics.metrics.StringMetrics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.indosat.kloc.mfsmw.dao.ApiUserDao;
import com.indosat.kloc.mfsmw.dao.ReferralDao;
import com.indosat.kloc.mfsmw.dao.RemitCouponInqDao;
import com.indosat.kloc.mfsmw.dao.RemittanceSchedulerDao;
import com.indosat.kloc.mfsmw.dao.RemittanceUserDao;
import com.indosat.kloc.mfsmw.dao.ResetPassTokenDao;
import com.indosat.kloc.mfsmw.dao.SMSOutboxDao;
import com.indosat.kloc.mfsmw.dao.TransactionDMTDao;
import com.indosat.kloc.mfsmw.dao.TrxDMTFeeDao;
import com.indosat.kloc.mfsmw.dao.UserDao;
import com.indosat.kloc.mfsmw.dao.UserWalletDao;
import com.indosat.kloc.mfsmw.dao.WhitelabelSequenceDao;
import com.indosat.kloc.mfsmw.httpclient.HttpClientSender;
import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.model.Biller;
import com.indosat.kloc.mfsmw.model.EkycWorkaround;
import com.indosat.kloc.mfsmw.model.ExtraTransData;
import com.indosat.kloc.mfsmw.model.Prefix;
import com.indosat.kloc.mfsmw.model.Referral;
import com.indosat.kloc.mfsmw.model.ReferralType;
import com.indosat.kloc.mfsmw.model.RemitCouponInq;
import com.indosat.kloc.mfsmw.model.RemittancePartner;
import com.indosat.kloc.mfsmw.model.ResetPassToken;
import com.indosat.kloc.mfsmw.model.SMSOutbox;
import com.indosat.kloc.mfsmw.model.User;
import com.indosat.kloc.mfsmw.model.UserWallet;
import com.indosat.kloc.mfsmw.model.WhitelabelSequence;
import com.indosat.kloc.mfsmw.pojo.AgentData;
import com.indosat.kloc.mfsmw.pojo.AppParam;
import com.indosat.kloc.mfsmw.pojo.AuthResponse;
import com.indosat.kloc.mfsmw.pojo.DMTTransaction;
import com.indosat.kloc.mfsmw.pojo.Parameter;
import com.indosat.kloc.mfsmw.pojo.RemittanceUser;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.pojo.TransactionArtajasa;
import com.indosat.kloc.mfsmw.pojo.TransactionDMT;
import com.indosat.kloc.mfsmw.pojo.TransactionMail;
import com.indosat.kloc.mfsmw.processor.KycProcessor;
import com.indosat.kloc.mfsmw.processor.MultiBillerProcessor;
import com.indosat.kloc.mfsmw.processor.PayproMicroappsProcessor;
import com.indosat.kloc.mfsmw.processor.biller.BillerProcessor;
import com.indosat.kloc.mfsmw.sender.MailSender;
import com.indosat.kloc.mfsmw.sender.MailSenderHandler;
import com.indosat.kloc.mfsmw.sender.ResendSMSCouponRemitHandler;
import com.indosat.kloc.mfsmw.sender.SmsClientSender;
import com.indosat.kloc.mfsmw.soap.SessionHandler;
import com.indosat.kloc.mfsmw.soap.UTIBAHandler;
import com.indosat.kloc.mfsmw.util.AESEncryption;
import com.indosat.kloc.mfsmw.util.AeSimpleSha2;
import com.indosat.kloc.mfsmw.util.AuthenticationRequest;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DESedeEncryption;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.EmailNotificationBuilder;
import com.indosat.kloc.mfsmw.util.ImageHandler;
import com.indosat.kloc.mfsmw.util.SignatureBuilder;
import com.indosat.kloc.mfsmw.util.TrxHistList;
import com.indosat.kloc.mfsmw.util.Utils;
import com.oracle.xmlns.validationnik.ValidationNIK;

import umarketscws.KeyValuePairMap;

@Service("financialServiceImpl")
public class FinancialServiceImpl implements FinancialService {

	private static Log log = LogFactory.getLog(FinancialServiceImpl.class);

	// =====================temporary
	@Autowired
	private TransactionDMTDao transactionDMTDao;
	// =====================
	@Autowired
	private BillerProcessor billerProcessor;
	@Autowired
	private RemittanceSchedulerDao remittanceSchedulerDao;
	@Autowired
	private ResendSMSCouponRemitHandler resendSMSCouponRemitHandler;
	@Autowired
	private SMSOutboxDao smsOutboxDao;
	@Autowired
	private MultiBillerProcessor multiBillerProcessor;
	@Autowired
	private AuthenticationRequest authenticationRequest;
	@Autowired
	private UTIBAHandler utibaHandler;
	@Autowired
	private SessionHandler sessionHandler;
	@Autowired
	private DataStore dataStore;
	@Autowired
	private TrxHistList histList;
	@Autowired
	private UserDao userDao;
	@Autowired
	private UserWalletDao userWalletDao;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private SimpleMailMessage mailMessage;
	@Autowired
	private HttpClientSender httpClientSender;
	@Autowired
	private SmsClientSender smsClientSender;
	@Autowired
	private TaskExecutor sendExecutor;
	@Autowired
	private ResetPassTokenDao resetPassTokenDao;
	@Autowired
	private ImageHandler imageHandler;
	@Autowired
	private RemitCouponInqDao remitCouponInqDao;
	@Autowired
	private RemittanceUserDao remittanceUserDao;
	@Autowired
	private RemittanceSchedulerDao remitUserDao;
	@Autowired
	private WhitelabelSequenceDao whitelabelSeqDao;
	@Autowired
	private ReferralDao referalDao;
	@Autowired
	private ApiUserDao apiUserDao;
	@Autowired
	private TrxDMTFeeDao trxDMTFeeDao;
	@Autowired
	private DataStore dataManager;
	@Autowired
	private AppParam appParam;
	@Autowired
	private EmailNotificationBuilder emailBuilder;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private ValidationNIK ektpService;
	@Autowired
	private KycProcessor kycProc;
	@Autowired
	private PayproMicroappsProcessor microApps;
	@Autowired
	private HttpClientSender httpSender;


	@Value("${app.image.virtual.dir}")
	private String imagePath;

	@Value("${app.api.banktrf}")
	private String bankTrfURl;
	@Value("${app.api.p2p.url}")
	private String p2pTrfURL;
	@Value("${app.sms.in.789.url}")
	private String in789URL;

	@Value("${app.url.reg.activation}")
	private String activationURL;
	@Value("${app.url.forgetpass}")
	private String forgetPassURL;
	@Value("${app.utiba.url.wsdl.umarketsc.endpoint}")
	private String utibaURL;

	private String encKey = "OTt_ygRfn_UyR6_yh_76YrTU";

	@Value("${app.otp.coupon.expired.hour}")
	private int couponOTPHourExpired;

	@Value("${app.dmt.max.trx.amount}")
	private int dmtMaxTrxAmount;
	@Value("${app.dmt.min.trx.amount}")
	private int dmtMinTrxAmount;
	@Value("${app.emoney.max.trx.limit}")
	private int emoneyMaxLimitAmout;

	// temporary
	@Autowired
	private MailSenderHandler mailHandler;
	@Value("${app.email.subject.otp}")
	private String subjectReqOTP;
	@Value("${app.email.subject.reset.password}")
	private String subjectResetPass;
	@Value("${app.email.subject.activation}")
	private String subjectActEmail;
	@Value("${app.email.body.activation}")
	private String bodyActEmail;
	@Value("${app.email.subject.transaction}")
	private String subjectEmailTransaction;
	@Value("${app.email.subject.error}")
	private String subjectEmailTransactionError;
	@Value("${app.email.body.transaction}")
	private String bodyEmailTransaction;
	@Value("${app.email.body.transfer.receive}")
	private String bodyEmailReceiveTransfer;
	@Value("${app.email.body.forgetpass}")
	private String bodyEmailForgetPass;
	@Value("${app.email.body.account.notfound}")
	private String bodyEmailAccountNotfound;
	@Value("${app.email.body.otp}")
	private String bodyEmailOTP;

	@Value("${app.utiba.agentid.spcsubs}")
	private long spcSubsAgentId;
	@Value("${app.utiba.agentid.unreg.informer}")
	private long unregSubsInformer;
	// ==========

	@Value("${app.sms.otp}")
	private String smsNotifOtp;
	@Value("${app.sms.kyc.otp}")
	private String smsKycOtp;
	@Value("${app.sms.updateprofile.otp}")
	private String smsUpdateProf;
	@Value("${sms.notify.walletfull}")
	private String smsWalletfull;
	@Value("${app.sms.merchant.success}")
	private String smsMerchantSuccess;
	@Value("${app.sms.merchant.failed}")
	private String smsMerchantFailed;

	@Value("${app.sms.resetpin.otp}")
	private String smsResetPINOTP;

	@Value("${app.sms.notificatin.trx.failed}")
	private String smsTextFailed;

	@Value("${app.utiba.agentid.wallet.nonkyc}")
	private long nonKycAgentId;

	@Value("${app.sms.notification.pendingtransfer}")
	private String smsPendingTransferNotification;
	@Value("${app.text.callcenter}")
	private String callCenter;

	@Value("${app.sms.ktpkyc.success}")
	private String smsSuccessKYCKTP;
	@Value("${app.sms.ktpkyc.retry}")
	private String smsRetryKYCKTP;
	@Value("${app.sms.ktpkyc.failed}")
	private String smsFailedKYCKTP;
	@Value("${app.sms.remitt.movebalance}")
	private String smsRemitMoveBalance;
	@Value("${app.sms.info}")
	private String smsInfo;
	@Value("${app.sms.info.reminder}")
	private String smsInfoReminder;
	@Value("${app.utiba.initiator}")
	private String userAdmin;
	@Value("${app.utiba.pin}")
	private String passAdmin;
	@Value("${app.notif.firebase.url}")
	private String firebaseURL;
	@Value("${app.group.eligiblejoin}")
	private String eligibleJoinParent;
	@Value("${app.qroff.timelimit}")
	private long limitTimeQR=600000;
	@Value("${app.kyc.block}")
	private long kycBloc=1;
	@Value("${app.ba.eligible}")
	private String ambasador_eligible;
	@Value("${app.ba.eligible.active}")
	private String ambasador_eligible_active;
	@Value("${app.sms.ba.success}")
	private String ambassador_sms_success;
	
	@Value("${app.ba.eligible.ustad_evie}")
	private String ambasador_eligible_ustad_evie;
	@Value("${app.ba.eligible.ustad_evie.agent.parent}")
	private String ambasador_eligible_ustad_evie_agent_parent;
	
	//CRS
	@Value("${app.ektpmware.userid}")
	private String ektpmware_userid;
	@Value("${app.ektpmware.password}")
	private String ektpmware_password;
	@Value("${app.ektpmware.endpoint.crs}")
	private String endpoint_crs;
	@Value("${app.ektpmware.sms.notif.message}")
	private String nonisat_kyc_message;

	@Override
	public Response getNextID(String userId, String signature) {
		// TODO Auto-generated method stub
		Response response = new Response();
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = utibaHandler.getNextId(authResponse.getSessionId());
			response.setExtRef(authResponse.getSessionId());
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	public Response smsKeyword(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String keyword = parameter.getKeyValueMap().get(Constant.KEYWORD_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		if (msisdn.startsWith("0"))
			msisdn = "62" + msisdn.substring(1);

		if (Utils.isNullorEmptyString(keyword) || Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			String url = this.in789URL;
			url = url.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
			url = url.replace(Constant.SMS_REPLACE_TAG, keyword);
			url = url.replace(Constant.DATETIME_REPLACE_TAG, Utils.getCurrentTime());
			url = url.replace(Constant.TRANSACTIONID_REPLACE_TAG, Utils.getCurrentTime() + msisdn);

			ResponseEntity<String> re = httpClientSender.sendGet(url);
			if (re != null) {
				response.setTrxid(Utils.getCurrentTime() + msisdn);
				response.setStatus(Constant.RC_SUCCESS);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
			} else {
				response.setTrxid(Utils.getCurrentTime() + msisdn);
				response.setStatus(Constant.RC_TIMEOUT);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_TIMEOUT));
			}

		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	public Response companionCard(String userId, String signature, Parameter parameter) {
		Response response = new Response();

		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String serviceName = parameter.getKeyValueMap().get(Constant.COMP_CARD_SERVICE_NAME);

		if (Utils.isNullorEmptyString(msisdn) || Utils.isNullorEmptyString(serviceName)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			String sms = "";
			String xmlData = appParam.getCompCardGenericXML();
			ApiUser apiUser = dataStore.getApiUser(userId);
			xmlData = xmlData.replace(Constant.REFERENCE_REPLACE_TAG, serviceName);
			xmlData = xmlData.replace(Constant.MSISDN_REPLACE_TAG, msisdn);

			if (serviceName.equals(Constant.COMP_CARD_DELIVERY_REQUEST)) {
				/*
				 * String address =
				 * parameter.getKeyValueMap().get(Constant.ADDRESS_PARAM);
				 * String name =
				 * parameter.getKeyValueMap().get(Constant.NAME_PARAM);
				 * if(Utils.isNullorEmptyString(address) ||
				 * Utils.isNullorEmptyString(name)){
				 * response.setStatus(Constant.RC_INVALID_PARAMETERS);
				 * response.setMsg(dataStore.getErrorMsg(Constant.
				 * RC_INVALID_PARAMETERS)); return response; }
				 */

				Response agentResp = utibaHandler.getAgentByReferenceRequest(msisdn, apiUser);

				xmlData = appParam.getCompCardDeliveryReqXML();
				xmlData = xmlData.replace(Constant.REFERENCE_REPLACE_TAG, serviceName);
				xmlData = xmlData.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
				xmlData = xmlData.replace(Constant.ADDRESS_REPLACE_TAG,
						agentResp.getAgentData().getAddress() == null ? "" : agentResp.getAgentData().getAddress());
				xmlData = xmlData.replace(Constant.NAME_REPLACE_TAG, agentResp.getName());

			} else if (serviceName.equals(Constant.COMP_CARD_DELIVERY_STATUS)) {

				xmlData = appParam.getCompCardGenericXML();
				xmlData = xmlData.replace(Constant.REFERENCE_REPLACE_TAG, Constant.COMP_CARD_GET_STATUS);
				xmlData = xmlData.replace(Constant.MSISDN_REPLACE_TAG, msisdn);

				List<MediaType> mediaTypes = new LinkedList<MediaType>();
				mediaTypes.add(MediaType.APPLICATION_XML);
				HttpEntity<String> he = httpClientSender.sendPost(appParam.getCompCardURL(), xmlData, mediaTypes);
				if (he != null) {
					String code = Utils.getXmlTagValue(he.getBody(), "<rc>");
					log.debug("Code: " + code);
					if (code != null) {
						if (code.equals("00")) {
							sms = appParam.getCompCardCardStatusActive();
							response.setMsg(sms);
							log.info("SMS: " + sms);
							sms = sms + " Ref " + Utils.generateNumber(6);
						} else {
							xmlData = appParam.getCompCardGenericXML();
							xmlData = xmlData.replace(Constant.REFERENCE_REPLACE_TAG,
									Constant.COMP_CARD_DELIVERY_STATUS);
							xmlData = xmlData.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
							he = httpClientSender.sendPost(appParam.getCompCardURL(), xmlData, mediaTypes);
							code = Utils.getXmlTagValue(he.getBody(), "<rc>");
							if (code.equals("00")) {
								sms = appParam.getCompCardSMSTextDeliveryStatusSuccess();
							} else {
								sms = dataManager.getCompCardErrorMessage(code);
							}
						}

						smsClientSender.send(sms, msisdn);
						response.setStatus(Integer.valueOf(code));
						response.setMsg(sms);
					} else {
						response.setStatus(Constant.RC_TIMEOUT);
						response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
					}
				} else {
					response.setStatus(Constant.RC_TIMEOUT);
					response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
				}

				return response;
			}

			List<MediaType> mediaTypes = new LinkedList<MediaType>();
			mediaTypes.add(MediaType.APPLICATION_XML);
			HttpEntity<String> he = httpClientSender.sendPost(appParam.getCompCardURL(), xmlData, mediaTypes);
			if (he != null) {
				String code = Utils.getXmlTagValue(he.getBody(), "<rc>");
				log.debug("Code: " + code);
				if (code != null) {
					response.setStatus(Integer.valueOf(code));

					if (serviceName.equals(Constant.COMP_CARD_GET_STATUS)) {
						if (code.equals("00")) {
							sms = appParam.getCompCardCardStatusActive();
						} else {
							sms = dataManager.getCompCardErrorMessage(code);
						}
					} else if (serviceName.equals(Constant.COMP_CARD_DELIVERY_REQUEST)) {
						if (code.equals("00")) {
							sms = appParam.getCompCardSMSTextRequestCardSuccess();
						} else {
							sms = dataManager.getCompCardErrorMessage(code);
						}
					} else if (serviceName.equals(Constant.COMP_CARD_DELIVERY_STATUS)) {
						if (code.equals("00")) {
							sms = appParam.getCompCardSMSTextDeliveryStatusSuccess();
						} else {
							sms = dataManager.getCompCardErrorMessage(code);
						}
					} else if (serviceName.equals(Constant.COMP_CARD_ACTIVATION)) {
						if (code.equals("00")) {
							sms = appParam.getCompCardSMSTextActivationSuccess();
						} else {
							sms = dataManager.getCompCardErrorMessage(code);
						}
					} else if (serviceName.equals(Constant.COMP_CARD_DEACTIVATION)) {
						if (code.equals("00")) {
							sms = appParam.getCompCardSMSTextDeactivationSuccess();
						} else {
							sms = dataManager.getCompCardErrorMessage(code);
						}
					}

					response.setMsg(sms);
					log.info("SMS: " + sms);
					sms = sms + " Ref " + Utils.generateNumber(6);
					smsClientSender.send(sms, msisdn);

				} else {
					response.setStatus(Constant.RC_TIMEOUT);
					response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
				}
			} else {
				response.setStatus(Constant.RC_TIMEOUT);
				response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			}

		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;
	}

	public Response sellPoint(String userId, String signature, Parameter parameter) {
		Response response = new Response();

		String to = parameter.getKeyValueMap().get(Constant.TO_PARAM);
		String amount = parameter.getKeyValueMap().get(Constant.AMOUNT_PARAM);
		String extRef = parameter.getKeyValueMap().get(Constant.EXT_REF_PARAM);

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature) || Utils.isNullorEmptyString(to)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(extRef)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);

		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			ApiUser apiUser = dataStore.getApiUser(userId);
			response = utibaHandler.sellWithPoint(authResponse.getSessionId(), to, amount, null, extRef, apiUser);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;
	}

	public Response transQueryExt(String userId, String signature, String extRef) {
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(extRef)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser ap = dataStore.getApiUser(userId);
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = utibaHandler.transQueryExt(authResponse.getSessionId(), extRef, ap);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;
	}
	
	@Override
	public Response lockAmount(String userId, String signature, String lowerLimit, String msisdn) {
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(lowerLimit)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser ap = dataStore.getApiUser(userId);
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = utibaHandler.modifyLowerLimit(authResponse.getSessionId(), msisdn, new BigDecimal(lowerLimit), ap);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;
	}

	@Override
	public Response resendEmailActivation(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String email = parameter.getKeyValueMap().get(Constant.EMAIL_PARAM);

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(email)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			User user = userDao.get(email);
			if (user != null) {

				String url = activationURL.replace("[%KEY%]", user.getEncryptedText());
				String body = this.bodyActEmail;
				body = body.replace("[%NAME%]", user.getFirstName() + " " + user.getLastName());
				body = body.replace("[%URL%]", url);
				this.sendEmailNotification(email, this.subjectActEmail, body);

				response.setStatus(Constant.RC_SUCCESS);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;
	}

	@Override
	public Response nfcSettlement(String userId, String signature, HashMap<String, String> parameterMap) {
		Response response = new Response();

		String amount = parameterMap.get(Constant.AMOUNT_PARAM);
		String extRef = parameterMap.get(Constant.EXT_REF_PARAM);

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(extRef)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			String sessionId = sessionHandler.getValidGlobalNFCSession();

			ApiUser apiUser = dataStore.getApiUser(userId);
			response = utibaHandler.nfcSettlement(sessionId, amount, authResponse.getInitiator(), extRef, apiUser);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;
	}

	@Override
	public Response doKYCByAgent(String userId, String signature, HashMap<String, String> parameterMap) {
		Response response = new Response();

		String firstName = parameterMap.get(Constant.FIRSTNAME_PARAM);
		String lastName = parameterMap.get(Constant.LASTNAME_PARAM);
		String idType = parameterMap.get(Constant.ID_TYPE_PARAM);
		String idNumber = parameterMap.get(Constant.ID_NUMBER_PARAM);
		String address = parameterMap.get(Constant.ADDRESS_PARAM);
		String idPhoto = parameterMap.get(Constant.IDPHOTO_PARAM);
		String dob = parameterMap.get(Constant.DOB_PARAM);
		String motherMaidenName = parameterMap.get(Constant.MOTHER_NAME_PARAM);
		String gender = parameterMap.get(Constant.GENDER_PARAM);
		String to = parameterMap.get(Constant.TO_PARAM);
		String otp = parameterMap.get(Constant.OTP_PARAM);
		String type = parameterMap.get(Constant.TYPE_PARAM);

		if (Utils.isNullorEmptyString(firstName) || Utils.isNullorEmptyString(to) || Utils.isNullorEmptyString(idType)
				|| Utils.isNullorEmptyString(idNumber) || Utils.isNullorEmptyString(address)
				|| Utils.isNullorEmptyString(idPhoto) || Utils.isNullorEmptyString(dob)
				|| Utils.isNullorEmptyString(motherMaidenName) || Utils.isNullorEmptyString(gender)
				|| Utils.isNullorEmptyString(otp)) {

			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {

			ApiUser apiUser = dataStore.getApiUser(userId);
			Response otpResp = utibaHandler.cancelCoupon(dataManager.getGlobalSessionId(), otp,
					authResponse.getInitiator(), apiUser);
			if (otpResp.getStatus() != Constant.RC_SUCCESS) {
				response.setStatus(Constant.RC_INVALID_COUPON_USER);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_COUPON_USER));
				response.setTrxid(otpResp.getTrxid());
				return response;
			}

			String name = firstName + " " + lastName;

			umarketscws.KeyValuePairMap value = new KeyValuePairMap();

			if (!Utils.isNullorEmptyString(firstName)) {
				umarketscws.KeyValuePair kvFirstName = new umarketscws.KeyValuePair();
				kvFirstName.setKey(Constant.FIRSTNAME_PARAM);
				kvFirstName.setValue(firstName);
				value.getKeyValuePair().add(kvFirstName);
			}

			if (!Utils.isNullorEmptyString(lastName)) {
				umarketscws.KeyValuePair kvLastName = new umarketscws.KeyValuePair();
				kvLastName.setKey(Constant.LASTNAME_PARAM);
				kvLastName.setValue(lastName);
				value.getKeyValuePair().add(kvLastName);
			}

			if (!Utils.isNullorEmptyString(idType)) {
				umarketscws.KeyValuePair kvIdType = new umarketscws.KeyValuePair();
				kvIdType.setKey("id_type");
				kvIdType.setValue(idType);
				value.getKeyValuePair().add(kvIdType);
			}

			if (!Utils.isNullorEmptyString(idNumber)) {
				umarketscws.KeyValuePair kvIdNumber = new umarketscws.KeyValuePair();
				kvIdNumber.setKey("id_num");
				kvIdNumber.setValue(idNumber);
				value.getKeyValuePair().add(kvIdNumber);
			}

			if (!Utils.isNullorEmptyString(address)) {
				umarketscws.KeyValuePair kvAddress = new umarketscws.KeyValuePair();
				kvAddress.setKey(Constant.ADDRESS_PARAM);
				kvAddress.setValue(address);
				value.getKeyValuePair().add(kvAddress);
			}

			if (!Utils.isNullorEmptyString(idPhoto)) {
				umarketscws.KeyValuePair kvIdPhoto = new umarketscws.KeyValuePair();
				kvIdPhoto.setKey(Constant.IDPHOTO_PARAM);
				kvIdPhoto.setValue(this.imagePath + "/" + Utils.buildImageFilename(to, 1));
				value.getKeyValuePair().add(kvIdPhoto);
				imageHandler.writeImage(to, 1, idPhoto.replace(" ", "+"));
			}

			if (!Utils.isNullorEmptyString(dob)) {
				umarketscws.KeyValuePair kvDOB = new umarketscws.KeyValuePair();
				kvDOB.setKey("birthdate");
				kvDOB.setValue(dob);
				value.getKeyValuePair().add(kvDOB);
			}

			if (!Utils.isNullorEmptyString(motherMaidenName)) {
				// umarketscws.KeyValuePair kvMotherMaidenName = new
				// umarketscws.KeyValuePair();
				// kvMotherMaidenName.setKey("mmn");
				// kvMotherMaidenName.setValue(motherMaidenName);
				// value.getKeyValuePair().add(kvMotherMaidenName);
				//
				// umarketscws.KeyValuePair kvMotherMaidenNameDompetku = new
				// umarketscws.KeyValuePair();
				// kvMotherMaidenNameDompetku.setKey("alt_id");
				// kvMotherMaidenNameDompetku.setValue(motherMaidenName);
				// value.getKeyValuePair().add(kvMotherMaidenNameDompetku);

				umarketscws.KeyValuePair kvMotherMaidenNameDompetku = new umarketscws.KeyValuePair();
				kvMotherMaidenNameDompetku.setKey("mother");
				kvMotherMaidenNameDompetku.setValue(motherMaidenName);
				value.getKeyValuePair().add(kvMotherMaidenNameDompetku);

			}

			if (!Utils.isNullorEmptyString(gender)) {
				umarketscws.KeyValuePair kvGender = new umarketscws.KeyValuePair();
				kvGender.setKey(Constant.GENDER_PARAM);
				kvGender.setValue(gender);
				value.getKeyValuePair().add(kvGender);
			}

			response = utibaHandler.updateProfile(authResponse.getSessionId(), to, name, to, null, value, apiUser);
			if (!type.equals("1")) {
				if (response.getStatus() == Constant.RC_SUCCESS) {
					response = utibaHandler.unmapAgent(authResponse.getSessionId(), to, nonKycAgentId, apiUser);
				}
			}

		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;
	}

	@Override
	public Response inquiryKYC(String userId, String signature, String msisdn, String type) {

		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			ApiUser apiUser = dataStore.getApiUser(userId);
			Response getAgentByRequestResponse = utibaHandler.getAgentByReferenceRequest(msisdn, apiUser);
			if (getAgentByRequestResponse.getStatus() == Constant.RC_AGENT_NOTFOUND) {
				utibaHandler.register(msisdn, Utils.generateNumber(6), msisdn, msisdn, null, null, apiUser);
			} else {
				/*
				 * if(getAgentByRequestResponse.getAgentData().getWalletType()
				 * == Constant.WALLET_TYPE_PREMIUM){
				 * response.setStatus(Constant.RC_AGENT_ALREADY_REGISTERED);
				 * response.setMsg(dataStore.getErrorMsg(Constant.
				 * RC_AGENT_ALREADY_REGISTERED)); return response; }
				 */
			}

			String coupon = createCouponByHour(userId, signature, 1).getCouponId();
			String sms = "";
			if (type.equals("1"))
				sms = smsUpdateProf.replace(Constant.COUPON_REPLACE_TAG, coupon);
			else
				sms = smsKycOtp.replace(Constant.COUPON_REPLACE_TAG, coupon);

			smsClientSender.send(sms, msisdn);
			response.setStatus(Constant.RC_SUCCESS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response inquiryResetPINByAgent(String userId, String signature, String msisdn) {
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			String coupon = createCouponByHour(userId, signature, 1).getCouponId();
			String sms = smsResetPINOTP.replace(Constant.COUPON_REPLACE_TAG, coupon);
			smsClientSender.send(sms, msisdn);
			response.setStatus(Constant.RC_SUCCESS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response resetPinByAgent(String userId, String signature, HashMap<String, String> parameterMap) {
		Response response = new Response();
		String to = parameterMap.get(Constant.TO_PARAM);
		String otp = parameterMap.get(Constant.OTP_PARAM);
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature) || Utils.isNullorEmptyString(to)
				|| Utils.isNullorEmptyString(otp)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			ApiUser apiUser = dataStore.getApiUser(userId);
			Response otpResp = utibaHandler.cancelCoupon(dataManager.getGlobalSessionId(), otp,
					authResponse.getInitiator(), apiUser);
			if (otpResp.getStatus() != Constant.RC_SUCCESS) {
				response.setStatus(Constant.RC_INVALID_COUPON_USER);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_COUPON_USER));
				response.setTrxid(otpResp.getTrxid());
				return response;
			}
			response = utibaHandler.resetPin(to, apiUser);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;
	}

	@Override
	public Response resendSMSCouponRemit(String userId, String signature, String transId) {
		Response response = new Response();
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(transId)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			SMSOutbox outbox = smsOutboxDao.getByTransId(transId);
			if (outbox != null) {
				if (outbox.getCounter() > 0) {
					resendSMSCouponRemitHandler.send(outbox.getMsisdn(), outbox.getSms(), outbox.getTransId());
				}
				smsOutboxDao.updateCounter(transId);
			} else {
				response.setStatus(Constant.RC_TRANSACTION_NOT_FOUND);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_TRANSACTION_NOT_FOUND));
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;
	}

	@Override
	public Response getDMTUserByIdentityID(String userId, String signature, String idType, String idNo) {
		Response response = new Response();
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(idType) || Utils.isNullorEmptyString(idNo)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			RemittanceUser user = remittanceUserDao.getById(idType, idNo);
			if (user != null) {
				response.setRemittanceUser(user);
				response.setStatus(Constant.RC_SUCCESS);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
			} else {
				response.setStatus(Constant.RC_AGENT_NOTFOUND);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_AGENT_NOTFOUND));
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response getTransactionById(String userId, String signature, String transId) {
		Response response = new Response();
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(transId)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			ApiUser apiUser = dataStore.getApiUser(userId);
			response = utibaHandler.getTransactionById(authResponse.getSessionId(), Integer.valueOf(transId), 0,
					apiUser);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response inquiryCouponRemit(String userId, String signature, Parameter parameter) {

		long startTime = new Date().getTime();
		Response response = new Response();

		HashMap<String, String> param = parameter.getKeyValueMap();
		String extRef = param.get(Constant.EXT_REF_PARAM);

		String note = param.get(Constant.NOTE_PARAM);
		String uniqueCode = param.get(Constant.UNIQUE_CODE_PARAM);
		String storeName = param.get(Constant.STORE_NAME_PARAM);
		String amount = param.get(Constant.AMOUNT_PARAM);
		String recipientPhoneNumber = param.get(Constant.RECIPIENT_PHONE_NUMBER_PARAM);
		String recipientName = param.get(Constant.RECIPIENT_NAME_PARAM);
		String recipientSourceOfFund = param.get(Constant.RECIPIENT_SOURCE_OF_FUND_PARAM);
		String recipientPurpose = param.get(Constant.RECIPIENT_PURPOSE_PARAM);
		String recipientAddress = param.get(Constant.RECIPIENT_ADDRESS_PARAM);
		String recipientProvince = param.get(Constant.RECIPIENT_PROVINCE_PARAM);
		String recipientCity = param.get(Constant.RECIPIENT_CITY_PARAM);
		String senderName = param.get(Constant.SENDER_NAME_PARAM);
		String senderPhoneNumber = param.get(Constant.SENDER_PHONE_NUMBER_PARAM);
		String senderIdType = param.get(Constant.SENDER_ID_TYPE_PARAM);
		String senderIdNo = param.get(Constant.SENDER_ID_NO_PARAM);
		String senderIdExp = param.get(Constant.SENDER_ID_EXP_PARAM);
		String senderGender = param.get(Constant.SENDER_GENDER_PARAM);
		String senderAddress = param.get(Constant.SENDER_ADDRESS_PARAM);
		String senderProvince = param.get(Constant.SENDER_PROVINCE_PARAM);
		String senderCity = param.get(Constant.SENDER_CITY_PARAM);
		String senderPlaceOfBirth = param.get(Constant.SENDER_PLACE_OF_BIRTH);
		String senderDateOfBirth = param.get(Constant.SENDER_DATE_OF_BIRTH);
		String senderOccupation = param.get(Constant.SENDER_OCCUPATION);
		String senderNationality = param.get(Constant.SENDER_NATIONALITY);
		String senderAggreeToReg = param.get(Constant.SENDER_AGGREE_TO_REGISTER_DOMPETKU);

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(storeName) || Utils.isNullorEmptyString(amount)
				|| Utils.isNullorEmptyString(recipientPhoneNumber) || Utils.isNullorEmptyString(recipientName)
				|| Utils.isNullorEmptyString(recipientSourceOfFund) || Utils.isNullorEmptyString(recipientPurpose)
				|| Utils.isNullorEmptyString(senderName) || Utils.isNullorEmptyString(senderPhoneNumber)
				|| Utils.isNullorEmptyString(senderIdType) || Utils.isNullorEmptyString(senderIdNo)
				|| Utils.isNullorEmptyString(senderIdExp) || Utils.isNullorEmptyString(senderGender)
				|| Utils.isNullorEmptyString(senderAddress) || Utils.isNullorEmptyString(senderProvince)
				|| Utils.isNullorEmptyString(senderCity) || Utils.isNullorEmptyString(senderPlaceOfBirth)
				|| Utils.isNullorEmptyString(senderDateOfBirth) || Utils.isNullorEmptyString(senderOccupation)
				|| Utils.isNullorEmptyString(senderNationality)) {

			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;

		}

		if (Integer.valueOf(amount) > dmtMaxTrxAmount) {
			response.setStatus(Constant.RC_AMOUNT_TO_BIG);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_AMOUNT_TO_BIG));
			return response;
		}

		if (Integer.valueOf(amount) < dmtMinTrxAmount) {
			response.setStatus(Constant.RC_AMOUNT_TO_SMALL);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_AMOUNT_TO_SMALL));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {

			RemittanceUser user = remittanceUserDao.getById(senderIdType, senderIdNo);
			if (user == null) {
				user = new RemittanceUser();
				user.setIdType(senderIdType);
				user.setIdNo(senderIdNo);
				user.setIdExp(senderIdExp);
				user.setName(senderName);
				user.setPhoneNumber(senderPhoneNumber);
				user.setGender(senderGender);
				user.setAddress(senderAddress);
				user.setProvince(senderProvince);
				user.setCity(senderCity);
				user.setPob(senderPlaceOfBirth);
				user.setDob(senderDateOfBirth);
				user.setOccupation(senderOccupation);
				user.setNationality(senderNationality);
				user.setAggreeReg(senderAggreeToReg);
				remittanceUserDao.save(user);
			} else {
				user.setIdType(user.getIdType());
				user.setIdNo(user.getIdNo());
				user.setIdExp(senderIdExp);
				user.setPhoneNumber(senderPhoneNumber);
				user.setGender(senderGender);
				user.setAddress(senderAddress);
				user.setProvince(senderProvince);
				user.setCity(senderCity);
				user.setPob(senderPlaceOfBirth);
				user.setDob(senderDateOfBirth);
				user.setOccupation(senderOccupation);
				user.setNationality(senderNationality);
				user.setAggreeReg(senderAggreeToReg);
				int up = remittanceUserDao.update(user);
				log.debug("update result: " + up);
			}

			response = utibaHandler.getNextId(authResponse.getSessionId());
			if (response.getStatus() == Constant.RC_SUCCESS) {
				RemitCouponInq couponInq = new RemitCouponInq();
				couponInq.setSessionId(authResponse.getSessionId());
				couponInq.setTransId(Integer.valueOf(response.getTrxid()));
				couponInq.setExtRef(extRef);
				couponInq.setStoreName(storeName);
				couponInq.setAmount(amount);
				couponInq.setRecipientPhoneNumber(recipientPhoneNumber);
				couponInq.setRecipientName(recipientName);
				couponInq.setRecipientSourceOfFund(recipientSourceOfFund);
				couponInq.setRecipientPurpose(recipientPurpose);
				couponInq.setRecipientAddress(recipientAddress);
				couponInq.setRecipientProvince(recipientProvince);
				couponInq.setRecipientCity(recipientCity);
				couponInq.setSenderIdType(senderIdType);
				couponInq.setSenderIdNo(senderIdNo);
				couponInq.setSenderName(senderName);
				couponInq.setSenderPhoneNumber(senderPhoneNumber);
				couponInq.setUniqueCode(uniqueCode);
				couponInq.setNote(note);
				int result = remitCouponInqDao.save(couponInq);
				if (result <= 0) {
					response.setStatus(Constant.RC_DB_ERROR);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_DB_ERROR));
				}
				// response.setFee(trxDMTFeeDao.getFee(amount));
				
				if(!Utils.isNullorEmptyString(uniqueCode)){
					if(uniqueCode.toUpperCase().startsWith("FREE"))
						response.setFee("0");
					else
						response.setFee(trxDMTFeeDao.getFee(amount, authResponse.getInitiator()));
				}else{
					response.setFee(trxDMTFeeDao.getFee(amount, authResponse.getInitiator()));
				}
				
			} else {
				response.setStatus(Constant.RC_BILLER_GENERAL_ERROR);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_BILLER_GENERAL_ERROR));
			}

			// =============Save Transaction=========================
			DMTTransaction trx = new DMTTransaction();
			trx.setTransId(response.getTrxid());
			trx.setExtRef(extRef);
			trx.setInitiator(authResponse.getInitiator());
			// trx.setCouponId(response.getCouponId());
			trx.setStoreName(storeName);
			trx.setRecipientPhoneNumber(recipientPhoneNumber);
			trx.setRecipientName(recipientName);
			trx.setRecipientSourceOfFund(recipientSourceOfFund);
			trx.setRecipientPurpose(recipientPurpose);
			trx.setRecipientAddress(recipientAddress);
			trx.setRecipientProvince(recipientProvince);
			trx.setRecipientCity(recipientCity);
			trx.setSenderIdType(senderIdType);
			trx.setSenderIdNo(senderIdNo);
			trx.setSenderPhoneNumber(senderPhoneNumber);
			trx.setAmount(amount);
			String fee = response.getFee();
			String total = String.valueOf(Integer.valueOf(fee) + Integer.valueOf(amount));
			trx.setFee(fee);
			trx.setTotal(total);
			trx.setStatusMessage(response.getMsg());
			trx.setStatus(response.getStatus());
			trx.setType(Constant.DMT_REQUEST_TYPE_CREATE_COUPON_INQUIRY);
			trx.setUniqueCode(uniqueCode);
			trx.setNote(note);
			trx.setRespTime(new Date().getTime() - startTime);
			histList.writeDMTHist(trx);
			// ======================================================

		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response commitCouponRemit(String userId, String signature, String transId, String extRef) {

		long startTime = new Date().getTime();
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(transId) || Utils.isNullorEmptyString(extRef)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;

		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			RemitCouponInq couponInq = remitCouponInqDao.getByTransId(transId, extRef);
			ApiUser apiUser = dataStore.getApiUser(userId);
			if (couponInq != null) {
				RemittanceUser user = remittanceUserDao.getById(couponInq.getSenderIdType(), couponInq.getSenderIdNo());
				HashMap<String, String> parameters = new HashMap<String, String>();
				parameters.put(Constant.STORE_NAME_PARAM_LITE, couponInq.getStoreName());
				parameters.put(Constant.RECIPIENT_SOURCE_OF_FUND_PARAM_LITE, couponInq.getRecipientSourceOfFund());
				parameters.put(Constant.RECIPIENT_PURPOSE_PARAM_LITE, couponInq.getRecipientPurpose());
				parameters.put(Constant.RECIPIENT_PHONE_NUMBER_PARAM_LITE, couponInq.getRecipientPhoneNumber());
				/*
				 * parameters.put(Constant.RECIPIENT_ADDRESS_PARAM_LITE,
				 * couponInq.getRecipientAddress());
				 * parameters.put(Constant.RECIPIENT_PROVINCE_PARAM_LITE,
				 * couponInq.getRecipientProvince());
				 * parameters.put(Constant.RECIPIENT_CITY_PARAM_LITE,
				 * couponInq.getRecipientCity());
				 */
				parameters.put(Constant.SENDER_PHONE_NUMBER_PARAM_LITE, user.getPhoneNumber());
				parameters.put(Constant.SENDER_ID_TYPE_PARAM_LITE, couponInq.getSenderIdType());
				parameters.put(Constant.SENDER_ID_NO_PARAM_LITE, couponInq.getSenderIdNo());
				parameters.put(Constant.SENDER_ID_EXP_PARAM_LITE, user.getIdExp());
				parameters.put(Constant.SENDER_GENDER_PARAM_LITE, user.getGender());
				parameters.put(Constant.SENDER_ADDRESS_PARAM_LITE, user.getAddress());
				parameters.put(Constant.SENDER_PROVINCE_PARAM_LITE, user.getProvince());
				parameters.put(Constant.SENDER_CITY_PARAM_LITE, user.getCity());
				parameters.put(Constant.SENDER_PLACE_OF_BIRTH_LITE, user.getPob());
				parameters.put(Constant.SENDER_DATE_OF_BIRTH_LITE, user.getDob());
				parameters.put(Constant.SENDER_OCCUPATION_LITE, user.getOccupation());
				parameters.put(Constant.SENDER_NATIONALITY_LITE, user.getNationality());
				parameters.put(Constant.EXT_REF_PARAM, extRef);

				response = utibaHandler.createCouponRemitance(couponInq.getSessionId(), couponInq.getAmount(),
						user.getPhoneNumber(), couponInq.getRecipientName(), couponInq.getSenderName(), extRef,
						parameters, apiUser);
				if (response.getStatus() == Constant.RC_SESSION_EXPIRED) {
					response = utibaHandler.createCouponRemitance(authResponse.getSessionId(), couponInq.getAmount(),
							user.getPhoneNumber(), couponInq.getRecipientName(), couponInq.getSenderName(), extRef,
							parameters, apiUser);
				}

				// =============Save Transaction=========================
				DMTTransaction trx = new DMTTransaction();
				trx.setTransId(response.getTrxid());
				trx.setExtRef(extRef);
				trx.setInitiator(authResponse.getInitiator());
				trx.setCouponId(response.getCouponId());
				trx.setStoreName(couponInq.getStoreName());
				trx.setRecipientPhoneNumber(couponInq.getRecipientPhoneNumber());
				trx.setRecipientName(couponInq.getRecipientName());
				trx.setRecipientSourceOfFund(couponInq.getRecipientSourceOfFund());
				trx.setRecipientPurpose(couponInq.getRecipientPurpose());
				trx.setSenderIdType(couponInq.getSenderIdType());
				trx.setSenderIdNo(couponInq.getSenderIdNo());
				trx.setSenderPhoneNumber(couponInq.getSenderPhoneNumber());
				trx.setAmount(couponInq.getAmount());
				String fee = trxDMTFeeDao.getFee(couponInq.getAmount(), authResponse.getInitiator());
				String total = String.valueOf(Integer.valueOf(fee) + Integer.valueOf(couponInq.getAmount()));
				trx.setFee(fee);
				trx.setTotal(total);
				trx.setStatusMessage(response.getMsg());
				trx.setStatus(response.getStatus());
				trx.setType(Constant.DMT_REQUEST_TYPE_CREATE_COUPON_COMMIT);
				trx.setUniqueCode(couponInq.getUniqueCode());
				trx.setNote(couponInq.getNote());
				trx.setRespTime(new Date().getTime() - startTime);
				histList.writeDMTHist(trx);
				// ======================================================

				if (response.getStatus() == Constant.RC_SUCCESS) {
					response.setDetailTagihan(this.callCenter);
					// response.setCouponId(null);
					remitCouponInqDao.updateStatus(transId, extRef);
				}

			} else {
				response.setStatus(Constant.RC_TRANSACTION_NOT_FOUND);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_TRANSACTION_NOT_FOUND));
			}

		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response cancelCoupon(String userId, String signature, String couponId, String initiator) {
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(couponId)) {

			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;

		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			ApiUser apiUser = dataStore.getApiUser(userId);
			response = utibaHandler.cancelCoupon(authResponse.getSessionId(), couponId, initiator, apiUser);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response createCouponRemitance(String userId, String signature, Parameter parameter) {
		long startTime = new Date().getTime();
		Response response = new Response();
		HashMap<String, String> param = parameter.getKeyValueMap();

		String note = param.get(Constant.NOTE_PARAM);
		String uniqueCode = param.get(Constant.UNIQUE_CODE_PARAM);
		String parentId = param.get(Constant.PARENT_ID_PARAM);
		String extRef = param.get(Constant.EXT_REF_PARAM);
		String storeName = param.get(Constant.STORE_NAME_PARAM);
		String amount = param.get(Constant.AMOUNT_PARAM);
		String recipientPhoneNumber = param.get(Constant.RECIPIENT_PHONE_NUMBER_PARAM);
		String recipientName = param.get(Constant.RECIPIENT_NAME_PARAM);
		String recipientSourceOfFund = param.get(Constant.RECIPIENT_SOURCE_OF_FUND_PARAM);
		String recipientPurpose = param.get(Constant.RECIPIENT_PURPOSE_PARAM);
		String recipientAddress = param.get(Constant.RECIPIENT_ADDRESS_PARAM);
		String recipientProvince = param.get(Constant.RECIPIENT_PROVINCE_PARAM);
		String recipientCity = param.get(Constant.RECIPIENT_CITY_PARAM);
		String senderName = param.get(Constant.SENDER_NAME_PARAM);
		String senderPhoneNumber = param.get(Constant.SENDER_PHONE_NUMBER_PARAM);
		String senderIdType = param.get(Constant.SENDER_ID_TYPE_PARAM);
		String senderIdNo = param.get(Constant.SENDER_ID_NO_PARAM);
		String senderIdExp = param.get(Constant.SENDER_ID_EXP_PARAM);
		String senderGender = param.get(Constant.SENDER_GENDER_PARAM);
		String senderAddress = param.get(Constant.SENDER_ADDRESS_PARAM);
		String senderProvince = param.get(Constant.SENDER_PROVINCE_PARAM);
		String senderCity = param.get(Constant.SENDER_CITY_PARAM);
		String senderPlaceOfBirth = param.get(Constant.SENDER_PLACE_OF_BIRTH);
		String senderDateOfBirth = param.get(Constant.SENDER_DATE_OF_BIRTH);
		String senderOccupation = param.get(Constant.SENDER_OCCUPATION);
		String senderNationality = param.get(Constant.SENDER_NATIONALITY);

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(storeName) || Utils.isNullorEmptyString(amount)
				|| Utils.isNullorEmptyString(recipientName) || Utils.isNullorEmptyString(recipientSourceOfFund)
				|| Utils.isNullorEmptyString(recipientPurpose) || Utils.isNullorEmptyString(senderName)
				|| Utils.isNullorEmptyString(senderPhoneNumber) || Utils.isNullorEmptyString(senderIdType)
				|| Utils.isNullorEmptyString(senderIdNo) || Utils.isNullorEmptyString(senderIdExp)
				|| Utils.isNullorEmptyString(senderGender) || Utils.isNullorEmptyString(senderAddress)
				|| Utils.isNullorEmptyString(senderProvince) || Utils.isNullorEmptyString(senderCity)
				|| Utils.isNullorEmptyString(senderPlaceOfBirth) || Utils.isNullorEmptyString(senderDateOfBirth)
				|| Utils.isNullorEmptyString(senderOccupation) || Utils.isNullorEmptyString(senderNationality)) {

			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;

		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			RemittanceUser user = remittanceUserDao.getById(senderIdType, senderIdNo);
			ApiUser apiUser = dataStore.getApiUser(userId);
			if (user == null) {
				user = new RemittanceUser();
				user.setIdType(senderIdType);
				user.setIdNo(senderIdNo);
				user.setIdExp(senderIdExp);
				user.setName(senderName);
				user.setPhoneNumber(senderPhoneNumber);
				user.setGender(senderGender);
				user.setAddress(senderAddress);
				user.setProvince(senderProvince);
				user.setCity(senderCity);
				user.setPob(senderPlaceOfBirth);
				user.setDob(senderDateOfBirth);
				user.setOccupation(senderOccupation);
				user.setNationality(senderNationality);
				remittanceUserDao.save(user);
			} else {
				user.setIdType(user.getIdType());
				user.setIdNo(user.getIdNo());
				user.setIdExp(senderIdExp);
				user.setPhoneNumber(senderPhoneNumber);
				user.setGender(senderGender);
				user.setAddress(senderAddress);
				user.setProvince(senderProvince);
				user.setCity(senderCity);
				user.setPob(senderPlaceOfBirth);
				user.setDob(senderDateOfBirth);
				user.setOccupation(senderOccupation);
				user.setNationality(senderNationality);
				int up = remittanceUserDao.update(user);
				log.debug("update result: " + up);
			}

			/*
			 * response =
			 * utibaHandler.createCouponRemitance(authResponse.getSessionId(),
			 * amount, recipientPhoneNumber, recipientName, senderName,
			 * senderPhoneNumber, senderIdNo, storeName);
			 */

			HashMap<String, String> parameters = new HashMap<String, String>();
			parameters.put(Constant.STORE_NAME_PARAM_LITE, storeName);
			parameters.put(Constant.RECIPIENT_SOURCE_OF_FUND_PARAM_LITE, recipientSourceOfFund);
			parameters.put(Constant.RECIPIENT_PURPOSE_PARAM_LITE, recipientPurpose);
			parameters.put(Constant.RECIPIENT_PHONE_NUMBER_PARAM_LITE, recipientPhoneNumber);
			/*
			 * parameters.put(Constant.RECIPIENT_ADDRESS_PARAM_LITE,
			 * recipientAddress);
			 * parameters.put(Constant.RECIPIENT_PROVINCE_PARAM_LITE,
			 * recipientProvince);
			 * parameters.put(Constant.RECIPIENT_CITY_PARAM_LITE,
			 * recipientCity);
			 */
			parameters.put(Constant.SENDER_PHONE_NUMBER_PARAM_LITE, user.getPhoneNumber());
			parameters.put(Constant.SENDER_ID_TYPE_PARAM_LITE, senderIdType);
			parameters.put(Constant.SENDER_ID_NO_PARAM_LITE, senderIdNo);
			parameters.put(Constant.SENDER_ID_EXP_PARAM_LITE, user.getIdExp());
			parameters.put(Constant.SENDER_GENDER_PARAM_LITE, user.getGender());
			parameters.put(Constant.SENDER_ADDRESS_PARAM_LITE, user.getAddress());
			parameters.put(Constant.SENDER_PROVINCE_PARAM_LITE, user.getProvince());
			parameters.put(Constant.SENDER_CITY_PARAM_LITE, user.getCity());
			parameters.put(Constant.SENDER_PLACE_OF_BIRTH_LITE, user.getPob());
			parameters.put(Constant.SENDER_DATE_OF_BIRTH_LITE, user.getDob());
			parameters.put(Constant.SENDER_OCCUPATION_LITE, user.getOccupation());
			parameters.put(Constant.SENDER_NATIONALITY_LITE, user.getNationality());

			response = utibaHandler.createCouponRemitance(authResponse.getSessionId(), amount, user.getPhoneNumber(),
					recipientName, senderName, extRef, parameters, apiUser);

			// =============Save Transaction=========================
			DMTTransaction trx = new DMTTransaction();
			trx.setParentId(parentId);
			trx.setTransId(response.getTrxid());
			trx.setExtRef(extRef);
			trx.setInitiator(authResponse.getInitiator());
			trx.setCouponId(response.getCouponId());
			trx.setStoreName(authResponse.getInitiator());
			trx.setRecipientPhoneNumber(recipientPhoneNumber);
			trx.setRecipientName(recipientName);
			trx.setRecipientSourceOfFund(recipientSourceOfFund);
			trx.setRecipientPurpose(recipientPurpose);
			trx.setRecipientAddress(recipientAddress);
			trx.setRecipientProvince(recipientProvince);
			trx.setRecipientCity(recipientCity);
			trx.setSenderIdType(senderIdType);
			trx.setSenderIdNo(senderIdNo);
			trx.setSenderPhoneNumber(senderPhoneNumber);
			trx.setAmount(amount);
			String fee = trxDMTFeeDao.getFee(amount);
			String total = String.valueOf(Integer.valueOf(fee) + Integer.valueOf(amount));
			trx.setFee(fee);
			trx.setTotal(total);
			trx.setStatusMessage(response.getMsg());
			trx.setStatus(response.getStatus());
			trx.setType(Constant.DMT_REQUEST_TYPE_CREATE_COUPON);
			trx.setUniqueCode(uniqueCode);
			trx.setNote(note);
			trx.setRespTime(new Date().getTime() - startTime);
			histList.writeDMTHist(trx);
			// ======================================================

		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	public Response wallet2Cash(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		HashMap<String, String> param = parameter.getKeyValueMap();

		String extRef = param.get(Constant.EXT_REF_PARAM);
		String storeName = param.get(Constant.STORE_NAME_PARAM);
		String amount = param.get(Constant.AMOUNT_PARAM);
		String recipientPhoneNumber = param.get(Constant.RECIPIENT_PHONE_NUMBER_PARAM);
		String recipientName = param.get(Constant.RECIPIENT_NAME_PARAM);

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() != Constant.RC_SUCCESS) {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
			return response;
		}

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(storeName) || Utils.isNullorEmptyString(amount)
				|| Utils.isNullorEmptyString(recipientName)) {

			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));

			String smsText = smsTextFailed;
			smsText = smsText.replace(Constant.DATE_REPLACE_TAG, Utils.getCurrentDate("dd/MM/yy HH:mm"));
			smsText = smsText.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Input tidak lengkap.");
			smsClientSender.send(smsText, authResponse.getInitiator());

			return response;

		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		Response agentResponse = utibaHandler.getAgentByReferenceRequest(authResponse.getInitiator(), apiUser);
		if (agentResponse.getStatus() == Constant.RC_SUCCESS
				&& !agentResponse.getAgentData().getCategory().equalsIgnoreCase("subscriber")) {
			response.setStatus(Constant.RC_RESTRICTED_CHANNEL);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_RESTRICTED_CHANNEL));
			String smsText = smsTextFailed;
			smsText = smsText.replace(Constant.DATE_REPLACE_TAG, Utils.getCurrentDate("dd/MM/yy HH:mm"));
			smsText = smsText.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
					"Anda tidak diperbolehkan menggunakan fitur ini.");
			smsClientSender.send(smsText, authResponse.getInitiator());
			return response;
		}

		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(Constant.STORE_NAME_PARAM_LITE, storeName);
		parameters.put(Constant.SENDER_PHONE_NUMBER_PARAM_LITE, authResponse.getInitiator());

		if (!Utils.isNullorEmptyString(agentResponse.getAgentData().getIdNo())) {
			parameters.put(Constant.SENDER_ID_NO_PARAM_LITE, agentResponse.getAgentData().getIdNo());
			if (agentResponse.getAgentData().getIdNo().length() == 16) {
				parameters.put(Constant.SENDER_ID_TYPE_PARAM_LITE, "1");
			}
		}

		agentResponse.getAgentData().getIdNo();
		// parameters.put(Constant.RECIPIENT_PHONE_NUMBER_PARAM_LITE,
		// recipientPhoneNumber);
		// Response getAgentbyRefResp =
		// utibaHandler.getAgentByReferenceRequest(authResponse.getInitiator());
		// response =
		// utibaHandler.createCouponRemitance(authResponse.getSessionId(),
		// amount, "123456", recipientName,
		// getAgentbyRefResp.getName(), extRef, parameters);
		response = utibaHandler.createCouponRemitance(authResponse.getSessionId(), amount, "123456", recipientName,
				agentResponse.getName(), extRef, parameters, apiUser);

		// =============Save Transaction=========================
		DMTTransaction trx = new DMTTransaction();
		trx.setTransId(response.getTrxid());
		trx.setExtRef(extRef);
		trx.setInitiator(authResponse.getInitiator());
		trx.setCouponId(response.getCouponId());
		trx.setStoreName(authResponse.getInitiator());
		trx.setRecipientPhoneNumber(recipientPhoneNumber);
		trx.setRecipientName(recipientName);
		trx.setSenderPhoneNumber(authResponse.getInitiator());
		trx.setAmount(amount);
		String fee = trxDMTFeeDao.getFee(amount);
		String total = String.valueOf(Integer.valueOf(fee) + Integer.valueOf(amount));
		trx.setFee(fee);
		trx.setTotal(total);
		trx.setStatusMessage(response.getMsg());
		trx.setStatus(response.getStatus());
		trx.setType(Constant.DMT_REQUEST_TYPE_WALLET2CASH);
		histList.writeDMTHist(trx);
		// ======================================================

		return response;
	}

	public Response wallet2CashInquiry(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		HashMap<String, String> param = parameter.getKeyValueMap();
		String extRef = param.get(Constant.EXT_REF_PARAM);
		String storeName = param.get(Constant.STORE_NAME_PARAM);
		String amount = param.get(Constant.AMOUNT_PARAM);
		String recipientPhoneNumber = param.get(Constant.RECIPIENT_PHONE_NUMBER_PARAM);
		String recipientName = param.get(Constant.RECIPIENT_NAME_PARAM);

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() != Constant.RC_SUCCESS) {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
			return response;
		}

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(storeName) || Utils.isNullorEmptyString(amount)
				|| Utils.isNullorEmptyString(recipientName)) {

			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));

			String smsText = smsTextFailed;
			smsText = smsText.replace(Constant.DATE_REPLACE_TAG, Utils.getCurrentDate("dd/MM/yy HH:mm"));
			smsText = smsText.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Input tidak lengkap.");
			smsClientSender.send(smsText, authResponse.getInitiator());

			return response;

		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		Response agentResponse = utibaHandler.getAgentByReferenceRequest(authResponse.getInitiator(), apiUser);
		if (agentResponse.getStatus() == Constant.RC_SUCCESS
				&& !agentResponse.getAgentData().getCategory().equalsIgnoreCase("subscriber")) {
			response.setStatus(Constant.RC_RESTRICTED_CHANNEL);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_RESTRICTED_CHANNEL));
			String smsText = smsTextFailed;
			smsText = smsText.replace(Constant.DATE_REPLACE_TAG, Utils.getCurrentDate("dd/MM/yy HH:mm"));
			smsText = smsText.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
					"Anda tidak diperbolehkan menggunakan fitur ini.");
			smsClientSender.send(smsText, authResponse.getInitiator());
			return response;
		}

		response.setTrxid("0");
		response.setStatus(Constant.RC_SUCCESS);
		response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
		response.setFee(trxDMTFeeDao.getFee(amount));

		return response;
	}

	@Override
	public Response getCouponRemitance(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		HashMap<String, String> param = parameter.getKeyValueMap();

		String couponId = param.get(Constant.COUPONID_PARAM);
		String storeName = param.get(Constant.STORE_NAME_PARAM);

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(couponId)) {

			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;

		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			ApiUser apiUser = dataStore.getApiUser(userId);
			response = utibaHandler.getCouponRemitance(authResponse.getSessionId(), couponId, apiUser);
			// =============Save Transaction=========================
			DMTTransaction trx = new DMTTransaction();
			trx.setTransId(response.getTrxid());
			trx.setInitiator(authResponse.getInitiator());
			trx.setCouponId(couponId);
			trx.setStoreName(storeName);
			trx.setStatusMessage(response.getMsg());
			trx.setStatus(response.getStatus());
			trx.setAmount(response.getCouponDetails().getAmount());
			trx.setSenderPhoneNumber(response.getCouponDetails().getSenderPhoneNumber());
			trx.setType(Constant.DMT_REQUEST_TYPE_INQUIRY_COUPON_REMIT);
			histList.writeDMTHist(trx);
			// ======================================================
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response cancelCouponRemitance(String userId, String signature, Parameter parameter) {
		long startTime = new Date().getTime();
		Response response = new Response();
		HashMap<String, String> param = parameter.getKeyValueMap();
		String parentId = param.get(Constant.PARENT_ID_PARAM);
		String couponId = param.get(Constant.COUPONID_PARAM);
		String password = param.get(Constant.PASSWORD_PARAM);
		String extRef = param.get(Constant.EXT_REF_PARAM);
		String storeName = param.get(Constant.STORE_NAME_PARAM);
		String note = param.get(Constant.NOTE_PARAM);
		String uniqueCode = param.get(Constant.UNIQUE_CODE_PARAM);

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(couponId) || Utils.isNullorEmptyString(password)
				|| Utils.isNullorEmptyString(extRef)) {

			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;

		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		ApiUser apiUser = dataStore.getApiUser(userId);
		Response getCouponResponse = utibaHandler.getCouponRemitance(authResponse.getSessionId(), couponId, apiUser);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = utibaHandler.cancelCouponRemitance(authResponse.getSessionId(), couponId, password, extRef,
					storeName, apiUser);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		// =============Save Transaction=========================
		DMTTransaction trx = new DMTTransaction();
		trx.setParentId(parentId);
		trx.setTransId(response.getTrxid());
		trx.setExtRef(extRef);
		trx.setInitiator(authResponse.getInitiator());
		trx.setCouponId(couponId);
		trx.setStoreName(storeName);
		trx.setRecipientPhoneNumber(getCouponResponse.getCouponDetails().getRecipientPhoneNumber());
		trx.setRecipientName(getCouponResponse.getCouponDetails().getRecipientName());
		trx.setSenderIdType(getCouponResponse.getCouponDetails().getIdType());
		trx.setSenderIdNo(getCouponResponse.getCouponDetails().getIdentityNo());
		trx.setSenderPhoneNumber(getCouponResponse.getCouponDetails().getRecipientPhoneNumber());
		trx.setAmount(getCouponResponse.getCouponDetails().getAmount());
		trx.setStatusMessage(response.getMsg());
		trx.setStatus(response.getStatus());
		trx.setType(Constant.DMT_REQUEST_TYPE_CANCEL_COUPON_REMIT);
		trx.setUniqueCode(uniqueCode);
		trx.setNote(note);
		trx.setRespTime(new Date().getTime() - startTime);
		histList.writeDMTHist(trx);
		// ======================================================

		return response;
	}

	@Override
	public Response remitCoupon(String userId, String signature, Parameter parameter) {
		long startTime = new Date().getTime();
		Response response = new Response();
		HashMap<String, String> param = parameter.getKeyValueMap();
		String couponId = param.get(Constant.COUPONID_PARAM);
		String extRef = param.get(Constant.EXT_REF_PARAM);
		String storeName = param.get(Constant.STORE_NAME_PARAM);
		String note = param.get(Constant.NOTE_PARAM);
		String uniqueCode = param.get(Constant.UNIQUE_CODE_PARAM);
		String parentId = param.get(Constant.PARENT_ID_PARAM);
		String recipientIdType = param.get(Constant.RECIPIENT_ID_TYPE_PARAM);
		String recipientIdNo = param.get(Constant.RECIPIENT_ID_NO_PARAM);
		String recipientIdExp = param.get(Constant.RECIPIENT_ID_EXP_PARAM);
		String recipientName = param.get(Constant.RECIPIENT_NAME_PARAM);
		String recipientGender = param.get(Constant.RECIPIENT_GENDER_PARAM);
		String recipientAddress = param.get(Constant.RECIPIENT_ADDRESS_PARAM);
		String recipientProvince = param.get(Constant.RECIPIENT_PROVINCE_PARAM);
		String recipientCity = param.get(Constant.RECIPIENT_CITY_PARAM);
		String recipientPhoneNumber = param.get(Constant.RECIPIENT_PHONE_NUMBER_PARAM);
		String recipientPlaceOfBirth = param.get(Constant.RECIPIENT_PLACE_OF_BIRTH);
		String recipientDateOfBirth = param.get(Constant.RECIPIENT_DATE_OF_BIRTH);
		String recipientOccupation = param.get(Constant.RECIPIENT_OCCUPATION);
		String recipientNationality = param.get(Constant.RECIPIENT_NATIONALITY);
		String recipientAggreeReg = param.get(Constant.RECIPIENT_AGGREE_TO_REGISTER_DOMPETKU);

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(couponId) || Utils.isNullorEmptyString(storeName)
				|| Utils.isNullorEmptyString(extRef) || Utils.isNullorEmptyString(recipientIdType)
				|| Utils.isNullorEmptyString(recipientIdNo) || Utils.isNullorEmptyString(recipientIdExp)
				|| Utils.isNullorEmptyString(recipientGender) || Utils.isNullorEmptyString(recipientAddress)
				|| Utils.isNullorEmptyString(recipientProvince) || Utils.isNullorEmptyString(recipientCity)
				|| Utils.isNullorEmptyString(recipientPlaceOfBirth) || Utils.isNullorEmptyString(recipientDateOfBirth)
				|| Utils.isNullorEmptyString(recipientOccupation) || Utils.isNullorEmptyString(recipientNationality)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		RemittanceUser user = remittanceUserDao.getById(recipientIdType, recipientIdNo);

		/*
		 * if(user == null){ if(Utils.isNullorEmptyString(recipientIdType) ||
		 * Utils.isNullorEmptyString(recipientIdNo)
		 * ||Utils.isNullorEmptyString(recipientIdExp) ||
		 * Utils.isNullorEmptyString(recipientGender) ||
		 * Utils.isNullorEmptyString(recipientAddress) ||
		 * Utils.isNullorEmptyString(recipientProvince) ||
		 * Utils.isNullorEmptyString(recipientCity) ||
		 * Utils.isNullorEmptyString(recipientPlaceOfBirth) ||
		 * Utils.isNullorEmptyString(recipientDateOfBirth) ||
		 * Utils.isNullorEmptyString(recipientOccupation) ||
		 * Utils.isNullorEmptyString(recipientNationality)){
		 * 
		 * response.setStatus(Constant.RC_INVALID_PARAMETERS);
		 * response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS)
		 * ); return response;
		 * 
		 * } }
		 */

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			ApiUser apiUser = dataStore.getApiUser(userId);
			Response getCouponResponse = utibaHandler.getCouponRemitance(authResponse.getSessionId(), couponId,
					apiUser);

			if (user == null) {
				user = new RemittanceUser();
				user.setIdType(recipientIdType);
				user.setIdNo(recipientIdNo);
				user.setIdExp(recipientIdExp);
				user.setName(recipientName);
				user.setPhoneNumber(recipientPhoneNumber);
				user.setGender(recipientGender);
				user.setAddress(recipientAddress);
				user.setProvince(recipientProvince);
				user.setCity(recipientCity);
				user.setPob(recipientPlaceOfBirth);
				user.setDob(recipientDateOfBirth);
				user.setOccupation(recipientOccupation);
				user.setNationality(recipientNationality);
				user.setAggreeReg(recipientAggreeReg);
				remittanceUserDao.save(user);
			} else {
				user.setIdType(user.getIdType());
				user.setIdNo(user.getIdNo());
				user.setIdExp(recipientIdExp);
				user.setPhoneNumber(recipientPhoneNumber);
				user.setGender(recipientGender);
				user.setAddress(recipientAddress);
				user.setProvince(recipientProvince);
				user.setCity(recipientCity);
				user.setPob(recipientPlaceOfBirth);
				user.setDob(recipientDateOfBirth);
				user.setOccupation(recipientOccupation);
				user.setNationality(recipientNationality);
				user.setAggreeReg(recipientAggreeReg);
				remittanceUserDao.update(user);
			}

			if (getCouponResponse.getStatus() == Constant.RC_SUCCESS) {
				HashMap<String, String> parameters = new HashMap<String, String>();
				parameters.put(Constant.STORE_NAME_PARAM_LITE, storeName);
				parameters.put(Constant.RECIPIENT_ID_TYPE_PARAM_LITE, recipientIdType);
				parameters.put(Constant.RECIPIENT_ID_NO_PARAM_LITE, recipientIdNo);
				parameters.put(Constant.RECIPIENT_ID_EXP_PARAM_LITE, recipientIdExp);
				parameters.put(Constant.RECIPIENT_NAME_PARAM_LITE, recipientName);
				parameters.put(Constant.RECIPIENT_GENDER_PARAM_LITE, recipientGender);
				parameters.put(Constant.RECIPIENT_ADDRESS_PARAM_LITE, recipientAddress);
				parameters.put(Constant.RECIPIENT_PROVINCE_PARAM_LITE, recipientProvince);
				parameters.put(Constant.RECIPIENT_CITY_PARAM_LITE, recipientCity);
				parameters.put(Constant.RECIPIENT_PHONE_NUMBER_PARAM_LITE, recipientPhoneNumber);
				parameters.put(Constant.RECIPIENT_PLACE_OF_BIRTH_LITE, recipientPlaceOfBirth);
				parameters.put(Constant.RECIPIENT_DATE_OF_BIRTH_LITE, recipientDateOfBirth);
				parameters.put(Constant.RECIPIENT_OCCUPATION_LITE, recipientOccupation);
				parameters.put(Constant.RECIPIENT_NATIONALITY_LITE, recipientNationality);
				parameters.put(Constant.EXT_REF_PARAM, extRef);
				response = utibaHandler.remiteCoupon(authResponse.getSessionId(), couponId, extRef, parameters,
						apiUser);
			}

			// =============Save Transaction=========================
			DMTTransaction trx = new DMTTransaction();
			trx.setParentId(parentId);
			trx.setTransId(response.getTrxid());
			trx.setExtRef(extRef);
			trx.setInitiator(authResponse.getInitiator());
			trx.setCouponId(couponId);
			trx.setStoreName(storeName);
			trx.setRecipientPhoneNumber(getCouponResponse.getCouponDetails().getRecipientPhoneNumber());
			trx.setRecipientName(recipientName);
			trx.setSenderIdType(getCouponResponse.getCouponDetails().getIdType());
			trx.setSenderIdNo(getCouponResponse.getCouponDetails().getIdentityNo());
			trx.setSenderPhoneNumber(getCouponResponse.getCouponDetails().getRecipientPhoneNumber());
			String amount = getCouponResponse.getCouponDetails().getAmount();
			trx.setAmount(amount);
			// String fee = trxDMTFeeDao.getFee(amount);
			// String total = String.valueOf(Integer.valueOf(fee) +
			// Integer.valueOf(amount));
			// trx.setFee(fee);
			// trx.setTotal(total);
			trx.setStatusMessage(response.getMsg());
			trx.setStatus(response.getStatus());
			trx.setType(Constant.DMT_REQUEST_TYPE_COUPON_REMIT);
			trx.setUniqueCode(uniqueCode);
			trx.setNote(note);
			trx.setRespTime(new Date().getTime() - startTime);
			histList.writeDMTHist(trx);
			// ======================================================

		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response oneBillSell(String userId, String key, Parameter parameter) {
		Response response = new Response();
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser == null) {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			return response;
		}

		if (!apiUser.getPassKey().equals(key)) {
			response.setStatus(Constant.RC_LOGIN_FAILED);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_LOGIN_FAILED));
			return response;
		}

		return response;
	}

	@Override
	public Response oneBillInq(String userId, String key, Parameter parameter) {
		Response response = new Response();
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser == null) {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			return response;
		}

		if (!apiUser.getPassKey().equals(key)) {
			response.setStatus(Constant.RC_LOGIN_FAILED);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_LOGIN_FAILED));
			return response;
		}

		HashMap<String, String> parameterMap = parameter.getKeyValueMap();
		// response =
		// multiBillerProcessor.inquiry(parameterMap.get(Constant.INITIATOR_PARAM),parameterMap);
		response = multiBillerProcessor.inquiryBillpay(userId, key, parameterMap);

		return response;
	}

	@Override
	public Response oneBillPay(String userId, String key, Parameter parameter) {
		Response response = new Response();
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser == null) {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			return response;
		}

		if (!apiUser.getPassKey().equals(key)) {
			response.setStatus(Constant.RC_LOGIN_FAILED);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_LOGIN_FAILED));
			return response;
		}

		HashMap<String, String> parameterMap = parameter.getKeyValueMap();
		// response =
		// multiBillerProcessor.pay(parameterMap.get(Constant.INITIATOR_PARAM),
		// parameterMap);
		response = multiBillerProcessor.paymentBillpay(userId, key, parameterMap);

		return response;
	}

	@Override
	public Response billerInq(String userId, String signature, HashMap<String, String> parameterMap) {

		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = multiBillerProcessor.inquiry(authResponse.getInitiator(), parameterMap);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;

	}

	@Override
	public Response billerPay(String userId, String signature, HashMap<String, String> parameterMap) {

		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = multiBillerProcessor.pay(authResponse.getInitiator(), parameterMap);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;
	}

	@Override
	public Response OTPCounter(String userId, String to) {
		log.info("Request getOtpLife: userId[" + userId + "]  to[" + to + "] ");
		Response response = new Response();
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(to)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			int counter = dataStore.getOtpCounter(to);
			response.setCounter(String.valueOf(counter));
			response.setStatus(Constant.RC_SUCCESS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response forgetPassword(String userId, String to) {

		log.info("Request forgetPassword: userId[" + userId + "]  to[" + to + "] ");

		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(to)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			Response inquiryAcc = this.balanceCheck(userId, to);
			if (inquiryAcc.getStatus() == Constant.RC_SUCCESS) {
				StringBuilder parameters = new StringBuilder(userId).append("|").append(to)
						.append(Utils.getCurrentTime());
				String token = null;
				String encryptedText = null;
				try {

					token = resetPassTokenDao.getToken(to);
					if (token == null) {
						encryptedText = AeSimpleSha2.SHA2(parameters.toString()).toUpperCase();
						token = URLEncoder.encode(encryptedText, "UTF-8");
					}
					// log.debug("token: " + token);
					String url = forgetPassURL;
					url = url.replace("[%KEY%]", token);
					url = url.replace("[%EMAIL%]", to);
					String body = this.bodyEmailForgetPass;
					body = body.replace("[%NAME%]", inquiryAcc.getName());
					body = body.replace("[%URL%]", url);

					this.sendEmailNotification(to, this.subjectResetPass, body);

					ResetPassToken resetPassToken = new ResetPassToken();
					resetPassToken.setInitiator(to);
					resetPassToken.setToken(encryptedText);
					resetPassTokenDao.save(resetPassToken);
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_REGISTER, to, "0",
							"0", 0);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				String body = this.bodyEmailAccountNotfound;
				body = body.replace(Constant.EMAIL_REPLACE_TAG, to);
				this.sendEmailNotification(to, this.subjectEmailTransaction, body);
			}

			response.setStatus(Constant.RC_SUCCESS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));

		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	@Override
	public Response p2pTransfer(String userId, String signature, String to, String amount) {
		Response response = new Response();
		log.info("Request OTP: userId[" + userId + "] signatiru[XXXXXXX] to[" + to + "] amount[" + amount + "]");
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature) || Utils.isNullorEmptyString(to)
				|| Utils.isNullorEmptyString(amount)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					try {
						String url = p2pTrfURL;
						String sms = "P2P " + to + " " + amount;
						url = url.replace("[%SMS%]", URLEncoder.encode(sms, "UTF-8"));
						url = url.replace("[%MSISDN%]", getCredential[0]);

						final String p2pURL = url;
						Thread t = new Thread(new Runnable() {
							public void run() {
								httpClientSender.sendGet(p2pURL);
							}
						});
						t.start();
						response.setStatus(Constant.RC_SUCCESS);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));

					} catch (Exception e) {
						// TODO: handle exception
						response.setStatus(Constant.RC_SYSTEM_ERROR);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
					}

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	};

	@Override
	public Response requestOTP(String userId, String signature, int count) {
		Response response = new Response();
		log.info("Request OTP: userId[" + userId + "] signatiru[XXXXXXX]");

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					String username = getCredential[0];
					int counter = dataStore.getOtpCounter(username);

					if (counter > 0) {
						int totalOTP = count;
						List<String> couponList = new ArrayList<String>();
						for (int i = 0; i < totalOTP; i++) {
							Response couponResp = utibaHandler.createCoupon(sessionId, couponOTPHourExpired,
									String.valueOf(apiUser.getChannelType()), apiUser);
							couponList.add(couponResp.getCouponId());
						}

						// response.setCoupon(coupon);
						response.setStatus(Constant.RC_SUCCESS);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
						response.setCounter(String.valueOf(counter - 1));
						dataStore.updateOTPCounter(username, counter - 1);
						// log.debug("+++++++COUPON: " + couponList.toString());
						Date date = Utils.getExpiredTokenTime(couponOTPHourExpired);
						String expiredDateTime = Utils.formatDate(date, "dd/MM/yy HH:mm:ss");
						String msisdn = this.doUserInquiry(userId, signature, username).getAgentData().getSmsAddress();
						String sms = smsNotifOtp.replace(Constant.COUPON_REPLACE_TAG, couponList.toString());
						sms = sms.replace(Constant.DATETIME_REPLACE_TAG, expiredDateTime);
						smsClientSender.sendForDPlus(sms, msisdn);
						/*
						 * String body = this.bodyEmailOTP; body =
						 * body.replace(Constant.COUPON_REPLACE_TAG,
						 * couponList.toString()); body =
						 * body.replace(Constant.DATETIME_REPLACE_TAG,
						 * expiredDateTime);
						 * this.sendEmailNotification(username,
						 * this.subjectReqOTP, body);
						 */

					} else {
						response.setStatus(Constant.RC_OTP_EXCEEDED);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_OTP_EXCEEDED));
					}

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response bankTransfer(String userId, String signature, String bankCode, String accountNo, String amount) {
		Response response = new Response();
		log.info("Request doLogin:: userId[" + userId + "] signature[XXXXXXX] " + "pin[XXXXX] bankCode[" + bankCode
				+ "] accountNo[" + accountNo + "] amount[" + amount + "]");
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(bankCode) || Utils.isNullorEmptyString(accountNo)
				|| Utils.isNullorEmptyString(amount)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					String trfBankUrl = this.bankTrfURl;
					String msisdn = getCredential[0];
					if (msisdn.startsWith("0"))
						msisdn = "62" + msisdn.substring(1);

					trfBankUrl = trfBankUrl.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
					trfBankUrl = trfBankUrl.replace(Constant.PIN_REPLACE_TAG, getCredential[1]);
					trfBankUrl = trfBankUrl.replace(Constant.BANK_CODE_REPLACE_TAG, bankCode);
					trfBankUrl = trfBankUrl.replace(Constant.ACC_NO_REPLACE_TAG, accountNo);
					trfBankUrl = trfBankUrl.replace(Constant.AMOUNT_REPLACE_TAG, amount);
					trfBankUrl = trfBankUrl.replace(Constant.DATETIME_REPLACE_TAG,
							Utils.getCurrentDate("yyyyMMddHHmmssSS"));

					log.info("Bank Transfer Request :: msisdn[" + msisdn + "] bankCode[" + bankCode + "] accountNo["
							+ accountNo + "] amount[" + amount + "]");

					ResponseEntity<String> re = httpClientSender.sendGet(trfBankUrl);
					if (re.getStatusCode() != null && re.getStatusCode().value() == 200) {
						response.setStatus(Constant.RC_SUCCESS);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
					} else {
						response.setStatus(Constant.RC_BAD_REQUEST);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_BAD_REQUEST));
					}

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}
	
	@Override
	public Response doLogin(String userId, String signature) {
		Response response = new Response();
		log.info("Request doLogin:: userId[" + userId + "] signature[XXXXXXX]");
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				
//				if(apiUser.getUserId().equalsIgnoreCase("mobile_apps_elasitas")){
//					response.setStatus(Constant.RC_CLOSING_DOMPETKU);
//					response.setMsg(dataStore.getErrorMsg(Constant.RC_CLOSING_DOMPETKU));
//					return response;
//				}
				// if (apiUser.getGroupId() == Constant.GROUPID_MYNT &&
				// StringUtils.isNumeric(getCredential[0])) {
				// response.setStatus(Constant.RC_RESTRICTED_CHANNEL);
				// response.setMsg(dataStore.getErrorMsg(Constant.RC_RESTRICTED_CHANNEL));
				// return response;
				// }

				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					if (!StringUtils.isNumeric(getCredential[0])) {
						log.debug("MASUK SINI");
						User user = userDao.get(getCredential[0]);
						if (user != null)
							response.setAccountId(user.getId());
					}

					response.setStatus(Constant.RC_SUCCESS);
					response.setTrxid("000000");
					response.setMsg(dataStore.getErrorMsg(validateSession));

					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_LOGIN,
							getCredential[0], "0000", response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	@Override
	public Response changePin(String userId, String signature, String newPin) {
		log.info("Change Pin Request: userId[" + userId + "] signature[XXXXXXX] ");
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(newPin)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				String sessionId = sessionHandler.getSessionId();
				String username = getCredential[0];
				String pin = getCredential[1];
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					response = utibaHandler.changePIN(sessionId, username, pin, newPin, apiUser);
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_GET_USER_INFO,
							getCredential[0], "0", response.getTrxid(), response.getStatus());
				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}

			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response resetPin(String userId, String signature) {
		log.info("resetPin Request: userId[" + userId + "] signature[XXXXXXX] ");
		Response response = new Response();
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				String initiator = getCredential[0];
				String pin = getCredential[1];
				response = utibaHandler.resetPIN(initiator, pin, apiUser);
				// ==================temporary
				// if (!StringUtils.isNumeric(getCredential[0])) {
				if (Utils.isValidEmailAddress2(getCredential[0])) {
					TransactionMail transactionMail = new TransactionMail();
					transactionMail.setTransactionTypeINA(Constant.TRANSACTION_RESET_PASSWORD);
					transactionMail.setTransactionTypeEN(Constant.TRANSACTION_RESET_PASSWORD);
					transactionMail.setDestination(initiator);
					transactionMail.setAmount(null);
					transactionMail.setTransId(response.getTrxid());
					transactionMail.setStatus(response.getStatus().intValue());
					mailHandler.sendEmailNotification(getCredential[0], subjectEmailTransaction, transactionMail,
							apiUser);
					// smsClientSender.sendMyntNotificaton(getCredential[0],
					// transactionMail);
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	@Override
	public Response simpleReg(String userId, String signature, HashMap<String, String> parameterMap) {

		Response response = new Response();

		String firstName = parameterMap.get(Constant.FIRSTNAME_PARAM);
		String lastName = parameterMap.get(Constant.LASTNAME_PARAM);
		String dob = parameterMap.get(Constant.DOB_PARAM);
		String gender = parameterMap.get(Constant.GENDER_PARAM);
		String motherMaidenName = parameterMap.get(Constant.MOTHER_NAME_PARAM);
		String address = parameterMap.get(Constant.ADDRESS_PARAM);
		String idType = parameterMap.get(Constant.ID_TYPE_PARAM);
		String idNo = parameterMap.get(Constant.ID_NUMBER_PARAM);
		String msisdn = parameterMap.get(Constant.MSISDN_PARAM);

		log.info("register Request: userId[" + userId + "] 	signature[" + "XXXXXX" + "] firstName[" + firstName
				+ "] lastName[" + lastName + "] " + "dob[" + dob + "] gender[" + gender + "] motherMaidenName["
				+ "XXXXXXX" + "] " + "address[" + address + "] idType[" + idType + "] idNo[" + idNo + "] msisdn["
				+ msisdn + "]");

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(firstName) || !Utils.isValidDate(dob)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {

			AuthResponse authResponse = authenticationRequest.validate(userId, signature);
			String name = firstName;
			if (authResponse.getStatus() == Constant.RC_SUCCESS) {
				response = utibaHandler.simpleReg(msisdn, name, dob, apiUser);
			} else {
				response.setStatus(authResponse.getStatus());
				response.setMsg(authResponse.getMsg());
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response register(String userId, String signature, HashMap<String, String> parameterMap) {
		// TODO Auto-generated method stub
		String firstName = parameterMap.get(Constant.FIRSTNAME_PARAM);
		String lastName = parameterMap.get(Constant.LASTNAME_PARAM);
		String dob = parameterMap.get(Constant.DOB_PARAM);
		String gender = parameterMap.get(Constant.GENDER_PARAM);
		String motherMaidenName = parameterMap.get(Constant.MOTHER_NAME_PARAM);
		String address = parameterMap.get(Constant.ADDRESS_PARAM);
		String idType = parameterMap.get(Constant.ID_TYPE_PARAM);
		String idNo = parameterMap.get(Constant.ID_NUMBER_PARAM);
		String msisdn = parameterMap.get(Constant.MSISDN_PARAM);
		String email = parameterMap.get(Constant.EMAIL_PARAM);
		String countryCode = parameterMap.get(Constant.COUNTRYCODE_PARAM);
		String reff_no = parameterMap.get(Constant.REFF_NO_PARAM);
		String reff_code = parameterMap.get(Constant.REFF_CODE_PARAM);
		String deviceID = parameterMap.get(Constant.DEVICE_ID);

		log.info("register Request: userId[" + userId + "] 	signature[" + "XXXXXX" + "] firstName[" + firstName
				+ "] lastName[" + lastName + "] " + "dob[" + dob + "] gender[" + gender + "] motherMaidenName["
				+ "XXXXXX" + "] " + "address[" + address + "] idType[" + idType + "] idNo[" + idNo + "] reff_no[" + reff_no + "] msisdn["
				+ msisdn + "]");
		Response response = new Response();
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(firstName) || Utils.isNullorEmptyString(deviceID) || !Utils.isValidDate(dob)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		ApiUser apiUser = dataStore.getApiUser(userId);
		Response cekUser = new Response();
		if (apiUser != null) {
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				String username = getCredential[0];
				String pin = getCredential[1];

				if (!Utils.isValidEmailAddress2(username)) {
					ReferralType reffType = dataStore.getReffTypeByReffNo(reff_no);
					umarketscws.KeyValuePairMap value = new KeyValuePairMap();
					if(!Utils.isNullorEmptyString(reff_no)){
						log.info("Register with Referal ["+reff_no+"]");
						cekUser = balanceCheck(userId, reff_no);
						if(cekUser.getStatus()==Constant.RC_SUCCESS){
//							if(ambasador_eligible_active.equals("1")){
//								if(!cekUser.getAgentData().getWalletGrouping().contains(ambasador_eligible) && 
//										!cekUser.getAgentData().getWalletGrouping().contains(ambasador_eligible_ustad_evie)){
//									response.setStatus(Constant.RC_JOIN_PARENT_PARENT_NOTVALID);
//									response.setMsg(dataStore.getErrorMsg(Constant.RC_JOIN_PARENT_PARENT_NOTVALID));
//									return response;
//								}
//							}
							
							if (reffType == null) {
								response.setStatus(Constant.RC_JOIN_PARENT_PARENT_NOTVALID);
								response.setMsg(dataStore.getErrorMsg(Constant.RC_JOIN_PARENT_PARENT_NOTVALID));
								return response;
							}
							
							if (reffType != null && reffType.getFlagActive() == 0) {
								response.setStatus(Constant.RC_JOIN_PARENT_PARENT_NOTVALID);
								response.setMsg(dataStore.getErrorMsg(Constant.RC_JOIN_PARENT_PARENT_NOTVALID));
								return response;
							}
							
						}else{
							response.setStatus(Constant.RC_JOIN_PARENT_PARENT_NOTVALID);
							response.setMsg(dataStore.getErrorMsg(Constant.RC_JOIN_PARENT_PARENT_NOTVALID));
							return response;
						}
					}

					umarketscws.KeyValuePair kv0 = new umarketscws.KeyValuePair();
					kv0.setKey("primary_group");
					kv0.setValue("subscribers");

					umarketscws.KeyValuePair kv1 = new umarketscws.KeyValuePair();
					kv1.setKey("suppress_pin_expiry");
					kv1.setValue("1");

					if (!Utils.isNullorEmptyString(dob)) {
						umarketscws.KeyValuePair kv2 = new umarketscws.KeyValuePair();
						kv2.setKey("birthdate");
						kv2.setValue(dob);
						value.getKeyValuePair().add(kv2);
					}

					if (!Utils.isNullorEmptyString(gender)) {
						umarketscws.KeyValuePair kv3 = new umarketscws.KeyValuePair();
						kv3.setKey(Constant.GENDER_PARAM);
						kv3.setValue(gender);
						value.getKeyValuePair().add(kv3);
					}

					if (!Utils.isNullorEmptyString(motherMaidenName)) {
						umarketscws.KeyValuePair kv4 = new umarketscws.KeyValuePair();
						kv4.setKey("mother");
						kv4.setValue(motherMaidenName);
						value.getKeyValuePair().add(kv4);
					}

					if (!Utils.isNullorEmptyString(countryCode)) {
						umarketscws.KeyValuePair kv5 = new umarketscws.KeyValuePair();
						kv5.setKey("countrycode");
						kv5.setValue(countryCode);
						value.getKeyValuePair().add(kv5);
					}

					value.getKeyValuePair().add(kv0);
					value.getKeyValuePair().add(kv1);
					response = utibaHandler.register(username, pin, firstName, msisdn, email, value, apiUser);
					//check unique device id -- edited by kukuh remove reff_no when get data from db to check referral code				
//					Referral reffData = referalDao.getDataByReferralCodeAndDeviceID(reff_no, deviceID);
					Referral reffData = referalDao.getDataByDeviceID(reff_no, deviceID);
					log.info("referraltype : "+reffType);
					//Join Parent if reference code not null (add by yoedi: and there are unique device id paired with referral code)
					if(!Utils.isNullorEmptyString(reff_no) && reffData==null){
						if(response.getStatus()==Constant.RC_SUCCESS){
//							if(cekUser.getAgentData().getWalletGrouping().contains(ambasador_eligible_ustad_evie))
//								response = utibaHandler.joinChild(dataManager.getGlobalSessionId(), ambasador_eligible_ustad_evie_agent_parent, 
//										username, apiUser);
							
							if (reffType.getJoinType().equals("child"))
								response = utibaHandler.joinChild(dataManager.getGlobalSessionId(), reffType.getParentId(), 
										username, apiUser);
							else
								response = utibaHandler.joinParent(dataManager.getGlobalSessionId(), reff_no, username, apiUser);
							
							if(Utils.isNullorEmptyString(reff_code)){
								reff_code = reff_no;
							}
							
							String reffRes = referalDao.registerWithReferral(username, reff_no, reff_code,deviceID);
							if(reffRes.equals("1"))
								log.info("New Referral ["+username+"] to ["+reff_no+"]");
							
							if(response.getStatus()==Constant.RC_SUCCESS){
								//map agent to get realtime cashback
//								if(cekUser.getAgentData().getWalletGrouping().contains(ambasador_eligible_ustad_evie))
//									utibaHandler.mapAgent(dataManager.getGlobalSessionId(), msisdn, Long.valueOf(ambasador_eligible_ustad_evie_agent_group), username, apiUser);
								
								String sms = ambassador_sms_success.replace(Constant.MSISDN_REPLACE_TAG, username);
								if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(reff_no).getName())) {
									if (dataStore.getTelcoIdByPrefix(reff_no).getName().equalsIgnoreCase("INDOSAT")) {
										smsClientSender.send(sms, reff_no);
									} else {
										mailHandler.sendFirebaseNotification(null, reff_no, response.getTrxid(), sms,
												sms);
									}
								}
							}
						}else{
							log.error("Failed to Referal Mapping ["+username+"] to ["+reff_no+"]");
						}
					}
				} else {
					User user = userDao.get(username);
					if (user == null) {
						user = new User();

						StringBuilder parameters = new StringBuilder(username).append("|").append(msisdn).append("|")
								.append(pin);
						String encryptedText = "";
						try {
							DESedeEncryption encryption = new DESedeEncryption(encKey);
							encryptedText = encryption.encrypt(parameters.toString());
							encryptedText = URLEncoder.encode(encryptedText, "UTF-8");
							log.debug("encryptedText: " + encryptedText);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						user.setUsername(username);
						user.setFirstName(firstName);
						user.setLastName(lastName == null ? firstName : lastName);
						user.setDob(dob);
						user.setGender(gender);
						user.setMotherMaidenName(motherMaidenName);
						user.setAddress(address);
						user.setIdType(idType);
						user.setIdNo(idNo);
						user.setMsisdn(msisdn);
						user.setEncryptedText(encryptedText);
						user.setStatus(Constant.USER_STATUS_INITIAL);
						String accountId = userDao.save(user);
						log.info("Save username: " + username + " with account ID: " + accountId);
						if (accountId != null) {
							String url = activationURL.replace("[%KEY%]", encryptedText);
							String body = this.bodyActEmail;
							body = body.replace("[%NAME%]", firstName + " " + lastName);
							body = body.replace("[%URL%]", url);
							this.sendEmailNotification(username, this.subjectActEmail, body);
							histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_REGISTER,
									getCredential[0], "0", "0", 0);

							response.setStatus(Constant.RC_SUCCESS);
							response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
						} else {
							response.setStatus(Constant.RC_DB_ERROR);
							response.setMsg(dataStore.getErrorMsg(Constant.RC_DB_ERROR));
						}
					} else if (user.getStatus() == 0) {
						String encryptedText = "";
						StringBuilder parameters = new StringBuilder(username).append("|").append(user.getId())
								.append("|").append(pin);
						try {
							DESedeEncryption encryption = new DESedeEncryption(encKey);
							encryptedText = encryption.encrypt(parameters.toString());
							encryptedText = URLEncoder.encode(encryptedText, "UTF-8");
							log.debug("encryptedText: " + encryptedText);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						String url = activationURL.replace("[%KEY%]", encryptedText);
						String body = this.bodyActEmail;
						body = body.replace("[%NAME%]", firstName + " " + lastName);
						body = body.replace("[%URL%]", url);
						this.sendEmailNotification(username, this.subjectActEmail, body);
						histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_REGISTER,
								getCredential[0], "0", "0", 0);

						response.setStatus(Constant.RC_SUCCESS);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
					} else {
						response.setStatus(Constant.RC_USER_ALREADY_EXIST);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_USER_ALREADY_EXIST));
					}
				}

			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	public Response doHistoryTransaction(String userId, String to, int count) {
		Response response = new Response();
		log.info("doHistoryTransaction Request: userId[" + userId + "]" + "to[" + to + "]");
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(to)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			response = utibaHandler.getLastTransaction(to, count, apiUser);
			histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_DO_PAYMENT, "", "0",
					response.getTrxid(), response.getStatus());
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	@Override
	public Response balanceCheck(String userId, String msisdn) {
		Response response = new Response();
		log.info("balanceCheck Request: userId[" + userId + "]" + "msisdn[" + msisdn + "]");
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			response = utibaHandler.getAgentByReferenceRequest(msisdn, apiUser);
			histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_GET_BALANCE, "", "0",
					response.getTrxid(), response.getStatus());
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	@Override
	public Response doUserInquiry(String userId, String signature, String msisdn) {
		Response response = new Response();
		log.info("Request getAgentByReferenceRequest:: userId[" + userId + "] signature[XXXXXXX] msisdn[" + msisdn
				+ "]");
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					// if(msisdn.startsWith("1"))
					// msisdn = userDao.getUserByAccountID(msisdn);

					if (Utils.isNullorEmptyString(msisdn)) {
						response.setStatus(Constant.RC_AGENT_NOTFOUND);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_AGENT_NOTFOUND));
					} else {
						response = utibaHandler.getAgentByReferenceRequest(msisdn, apiUser);
						histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_GET_USER_INFO,
								getCredential[0], "0", response.getTrxid(), response.getStatus());
					}
				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		// TODO Auto-generated method stub
		return response;
	}

	@Override
	public Response sellNoConfirm(String userId, String signature, String to, String amount, String extRef) {
		log.info("Request sellNoConfirm:: userId[" + userId + "] signature[XXXXXXX] to[" + to + "] amount[" + amount
				+ "] extRef[" + extRef + "]");
		Response response = new Response();
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature) || Utils.isNullorEmptyString(to)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(extRef)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			ApiUser apiUser = dataStore.getApiUser(userId);
			response = utibaHandler.sellNoConfirm(authResponse.getSessionId(), to, amount, extRef,
					authResponse.getInitiator(), apiUser);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response sell(String userId, String signature, String to, String amount, String extRef, String extraParam) {
		// TODO Auto-generated method stub
		log.info("Request sell:: userId[" + userId + "] signature[XXXXXXX] to[" + to + "] amount[" + amount
				+ "] extRef[" + extRef + "] extraParam[" + extraParam + "]");

		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature) || Utils.isNullorEmptyString(to)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(extRef)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					if (Utils.isNullorEmptyString(extraParam)) {
						response = utibaHandler.sell(sessionId, to, amount, apiUser.getServiceType(), extRef, apiUser);
					} else {
						response = utibaHandler.sell(sessionId, to, amount, extraParam, extRef, apiUser);
					}
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_DO_PAYMENT,

							getCredential[0], extRef, response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}
	
	@Override
	public Response sellWithParam(String userId, String signature, Parameter parameter) {
		String to = parameter.getKeyValueMap().get(Constant.TO_PARAM);
		String amount = parameter.getKeyValueMap().get(Constant.AMOUNT_PARAM);
		String extRef = parameter.getKeyValueMap().get(Constant.EXT_REF_PARAM);
		String merchantNumber = parameter.getKeyValueMap().get(Constant.MERCHANT_NUMBER_PARAM);
		String sms = null;
		
		// TODO Auto-generated method stub
		log.info("Request sell:: userId[" + userId + "] signature[XXXXXXX] to[" + to + "] parameter[" + parameter.toString()+ "]");
		Response response = new Response();
		
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature) || Utils.isNullorEmptyString(to)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(extRef)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					response = utibaHandler.sellWithParam(sessionId, to, amount, apiUser.getServiceType(), extRef, apiUser, parameter);
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_DO_PAYMENT,getCredential[0],extRef, response.getTrxid(), response.getStatus());
					if(!Utils.isNullorEmptyString(merchantNumber)){
						if(response.getStatus()==Constant.RC_SUCCESS){
							// Anda menerima pembayaran sebesar [%AMOUNT%] dari [%TO%] pada [%DATE%]. Ref [%REFERENCE%].
							sms = smsMerchantSuccess;
							sms = sms.replace(Constant.AMOUNT_REPLACE_TAG, amount);
							sms = sms.replace(Constant.TO_REPLACE_TAG, to);
							sms = sms.replace(Constant.REFERENCE_REPLACE_TAG, response.getTrxid());
							sms = sms.replace(Constant.DATE_REPLACE_TAG, Utils.getCurrentDate("dd-MM-yy hh:mm:ss"));
							if (dataStore.getTelcoIdByPrefix(merchantNumber).getName().equalsIgnoreCase("INDOSAT")) {
								smsClientSender.send(sms, merchantNumber);
							}else{
								smsClientSender.sendUsingMMD(sms, merchantNumber);
							}
							
						}else{
							sms = smsMerchantFailed;
							sms = sms.replace(Constant.AMOUNT_REPLACE_TAG, amount);
							sms = sms.replace(Constant.TO_REPLACE_TAG, to);
							sms = sms.replace(Constant.REFERENCE_REPLACE_TAG, response.getTrxid());
							sms = sms.replace(Constant.DATE_REPLACE_TAG, Utils.getCurrentDate("dd-MM-yy hh:mm:ss"));
							if (dataStore.getTelcoIdByPrefix(merchantNumber).getName().equalsIgnoreCase("INDOSAT")) {
								smsClientSender.send(sms, merchantNumber);
							}else{
								smsClientSender.sendUsingMMD(sms, merchantNumber);
							}
						}
					}
				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response checkTransaction(String userId, String signature, String extRef) {
		log.info("Request checkTransaction:: userId[" + userId + "] signature[XXXXXXX] extRef[" + extRef + "]");
		// TODO Auto-generated method stub
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					/*
					 * response = utibaHandler.checkTransaction(sessionId,
					 * extRef);
					 * 
					 * int ref =0; int respCode = 999;
					 * if(response.getCommonResponse() != null){ ref =
					 * response.getCommonResponse().getTrxid(); respCode =
					 * response.getCommonResponse().getResponseCode(); }else{
					 * ref = Integer.valueOf(response.getTrxid()); respCode =
					 * response.getStatus(); }
					 */

					// temporary
					TransactionDMT trx = transactionDMTDao.getByExtRef(extRef);
					if (trx == null) {
						response.setStatus(Constant.RC_TRANSACTION_NOT_FOUND);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_TRANSACTION_NOT_FOUND));
						response.setExtRef(extRef);
					} else {
						response.setStatus(trx.getStatus());
						response.setMsg(dataStore.getErrorMsg(trx.getStatus()));
						response.setExtRef(extRef);
						response.setTrxid(trx.getTransId());
						response.setCouponId(trx.getCouponId());
					}

					/*
					 * histList.writeHist(apiUser.getId(),
					 * apiUser.getChannelType(),
					 * Constant.SERVICE_GET_TRX_STATUS, getCredential[0],
					 * extRef, String.valueOf(ref), respCode);
					 */

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response couponTransfer(String userId, String signature, String subscriber, String extRef, String amount,
			String couponId) {
		log.info("Request couponTransfer:: userId[" + userId + "] signature[XXXXXXX] subscriber[" + subscriber + "] "
				+ "extRef[" + extRef + "] amount[" + amount + "] couponId[" + couponId + "]");
		// TODO Auto-generated method stub
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(subscriber) || Utils.isNullorEmptyString(extRef)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(couponId)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					response = utibaHandler.couponTransfer(Constant.SERVICE_DO_TRANSFER_TOKEN, sessionId, subscriber,
							apiUser.getServiceType(), extRef, amount, couponId, getCredential[0], apiUser);

					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_DO_TRANSFER_TOKEN,
							getCredential[0], extRef, response.getTrxid(), response.getStatus());

					// ==================temporary
					// if (!StringUtils.isNumeric(subscriber)) {
					if (Utils.isValidEmailAddress2(subscriber)) {
						TransactionMail transactionMail = new TransactionMail();
						transactionMail.setTransactionTypeINA(Constant.TRANSACTION_TEXT_TYPE_ECOMMERCE);
						transactionMail.setTransactionTypeEN(Constant.TRANSACTION_TEXT_TYPE_ECOMMERCE);
						transactionMail.setDestination(getCredential[0]);
						transactionMail.setAmount(amount);
						transactionMail.setTransId(response.getTrxid());
						transactionMail.setStatus(response.getStatus().intValue());
						mailHandler.sendEmailNotification(subscriber, subjectEmailTransaction, transactionMail,
								apiUser);
						// smsClientSender.sendMyntNotificaton(subscriber,
						// transactionMail);
					}
					// ================

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response cashout(String userId, String signature, String subscriber, String extRef, String amount,
			String couponId) {
		log.info("Request cashout:: userId[" + userId + "] signature[XXXXXXX] subscriber[" + subscriber + "] "
				+ "extRef[" + extRef + "] amount[" + amount + "] couponId[" + couponId + "]");
		// TODO Auto-generated method stub
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(subscriber) || Utils.isNullorEmptyString(extRef)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(couponId)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					response = utibaHandler.couponTransfer(Constant.SERVICE_CASHOUT, sessionId, subscriber,
							apiUser.getServiceType(), extRef, amount, couponId, getCredential[0], apiUser);

					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_CASHOUT,
							getCredential[0], extRef, response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response cashin(String userId, String signature, String to, String amount, String transId,
			boolean supressSms) {
		log.info("Request cashin:: userId[" + userId + "] signature[XXXXXXX] to[" + to + "] " + "transId[" + transId
				+ "] amount[" + amount + "] supressSms[" + supressSms + "]");
		// TODO Auto-generated method stub
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature) || Utils.isNullorEmptyString(to)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(amount)
				|| Utils.isNullorEmptyString(transId)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					response = utibaHandler.cashin(sessionId, to, amount, getCredential[0], transId, supressSms,
							apiUser);

					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_CASHIN,
							getCredential[0], transId, response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response reversal(String userId, String signature, Parameter parameter) {
		// TODO Auto-generated method stub
		Response response = new Response();
		HashMap<String, String> parameterMap = parameter.getKeyValueMap();
		String amount = parameterMap.get(Constant.AMOUNT_PARAM);
		String transId = parameterMap.get(Constant.TRANSID_PARAM);
		String recipient = parameterMap.get(Constant.RECIPIENT_PARAM);

		log.info("Request reversal:: userId[" + userId + "] signature[XXXXXXX] amount[" + amount + "] transId["
				+ transId + "] ");

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(amount)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					response = utibaHandler.reversal(sessionId, transId, amount, recipient, apiUser);

					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_REVERSAL,
							getCredential[0], transId, response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}
	
	@Override
	public Response reversalExtraParam(String userId, String signature, Parameter parameter) {
		// TODO Auto-generated method stub
		Response response = new Response();
		HashMap<String, String> parameterMap = parameter.getKeyValueMap();
		String amount = parameterMap.get(Constant.AMOUNT_PARAM);
		String transId = parameterMap.get(Constant.TRANSID_PARAM);
		String recipient = parameterMap.get(Constant.RECIPIENT_PARAM);

		log.info("Request reversal:: userId[" + userId + "] signature[XXXXXXX] amount[" + amount + "] transId["
				+ transId + "] ");

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(amount)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					response = utibaHandler.reversal(sessionId, transId, amount, recipient,parameter, apiUser);

					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_REVERSAL,
							getCredential[0], transId, response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response balance(String userId, String signature) {
		log.info("Request balance:: userId[" + userId + "] signature[XXXXXXX] ");
		// TODO Auto-generated method stub
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					response = utibaHandler.balance(sessionId, apiUser);

					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_GET_BALANCE,
							getCredential[0], response.getTrxid(), response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response createCoupon(String userId, String signature) {
		log.info("Request createCoupon:: userId[" + userId + "] signature[XXXXXXX] ");
		// TODO Auto-generated method stub
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					response = utibaHandler.createCoupon(sessionId, apiUser);

					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_CREATE_COUPON,
							getCredential[0], response.getTrxid(), response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response createCouponByHour(String userId, String signature, int hour) {
		log.info("Request createCoupon:: userId[" + userId + "] signature[XXXXXXX] ");
		// TODO Auto-generated method stub
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					response = utibaHandler.createCoupon(sessionId, hour, String.valueOf(apiUser.getChannelType()),
							apiUser);

					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_CREATE_COUPON,
							getCredential[0], response.getTrxid(), response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response merchantTransfer(String userId, String signature, String amount, String to,
			HashMap<String, String> parameterMap) {
		log.info("Request merchantTransfer:: userId[" + userId + "] signature[XXXXXXX] amount[" + amount + "] to[" + to
				+ "] ");
		// TODO Auto-generated method stub
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(to)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					// com.utiba.delirium.ws.misc.KeyValuePairMap kpvmap = new
					// com.utiba.delirium.ws.misc.KeyValuePairMap();
					//
					// if (parameterMap.containsKey("suppress_sms")) {
					// //
					// log.debug("SUPPRESS:"+parameterMap.get("suppress_sms"));
					// if (parameterMap.get("suppress_sms").equals("1")) {
					// KeyValuePair kvp = new KeyValuePair();
					// kvp.setKey("suppress_sms");
					// kvp.setValue("1");
					// kpvmap.getKeyValuePairs().add(kvp);
					// }
					// }
					//
					// for (Map.Entry<String, String> entry :
					// parameterMap.entrySet()) {
					// KeyValuePair kvp = new KeyValuePair();
					// if
					// (!entry.getKey().equalsIgnoreCase(Constant.SIGNATURE_PARAM)
					// ||
					// !entry.getKey().equalsIgnoreCase(Constant.USERID_PARAM))
					// {
					// kvp.setKey(entry.getKey());
					// kvp.setValue(entry.getValue());
					// kpvmap.getKeyValuePairs().add(kvp);
					// }
					//
					// }
					response = utibaHandler.merchantTransfer(sessionId, amount, to, parameterMap, apiUser);
					// response = utibaHandler.merchantTransfer(sessionId,
					// amount, to, kpvmap,apiUser);
					// ==================temporary
					// if (!StringUtils.isNumeric(getCredential[0])) {
					if (Utils.isValidEmailAddress2(getCredential[0])) {
						TransactionMail transactionMail = new TransactionMail();
						transactionMail.setTransactionTypeINA(Constant.TRANSACTION_TEXT_TYPE_ECOMMERCE);
						transactionMail.setTransactionTypeEN(Constant.TRANSACTION_TEXT_TYPE_ECOMMERCE);
						transactionMail.setDestination(to);
						transactionMail.setAmount(amount);
						transactionMail.setTransId(response.getTrxid());
						transactionMail.setStatus(response.getStatus().intValue());
						mailHandler.sendEmailNotification(getCredential[0], subjectEmailTransaction, transactionMail,
								apiUser);
						// smsClientSender.sendMyntNotificaton(getCredential[0],
						// transactionMail);
					}
					// ================
					// Add for DBS
					// if(response.getStatus()==Constant.RC_SUCCESS){
					String respMessage = "";
					if (to.contains("dbs")) {
						if (response.getStatus() == Constant.RC_SUCCESS) {
							respMessage = dataStore.getSMSNotificationByTitle("DBS_PAY");
							respMessage = respMessage.replace(Constant.AMOUNT_REPLACE_TAG,
									Utils.convertStringtoCurrency(amount));
							respMessage = respMessage.replace(Constant.TO_REPLACE_TAG,
									parameterMap.get(Constant.IDPELANGGAN_PARAM));// dbs.getTarget
																					// =
																					// DBS
																					// Phone
																					// Number
							respMessage = respMessage.replace(Constant.TRANSACTIONID_REPLACE_TAG, response.getTrxid());
							if (dataStore.getTelcoIdByPrefix(getCredential[0]).getName().equalsIgnoreCase("INDOSAT")) {
								smsClientSender.send(respMessage, getCredential[0]);
							} else {
								mailHandler.sendFirebaseNotification(null, getCredential[0], response.getTrxid(),
										respMessage, respMessage.substring(0, 100) + "...");
							}
						} else {
							respMessage = smsTextFailed;
							respMessage = respMessage.replace(Constant.DATE_REPLACE_TAG,
									Utils.getCurrentDate("dd/MM/yyyy hh:mm:ss"));
							respMessage = respMessage.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
									dataManager.getErrorMsg(response.getStatus()));
							respMessage = "Pembayaran Dana Bantuan Sahabat Gagal." + respMessage;
							if (dataStore.getTelcoIdByPrefix(getCredential[0]).getName().equalsIgnoreCase("INDOSAT")) {
								smsClientSender.send(respMessage, getCredential[0]);
							} else {
								mailHandler.sendFirebaseNotification(null, getCredential[0], response.getTrxid(),
										respMessage, respMessage.substring(0, 100) + "...");
							}
						}
					}
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_MERCHANT_TRANSFER,
							getCredential[0], response.getTrxid(), response.getTrxid(), response.getStatus());
				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response qrMerchantTransfer(String userId, String signature, Parameter parameter) {

		HashMap<String, String> param = parameter.getKeyValueMap();
		String amount = param.get(Constant.AMOUNT_PARAM);
		String to = param.get(Constant.TO_PARAM);

		log.info("Request merchantTransfer:: userId[" + userId + "] signature[XXXXXXX] amount[" + amount + "] to[" + to
				+ "] ");
		// TODO Auto-generated method stub
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(to)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					response = utibaHandler.merchantTransfer(sessionId, amount, to, param, apiUser);
					// response = utibaHandler.merchantTransfer(sessionId,
					// amount, to, kpvmap,apiUser);
					// ==================temporary
					// if (!StringUtils.isNumeric(getCredential[0])) {
					if (Utils.isValidEmailAddress2(getCredential[0])) {
						TransactionMail transactionMail = new TransactionMail();
						transactionMail.setTransactionTypeINA(Constant.TRANSACTION_TEXT_TYPE_ECOMMERCE);
						transactionMail.setTransactionTypeEN(Constant.TRANSACTION_TEXT_TYPE_ECOMMERCE);
						transactionMail.setDestination(to);
						transactionMail.setAmount(amount);
						transactionMail.setTransId(response.getTrxid());
						transactionMail.setStatus(response.getStatus().intValue());
						mailHandler.sendEmailNotification(getCredential[0], subjectEmailTransaction, transactionMail,
								apiUser);
						// smsClientSender.sendMyntNotificaton(getCredential[0],
						// transactionMail);
					}
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_MERCHANT_TRANSFER,
							getCredential[0], response.getTrxid(), response.getTrxid(), response.getStatus());
				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response moneyTransferExtParam(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		HashMap<String, String> param = parameter.getKeyValueMap();
		String amount = param.get(Constant.AMOUNT_PARAM);
		String to = param.get(Constant.TO_PARAM);
		String extRef = param.get(Constant.EXT_REF_PARAM);

		log.info("Request moneyTransfer:: userId[" + userId + "] signature[XXXXXXX] amount[" + amount + "] to[" + to
				+ "] extRef[" + extRef + "]");
		// TODO Auto-generated method stub

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(to)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					//must kyc by megi untuk P2P
					response = utibaHandler.moneyTransfer(sessionId, amount, to, extRef, parameter, apiUser);
					
					log.debug("Cred 0:" + getCredential[0] + "---To:" + to);
					if (Utils.isValidEmailAddress2(getCredential[0])) {
						TransactionMail transactionMail = new TransactionMail();
						transactionMail.setTransactionTypeINA(Constant.TRANSACTION_TEXT_TYPE_SEND_MONEY_INA);
						transactionMail.setTransactionTypeEN(Constant.TRANSACTION_TEXT_TYPE_SEND_MONEY_EN);
						transactionMail.setDestination(to);
						transactionMail.setAmount(amount);
						transactionMail.setTransId(response.getTrxid());
						transactionMail.setStatus(response.getStatus().intValue());
						mailHandler.sendEmailNotification(getCredential[0], subjectEmailTransaction, transactionMail,
								apiUser);
					}

					if (response.getStatus() == Constant.RC_SUCCESS) {
						if (Utils.isValidEmailAddress2(to)) {
							TransactionMail transactionMail = new TransactionMail();
							transactionMail.setDestination(getCredential[0]);
							transactionMail.setAmount(amount);
							transactionMail.setTransId(response.getTrxid());
							transactionMail.setStatus(response.getStatus().intValue());
							mailHandler.sendEmailNotificationToReceiver(to, subjectEmailTransaction, transactionMail,
									apiUser);
						}
					}

					// ================
					response.setStatus(response.getStatus());

					if (response.getStatus() == Constant.RC_WALLET_BALANCE_EXCEEDED) {
						RemittancePartner remittancePartner = dataStore.getRemittancePartner(getCredential[0]);
						if (remittancePartner != null && remittancePartner.getIsAllowed() == 1) {
							log.info("Wallet Balance Exceeded found, trying to get remittance partner ["
									+ getCredential[0] + "] " + remittancePartner.toString());
							if (Integer.valueOf(amount) <= emoneyMaxLimitAmout) {
								StringBuilder vaBuilder = new StringBuilder("RT").append(to);
								Response userResponse = utibaHandler.getAgentByReferenceRequest(to, apiUser);
								String smsText = this.smsPendingTransferNotification;
								smsText = smsText.replace(Constant.AMOUNT_REPLACE_TAG,
										Utils.convertStringtoCurrency(amount));
								smsText = smsText.replace(Constant.FROM_REPLACE_TAG, remittancePartner.getName());
								if (userResponse.getStatus() == Constant.RC_AGENT_NOTFOUND) {
									utibaHandler.simpleReg(to, to, null, apiUser);
									utibaHandler.mapAgent(dataStore.getGlobalSessionId(), to, 10260, userAdmin,
											apiUser);
									utibaHandler.simpleReg(vaBuilder.toString(), to, null, apiUser);
									utibaHandler.unmapAgent(dataStore.getGlobalSessionId(), vaBuilder.toString(),
											nonKycAgentId, apiUser);
									response = utibaHandler.moneyTransfer(sessionId, amount, vaBuilder.toString(),
											"RT" + extRef, parameter, apiUser);
									if (response.getStatus() == Constant.RC_SUCCESS) {
										smsClientSender.send(smsText, to);
										remittanceSchedulerDao.save(to);
									}

								} else if (userResponse.getStatus() == Constant.RC_SUCCESS
										&& userResponse.getAgentData().getWalletType() == Constant.WALLET_TYPE_REG) {
									utibaHandler.simpleReg(vaBuilder.toString(), to, null, apiUser);
									utibaHandler.unmapAgent(dataStore.getGlobalSessionId(), vaBuilder.toString(),
											nonKycAgentId, apiUser);
									response = utibaHandler.moneyTransfer(sessionId, amount, vaBuilder.toString(),
											"RT" + extRef, parameter, apiUser);
									if (response.getStatus() == Constant.RC_SUCCESS) {
										smsClientSender.send(smsText, to);
										remittanceSchedulerDao.save(to);
									}

								}
							}
							/*
							 * Response userResponse =
							 * utibaHandler.getAgentByReferenceRequest(to);
							 * if(userResponse.getStatus() ==
							 * Constant.RC_AGENT_NOTFOUND){ Response register =
							 * utibaHandler.simpleReg(to, to, null); Response
							 * registerRT =
							 * utibaHandler.simpleReg(vaBuilder.toString(), to,
							 * null); if(registerRT.getStatus() ==
							 * Constant.RC_SUCCESS){ registerRT =
							 * utibaHandler.unmapAgent(dataStore.
							 * getGlobalSessionId(), vaBuilder.toString(),
							 * nonKycAgentId); }
							 * 
							 * response = utibaHandler.moneyTransfer(sessionId,
							 * amount, vaBuilder.toString(), "RT"+extRef,
							 * parameter); if(response.getStatus() ==
							 * Constant.RC_SUCCESS) smsClientSender.send(
							 * smsPendingTransferNotification.replace(Constant.
							 * AMOUNT_REPLACE_TAG,
							 * Utils.convertStringtoCurrency(amount)), to);
							 * 
							 * }else if(userResponse.getStatus()==
							 * Constant.RC_SUCCESS &&
							 * userResponse.getAgentData().getWalletType()==
							 * Constant.WALLET_TYPE_REG){ Response register =
							 * utibaHandler.simpleReg(to, to, null); Response
							 * registerRT =
							 * utibaHandler.simpleReg(vaBuilder.toString(), to,
							 * null); if(registerRT.getStatus() ==
							 * Constant.RC_SUCCESS){ registerRT =
							 * utibaHandler.unmapAgent(dataStore.
							 * getGlobalSessionId(), vaBuilder.toString(),
							 * nonKycAgentId); }
							 * 
							 * response = utibaHandler.moneyTransfer(sessionId,
							 * amount, vaBuilder.toString(), "RT"+extRef,
							 * parameter); if(response.getStatus() ==
							 * Constant.RC_SUCCESS) smsClientSender.send(
							 * smsPendingTransferNotification.replace(Constant.
							 * AMOUNT_REPLACE_TAG,
							 * Utils.convertStringtoCurrency(amount)), to);
							 * 
							 * }
							 */
						}
					}

					response.setMsg(dataStore.getErrorMsg(response.getStatus()));
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_MONEY_TRANSFER,
							getCredential[0], response.getTrxid(), response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	public Response moneyTransferExtParamSeperatedP2P(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		HashMap<String, String> param = parameter.getKeyValueMap();
		String amount = param.get(Constant.AMOUNT_PARAM);
		String to = param.get(Constant.TO_PARAM);
		String extRef = param.get(Constant.EXT_REF_PARAM);

		log.info("Request moneyTransfer:: userId[" + userId + "] signature[XXXXXXX] amount[" + amount + "] to[" + to
				+ "] extRef[" + extRef + "]");
		// TODO Auto-generated method stub

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(to)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					//must kyc by megi
//					response = utibaHandler.moneyTransfer(sessionId, amount, to, extRef, parameter, apiUser);
					//dont have to kyc by yody
					response = utibaHandler.merchantTransfer(sessionId, amount, to, param, apiUser);
					log.debug("Cred 0:" + getCredential[0] + "---To:" + to);
					if (Utils.isValidEmailAddress2(getCredential[0])) {
						TransactionMail transactionMail = new TransactionMail();
						transactionMail.setTransactionTypeINA(Constant.TRANSACTION_TEXT_TYPE_SEND_MONEY_INA);
						transactionMail.setTransactionTypeEN(Constant.TRANSACTION_TEXT_TYPE_SEND_MONEY_EN);
						transactionMail.setDestination(to);
						transactionMail.setAmount(amount);
						transactionMail.setTransId(response.getTrxid());
						transactionMail.setStatus(response.getStatus().intValue());
						mailHandler.sendEmailNotification(getCredential[0], subjectEmailTransaction, transactionMail,
								apiUser);
					}

					if (response.getStatus() == Constant.RC_SUCCESS) {
						if (Utils.isValidEmailAddress2(to)) {
							TransactionMail transactionMail = new TransactionMail();
							transactionMail.setDestination(getCredential[0]);
							transactionMail.setAmount(amount);
							transactionMail.setTransId(response.getTrxid());
							transactionMail.setStatus(response.getStatus().intValue());
							mailHandler.sendEmailNotificationToReceiver(to, subjectEmailTransaction, transactionMail,
									apiUser);
						}
					}

					// ================
					response.setStatus(response.getStatus());

					if (response.getStatus() == Constant.RC_WALLET_BALANCE_EXCEEDED) {
						
//						RemittancePartner remittancePartner = dataStore.getRemittancePartner(getCredential[0]);
//						if (remittancePartner != null && remittancePartner.getIsAllowed() == 1) {
//							log.info("Wallet Balance Exceeded found, trying to get remittance partner ["
//									+ getCredential[0] + "] " + remittancePartner.toString());
//							if (Integer.valueOf(amount) <= emoneyMaxLimitAmout) {
//								StringBuilder vaBuilder = new StringBuilder("RT").append(to);
//								Response userResponse = utibaHandler.getAgentByReferenceRequest(to, apiUser);
//								String smsText = this.smsPendingTransferNotification;
//								smsText = smsText.replace(Constant.AMOUNT_REPLACE_TAG,
//										Utils.convertStringtoCurrency(amount));
//								smsText = smsText.replace(Constant.FROM_REPLACE_TAG, remittancePartner.getName());
//								if (userResponse.getStatus() == Constant.RC_AGENT_NOTFOUND) {
//									utibaHandler.simpleReg(to, to, null, apiUser);
//									utibaHandler.mapAgent(dataStore.getGlobalSessionId(), to, 10260, userAdmin,
//											apiUser);
//									utibaHandler.simpleReg(vaBuilder.toString(), to, null, apiUser);
//									utibaHandler.unmapAgent(dataStore.getGlobalSessionId(), vaBuilder.toString(),
//											nonKycAgentId, apiUser);
//									response = utibaHandler.moneyTransfer(sessionId, amount, vaBuilder.toString(),
//											"RT" + extRef, parameter, apiUser);
//									if (response.getStatus() == Constant.RC_SUCCESS) {
//										smsClientSender.send(smsText, to);
//										remittanceSchedulerDao.save(to);
//									}
//
//								} else if (userResponse.getStatus() == Constant.RC_SUCCESS
//										&& userResponse.getAgentData().getWalletType() == Constant.WALLET_TYPE_REG) {
//									utibaHandler.simpleReg(vaBuilder.toString(), to, null, apiUser);
//									utibaHandler.unmapAgent(dataStore.getGlobalSessionId(), vaBuilder.toString(),
//											nonKycAgentId, apiUser);
//									response = utibaHandler.moneyTransfer(sessionId, amount, vaBuilder.toString(),
//											"RT" + extRef, parameter, apiUser);
//									if (response.getStatus() == Constant.RC_SUCCESS) {
//										smsClientSender.send(smsText, to);
//										remittanceSchedulerDao.save(to);
//									}
//
//								}
//							}
							/*
							 * Response userResponse =
							 * utibaHandler.getAgentByReferenceRequest(to);
							 * if(userResponse.getStatus() ==
							 * Constant.RC_AGENT_NOTFOUND){ Response register =
							 * utibaHandler.simpleReg(to, to, null); Response
							 * registerRT =
							 * utibaHandler.simpleReg(vaBuilder.toString(), to,
							 * null); if(registerRT.getStatus() ==
							 * Constant.RC_SUCCESS){ registerRT =
							 * utibaHandler.unmapAgent(dataStore.
							 * getGlobalSessionId(), vaBuilder.toString(),
							 * nonKycAgentId); }
							 * 
							 * response = utibaHandler.moneyTransfer(sessionId,
							 * amount, vaBuilder.toString(), "RT"+extRef,
							 * parameter); if(response.getStatus() ==
							 * Constant.RC_SUCCESS) smsClientSender.send(
							 * smsPendingTransferNotification.replace(Constant.
							 * AMOUNT_REPLACE_TAG,
							 * Utils.convertStringtoCurrency(amount)), to);
							 * 
							 * }else if(userResponse.getStatus()==
							 * Constant.RC_SUCCESS &&
							 * userResponse.getAgentData().getWalletType()==
							 * Constant.WALLET_TYPE_REG){ Response register =
							 * utibaHandler.simpleReg(to, to, null); Response
							 * registerRT =
							 * utibaHandler.simpleReg(vaBuilder.toString(), to,
							 * null); if(registerRT.getStatus() ==
							 * Constant.RC_SUCCESS){ registerRT =
							 * utibaHandler.unmapAgent(dataStore.
							 * getGlobalSessionId(), vaBuilder.toString(),
							 * nonKycAgentId); }
							 * 
							 * response = utibaHandler.moneyTransfer(sessionId,
							 * amount, vaBuilder.toString(), "RT"+extRef,
							 * parameter); if(response.getStatus() ==
							 * Constant.RC_SUCCESS) smsClientSender.send(
							 * smsPendingTransferNotification.replace(Constant.
							 * AMOUNT_REPLACE_TAG,
							 * Utils.convertStringtoCurrency(amount)), to);
							 * 
							 * }
							 */
//						}
					}

					response.setMsg(dataStore.getErrorMsg(response.getStatus()));
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_MONEY_TRANSFER,
							getCredential[0], response.getTrxid(), response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	
	@Override
	public Response moneyTransfer(String userId, String signature, String amount, String to, String extRef) {
		log.info("Request moneyTransfer:: userId[" + userId + "] signature[XXXXXXX] amount[" + amount + "] to[" + to
				+ "] extRef[" + extRef + "]");
		// TODO Auto-generated method stub
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(to)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					// if(to.startsWith("1"))
					// to = userDao.getUserByAccountID(to);

					response = utibaHandler.moneyTransfer(sessionId, amount, to, extRef, apiUser);

					// ==================temporary
					// if (!StringUtils.isNumeric(getCredential[0])) {
					log.debug("Cred 0:" + getCredential[0] + "---To:" + to);
					if (Utils.isValidEmailAddress2(getCredential[0])) {
						TransactionMail transactionMail = new TransactionMail();
						transactionMail.setTransactionTypeINA(Constant.TRANSACTION_TEXT_TYPE_SEND_MONEY_INA);
						transactionMail.setTransactionTypeEN(Constant.TRANSACTION_TEXT_TYPE_SEND_MONEY_EN);
						transactionMail.setDestination(to);
						transactionMail.setAmount(amount);
						transactionMail.setTransId(response.getTrxid());
						transactionMail.setStatus(response.getStatus().intValue());
						mailHandler.sendEmailNotification(getCredential[0], subjectEmailTransaction, transactionMail,
								apiUser);
						// smsClientSender.sendMyntNotificaton(getCredential[0],
						// transactionMail);
					}

					if (response.getStatus() == Constant.RC_SUCCESS) {
						// if (!StringUtils.isNumeric(to)) {
						if (Utils.isValidEmailAddress2(to)) {
							TransactionMail transactionMail = new TransactionMail();
							transactionMail.setDestination(getCredential[0]);
							transactionMail.setAmount(amount);
							transactionMail.setTransId(response.getTrxid());
							transactionMail.setStatus(response.getStatus().intValue());
							mailHandler.sendEmailNotificationToReceiver(to, subjectEmailTransaction, transactionMail,
									apiUser);
							// smsClientSender.sendMyntNotificaton(to,
							// transactionMail);
						}
					}
					// ================
					response.setStatus(response.getStatus());
					response.setMsg(dataStore.getErrorMsg(response.getStatus()));
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_MONEY_TRANSFER,
							getCredential[0], response.getTrxid(), response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response creditAirtimeInquiry(String userId, String signature, String to, String amount, String transId) {
		log.info("Request creditAirtimeInquiry:: userId[" + userId + "] signature[XXXXXXX] amount[" + amount + "] to["
				+ to + "] " + "transId[" + transId + "]");
		// TODO Auto-generated method stub
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(to)
				|| Utils.isNullorEmptyString(transId)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					Prefix prefix = dataStore.getTelcoIdByPrefix(to);
//					if(dataStore.getTelcoIdByPrefix(to).getName().equalsIgnoreCase("INDOSAT")){
//						response = utibaHandler.isatCreditInquiry(sessionId, to, amount, "sev", apiUser);
//					}else{
//						response = utibaHandler.creditAirtimeInquiry(getCredential[0], to, amount, transId, apiUser);
//					}
					response = utibaHandler.airtimeBillpayCreditInquiry(sessionId, to, amount, "sev", prefix ,apiUser);
					if (response.getStatus() == Constant.RC_SUCCESS) {
						// Add fee by Ryan
						int fee = Integer.parseInt(response.getPrice()) - Integer.parseInt(amount);
						response.setFee(String.valueOf(fee));
					}

					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_AIRTIME_INQUIRY,
							getCredential[0], transId, response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	@Override
	public Response commitCreditAirtime(String userId, String signature, int serviceTransactionId, String transactionId, String operatorName) {
		log.info("Request commitCreditAirtime:: userId[" + userId + "] signature[XXXXXXX] " + "serviceTransactionId["
				+ serviceTransactionId + "] transactionId[" + transactionId + "] ");
		// TODO Auto-generated method stub
		Response response = new Response();
		String target = "";
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(transactionId)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					HashMap<String,String> detailTrx = utibaHandler.getTransactionDetail(sessionId, serviceTransactionId, apiUser).getDetailTrx();
					if(detailTrx!=null){
						if(detailTrx.get("customer_id")!=null){
							target = detailTrx.get("customer_id");
						}
					}
//					if(!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(target).getName())){
//						if(dataStore.getTelcoIdByPrefix(target).getName().equalsIgnoreCase("INDOSAT")){
//							response = utibaHandler.confirm(sessionId, serviceTransactionId, apiUser);
//						}
//					}else{
//						response = utibaHandler.commitCreditAirtime(getCredential[0], getCredential[1],
//								String.valueOf(serviceTransactionId), transactionId, operatorName, apiUser);
//					}
					response = utibaHandler.confirm(sessionId, serviceTransactionId, apiUser);
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_COMMIT_AIRTIME,
							getCredential[0], String.valueOf(serviceTransactionId), response.getTrxid(),
							response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	@Override
	public Response confirm(String userId, String signature, int transId) {
		log.info("Request confirm:: userId[" + userId + "] signature[XXXXXXX] transId[" + transId + "] ");
		// TODO Auto-generated method stub
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					response = utibaHandler.confirm(sessionId, transId, apiUser);

					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_CONFIRM,
							getCredential[0], response.getTrxid(), response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}
	
	@Override
	public Response confirmNoTransid(String userId, String signature) {
		log.info("Request confirm:: userId[" + userId + "] signature[XXXXXXX]");
		// TODO Auto-generated method stub
		Response response = new Response();
		
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					
					response = utibaHandler.confirmNoTransid(sessionId, apiUser);
					
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_CONFIRM,
							getCredential[0], response.getTrxid(), response.getTrxid(), response.getStatus());
					
				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		
		return response;
	}

	@Override
	public Response matrixBillPay(String userId, String signature, String recipient, String target) {
		// TODO Auto-generated method stub
		log.info("Request matrixBillPay:: userId[" + userId + "] signature[XXXXXXX] transId[" + recipient + "] target["
				+ target + "]");
		Response response = new Response();
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(recipient) || Utils.isNullorEmptyString(target)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					response = utibaHandler.matrixBillPay(sessionId, recipient, target, apiUser);
					if (response.getStatus() != Constant.RC_SUCCESS) {
						response = new Response();
						response.setStatus(Constant.RC_TRANSACTION_NOT_FOUND);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_TRANSACTION_NOT_FOUND));
					}

					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_MATRIX_INQUIRY,
							getCredential[0], response.getTrxid(), response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	public Response queryBillPay(String userId, String signature, String target, String to,
			HashMap<String, String> parameterMap) {
		Response response = new Response();
		// check parameter
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(target)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		Biller biller = dataStore.getBiller(target);
		// check biller based on target parameter
		if (biller == null) {
			response.setStatus(Constant.RC_BILLER_NOTFOUND);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_BILLER_NOTFOUND));
			return response;
		}
		log.info("biller:  " + biller.toString());

		// check api user is eligible or not, check credential and validate
		// session
		if (apiUser == null) {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
			return response;
		}
		String sessionId = sessionHandler.getSessionId();
		String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
		if (getCredential == null) {
			response.setStatus(Constant.RC_INVALID_SIGNATURE);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			return response;
		}
		int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
		if (validateSession != Constant.RC_SUCCESS) {
			response.setStatus(validateSession);
			response.setMsg(dataStore.getErrorMsg(validateSession));
			return response;
		}

		List<ExtraTransData> extList = new ArrayList<ExtraTransData>();
		for (ExtraTransData obj : biller.getExtTransDataQueryBillpay()) {
			ExtraTransData extraTransData = new ExtraTransData();
			extraTransData.setExtKey(obj.getExtKey());
			extraTransData.setExtValue(obj.getExtValue());
			extraTransData.setId(obj.getId());
			extraTransData.setParamName(obj.getParamName());

			if (Utils.isNullorEmptyString(extraTransData.getExtValue())) {
				log.info("extraTransData.getKey(): " + extraTransData.getExtKey() + " "
						+ "request.getParameter(extraTransData.getParamName()): "
						+ parameterMap.get(extraTransData.getParamName()) + " extraTransData.getParamName():"
						+ extraTransData.getParamName());
				if (parameterMap.get(extraTransData.getParamName()) != null)
					extraTransData.setExtValue(parameterMap.get(extraTransData.getParamName()));

				if (extraTransData.getParamName() != null
						&& extraTransData.getParamName().equals(Constant.INITIATOR_PARAM))
					extraTransData.setExtValue(getCredential[0]);
			}

			if (extraTransData.getExtKey().equalsIgnoreCase("now"))
				extraTransData.setExtValue(Utils.getCurrentDate("MMddhhmmss"));

			extList.add(extraTransData);
		}

		response = utibaHandler.queryBillPay(sessionId, StringUtils.isNumeric(to) == true ? to : getCredential[0],
				biller.getTarget(), extList, apiUser);

//		if (biller.getName().equalsIgnoreCase("hore")) {
//			response.setPrice(Integer.valueOf(response.getPrice()) + Integer.valueOf(response.getFee()) + "");
//		}
		
		histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_QUERY_BILLPAY, getCredential[0],
				response.getTrxid(), response.getTrxid(), response.getStatus());

		return response;
	}

	// @Override
	public Response queryBillPay(String userId, String signature, String to, String amount, String target) {
		log.info("Request queryBillPay:: userId[" + userId + "] signature[XXXXXXX] " + "to[" + to + "] amount[" + amount
				+ "] target[" + target + "] ");
		// TODO Auto-generated method stub
		Response response = new Response();
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(target)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		Biller biller = dataStore.getBiller(target);
		if (biller == null) {
			response.setStatus(Constant.RC_BILLER_NOTFOUND);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_BILLER_NOTFOUND));
			return response;
		}

		if (apiUser != null) {
			log.info("biller:  " + biller.toString());
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					List<ExtraTransData> extList = new ArrayList<ExtraTransData>();
					for (ExtraTransData obj : biller.getExtTransDataQueryBillpay()) {
						ExtraTransData extraTransData = new ExtraTransData();
						extraTransData.setExtKey(obj.getExtKey());
						extraTransData.setExtValue(obj.getExtValue());
						extraTransData.setId(obj.getId());
						extraTransData.setParamName(obj.getParamName());
						System.out
								.println("obj.getKey(): " + obj.getExtKey() + " obj.getValue(): " + obj.getExtValue());
						if (Utils.isNullorEmptyString(extraTransData.getExtValue())) {
							if (obj.getParamName().equalsIgnoreCase("msisdn")
									|| obj.getParamName().equalsIgnoreCase("to"))
								extraTransData.setExtValue(to);
							if (obj.getParamName().equalsIgnoreCase("amount")
									|| obj.getParamName().equalsIgnoreCase("denom"))
								extraTransData.setExtValue(amount);
						}

						if (extraTransData.getExtKey().equalsIgnoreCase("now"))
							extraTransData.setExtValue(Utils.getCurrentDate("MMddhhmmss"));

						extList.add(extraTransData);
					}

					response = utibaHandler.queryBillPay(sessionId,
							StringUtils.isNumeric(to) == true ? to : getCredential[0], biller.getTarget(), extList,
							apiUser);

					if (biller.getName().equalsIgnoreCase("hore")) {
						response.setPrice(Integer.valueOf(response.getPrice()) + Integer.valueOf(response.getFee()) + "");
					}
					
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_QUERY_BILLPAY,
							getCredential[0], response.getTrxid(), response.getTrxid(), response.getStatus());

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	@Override
	public Response billPay(String userId, String signature, String target, String to, String amount, String transId,
			HashMap<String, String> parameterMap) {
		log.info("Request billPay:: userId[" + userId + "] signature[XXXXXXX] " + "to[" + to + "] amount[" + amount
				+ "] target[" + target + "] transId[" + transId + "] ");

		// TODO Auto-generated method stub
		Response response = new Response();
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(target) || Utils.isNullorEmptyString(amount)
				|| Utils.isNullorEmptyString(transId)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);

		Biller biller = dataStore.getBiller(target);
		if (biller == null) {
			response.setStatus(Constant.RC_BILLER_NOTFOUND);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_BILLER_NOTFOUND));
			return response;
		}

		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					List<ExtraTransData> extList = new ArrayList<ExtraTransData>();
					for (ExtraTransData obj : biller.getExtTransDataBillpay()) {

						ExtraTransData extraTransData = new ExtraTransData();
						extraTransData.setExtKey(obj.getExtKey());
						extraTransData.setExtValue(obj.getExtValue());
						extraTransData.setId(obj.getId());
						extraTransData.setParamName(obj.getParamName());

						if (Utils.isNullorEmptyString(extraTransData.getExtValue())) {
							if (obj.getParamName().equalsIgnoreCase("msisdn")
									|| obj.getParamName().equalsIgnoreCase("to"))
								extraTransData.setExtValue(to);
							if (obj.getParamName().equalsIgnoreCase("amount")
									|| obj.getParamName().equalsIgnoreCase("denom")
									|| obj.getParamName().equalsIgnoreCase("price"))
								extraTransData.setExtValue(amount);
							if (obj.getParamName().equalsIgnoreCase("TRANSACTIONID"))
								extraTransData.setExtValue(transId);

							if (obj.getParamName() != null && obj.getParamName().equals(Constant.INITIATOR_PARAM))
								extraTransData.setExtValue(getCredential[0]);

							else
								extraTransData.setExtValue(parameterMap.get(obj.getParamName()));
						}
						
						if (biller.getName().equalsIgnoreCase("hore") && obj.getParamName().equalsIgnoreCase("price")) {
							extraTransData.setExtValue(amount);
						}
						
						log.info("[" + biller.getName() + "] Attribute Name - " + obj.getParamName() + ", Value - " + extraTransData.getExtValue());
						
						extList.add(extraTransData);

					}

					String notifAmount = amount;

					for (ExtraTransData obj : biller.getExtTransDataQueryBillpay()) {
						if (obj.getExtKey().equalsIgnoreCase("denomination"))
							if (!Utils.isNullorEmptyString(obj.getExtValue())) {
								amount = obj.getExtValue();
								break;
							}
					}

					String vcode = parameterMap.get(Constant.VOUCHER_CODE_PARAM);
					
					//validate price key param
					String fee = "0";
					if(!Utils.isNullorEmptyString(parameterMap.get("price")))
						fee = (Integer.valueOf(parameterMap.get("price")) - Integer.valueOf(amount)) + "";
					//===
					
					if (Utils.isNullorEmptyString(vcode)) {
						// biller.getName())
						//if biller == hore
						if (biller.getName().toLowerCase().equalsIgnoreCase("hore")) {
							response = utibaHandler.billPayWithAdminFee(sessionId, biller.getTarget(), transId, amount, fee,extList, null,
									apiUser);
							response.setPrice(response.getPrice() + response.getFee());
						}
						//other biller
						else
							response = utibaHandler.billPay(sessionId, biller.getTarget(), transId, amount, extList, null,
								apiUser);
					} else {
						//if biller == hore
						if (biller.getName().toLowerCase().equalsIgnoreCase("hore")) {
							response = utibaHandler.billPayWithAdminFee(sessionId, biller.getTarget(), transId, amount, fee, extList, vcode,
									apiUser);
							response.setPrice(response.getPrice() + response.getFee());
						}
						//other biller
						else
							response = utibaHandler.billPay(sessionId, biller.getTarget(), transId, amount, extList, vcode,
									apiUser);
					}

					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_BILLPAY,
							getCredential[0], response.getTrxid(), response.getTrxid(), response.getStatus());

					// ==================temporary
					if (Utils.isValidEmailAddress2(getCredential[0])) {
						// if (!StringUtils.isNumeric(getCredential[0])) {
						TransactionMail transactionMail = new TransactionMail();
						transactionMail.setTransactionTypeINA(
								Constant.TRANSACTION_TEXT_TYPE_BILL_PAYMENT + " - " + biller.getName());
						transactionMail.setTransactionTypeEN(
								Constant.TRANSACTION_TEXT_TYPE_BILL_PAYMENT + " - " + biller.getName());
						transactionMail.setDestination(to);
						transactionMail.setAmount(notifAmount);
						transactionMail.setTransId(response.getTrxid());
						transactionMail.setStatus(response.getStatus().intValue());
						mailHandler.sendEmailNotification(getCredential[0], subjectEmailTransaction, transactionMail,
								apiUser);
						// smsClientSender.sendMyntNotificaton(getCredential[0],
						// transactionMail);
					}
					// ================

				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	private void sendEmailNotification(String destination, String subject, String emailText) {
		MailSender sender = new MailSender();
		sender.setMailMessage(mailMessage);
		sender.setMailSender(mailSender);
		sender.setDestination(destination);
		sender.setSubject(subject);
		sender.setTextMessage(emailText);
		sendExecutor.execute(sender);
	}

	@Override
	public Response mobileAgnetkycUpgrade(String userId, String signature, HashMap<String, String> parameterMap) {
		// TODO Auto-generated method stub
		Response response = new Response();

		String firstName = parameterMap.get(Constant.FIRSTNAME_PARAM);
		String lastName = parameterMap.get(Constant.LASTNAME_PARAM);
		String msisdn = parameterMap.get(Constant.MSISDN_PARAM);
		String idType = parameterMap.get(Constant.ID_TYPE_PARAM);
		String idNumber = parameterMap.get(Constant.ID_NUMBER_PARAM);
		String address = parameterMap.get(Constant.ADDRESS_PARAM);
		String idPhoto = parameterMap.get(Constant.IDPHOTO_PARAM);
		String dob = parameterMap.get(Constant.DOB_PARAM);
		String motherMaidenName = parameterMap.get(Constant.MOTHER_NAME_PARAM);
		String gender = parameterMap.get(Constant.GENDER_PARAM);

		if (Utils.isNullorEmptyString(firstName) || Utils.isNullorEmptyString(lastName)
				|| Utils.isNullorEmptyString(msisdn) || Utils.isNullorEmptyString(idType)
				|| Utils.isNullorEmptyString(idNumber) || Utils.isNullorEmptyString(address)
				|| Utils.isNullorEmptyString(idPhoto) || Utils.isNullorEmptyString(dob)
				|| Utils.isNullorEmptyString(motherMaidenName) || Utils.isNullorEmptyString(gender)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		response = this.balanceCheck(userId, msisdn);

		if (response.getStatus() == Constant.RC_AGENT_NOTFOUND) {
			this.simpleReg(userId, signature, parameterMap);
		}

		response = this.updateProfile(userId, signature, parameterMap);
		if (response.getStatus() == Constant.RC_SUCCESS) {
			ApiUser apiUser = dataStore.getApiUser(userId);
			response = utibaHandler.unmapAgentExtParam(dataStore.getGlobalSessionId(), msisdn, nonKycAgentId, idNumber,
					idType, userAdmin, apiUser);
		}

		return response;
	}

	@Override
	public Response kycUpgrade(String userId, String signature, HashMap<String, String> parameterMap) {
		// TODO Auto-generated method stub
		Response response = new Response();

		String firstName = parameterMap.get(Constant.FIRSTNAME_PARAM);
		String lastName = parameterMap.get(Constant.LASTNAME_PARAM);
		String msisdn = parameterMap.get(Constant.MSISDN_PARAM);
		String idType = parameterMap.get(Constant.ID_TYPE_PARAM);
		String idNumber = parameterMap.get(Constant.ID_NUMBER_PARAM);
		String address = parameterMap.get(Constant.ADDRESS_PARAM);
		String idPhoto = parameterMap.get(Constant.IDPHOTO_PARAM);
		String dob = parameterMap.get(Constant.DOB_PARAM);
		String motherMaidenName = parameterMap.get(Constant.MOTHER_NAME_PARAM);
		String gender = parameterMap.get(Constant.GENDER_PARAM);

		if (Utils.isNullorEmptyString(firstName) || Utils.isNullorEmptyString(lastName)
				|| Utils.isNullorEmptyString(msisdn) || Utils.isNullorEmptyString(idType)
				|| Utils.isNullorEmptyString(idNumber) || Utils.isNullorEmptyString(address)
				|| Utils.isNullorEmptyString(idPhoto) || Utils.isNullorEmptyString(dob)
				|| Utils.isNullorEmptyString(motherMaidenName) || Utils.isNullorEmptyString(gender)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		parameterMap.put(Constant.KYC_REQUEST_PARAM, "true");

		response = this.updateProfile(userId, signature, parameterMap);
		if (response.getStatus() == Constant.RC_SUCCESS) {
			ApiUser apiUser = dataStore.getApiUser(userId);
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			userDao.updateStatus(getCredential[0], Constant.USER_STATUS_REQUEST_TO_KYC);
		}

		return response;
	}

	@Override
	public Response updateProfile(String userId, String signature, HashMap<String, String> parameterMap) {
		// TODO Auto-generated method stub
		Response response = new Response();

		String firstName = parameterMap.get(Constant.FIRSTNAME_PARAM);
		String lastName = parameterMap.get(Constant.LASTNAME_PARAM);
		String idType = parameterMap.get(Constant.ID_TYPE_PARAM);
		String idNumber = parameterMap.get(Constant.ID_NUMBER_PARAM);
		String address = parameterMap.get(Constant.ADDRESS_PARAM);
		String profilePic = parameterMap.get(Constant.PROFILEPIC_PARAM);
		String idPhoto = parameterMap.get(Constant.IDPHOTO_PARAM);
		String dob = parameterMap.get(Constant.DOB_PARAM);
		String motherMaidenName = parameterMap.get(Constant.MOTHER_NAME_PARAM);
		String gender = parameterMap.get(Constant.GENDER_PARAM);
		String msisdn = parameterMap.get(Constant.MSISDN_PARAM);
		String kycRequest = parameterMap.get(Constant.KYC_REQUEST_PARAM);
		String email = parameterMap.get(Constant.EMAIL_PARAM);

		if (Utils.isNullorEmptyString(firstName) || Utils.isNullorEmptyString(lastName)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {

					String name = firstName + " " + lastName;

					umarketscws.KeyValuePairMap value = new KeyValuePairMap();

					if (!Utils.isNullorEmptyString(firstName)) {
						umarketscws.KeyValuePair kvFirstName = new umarketscws.KeyValuePair();
						kvFirstName.setKey(Constant.FIRSTNAME_PARAM);
						kvFirstName.setValue(firstName);
						value.getKeyValuePair().add(kvFirstName);
					}

					if (!Utils.isNullorEmptyString(lastName)) {
						umarketscws.KeyValuePair kvLastName = new umarketscws.KeyValuePair();
						kvLastName.setKey(Constant.LASTNAME_PARAM);
						kvLastName.setValue(lastName);
						value.getKeyValuePair().add(kvLastName);
					}

					if (!Utils.isNullorEmptyString(idType)) {
						umarketscws.KeyValuePair kvIdType = new umarketscws.KeyValuePair();
						kvIdType.setKey("id_type");
						kvIdType.setValue(idType);
						value.getKeyValuePair().add(kvIdType);
					}

					if (!Utils.isNullorEmptyString(idNumber)) {
						umarketscws.KeyValuePair kvIdNumber = new umarketscws.KeyValuePair();
						kvIdNumber.setKey("id_num");
						kvIdNumber.setValue(idNumber);
						value.getKeyValuePair().add(kvIdNumber);
					}

					if (!Utils.isNullorEmptyString(address)) {
						umarketscws.KeyValuePair kvAddress = new umarketscws.KeyValuePair();
						kvAddress.setKey(Constant.ADDRESS_PARAM);
						kvAddress.setValue(address);
						value.getKeyValuePair().add(kvAddress);
					}

					if (!Utils.isNullorEmptyString(profilePic)) {
						umarketscws.KeyValuePair kvProfilePic = new umarketscws.KeyValuePair();
						kvProfilePic.setKey(Constant.PROFILEPIC_PARAM);
						kvProfilePic.setValue(this.imagePath + "/" + Utils.buildImageFilename(getCredential[0], 0));
						value.getKeyValuePair().add(kvProfilePic);
						imageHandler.writeImage(getCredential[0], 0, profilePic.replace(" ", "+"));
					}

					if (!Utils.isNullorEmptyString(idPhoto)) {
						umarketscws.KeyValuePair kvIdPhoto = new umarketscws.KeyValuePair();
						kvIdPhoto.setKey(Constant.IDPHOTO_PARAM);
						kvIdPhoto.setValue(this.imagePath + "/" + Utils.buildImageFilename(getCredential[0], 1));
						value.getKeyValuePair().add(kvIdPhoto);
						imageHandler.writeImage(getCredential[0], 1, idPhoto.replace(" ", "+"));
					}

					if (!Utils.isNullorEmptyString(dob)) {
						umarketscws.KeyValuePair kvDOB = new umarketscws.KeyValuePair();
						kvDOB.setKey("birthdate");
						kvDOB.setValue(dob);
						value.getKeyValuePair().add(kvDOB);
					}

					if (!Utils.isNullorEmptyString(motherMaidenName)) {
						// umarketscws.KeyValuePair kvMotherMaidenName = new
						// umarketscws.KeyValuePair();
						// kvMotherMaidenName.setKey("mmn");
						// kvMotherMaidenName.setValue(motherMaidenName);
						// value.getKeyValuePair().add(kvMotherMaidenName);
						//
						// umarketscws.KeyValuePair kvMotherMaidenNameDompetku =
						// new umarketscws.KeyValuePair();
						// kvMotherMaidenNameDompetku.setKey("alt_id");
						// kvMotherMaidenNameDompetku.setValue(motherMaidenName);
						// value.getKeyValuePair().add(kvMotherMaidenNameDompetku);
						umarketscws.KeyValuePair kvMotherMaidenNameDompetku = new umarketscws.KeyValuePair();
						kvMotherMaidenNameDompetku.setKey("mother");
						kvMotherMaidenNameDompetku.setValue(motherMaidenName);
						value.getKeyValuePair().add(kvMotherMaidenNameDompetku);

					}

					if (!Utils.isNullorEmptyString(gender)) {
						umarketscws.KeyValuePair kvGender = new umarketscws.KeyValuePair();
						kvGender.setKey(Constant.GENDER_PARAM);
						kvGender.setValue(gender);
						value.getKeyValuePair().add(kvGender);
					}

					if (!Utils.isNullorEmptyString(kycRequest)) {
						umarketscws.KeyValuePair kvKycReq = new umarketscws.KeyValuePair();
						kvKycReq.setKey(Constant.KYC_REQUEST_PARAM);
						kvKycReq.setValue(kycRequest);
						value.getKeyValuePair().add(kvKycReq);
					}

					// if (StringUtils.isNumeric(getCredential[0])) {// dompetku
					if (!Utils.isValidEmailAddress2(getCredential[0])) {// dompetku
						response = utibaHandler.updateProfile(sessionId, getCredential[0], name, msisdn, email, value,
								apiUser);
					} else {// mynt
						User user = userDao.get(getCredential[0]);
						if (user.getStatus() == Constant.USER_STATUS_ACTIVE) {
							response = utibaHandler.updateProfile(sessionId, getCredential[0], name, msisdn, email,
									value, apiUser);
						} else {
							response.setStatus(Constant.RC_AGENT_UPGRADE_SUSPENDED);
							response.setMsg(dataStore.getErrorMsg(Constant.RC_AGENT_UPGRADE_SUSPENDED));
						}
					}
				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}

		return response;
	}

	// Added By Ryanbhuled
	public Response updateUserBecomeModifier(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		int status = Integer.parseInt(parameter.getKeyValueMap().get(Constant.TYPE_PARAM));

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = utibaHandler.updateAgentToModifyAble(authResponse.getSessionId(), msisdn, status, apiUser);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response registerGeneratePin(String userId, String signature, Parameter parameter) {
		// TODO Auto-generated method stub
		String firstName = parameter.getKeyValueMap().get(Constant.FIRSTNAME_PARAM);
		String lastName = parameter.getKeyValueMap().get(Constant.LASTNAME_PARAM);
		String dob = parameter.getKeyValueMap().get(Constant.DOB_PARAM);
		String gender = parameter.getKeyValueMap().get(Constant.GENDER_PARAM);
		String motherMaidenName = parameter.getKeyValueMap().get(Constant.MOTHER_NAME_PARAM);
		String address = parameter.getKeyValueMap().get(Constant.ADDRESS_PARAM);
		String idType = parameter.getKeyValueMap().get(Constant.ID_TYPE_PARAM);
		String idNo = parameter.getKeyValueMap().get(Constant.ID_NUMBER_PARAM);
		String email = parameter.getKeyValueMap().get(Constant.EMAIL_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);

		log.info("register Request: userId[" + userId + "] 	signature[" + "XXXXXX" + "] firstName[" + firstName
				+ "] lastName[" + lastName + "] " + "dob[" + dob + "] gender[" + gender + "] motherMaidenName["
				+ "XXXXXX" + "] " + "address[" + address + "] idType[" + idType + "] idNo[" + idNo + "] msisdn["
				+ msisdn + "]");
		Response response = new Response();
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(firstName) || !Utils.isValidDate(dob)
				|| Utils.isNullorEmptyString(email)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		// response = eKTP(userId, signature, parameter);
		response = eKTP(parameter);
		if (response.getStatus() == Constant.RC_EKTP_NOT_VALID) {
			response.setStatus(Constant.RC_EKTP_NOT_VALID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_EKTP_NOT_VALID));
			return response;
		} else if (response.getStatus() == Constant.RC_EKTP_NOT_EXISTS) {
			response.setStatus(Constant.RC_EKTP_NOT_EXISTS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_EKTP_NOT_EXISTS));
			return response;
			// timeout
		} else if (response.getStatus() == Constant.RC_EKTP_UNKNOWN_ERROR) {
			response.setStatus(Constant.RC_EKTP_UNKNOWN_ERROR);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_EKTP_UNKNOWN_ERROR));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			// Signature Check
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				String username = email;
				String pin = Utils.generateNumber(8);
				User user = userDao.get(username);
				if (user == null) {
					user = new User();
					StringBuilder parameters = new StringBuilder(username).append("|").append(msisdn).append("|")
							.append(pin).append("|").append("generatedPin");
					String encryptedText = "";
					try {
						DESedeEncryption encryption = new DESedeEncryption(encKey);
						encryptedText = encryption.encrypt(parameters.toString());
						encryptedText = URLEncoder.encode(encryptedText, "UTF-8");
						log.debug("encryptedText: " + encryptedText);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					user.setUsername(username);
					user.setFirstName(firstName);
					user.setLastName(lastName == null ? firstName : lastName);
					user.setDob(dob);
					user.setGender(gender);
					user.setMotherMaidenName(motherMaidenName);
					user.setAddress(address);
					user.setIdType(idType);
					user.setIdNo(idNo);
					user.setMsisdn(msisdn);
					user.setEncryptedText(encryptedText);
					user.setStatus(Constant.USER_STATUS_INITIAL);
					String accountId = userDao.save(user);
					log.info("Save username: " + username + " with account ID: " + accountId);
					if (accountId != null) {
						String url = activationURL.replace("[%KEY%]", encryptedText);
						String body = this.bodyActEmail;
						body = body.replace("[%NAME%]", firstName + " " + lastName);
						body = body.replace("[%URL%]", url);
						this.sendEmailNotification(username, this.subjectActEmail, body);
						histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_REGISTER,
								getCredential[0], "0", "0", 0);

						response.setStatus(Constant.RC_SUCCESS);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
					} else {
						response.setStatus(Constant.RC_DB_ERROR);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_DB_ERROR));
					}
				} else if (user.getStatus() == 0) {
					String encryptedText = "";
					StringBuilder parameters = new StringBuilder(username).append("|").append(msisdn).append("|")
							.append(pin).append("|").append("generatedPin");
					try {
						DESedeEncryption encryption = new DESedeEncryption(encKey);
						encryptedText = encryption.encrypt(parameters.toString());
						encryptedText = URLEncoder.encode(encryptedText, "UTF-8");
						log.debug("encryptedText: " + encryptedText);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					String url = activationURL.replace("[%KEY%]", encryptedText);
					String body = this.bodyActEmail;
					body = body.replace("[%NAME%]", firstName + " " + lastName);
					body = body.replace("[%URL%]", url);
					this.sendEmailNotification(username, this.subjectActEmail, body);
					histList.writeHist(apiUser.getId(), apiUser.getChannelType(), Constant.SERVICE_REGISTER,
							getCredential[0], "0", "0", 0);

					response.setStatus(Constant.RC_SUCCESS);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
				} else {
					response.setStatus(Constant.RC_USER_ALREADY_EXIST);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_USER_ALREADY_EXIST));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	public Response removeAgent(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		// String key =
		// parameter.getKeyValueMap().get(Constant.PUBLIC_KEY_PARAM);

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		ApiUser ap = dataStore.getApiUser(userId);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			// if(key.equals(ap.getPassKey()))
			response = utibaHandler.removeAgent(authResponse.getSessionId(), msisdn, ap);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response programInformer(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String sleepTime = parameter.getKeyValueMap().get(Constant.SLEEP_PARAM);
		String type = parameter.getKeyValueMap().get(Constant.TYPE_PARAM);
		String smsEncoded = "";
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn) || Utils.isNullorEmptyString(type)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (utibaHandler.getAgentByReferenceRequest(msisdn, apiUser).getStatus().equals(Constant.RC_AGENT_NOTFOUND)) {
			AuthResponse authResponse = authenticationRequest.validate(userId, signature);
			if (authResponse.getStatus() == Constant.RC_SUCCESS) {
				try {
					Thread.sleep(Integer.parseInt(sleepTime));
				} catch (InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
				response = utibaHandler.mapAgent(authResponse.getSessionId(), msisdn, unregSubsInformer,
						authResponse.getInitiator(), apiUser);
				if (type.equalsIgnoreCase("1")) {
					if (response.getStatus() == Constant.RC_SUCCESS) {
						if (smsInfo.contains("~pound~")) {
							smsEncoded = smsInfo.replace("~pound~", "#");
							smsClientSender.sendByPutu(smsEncoded, msisdn);
						} else {
							smsClientSender.send(smsInfo, msisdn);
						}
					} else if (response.getStatus() == Constant.RC_ALREADY_ON_GROUP) {
						if (smsInfo.contains("~pound~")) {
							smsEncoded = smsInfoReminder.replace("~pound~", "#");
							smsClientSender.sendByPutu(smsEncoded, msisdn);
						} else {
							smsClientSender.send(smsInfo, msisdn);
						}
					}
				}
			} else {
				response.setStatus(authResponse.getStatus());
				response.setMsg(authResponse.getMsg());
			}
		} else {
			response.setStatus(Constant.RC_AGENT_ALREADY_REGISTERED);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_AGENT_ALREADY_REGISTERED));
			return response;
		}
		return response;
	}

	public Response checkTransactionById(String userId, String signature, String extRef) {
		log.info("Request checkTransaction:: userId[" + userId + "] signature[XXXXXXX] extRef[" + extRef + "]");
		// TODO Auto-generated method stub
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					response = utibaHandler.getTransactionById(sessionId, Integer.parseInt(extRef), 999999, apiUser);
					if (response.getStatus() != Constant.RC_SUCCESS) {
						response.setStatus(Constant.RC_TARGET_NOTFOUND);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_TARGET_NOTFOUND));
					}
				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	@Override
	public Response getTrxByTrxid(String userId, String signature, Parameter parameter) {
		log.info("Request checkTransaction:: userId[" + userId + "] signature[XXXXXXX]");
		// TODO Auto-generated method stub
		Response response = new Response();
		String trxid = parameter.getKeyValueMap().get(Constant.TRXID_PARAM);

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					response = utibaHandler.getTransactionDetail(sessionId, Integer.parseInt(trxid), apiUser);
					if (response.getStatus() != Constant.RC_SUCCESS) {
						response.setStatus(response.getStatus());
						response.setMsg(dataStore.getErrorMsg(response.getStatus()));
					}
				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		response.setAgentData(null);
		return response;
	}

	// ============Artajasa DompetkuPlus ================
	public Response ajInquiry(String userId, String signature, Parameter parameter) {
		Response response = new Response();

		// Setting up param
		String bankCode = parameter.getKeyValueMap().get(Constant.BANK_CODE_PARAM);
		String accTarget = parameter.getKeyValueMap().get(Constant.BANK_ACC_NO);
		String amount = parameter.getKeyValueMap().get(Constant.AMOUNT_PARAM);

		// Validation Param
		if (Utils.isNullorEmptyString(bankCode) || Utils.isNullorEmptyString(accTarget)
				|| Utils.isNullorEmptyString(amount)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			ApiUser apiUser = dataStore.getApiUser(userId);
			String xmlData = appParam.getAjDplusInqXML();
			long epochTime = System.currentTimeMillis() / 1000L;
			xmlData = xmlData.replace(Constant.UNIX_TIME_REPLACE_TAG, String.valueOf(epochTime));
			// get from UTIBA
			String accID = authResponse.getInitiator();
			String pass = authResponse.getPassword();
			Response utibaResponse = utibaHandler.getAgentByReferenceRequest(accID, apiUser);
			String msisdn = utibaResponse.getAgentData().getSmsAddress();
			xmlData = xmlData.replace(Constant.ACCOUNTID_REPLACE_TAG, accID);
			xmlData = xmlData.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
			// use random data
			// String randData = Utils.generateNumber(6);
			String trxId = utibaResponse.getTrxid();
			xmlData = xmlData.replace(Constant.TRANSACTIONID_REPLACE_TAG, trxId);
			// get pass and encrypt to AES
			final String key = appParam.getAjAesKey();
			String partTranid = trxId.substring(0, 1);
			String partAccID = accID.substring(0, 1);
			String clearPass = partTranid + partAccID + pass;
			String passEnc = AESEncryption.encrypt(clearPass.trim(), key);
			xmlData = xmlData.replace(Constant.PIN_REPLACE_TAG, passEnc);

			xmlData = xmlData.replace(Constant.BANK_CODE_REPLACE_TAG, bankCode);
			xmlData = xmlData.replace(Constant.TO_REPLACE_TAG, accTarget);
			xmlData = xmlData.replace(Constant.AMOUNT_REPLACE_TAG, amount);
			String xmlDataUTF8 = xmlData;
			try {
				xmlDataUTF8 = URLEncoder.encode(xmlData, "UTF-8");
			} catch (UnsupportedEncodingException e1) {
				response.setStatus(Constant.RC_SYSTEM_ERROR);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
			}

			List<MediaType> mediaTypes = new LinkedList<MediaType>();
			mediaTypes.add(MediaType.APPLICATION_XML);
			HttpEntity<String> he = httpClientSender.sendPost(appParam.getAjDplusURL(), xmlDataUTF8, mediaTypes);
			if (he != null) {
				String xmlRes = he.getBody();
				try {
					xmlRes = java.net.URLDecoder.decode(he.getBody(), "UTF-8");
				} catch (UnsupportedEncodingException e) {
					response.setStatus(Constant.RC_SYSTEM_ERROR);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
				}
				String rc = Utils.getXmlTagValue(xmlRes, "<rc>");
				String dkrc = Utils.getXmlTagValue(xmlRes, "<dkrc>");
				String issname = Utils.getXmlTagValue(xmlRes, "<issname>");
				String destbankname = Utils.getXmlTagValue(xmlRes, "<destbankname>");
				String destname = Utils.getXmlTagValue(xmlRes, "<destname>");

				log.debug("Response XML:" + xmlRes);
				log.debug("RC: " + rc + "~DKRC: " + dkrc + "~ISSName: " + issname + "~DestBank: " + destbankname
						+ "~DestCustName: " + destname);
				if (rc.equals("00")) {
					TransactionArtajasa ajTrans = new TransactionArtajasa();
					ajTrans.setTransTime(epochTime);
					ajTrans.setTransID(trxId);
					// ajTrans.setTransType("inq");
					ajTrans.setMsisdn(msisdn);
					ajTrans.setAccId(accID);
					ajTrans.setAmount(amount);
					ajTrans.setRc(rc);
					ajTrans.setDrkc(dkrc);
					ajTrans.setIssname(issname);
					ajTrans.setAccTarget(accTarget);
					ajTrans.setDestAccName(destname);
					ajTrans.setDestBankName(destbankname);
					// ajTrans.setDestBankId(bankCode);
					response.setArtaJasaTrans(ajTrans);
					response.setStatus(Constant.RC_SUCCESS);
					response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
				} else if (rc.equals("76")) {
					response.setStatus(Integer.parseInt(rc));
					response.setMsg("rc-" + rc + "=>Nomor Tujuan Salah / Invalid Destination Number");
				} else if (rc.equals("51")) {
					response.setStatus(Integer.parseInt(rc));
					response.setMsg("rc-" + rc + "=>Saldo Anda tidak mencukupi/Insuficient Fund");
					// } else if (rc.equals("68")) {
					// response.setStatus(Integer.parseInt(rc));
					// response.setMsg("rc-" + rc + "=>Transaksi Sedang
					// Diproses/Timeout");
				} else if (rc.equals("89") || rc.equals("91") || rc.equals("05") || rc.equals("68")) {
					response.setStatus(Constant.RC_TRANSACTIN_FAILED);
					response.setMsg("rc-err-" + rc + ":Transaksi Gagal : Transaksi sedang tidak dapat dilakukan");
				} else {
					response.setStatus(Constant.RC_TRANSACTIN_FAILED);
					response.setMsg("rc-err-" + rc);
				}
			} else {
				response.setStatus(Constant.RC_TIMEOUT);
				response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		log.debug("RESPONSE:" + response.toString());
		return response;
	}

	public Response ajTransfer(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msgAJStr = "";
		String msgAJStrShort = "";

		String token = parameter.getKeyValueMap().get(Constant.TOKEN_PARAM);
		String accTarget = parameter.getKeyValueMap().get(Constant.BANK_ACC_NO);
		String amount = parameter.getKeyValueMap().get(Constant.AMOUNT_PARAM);
		String bankCode = parameter.getKeyValueMap().get(Constant.BANK_CODE_PARAM);
		String bankName = parameter.getKeyValueMap().get(Constant.BANK_NAME_PARAM);
		String issName = parameter.getKeyValueMap().get(Constant.ISSNAME_PARAM);
		String destName = parameter.getKeyValueMap().get(Constant.DESTNAME_PARAM);
		String transID = parameter.getKeyValueMap().get(Constant.TRANSID_PARAM);

		if (Utils.isNullorEmptyString(token)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		ApiUser apiUser = dataStore.getApiUser(userId);
		String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			String xmlData = appParam.getAjDplusTransXML();
			long epochTime = System.currentTimeMillis() / 1000L;
			xmlData = xmlData.replace(Constant.UNIX_TIME_REPLACE_TAG, String.valueOf(epochTime));
			// get from UTIBA
			String accID = authResponse.getInitiator();
			Response utibaResponse = utibaHandler.getAgentByReferenceRequest(accID, apiUser);
			String msisdn = utibaResponse.getAgentData().getSmsAddress();

			Prefix prefix = dataStore.getTelcoIdByPrefix(msisdn);
			// String trxId = utibaResponse.getTrxid();
			xmlData = xmlData.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
			xmlData = xmlData.replace(Constant.PIN_REPLACE_TAG, token);
			xmlData = xmlData.replace(Constant.ACCOUNTID_REPLACE_TAG, accID);
			xmlData = xmlData.replace(Constant.TO_REPLACE_TAG, accTarget);
			xmlData = xmlData.replace(Constant.AMOUNT_REPLACE_TAG, amount);
			xmlData = xmlData.replace(Constant.BANK_CODE_REPLACE_TAG, bankCode);
			xmlData = xmlData.replace(Constant.ISS_NAME_REPLACE_TAG, issName);
			xmlData = xmlData.replace(Constant.DEST_NAME_REPLACE_TAG, destName);
			xmlData = xmlData.replace(Constant.TRANSACTIONID_REPLACE_TAG, transID);

			String xmlDataUTF8 = xmlData;
			try {
				xmlDataUTF8 = URLEncoder.encode(xmlData, "UTF-8");
			} catch (UnsupportedEncodingException e1) {
				response.setStatus(Constant.RC_SYSTEM_ERROR);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
			}

			List<MediaType> mediaTypes = new LinkedList<MediaType>();
			String descMsg = "Transaction Failed";
			mediaTypes.add(MediaType.APPLICATION_XML);
			HttpEntity<String> he = httpClientSender.sendPost(appParam.getAjDplusURL(), xmlDataUTF8, mediaTypes);
			if (he != null) {
				String xmlRes = he.getBody();
				try {
					xmlRes = java.net.URLDecoder.decode(he.getBody(), "UTF-8");
				} catch (UnsupportedEncodingException e) {
					response.setStatus(Constant.RC_SYSTEM_ERROR);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
				}
				String rc = Utils.getXmlTagValue(xmlRes, "<rc>");
				String dkrc = Utils.getXmlTagValue(xmlRes, "<dkrc>");
				String rrn = Utils.getXmlTagValue(xmlRes, "<rrn>");
				String stan = Utils.getXmlTagValue(xmlRes, "<stan>");
				String dkutranid = Utils.getXmlTagValue(xmlRes, "<dkutranid>");
				String issname = Utils.getXmlTagValue(xmlRes, "<issname>");
				String destname = Utils.getXmlTagValue(xmlRes, "<destname>");
				String trxId = dkutranid;
				log.debug("Response XML:" + xmlRes);
				log.debug("RC: " + rc + "~DKRC: " + dkrc + "~RRN: " + rrn + "~Stan: " + stan + "~dkutranid: "
						+ dkutranid);
				if (rc.equals("00")) {
					TransactionArtajasa ajTrans = new TransactionArtajasa();
					ajTrans.setTransTime(epochTime);
					ajTrans.setTransID(trxId);
					// ajTrans.setTransType("trf");
					ajTrans.setMsisdn(msisdn);
					ajTrans.setAccId(accID);
					ajTrans.setAmount(amount);
					ajTrans.setRc(rc);
					ajTrans.setDrkc(dkrc);
					ajTrans.setIssname(issName);
					// ajTrans.setDestBankId(bankCode);
					ajTrans.setDestBankName(issName);
					ajTrans.setRrn(rrn);
					ajTrans.setStan(stan);
					ajTrans.setDkutranid(dkutranid);
					response.setArtaJasaTrans(ajTrans);
					response.setStatus(Constant.RC_SUCCESS);
					response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
					descMsg = "Transaction Success";
				} else {
					if (rc.equals("89") || rc.equals("91") || rc.equals("05")) {
						response.setStatus(Integer.parseInt(rc));
						response.setMsg("rc-" + rc
								+ "=>Transaksi Gagal : Transaksi sedang tidak dapat dilakukan/Transaction Unavailable");
					} else if (rc.equals("76")) {
						response.setStatus(Integer.parseInt(rc));
						response.setMsg("rc-" + rc + "=>Nomor Tujuan Salah / Invalid Destination Number");
					} else if (rc.equals("68")) {
						response.setStatus(Integer.parseInt(rc));
						response.setMsg("rc-" + rc + "=>Transaksi Sedang Diproses");
						descMsg = "Transaksi Sedang Diproses/Transaction on process";
					} else if (rc.equals("51")) {
						response.setStatus(Integer.parseInt(rc));
						response.setMsg("rc-" + rc + "=>Saldo Anda tidak mencukupi/Insuficient Fund");
					} else if (rc.equals("13")) {
						response.setStatus(Integer.parseInt(rc));
						response.setMsg("rc-" + rc
								+ "=>Transaksi Gagal : Transaksi sedang tidak diizinkan/Trasaction not permitted");
					} else if (rc.equals("15")) {
						response.setStatus(Integer.parseInt(rc));
						response.setMsg("rc-" + rc + "=>Transaksi Gagal : Token Salah/Wrong Token");
					} else if (rc.equals("14")) {
						response.setStatus(Integer.parseInt(rc));
						response.setMsg(
								"rc-" + rc + "=>Transaksi Gagal : Nomor akun tidak terdaftar / Account unregistered");
					} else if (rc.equals("55")) {
						response.setStatus(Integer.parseInt(rc));
						response.setMsg("rc-" + rc + "=>Transaksi Gagal : Salah Password / Wrong Password");
					} else if (rc.equals("75")) {
						response.setStatus(Integer.parseInt(rc));
						response.setMsg(
								"rc-" + rc + "=>Account Terblokir, Salah Password Melebih Batas / Blocked Account");
					} else if (rc.equals("31")) {
						response.setStatus(Integer.parseInt(rc));
						response.setMsg("rc-" + rc + "=>Bank tidak terdaftar / Bank unregistered");
					} else {
						response.setStatus(Integer.parseInt(rc));
						response.setMsg("rc-" + rc + "=>General Error");
					}
				}

				// Check notif, and send to firebase if not Indosat{
				// if (!prefix.getName().equalsIgnoreCase("INDOSAT")&&
				// !Utils.isValidEmailAddress2(utibaResponse.getAgentData().getEmail()))
				// {
				if (StringUtils.isNumeric(getCredential[0])
						&& !Utils.isValidEmailAddress2(utibaResponse.getAgentData().getEmail())) {
					msgAJStr = emailBuilder.firebaseTransferToBankMsg("full", descMsg, amount, getCredential[0],
							accTarget, destName, bankName, rrn, trxId);
					msgAJStrShort = emailBuilder.firebaseTransferToBankMsg("short", descMsg, amount, getCredential[0],
							accTarget, destName, bankName, rrn, trxId);
					mailHandler.sendFirebaseNotification("BANK TRANSFER", msisdn, trxId, msgAJStr, msgAJStrShort);
				} else {
					msgAJStr = emailBuilder.transactionAJ(Constant.TRANSACTION_TEXT_TYPE_SEND_MONEY_INA,
							Constant.TRANSACTION_TEXT_TYPE_SEND_MONEY_EN, accTarget, destname, issname,
							getCredential[0], amount, trxId, rrn, descMsg, bankCode, bankName, destName);
					mailHandler.sendEmailNotification(utibaResponse.getAgentData().getEmail(),
							"Dompetku Plus - Bank Transfer", msgAJStr);
				}
			} else {
				// if (!prefix.getName().equalsIgnoreCase("INDOSAT")&&
				// !Utils.isValidEmailAddress2(utibaResponse.getAgentData().getEmail()))
				// {
				if (StringUtils.isNumeric(getCredential[0])
						&& !Utils.isValidEmailAddress2(utibaResponse.getAgentData().getEmail())) {
					response.setStatus(Constant.RC_TIMEOUT);
					response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
					msgAJStr = emailBuilder.firebaseTransferToBankMsg("full", descMsg, amount, getCredential[0],
							accTarget, destName, bankName, "N/A", transID);
					msgAJStrShort = emailBuilder.firebaseTransferToBankMsg("short", descMsg, amount, getCredential[0],
							accTarget, destName, bankName, "N/A", transID);
					try {
						mailHandler.sendFirebaseNotification("BANK TRANSFER", msisdn, transID, msgAJStr, msgAJStrShort);
					} catch (Exception ex) {
						response.setStatus(99);
						response.setMsg("rc-" + 99 + "=>General Error:" + ex.getMessage());
					}
				} else {
					response.setStatus(Constant.RC_TIMEOUT);
					response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
					msgAJStr = emailBuilder.transactionAJ(Constant.TRANSACTION_TEXT_TYPE_SEND_MONEY_INA,
							Constant.TRANSACTION_TEXT_TYPE_SEND_MONEY_EN, accTarget, destName, getCredential[0],
							issName, amount, "N/A", "N/A", "Transaksi Sedang Diproses/Transaction on process", bankCode,
							bankName, destName);
					mailHandler.sendEmailNotification(utibaResponse.getAgentData().getEmail(),
							"Dompetku Plus - Suspect Transaction", msgAJStr);
				}
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		log.debug("RESPONSE:" + response.toString());
		return response;
	}
	

//	public Response ajInquirySimulator(String userId, String signature, Parameter parameter) {
//		Response response = new Response();
//
//		// Setting up param
//		String bankCode = parameter.getKeyValueMap().get(Constant.BANK_CODE_PARAM);
//		String accTarget = parameter.getKeyValueMap().get(Constant.BANK_ACC_NO);
//		String amount = parameter.getKeyValueMap().get(Constant.AMOUNT_PARAM);
//
//		// Validation Param
//		if (Utils.isNullorEmptyString(bankCode) || Utils.isNullorEmptyString(accTarget)
//				|| Utils.isNullorEmptyString(amount)) {
//			response.setStatus(Constant.RC_INVALID_PARAMETERS);
//			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
//			return response;
//		}
//
//		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
//		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
//			if(bankCode.equals("014") && accTarget.equals("5271120223")){//success
//				TransactionArtajasa ajTrans = new TransactionArtajasa();
//				ajTrans.setTransTime(new Date().getTime());
//				ajTrans.setTransID(Utils.generateNumber(8));
//				ajTrans.setMsisdn(authResponse.getInitiator());
//				ajTrans.setAmount(amount);
//				ajTrans.setRc("00");
//				ajTrans.setDrkc("0");
//				ajTrans.setIssname("MEGI JAKA PERMANA");
//				ajTrans.setAccTarget(accTarget);
//				ajTrans.setDestAccName("MEGI JAKA PERMANA");
//				ajTrans.setDestBankName("BCA");
//				response.setArtaJasaTrans(ajTrans);
//				response.setStatus(Constant.RC_SUCCESS);
//				response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
//			}else if(bankCode.equals("213") && accTarget.equals("90010509946")){//pending
//				TransactionArtajasa ajTrans = new TransactionArtajasa();
//				ajTrans.setTransTime(new Date().getTime());
//				ajTrans.setTransID(Utils.generateNumber(8));
//				ajTrans.setMsisdn(authResponse.getInitiator());
//				ajTrans.setAmount(amount);
//				ajTrans.setRc("00");
//				ajTrans.setDrkc("0");
//				ajTrans.setIssname("MEGI JAKA PERMANA");
//				ajTrans.setAccTarget(accTarget);
//				ajTrans.setDestAccName("MEGI JAKA PERMANA");
//				ajTrans.setDestBankName("BTPN");
//				response.setArtaJasaTrans(ajTrans);
//				response.setStatus(Constant.RC_SUCCESS);
//				response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
//			}else{
//				response.setStatus(76);
//				response.setMsg("rc- 76 =>Nomor Tujuan Salah / Invalid Destination Number");
//			}
//			
//			
//		} else {
//			response.setStatus(authResponse.getStatus());
//			response.setMsg(authResponse.getMsg());
//		}
//		log.debug("RESPONSE:" + response.toString());
//		return response;
//	}
//
//	public Response ajTransferSimulator(String userId, String signature, Parameter parameter) {
//		Response response = new Response();
//		String msgAJStr = "";
//		String msgAJStrShort = "";
//
//		String token = parameter.getKeyValueMap().get(Constant.TOKEN_PARAM);
//		String accTarget = parameter.getKeyValueMap().get(Constant.BANK_ACC_NO);
//		String amount = parameter.getKeyValueMap().get(Constant.AMOUNT_PARAM);
//		String bankCode = parameter.getKeyValueMap().get(Constant.BANK_CODE_PARAM);
//		String bankName = parameter.getKeyValueMap().get(Constant.BANK_NAME_PARAM);
//		String issName = parameter.getKeyValueMap().get(Constant.ISSNAME_PARAM);
//		String destName = parameter.getKeyValueMap().get(Constant.DESTNAME_PARAM);
//		String transID = parameter.getKeyValueMap().get(Constant.TRANSID_PARAM);
//
//		if (Utils.isNullorEmptyString(token) || Utils.isNullorEmptyString(accTarget) 
//				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(bankCode) 
//				|| Utils.isNullorEmptyString(bankName) || Utils.isNullorEmptyString(issName) 
//				|| Utils.isNullorEmptyString(destName) || Utils.isNullorEmptyString(transID)) {
//			response.setStatus(Constant.RC_INVALID_PARAMETERS);
//			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
//			return response;
//		}
//
//		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
//		ApiUser apiUser = dataStore.getApiUser(userId);
//		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
//			
//			if(bankCode.equals("014") && accTarget.equals("5271120223")){//success
//				
//				Response couponTransferResp = this.couponTransfer("web_api_test", "f1/mCUN//SqjBN88aTxTsCi0iHjOrNv5Jvn84++1oAY=", 
//						authResponse.getInitiator(), transID, amount, token);
//				if(couponTransferResp.getStatus() != Constant.RC_SUCCESS){
//					response.setStatus(couponTransferResp.getStatus());
//					response.setMsg(dataManager.getErrorMsg(couponTransferResp.getStatus()));
//					return response;
//				}
//				
//				TransactionArtajasa ajTrans = new TransactionArtajasa();
//				ajTrans.setTransTime(new Date().getTime());
//				ajTrans.setTransID(Utils.generateNumber(8));
//				ajTrans.setMsisdn(authResponse.getInitiator());
//				ajTrans.setAmount(amount);
//				ajTrans.setRc("00");
//				ajTrans.setDrkc("0");
//				ajTrans.setIssname("MEGI JAKA PERMANA");
//				ajTrans.setAccTarget(accTarget);
//				ajTrans.setDestAccName("MEGI JAKA PERMANA");
//				ajTrans.setDestBankName("BCA");
//				ajTrans.setIssname(issName);
//				ajTrans.setDestBankName(issName);
//				response.setArtaJasaTrans(ajTrans);
//				response.setStatus(Constant.RC_SUCCESS);
//				response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
//			}else if(bankCode.equals("213") && accTarget.equals("90010509946")){//pending
//				
//
//				Response couponTransferResp = this.couponTransfer("web_api_test", "f1/mCUN//SqjBN88aTxTsCi0iHjOrNv5Jvn84++1oAY=", 
//						authResponse.getInitiator(), transID, amount, token);
//				if(couponTransferResp.getStatus() != Constant.RC_SUCCESS){
//					response.setStatus(couponTransferResp.getStatus());
//					response.setMsg(dataManager.getErrorMsg(couponTransferResp.getStatus()));
//					return response;
//				}
//				
//				TransactionArtajasa ajTrans = new TransactionArtajasa();
//				ajTrans.setTransTime(new Date().getTime());
//				ajTrans.setTransID(Utils.generateNumber(8));
//				ajTrans.setMsisdn(authResponse.getInitiator());
//				ajTrans.setAmount(amount);
//				ajTrans.setRc("68");
//				ajTrans.setDrkc("0");
//				ajTrans.setIssname("MEGI JAKA PERMANA");
//				ajTrans.setAccTarget(accTarget);
//				ajTrans.setDestAccName("MEGI JAKA PERMANA");
//				ajTrans.setDestBankName("BTPN");
//				ajTrans.setIssname(issName);
//				ajTrans.setDestBankName(issName);
//				response.setArtaJasaTrans(ajTrans);
//				response.setStatus(68);
//				response.setMsg("rc- 68 =>Transaksi Sedang Diproses");
//			}else{
//				response.setStatus(76);
//				response.setMsg("rc- 76 =>Nomor Tujuan Salah / Invalid Destination Number");
//			}
//			
//		
//		} else {
//			response.setStatus(authResponse.getStatus());
//			response.setMsg(authResponse.getMsg());
//		}
//		log.debug("RESPONSE:" + response.toString());
//		return response;
//	}

	// ============E-KTP ================
	// public Response eKTP(String userId, String signature, Parameter
	// parameter) {
	public Response eKTP(Parameter parameter) {
		Response response = new Response();
		List<MediaType> mediaTypes;
		MultiValueMap<String, String> paramKTP = new LinkedMultiValueMap<String, String>();

		String idNumber = parameter.getKeyValueMap().get(Constant.ID_NUMBER_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String keyword = parameter.getKeyValueMap().get(Constant.KEYWORD_PARAM);
		String trxid = "PPRO-" + System.currentTimeMillis() / 1000;

		if (Utils.isNullorEmptyString(idNumber) || Utils.isNullorEmptyString(msisdn)
				|| Utils.isNullorEmptyString(keyword)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		if (idNumber.length() < 10) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg("Format NIK Salah");
			return response;
		}

		if (keyword.length() < 2) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg("Nama Ibu/Nomor KK Salah");
			return response;
		}
		response = kycProc.ektpHit(msisdn, idNumber, keyword, trxid);
		return response;
	}

	public Response eKTPConfirm(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String idNumber = parameter.getKeyValueMap().get(Constant.ID_NUMBER_PARAM);
		String keyword = parameter.getKeyValueMap().get(Constant.KEYWORD_PARAM);

		if (Utils.isNullorEmptyString(idNumber) || idNumber.length() < 10 || Utils.isNullorEmptyString(keyword)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			if (idNumber.length() < 10)
				response.setMsg("Format NIK Salah");
			else
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			// response = eKTP(userId, signature, parameter);
			response = eKTP(parameter);
			if (response.getStatus() == Constant.RC_SUCCESS) {
				StringMetric metric = StringMetrics.damerauLevenshtein();
				AgentData ag = response.getAgentData();
				if (metric.compare(keyword.trim().toLowerCase(),
						response.getAgentData().getMotherMaidenName().trim().toLowerCase()) > 0.8f
						|| keyword.equalsIgnoreCase(response.getAgentData().getNo_kk())) {
					return response;
				} else {
					response = new Response();
					response.setStatus(Constant.RC_INVALID_USERID);
					response.setMsg("Nama Ibu /Nomor KK tidak sesuai dengan data pada database kami");
					return response;
				}
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	public Response eKTPConfirmResultNameOnly(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String idNumber = parameter.getKeyValueMap().get(Constant.ID_NUMBER_PARAM);

		if (Utils.isNullorEmptyString(idNumber) || idNumber.length() < 10) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			if (idNumber.length() < 10)
				response.setMsg("Format NIK Salah");
			else
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = eKTP(parameter);
			if (response.getStatus() == Constant.RC_SUCCESS) {
				String firstName = response.getAgentData().getFirstName();
				String lastName = response.getAgentData().getLastName();
				AgentData resAG = new AgentData();
				resAG.setFirstName(firstName);
				resAG.setLastName(lastName);
				response.setAgentData(resAG);

				return response;
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response doKYCKTP(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdnTo = null;

		String idNumber = parameter.getKeyValueMap().get(Constant.ID_NUMBER_PARAM);
		String keyword = parameter.getKeyValueMap().get(Constant.KEYWORD_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String type = parameter.getKeyValueMap().get(Constant.TYPE_PARAM);
//		String idphoto = parameter.getKeyValueMap().get(Constant.IDPHOTO_PARAM);
//		String idphotouser = parameter.getKeyValueMap().get(Constant.IDPHOTOUSER_PARAM);
		String sms = "";
		
		if (Utils.isNullorEmptyString(idNumber) || Utils.isNullorEmptyString(keyword)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		// if (!Utils.validateMSISDN(msisdn)) {
		if (!StringUtils.isNumeric(msisdn)) {
			response.setStatus(Constant.RC_INVALID_MSISDN_FORMAT);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_MSISDN_FORMAT));
			return response;
		}
		
		//BLOCKED DUE TO SUSPECT TRX
		if(kycBloc==1){
			response.setStatus(Constant.RC_UNKNOWN_ERROR);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_UNKNOWN_ERROR));
			sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
					.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "");
			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		msisdnTo = authResponse.getInitiator();
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			// log.debug(authResponse);
			response = eKTP(parameter);
			if (response.getStatus() == Constant.RC_SUCCESS) {
				StringMetric metric = StringMetrics.damerauLevenshtein();
				AgentData ag = response.getAgentData();
				//Add photo id and store to our server
//				if(!Utils.isNullorEmptyString(idphoto)){
//					idphoto = (this.imagePath + "/" + Utils.buildImageFilename(msisdn, Constant.TYPE_PHOTO_ID_ONLY));
//					ag.setIdPhoto(idphoto);
//					imageHandler.writeImage(msisdn, Constant.TYPE_PHOTO_ID_ONLY, idphoto.replace(" ", "+"));
//				}
//				if(!Utils.isNullorEmptyString(idphotouser)){
//					idphotouser = (this.imagePath + "/" + Utils.buildImageFilename(msisdn, Constant.TYPE_PHOTO_ID_AND_PHOTO));
//					ag.setProfilePic(idphotouser);
//					imageHandler.writeImage(msisdn, Constant.TYPE_PHOTO_ID_AND_PHOTO, idphotouser.replace(" ", "+"));
//				}
				
				// Mapping to Utiba ExtraValue
				umarketscws.KeyValuePairMap value = utibaHandler.utibaUserMapping(ag);
				// Register or Update Data
				Response userFromUtiba = utibaHandler.getAgentByReferenceRequest(msisdn, apiUser);
				if (userFromUtiba.getStatus() != Constant.RC_AGENT_NOTFOUND) {
					String email = userFromUtiba.getAgentData().getEmail();
					// response =
					// utibaHandler.updateByAgent(authResponse.getSessionId(),
					// msisdn,
					// authResponse.getInitiator(), userFromUtiba.getName(),
					// msisdn, email, value);
					response = utibaHandler.updateByAgent(dataManager.getGlobalSessionId(), msisdn,
							authResponse.getInitiator(), userFromUtiba.getName(), msisdn, email, value, apiUser);
				} else {
					response = utibaHandler.simpleReg(msisdn, msisdn, null, apiUser);
					if (response.getStatus() == Constant.RC_SUCCESS) {
						response = utibaHandler.updateByAgent(dataManager.getGlobalSessionId(), msisdn,
								authResponse.getInitiator(), userFromUtiba.getName(), msisdn, null, value, apiUser);
					}
				}
				if (metric.compare(keyword.trim().toLowerCase(), ag.getMotherMaidenName().trim().toLowerCase()) > 0.8f
						|| keyword.equalsIgnoreCase(ag.getNo_kk())) {
					// Unmap Data
					if (response.getStatus() == Constant.RC_SUCCESS) {
						if (!Utils.isNullorEmptyString(type)) {
							response = utibaHandler.unmapAgentExtParam(dataManager.getGlobalSessionId(), msisdn,
									nonKycAgentId, idNumber, type, authResponse.getInitiator(), apiUser);
						} else {
							response = utibaHandler.unmapAgentExtParam(dataManager.getGlobalSessionId(), msisdn,
									nonKycAgentId, null, null, authResponse.getInitiator(), apiUser);
						}
						if (response.getStatus() == Constant.RC_SUCCESS) {
							response.setStatus(Constant.RC_SUCCESS);
							response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
							response.setAgentData(ag);

							// Cek For remmitance
							String vaWallet = "rt" + msisdn;
							Response responseRemitt = utibaHandler.getAgentByReferenceRequest(vaWallet, apiUser);
							if (responseRemitt.getStatus() == Constant.RC_SUCCESS) {
								response = utibaHandler.adjustWallet(dataManager.getGlobalSessionId(),
										BigDecimal.valueOf(Long.valueOf(responseRemitt.getBalance())), vaWallet, msisdn,
										"remitanceOverhandler", 1, apiUser);
								if (response.getStatus() == Constant.RC_SUCCESS) {
									utibaHandler.removeAgent(dataManager.getGlobalSessionId(), vaWallet, apiUser);
									remitUserDao.deleteUser(vaWallet);
									sms = smsRemitMoveBalance.replace(Constant.BALANCE_REPLACE_TAG,
											Utils.convertStringtoCurrency(responseRemitt.getBalance()));

									if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
										if (dataStore.getTelcoIdByPrefix(msisdn).getName()
												.equalsIgnoreCase("INDOSAT")) {
											smsClientSender.send(sms, msisdn);
										} else {
											mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
													sms.substring(0, 100) + "...");
										}
									}

									log.info("=======END Remittance wallet overhandler, MSISDN:" + msisdn + "Status:"
											+ dataStore.getErrorMsg(Constant.RC_SUCCESS) + "=======");
								} else {
									log.error("=======END Remittance wallet overhandler, MSISDN:" + msisdn + "ERROR:"
											+ dataStore.getErrorMsg(response.getStatus()) + "=======");
								}
							}
							sms = smsSuccessKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
							// smsClientSender.send(sms, msisdn);
						} else if (response.getStatus() == Constant.RC_DUPLICATE_EXTERNAL_REFERENCE) {
							response.setStatus(Constant.RC_DUPLICATE_EXTERNAL_REFERENCE);
							response.setMsg("KTP Sudah pernah digunakan untuk KYC");
							sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
									"KTP Sudah pernah digunakan untuk KYC");
							// smsClientSender.send(sms, msisdn);
						} else if (response.getStatus() == Constant.RC_UNKNOWN_ERROR) {
							response.setStatus(Constant.RC_UNKNOWN_ERROR);
							response.setMsg("Akun sudah PREMIUM");
							sms = smsFailedKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
									"Akun anda sudah PREMIUM");
							// smsClientSender.send(sms, msisdn);
						}
						// if
						// (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT"))
						// {
						// smsClientSender.send(sms, msisdn);
						// } else {
						// mailHandler.sendFirebaseNotification(null, msisdn,
						// response.getTrxid(), sms,
						// sms.substring(0, 100) + "...");
						// }
					}
				} else {
					response = new Response();
					response.setStatus(Constant.RC_INVALID_USERID);
					response.setMsg("Nama Ibu /Nomor KK tidak sesuai dengan data pada database kami");
					sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
							"Karena Nama Ibu /Nomor KK tidak sesuai dengan data pada database kami");
					if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
						if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
							smsClientSender.send(sms, msisdn);
						} else {
							mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
									sms.substring(0, 100) + "...");
						}
					}
				}
			} else if (response.getStatus() == Constant.RC_EKTP_NOT_VALID) {
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, response.getMsg());
				// smsClientSender.send(sms, msisdn);
			} else if (response.getMsg().contains("Format NIK Salah")) {
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Format Nomor KTP Salah");
				// smsClientSender.send(sms, msisdn);
			} else if (response.getStatus() == Constant.RC_EKTP_NOT_EXISTS) {
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Data KTP Tidak terdaftar di Adminduk");
				// smsClientSender.send(sms, msisdn);
			} else if (response.getStatus() == Constant.RC_EKTP_UNKNOWN_ERROR) {
				sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Problem pada sistem EKTP");
				// smsClientSender.send(sms, msisdn);
			}

			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}

		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
			sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
					.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Karena: " + authResponse.getMsg());
			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}
		}
		return response;
	}

	@Override
	public Response doKYCKTPStrict(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdnTo = null;

		String idNumber = parameter.getKeyValueMap().get(Constant.ID_NUMBER_PARAM);
		String keyword = parameter.getKeyValueMap().get(Constant.KEYWORD_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String type = parameter.getKeyValueMap().get(Constant.TYPE_PARAM);
		String sms = "";

		if (Utils.isNullorEmptyString(idNumber) || Utils.isNullorEmptyString(keyword)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		if (!StringUtils.isNumeric(msisdn)) {
			response.setStatus(Constant.RC_INVALID_MSISDN_FORMAT);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_MSISDN_FORMAT));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		msisdnTo = authResponse.getInitiator();
		ApiUser ap = dataStore.getApiUser(userId);
		
		//BLOCKED DUE TO SUSPECT TRX
		if(kycBloc==1){
			response.setStatus(Constant.RC_UNKNOWN_ERROR);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_UNKNOWN_ERROR));
			sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
					.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "");
			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}
			return response;
		}
		
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			// log.debug(authResponse);
			response = eKTP(parameter);
			if (response.getStatus() == Constant.RC_SUCCESS) {
				StringMetric metric = StringMetrics.damerauLevenshtein();
				AgentData ag = response.getAgentData();
				// Mapping to Utiba ExtraValue
				umarketscws.KeyValuePairMap value = utibaHandler.utibaUserMapping(ag);
				if (metric.compare(keyword.trim().toLowerCase(), ag.getMotherMaidenName().trim().toLowerCase()) > 0.8f
						|| keyword.equalsIgnoreCase(ag.getNo_kk())) {
					// Register or Update Data
					Response userFromUtiba = utibaHandler.getAgentByReferenceRequest(msisdn, ap);
					if (userFromUtiba.getStatus() != Constant.RC_AGENT_NOTFOUND) {
						String email = userFromUtiba.getAgentData().getEmail();
						response = utibaHandler.updateByAgent(dataManager.getGlobalSessionId(), msisdn,
								authResponse.getInitiator(), userFromUtiba.getName(), msisdn, email, value, ap);
					} else {
						response = utibaHandler.simpleReg(msisdn, msisdn, null, ap);
						if (response.getStatus() == Constant.RC_SUCCESS) {
							response = utibaHandler.updateByAgent(dataManager.getGlobalSessionId(), msisdn,
									authResponse.getInitiator(), userFromUtiba.getName(), msisdn, null, value, ap);
						}
					}
					// Unmap Data
					if (response.getStatus() == Constant.RC_SUCCESS) {
						if (!Utils.isNullorEmptyString(type)) {
							response = utibaHandler.unmapAgentExtParam(dataManager.getGlobalSessionId(), msisdn,
									nonKycAgentId, idNumber, type, authResponse.getInitiator(), ap);
						} else {
							response = utibaHandler.unmapAgentExtParam(dataManager.getGlobalSessionId(), msisdn,
									nonKycAgentId, null, null, authResponse.getInitiator(), ap);
						}
						if (response.getStatus() == Constant.RC_SUCCESS) {
							response.setStatus(Constant.RC_SUCCESS);
							response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
							response.setAgentData(ag);

							// Cek For remmitance
							String vaWallet = "rt" + msisdn;
							Response responseRemitt = utibaHandler.getAgentByReferenceRequest(vaWallet, ap);
							if (responseRemitt.getStatus() == Constant.RC_SUCCESS) {
								response = utibaHandler.adjustWallet(dataManager.getGlobalSessionId(),
										BigDecimal.valueOf(Long.valueOf(responseRemitt.getBalance())), vaWallet, msisdn,
										"remitanceOverhandler", 1, ap);
								if (response.getStatus() == Constant.RC_SUCCESS) {
									utibaHandler.removeAgent(dataManager.getGlobalSessionId(), vaWallet, ap);
									remitUserDao.deleteUser(vaWallet);
									sms = smsRemitMoveBalance.replace(Constant.BALANCE_REPLACE_TAG,
											Utils.convertStringtoCurrency(responseRemitt.getBalance()));

									if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
										if (dataStore.getTelcoIdByPrefix(msisdn).getName()
												.equalsIgnoreCase("INDOSAT")) {
											smsClientSender.send(sms, msisdn);
										} else {
											mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
													sms.substring(0, 100) + "...");
										}
									}

									log.info("=======END Remittance wallet overhandler, MSISDN:" + msisdn + "Status:"
											+ dataStore.getErrorMsg(Constant.RC_SUCCESS) + "=======");
								} else {
									log.error("=======END Remittance wallet overhandler, MSISDN:" + msisdn + "ERROR:"
											+ dataStore.getErrorMsg(response.getStatus()) + "=======");
								}
							}
							sms = smsSuccessKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
						} else if (response.getStatus() == Constant.RC_DUPLICATE_EXTERNAL_REFERENCE) {
							response.setStatus(Constant.RC_DUPLICATE_EXTERNAL_REFERENCE);
							response.setMsg("KTP Sudah pernah digunakan untuk KYC");
							sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
									"KTP Sudah pernah digunakan untuk KYC");
						} else if (response.getStatus() == Constant.RC_UNKNOWN_ERROR) {
							response.setStatus(Constant.RC_UNKNOWN_ERROR);
							response.setMsg("Akun sudah PREMIUM");
							sms = smsFailedKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
									"Akun anda sudah PREMIUM");
						}
					}
				} else {
					response = new Response();
					response.setStatus(Constant.RC_INVALID_USERID);
					response.setMsg("Nama Ibu /Nomor KK tidak sesuai dengan data pada database kami");
					sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
							"Karena Nama Ibu /Nomor KK tidak sesuai dengan data pada database kami");
					if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
						if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
							smsClientSender.send(sms, msisdn);
						} else {
							mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
									sms.substring(0, 100) + "...");
						}
					}
				}
			} else if (response.getStatus() == Constant.RC_EKTP_NOT_VALID) {
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, response.getMsg());
				// smsClientSender.send(sms, msisdn);
			} else if (response.getMsg().contains("Format NIK Salah")) {
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Format Nomor KTP Salah");
			} else if (response.getStatus() == Constant.RC_EKTP_NOT_EXISTS) {
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Data KTP Tidak terdaftar di Adminduk");
			} else if (response.getStatus() == Constant.RC_EKTP_UNKNOWN_ERROR) {
				sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Problem pada sistem EKTP");
			}

			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
			sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
					.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Karena: " + authResponse.getMsg());
			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}
		}
		return response;
	}

	@Override
	public Response doKYCKTPByAgent(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		List<MediaType> mediaTypes;
		MultiValueMap<String, String> paramKTP = new LinkedMultiValueMap<String, String>();
		String msisdnTo = null;

		String idNumber = parameter.getKeyValueMap().get(Constant.ID_NUMBER_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String type = parameter.getKeyValueMap().get(Constant.TYPE_PARAM);
		String sms = "";

		if (Utils.isNullorEmptyString(idNumber) || Utils.isNullorEmptyString(msisdn)) {
			// if (Utils.isNullorEmptyString(idNumber) ||
			// Utils.isNullorEmptyString(msisdn)
			// || Utils.isNullorEmptyString(keyword)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		// if (!Utils.validateMSISDN(msisdn)) {
		if (!StringUtils.isNumeric(msisdn)) {
			response.setStatus(Constant.RC_INVALID_MSISDN_FORMAT);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_MSISDN_FORMAT));
			return response;
		}
		
		//BLOCKED DUE TO SUSPECT TRX
		if(kycBloc==1){
			response.setStatus(Constant.RC_UNKNOWN_ERROR);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_UNKNOWN_ERROR));
			sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
					.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "");
			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}
			return response;
		}
		
		// If not INDOSAT number, cannot KYC & register
		ApiUser apiUser = dataStore.getApiUser(userId);
		// Prefix prefix = dataStore.getTelcoIdByPrefix(msisdn);
		// if (!prefix.getName().equalsIgnoreCase("INDOSAT") || msisdn.length()
		// > 15) {
		// response.setStatus(Constant.RC_INVALID_PARAMETERS);
		// response.setMsg("Nomor yang digunakan tidak valid");
		// sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
		// "Nomor yang digunakan tidak valid");
		// if
		// (Utils.validateMSISDN(Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0])) {
		// // smsClientSender.send(
		// // sms.replace("Proses konfirmasi data gagal", "Proses
		// // konfirmasi data " + msisdn + " gagal"),
		// // Utils.getUsernameAndPassFromSignature(signature,
		// // apiUser.getPassKey())[0]);
		// if (dataStore
		// .getTelcoIdByPrefix(Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0])
		// .getName().equalsIgnoreCase("INDOSAT")) {
		// smsClientSender.send(
		// sms.replace("Proses konfirmasi data gagal", "Proses konfirmasi data "
		// + msisdn + " gagal"),
		// Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0]);
		// } else {
		// mailHandler
		// .sendFirebaseNotification(null,
		// Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0], response
		// .getTrxid(),
		// sms.replace("Proses konfirmasi data gagal",
		// "Proses konfirmasi data " + msisdn + " gagal"),
		// sms.replace("Proses konfirmasi data gagal",
		// "Proses konfirmasi data " + msisdn + " gagal").substring(0, 100) +
		// "...");
		// }
		// }
		// return response;
		// }

		// Get Signature Master for Unmap
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		msisdnTo = authResponse.getInitiator();
		ApiUser ap = dataStore.getApiUser(userId);
		log.debug(authResponse);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			log.debug(authResponse);
			response = eKTP(parameter);
			if (response.getStatus() == Constant.RC_SUCCESS) {
				StringMetric metric = StringMetrics.damerauLevenshtein();
				AgentData ag = response.getAgentData();
				// Mapping to Utiba ExtraValue
				umarketscws.KeyValuePairMap value = utibaHandler.utibaUserMapping(ag);
				// Update or Register
				Response userFromUtiba = utibaHandler.getAgentByReferenceRequest(msisdn, ap);
				if (userFromUtiba.getStatus() != Constant.RC_AGENT_NOTFOUND) {
					String email = userFromUtiba.getAgentData().getEmail();
					response = utibaHandler.updateByAgent(dataManager.getGlobalSessionId(), msisdn,
							authResponse.getInitiator(), userFromUtiba.getName(), msisdn, email, value, ap);
				} else {
					response = utibaHandler.simpleReg(msisdn, msisdn, null, ap);
					if (response.getStatus() == Constant.RC_SUCCESS) {
						response = utibaHandler.updateByAgent(dataManager.getGlobalSessionId(), msisdn,
								authResponse.getInitiator(), userFromUtiba.getName(), msisdn, null, value, ap);
					}
				}
				// UnMap, Map, JoinParent
				if (response.getStatus() == Constant.RC_SUCCESS) {
					if (!Utils.isNullorEmptyString(type)) {
						response = utibaHandler.unmapAgentExtParam(dataManager.getGlobalSessionId(), msisdn,
								nonKycAgentId, idNumber, type, authResponse.getInitiator(), ap);
					} else {
						response = utibaHandler.unmapAgentExtParam(dataManager.getGlobalSessionId(), msisdn,
								nonKycAgentId, null, null, authResponse.getInitiator(), ap);
					}
					if (response.getStatus() == Constant.RC_SUCCESS) {
						// Mapping user to special subs
						// checkEligibleJoinParent
						Response initDetails = this.balanceCheck(userId, authResponse.getInitiator());
						if (!Utils.isNullorEmptyString(initDetails.getAgentData().getWalletGrouping())) {
							String walletGrouping = initDetails.getAgentData().getWalletGrouping().replace("[", "")
									.replace("]", "");
							if (walletGrouping.split(",").length > 0) {
								for (String group : walletGrouping.split(",")) {
									if (eligibleJoinParent.contains(group.trim())) {
										response = utibaHandler.mapAgent(dataManager.getGlobalSessionId(), msisdn,
												Long.valueOf(spcSubsAgentId), authResponse.getInitiator(), ap);
										response = utibaHandler.joinParent(dataManager.getGlobalSessionId(),
												authResponse.getInitiator(), msisdn, ap);
									}
								}
							}
						}
						if (response.getStatus() == Constant.RC_UNKNOWN_ERROR) {
							response.setStatus(Constant.RC_UNKNOWN_ERROR);
							response.setMsg("Terjadi kesalahan");
							sms = smsFailedKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Terjadi kesalahan");
							if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
								if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
									smsClientSender.send(sms, msisdn);
								} else {
									mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
											sms.substring(0, 100) + "...");
								}
							}
							// smsClientSender.send(sms, msisdn);
							if (!Utils.isNullorEmptyString(
									dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
								if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
										.equalsIgnoreCase("INDOSAT")) {
									smsClientSender.send(
											sms.replace("Proses konfirmasi data gagal",
													"Proses konfirmasi data " + msisdn + " gagal"),
											authResponse.getInitiator());
								} else {
									mailHandler
											.sendFirebaseNotification(null, authResponse.getInitiator(),
													response.getTrxid(),
													sms.replace("Proses konfirmasi data gagal",
															"Proses konfirmasi data " + msisdn + " gagal"),
													sms.replace("Proses konfirmasi data gagal",
															"Proses konfirmasi data " + msisdn + " gagal")
															.substring(0, 100) + "...");
								}
							}
							// smsClientSender.send(
							// sms.replace("Proses konfirmasi data gagal",
							// "Proses konfirmasi data " + msisdn + " gagal"),
							// authResponse.getInitiator());
						} else {
							response.setStatus(Constant.RC_SUCCESS);
							sms = smsSuccessKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
							// smsClientSender.send(sms, msisdn);
							// smsClientSender.send(sms.replace("anda sudah",
							// "sudah"), authResponse.getInitiator());
							if (!Utils.isNullorEmptyString(
									dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
								if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
										.equalsIgnoreCase("INDOSAT")) {
									smsClientSender.send(sms.replace("anda sudah", "sudah"),
											authResponse.getInitiator());
								} else {
									mailHandler.sendFirebaseNotification(null, authResponse.getInitiator(),
											response.getTrxid(), sms.replace("anda sudah", "sudah"),
											sms.replace("anda sudah", "sudah").substring(0, 100) + "...");
								}
							}
						}
					} else if (response.getStatus() == Constant.RC_DUPLICATE_EXTERNAL_REFERENCE) {
						response.setStatus(Constant.RC_DUPLICATE_EXTERNAL_REFERENCE);
						response.setMsg("KTP Sudah pernah digunakan untuk KYC");
						sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
								"KTP Sudah pernah digunakan untuk KYC");
						if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
							if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
								smsClientSender.send(sms, msisdn);
							} else {
								mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
										sms.substring(0, 100) + "...");
							}
						}
						// smsClientSender.send(sms, msisdn);
						if (!Utils.isNullorEmptyString(
								dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
							if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
									.equalsIgnoreCase("INDOSAT")) {
								smsClientSender.send(
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal"),
										authResponse.getInitiator());
							} else {
								mailHandler
										.sendFirebaseNotification(null, authResponse.getInitiator(),
												response.getTrxid(), sms
														.replace("Proses konfirmasi data gagal",
																"Proses konfirmasi data " + msisdn + " gagal"),
												sms.replace("Proses konfirmasi data gagal",
														"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100)
														+ "...");
							}
						}
						// smsClientSender.send(sms.replace("Proses konfirmasi
						// data gagal",
						// "Proses konfirmasi data " + msisdn + " gagal"),
						// authResponse.getInitiator());
					}
					if (response.getStatus() == Constant.RC_UNKNOWN_ERROR) {
						response.setStatus(Constant.RC_UNKNOWN_ERROR);
						response.setMsg("Akun sudah PREMIUM");
						sms = smsFailedKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Akun anda sudah PREMIUM");
						if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
							if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
								smsClientSender.send(sms, msisdn);
							} else {
								mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
										sms.substring(0, 100) + "...");
							}
						}
						// smsClientSender.send(sms, msisdn);
						if (!Utils.isNullorEmptyString(
								dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
							if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
									.equalsIgnoreCase("INDOSAT")) {
								smsClientSender.send(
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal"),
										authResponse.getInitiator());
							} else {
								mailHandler
										.sendFirebaseNotification(null, authResponse.getInitiator(),
												response.getTrxid(), sms
														.replace("Proses konfirmasi data gagal",
																"Proses konfirmasi data " + msisdn + " gagal"),
												sms.replace("Proses konfirmasi data gagal",
														"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100)
														+ "...");
							}
						}
						// smsClientSender.send(sms.replace("Proses konfirmasi
						// data gagal",
						// "Proses konfirmasi data " + msisdn + " gagal"),
						// authResponse.getInitiator());
					}
				}
			} else if (response.getMsg().contains("Format NIK Salah")) {
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Format Nomor KTP Salah");
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
					if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms, msisdn);
					} else {
						mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
								sms.substring(0, 100) + "...");
					}
				}
				// smsClientSender.send(sms, msisdn);
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
					if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
							.equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms.replace("Proses konfirmasi data gagal",
								"Proses konfirmasi data " + msisdn + " gagal"), authResponse.getInitiator());
					} else {
						mailHandler
								.sendFirebaseNotification(null,
										authResponse.getInitiator(), response
												.getTrxid(),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal"),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100)
												+ "...");
					}
				}
				// smsClientSender.send(
				// sms.replace("Proses konfirmasi data gagal", "Proses
				// konfirmasi data " + msisdn + " gagal"),
				// authResponse.getInitiator());
			} else if (response.getStatus() == Constant.RC_EKTP_NOT_VALID
					|| response.getMsg().equalsIgnoreCase("Format Nomor KTP Salah")) {
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, response.getMsg());
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
					if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms, msisdn);
					} else {
						mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
								sms.substring(0, 100) + "...");
					}
				}
				// smsClientSender.send(sms, msisdn);
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
					if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
							.equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms.replace("Proses konfirmasi data gagal",
								"Proses konfirmasi data " + msisdn + " gagal"), authResponse.getInitiator());
					} else {
						mailHandler
								.sendFirebaseNotification(null,
										authResponse.getInitiator(), response
												.getTrxid(),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal"),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100)
												+ "...");
					}
				}
				// smsClientSender.send(
				// sms.replace("Proses konfirmasi data gagal", "Proses
				// konfirmasi data " + msisdn + " gagal"),
				// authResponse.getInitiator());
			} else if (response.getStatus() == Constant.RC_EKTP_NOT_EXISTS) {
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Data KTP Tidak terdaftar di Adminduk");
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
					if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms, msisdn);
					} else {
						mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
								sms.substring(0, 100) + "...");
					}
				}
				// smsClientSender.send(sms, msisdn);
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
					if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
							.equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms.replace("Proses konfirmasi data gagal",
								"Proses konfirmasi data " + msisdn + " gagal"), authResponse.getInitiator());
					} else {
						mailHandler
								.sendFirebaseNotification(null,
										authResponse.getInitiator(), response
												.getTrxid(),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal"),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100)
												+ "...");
					}
				}
				// smsClientSender.send(
				// sms.replace("Proses konfirmasi data gagal", "Proses
				// konfirmasi data " + msisdn + " gagal"),
				// authResponse.getInitiator());
			} else if (response.getStatus() == Constant.RC_EKTP_UNKNOWN_ERROR) {
				sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Problem pada sistem EKTP");
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
					if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms, msisdn);
					} else {
						mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
								sms.substring(0, 100) + "...");
					}
				}
				// smsClientSender.send(sms, msisdn);
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
					if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
							.equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms.replace("Proses konfirmasi data gagal",
								"Proses konfirmasi data " + msisdn + " gagal"), authResponse.getInitiator());
					} else {
						mailHandler
								.sendFirebaseNotification(null,
										authResponse.getInitiator(), response
												.getTrxid(),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal"),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100)
												+ "...");
					}
				}
				// smsClientSender.send(
				// sms.replace("Proses konfirmasi data gagal", "Proses
				// konfirmasi data " + msisdn + " gagal"),
				// authResponse.getInitiator());
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
			sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
					.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Karena: " + authResponse.getMsg());
			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}
			// smsClientSender.send(sms, msisdn);
			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
				if (dataStore
						.getTelcoIdByPrefix(Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey())[0])
						.getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(
							sms.replace("Proses konfirmasi data gagal", "Proses konfirmasi data " + msisdn + " gagal"),
							Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey())[0]);
				} else {
					mailHandler
							.sendFirebaseNotification(null,
									Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey())[0], response
											.getTrxid(),
									sms.replace("Proses konfirmasi data gagal",
											"Proses konfirmasi data " + msisdn + " gagal"),
									sms.replace("Proses konfirmasi data gagal",
											"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100) + "...");
				}
			}
			// smsClientSender.send(
			// sms.replace("Proses konfirmasi data gagal", "Proses konfirmasi
			// data " + msisdn + " gagal"),
			// Utils.getUsernameAndPassFromSignature(signature,
			// apiUser.getPassKey())[0]);
		}
		return response;
	}

	@Override
	public Response doKYCKTPByAgentWithReference(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdnTo = null;

		String idNumber = parameter.getKeyValueMap().get(Constant.ID_NUMBER_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String type = parameter.getKeyValueMap().get(Constant.TYPE_PARAM);
		String keyword = parameter.getKeyValueMap().get(Constant.KEYWORD_PARAM);
		String sms = "";

		if (Utils.isNullorEmptyString(idNumber) || Utils.isNullorEmptyString(msisdn)
				|| Utils.isNullorEmptyString(keyword)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		// if (!Utils.validateMSISDN(msisdn)) {
		if (!StringUtils.isNumeric(msisdn)) {
			response.setStatus(Constant.RC_INVALID_MSISDN_FORMAT);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_MSISDN_FORMAT));
			return response;
		}

		//BLOCKED DUE TO SUSPECT TRX
		if(kycBloc==1){
			response.setStatus(Constant.RC_UNKNOWN_ERROR);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_UNKNOWN_ERROR));
			sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
					.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "");
			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}
			return response;
		}
		
		// If not INDOSAT number, cannot KYC & register
		ApiUser apiUser = dataStore.getApiUser(userId);
		// Prefix prefix = dataStore.getTelcoIdByPrefix(msisdn);
		// if (prefix != null) {
		// if (!prefix.getName().equalsIgnoreCase("INDOSAT") || msisdn.length()
		// > 15) {
		// response.setStatus(Constant.RC_INVALID_PARAMETERS);
		// response.setMsg("Nomor yang digunakan tidak valid");
		// sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
		// "Nomor yang digunakan tidak valid");
		// if
		// (Utils.validateMSISDN(Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0])) {
		// // smsClientSender.send(
		// // sms.replace("Proses konfirmasi data gagal", "Proses
		// // konfirmasi data " + msisdn + " gagal"),
		// // Utils.getUsernameAndPassFromSignature(signature,
		// // apiUser.getPassKey())[0]);
		// if (dataStore
		// .getTelcoIdByPrefix(Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0])
		// .getName().equalsIgnoreCase("INDOSAT")) {
		// smsClientSender.send(
		// sms.replace("Proses konfirmasi data gagal", "Proses konfirmasi data "
		// + msisdn + " gagal"),
		// Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0]);
		// } else {
		// mailHandler
		// .sendFirebaseNotification(null,
		// Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0], response
		// .getTrxid(),
		// sms.replace("Proses konfirmasi data gagal",
		// "Proses konfirmasi data " + msisdn + " gagal"),
		// sms.replace("Proses konfirmasi data gagal",
		// "Proses konfirmasi data " + msisdn + " gagal").substring(0, 100) +
		// "...");
		// }
		// }
		// return response;
		// }
		// } else {
		// response.setStatus(Constant.RC_INVALID_PARAMETERS);
		// response.setMsg("Nomor yang digunakan tidak valid");
		// sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
		// "Nomor yang digunakan tidak valid");
		//// if
		// (Utils.validateMSISDN(Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0]))
		//// smsClientSender.send(
		//// sms.replace("Proses konfirmasi data gagal", "Proses konfirmasi data
		// " + msisdn + " gagal"),
		//// Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0]);
		// if
		// (Utils.validateMSISDN(Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0])) {
		// if (dataStore
		// .getTelcoIdByPrefix(Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0])
		// .getName().equalsIgnoreCase("INDOSAT")) {
		// smsClientSender.send(
		// sms.replace("Proses konfirmasi data gagal", "Proses konfirmasi data "
		// + msisdn + " gagal"),
		// Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0]);
		// } else {
		// mailHandler
		// .sendFirebaseNotification(null,
		// Utils.getUsernameAndPassFromSignature(signature,
		// apiUser.getPassKey())[0], response
		// .getTrxid(),
		// sms.replace("Proses konfirmasi data gagal",
		// "Proses konfirmasi data " + msisdn + " gagal"),
		// sms.replace("Proses konfirmasi data gagal",
		// "Proses konfirmasi data " + msisdn + " gagal").substring(0, 100) +
		// "...");
		// }
		// }
		// return response;
		// }

		if (idNumber.length() < 5) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg("Nomor KTP yang digunakan tidak valid");
			sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Nomor KTP yang digunakan tidak valid");
			// if
			// (Utils.validateMSISDN(Utils.getUsernameAndPassFromSignature(signature,
			// apiUser.getPassKey())[0])) {
			if (StringUtils.isNumeric(Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey())[0])) {
				if (dataStore
						.getTelcoIdByPrefix(Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey())[0])
						.getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(
							sms.replace("Proses konfirmasi data gagal", "Proses konfirmasi data " + msisdn + " gagal"),
							Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey())[0]);
				} else {
					mailHandler
							.sendFirebaseNotification(null,
									Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey())[0], response
											.getTrxid(),
									sms.replace("Proses konfirmasi data gagal",
											"Proses konfirmasi data " + msisdn + " gagal"),
									sms.replace("Proses konfirmasi data gagal",
											"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100) + "...");
				}
			}
			return response;
		}

		// Get Signature Master for Unmap
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		msisdnTo = authResponse.getInitiator();
		log.debug(authResponse);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			log.debug(authResponse);
			response = eKTP(parameter);
			if (response.getStatus() == Constant.RC_SUCCESS) {
				StringMetric metric = StringMetrics.damerauLevenshtein();
				AgentData ag = response.getAgentData();
				if (metric.compare(keyword.trim().toLowerCase(), ag.getMotherMaidenName().trim().toLowerCase()) > 0.8f
						|| keyword.equalsIgnoreCase(ag.getNo_kk())) {
					// Mapping to Utiba ExtraValue
					umarketscws.KeyValuePairMap value = utibaHandler.utibaUserMapping(ag);
					// Update or Register
					Response userFromUtiba = utibaHandler.getAgentByReferenceRequest(msisdn, apiUser);
					if (userFromUtiba.getStatus() != Constant.RC_AGENT_NOTFOUND) {
						String email = userFromUtiba.getAgentData().getEmail();
						response = utibaHandler.updateByAgent(dataManager.getGlobalSessionId(), msisdn,
								authResponse.getInitiator(), userFromUtiba.getName(), msisdn, email, value, apiUser);
					} else {
						response = utibaHandler.simpleReg(msisdn, msisdn, null, apiUser);
						if (response.getStatus() == Constant.RC_SUCCESS) {
							response = utibaHandler.updateByAgent(dataManager.getGlobalSessionId(), msisdn,
									authResponse.getInitiator(), userFromUtiba.getName(), msisdn, null, value, apiUser);
						}
					}
					// UnMap, Map, JoinParent
					if (response.getStatus() == Constant.RC_SUCCESS) {
						if (!Utils.isNullorEmptyString(type)) {
							response = utibaHandler.unmapAgentExtParam(dataManager.getGlobalSessionId(), msisdn,
									nonKycAgentId, idNumber, type, authResponse.getInitiator(), apiUser);
						} else {
							response = utibaHandler.unmapAgentExtParam(dataManager.getGlobalSessionId(), msisdn,
									nonKycAgentId, null, null, authResponse.getInitiator(), apiUser);
						}
						if (response.getStatus() == Constant.RC_SUCCESS) {
							// checkEligibleJoinParent
							Response initDetails = this.balanceCheck(userId, authResponse.getInitiator());
							if (!Utils.isNullorEmptyString(initDetails.getAgentData().getWalletGrouping())) {
								String walletGrouping = initDetails.getAgentData().getWalletGrouping().replace("[", "")
										.replace("]", "");
								if (walletGrouping.split(",").length > 0) {
									for (String group : walletGrouping.split(",")) {
										if (eligibleJoinParent.contains(group.trim())) {
											response = utibaHandler.mapAgent(dataManager.getGlobalSessionId(), msisdn,
													Long.valueOf(spcSubsAgentId), authResponse.getInitiator(), apiUser);
											response = utibaHandler.joinParent(dataManager.getGlobalSessionId(),
													authResponse.getInitiator(), msisdn, apiUser);
										}
									}
								}
							}

							if (response.getStatus() == Constant.RC_UNKNOWN_ERROR) {
								response.setStatus(Constant.RC_UNKNOWN_ERROR);
								response.setMsg("Terjadi kesalahan");
								sms = smsFailedKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Terjadi kesalahan");
								if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
									if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
										smsClientSender.send(sms, msisdn);
									} else {
										mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
												sms.substring(0, 100) + "...");
									}
								}
								// smsClientSender.send(sms, msisdn);
								if (!Utils.isNullorEmptyString(
										dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
									if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
											.equalsIgnoreCase("INDOSAT")) {
										smsClientSender.send(
												sms.replace("Proses konfirmasi data gagal",
														"Proses konfirmasi data " + msisdn + " gagal"),
												authResponse.getInitiator());
									} else {
										mailHandler
												.sendFirebaseNotification(null, authResponse.getInitiator(),
														response.getTrxid(),
														sms.replace("Proses konfirmasi data gagal",
																"Proses konfirmasi data " + msisdn + " gagal"),
														sms.replace("Proses konfirmasi data gagal",
																"Proses konfirmasi data " + msisdn + " gagal")
																.substring(0, 100) + "...");
									}
								}
								// smsClientSender.send(
								// sms.replace("Proses konfirmasi data gagal",
								// "Proses konfirmasi data " + msisdn + "
								// gagal"),
								// authResponse.getInitiator());
							} else {
								response.setStatus(Constant.RC_SUCCESS);
								sms = smsSuccessKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
								// smsClientSender.send(sms, msisdn);
								// smsClientSender.send(sms.replace("anda
								// sudah", "sudah"),
								// authResponse.getInitiator());
								if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
									if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
											.equalsIgnoreCase("INDOSAT")) {
										smsClientSender.send(sms.replace("anda sudah", "sudah"),
												authResponse.getInitiator());
									} else {
										mailHandler.sendFirebaseNotification(null, authResponse.getInitiator(),
												response.getTrxid(), sms.replace("anda sudah", "sudah"),
												sms.replace("anda sudah", "sudah").substring(0, 100) + "...");
									}
								}
							}
							response.setStatus(Constant.RC_DUPLICATE_EXTERNAL_REFERENCE);
							response.setMsg("KTP Sudah pernah digunakan untuk KYC");
							sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
									"KTP Sudah pernah digunakan untuk KYC");
							if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
								if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
									smsClientSender.send(sms, msisdn);
								} else {
									mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
											sms.substring(0, 100) + "...");
								}
							}
							// smsClientSender.send(sms, msisdn);
							if (!Utils.isNullorEmptyString(
									dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
								if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
										.equalsIgnoreCase("INDOSAT")) {
									smsClientSender.send(
											sms.replace("Proses konfirmasi data gagal",
													"Proses konfirmasi data " + msisdn + " gagal"),
											authResponse.getInitiator());
								} else {
									mailHandler
											.sendFirebaseNotification(null, authResponse.getInitiator(),
													response.getTrxid(),
													sms.replace("Proses konfirmasi data gagal",
															"Proses konfirmasi data " + msisdn + " gagal"),
													sms.replace("Proses konfirmasi data gagal",
															"Proses konfirmasi data " + msisdn + " gagal")
															.substring(0, 100) + "...");
								}
							}
							// smsClientSender.send(sms.replace("Proses
							// konfirmasi data gagal",
							// "Proses konfirmasi data " + msisdn + " gagal"),
							// authResponse.getInitiator());
						}
						if (response.getStatus() == Constant.RC_UNKNOWN_ERROR) {
							response.setStatus(Constant.RC_UNKNOWN_ERROR);
							response.setMsg("Akun sudah PREMIUM");
							sms = smsFailedKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
									"Akun anda sudah PREMIUM");
							if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
								if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
									smsClientSender.send(sms, msisdn);
								} else {
									mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
											sms.substring(0, 100) + "...");
								}
							}
							// smsClientSender.send(sms, msisdn);
							if (!Utils.isNullorEmptyString(
									dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
								if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
										.equalsIgnoreCase("INDOSAT")) {
									smsClientSender.send(
											sms.replace("Proses konfirmasi data gagal",
													"Proses konfirmasi data " + msisdn + " gagal"),
											authResponse.getInitiator());
								} else {
									mailHandler
											.sendFirebaseNotification(null, authResponse.getInitiator(),
													response.getTrxid(),
													sms.replace("Proses konfirmasi data gagal",
															"Proses konfirmasi data " + msisdn + " gagal"),
													sms.replace("Proses konfirmasi data gagal",
															"Proses konfirmasi data " + msisdn + " gagal")
															.substring(0, 100) + "...");
								}
							}
							// smsClientSender.send(sms.replace("Proses
							// konfirmasi data gagal",
							// "Proses konfirmasi data " + msisdn + " gagal"),
							// authResponse.getInitiator());
						}
					}
				} else {
					response = new Response();
					response.setStatus(Constant.RC_INVALID_USERID);
					response.setMsg("Nama Ibu /Nomor KK tidak sesuai dengan data pada database kami");
					sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
							"Karena Nama Ibu /Nomor KK tidak sesuai dengan data pada database kami");
					if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
						if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
							smsClientSender.send(sms, msisdn);
						} else {
							mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
									sms.substring(0, 100) + "...");
						}
					}
					// smsClientSender.send(sms, msisdn);
					if (!Utils
							.isNullorEmptyString(dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
						if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
								.equalsIgnoreCase("INDOSAT")) {
							smsClientSender.send(
									sms.replace("Proses konfirmasi data gagal",
											"Proses konfirmasi data " + msisdn + " gagal"),
									authResponse.getInitiator());
						} else {
							mailHandler
									.sendFirebaseNotification(null, authResponse.getInitiator(),
											response.getTrxid(), sms
													.replace("Proses konfirmasi data gagal",
															"Proses konfirmasi data " + msisdn + " gagal"),
											sms.replace("Proses konfirmasi data gagal",
													"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100)
													+ "...");
						}
					}
				}
			} else if (response.getMsg().contains("Format NIK Salah")) {
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Format Nomor KTP Salah");
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
					if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms, msisdn);
					} else {
						mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
								sms.substring(0, 100) + "...");
					}
				}
				// smsClientSender.send(sms, msisdn);
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
					if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
							.equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms.replace("Proses konfirmasi data gagal",
								"Proses konfirmasi data " + msisdn + " gagal"), authResponse.getInitiator());
					} else {
						mailHandler
								.sendFirebaseNotification(null,
										authResponse.getInitiator(), response
												.getTrxid(),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal"),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100)
												+ "...");
					}
				}
				// smsClientSender.send(
				// sms.replace("Proses konfirmasi data gagal", "Proses
				// konfirmasi data " + msisdn + " gagal"),
				// authResponse.getInitiator());

			} else if (response.getStatus() == Constant.RC_EKTP_NOT_VALID
					|| response.getMsg().equalsIgnoreCase("Format Nomor KTP Salah")) {
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, response.getMsg());
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
					if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms, msisdn);
					} else {
						mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
								sms.substring(0, 100) + "...");
					}
				}
				// smsClientSender.send(sms, msisdn);
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
					if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
							.equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms.replace("Proses konfirmasi data gagal",
								"Proses konfirmasi data " + msisdn + " gagal"), authResponse.getInitiator());
					} else {
						mailHandler
								.sendFirebaseNotification(null,
										authResponse.getInitiator(), response
												.getTrxid(),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal"),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100)
												+ "...");
					}
				}
				// smsClientSender.send(
				// sms.replace("Proses konfirmasi data gagal", "Proses
				// konfirmasi data " + msisdn + " gagal"),
				// authResponse.getInitiator());
			} else if (response.getStatus() == Constant.RC_EKTP_NOT_EXISTS) {
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Data KTP Tidak terdaftar di Adminduk");
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
					if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms, msisdn);
					} else {
						mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
								sms.substring(0, 100) + "...");
					}
				}
				// smsClientSender.send(sms, msisdn);
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
					if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
							.equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms.replace("Proses konfirmasi data gagal",
								"Proses konfirmasi data " + msisdn + " gagal"), authResponse.getInitiator());
					} else {
						mailHandler
								.sendFirebaseNotification(null,
										authResponse.getInitiator(), response
												.getTrxid(),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal"),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100)
												+ "...");
					}
				}
				// smsClientSender.send(
				// sms.replace("Proses konfirmasi data gagal", "Proses
				// konfirmasi data " + msisdn + " gagal"),
				// authResponse.getInitiator());
			} else if (response.getStatus() == Constant.RC_EKTP_UNKNOWN_ERROR) {
				sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Problem pada sistem EKTP");
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
					if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms, msisdn);
					} else {
						mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
								sms.substring(0, 100) + "...");
					}
				}
				// smsClientSender.send(sms, msisdn);
				if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName())) {
					if (dataStore.getTelcoIdByPrefix(authResponse.getInitiator()).getName()
							.equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(sms.replace("Proses konfirmasi data gagal",
								"Proses konfirmasi data " + msisdn + " gagal"), authResponse.getInitiator());
					} else {
						mailHandler
								.sendFirebaseNotification(null,
										authResponse.getInitiator(), response
												.getTrxid(),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal"),
										sms.replace("Proses konfirmasi data gagal",
												"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100)
												+ "...");
					}
				}
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
			sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
					.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Karena: " + authResponse.getMsg());
			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}
			// smsClientSender.send(sms, msisdn);
			if (!Utils.isNullorEmptyString(dataStore
					.getTelcoIdByPrefix(Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey())[0])
					.getName())) {
				if (dataStore
						.getTelcoIdByPrefix(Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey())[0])
						.getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(
							sms.replace("Proses konfirmasi data gagal", "Proses konfirmasi data " + msisdn + " gagal"),
							Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey())[0]);
				} else {
					mailHandler
							.sendFirebaseNotification(null,
									Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey())[0], response
											.getTrxid(),
									sms.replace("Proses konfirmasi data gagal",
											"Proses konfirmasi data " + msisdn + " gagal"),
									sms.replace("Proses konfirmasi data gagal",
											"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100) + "...");
				}
			}
			// smsClientSender.send(
			// sms.replace("Proses konfirmasi data gagal", "Proses konfirmasi
			// data " + msisdn + " gagal"),
			// Utils.getUsernameAndPassFromSignature(signature,
			// apiUser.getPassKey())[0]);
		}
		return response;
	}
	
	//CRS Yoedi
	public boolean isKycToCRSValid(String msisdn, String nik, String motherName){
//		Response res = new Response();
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		parameters.add("userid", ektpmware_userid);
		parameters.add("password", ektpmware_password);
		parameters.add("nik", nik);
		parameters.add("msisdn", msisdn);
		parameters.add("motherMaidenName", motherName);

		
		ResponseEntity<String> responseHttp = httpSender.sendPost(endpoint_crs, parameters, acceptableMediaTypes);
		if (responseHttp == null)
			return false;
		
		String resString = responseHttp.getBody();

		//MAPPING RESULT
		String registeredMotherName = Utils.getXmlTagValue(resString, "<NAMA_LGKP_IBU>");
		if (Utils.isNullorEmptyString(registeredMotherName))
			return false;
		
		return motherName.toUpperCase().compareToIgnoreCase(registeredMotherName.toUpperCase())==0;
	}

	@Override
	public Response doKYCNoKTP(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String name = parameter.getKeyValueMap().get(Constant.NAME_PARAM);
		String idno = parameter.getKeyValueMap().get(Constant.ID_NUMBER_PARAM);
		String idtype = parameter.getKeyValueMap().get(Constant.ID_TYPE_PARAM);
		String countrycode = parameter.getKeyValueMap().get(Constant.COUNTRYCODE_PARAM);
		String gender = parameter.getKeyValueMap().get(Constant.GENDER_PARAM);
		String dob = parameter.getKeyValueMap().get(Constant.DOB_PARAM);
		String pob = parameter.getKeyValueMap().get(Constant.POB_PARAM);
		String address = parameter.getKeyValueMap().get(Constant.ADDRESS_PARAM);
		String city = parameter.getKeyValueMap().get(Constant.CITY_PARAM);
		String province = parameter.getKeyValueMap().get(Constant.PROVINCE_PARAM);
		String email = parameter.getKeyValueMap().get(Constant.EMAIL_PARAM);
		String idphoto = parameter.getKeyValueMap().get(Constant.IDPHOTO_PARAM);
		String idphotouser = parameter.getKeyValueMap().get(Constant.IDPHOTOUSER_PARAM);
		String keyword = parameter.getKeyValueMap().get(Constant.KEYWORD_PARAM);
		// 0:Registered,1:Premium,2:Error,3:non isat kyc
		int status = 0;
		int walletType = 0;
		
		//CRS for isat number only
//		String[] arr = { "62855","62856","62857","62858","62814","62815","62816" };
//	    Set<String> indosatPrefixs = new HashSet<String>(Arrays.asList(arr));
//		
//		msisdn = msisdn.replace("+62", "62");
//		msisdn = msisdn.replaceFirst("^08", "628");
		log.debug("CRS: "+msisdn);
		if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
			log.debug("CRS: Checking");
			if(!isKycToCRSValid(msisdn, idno, keyword)) {
				response.setStatus(Constant.RC_KYC_NOT_VALID);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_KYC_NOT_VALID));
				log.debug(response.toString());
				return response;
			}
		}
		//===

		// enc_code = msisdn|keyword
		// enc_text = msisdn|photoA|photoB
		DESedeEncryption desEnc = new DESedeEncryption(encKey);
		String encCode = desEnc.encrypt(msisdn + "|" + keyword);

		log.info("Request KYC Without KTP:: userId[" + userId + "] signature[XXXXXXX] msisdn[" + msisdn + "]");

		if (Utils.isNullorEmptyString(msisdn) || Utils.isNullorEmptyString(name) || Utils.isNullorEmptyString(idno)
				|| Utils.isNullorEmptyString(idtype) || Utils.isNullorEmptyString(countrycode)
				|| Utils.isNullorEmptyString(keyword)|| Utils.isNullorEmptyString(address)|| Utils.isNullorEmptyString(city)|| Utils.isNullorEmptyString(province)
				|| Utils.isNullorEmptyString(idphoto)|| Utils.isNullorEmptyString(idphotouser)|| Utils.isNullorEmptyString(gender)|| Utils.isNullorEmptyString(dob)|| Utils.isNullorEmptyString(pob)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		if (!StringUtils.isNumeric(msisdn)) {
			response.setStatus(Constant.RC_INVALID_MSISDN_FORMAT);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_MSISDN_FORMAT));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			AuthResponse authResponse = authenticationRequest.validate(userId, signature);
			if (authResponse.getStatus() == Constant.RC_SUCCESS) {
				response = utibaHandler.getAgentByReferenceRequest(msisdn, apiUser);
				
				umarketscws.KeyValuePairMap value = new KeyValuePairMap();
				for (Entry<String, String> e : parameter.getKeyValueMap().entrySet()) {
					umarketscws.KeyValuePair kv0 = new umarketscws.KeyValuePair();
					if (!e.getKey().equalsIgnoreCase(Constant.MSISDN_PARAM)
							&& !e.getKey().equalsIgnoreCase(Constant.NAME_PARAM)
							&& !e.getKey().equalsIgnoreCase(Constant.SIGNATURE_PARAM)
							&& !e.getKey().equalsIgnoreCase(Constant.IDPHOTO_PARAM)
							&& !e.getKey().equalsIgnoreCase(Constant.IDPHOTOUSER_PARAM)) {
						kv0.setKey(e.getKey());
						kv0.setValue(e.getValue());
						value.getKeyValuePair().add(kv0);
					}
				}

				// if not found, register new, unmap, save database
				//Add photo id and store to our server
				if(!Utils.isNullorEmptyString(idphoto)){
					imageHandler.writeImage(msisdn, Constant.TYPE_PHOTO_ID_ONLY, idphoto);
					idphoto = (this.imagePath + "/" + Utils.buildImageFilename(msisdn, Constant.TYPE_PHOTO_ID_ONLY));
//					imageHandler.writeImage(msisdn, Constant.TYPE_PHOTO_ID_ONLY, idphoto.replace(" ", "+"));
					umarketscws.KeyValuePair kvPhoto = new umarketscws.KeyValuePair();
					kvPhoto.setKey(Constant.IDPHOTO_PARAM);
					kvPhoto.setValue(idphoto);
					value.getKeyValuePair().add(kvPhoto);
				}
				if(!Utils.isNullorEmptyString(idphotouser)){
					imageHandler.writeImage(msisdn, Constant.TYPE_PHOTO_ID_AND_PHOTO, idphotouser);
					idphotouser = (this.imagePath + "/" + Utils.buildImageFilename(msisdn, Constant.TYPE_PHOTO_ID_AND_PHOTO));
//					imageHandler.writeImage(msisdn, Constant.TYPE_PHOTO_ID_AND_PHOTO, idphotouser.replace(" ", "+"));
					umarketscws.KeyValuePair kvPhoto = new umarketscws.KeyValuePair();
					kvPhoto.setKey(Constant.IDPHOTOUSER_PARAM);
					kvPhoto.setValue(idphotouser);
					value.getKeyValuePair().add(kvPhoto);
				}
				String encText = desEnc.encrypt(msisdn + "|" + idphoto + "|" + idphotouser);

				if (response.getStatus() == Constant.RC_AGENT_NOTFOUND) {
					response = utibaHandler.registerGeneratePin(name, msisdn, value, apiUser);
				} else if (response.getStatus() == Constant.RC_SUCCESS) {
					response = utibaHandler.updateByAgent(dataManager.getGlobalSessionId(), msisdn, msisdn, name,
							msisdn, null, value, apiUser);
				}

				if (response.getStatus() == Constant.RC_SUCCESS) {
					//non isat should contact CRS for manual KYC
					if (!dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
						response = utibaHandler.mapAgent(dataManager.getGlobalSessionId(), msisdn, nonKycAgentId,
								authResponse.getInitiator(),apiUser);
						//send notif to apps
				    		mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), 
				    				nonisat_kyc_message, nonisat_kyc_message);
				    		response.setStatus(Constant.RC_SUCCESS);
						response.setMsg("Akun anda akan di proses untuk menjadi premium");
						status = 3;
						walletType = Constant.WALLET_TYPE_KYC_REQUEST;
				    }else {
				    	//isat number that pass CRS verification would be unmapped and be premium
				    		response = utibaHandler.unmapAgent(dataManager.getGlobalSessionId(), msisdn, nonKycAgentId,
								apiUser);
						if (response.getStatus() == Constant.RC_SUCCESS) {
							response.setStatus(Constant.RC_SUCCESS);
							response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
							status = 1;
							walletType = Constant.WALLET_TYPE_PREMIUM;
							// sms =
							// smsSuccessKYCKTP.replace(Constant.MSISDN_REPLACE_TAG,
							// msisdn);
						} else if (response.getStatus() == Constant.RC_UNKNOWN_ERROR) {
							response.setStatus(Constant.RC_UNKNOWN_ERROR);
							response.setMsg("Akun sudah PREMIUM");
							status = 1;
							walletType = Constant.WALLET_TYPE_PREMIUM;
						}
				    }
				}

				// if
				// (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName()))
				// {
				// if
				// (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT"))
				// {
				// smsClientSender.send(sms, msisdn);
				// } else {
				// mailHandler.sendFirebaseNotification(null, msisdn,
				// response.getTrxid(), sms,
				// sms.substring(0, 100) + "...");
				// }
				// }

				// Preparing object UserWallet
				UserWallet user = new UserWallet();
				user.setAddress(address);
				user.setCountrycode(countrycode);
				user.setDob(dob);
				user.setGender(gender);
				user.setIdno(idno);
				user.setIdtype(idtype);
				user.setMsisdn(msisdn);
				user.setName(name);
				user.setCreated_time(Utils.getCurrentDate("yyyy-MM-dd hh:mm:ss"));
				user.setEncrypted_code(encCode);
				user.setEncrypted_text(encText);
				user.setUpdated_time(Utils.getCurrentDate("yyyy-MM-dd hh:mm:ss"));
				user.setWallet_type(String.valueOf(walletType));
				user.setStatus(String.valueOf(status));

				List<UserWallet> getUser = userWalletDao.getList(msisdn);
				if (getUser.size() > 0) {
					log.debug("USER Query[" + getUser.toString() + "]");
					userWalletDao.updateStatus(user);
				} else {
					/// userWalletDao.save(user);
					histList.writeUserWalletList(user);
				}
			} else {
				response.setStatus(authResponse.getStatus());
				response.setMsg(authResponse.getMsg());
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	@Override
	public Response doKYCKTPTemp(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdnTo = null;

		String idNumber = parameter.getKeyValueMap().get(Constant.ID_NUMBER_PARAM);
		String keyword = parameter.getKeyValueMap().get(Constant.KEYWORD_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String type = parameter.getKeyValueMap().get(Constant.TYPE_PARAM);
		String idphoto = parameter.getKeyValueMap().get(Constant.IDPHOTO_PARAM);
		String idphotouser = parameter.getKeyValueMap().get(Constant.IDPHOTOUSER_PARAM);
		String sms = "";

		if (Utils.isNullorEmptyString(idNumber) || Utils.isNullorEmptyString(keyword)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		// if (!Utils.validateMSISDN(msisdn)) {
		if (!StringUtils.isNumeric(msisdn)) {
			response.setStatus(Constant.RC_INVALID_MSISDN_FORMAT);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_MSISDN_FORMAT));
			return response;
		}

		//BLOCKED DUE TO SUSPECT TRX
		if(kycBloc==1){
			response.setStatus(Constant.RC_UNKNOWN_ERROR);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_UNKNOWN_ERROR));
			sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
					.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "");
			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}
			return response;
		}
		
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		msisdnTo = authResponse.getInitiator();
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			// log.debug(authResponse);
			if (StringUtils.isNumeric(idNumber) && idNumber.length() == 16) {
				log.debug("Check Kecamatan ["+idNumber.substring(0,6)+"]:"+dataStore.checkKecamatanCode(idNumber.substring(0,6)));
				log.debug("Check tgllahir ["+idNumber.substring(6,12)+"]:"+dataStore.checkTglLahirKTP(idNumber.substring(6,12)));
//				if(!dataStore.checkKecamatanCode(idNumber.substring(0,6))||!dataStore.checkTglLahirKTP(idNumber.substring(6,12))){
				if(!dataStore.checkTglLahirKTP(idNumber.substring(6,12))){
					log.info("["+msisdn+"] Not Valid:Kode Kecamatan atau Tgl Lahir Not Valid");
					response.setStatus(Constant.RC_EKTP_NOT_VALID);
					response.setMsg(dataManager.getErrorMsg(Constant.RC_EKTP_NOT_VALID));
					sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo).replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Format Nomor KTP Salah");
					if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
						if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
							smsClientSender.send(sms, msisdn);
						} else {
							mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
									sms.substring(0, 100) + "...");
						}
					}
					return response;
				}
					
				AgentData ag = response.getAgentData();
				ag.setIdNo(idNumber);
				ag.setMotherMaidenName(keyword);
				if(!Utils.isNullorEmptyString(idphoto)){
//					String idphoto_name = (this.imagePath + "/" + Utils.buildImageFilename(msisdn, Constant.TYPE_PHOTO_ID_ONLY));
					String idphoto_name = (Utils.buildImageFilename(msisdn, Constant.TYPE_PHOTO_ID_ONLY));
					ag.setIdPhoto(idphoto_name);
					imageHandler.writeImage(msisdn, Constant.TYPE_PHOTO_ID_ONLY, idphoto.replace(" ", "+"));
				}
				if(!Utils.isNullorEmptyString(idphotouser)){
//					String idphotouser_name = (this.imagePath + "/" + Utils.buildImageFilename(msisdn, Constant.TYPE_PHOTO_ID_AND_PHOTO));
					String idphotouser_name = (Utils.buildImageFilename(msisdn, Constant.TYPE_PHOTO_ID_AND_PHOTO));
					ag.setProfilePic(idphotouser_name);
					imageHandler.writeImage(msisdn, Constant.TYPE_PHOTO_ID_AND_PHOTO, idphotouser.replace(" ", "+"));
				}
				// Mapping to Utiba ExtraValue
				umarketscws.KeyValuePairMap value = utibaHandler.utibaUserMapping(ag);
				// Register or Update Data
				Response userFromUtiba = utibaHandler.getAgentByReferenceRequest(msisdn, apiUser);
				if (userFromUtiba.getStatus() == Constant.RC_AGENT_NOTFOUND) {
					response = utibaHandler.simpleReg(msisdn, msisdn, null, apiUser);
					if (response.getStatus() == Constant.RC_SUCCESS) {
						response = utibaHandler.updateByAgent(dataManager.getGlobalSessionId(), msisdn,
								authResponse.getInitiator(), userFromUtiba.getName(), msisdn, null, value, apiUser);
					}
				}else{
					response.setStatus(Constant.RC_SUCCESS);
				}

				if (StringUtils.isNumeric(keyword)) {
					if (keyword.length() > 16 || keyword.length() < 1){
					response = new Response();
					response.setStatus(Constant.RC_INVALID_USERID);
					response.setMsg("Nama Ibu /Nomor KK tidak sesuai dengan data pada database kami");
					sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
							"Karena Nama Ibu /Nomor KK tidak sesuai dengan data pada database kami");
						if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
							if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
								smsClientSender.send(sms, msisdn);
							} else {
								mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
										sms.substring(0, 100) + "...");
							}
						}
						return response;
					}
				}

				// Unmap Data
				if (response.getStatus() == Constant.RC_SUCCESS) {
					if (!Utils.isNullorEmptyString(type)) {
						response = utibaHandler.unmapAgentExtParam(dataManager.getGlobalSessionId(), msisdn,
								nonKycAgentId, idNumber, type, authResponse.getInitiator(), apiUser);
					} else {
						response = utibaHandler.unmapAgentExtParam(dataManager.getGlobalSessionId(), msisdn,
								nonKycAgentId, null, null, authResponse.getInitiator(), apiUser);
					}
					if (response.getStatus() == Constant.RC_SUCCESS) {
						response.setStatus(Constant.RC_SUCCESS);
						response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
						response.setAgentData(ag);

						// Updatefor registered user
						if (userFromUtiba.getStatus() != Constant.RC_AGENT_NOTFOUND) {
							String email = userFromUtiba.getAgentData().getEmail();
							response = utibaHandler.updateByAgent(dataManager.getGlobalSessionId(), msisdn,
									authResponse.getInitiator(), userFromUtiba.getName(), msisdn, email, value,
									apiUser);
						}

						// Cek For remmitance
						String vaWallet = "rt" + msisdn;
						Response responseRemitt = utibaHandler.getAgentByReferenceRequest(vaWallet, apiUser);
						if (responseRemitt.getStatus() == Constant.RC_SUCCESS) {
							response = utibaHandler.adjustWallet(dataManager.getGlobalSessionId(),
									BigDecimal.valueOf(Long.valueOf(responseRemitt.getBalance())), vaWallet, msisdn,
									"remitanceOverhandler", 1, apiUser);
							if (response.getStatus() == Constant.RC_SUCCESS) {
								utibaHandler.removeAgent(dataManager.getGlobalSessionId(), vaWallet, apiUser);
								remitUserDao.deleteUser(vaWallet);
								sms = smsRemitMoveBalance.replace(Constant.BALANCE_REPLACE_TAG,
										Utils.convertStringtoCurrency(responseRemitt.getBalance()));

								if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
									if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
										smsClientSender.send(sms, msisdn);
									} else {
										mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
												sms.substring(0, 100) + "...");
									}
								}

								log.info("=======END Remittance wallet overhandler, MSISDN:" + msisdn + "Status:"
										+ dataStore.getErrorMsg(Constant.RC_SUCCESS) + "=======");
							} else {
								log.error("=======END Remittance wallet overhandler, MSISDN:" + msisdn + "ERROR:"
										+ dataStore.getErrorMsg(response.getStatus()) + "=======");
							}
						}
						sms = smsSuccessKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
						userWalletDao.saveEkycWorkaround(new EkycWorkaround(msisdn, idNumber, keyword));
						// smsClientSender.send(sms, msisdn);
					} else if (response.getStatus() == Constant.RC_DUPLICATE_EXTERNAL_REFERENCE) {
						response.setStatus(Constant.RC_DUPLICATE_EXTERNAL_REFERENCE);
						response.setMsg("KTP Sudah pernah digunakan untuk KYC");
						sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
								"KTP Sudah pernah digunakan untuk KYC");
						// smsClientSender.send(sms, msisdn);
					} else if (response.getStatus() == Constant.RC_UNKNOWN_ERROR) {
						response.setStatus(Constant.RC_UNKNOWN_ERROR);
						response.setMsg("Akun sudah PREMIUM");
						sms = smsFailedKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Akun anda sudah PREMIUM");
						// smsClientSender.send(sms, msisdn);
					}
				}

			} else {
				response.setStatus(Constant.RC_EKTP_NOT_VALID);
				response.setMsg(dataManager.getErrorMsg(Constant.RC_EKTP_NOT_VALID));
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Format Nomor KTP Salah");
				// smsClientSender.send(sms, msisdn);
			}

			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}

		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
			sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
					.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Karena: " + authResponse.getMsg());
			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}
		}
		return response;
	}

	@Override
	public Response doKYCKTPStrictTemp(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdnTo = null;

		String idNumber = parameter.getKeyValueMap().get(Constant.ID_NUMBER_PARAM);
		String keyword = parameter.getKeyValueMap().get(Constant.KEYWORD_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String type = parameter.getKeyValueMap().get(Constant.TYPE_PARAM);
		String sms = "";

		if (Utils.isNullorEmptyString(idNumber) || Utils.isNullorEmptyString(keyword)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		// if (!Utils.validateMSISDN(msisdn)) {
		if (!StringUtils.isNumeric(msisdn)) {
			response.setStatus(Constant.RC_INVALID_MSISDN_FORMAT);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_MSISDN_FORMAT));
			return response;
		}

		//BLOCKED DUE TO SUSPECT TRX
		if(kycBloc==1){
			response.setStatus(Constant.RC_UNKNOWN_ERROR);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_UNKNOWN_ERROR));
			sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
					.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "");
			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}
			return response;
		}
		
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		msisdnTo = authResponse.getInitiator();
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			// log.debug(authResponse);
			if (StringUtils.isNumeric(idNumber) && idNumber.length() == 16) {
				
//				if(!dataStore.checkKecamatanCode(idNumber.substring(0,6))||!dataStore.checkTglLahirKTP(idNumber.substring(6,12))){
				if(!dataStore.checkTglLahirKTP(idNumber.substring(6,12))){
					log.info("["+msisdn+"] Not Valid:Kode Kecamatan/Tanggal Lahir Not Valid");
					response.setStatus(Constant.RC_EKTP_NOT_VALID);
					response.setMsg(dataManager.getErrorMsg(Constant.RC_EKTP_NOT_VALID));
					sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo).replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Format Nomor KTP Salah");
					if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
						if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
							smsClientSender.send(sms, msisdn);
						} else {
							mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
									sms.substring(0, 100) + "...");
						}
					}
					return response;
				}
					
				AgentData ag = response.getAgentData();
				ag.setIdNo(idNumber);
				ag.setMotherMaidenName(keyword);
				// Mapping to Utiba ExtraValue
				umarketscws.KeyValuePairMap value = utibaHandler.utibaUserMapping(ag);
				// Register or Update Data
				if (StringUtils.isNumeric(keyword)) {
					if (keyword.length() > 16 || keyword.length() < 1){
					response = new Response();
					response.setStatus(Constant.RC_INVALID_USERID);
					response.setMsg("Nama Ibu /Nomor KK tidak sesuai dengan data pada database kami");
					sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
							"Karena Nama Ibu /Nomor KK tidak sesuai dengan data pada database kami");
						if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
							if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
								smsClientSender.send(sms, msisdn);
							} else {
								mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
										sms.substring(0, 100) + "...");
							}
						}
						return response;
					}
				}
				Response userFromUtiba = utibaHandler.getAgentByReferenceRequest(msisdn, apiUser);
				if (userFromUtiba.getStatus() == Constant.RC_AGENT_NOTFOUND) {
					response = utibaHandler.simpleReg(msisdn, msisdn, null, apiUser);
					if (response.getStatus() == Constant.RC_SUCCESS) {
						response = utibaHandler.updateByAgent(dataManager.getGlobalSessionId(), msisdn,
								authResponse.getInitiator(), userFromUtiba.getName(), msisdn, null, value, apiUser);
					}
				}else{
					response.setStatus(Constant.RC_SUCCESS);
				}
				// Unmap Data
				if (response.getStatus() == Constant.RC_SUCCESS) {
					if (!Utils.isNullorEmptyString(type)) {
						response = utibaHandler.unmapAgentExtParam(dataManager.getGlobalSessionId(), msisdn,
								nonKycAgentId, idNumber, type, authResponse.getInitiator(), apiUser);
					} else {
						response = utibaHandler.unmapAgentExtParam(dataManager.getGlobalSessionId(), msisdn,
								nonKycAgentId, null, null, authResponse.getInitiator(), apiUser);
					}
					if (response.getStatus() == Constant.RC_SUCCESS) {
						response.setStatus(Constant.RC_SUCCESS);
						response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
						response.setAgentData(ag);

						// Updatefor registered user
						if (userFromUtiba.getStatus() != Constant.RC_AGENT_NOTFOUND) {
							String email = userFromUtiba.getAgentData().getEmail();
							response = utibaHandler.updateByAgent(dataManager.getGlobalSessionId(), msisdn,
									authResponse.getInitiator(), userFromUtiba.getName(), msisdn, email, value,
									apiUser);
						}

						// Cek For remmitance
						String vaWallet = "rt" + msisdn;
						Response responseRemitt = utibaHandler.getAgentByReferenceRequest(vaWallet, apiUser);
						if (responseRemitt.getStatus() == Constant.RC_SUCCESS) {
							response = utibaHandler.adjustWallet(dataManager.getGlobalSessionId(),
									BigDecimal.valueOf(Long.valueOf(responseRemitt.getBalance())), vaWallet, msisdn,
									"remitanceOverhandler", 1, apiUser);
							if (response.getStatus() == Constant.RC_SUCCESS) {
								utibaHandler.removeAgent(dataManager.getGlobalSessionId(), vaWallet, apiUser);
								remitUserDao.deleteUser(vaWallet);
								sms = smsRemitMoveBalance.replace(Constant.BALANCE_REPLACE_TAG,
										Utils.convertStringtoCurrency(responseRemitt.getBalance()));

								if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
									if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
										smsClientSender.send(sms, msisdn);
									} else {
										mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
												sms.substring(0, 100) + "...");
									}
								}

								log.info("=======END Remittance wallet overhandler, MSISDN:" + msisdn + "Status:"
										+ dataStore.getErrorMsg(Constant.RC_SUCCESS) + "=======");
							} else {
								log.error("=======END Remittance wallet overhandler, MSISDN:" + msisdn + "ERROR:"
										+ dataStore.getErrorMsg(response.getStatus()) + "=======");
							}
						}
						sms = smsSuccessKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn);
						userWalletDao.saveEkycWorkaround(new EkycWorkaround(msisdn, idNumber, keyword));
						// smsClientSender.send(sms, msisdn);
					} else if (response.getStatus() == Constant.RC_DUPLICATE_EXTERNAL_REFERENCE) {
						response.setStatus(Constant.RC_DUPLICATE_EXTERNAL_REFERENCE);
						response.setMsg("KTP Sudah pernah digunakan untuk KYC");
						sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
								"KTP Sudah pernah digunakan untuk KYC");
						// smsClientSender.send(sms, msisdn);
					} else if (response.getStatus() == Constant.RC_UNKNOWN_ERROR) {
						response.setStatus(Constant.RC_UNKNOWN_ERROR);
						response.setMsg("Akun sudah PREMIUM");
						sms = smsFailedKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Akun anda sudah PREMIUM");
						// smsClientSender.send(sms, msisdn);
					}
				}
			} else {
				response.setStatus(Constant.RC_EKTP_NOT_VALID);
				response.setMsg(dataManager.getErrorMsg(Constant.RC_EKTP_NOT_VALID));
				sms = smsRetryKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdnTo)
						.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Format Nomor KTP Salah");
				// smsClientSender.send(sms, msisdn);
			}

			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}

		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
			sms = smsFailedKYCKTP.replace(Constant.MSISDN_REPLACE_TAG, msisdn)
					.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, "Karena: " + authResponse.getMsg());
			if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(sms, msisdn);
				} else {
					mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), sms,
							sms.substring(0, 100) + "...");
				}
			}
		}
		return response;
	}

	// ============API HOLD BALANCE======================
	public Response hold(String userId, String signature, Parameter parameter) {
		// TODO Auto-generated method stub
		Response response = new Response();
		HashMap<String, String> parameterMap = parameter.getKeyValueMap();
		String amount = parameterMap.get(Constant.AMOUNT_PARAM);
		String extRef = parameterMap.get(Constant.EXT_REF_PARAM);
		String to = parameterMap.get(Constant.TO_PARAM);

		log.info("Request HOLD: userId[" + userId + "] signature[XXXXXXX] amount[" + amount + "]");

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(amount)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					response = utibaHandler.sellHold(sessionId, to, amount, getCredential[0], extRef, "hold_sell", 1,
							apiUser);
				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	public Response holdConfirm(String userId, String signature, Parameter parameter) {
		// TODO Auto-generated method stub
		Response response = new Response();
		HashMap<String, String> parameterMap = parameter.getKeyValueMap();
		String extRefHold = parameterMap.get(Constant.TRANSID_PARAM);
		String amount = parameterMap.get(Constant.AMOUNT_PARAM);
		String extRef = parameterMap.get(Constant.EXT_REF_PARAM);
		String to = parameterMap.get(Constant.TO_PARAM);

		log.info("Request confirmHold: userId[" + userId + "] signature[XXXXXXX] amount[" + amount + "] extRefHold["
				+ extRefHold + "] ");

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(amount)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					response = utibaHandler.reversalNoAmount(sessionId, extRefHold, apiUser);
					response = utibaHandler.sellHold(sessionId, to, amount, getCredential[0], extRef, "hold_confirm", 0,
							apiUser);
					// Ini buat apaan ya?
					// histList.writeHist(apiUser.getId(),
					// apiUser.getChannelType(), Constant.SERVICE_REVERSAL,
					// getCredential[0], transId, response.getTrxid(),
					// response.getStatus());
				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	public Response holdRelease(String userId, String signature, Parameter parameter) {
		// TODO Auto-generated method stub
		Response response = new Response();
		HashMap<String, String> parameterMap = parameter.getKeyValueMap();
		String extRefHold = parameterMap.get(Constant.TRANSID_PARAM);

		log.info("Request releaseHold: userId[" + userId + "] signature[XXXXXXX] extRefHold[" + extRefHold + "] ");

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String sessionId = sessionHandler.getSessionId();
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				int validateSession = sessionHandler.validateSession(sessionId, getCredential[0], getCredential[1]);
				if (validateSession == Constant.RC_SUCCESS) {
					response = utibaHandler.reversalNoAmount(sessionId, extRefHold, apiUser);
				} else {
					response.setStatus(validateSession);
					response.setMsg(dataStore.getErrorMsg(validateSession));
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	// ============One Bill======================
	public Response oneBillReg(String userId, String signature, Parameter parameter) {
		Response response = new Response();

		String extRef = parameter.getKeyValueMap().get(Constant.EXT_REF_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String to = parameter.getKeyValueMap().get(Constant.TO_PARAM);
		String date = parameter.getKeyValueMap().get(Constant.DATE_PARAM);
		String biller = parameter.getKeyValueMap().get(Constant.BILLER_PARAM);
		String amount = parameter.getKeyValueMap().get(Constant.AMOUNT_PARAM);

		if (Utils.isNullorEmptyString(msisdn) || Utils.isNullorEmptyString(to) || Utils.isNullorEmptyString(date)
				|| Utils.isNullorEmptyString(biller) || Utils.isNullorEmptyString(amount)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			if (Float.parseFloat(amount) > 100f) {
				HttpEntity<String> he = null;
				if (!Utils.isNullorEmptyString(extRef)) {
					he = httpClientSender
							.sendGet(appParam.getOnebillURL() + "register?to=" + to + "&date=" + date + "&biller="
									+ biller + "&type=1&msisdn=" + msisdn + "&transid=" + extRef + "&amount=" + amount);
				} else {
					he = httpClientSender.sendGet(appParam.getOnebillURL() + "register?to=" + to + "&date=" + date
							+ "&biller=" + biller + "&type=1&msisdn=" + msisdn + "&amount=" + amount);
				}
				if (he != null) {
					String xmlRes = he.getBody();
					try {
						xmlRes = java.net.URLDecoder.decode(he.getBody(), "UTF-8");
					} catch (UnsupportedEncodingException e) {
						response.setStatus(Constant.RC_SYSTEM_ERROR);
						response.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
					}
					log.debug("Response XML:" + xmlRes);
					int status = Integer.parseInt(Utils.getXmlTagValue(xmlRes, "<status>"));
					if (status == 0) {
						response.setStatus(Constant.RC_SUCCESS);
						response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
					} else if (status == Constant.RC_BILLER_GENERAL_ERROR) {
						response.setStatus(Constant.RC_BILLER_GENERAL_ERROR);
						response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS) + "~Biller Not Found");
					} else if (status == Constant.RC_AGENT_ALREADY_REGISTERED) {
						response.setStatus(Constant.RC_AGENT_ALREADY_REGISTERED);
						response.setMsg(dataManager.getErrorMsg(Constant.RC_AGENT_ALREADY_REGISTERED));
					} else {
					}
				} else {
					response.setStatus(Constant.RC_TIMEOUT);
					response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
				}
			} else {
				response.setStatus(Constant.RC_AMOUNT_TO_SMALL);
				response.setMsg(dataManager.getErrorMsg(Constant.RC_AMOUNT_TO_SMALL));
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	public Response oneBillUpdate(String userId, String signature, Parameter parameter) {
		Response response = new Response();

		String extRef = parameter.getKeyValueMap().get(Constant.EXT_REF_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String to = parameter.getKeyValueMap().get(Constant.TO_PARAM);
		String date = parameter.getKeyValueMap().get(Constant.DATE_PARAM);
		String biller = parameter.getKeyValueMap().get(Constant.BILLER_PARAM);
		String amount = parameter.getKeyValueMap().get(Constant.AMOUNT_PARAM);

		if (Utils.isNullorEmptyString(msisdn) || Utils.isNullorEmptyString(to) || Utils.isNullorEmptyString(date)
				|| Utils.isNullorEmptyString(biller) || Utils.isNullorEmptyString(amount)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			HttpEntity<String> he = null;
			if (!Utils.isNullorEmptyString(extRef)) {
				he = httpClientSender
						.sendGet(appParam.getOnebillURL() + "update?to=" + to + "&date=" + date + "&biller=" + biller
								+ "&type=1&msisdn=" + msisdn + "&transid=" + extRef + "&amount=" + amount);
			} else {
				he = httpClientSender.sendGet(appParam.getOnebillURL() + "update?to=" + to + "&date=" + date
						+ "&biller=" + biller + "&type=1&msisdn=" + msisdn + "&amount=" + amount);
			}
			if (he != null) {
				String xmlRes = he.getBody();
				try {
					xmlRes = java.net.URLDecoder.decode(he.getBody(), "UTF-8");
				} catch (UnsupportedEncodingException e) {
					response.setStatus(Constant.RC_SYSTEM_ERROR);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
				}
				log.debug("Response XML:" + xmlRes);
				int status = Integer.parseInt(Utils.getXmlTagValue(xmlRes, "<status>"));
				if (status == 0) {
					response.setStatus(Constant.RC_SUCCESS);
					response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
				} else if (status == Constant.RC_BILLER_GENERAL_ERROR) {
					response.setStatus(Constant.RC_BILLER_GENERAL_ERROR);
					response.setMsg("Biller Not Found");
				} else {
					response.setStatus(Constant.RC_TRANSACTIN_FAILED);
					response.setMsg("ErrorCode:" + status);
				}
			} else {
				response.setStatus(Constant.RC_TIMEOUT);
				response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	public Response oneBillUnreg(String userId, String signature, Parameter parameter) {
		Response response = new Response();

		String extRef = parameter.getKeyValueMap().get(Constant.EXT_REF_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String to = parameter.getKeyValueMap().get(Constant.TO_PARAM);
		String date = parameter.getKeyValueMap().get(Constant.DATE_PARAM);
		String biller = parameter.getKeyValueMap().get(Constant.BILLER_PARAM);

		if (Utils.isNullorEmptyString(msisdn) || Utils.isNullorEmptyString(to) || Utils.isNullorEmptyString(date)
				|| Utils.isNullorEmptyString(biller)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			HttpEntity<String> he = null;
			if (!Utils.isNullorEmptyString(extRef)) {
				he = httpClientSender.sendGet(appParam.getOnebillURL() + "unreg?to=" + to + "&date=" + date + "&biller="
						+ biller + "&type=1&msisdn=" + msisdn + "&transid=" + extRef);
			} else {
				he = httpClientSender.sendGet(appParam.getOnebillURL() + "unreg?to=" + to + "&date=" + date + "&biller="
						+ biller + "&type=1&msisdn=" + msisdn);
			}
			if (he != null) {
				String xmlRes = he.getBody();
				try {
					xmlRes = java.net.URLDecoder.decode(he.getBody(), "UTF-8");
				} catch (UnsupportedEncodingException e) {
					response.setStatus(Constant.RC_SYSTEM_ERROR);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
				}
				log.debug("Response XML:" + xmlRes);
				int status = Integer.parseInt(Utils.getXmlTagValue(xmlRes, "<status>"));
				if (status == 0) {
					response.setStatus(Constant.RC_SUCCESS);
					response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
				} else if (status == Constant.RC_BILLER_GENERAL_ERROR) {
					response.setStatus(Constant.RC_BILLER_GENERAL_ERROR);
					response.setMsg("Biller Not Found");
				} else if (status == Constant.RC_AGENT_NOTFOUND) {
					response.setStatus(Constant.RC_AGENT_NOTFOUND);
					response.setMsg(dataManager.getErrorMsg(Constant.RC_AGENT_NOTFOUND));
				} else {
					response.setStatus(Constant.RC_TRANSACTIN_FAILED);
					response.setMsg("ErrorCode:" + status);
				}
			} else {
				response.setStatus(Constant.RC_TIMEOUT);
				response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	public Response oneBillGetList(String userId, String signature, Parameter parameter) {
		Response response = new Response();

		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);

		if (Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			HttpEntity<String> he = httpClientSender
					.sendGet(appParam.getOnebillURL() + "get_active_list?msisdn=" + msisdn);
			if (he != null) {
				String xmlRes = he.getBody();
				try {
					xmlRes = java.net.URLDecoder.decode(he.getBody(), "UTF-8");
				} catch (UnsupportedEncodingException e) {
					response.setStatus(Constant.RC_SYSTEM_ERROR);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
				}
				log.debug("Response XML:" + xmlRes);
				log.debug("List Value: " + Utils.getXmlTagValue(xmlRes, "<result>"));
				if (Utils.getXmlTagValue(xmlRes, "<list>") != null) {
					response.setStatus(Constant.RC_SUCCESS);
					response.setMsg(Utils.getXmlTagValue(xmlRes, "<list>"));
				} else {
					response.setStatus(Constant.RC_TRANSACTIN_FAILED);
					response.setMsg("User Not Registered in any Bill Payment");
				}
			} else {
				response.setStatus(Constant.RC_TIMEOUT);
				response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	// ============D-NUSANTARA======================
	public Response nusantaraReg(String userId, String signature, Parameter parameter) {
		Response response = new Response();

		String name = parameter.getKeyValueMap().get(Constant.NAME_PARAM);
		String idType = parameter.getKeyValueMap().get(Constant.ID_TYPE_PARAM);
		String idNumber = parameter.getKeyValueMap().get(Constant.ID_NUMBER_PARAM);
		String address = parameter.getKeyValueMap().get(Constant.ADDRESS_PARAM);
		String dob = parameter.getKeyValueMap().get(Constant.DOB_PARAM);
		String motherMaidenName = parameter.getKeyValueMap().get(Constant.MOTHER_NAME_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String email = parameter.getKeyValueMap().get(Constant.EMAIL_PARAM);
		String type = parameter.getKeyValueMap().get(Constant.TYPE_PARAM);
		String groupid = parameter.getKeyValueMap().get(Constant.GROUPID_PARAM);

		if (Utils.isNullorEmptyString(name) || Utils.isNullorEmptyString(msisdn) || Utils.isNullorEmptyString(idType)
				|| Utils.isNullorEmptyString(idNumber) || Utils.isNullorEmptyString(address)
				|| Utils.isNullorEmptyString(dob) || Utils.isNullorEmptyString(motherMaidenName)
				|| Utils.isNullorEmptyString(type)) {

			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			umarketscws.KeyValuePairMap value = new KeyValuePairMap();
			ApiUser ap = dataStore.getApiUser(userId);

			if (!Utils.isNullorEmptyString(name)) {
				umarketscws.KeyValuePair kvName = new umarketscws.KeyValuePair();
				kvName.setKey(Constant.FIRSTNAME_PARAM);
				kvName.setValue(name);
				value.getKeyValuePair().add(kvName);
			}

			if (!Utils.isNullorEmptyString(idType)) {
				umarketscws.KeyValuePair kvIdType = new umarketscws.KeyValuePair();
				kvIdType.setKey("id_type");
				kvIdType.setValue(idType);
				value.getKeyValuePair().add(kvIdType);
			}

			if (!Utils.isNullorEmptyString(idNumber)) {
				umarketscws.KeyValuePair kvIdNumber = new umarketscws.KeyValuePair();
				kvIdNumber.setKey("id_num");
				kvIdNumber.setValue(idNumber);
				value.getKeyValuePair().add(kvIdNumber);
			}

			if (!Utils.isNullorEmptyString(address)) {
				umarketscws.KeyValuePair kvAddress = new umarketscws.KeyValuePair();
				kvAddress.setKey(Constant.ADDRESS_PARAM);
				kvAddress.setValue(address);
				value.getKeyValuePair().add(kvAddress);
			}

			if (!Utils.isNullorEmptyString(dob)) {
				umarketscws.KeyValuePair kvDOB = new umarketscws.KeyValuePair();
				kvDOB.setKey("birthdate");
				kvDOB.setValue(dob);
				value.getKeyValuePair().add(kvDOB);
			}

			if (!Utils.isNullorEmptyString(motherMaidenName)) {

				umarketscws.KeyValuePair kvMotherMaidenNameDompetku = new umarketscws.KeyValuePair();
				kvMotherMaidenNameDompetku.setKey("mother");
				kvMotherMaidenNameDompetku.setValue(motherMaidenName);
				value.getKeyValuePair().add(kvMotherMaidenNameDompetku);

			}

			if (!Utils.isNullorEmptyString(type)) {
				umarketscws.KeyValuePair kv1 = new umarketscws.KeyValuePair();
				if (type.equalsIgnoreCase("1")) {
					kv1.setKey("suppress_sms");
					kv1.setValue("1");
					value.getKeyValuePair().add(kv1);
				}
			}

			response = utibaHandler.register(msisdn, authResponse.getPassword(), name, msisdn, email, value, ap);
			if (response.getStatus() == Constant.RC_AGENT_ALREADY_REGISTERED) {
				response = utibaHandler.updateByAgent(authResponse.getSessionId(), msisdn, authResponse.getInitiator(),
						name, msisdn, email, value, ap);
			}
			if (response.getStatus() == Constant.RC_SUCCESS) {
				response = utibaHandler.unmapAgent(authResponse.getSessionId(), msisdn, nonKycAgentId, ap);
				if (!Utils.isNullorEmptyString(groupid)) {
					if (response.getStatus() == Constant.RC_SUCCESS
							|| response.getStatus() == Constant.RC_ALREADY_ON_GROUP
							|| response.getStatus() == Constant.RC_UNKNOWN_ERROR) {
						response = utibaHandler.mapAgent(authResponse.getSessionId(), msisdn, Long.valueOf(groupid),
								authResponse.getInitiator(), ap);
					}
				} else {
					response.setStatus(Constant.RC_INVALID_PARAMETERS);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
				}
			} else {
				response.setStatus(authResponse.getStatus());
				response.setMsg(authResponse.getMsg());
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response resetPinNusantara(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String newPin = parameter.getKeyValueMap().get(Constant.PIN_PARAM);
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn) || Utils.isNullorEmptyString(newPin)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			ApiUser ap = dataStore.getApiUser(userId);
			response = utibaHandler.resetPIN(msisdn, newPin, ap);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	public Response nusantaraReferralMapping(String userId, String signature, Parameter parameter) {
		Response response = new Response();

		String name = parameter.getKeyValueMap().get(Constant.NAME_PARAM);
		String group = parameter.getKeyValueMap().get(Constant.GROUPID_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);

		if (Utils.isNullorEmptyString(name) || Utils.isNullorEmptyString(msisdn) || Utils.isNullorEmptyString(group)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		if(msisdn.contains("+")){
			msisdn = "0"+msisdn.substring(3);
		}else if(msisdn.contains("62")){
			msisdn = "0"+msisdn.substring(2);
		}
		
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			ApiUser ap = dataStore.getApiUser(userId);
			response = utibaHandler.mapAgentWithParam(authResponse.getSessionId(), msisdn, Long.valueOf(group),
					parameter.getKeyValueMap(), ap);
			String details = dataStore.getTarget("DBS").getDescription();
			String respMessage = "";
			if (group.equalsIgnoreCase(details.split(";")[2])) {
				if (response.getStatus() == Constant.RC_SUCCESS) {
					respMessage = dataStore.getSMSNotificationByTitle("DBS");
					respMessage = respMessage.replace(Constant.NAME_REPLACE_TAG, "DBS");
					respMessage = respMessage.replace("[%CONTACT_NUMBER%]", details.split(";")[3]);// dbs.getTarget
																									// =
																									// DBS
																									// Phone
																									// Number
					respMessage = respMessage.replace("[%REFID%]", String.valueOf(response.getTrxid()));
					// smsClientSender.send(respMessage,msisdn);
					if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
						if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
							smsClientSender.send(respMessage, msisdn);
						} else {
							mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), respMessage,
									respMessage.substring(0, 100) + "...");
						}
					}
				} else {
					respMessage = smsTextFailed;
					respMessage = respMessage.replace(Constant.DATE_REPLACE_TAG,
							Utils.getCurrentDate("dd/MM/yyyy hh:mm:ss"));
					respMessage = respMessage.replace(Constant.ERROR_MESSAGE_REPLACE_TAG, response.getMsg());
					if (!Utils.isNullorEmptyString(dataStore.getTelcoIdByPrefix(msisdn).getName())) {
						if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
							smsClientSender.send(respMessage, msisdn);
						} else {
							mailHandler.sendFirebaseNotification(null, msisdn, response.getTrxid(), respMessage,
									respMessage.substring(0, 100) + "...");
						}
					}
				}
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	// ============DMT======================
	// public Response dmtMobilePickup(String userId, String signature,
	// Parameter parameter){
	// Response response = new Response();
	// Response responseTemp = new Response();
	//
	// String couponId =
	// parameter.getKeyValueMap().get(Constant.COUPONID_PARAM);
	//
	// if (Utils.isNullorEmptyString(couponId)) {
	// response.setStatus(Constant.RC_INVALID_PARAMETERS);
	// response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
	// return response;
	// }
	//
	// AuthResponse authResponse = authenticationRequest.validate(userId,
	// signature);
	// if (authResponse.getStatus() == Constant.RC_SUCCESS) {
	// response = utibaHandler.getCouponRemitance(authResponse.getSessionId(),
	// couponId);
	// responseTemp =
	// utibaHandler.getAgentByReferenceRequest(authResponse.getInitiator());
	// if(response.getCouponDetails().getRecipientName().equalsIgnoreCase(responseTemp.getName())){
	// HashMap<String, String> parameters = new HashMap<String, String>();
	// parameters.put(Constant.STORE_NAME_PARAM_LITE,
	// authResponse.getInitiator());
	// parameters.put(Constant.RECIPIENT_ID_TYPE_PARAM_LITE,
	// responseTemp.getAgentData().getIdType());
	// parameters.put(Constant.RECIPIENT_ID_NO_PARAM_LITE,
	// responseTemp.getAgentData().getIdNo());
	// parameters.put(Constant.RECIPIENT_ID_EXP_PARAM_LITE, "NE");
	// parameters.put(Constant.RECIPIENT_NAME_PARAM_LITE,
	// responseTemp.getName());
	// parameters.put(Constant.RECIPIENT_GENDER_PARAM_LITE,
	// responseTemp.getAgentData().getGender());
	// parameters.put(Constant.RECIPIENT_ADDRESS_PARAM_LITE,
	// responseTemp.getAgentData().getAddress());
	// parameters.put(Constant.RECIPIENT_PROVINCE_PARAM_LITE,
	// responseTemp.getAgentData().getProvince());
	// parameters.put(Constant.RECIPIENT_CITY_PARAM_LITE,
	// responseTemp.getAgentData().getCity());
	// parameters.put(Constant.RECIPIENT_PHONE_NUMBER_PARAM_LITE,
	// responseTemp.getMsisdn());
	// parameters.put(Constant.RECIPIENT_PLACE_OF_BIRTH_LITE,
	// responseTemp.getAgentData().getPlaceOfBirth());
	// parameters.put(Constant.RECIPIENT_DATE_OF_BIRTH_LITE,
	// responseTemp.get().getDateOfBirth());
	// parameters.put(Constant.RECIPIENT_OCCUPATION_LITE,
	// responseTemp.getAgentData().getOccupation());
	// parameters.put(Constant.RECIPIENT_NATIONALITY_LITE, "1");
	// String randData = Utils.generateNumber(3);
	// response = utibaHandler.remiteCoupon(authResponse.getSessionId(),
	// couponId, response.getTrxid()+randData, parameters);
	// }else{
	// response.setStatus(Constant.RC_COUPON_NO_EXIST);
	// response.setMsg(dataStore.getErrorMsg(Constant.RC_COUPON_NO_EXIST));
	// }
	// } else {
	// response.setStatus(authResponse.getStatus());
	// response.setMsg(authResponse.getMsg());
	// }
	// return response;
	// }

	// ============AdjustWalletDompetku======================
	public Response overHandlingWallet(String source, String target, Integer limit) {
		// TODO Auto-generated method stub
		Response response = new Response();

		log.info("Request OverhandlingWallet: source[" + source + "] target[" + target + "]");

		String sessionId = sessionHandler.getSessionId();
		int validateSession = sessionHandler.validateSessionMerchant(sessionId);
		int channel = Constant.CHANNEL_TYPE_PLATFORM;
		ApiUser ap = new ApiUser();
		ap.setChannelType(channel);
		ap.setUserId("overhandler");
		Float virtualWalletBalance = Float.parseFloat(utibaHandler.getAgentByReferenceRequest(source, ap).getBalance());
		Float masterWalletBalance = Float.parseFloat(utibaHandler.getAgentByReferenceRequest(target, ap).getBalance());
		if (virtualWalletBalance > 0) {
			if (masterWalletBalance < limit) {
				Float amount = limit - masterWalletBalance;
				if (amount <= virtualWalletBalance) {
					response = utibaHandler.adjustWallet(sessionId, BigDecimal.valueOf(amount), source, target,
							"Prog.Uang Makan Dompetku", 0, ap);
				} else if (amount > virtualWalletBalance) {
					response = utibaHandler.adjustWallet(sessionId, BigDecimal.valueOf(virtualWalletBalance), source,
							target, "Prog.Uang Makan Dompetku", 0, ap);
				}
				virtualWalletBalance = Float
						.parseFloat(utibaHandler.getAgentByReferenceRequest(source, ap).getBalance());
				masterWalletBalance = Float
						.parseFloat(utibaHandler.getAgentByReferenceRequest(target, ap).getBalance());
				response.setStatus(Constant.RC_SUCCESS);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
				response.setBalance(String.valueOf(masterWalletBalance));
				response.setAmount(String.valueOf(virtualWalletBalance));
			} else {
				// Master Amount full
				response.setStatus(Constant.RC_WALLET_BALANCE_EXCEEDED);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_WALLET_BALANCE_EXCEEDED));
				response.setBalance(String.valueOf(masterWalletBalance));
				response.setAmount(String.valueOf(virtualWalletBalance));
			}
		} else {
			// Virtual Amount no credits
			response.setStatus(Constant.RC_AMOUNT_TO_SMALL);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_AMOUNT_TO_SMALL));
			response.setBalance(String.valueOf(masterWalletBalance));
			response.setAmount(String.valueOf(virtualWalletBalance));
		}
		return response;
	}

	@Override
	public Response mapUnmap(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String type = parameter.getKeyValueMap().get(Constant.TYPE_PARAM);
		String groupid = parameter.getKeyValueMap().get(Constant.GROUPID_PARAM);
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser ap = dataStore.getApiUser(userId);
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			if (type.equalsIgnoreCase("map")) {
				response = utibaHandler.mapAgent(authResponse.getSessionId(), msisdn, Long.valueOf(groupid),
						authResponse.getInitiator(), ap);
			} else if (type.equalsIgnoreCase("unmap")) {
				response = utibaHandler.unmapAgent(authResponse.getSessionId(), msisdn, Long.valueOf(groupid), ap);
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response updateAdmin(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String name = parameter.getKeyValueMap().get(Constant.NAME_PARAM);
		String category = parameter.getKeyValueMap().get(Constant.TYPE_PARAM);
		String isOutlet = parameter.getKeyValueMap().get(Constant.EXT_REF_PARAM);
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		ApiUser ap = dataStore.getApiUser(userId);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = utibaHandler.updateAgentToOperator(authResponse.getSessionId(), msisdn, name, msisdn, category,
					isOutlet, ap);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response directHitUtiba(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		//
		// // Setting up param
		// String xml = parameter.getKeyValueMap().get("xml");
		//
		// // Validation Param
		// if (Utils.isNullorEmptyString(xml)) {
		// response.setStatus(Constant.RC_INVALID_PARAMETERS);
		// response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
		// return response;
		// }
		//
		// AuthResponse authResponse = authenticationRequest.validate(userId,
		// signature);
		// if (authResponse.getStatus() == Constant.RC_SUCCESS) {
		// String xmlDataUTF8 = xml;
		// try {
		// xmlDataUTF8 = URLEncoder.encode(xml, "UTF-8");
		// } catch (UnsupportedEncodingException e1) {
		// response.setStatus(Constant.RC_SYSTEM_ERROR);
		// response.setMsg(dataStore.getErrorMsg(Constant.RC_SYSTEM_ERROR));
		// }
		//
		// List<MediaType> mediaTypes = new LinkedList<MediaType>();
		// mediaTypes.add(MediaType.APPLICATION_XML);
		// HttpEntity<String> he =
		// httpClientSender.sendPostByPassCertificate(utibaURL, xmlDataUTF8,
		// mediaTypes);
		// log.info(he.getBody());
		// if (he != null) {
		// response.setMsg(he.getBody());
		// } else {
		// response.setStatus(Constant.RC_TIMEOUT);
		// response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		// }
		// } else {
		// response.setStatus(authResponse.getStatus());
		// response.setMsg(authResponse.getMsg());
		// }
		// log.debug("RESPONSE:" + response.toString());
		return response;
	}

	@Override
	public Response registerOutlet(String userId, String signature, HashMap<String, String> parameterMap) {
		Response response = new Response();
		String firstName = parameterMap.get(Constant.FIRSTNAME_PARAM);
		String address = parameterMap.get(Constant.ADDRESS_PARAM);
		String msisdn = parameterMap.get(Constant.MSISDN_PARAM);
		String initiator = parameterMap.get(Constant.INITIATOR_PARAM);

		log.info("registerOutlet Request: userId[" + userId + "] 	signature[" + "XXXXXX" + "] " + "firstName["
				+ firstName + "] address[" + address + "]  msisdn[" + msisdn + "] target initiator[" + initiator + "]");

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(firstName) || Utils.isNullorEmptyString(address)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			AuthResponse authResponse = authenticationRequest.validate(userId, signature);
			if (authResponse.getStatus() == Constant.RC_SUCCESS) {
				response = utibaHandler.registerOutlet(initiator, firstName, apiUser);
			} else {
				response.setStatus(authResponse.getStatus());
				response.setMsg(authResponse.getMsg());
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	@Override
	public Response checkPointDompetku(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String suppressSMS = parameter.getKeyValueMap().get(Constant.SUPPRESS_SMS_PARAM);
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = utibaHandler.checkPoint(authResponse.getSessionId(), apiUser);
			if(Utils.isNullorEmptyString(suppressSMS))
				smsClientSender.sendPointDompetku(response.getBalance(), response.getTrxid(), msisdn);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response checkGroupingName(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = utibaHandler.getUserGroupingName(msisdn, apiUser);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response checkChild(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = utibaHandler.getChildList(msisdn, apiUser);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}
	
	// ============Firebase registration======================
	@Override
	public Response registerFirebase(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String deviceId = parameter.getKeyValueMap().get(Constant.ACCID_PARAM);
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			log.info("Send Register Req to Firebase:[" + msisdn + "]~[" + deviceId + "]");
			// http://10.138.71.2:8080/MFSP/v1/pushRegister
			HashMap<String, String> data = new HashMap<String, String>();
			data.put("deviceId", deviceId);
			data.put("userAccount", msisdn);
			String dataJson = Utils.convertToJSON(data);
			// ResponseEntity<String> responsePOST =
			// httpClientSender.sendPost(firebaseURL+"pushRegister", dataJson,
			// acceptableMediaTypes);
			MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
			headers.add("sender", "middleware-indosat-dompetku");
			headers.add("Content-Type", "application/json");
			HttpEntity<String> entity = new HttpEntity<String>(dataJson, headers);
			ResponseEntity<String> responsePOST = restTemplate.exchange(firebaseURL + "pushRegister", HttpMethod.POST,
					entity, String.class);
			try {
				if (responsePOST.getStatusCode() == HttpStatus.OK) {
					response.setStatus(Constant.RC_SUCCESS);
					response.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
				} else {
					response.setStatus(Integer.parseInt(responsePOST.getStatusCode().toString()));
					response.setMsg(responsePOST.getBody());
				}
			} catch (Exception ex) {
				response.setStatus(Constant.RC_SYSTEM_ERROR);
				response.setMsg("Error[" + ExceptionUtils.getCause(ex) + "]-RootError["
						+ ExceptionUtils.getRootCauseMessage(ex) + "]");
			}

		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response billerGwTool(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String deviceId = parameter.getKeyValueMap().get(Constant.ACCID_PARAM);
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(msisdn)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			log.info("BillerTools:[" + msisdn + "]~[" + deviceId + "]");
			HashMap<String, String> hashParam = parameter.getKeyValueMap();
			hashParam.remove(Constant.SIGNATURE_PARAM);
			hashParam.remove(Constant.USERID_PARAM);
			response = billerProcessor.processBillerGw(parameter.getKeyValueMap());
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	@Override
	public Response forgotPin(String userId, String signature, Parameter parameter) {
		log.info("resetPin Request: userId[" + userId + "] signature[XXXXXXX] ");
		Response response = new Response();
		String sms = "";

		String ektp = parameter.getKeyValueMap().get(Constant.ID_NUMBER_PARAM);
		String keyword = parameter.getKeyValueMap().get(Constant.KEYWORD_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature) || Utils.isNullorEmptyString(ektp)
				|| Utils.isNullorEmptyString(keyword)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			// CheckEKTP Length
			if (ektp.length() < 5) {
				response.setStatus(Constant.RC_INVALID_PARAMETERS);
				response.setMsg("Nomor KTP yang digunakan tidak valid");
				sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
						"Nomor KTP yang digunakan tidak valid");
				// if
				// (StringUtils.isNumeric(Utils.getUsernameAndPassFromSignature(signature,
				// apiUser.getPassKey())[0]))
				if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
					smsClientSender.send(
							sms.replace("Proses konfirmasi data gagal", "Proses konfirmasi data " + msisdn + " gagal"),
							msisdn);
				} else {
					mailHandler
							.sendFirebaseNotification(null,
									msisdn, response
											.getTrxid(),
									sms.replace("Proses konfirmasi data gagal",
											"Proses konfirmasi data " + msisdn + " gagal"),
									sms.replace("Proses konfirmasi data gagal",
											"Proses konfirmasi data " + msisdn + " gagal").substring(0, 100) + "...");
				}
				// }
				return response;
			}

			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {
				response = eKTP(parameter);
				StringMetric metric = StringMetrics.damerauLevenshtein();
				AgentData ag = response.getAgentData();
				if (metric.compare(keyword.trim().toLowerCase(), ag.getMotherMaidenName().trim().toLowerCase()) > 0.8f
						|| keyword.equalsIgnoreCase(ag.getNo_kk())) {
					response.setAgentData(null);
					response = utibaHandler.resetPin(msisdn, apiUser);
				} else {
					response.setStatus(Constant.RC_EKTP_NOT_VALID);
					response.setMsg("Validasi KTP Gagal, silahkan ulangi kembali");
					sms = smsRetryKYCKTP.replace(Constant.ERROR_MESSAGE_REPLACE_TAG,
							dataStore.getErrorMsg(Constant.RC_EKTP_NOT_VALID));
					// if
					// (StringUtils.isNumeric(Utils.getUsernameAndPassFromSignature(signature,
					// apiUser.getPassKey())[0]))
					if (dataStore.getTelcoIdByPrefix(msisdn).getName().equalsIgnoreCase("INDOSAT")) {
						smsClientSender.send(
								sms.replace("Proses Reset PIN gagal", "Proses Reset PIN " + msisdn + " gagal"), msisdn);
					} else {
						mailHandler
								.sendFirebaseNotification(null,
										msisdn, response
												.getTrxid(),
										sms.replace("Proses Reset PIN gagal", "Proses Reset PIN " + msisdn + " gagal"),
										sms.replace("Proses Reset PIN gagal", "Proses Reset PIN " + msisdn + " gagal")
												.substring(0, 100) + "...");
					}
					// }
					response.setAgentData(null);
					return response;
				}
			} else {
				response.setStatus(Constant.RC_INVALID_SIGNATURE);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_SIGNATURE));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	@Override
	public Response getWhitelabelSequence(String userId, Parameter parameter) {
		log.info("Get WhitelabelSequence Request: userId[" + userId + "] signature[XXXXXXX] ");
		Response response = new Response();

		String partMsisdn = parameter.getKeyValueMap().get("partMsisdn");
		String partnerCode = parameter.getKeyValueMap().get("partnerCode");

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(partMsisdn)
				|| Utils.isNullorEmptyString(partnerCode)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			List<WhitelabelSequence> wls = whitelabelSeqDao.getWhitelabelSequenceSpesific(partMsisdn, partnerCode);
			List<String> resDetail = new ArrayList<>();
			int counter = 0;
			if (wls.size() > 0) {
				response.setStatus(Constant.RC_SUCCESS);
				response.setAmount(String.valueOf(wls.size()));
				for (WhitelabelSequence obj : wls) {
					counter++;
					resDetail.add(counter + ":" + obj.getMsisdn());
				}
				response.setMsg(resDetail.toString());
				response.setMsisdn("999" + partnerCode + partMsisdn + String.format("%03d", counter + 1));
			} else {
				response.setStatus(Constant.RC_AGENT_NOTFOUND);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_AGENT_NOTFOUND));
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	@Override
	public Response setWhitelabelSequence(String userId, String signature, Parameter parameter) {
		log.info("Set WhitelabelSequence Request: userId[" + userId + "] signature[XXXXXXX] ");
		Response response = new Response();

		String partnerCode = parameter.getKeyValueMap().get("partnerCode");
		String msisdn = parameter.getKeyValueMap().get("msisdn");
		String partMsisdn = msisdn.substring(msisdn.length() - 4, msisdn.length());
		// System.out.println("085721067790".substring("085721067790".length()-4,
		// "085721067790".length()));

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(partMsisdn)
				|| Utils.isNullorEmptyString(signature)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser apiUser = dataStore.getApiUser(userId);
		if (apiUser != null) {
			String getCredential[] = Utils.getUsernameAndPassFromSignature(signature, apiUser.getPassKey());
			if (getCredential != null) {

				List<WhitelabelSequence> wls = whitelabelSeqDao.getWhitelabelSequenceSpesificMsisdn(msisdn,
						partnerCode);
				if (wls.size() > 0) {
					response.setStatus(Constant.RC_USER_ALREADY_EXIST);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_USER_ALREADY_EXIST));
					return response;
				}

				WhitelabelSequence newObj = new WhitelabelSequence();
				newObj.setMsisdn(msisdn);
				newObj.setMsisdn_part(partMsisdn);
				newObj.setPartner_code(partnerCode);
				Integer res = whitelabelSeqDao.insert(newObj);
				if (res > 0) {
					response.setStatus(Constant.RC_SUCCESS);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
				} else {
					response.setStatus(Constant.RC_TRANSACTIN_FAILED);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_TRANSACTIN_FAILED));
				}
			}
		} else {
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		return response;
	}

	@Override
	public Response transQueryExtExtra(String userId, String signature, String extRef) {
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(extRef)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		ApiUser ap = dataStore.getApiUser(userId);
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = utibaHandler.transQueryExtExtra(authResponse.getSessionId(), extRef, ap);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;
	}

	@Override
	public Response getDetailTrx(String userId, String signature, String transid) {
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(transid)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = utibaHandler.getTransactionById(authResponse.getSessionId(), Integer.parseInt(transid), 999998,
					apiUser);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;
	}
	
	@Override
	public Response reversalExtRef(String userId, String signature, String extRef, String amount) {
		Response response = new Response();

		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(extRef) || Utils.isNullorEmptyString(amount)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}

		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			response = utibaHandler.reversalExtRef(authResponse.getSessionId(), extRef, amount, apiUser);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}

		return response;
	}

	@Override
	public Response adjustWallet(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		String amount = parameter.getKeyValueMap().get(Constant.AMOUNT_PARAM);
		String source = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String target = parameter.getKeyValueMap().get(Constant.TARGET_PARAM);
		String desc = parameter.getKeyValueMap().get(Constant.NOTE_PARAM);
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(amount) || Utils.isNullorEmptyString(source)
				|| Utils.isNullorEmptyString(target)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		ApiUser apiUser = dataStore.getApiUser(userId);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			log.info("Adjustment Wallet:[" + source + "]~[" + target + "]~[" + amount + "]");
			response = utibaHandler.adjustWallet(authResponse.getSessionId(),
					BigDecimal.valueOf(Double.valueOf(amount)), source, target, desc, 0, apiUser);
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}

	//Only can called after merchant created on Utiba
	//Use User id from apps, and Signature consist of new Merchant data
	@Override
	public Response createNewMerchant(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		Integer responseType = parameter.getKeyValueMap().get(Constant.TYPE_PARAM)==null?0:Integer.parseInt(parameter.getKeyValueMap().get(Constant.TYPE_PARAM));
		Integer channelType = parameter.getKeyValueMap().get(Constant.CHANNEL_PARAM)==null?0:Integer.parseInt(parameter.getKeyValueMap().get(Constant.CHANNEL_PARAM));
		String ipList = parameter.getKeyValueMap().get(Constant.IP_PARAM);
		String apiList = parameter.getKeyValueMap().get(Constant.API_LIST_PARAM);
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(ipList)|| Utils.isNullorEmptyString(apiList)) {
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			log.info("Create Merchant Web API Userid:[" + authResponse.getInitiator() + "]~[" + ipList + "]");
			//Check user merchant, existing or not
			ApiUser apiMerchantCheck = dataStore.getApiUser(authResponse.getInitiator());
			if(apiMerchantCheck!=null){
				response.setStatus(Constant.RC_AGENT_ALREADY_REGISTERED);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_AGENT_ALREADY_REGISTERED));
				return response;
			}
			/*
			 * createMerchant in Utiba Part
			 */
			String key = RandomStringUtils.randomAlphanumeric(24);
			ApiUser userMerchant = new ApiUser();
			userMerchant.setActive(0);
			userMerchant.setApiList(apiList);
			userMerchant.setChannelType(channelType);
			userMerchant.setGroupId(1);
			userMerchant.setIpList(ipList);
			userMerchant.setResponseType(responseType);
			userMerchant.setServiceType("1");
			userMerchant.setUserId(authResponse.getInitiator());
			userMerchant.setPassKey(key);
			int resSave = apiUserDao.saveNewMerchant(userMerchant);
			if(resSave>0){
				HashMap<String,String> detail = new HashMap<>();
				detail.put("userid", authResponse.getInitiator());
				detail.put("key",key);
				detail.put("ip",ipList);
				response.setStatus(Constant.RC_SUCCESS);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_SUCCESS));
				response.setDetailTrx(detail);
				return response;
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}
	
	@Override
	public Response qrOfflineRequest(String userId, String signature, Parameter parameter) {
		Response response = new Response();
		long timeNow = System.currentTimeMillis()/1000;
		
		String extRef = parameter.getKeyValueMap().get(Constant.EXT_REF_PARAM);
		String amount = parameter.getKeyValueMap().get(Constant.AMOUNT_PARAM);
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String timestamp = parameter.getKeyValueMap().get(Constant.QR_TIMESTAMP_PARAM);
		String digest = parameter.getKeyValueMap().get(Constant.QR_DIGEST_PARAM);
		String terminalID = parameter.getKeyValueMap().get(Constant.TERMID_PARAM);
		String operatorID = parameter.getKeyValueMap().get(Constant.OPERID_PARAM);
		HashMap<String,String> paramHash = parameter.getKeyValueMap();
		if (Utils.isNullorEmptyString(userId) || Utils.isNullorEmptyString(signature)
				|| Utils.isNullorEmptyString(amount)|| Utils.isNullorEmptyString(msisdn)
				|| Utils.isNullorEmptyString(timestamp)|| Utils.isNullorEmptyString(digest)
				|| Utils.isNullorEmptyString(terminalID)|| Utils.isNullorEmptyString(operatorID)) {
			log.debug("Error [Input param not complete]");
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		
		//check is timestamp can parse to "long"
		try{
			if(Utils.deltaTime(timeNow, Long.parseLong(timestamp)/1000)>=limitTimeQR){
				log.info("Expired Offline QR ["+msisdn+"]["+extRef+"]["+timestamp+"]");
				response.setStatus(Constant.RC_COUPON_NO_EXIST);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_COUPON_NO_EXIST));
				return response;
			}
		}catch(NumberFormatException ex){
			log.debug("Error [Time format error]");
			response.setStatus(Constant.RC_INVALID_PARAMETERS);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
			return response;
		}
		
		AuthResponse authResponse = authenticationRequest.validate(userId, signature);
		if (authResponse.getStatus() == Constant.RC_SUCCESS) {
			log.info("QR Offline Request:[" + authResponse.getInitiator() + "]-[" + amount + "]-[" + msisdn + "]-[" + terminalID + "]-[" + operatorID + "]");
			ApiUser apiUser = dataStore.getApiUser(userId);
			//Check digest valid or not
			try{
				Map<String,String> qr = microApps.qrOfflineParser(digest);
				if(!qr.get("msisdn").equalsIgnoreCase(msisdn) || !qr.get("amount").equalsIgnoreCase(amount)){
					log.debug("Error [msisdn & amount not match]");
					response.setStatus(Constant.RC_INVALID_PARAMETERS);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
					return response;
				}
				
				paramHash.put(Constant.TO_PARAM, authResponse.getInitiator());
				parameter.setKeyValueMap(paramHash);
				if(qr!=null){
					String secret = qr.get("secret");
					String signatureUser = SignatureBuilder.build(msisdn, secret, apiUser.getPassKey());
					String to = paramHash.get(Constant.TO_PARAM);
					response = merchantTransfer(userId, signatureUser, amount, to, paramHash);
//					response = moneyTransferExtParamSeperatedP2P(userId, signatureUser, parameter);
//					response = utibaHandler.sellNoConfirmWithParam(authResponse.getSessionId(), msisdn, amount, extRef,"Qr Offline",0,parameter, apiUser);
					response.setAmount(amount);
				}else{
					response.setStatus(Constant.RC_INVALID_PARAMETERS);
					response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
					return response;
				}
//				if(microApps.qrOfflineCheck(amount, msisdn, timestamp, digest)){
//					response = utibaHandler.sellNoConfirmWithParam(authResponse.getSessionId(), msisdn, amount, extRef,
//							"Qr Offline",0,parameter, apiUser);
//					response.setAmount(amount);
//				}else{
//					response.setStatus(Constant.RC_INVALID_PARAMETERS);
//					response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
//					return response;
//				}
			}catch(Exception ex){
				log.error(ExceptionUtils.getFullStackTrace(ex));
				response.setStatus(Constant.RC_INVALID_PARAMETERS);
				response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_PARAMETERS));
				return response;
			}
		} else {
			response.setStatus(authResponse.getStatus());
			response.setMsg(authResponse.getMsg());
		}
		return response;
	}
	
	@Override
	public Response sendNotification(String userId, Parameter parameter) {
		Response response = new Response();
		long timeNow = System.currentTimeMillis()/1000;
		
		String msisdn = parameter.getKeyValueMap().get(Constant.MSISDN_PARAM);
		String msg = parameter.getKeyValueMap().get(Constant.NOTE_PARAM);
		HashMap<String,String> paramHash = parameter.getKeyValueMap();
		
		ApiUser aUser = dataStore.getApiUser(userId);
		if(aUser!=null){
			smsClientSender.sendAutoChoose(msg, msisdn);
		}else{
			response.setStatus(Constant.RC_INVALID_USERID);
			response.setMsg(dataStore.getErrorMsg(Constant.RC_INVALID_USERID));
		}
		
		return response;
	}

	/*
	 * @Override public Response getTransactionById(String userId, String
	 * signature, int id, int requestType) { // TODO Auto-generated method stub
	 * return null; }
	 */

	/*
	 * @Override public Response getTransactionById(String userId, String
	 * signature, int id, int requestType) { // TODO Auto-generated method stub
	 * return null; }
	 */

}
