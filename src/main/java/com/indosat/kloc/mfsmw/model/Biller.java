/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 19, 2014 
 * Time       : 2:37:06 PM 
 */
package com.indosat.kloc.mfsmw.model;

import java.util.List;

public class Biller {

	private int id;
	private String name;
	private String target;
	private String inquiry;
	private String payment;
	
	private List<ExtraTransData> extTransDataQueryBillpay;
	private List<ExtraTransData> extTransDataBillpay;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getInquiry() {
		return inquiry;
	}
	public void setInquiry(String inquiry) {
		this.inquiry = inquiry;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public List<ExtraTransData> getExtTransDataQueryBillpay() {
		return extTransDataQueryBillpay;
	}
	public void setExtTransDataQueryBillpay(
			List<ExtraTransData> extTransDataQueryBillpay) {
		this.extTransDataQueryBillpay = extTransDataQueryBillpay;
	}
	public List<ExtraTransData> getExtTransDataBillpay() {
		return extTransDataBillpay;
	}
	public void setExtTransDataBillpay(List<ExtraTransData> extTransDataBillpay) {
		this.extTransDataBillpay = extTransDataBillpay;
	}
	
	@Override
	public String toString() {
		return "Biller [id=" + id + ", name=" + name + ", target=" + target
				+ ", inquiry=" + inquiry + ", payment=" + payment
				+ ", extTransDataQueryBillpay=" + extTransDataQueryBillpay
				+ ", extTransDataBillpay=" + extTransDataBillpay + "]";
	}
	
	
}
