/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 12, 2015 
 * Time       : 11:52:52 AM 
 */
package com.indosat.kloc.mfsmw.model;

public class Product {

	private int id;
	private int billerId;
	private int targetId;
	private int productRefId;
	private String amount;
	private String fee;
	private String productCode;
	private int priority;
	private int status;
	private int smsNotificationTemplateId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBillerId() {
		return billerId;
	}
	public void setBillerId(int billerId) {
		this.billerId = billerId;
	}
	public int getTargetId() {
		return targetId;
	}
	public void setTargetId(int targetId) {
		this.targetId = targetId;
	}
	public int getProductRefId() {
		return productRefId;
	}
	public void setProductRefId(int productRefId) {
		this.productRefId = productRefId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getSmsNotificationTemplateId() {
		return smsNotificationTemplateId;
	}
	public void setSmsNotificationTemplateId(int smsNotificationTemplateId) {
		this.smsNotificationTemplateId = smsNotificationTemplateId;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", billerId=" + billerId + ", targetId="
				+ targetId + ", productRefId=" + productRefId + ", amount="
				+ amount + ", fee=" + fee + ", productCode=" + productCode
				+ ", priority=" + priority + ", status=" + status
				+ ", smsNotificationTemplateId=" + smsNotificationTemplateId
				+ "]";
	}
	
	
	
}
