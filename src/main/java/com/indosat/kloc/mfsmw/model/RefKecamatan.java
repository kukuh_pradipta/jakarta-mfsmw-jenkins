/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 19, 2014 
 * Time       : 2:37:06 PM 
 */
package com.indosat.kloc.mfsmw.model;

public class RefKecamatan {

	private String lokasi_kode;
	private String lokasi_nama;
	
	public String getLokasi_kode() {
		return lokasi_kode;
	}
	public void setLokasi_kode(String lokasi_kode) {
		this.lokasi_kode = lokasi_kode;
	}
	public String getLokasi_nama() {
		return lokasi_nama;
	}
	public void setLokasi_nama(String lokasi_nama) {
		this.lokasi_nama = lokasi_nama;
	}
	
	@Override
	public String toString() {
		return "RefKecamatan [" + (lokasi_kode != null ? "lokasi_kode=" + lokasi_kode + ", " : "")
				+ (lokasi_nama != null ? "lokasi_nama=" + lokasi_nama : "") + "]";
	}
}
