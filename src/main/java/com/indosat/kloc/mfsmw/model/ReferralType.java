/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 22, 2018 
 * Time       : 2:24:28 PM 
 */
package com.indosat.kloc.mfsmw.model;

public class ReferralType {
	private String referralNo;
	private String joinType;
	private String parentId;
	private int flagActive;
	
	public String getReferralNo() {
		return referralNo;
	}
	public void setReferralNo(String referralNo) {
		this.referralNo = referralNo;
	}
	public String getJoinType() {
		return joinType;
	}
	public void setJoinType(String joinType) {
		this.joinType = joinType;
	}
	
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	public int getFlagActive() {
		return flagActive;
	}
	public void setFlagActive(int flagActive) {
		this.flagActive = flagActive;
	}
	
	@Override
	public String toString() {
		return "ReferralType [referralNo=" + referralNo + ", joinType=" + joinType + ", parentId=" + parentId
				+ ", flagActive=" + flagActive + "]";
	}
	
	
	
}
