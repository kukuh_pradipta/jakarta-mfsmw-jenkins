/**
 * Project    : Dompetku - Domestic Money Transfer
 * Created By : Megi Jaka Permana
 * Date       : May 15, 2015 
 * Time       : 5:54:28 PM 
 */
package com.indosat.kloc.mfsmw.model;

public class TrxDMTFee {

	private int id;
	private String min;
	private String max;
	private String fee;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMin() {
		return min;
	}
	public void setMin(String min) {
		this.min = min;
	}
	public String getMax() {
		return max;
	}
	public void setMax(String max) {
		this.max = max;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	
	@Override
	public String toString() {
		return "TrxFee [id=" + id + ", min=" + min + ", max=" + max + ", fee="
				+ fee + "]";
	}
	
	
	
}
