/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 18, 2015 
 * Time       : 4:18:31 PM 
 */
package com.indosat.kloc.mfsmw.model;

public class MBillerTransaction {

	private String transId;
	private String refTransId;
	private String walletTransId;
	private String billerTransId;
	private String channelTransId;
	private int productId;
	private int type;
	private String initiator;
	private String destination;
	private String destinationName;
	private String msg;
	private String amount;
	private String price;
	private String fee;
	private String details;
	private String dateTime;
	private long responseTime;
	private int responseCode;
	
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getWalletTransId() {
		return walletTransId;
	}
	public void setWalletTransId(String walletTransId) {
		this.walletTransId = walletTransId;
	}
	public String getBillerTransId() {
		return billerTransId;
	}
	public void setBillerTransId(String billerTransId) {
		this.billerTransId = billerTransId;
	}
	public String getChannelTransId() {
		return channelTransId;
	}
	public void setChannelTransId(String channelTransId) {
		this.channelTransId = channelTransId;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getInitiator() {
		return initiator;
	}
	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public long getResponseTime() {
		return responseTime;
	}
	public void setResponseTime(long responseTime) {
		this.responseTime = responseTime;
	}
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	
	public String getRefTransId() {
		return refTransId;
	}
	public void setRefTransId(String refTransId) {
		this.refTransId = refTransId;
	}
	public String getDestinationName() {
		return destinationName;
	}
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	@Override
	public String toString() {
		return "MBillerTransaction [transId=" + transId + ", refTransId="
				+ refTransId + ", walletTransId=" + walletTransId
				+ ", billerTransId=" + billerTransId + ", channelTransId="
				+ channelTransId + ", productId=" + productId + ", type="
				+ type + ", initiator=" + initiator + ", destination="
				+ destination + ", destinationName=" + destinationName
				+ ", msg=" + msg + ", amount=" + amount + ", price=" + price
				+ ", fee=" + fee + ", details=" + details + ", dateTime="
				+ dateTime + ", responseTime=" + responseTime
				+ ", responseCode=" + responseCode + "]";
	}
	
	
	
}
