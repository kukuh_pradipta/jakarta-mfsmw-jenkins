/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Feb 16, 2015 
 * Time       : 2:34:48 PM 
 */
package com.indosat.kloc.mfsmw.model;

public class WhitelabelSequence {

	private int id;
	private String partner_code;
	private String msisdn;
	private String msisdn_part;
	private String created_time;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPartner_code() {
		return partner_code;
	}
	public void setPartner_code(String partner_code) {
		this.partner_code = partner_code;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getMsisdn_part() {
		return msisdn_part;
	}
	public void setMsisdn_part(String msisdn_part) {
		this.msisdn_part = msisdn_part;
	}
	public String getCreated_time() {
		return created_time;
	}
	public void setCreated_time(String created_time) {
		this.created_time = created_time;
	}
	@Override
	public String toString() {
		return "[id=" + id + ", partner_code=" + partner_code + ", msisdn=" + msisdn
				+ ", msisdn_part=" + msisdn_part + ", created_time=" + created_time + "]";
	}
	
	
	
}
