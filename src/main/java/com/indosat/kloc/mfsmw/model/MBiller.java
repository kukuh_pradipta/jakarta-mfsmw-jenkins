/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 12, 2015 
 * Time       : 10:49:06 AM 
 */
package com.indosat.kloc.mfsmw.model;

public class MBiller {

	private int id;
	private String name;
	private String url;
	private String funcInq;
	private String funcPay;
	private String initiator;
	private String pin;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getFuncInq() {
		return funcInq;
	}
	public void setFuncInq(String funcInq) {
		this.funcInq = funcInq;
	}
	public String getFuncPay() {
		return funcPay;
	}
	public void setFuncPay(String funcPay) {
		this.funcPay = funcPay;
	}
	
	public String getInitiator() {
		return initiator;
	}
	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	@Override
	public String toString() {
		return "MBiller [id=" + id + ", name=" + name + ", url=" + url
				+ ", funcInq=" + funcInq + ", funcPay=" + funcPay
				+ ", initiator=" + initiator + ", pin=" + pin + "]";
	}
	
	
}
