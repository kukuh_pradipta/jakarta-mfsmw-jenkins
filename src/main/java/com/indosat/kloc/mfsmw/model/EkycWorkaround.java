/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 6, 2014 
 * Time       : 12:09:42 PM 
 */
package com.indosat.kloc.mfsmw.model;

public class EkycWorkaround {

	private String msisdn;
	private String idno;
	private String keyword;
	private String created_date;

	public EkycWorkaround(String msisdn, String idno, String keyword) {
		super();
		this.msisdn = msisdn;
		this.idno = idno;
		this.keyword = keyword;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getIdno() {
		return idno;
	}
	public void setIdno(String idno) {
		this.idno = idno;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	@Override
	public String toString() {
		return "Prefix [" + (msisdn != null ? "msisdn=" + msisdn + ", " : "")
				+ (idno != null ? "idno=" + idno + ", " : "") + (keyword != null ? "keyword=" + keyword + ", " : "")
				+ (created_date != null ? "created_date=" + created_date : "") + "]";
	}
	
	
	
}
