/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Apr 21, 2015 
 * Time       : 11:49:49 AM 
 */
package com.indosat.kloc.mfsmw.model;

public class RemitCouponInq {

	private int id;
	private String sessionId;
	private int transId;
	private String extRef;
	private String storeName;
	private String amount;
	private String recipientPhoneNumber;
	private String recipientName;
	private String recipientSourceOfFund;
	private String recipientPurpose;
	private String recipientAddress;
	private String recipientProvince;
	private String recipientCity;
	private String senderIdType;
	private String senderIdNo;
	private String senderName;
	private String senderPhoneNumber;
	private int status;
	private String uniqueCode;
	private String note;
	
	public int getId() {
		return id;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTransId() {
		return transId;
	}
	public void setTransId(int transId) {
		this.transId = transId;
	}
	public String getExtRef() {
		return extRef;
	}
	public void setExtRef(String extRef) {
		this.extRef = extRef;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getRecipientName() {
		return recipientName;
	}
	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getRecipientPhoneNumber() {
		return recipientPhoneNumber;
	}
	public void setRecipientPhoneNumber(String recipientPhoneNumber) {
		this.recipientPhoneNumber = recipientPhoneNumber;
	}
	public String getRecipientSourceOfFund() {
		return recipientSourceOfFund;
	}
	public void setRecipientSourceOfFund(String recipientSourceOfFund) {
		this.recipientSourceOfFund = recipientSourceOfFund;
	}
	public String getRecipientPurpose() {
		return recipientPurpose;
	}
	public void setRecipientPurpose(String recipientPurpose) {
		this.recipientPurpose = recipientPurpose;
	}
	public String getSenderIdType() {
		return senderIdType;
	}
	public void setSenderIdType(String senderIdType) {
		this.senderIdType = senderIdType;
	}
	public String getSenderIdNo() {
		return senderIdNo;
	}
	public void setSenderIdNo(String senderIdNo) {
		this.senderIdNo = senderIdNo;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getRecipientAddress() {
		return recipientAddress;
	}
	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}
	public String getRecipientProvince() {
		return recipientProvince;
	}
	public void setRecipientProvince(String recipientProvince) {
		this.recipientProvince = recipientProvince;
	}
	public String getRecipientCity() {
		return recipientCity;
	}
	public void setRecipientCity(String recipientCity) {
		this.recipientCity = recipientCity;
	}
	
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getSenderPhoneNumber() {
		return senderPhoneNumber;
	}
	public void setSenderPhoneNumber(String senderPhoneNumber) {
		this.senderPhoneNumber = senderPhoneNumber;
	}
	public String getUniqueCode() {
		return uniqueCode;
	}
	public void setUniqueCode(String uniqueCode) {
		this.uniqueCode = uniqueCode;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	@Override
	public String toString() {
		return "RemitCouponInq [id=" + id + ", sessionId=" + sessionId + ", transId=" + transId + ", extRef=" + extRef
				+ ", storeName=" + storeName + ", amount=" + amount + ", recipientPhoneNumber=" + recipientPhoneNumber
				+ ", recipientName=" + recipientName + ", recipientSourceOfFund=" + recipientSourceOfFund
				+ ", recipientPurpose=" + recipientPurpose + ", recipientAddress=" + recipientAddress
				+ ", recipientProvince=" + recipientProvince + ", recipientCity=" + recipientCity + ", senderIdType="
				+ senderIdType + ", senderIdNo=" + senderIdNo + ", senderName=" + senderName + ", senderPhoneNumber="
				+ senderPhoneNumber + ", status=" + status + ", uniqueCode=" + uniqueCode + ", note=" + note + "]";
	}
	
}
