/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : May 12, 2015 
 * Time       : 11:31:48 AM 
 */
package com.indosat.kloc.mfsmw.model;

public class SMSOutbox {

	private int id;
	private String transId;
	private String msisdn;
	private String sms;
	private String responseBody;
	private String messageId;
	private int status;
	private int counter;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getSms() {
		return sms;
	}
	public void setSms(String sms) {
		this.sms = sms;
	}
	public String getResponseBody() {
		return responseBody;
	}
	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	@Override
	public String toString() {
		return "SMSOutbox [id=" + id + ", transId=" + transId + ", msisdn="
				+ msisdn + ", sms=" + sms + ", responseBody=" + responseBody
				+ ", messageId=" + messageId + ", status=" + status
				+ ", counter=" + counter + "]";
	}
	
}
