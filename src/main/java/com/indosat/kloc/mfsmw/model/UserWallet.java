/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Nov 24, 2014 
 * Time       : 3:45:57 PM 
 */
package com.indosat.kloc.mfsmw.model;

import java.math.BigInteger;

public class UserWallet {

	private BigInteger id;
	private String msisdn;
	private String name;
	private String idno;
	private String idtype;
	private String countrycode;
	private String gender;
	private String dob;
	private String address;
	private String wallet_type;
	private String status;
	private String encrypted_text;
	private String encrypted_code;
	private String created_time;
	private String updated_time;
	
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIdno() {
		return idno;
	}
	public void setIdno(String idno) {
		this.idno = idno;
	}
	public String getIdtype() {
		return idtype;
	}
	public void setIdtype(String idtype) {
		this.idtype = idtype;
	}
	public String getCountrycode() {
		return countrycode;
	}
	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getWallet_type() {
		return wallet_type;
	}
	public void setWallet_type(String wallet_type) {
		this.wallet_type = wallet_type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEncrypted_text() {
		return encrypted_text;
	}
	public void setEncrypted_text(String encrypted_text) {
		this.encrypted_text = encrypted_text;
	}
	public String getEncrypted_code() {
		return encrypted_code;
	}
	public void setEncrypted_code(String encrypted_code) {
		this.encrypted_code = encrypted_code;
	}
	public String getCreated_time() {
		return created_time;
	}
	public void setCreated_time(String created_time) {
		this.created_time = created_time;
	}
	public String getUpdated_time() {
		return updated_time;
	}
	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}
	
	@Override
	public String toString() {
		return "UserWallet [" + (id != null ? "id=" + id + ", " : "")
				+ (msisdn != null ? "msisdn=" + msisdn + ", " : "") + (name != null ? "name=" + name + ", " : "")
				+ (idno != null ? "idno=" + idno + ", " : "") + (idtype != null ? "idtype=" + idtype + ", " : "")
				+ (countrycode != null ? "countrycode=" + countrycode + ", " : "")
				+ (gender != null ? "gender=" + gender + ", " : "") + (dob != null ? "dob=" + dob + ", " : "")
				+ (address != null ? "address=" + address + ", " : "")
				+ (wallet_type != null ? "wallet_type=" + wallet_type + ", " : "")
				+ (status != null ? "status=" + status + ", " : "")
				+ (encrypted_text != null ? "encrypted_text=" + encrypted_text + ", " : "")
				+ (encrypted_code != null ? "encrypted_code=" + encrypted_code + ", " : "")
				+ (created_time != null ? "created_time=" + created_time + ", " : "")
				+ (updated_time != null ? "updated_time=" + updated_time : "") + "]";
	}
}
