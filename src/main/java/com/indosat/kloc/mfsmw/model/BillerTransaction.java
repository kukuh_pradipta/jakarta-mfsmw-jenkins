/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 21, 2014 
 * Time       : 5:11:47 PM 
 */
package com.indosat.kloc.mfsmw.model;

public class BillerTransaction {
	
	private int id;
	private String billerName;
	private String function;
	private String extRef;
	private String transId;
	private String amount;
	private String dateTime;
	private String msisdn;
	private String initiator;
	private String responseCode;
	private String responseBody;
	
	
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBillerName() {
		return billerName;
	}
	public void setBillerName(String billerName) {
		this.billerName = billerName;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getExtRef() {
		return extRef;
	}
	public void setExtRef(String extRef) {
		this.extRef = extRef;
	}
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseBody() {
		return responseBody;
	}
	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}
	public String getInitiator() {
		return initiator;
	}
	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}
	@Override
	public String toString() {
		return "BillerTransaction [id=" + id + ", billerName=" + billerName
				+ ", function=" + function + ", extRef=" + extRef
				+ ", transId=" + transId + ", amount=" + amount + ", dateTime="
				+ dateTime + ", msisdn=" + msisdn + ", initiator=" + initiator
				+ ", responseCode=" + responseCode + ", responseBody="
				+ responseBody + "]";
	}
	
	

}
