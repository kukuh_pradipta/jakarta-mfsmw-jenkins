/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 24, 2015 
 * Time       : 6:29:06 PM 
 */
package com.indosat.kloc.mfsmw.model;

public class SmsNotificationTemplate {

	private int id;
	private String title;
	private String message;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return "SmsNotificationTemplate [id=" + id + ", title=" + title
				+ ", message=" + message + "]";
	}
	
}
