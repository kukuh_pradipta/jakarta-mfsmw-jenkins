/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Nov 24, 2014 
 * Time       : 3:45:57 PM 
 */
package com.indosat.kloc.mfsmw.model;

import java.math.BigInteger;
import java.util.Date;

public class User {

	private BigInteger id;
	private String username;
	private String firstName;
	private String lastName;
	private String dob;
	private String gender;
	private String motherMaidenName;
	private String address;
	private String idType;
	private String idNo;
	private String msisdn;
	private String profilePic;
	private String encryptedText;
	
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	private int status;
	private Date createdDate;
	
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMotherMaidenName() {
		return motherMaidenName;
	}
	public void setMotherMaidenName(String motherMaidenName) {
		this.motherMaidenName = motherMaidenName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getIdNo() {
		return idNo;
	}
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getProfilePic() {
		return profilePic;
	}
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	
	public String getEncryptedText() {
		return encryptedText;
	}
	public void setEncryptedText(String encryptedText) {
		this.encryptedText = encryptedText;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", firstName="
				+ firstName + ", lastName=" + lastName + ", dob=" + dob
				+ ", gender=" + gender 
				+ ", motherMaidenName="+ motherMaidenName 
				+ ", address=" + address + ", idType="
				+ idType + ", idNo=" + idNo + ", msisdn=" + msisdn
				+ ", profilePic=" + profilePic + ", status=" + status
				+ ", createdDate=" + createdDate + "]";
	}
	
	
	
	
}
