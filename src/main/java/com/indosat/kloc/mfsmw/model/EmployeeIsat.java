/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 8, 2014 
 * Time       : 9:39:01 AM 
 */
package com.indosat.kloc.mfsmw.model;

public class EmployeeIsat {
	
	private int id;
	private String mainWallet;
	private int virtualAmount;
	private int mainAmount;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMainWallet() {
		return mainWallet;
	}
	public void setMainWallet(String mainWallet) {
		this.mainWallet = mainWallet;
	}
	public int getVirtualAmount() {
		return virtualAmount;
	}
	public void setVirtualAmount(int virtualAmount) {
		this.virtualAmount = virtualAmount;
	}
	public int getMainAmount() {
		return mainAmount;
	}
	public void setMainAmount(int mainAmount) {
		this.mainAmount = mainAmount;
	}
	@Override
	public String toString() {
		return "EmployeeIsat [id=" + id + ", mainWallet=" + mainWallet + ", virtualAmount=" + virtualAmount
				+ ", mainAmount=" + mainAmount + "]";
	}
}
