/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 12, 2015 
 * Time       : 12:31:28 PM 
 */
package com.indosat.kloc.mfsmw.model;

public class PrefixReference {

	private int id;
	private String prefix;
	private String denom;
	private String reference;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getDenom() {
		return denom;
	}
	public void setDenom(String denom) {
		this.denom = denom;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	
	@Override
	public String toString() {
		return "PrefixReference [id=" + id + ", prefix=" + prefix + ", denom="
				+ denom + ", reference=" + reference + "]";
	}
	
	
}
