/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 8, 2014 
 * Time       : 9:39:01 AM 
 */
package com.indosat.kloc.mfsmw.model;

public class Denom {
	
	private int id;
	private int prefixId;
	private String denom;
	private String price;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPrefixId() {
		return prefixId;
	}
	public void setPrefixId(int prefixId) {
		this.prefixId = prefixId;
	}
	public String getDenom() {
		return denom;
	}
	public void setDenom(String denom) {
		this.denom = denom;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
}
