/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jan 10, 2015 
 * Time       : 9:53:06 PM 
 */
package com.indosat.kloc.mfsmw.model;

public class OTPPaypro {

	private String otp_id;
	private String otp_msisdn;
	private String otp_sessid;
	private String otp_date_start;
	private String otp_date_end;
	private String otp_flag;
	private String otp_request_id;
	private String otp_sms_result;
	public String getOtp_id() {
		return otp_id;
	}
	public void setOtp_id(String otp_id) {
		this.otp_id = otp_id;
	}
	public String getOtp_msisdn() {
		return otp_msisdn;
	}
	public void setOtp_msisdn(String otp_msisdn) {
		this.otp_msisdn = otp_msisdn;
	}
	public String getOtp_sessid() {
		return otp_sessid;
	}
	public void setOtp_sessid(String otp_sessid) {
		this.otp_sessid = otp_sessid;
	}
	public String getOtp_date_start() {
		return otp_date_start;
	}
	public void setOtp_date_start(String otp_date_start) {
		this.otp_date_start = otp_date_start;
	}
	public String getOtp_date_end() {
		return otp_date_end;
	}
	public void setOtp_date_end(String otp_date_end) {
		this.otp_date_end = otp_date_end;
	}
	public String getOtp_flag() {
		return otp_flag;
	}
	public void setOtp_flag(String otp_flag) {
		this.otp_flag = otp_flag;
	}
	public String getOtp_request_id() {
		return otp_request_id;
	}
	public void setOtp_request_id(String otp_request_id) {
		this.otp_request_id = otp_request_id;
	}
	public String getOtp_sms_result() {
		return otp_sms_result;
	}
	public void setOtp_sms_result(String otp_sms_result) {
		this.otp_sms_result = otp_sms_result;
	}
	@Override
	public String toString() {
		return "OTPPaypro [" + (otp_id != null ? "otp_id=" + otp_id + ", " : "")
				+ (otp_msisdn != null ? "otp_msisdn=" + otp_msisdn + ", " : "")
				+ (otp_sessid != null ? "otp_sessid=" + otp_sessid + ", " : "")
				+ (otp_date_start != null ? "otp_date_start=" + otp_date_start + ", " : "")
				+ (otp_date_end != null ? "otp_date_end=" + otp_date_end + ", " : "")
				+ (otp_flag != null ? "otp_flag=" + otp_flag + ", " : "")
				+ (otp_request_id != null ? "otp_request_id=" + otp_request_id + ", " : "")
				+ (otp_sms_result != null ? "otp_sms_result=" + otp_sms_result : "") + "]";
	}
}
