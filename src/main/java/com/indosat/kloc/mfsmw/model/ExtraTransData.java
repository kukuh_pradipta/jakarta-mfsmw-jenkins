/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 18, 2014 
 * Time       : 11:58:29 AM 
 */
package com.indosat.kloc.mfsmw.model;

public class ExtraTransData {

	private int id;
	private String userId;
	private int extType;
	private String paramName;
	private String extKey;
	private String extValue;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getExtType() {
		return extType;
	}
	public void setExtType(int extType) {
		this.extType = extType;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public String getExtKey() {
		return extKey;
	}
	public void setExtKey(String extKey) {
		this.extKey = extKey;
	}
	public String getExtValue() {
		return extValue;
	}
	public void setExtValue(String extValue) {
		this.extValue = extValue;
	}
	
	
	
}
