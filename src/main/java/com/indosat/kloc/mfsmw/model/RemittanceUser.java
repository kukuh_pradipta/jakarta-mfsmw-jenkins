/**
 * Project    : One Bill
 * Created By : Megi Jaka Permana
 * Date       : May 26, 2016 
 * Time       : 8:38:38 AM 
 */
package com.indosat.kloc.mfsmw.model;

public class RemittanceUser {
	
	private int id;
	private String msisdn;
	private Float savingAmount;
	private Float mainAmount;
	
	public RemittanceUser() {
		super();
	}
	
	public RemittanceUser(int id, String msisdn, Float savingAmount, Float mainAmount) {
		super();
		this.id = id;
		this.msisdn = msisdn;
		this.savingAmount = savingAmount;
		this.mainAmount = mainAmount;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public Float getSavingAmount() {
		return savingAmount;
	}
	public void setSavingAmount(Float savingAmount) {
		this.savingAmount = savingAmount;
	}
	public Float getMainAmount() {
		return mainAmount;
	}
	public void setMainAmount(Float mainAmount) {
		this.mainAmount = mainAmount;
	}

	@Override
	public String toString() {
		return "RemmitanceUser [id=" + id + ", msisdn=" + msisdn + ", savingAmount=" + savingAmount + ", mainAmount="
				+ mainAmount + "]";
	}
	
	
}
