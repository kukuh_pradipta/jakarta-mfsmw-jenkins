/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 23, 2015 
 * Time       : 9:46:38 AM 
 */
package com.indosat.kloc.mfsmw.model;

public class KycRawData {

	private int id;
	private int msisdn;
	private String name;
	private String idNo;
	private String motherMaidenName;
	private String dob;
	private int gender;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(int msisdn) {
		this.msisdn = msisdn;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIdNo() {
		return idNo;
	}
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	public String getMotherMaidenName() {
		return motherMaidenName;
	}
	public void setMotherMaidenName(String motherMaidenName) {
		this.motherMaidenName = motherMaidenName;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	
	@Override
	public String toString() {
		return "KycRawData [id=" + id + ", msisdn=" + msisdn + ", name=" + name
				+ ", idNo=" + idNo + ", motherMaidenName=" + motherMaidenName
				+ ", dob=" + dob + ", gender=" + gender + "]";
	}
	
}
