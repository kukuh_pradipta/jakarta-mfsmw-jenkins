/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 22, 2017 
 * Time       : 9:38:33 PM 
 */
package com.indosat.kloc.mfsmw.model;

public class Referral {
	private String id;
	private String sender;
	private String receiver;
	private String referralCode;
	private String deviceID;
	
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	@Override
	public String toString() {
		return "Referral [id=" + id + ", sender=" + sender + ", receiver=" + receiver + ", referralCode=" + referralCode
				+ "]";
	}
	
}