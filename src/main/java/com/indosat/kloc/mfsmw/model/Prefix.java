/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 6, 2014 
 * Time       : 12:09:42 PM 
 */
package com.indosat.kloc.mfsmw.model;

public class Prefix {

	private int id;
	private String name;
	private String prefixNo;
	private String cardNo;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrefixNo() {
		return prefixNo;
	}
	public void setPrefixNo(String prefixNo) {
		this.prefixNo = prefixNo;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
	
	
}
