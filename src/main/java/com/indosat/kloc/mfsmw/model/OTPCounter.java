/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jan 10, 2015 
 * Time       : 9:53:06 PM 
 */
package com.indosat.kloc.mfsmw.model;

public class OTPCounter {

	private String username;
	private int counter;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	
	
	
}
