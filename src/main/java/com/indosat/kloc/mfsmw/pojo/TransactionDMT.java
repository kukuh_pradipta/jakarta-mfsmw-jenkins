/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jun 2, 2015 
 * Time       : 8:01:25 PM 
 */
package com.indosat.kloc.mfsmw.pojo;

public class TransactionDMT {

	private String transId;
	private String extRef;
	private String trxTime;
	private String amount;
	private String couponId;
	private int status;
	
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getExtRef() {
		return extRef;
	}
	public void setExtRef(String extRef) {
		this.extRef = extRef;
	}
	public String getTrxTime() {
		return trxTime;
	}
	public void setTrxTime(String trxTime) {
		this.trxTime = trxTime;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getCouponId() {
		return couponId;
	}
	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}
	
	@Override
	public String toString() {
		return "TransactionDMT [transId=" + transId + ", extRef=" + extRef + ", trxTime=" + trxTime + ", amount="
				+ amount + ", couponId=" + couponId + ", status=" + status + "]";
	}
	
	
	
}
