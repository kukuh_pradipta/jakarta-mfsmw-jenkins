/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : May 4, 2015 
 * Time       : 12:27:32 PM 
 */
package com.indosat.kloc.mfsmw.pojo;

public class DMTTransaction {

	private int id;
	private String parentId;
	private String storeName;
	private String transId;
	private String extRef;
	private String initiator;
	private String couponId;
	private String recipientPhoneNumber;
	private String recipientName;
	private String recipientSourceOfFund;
	private String recipientPurpose;
	private String recipientAddress;
	private String recipientProvince;
	private String recipientCity;
	private String senderName;
	private String senderPhoneNumber;
	private String senderIdType;
	private String senderIdNo;
	private String amount;
	private String fee;
	private String total;
	private int status;
	private String statusMessage;
	private String trxTime;
	private int type;
	private long respTime;
	private String uniqueCode;
	private String note;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getInitiator() {
		return initiator;
	}
	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}
	public String getCouponId() {
		return couponId;
	}
	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}
	public String getRecipientPhoneNumber() {
		return recipientPhoneNumber;
	}
	public void setRecipientPhoneNumber(String recipientPhoneNumber) {
		this.recipientPhoneNumber = recipientPhoneNumber;
	}
	public String getRecipientName() {
		return recipientName;
	}
	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}
	public String getRecipientSourceOfFund() {
		return recipientSourceOfFund;
	}
	public void setRecipientSourceOfFund(String recipientSourceOfFund) {
		this.recipientSourceOfFund = recipientSourceOfFund;
	}
	public String getRecipientPurpose() {
		return recipientPurpose;
	}
	public void setRecipientPurpose(String recipientPurpose) {
		this.recipientPurpose = recipientPurpose;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getSenderPhoneNumber() {
		return senderPhoneNumber;
	}
	public void setSenderPhoneNumber(String senderPhoneNumber) {
		this.senderPhoneNumber = senderPhoneNumber;
	}
	
	public String getSenderIdNo() {
		return senderIdNo;
	}
	public void setSenderIdNo(String senderIdNo) {
		this.senderIdNo = senderIdNo;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public String getTrxTime() {
		return trxTime;
	}
	public void setTrxTime(String trxTime) {
		this.trxTime = trxTime;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getSenderIdType() {
		return senderIdType;
	}
	public void setSenderIdType(String senderIdType) {
		this.senderIdType = senderIdType;
	}
	public String getExtRef() {
		return extRef;
	}
	public void setExtRef(String extRef) {
		this.extRef = extRef;
	}
	public String getRecipientProvince() {
		return recipientProvince;
	}
	public void setRecipientProvince(String recipientProvince) {
		this.recipientProvince = recipientProvince;
	}
	public String getRecipientCity() {
		return recipientCity;
	}
	public void setRecipientCity(String recipientCity) {
		this.recipientCity = recipientCity;
	}
	public String getRecipientAddress() {
		return recipientAddress;
	}
	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public long getRespTime() {
		return respTime;
	}
	public void setRespTime(long respTime) {
		this.respTime = respTime;
	}
	public String getUniqueCode() {
		return uniqueCode;
	}
	public void setUniqueCode(String uniqueCode) {
		this.uniqueCode = uniqueCode;
	}
	
	
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	@Override
	public String toString() {
		return "DMTTransaction [id=" + id + ", parentId=" + parentId + ", storeName=" + storeName + ", transId="
				+ transId + ", extRef=" + extRef + ", initiator=" + initiator + ", couponId=" + couponId
				+ ", recipientPhoneNumber=" + recipientPhoneNumber + ", recipientName=" + recipientName
				+ ", recipientSourceOfFund=" + recipientSourceOfFund + ", recipientPurpose=" + recipientPurpose
				+ ", recipientAddress=" + recipientAddress + ", recipientProvince=" + recipientProvince
				+ ", recipientCity=" + recipientCity + ", senderName=" + senderName + ", senderPhoneNumber="
				+ senderPhoneNumber + ", senderIdType=" + senderIdType + ", senderIdNo=" + senderIdNo + ", amount="
				+ amount + ", fee=" + fee + ", total=" + total + ", status=" + status + ", statusMessage="
				+ statusMessage + ", trxTime=" + trxTime + ", type=" + type + ", respTime=" + respTime
				+ ", uniqueCode=" + uniqueCode + ", note=" + note + "]";
	}
	
	
	
}
