/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Apr 8, 2015 
 * Time       : 4:30:05 PM 
 */
package com.indosat.kloc.mfsmw.pojo;


public class CouponDetails {

	private String couponId;
	private String idType;
	private String identityNo;
	private String senderPhoneNumber;
	private String senderName;
	private String recipientName;
	private String recipientPhoneNumber;
	private String amount;
	private int couponTransId;
	
	public String getCouponId() {
		return couponId;
	}
	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getRecipientName() {
		return recipientName;
	}
	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public int getCouponTransId() {
		return couponTransId;
	}
	public void setCouponTransId(int couponTransId) {
		this.couponTransId = couponTransId;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getSenderPhoneNumber() {
		return senderPhoneNumber;
	}
	public void setSenderPhoneNumber(String senderPhoneNumber) {
		this.senderPhoneNumber = senderPhoneNumber;
	}
	public String getRecipientPhoneNumber() {
		return recipientPhoneNumber;
	}
	public void setRecipientPhoneNumber(String recipientPhoneNumber) {
		this.recipientPhoneNumber = recipientPhoneNumber;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	@Override
	public String toString() {
		return "CouponDetails [couponId=" + couponId + ", idType=" + idType
				+ ", identityNo=" + identityNo + ", senderPhoneNumber="
				+ senderPhoneNumber + ", senderName=" + senderName
				+ ", recipientName=" + recipientName
				+ ", recipientPhoneNumber=" + recipientPhoneNumber
				+ ", amount=" + amount + ", couponTransId=" + couponTransId
				+ "]";
	}
	
	
}
