/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 17, 2014 
 * Time       : 6:41:35 PM 
 */
package com.indosat.kloc.mfsmw.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "common_response")
public class CommonResponse {

	
	private Integer responseCode;
	private String responseMessage;
	private Integer trxid;
	private TransactionInfo transactionInfo;
	
	public Integer getResponseCode() {
		return responseCode;
	}
	
	@XmlElement(name="response_code")
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}
	@XmlElement(name="response_message")
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public Integer getTrxid() {
		return trxid;
	}
	public void setTrxid(Integer trxid) {
		this.trxid = trxid;
	}
	
	@XmlElement(name="trx_info")
	public TransactionInfo getTransactionInfo() {
		return transactionInfo;
	}
	public void setTransactionInfo(TransactionInfo transactionInfo) {
		this.transactionInfo = transactionInfo;
	}
	
	
	
}
