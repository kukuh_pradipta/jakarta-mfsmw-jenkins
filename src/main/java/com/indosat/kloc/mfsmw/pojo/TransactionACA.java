/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Nov 9, 2015 
 * Time       : 9:32:45 AM 
 */
package com.indosat.kloc.mfsmw.pojo;

public class TransactionACA {
	
	private String tglRespon;
	private String response;
	private String responseDesc;
	private String kdProduk;
	private String amount;
	private String trasID;
	private String kdOutlet;
	private String pin;
	private String serialNo;
	private String prodDesc;
	private String supportInfo;
	
	public TransactionACA() {
		super();
	}

	public String getTglRespon() {
		return tglRespon;
	}

	public void setTglRespon(String tglRespon) {
		this.tglRespon = tglRespon;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

	public String getKdProduk() {
		return kdProduk;
	}

	public void setKdProduk(String kdProduk) {
		this.kdProduk = kdProduk;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTrasID() {
		return trasID;
	}

	public void setTrasID(String trasID) {
		this.trasID = trasID;
	}

	public String getKdOutlet() {
		return kdOutlet;
	}

	public void setKdOutlet(String kdOutlet) {
		this.kdOutlet = kdOutlet;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getProdDesc() {
		return prodDesc;
	}

	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}

	public String getSupportInfo() {
		return supportInfo;
	}

	public void setSupportInfo(String supportInfo) {
		this.supportInfo = supportInfo;
	}
}
