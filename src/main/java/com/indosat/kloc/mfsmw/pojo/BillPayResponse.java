/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Sep 5, 2014 
 * Time       : 10:24:02 AM 
 */
package com.indosat.kloc.mfsmw.pojo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "billpay")
public class BillPayResponse {

	private int transactionId;
	private String sessionId;
	private String to;
	private String destinationName;
	private String amount;
	private String price;
	private String adm;
	private boolean billpayQueryState;
	private boolean billpayState;
	private int errorCode;
	
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getDestinationName() {
		return destinationName;
	}
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getAdm() {
		return adm;
	}
	public void setAdm(String adm) {
		this.adm = adm;
	}
	public boolean isBillpayQueryState() {
		return billpayQueryState;
	}
	public void setBillpayQueryState(boolean billpayQueryState) {
		this.billpayQueryState = billpayQueryState;
	}
	public boolean isBillpayState() {
		return billpayState;
	}
	public void setBillpayState(boolean billpayState) {
		this.billpayState = billpayState;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	
	
}
