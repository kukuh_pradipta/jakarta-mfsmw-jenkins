/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 17, 2014 
 * Time       : 6:38:46 PM 
 */
package com.indosat.kloc.mfsmw.pojo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "trx_info")
public class TransactionInfo {

	String transid;
	
	String name;
	int referenceId;
	String category;

	public String getTransid() {
		return transid;
	}

	public void setTransid(String transid) {
		this.transid = transid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(int referenceId) {
		this.referenceId = referenceId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	
}
