/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Nov 5, 2015 
 * Time       : 5:55:24 AM 
 */
package com.indosat.kloc.mfsmw.pojo;

public class TransactionArtajasa {

	private Long transTime;
	private String transID;
	private String transType;
	private String msisdn;
	private String accId;
	private String accTarget;
	private String amount;
	private String rc;
	private String drkc;
	private String issname;
	private String destBankName;
	private String destBankId;
	private String destAccName;
	private String rrn;
	private String stan;
	private String dkutranid;
	
	public Long getTransTime() {
		return transTime;
	}
	public void setTransTime(Long transTime) {
		this.transTime = transTime;
	}
	public String getTransID() {
		return transID;
	}
	public void setTransID(String transID) {
		this.transID = transID;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getIssname() {
		return issname;
	}
	public void setIssname(String issname) {
		this.issname = issname;
	}
	public String getRrn() {
		return rrn;
	}
	public void setRrn(String rrn) {
		this.rrn = rrn;
	}
	public String getStan() {
		return stan;
	}
	public void setStan(String stan) {
		this.stan = stan;
	}
	public String getDkutranid() {
		return dkutranid;
	}
	public void setDkutranid(String dkutranid) {
		this.dkutranid = dkutranid;
	}
	public String getAccId() {
		return accId;
	}
	public void setAccId(String accId) {
		this.accId = accId;
	}
	public String getAccTarget() {
		return accTarget;
	}
	public void setAccTarget(String accTarget) {
		this.accTarget = accTarget;
	}
	public String getRc() {
		return rc;
	}
	public void setRc(String rc) {
		this.rc = rc;
	}
	public String getDrkc() {
		return drkc;
	}
	public void setDrkc(String drkc) {
		this.drkc = drkc;
	}
	public String getDestBankName() {
		return destBankName;
	}
	public void setDestBankName(String destBankName) {
		this.destBankName = destBankName;
	}
	public String getDestBankId() {
		return destBankId;
	}
	public void setDestBankId(String destBankId) {
		this.destBankId = destBankId;
	}
	public String getDestAccName() {
		return destAccName;
	}
	public void setDestAccName(String destAccName) {
		this.destAccName = destAccName;
	}
	
}
