/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Feb 20, 2015 
 * Time       : 6:01:16 PM 
 */
package com.indosat.kloc.mfsmw.pojo;

public class TransactionMail {

	private String transactionTypeINA;
	private String transactionTypeEN;
	private String destination;
	private String amount;
	private String transId;
	private int status;
	
	
	public String getTransactionTypeINA() {
		return transactionTypeINA;
	}
	public void setTransactionTypeINA(String transactionTypeINA) {
		this.transactionTypeINA = transactionTypeINA;
	}
	public String getTransactionTypeEN() {
		return transactionTypeEN;
	}
	public void setTransactionTypeEN(String transactionTypeEN) {
		this.transactionTypeEN = transactionTypeEN;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "TransactionMail [transactionTypeINA=" + transactionTypeINA
				+ ", transactionTypeEN=" + transactionTypeEN + ", destination="
				+ destination + ", amount=" + amount + ", transId=" + transId
				+ ", status=" + status + "]";
	}
	
}
