/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 5, 2015 
 * Time       : 11:15:31 AM 
 */
package com.indosat.kloc.mfsmw.pojo;

public class AuthResponse {

	private int status;
	private String msg;
	private String initiator;
	private String password;
	private String sessionId;
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public int getStatus() {
		return status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getInitiator() {
		return initiator;
	}
	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "AuthResponse [status=" + status + ", msg=" + msg + ", initiator=" + initiator + ", password=" + password
				+ ", sessionId=" + sessionId + "]";
	}
	
	
	
}
