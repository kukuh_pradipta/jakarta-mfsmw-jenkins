/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 16, 2015 
 * Time       : 4:41:02 PM 
 */
package com.indosat.kloc.mfsmw.pojo;

public class MBillerRequest {

	private String url;
	private String initiator; 
	private String function;
	private String to;
	private String amount;
	private String price;
	private String transId; 
	private String extRef;
	private String productId;
	private String signature;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getInitiator() {
		return initiator;
	}
	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getExtRef() {
		return extRef;
	}
	public void setExtRef(String extRef) {
		this.extRef = extRef;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	@Override
	public String toString() {
		return "MBillerRequest [url=" + url + ", initiator=" + initiator + ", function=" + function + ", to=" + to
				+ ", amount=" + amount + ", price=" + price + ", transId=" + transId + ", extRef=" + extRef
				+ ", productId=" + productId + ", signature=XXX"+"]";
	}
	
}
