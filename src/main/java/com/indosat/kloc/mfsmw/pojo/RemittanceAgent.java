/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jun 3, 2015 
 * Time       : 9:48:11 AM 
 */
package com.indosat.kloc.mfsmw.pojo;

public class RemittanceAgent {

	private String id;
	private String initiator;
	private String storeId;
	private String name;
	private String address;
	private String province;
	private String city;
	private String phoneNumber;
	private String status;
	private String createdTime;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInitiator() {
		return initiator;
	}
	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	
	@Override
	public String toString() {
		return "RemittanceAgent [id=" + id + ", initiator=" + initiator
				+ ", storeId=" + storeId + ", name=" + name + ", address="
				+ address + ", province=" + province + ", city=" + city
				+ ", phoneNumber=" + phoneNumber + ", status=" + status
				+ ", createdTime=" + createdTime + "]";
	}
	
	
	
}
