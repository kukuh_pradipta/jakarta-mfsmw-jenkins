/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Apr 2, 2015 
 * Time       : 1:45:41 PM 
 */
package com.indosat.kloc.mfsmw.pojo;

import java.util.HashMap;

public class Parameter {

	private HashMap<String, String> keyValueMap;

	public HashMap<String, String> getKeyValueMap() {
		return keyValueMap;
	}

	public void setKeyValueMap(HashMap<String, String> keyValueMap) {
		this.keyValueMap = keyValueMap;
	}

	@Override
	public String toString() {
		return "Parameter [keyValueMap=" + keyValueMap + "]";
	}
	
}
