/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Jan 30, 2015 
 * Time       : 12:24:35 AM 
 */
package com.indosat.kloc.mfsmw.pojo;

import org.jpos.util.Log;

public class AgentData {

	private String firstName;
	private String lastName;
	private String profilePic;
	private String idPhoto;
	private String idPhotoSelfie;
	private String email;
	
	private String idNo;
	private String idType;
	private String address;
	private String dateOfBirth;
	private String gender;
	private String motherMaidenName;
	private String smsAddress;
	private String accountId;
	
	//New From ektp
	private String religion; 
	private String bloodType;
	private String city;
	private String kecamatan;
	private String kelurahan;
	private String postalCode;
	private String occupation;
	private String lastEducation;
	private String province;
	private String placeOfBirth;
	private String no_rt;
	private String no_rw;
	private String no_kk;
	private String countrycode;
	
	private String walletGrouping;
	private int walletType;

	private boolean isWaitingForKyc;
	
	private boolean isAllowToKyc;
	
	private boolean isOutlet;
	
	private String category;
	
	public String getIdNo() {
		return idNo;
	}
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMotherMaidenName() {
		return motherMaidenName;
	}
	public void setMotherMaidenName(String motherMaidenName) {
		this.motherMaidenName = motherMaidenName;
	}
	public String getSmsAddress() {
		return smsAddress;
	}
	public void setSmsAddress(String smsAddress) {
		this.smsAddress = smsAddress;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getProfilePic() {
		return profilePic;
	}
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	public String getIdPhoto() {
		return idPhoto;
	}
	public void setIdPhoto(String idPhoto) {
		this.idPhoto = idPhoto;
	}
	
	public int getWalletType() {
		return walletType;
	}
	public void setWalletType(int walletType) {
		this.walletType = walletType;
	}
	public boolean isWaitingForKyc() {
		return isWaitingForKyc;
	}
	public void setWaitingForKyc(boolean isWaitingForKyc) {
		this.isWaitingForKyc = isWaitingForKyc;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isAllowToKyc() {
		return isAllowToKyc;
	}
	public void setAllowToKyc(boolean isAllowToKyc) {
		this.isAllowToKyc = isAllowToKyc;
	}
	public boolean isOutlet() {
		return isOutlet;
	}
	public void setOutlet(boolean isOutlet) {
		this.isOutlet = isOutlet;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	//New GetterSetter from eKTP
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getBloodType() {
		return bloodType;
	}
	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getKecamatan() {
		return kecamatan;
	}
	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}
	public String getKelurahan() {
		return kelurahan;
	}
	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getLastEducation() {
		return lastEducation;
	}
	public void setLastEducation(String lastEducation) {
		this.lastEducation = lastEducation;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getWalletGrouping() {
		return walletGrouping;
	}
	public void setWalletGrouping(String walletGrouping) {
		this.walletGrouping = walletGrouping;
	}
	
	public String getNo_rt() {
		return no_rt;
	}
	public void setNo_rt(String no_rt) {
		this.no_rt = no_rt;
	}
	public String getNo_rw() {
		return no_rw;
	}
	public void setNo_rw(String no_rw) {
		this.no_rw = no_rw;
	}
	public String getNo_kk() {
		return no_kk;
	}
	public void setNo_kk(String no_kk) {
		this.no_kk = no_kk;
	}
	public String getCountrycode() {
		return countrycode;
	}
	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}
	public String getIdPhotoSelfie() {
		return idPhotoSelfie;
	}
	public void setIdPhotoSelfie(String idPhotoSelfie) {
		this.idPhotoSelfie = idPhotoSelfie;
	}
	@Override
	public String toString() {
		return "AgentData [" + (firstName != null ? "firstName=" + firstName + ", " : "")
				+ (lastName != null ? "lastName=" + lastName + ", " : "")
				+ (profilePic != null ? "profilePic=" + profilePic + ", " : "")
				+ (idPhoto != null ? "idPhoto=" + idPhoto + ", " : "")
				+ (idPhotoSelfie != null ? "idPhotoSelfie=" + idPhotoSelfie + ", " : "")
				+ (email != null ? "email=" + email + ", " : "") + (idNo != null ? "idNo=" + idNo + ", " : "")
				+ (idType != null ? "idType=" + idType + ", " : "")
				+ (address != null ? "address=" + address + ", " : "")
				+ (dateOfBirth != null ? "dateOfBirth=" + dateOfBirth + ", " : "")
				+ (gender != null ? "gender=" + gender + ", " : "")
				+ (motherMaidenName != null ? "motherMaidenName=" + motherMaidenName + ", " : "")
				+ (smsAddress != null ? "smsAddress=" + smsAddress + ", " : "")
				+ (accountId != null ? "accountId=" + accountId + ", " : "")
				+ (religion != null ? "religion=" + religion + ", " : "")
				+ (bloodType != null ? "bloodType=" + bloodType + ", " : "")
				+ (city != null ? "city=" + city + ", " : "")
				+ (kecamatan != null ? "kecamatan=" + kecamatan + ", " : "")
				+ (kelurahan != null ? "kelurahan=" + kelurahan + ", " : "")
				+ (postalCode != null ? "postalCode=" + postalCode + ", " : "")
				+ (occupation != null ? "occupation=" + occupation + ", " : "")
				+ (lastEducation != null ? "lastEducation=" + lastEducation + ", " : "")
				+ (province != null ? "province=" + province + ", " : "")
				+ (placeOfBirth != null ? "placeOfBirth=" + placeOfBirth + ", " : "")
				+ (no_rt != null ? "no_rt=" + no_rt + ", " : "") + (no_rw != null ? "no_rw=" + no_rw + ", " : "")
				+ (no_kk != null ? "no_kk=" + no_kk + ", " : "")
				+ (countrycode != null ? "countrycode=" + countrycode + ", " : "")
				+ (walletGrouping != null ? "walletGrouping=" + walletGrouping + ", " : "") + "walletType=" + walletType
				+ ", isWaitingForKyc=" + isWaitingForKyc + ", isAllowToKyc=" + isAllowToKyc + ", isOutlet=" + isOutlet
				+ ", " + (category != null ? "category=" + category : "") + "]";
	}
	
	
	
}
