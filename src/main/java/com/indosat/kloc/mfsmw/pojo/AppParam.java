/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 5, 2015 
 * Time       : 2:09:51 PM 
 */
package com.indosat.kloc.mfsmw.pojo;

public class AppParam {

	private String compCardURL;
	private String compCardGenericXML;
	private String compCardDeliveryReqXML;
	private String compCardCardStatusActive;
	private String compCardSMSTextRequestCardSuccess;
	private String compCardSMSTextActivationSuccess;
	private String compCardSMSTextDeliveryStatusSuccess;
	private String compCardSMSTextDeactivationSuccess;
	private String compCardSMSTextGenericError;
	
	private String ajDplusInqXML;
	private String ajDplusTransXML;
	private String ajDplusURL;
	private String ajAesKey;
	
	private String eKTPURL;
	private String eKTPUser;
	private String eKTPPass;
	private String eKTPMsisdnDefault;
	
	private String onebillURL;
	
	public String getCompCardCardStatusActive() {
		return compCardCardStatusActive;
	}
	public void setCompCardCardStatusActive(String compCardCardStatusActive) {
		this.compCardCardStatusActive = compCardCardStatusActive;
	}
	public String getCompCardURL() {
		return compCardURL;
	}
	public void setCompCardURL(String compCardURL) {
		this.compCardURL = compCardURL;
	}
	public String getCompCardGenericXML() {
		return compCardGenericXML;
	}
	public void setCompCardGenericXML(String compCardGenericXML) {
		this.compCardGenericXML = compCardGenericXML;
	}
	public String getCompCardDeliveryReqXML() {
		return compCardDeliveryReqXML;
	}
	public void setCompCardDeliveryReqXML(String compCardDeliveryReqXML) {
		this.compCardDeliveryReqXML = compCardDeliveryReqXML;
	}
	
	public String getCompCardSMSTextRequestCardSuccess() {
		return compCardSMSTextRequestCardSuccess;
	}
	public void setCompCardSMSTextRequestCardSuccess(
			String compCardSMSTextRequestCardSuccess) {
		this.compCardSMSTextRequestCardSuccess = compCardSMSTextRequestCardSuccess;
	}
	public String getCompCardSMSTextActivationSuccess() {
		return compCardSMSTextActivationSuccess;
	}
	public void setCompCardSMSTextActivationSuccess(
			String compCardSMSTextActivationSuccess) {
		this.compCardSMSTextActivationSuccess = compCardSMSTextActivationSuccess;
	}
	public String getCompCardSMSTextDeactivationSuccess() {
		return compCardSMSTextDeactivationSuccess;
	}
	public void setCompCardSMSTextDeactivationSuccess(
			String compCardSMSTextDeactivationSuccess) {
		this.compCardSMSTextDeactivationSuccess = compCardSMSTextDeactivationSuccess;
	}
	public String getCompCardSMSTextGenericError() {
		return compCardSMSTextGenericError;
	}
	public void setCompCardSMSTextGenericError(String compCardSMSTextGenericError) {
		this.compCardSMSTextGenericError = compCardSMSTextGenericError;
	}
	public String getCompCardSMSTextDeliveryStatusSuccess() {
		return compCardSMSTextDeliveryStatusSuccess;
	}
	public void setCompCardSMSTextDeliveryStatusSuccess(
			String compCardSMSTextDeliveryStatusSuccess) {
		this.compCardSMSTextDeliveryStatusSuccess = compCardSMSTextDeliveryStatusSuccess;
	}
	
	//Added By Ryanbhuled
	//===========Artajasa DPlus============
	public String getAjDplusURL() {
		return ajDplusURL;
	}
	public void setAjDplusURL(String ajDplusURL) {
		this.ajDplusURL = ajDplusURL;
	}
	public void setAjDplusInqXML(String ajDplusInqXML) {
		this.ajDplusInqXML = ajDplusInqXML;
	}
	public String getAjDplusInqXML() {
		return ajDplusInqXML;
	}
	public String getAjDplusTransXML() {
		return ajDplusTransXML;
	}
	public void setAjDplusTransXML(String ajDplusTransXML) {
		this.ajDplusTransXML = ajDplusTransXML;
	}
	public String getAjAesKey() {
		return ajAesKey;
	}
	public void setAjAesKey(String ajAesKey) {
		this.ajAesKey = ajAesKey;
	}
	
	//===========E-KTP============
	public String geteKTPURL() {
		return eKTPURL;
	}
	public void seteKTPURL(String eKTPURL) {
		this.eKTPURL = eKTPURL;
	}
	public String geteKTPUser() {
		return eKTPUser;
	}
	public void seteKTPUser(String eKTPUser) {
		this.eKTPUser = eKTPUser;
	}
	public String geteKTPPass() {
		return eKTPPass;
	}
	public void seteKTPPass(String eKTPPass) {
		this.eKTPPass = eKTPPass;
	}
	public String geteKTPMsisdnDefault() {
		return eKTPMsisdnDefault;
	}
	public void seteKTPMsisdnDefault(String eKTPMsisdnDefault) {
		this.eKTPMsisdnDefault = eKTPMsisdnDefault;
	}
	//===========OneBill=========
	public String getOnebillURL() {
		return onebillURL;
	}
	public void setOnebillURL(String onebillURL) {
		this.onebillURL = onebillURL;
	}
	
	
}
