/**
 * Project    : Mobile Financial Service Middleware  
 * Created By : Megi Jaka Permana
 * Date       : Aug 14, 2014 
 * Time       : 2:15:20 PM 
 */
package com.indosat.kloc.mfsmw.pojo;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@XmlRootElement(name = "result")
public class Response {

	private Integer status;
	private String extRef;
	private String trxid;
	private String msg;
	
	private String couponId;
	private String balance;
	private String price;
	private String name;
	private String msisdn;
	private String fee;
	private String voucherCode;
	private String periode;
	private String detailTagihan;
	private String amount;
	private String resultNameSpace;
	
	private BigInteger accountId;
	
	private String [] coupon;
	private String counter;
	
	private boolean billpayQueryState;
	private boolean billpayState;
	private CommonResponse commonResponse;
	private List<LastTransaction> trxList;

	private AgentData agentData;
	private CouponDetails couponDetails;
	private RemittanceUser remittanceUser;
	private TransactionArtajasa artaJasaTrans;
	private TransactionACA acaTrans;
	
	private HashMap<String,String> detailTrx;
	

	public CouponDetails getCouponDetails() {
		if(couponDetails == null)
			couponDetails = new CouponDetails();
		return couponDetails;
	}
	public void setCouponDetails(CouponDetails couponDetails) {
		this.couponDetails = couponDetails;
	}
	
	public String getCounter() {
		return counter;
	}
	
	public void setCounter(String counter) {
		this.counter = counter;
	}
	public BigInteger getAccountId() {
		return accountId;
	}
	public void setAccountId(BigInteger accountId) {
		this.accountId = accountId;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getExtRef() {
		return extRef;
	}
	public void setExtRef(String extRef) {
		this.extRef = extRef;
	}
	public String getTrxid() {
		return trxid;
	}
	public void setTrxid(String trxid) {
		this.trxid = trxid;
	}
	
	public String getCouponId() {
		return couponId;
	}
	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	@XmlTransient
	public boolean isBillpayQueryState() {
		return billpayQueryState;
	}
	public void setBillpayQueryState(boolean billpayQueryState) {
		this.billpayQueryState = billpayQueryState;
	}
	@XmlTransient
	public boolean isBillpayState() {
		return billpayState;
	}
	public void setBillpayState(boolean billpayState) {
		this.billpayState = billpayState;
	}
	@XmlElement(name="common_response")
	public CommonResponse getCommonResponse() {
		return commonResponse;
	}
	public void setCommonResponse(CommonResponse commonResponse) {
		this.commonResponse = commonResponse;
	}
	
	public List<LastTransaction> getTrxList() {
		return trxList;
	}
	public void setTrxList(List<LastTransaction> trxList) {
		this.trxList = trxList;
	}
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}
	
	public String[] getCoupon() {
		return coupon;
	}
	public void setCoupon(String[] coupon) {
		this.coupon = coupon;
	}
	
	public AgentData getAgentData() {
		if(agentData == null)
			agentData = new AgentData();
		return agentData;
	}
	public void setAgentData(AgentData agentData) {
		this.agentData = agentData;
	}
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	public String getDetailTagihan() {
		return detailTagihan;
	}
	public void setDetailTagihan(String detailTagihan) {
		this.detailTagihan = detailTagihan;
	}
	
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getResultNameSpace() {
		return resultNameSpace;
	}
	public void setResultNameSpace(String resultNameSpace) {
		this.resultNameSpace = resultNameSpace;
	}
	public RemittanceUser getRemittanceUser() {
		if(remittanceUser == null)
			remittanceUser = new RemittanceUser();
		return remittanceUser;
	}
	public void setRemittanceUser(RemittanceUser remittanceUser) {
		this.remittanceUser = remittanceUser;
	}
	
	//Added By Ryanbhuled
	public TransactionArtajasa getArtaJasaTrans() {
		return artaJasaTrans;
	}
	public void setArtaJasaTrans(TransactionArtajasa artaJasaTrans) {
		this.artaJasaTrans = artaJasaTrans;
	}
	public TransactionACA getAcaTrans() {
		return acaTrans;
	}
	public void setAcaTrans(TransactionACA acaTrans) {
		this.acaTrans = acaTrans;
	}
	
	public HashMap<String, String> getDetailTrx() {
		return detailTrx;
	}
	public void setDetailTrx(HashMap<String, String> detailTrx) {
		this.detailTrx = detailTrx;
	}
	
	@Override
	public String toString() {
		return "Response [" + (status != null ? "status=" + status + ", " : "")
				+ (extRef != null ? "extRef=" + extRef + ", " : "") + (trxid != null ? "trxid=" + trxid + ", " : "")
				+ (msg != null ? "msg=" + msg + ", " : "") + (couponId != null ? "couponId=" + couponId + ", " : "")
				+ (balance != null ? "balance=" + balance + ", " : "") + (price != null ? "price=" + price + ", " : "")
				+ (name != null ? "name=" + name + ", " : "") + (msisdn != null ? "msisdn=" + msisdn + ", " : "")
				+ (fee != null ? "fee=" + fee + ", " : "")
				+ (voucherCode != null ? "voucherCode=" + voucherCode + ", " : "")
				+ (periode != null ? "periode=" + periode + ", " : "")
				+ (detailTagihan != null ? "detailTagihan=" + detailTagihan + ", " : "")
				+ (amount != null ? "amount=" + amount + ", " : "")
				+ (resultNameSpace != null ? "resultNameSpace=" + resultNameSpace + ", " : "")
				+ (accountId != null ? "accountId=" + accountId + ", " : "")
				+ (coupon != null ? "coupon=" + Arrays.toString(coupon) + ", " : "")
				+ (counter != null ? "counter=" + counter + ", " : "") + "billpayQueryState=" + billpayQueryState
				+ ", billpayState=" + billpayState + ", "
				+ (commonResponse != null ? "commonResponse=" + commonResponse + ", " : "")
				+ (trxList != null ? "trxList=" + trxList + ", " : "")
				+ (agentData != null ? "agentData=" + agentData + ", " : "")
				+ (couponDetails != null ? "couponDetails=" + couponDetails + ", " : "")
				+ (remittanceUser != null ? "remittanceUser=" + remittanceUser + ", " : "")
				+ (artaJasaTrans != null ? "artaJasaTrans=" + artaJasaTrans + ", " : "")
				+ (acaTrans != null ? "acaTrans=" + acaTrans + ", " : "")
				+ (detailTrx != null ? "detailTrx=" + detailTrx : "") + "]";
	}

}
