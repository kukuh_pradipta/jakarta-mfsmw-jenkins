/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Apr 29, 2015 
 * Time       : 3:19:49 PM 
 */
package com.indosat.kloc.mfsmw.pojo;

public class RemittanceUser {

	private String idNo;
	private String idType;
	private String idExp;
	private String name;
	private String phoneNumber;
	private String gender;
	private String address;
	private String province;
	private String city;
	private String pob;
	private String dob;
	private String occupation;
	private String nationality;
	private String aggreeReg;
	
	public String getIdNo() {
		return idNo;
	}
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getIdExp() {
		return idExp;
	}
	public void setIdExp(String idExp) {
		this.idExp = idExp;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPob() {
		return pob;
	}
	public void setPob(String pob) {
		this.pob = pob;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getAggreeReg() {
		return aggreeReg;
	}
	public void setAggreeReg(String aggreeReg) {
		this.aggreeReg = aggreeReg;
	}
	@Override
	public String toString() {
		return "RemittanceUser [idNo=" + idNo + ", idType=" + idType
				+ ", idExp=" + idExp + ", name=" + name + ", phoneNumber="
				+ phoneNumber + ", gender=" + gender + ", address=" + address
				+ ", province=" + province + ", city=" + city + ", pob=" + pob
				+ ", dob=" + dob + ", occupation=" + occupation
				+ ", nationality=" + nationality + ", aggreeReg=" + aggreeReg
				+ "]";
	}
	
	
	
}
