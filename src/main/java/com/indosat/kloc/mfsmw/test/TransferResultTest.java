/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 16, 2014 
 * Time       : 3:51:19 PM 
 */
package com.indosat.kloc.mfsmw.test;

public class TransferResultTest {

	private String username;
	private long createSession;
	private long login;
	private long transfer;
	private long totalTime;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public long getCreateSession() {
		return createSession;
	}
	public void setCreateSession(long createSession) {
		this.createSession = createSession;
	}
	public long getLogin() {
		return login;
	}
	public void setLogin(long login) {
		this.login = login;
	}
	public long getTransfer() {
		return transfer;
	}
	public void setTransfer(long transfer) {
		this.transfer = transfer;
	}
	public long getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(long totalTime) {
		this.totalTime = totalTime;
	}
	@Override
	public String toString() {
		return "TransferResultTest [username=" + username + ", createSession="
				+ createSession + ", login=" + login + ", transfer=" + transfer
				+ ", totalTime=" + totalTime + "]";
	}
	
	
	
	
}
