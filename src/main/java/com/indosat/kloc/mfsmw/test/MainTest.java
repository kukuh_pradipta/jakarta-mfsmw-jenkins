/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 16, 2014 
 * Time       : 2:54:37 PM 
 */
package com.indosat.kloc.mfsmw.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.soap.SessionHandler;
import com.indosat.kloc.mfsmw.soap.UTIBAHandler;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.Utils;

public class MainTest {

	@Autowired
	private SessionHandler sessionHandler;
	@Autowired
	private UTIBAHandler utibaHandler;
	@Autowired
	private  DataStore dataManager;
	
	static ApiUser users =new ApiUser();
	static List<String> ips = new ArrayList<>();
	static List<String> limits = new ArrayList<>();
	
	public void doTesting(){
		TransferSimulation simulation = new TransferSimulation();
		simulation.setSessionHandler(sessionHandler);
		simulation.setUtibaHandler(utibaHandler);
		
		simulation.run();
	}
	
	public void getDatas(){
//		System.out.println(dataManager);
//		System.out.println(dataManager.getApiUser("mynt_web"));
		users = dataManager.getApiUser("mynt_web");
		ips = dataManager.getIPApiUser("mynt_mobile");
		limits = dataManager.getLimitApiUser("cipika_dompetku_plus");
	}
	
	public static void main(String[] args) {
		
		//ROT 13 Encyptor
//        String s = "middleware";
        String s = "Middleware2014!@#";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if       (c >= 'a' && c <= 'm') c += 13;
            else if  (c >= 'A' && c <= 'M') c += 13;
            else if  (c >= 'n' && c <= 'z') c -= 13;
            else if  (c >= 'N' && c <= 'Z') c -= 13;
            sb.append(c);
        }
        System.out.println("Result:"+sb.toString());
        
        String a = "SERVICEID|9002,TO|10077764594,DENOM|20000,PIN|123456";
        System.out.println(a.split("SERVICEID\\|")[1].split(""));
        
		
//		String namalkp = "7701234567";
//		if (namalkp.split(" ").length > 1) {
//			System.out.println(namalkp.split(" ")[0]);
//			System.out.println(namalkp.split(" ").length);
//			System.out.println(namalkp.replace(namalkp.split(" ")[0]+" ", ""));
//		}
//		System.out.println(namalkp.substring(0,3));
		
//		String s="Sri Rubijantini";
//		String t="Sli Rubiyantini";
//		double max = Math.max(s.length(), t.length());
//		double a = StringUtils.getLevenshteinDistance(s, t);
//		System.out.println(a);
//		System.out.println(max);
//		Double res = (1-(a/max))*100d;
//		System.out.println(res.floatValue());
		
//		Double a = 0.0050215;
//		System.out.println(a.toString());
//		System.out.println(String.valueOf(a.shortValue()));
//		System.out.println(a.);
		
		
		
//		for (int i = 0; i < 10; i++) {
//			Thread a = new Thread(new Runnable() {
//			    public void run() {
//					HttpClientSender httpClientSender = new HttpClientSender();
//					List<MediaType> mediaTypes;
//					MultiValueMap<String, String> paramKTP = new LinkedMultiValueMap<String, String>();
//					mediaTypes = new LinkedList<MediaType>();
//					mediaTypes.add(MediaType.TEXT_HTML);
//					String url = "http://114.4.68.19:8181/mfsmw/webapiinternal/prog_informer";
//					paramKTP.set("msisdn", "085721067790");
//					paramKTP.set("signature", "4JQgTDXJ507KUQ3bf43eiu3tqyxuNdFUjIsvUyNKa5yp5b6eVq4SbA==");
//					paramKTP.set("userid", "web_api_test");
//			    	HttpEntity<String> he = null;
//					he = httpClientSender.sendPost(url, paramKTP, mediaTypes);
//			    }
//			});
//			a.start();
//		}
		
		
		
		
//		ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");	
//		MainTest mainTest = new MainTest();
//		mainTest.doTesting();

//		String json = "{\"content\":[{\"NIK\":3273180407900001,\"NAMA_LGKP\":\"RYAN PERMANA\",\"KAB_NAME\":\"KOTA BANDUNG\",\"AGAMA\":\"ISLAM\",\"NO_RW\":12,\"KEC_NAME\":\"CIBEUNYING KALER\"}],\"lastPage\":true,\"numberOfElements\":1,\"sort\":null,\"totalElements\":1,\"firstPage\":true,\"number\":0,\"size\":1}";
//		System.out.println(json);
//		JsonObject jo = Utils.getJsonData(json);
//		JsonArray ja = jo.get("content").getAsJsonArray();
//		System.out.println(ja.get(0).getAsJsonObject().get("NIK"));
//		
//		String x = "ABDUL MICHAEL SUHARTONO SYALALA";
//		System.out.println(x);
//		System.out.println(x.split(" ")[0]);
//		System.out.println(x.split(" ")[1]);
//		System.out.println(x.split(" ")[2]);
//		System.out.println(x.split(" ")[3]);
		
//		AgentData ad = (AgentData) Utils.convertToXml(source, type);
		
//		MainTest mt = new MainTest();
//		mt.getDatas();
//		System.out.println(users.getPassKey());
		
//		List<String> ips = new ArrayList<String>();
//		for(String ip: "127.0.0.1,localhost,172.16.36.90,10.7.103.234".split(",")){
//			ips.add(ip);
////			System.out.println(ip);
//		}
//		System.out.println(ips.size());
		
//		System.out.println("SERVICEID|9002,TO|14013265880,DENOM|50000,PIN|197012");
//		String x = "SERVICEID|9002,TO|14013265880,DENOM|50000,PIN|197012";
//		String y = x.split(",PIN\\|")[1];
//		System.out.println(x.split(",PIN\\|")[0]);
//		String x = "[1101, 12102, 12104, 180162]";
//		String y = x.replace("[", "").replace("]", "");
//		for (String group : y.split(",")) {
//			System.out.println(group.trim());
//		}
//		HashMap<String,String> a = new HashMap<String,String>();
//		a.put("kode_toko", "outlet_1");
////		a.put("amount", "10000");
//		System.out.println(Utils.convertToJSON(a));
		
		
	}
	
	
}
