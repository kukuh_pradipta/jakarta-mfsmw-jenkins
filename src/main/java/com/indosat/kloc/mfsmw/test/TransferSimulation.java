/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 16, 2014 
 * Time       : 2:47:08 PM 
 */
package com.indosat.kloc.mfsmw.test;

import java.util.ArrayList;
import java.util.List;

import com.indosat.kloc.mfsmw.soap.SessionHandler;
import com.indosat.kloc.mfsmw.soap.UTIBAHandler;

public class TransferSimulation implements Runnable{

	private SessionHandler sessionHandler;
	private UTIBAHandler utibaHandler;
	
	List<TransferResultTest> result = new ArrayList<TransferResultTest>();
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		List<String> userList = getUserlist();
		
		for(String obj:userList){
			TransferResultTest resultTest = new TransferResultTest();
			resultTest.setUsername(obj);
			long m1 = System.currentTimeMillis();
			String sessionId = sessionHandler.getSessionId();
			resultTest.setCreateSession(System.currentTimeMillis()-m1);
			
			long mval = System.currentTimeMillis();
			sessionHandler.validateSession(sessionId, obj, "123456");
			resultTest.setLogin(System.currentTimeMillis()-mval);
			
			long mt =  System.currentTimeMillis();
//			utibaHandler.moneyTransfer(sessionId, "100", "stresstest_ioc",null);
			resultTest.setTransfer(System.currentTimeMillis() -mt);
			
			resultTest.setTotalTime(System.currentTimeMillis() - m1);
			result.add(resultTest);
		}
		
	}
	
	public List<TransferResultTest> getResult(){
		return result;
	}
	
	private List<String> getUserlist(){
		List<String> list = new ArrayList<String>();
		for(int i=0; i<99; i++){
			list.add("test_stresstest"+i);
		}
		
		return list;
	}

	public void setSessionHandler(SessionHandler sessionHandler) {
		this.sessionHandler = sessionHandler;
	}

	public void setUtibaHandler(UTIBAHandler utibaHandler) {
		this.utibaHandler = utibaHandler;
	}

	
}
