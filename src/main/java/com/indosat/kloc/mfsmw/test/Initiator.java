/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Oct 16, 2014 
 * Time       : 3:14:21 PM 
 */
package com.indosat.kloc.mfsmw.test;

public class Initiator {

	private String username;
	private String pin;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	
}
