/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Aug 12, 2014 
 * Time       : 9:11:08 PM 
 */
package com.indosat.kloc.mfsmw.soap;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.indosat.kloc.mfsmw.model.ApiUser;
import com.indosat.kloc.mfsmw.model.ExtraTransData;
import com.indosat.kloc.mfsmw.model.Prefix;
import com.indosat.kloc.mfsmw.pojo.AgentData;
import com.indosat.kloc.mfsmw.pojo.CommonResponse;
import com.indosat.kloc.mfsmw.pojo.LastTransaction;
import com.indosat.kloc.mfsmw.pojo.Parameter;
import com.indosat.kloc.mfsmw.pojo.Response;
import com.indosat.kloc.mfsmw.pojo.TransactionInfo;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DataStore;
import com.indosat.kloc.mfsmw.util.GeneratePIN;
import com.indosat.kloc.mfsmw.util.Utils;
import com.utiba.delirium.ws.misc.KeyValuePair;
import com.utiba.delirium.ws.misc.KeyValuePairMap;
import com.utiba.delirium.ws.typedquery.Agent;
import com.utiba.delirium.ws.typedquery.AgentGroup;
import com.utiba.delirium.ws.typedquery.ExtTransaction;
import com.utiba.delirium.ws.typedquery.TransactionData;
import com.utiba.delirium.ws.typedquery.TransactionParty;
import com.utiba.delirium.ws.typedquery.TransactionSummary;

import umarketscws.AccountResponseType;
import umarketscws.AdjustWalletRequest;
import umarketscws.AdjustWalletRequestType;
import umarketscws.AdjustWalletResponse;
import umarketscws.AgentGroupsResponseType;
import umarketscws.AgentResponseType;
import umarketscws.BalanceRequestType;
import umarketscws.BalanceResponseType;
import umarketscws.BillpayRequestType;
import umarketscws.CancelcouponRequestType;
import umarketscws.CashinRequestType;
import umarketscws.ConfirmRequestType;
import umarketscws.CoupontransferRequestType;
import umarketscws.CreatecouponRequestType;
import umarketscws.CreatecouponResponseType;
import umarketscws.DeleteRequestType;
import umarketscws.GetAccountByReferenceIDAndTypeRequest;
import umarketscws.GetAgentByReferenceRequest;
import umarketscws.GetAgentGroupByAgentIDRequest;
import umarketscws.GetChildListByReferenceIDRequest;
import umarketscws.GetChildListByReferenceIDRequestType;
import umarketscws.GetChildListByReferenceIDResponse;
import umarketscws.GetTransactionByIDRequest;
import umarketscws.GetcouponRequestType;
import umarketscws.GetcouponResponseType;
import umarketscws.JoinchildRequestType;
import umarketscws.JoinparentRequestType;
import umarketscws.LastTransactionsRequest;
import umarketscws.LastTransactionsRequestType;
import umarketscws.LastTransactionsResponse;
import umarketscws.MapAgentRequest;
import umarketscws.MapAgentRequestType;
import umarketscws.MapAgentResponse;
import umarketscws.ModifyAccountRequest;
import umarketscws.ModifyAccountRequestType;
import umarketscws.ModifyAccountResponse;
import umarketscws.ModifyRequestType;
import umarketscws.NextidRequestType;
import umarketscws.PinRequestType;
import umarketscws.QuerybillpayRequestType;
import umarketscws.QuerybillpayResponseType;
import umarketscws.RegisterESubscriberRequestType;
import umarketscws.RegisterRequestType;
import umarketscws.RemitcouponRequestType;
import umarketscws.ResetPinRequest;
import umarketscws.ResetPinRequestType;
import umarketscws.ResetPinResponse;
import umarketscws.ReverseRequestType;
import umarketscws.SellRequestType;
import umarketscws.StandardBizResponse;
import umarketscws.TransactionResponseType;
import umarketscws.TransferRequestType;
import umarketscws.TransqueryextRequestType;
import umarketscws.TransqueryextResponseType;
import umarketscws.UMarketSC;
import umarketscws.UnmapAgentRequest;
import umarketscws.UnmapAgentRequestType;
import umarketscws.UnmapAgentResponse;
import umarketspiws.v1.CommitRequest;
import umarketspiws.v1.CommitResponse;
import umarketspiws.v1.CreditAirtimeRequest;
import umarketspiws.v1.CreditAirtimeResponse;
import umarketspiws.v1.INService;
import umarketspiws.v1.IsAirtimeAccountValidRequest;
import umarketspiws.v1.IsAirtimeAccountValidResponse;

@Component
public class UTIBAHandler {

	private static Log log = LogFactory.getLog(UTIBAHandler.class);

	@Autowired
	private SessionHandler sessionHandler;
	@Autowired
	private UMarketSC utibaClient;
	@Autowired
	private INService isc;
	@Autowired
	private DataStore dataManager;
	@Autowired
	private GeneratePIN generatePIN;

	@Value("${app.utiba.agentid.wallet.nonkyc}")
	private int nonKycAgentId;
	@Value("${app.coupon.expired.hour}")
	private Integer expiredHour;
	@Value("${app.utiba.kyc.agentid}")
	private String agentIDallowToKYC;
	@Value("${app.utiba.outlet.agentid}")
	private String agentIDOutlet;

	public Response transQueryExt(String sessionId, String extRef, ApiUser user) {
		Response response = new Response();
		try {
			TransqueryextRequestType requestType = new TransqueryextRequestType();
			requestType.setSessionid(sessionId);
			requestType.setTransactionExtRef(extRef);
			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			requestType.setExtraTransData(kvpm);

			TransqueryextResponseType extResp = utibaClient.transqueryext(requestType);
			if (extResp.getResult() == 415) {
				response.setTrxid(String.valueOf(extResp.getTransid()));
				response.setStatus(extResp.getResult());
				response.setMsg(dataManager.getErrorMsg(extResp.getResult()));
				return response;
			}
			response.setTrxid(String.valueOf(extResp.getTransid()));
			response.setStatus(extResp.getTransResult());
			response.setMsg(dataManager.getErrorMsg(extResp.getTransResult()));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("transQueryExt Found Error: sessionId[" + sessionId + "] extRef[" + extRef + "]");
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}

		return response;
	}

	public Response reversalExtRef(String sessionId, String extRef, String amount, ApiUser user) {
		Response response = new Response();
		String transid = "";
		String amountFromQuery = "";
		try {
			TransqueryextRequestType requestType = new TransqueryextRequestType();
			requestType.setSessionid(sessionId);
			requestType.setTransactionExtRef(extRef);
			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			requestType.setExtraTransData(kvpm);

			TransqueryextResponseType extResp = utibaClient.transqueryext(requestType);
			if (extResp.getResult() == Constant.RC_SUCCESS) {
				transid = String.valueOf(extResp.getQueryTransid());
				amountFromQuery = String.valueOf(extResp.getAmount());

				ReverseRequestType revRequestType = new ReverseRequestType();
				revRequestType.setSessionid(sessionId);
				revRequestType.setTransactionID(Integer.valueOf(transid));
				revRequestType.setAmount(new BigDecimal(amountFromQuery.split(".00")[0]));
				revRequestType.setExtraTransData(kvpm);
				StandardBizResponse bizResponse = utibaClient.reverse(revRequestType);
				response.setStatus(bizResponse.getResult());
				response.setTrxid(String.valueOf(bizResponse.getTransid()));
				response.setMsg(dataManager.getErrorMsg(bizResponse.getResult()));
			} else {
				response.setStatus(extResp.getResult());
				response.setTrxid(String.valueOf(extResp.getTransid()));
				response.setMsg(dataManager.getErrorMsg(extResp.getResult()));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("transQueryExt Found Error: sessionId[" + sessionId + "] extRef[" + extRef + "]");
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}

		return response;
	}

	public Response transQueryExtExtra(String sessionId, String extRef, ApiUser user) {
		Response response = new Response();
		try {
			TransqueryextRequestType requestType = new TransqueryextRequestType();
			requestType.setSessionid(sessionId);
			requestType.setTransactionExtRef(extRef);
			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			requestType.setExtraTransData(kvpm);

			TransqueryextResponseType extResp = utibaClient.transqueryext(requestType);
			log.debug("Trans Ext Ref Data[" + extResp.getResult() + "]");
			log.debug("Trans Ext Ref Data[" + extResp.getTransResult() + "]");
			response.setTrxid(String.valueOf(extResp.getTransid()));
			response.setStatus(extResp.getResult());
			response.setMsg(dataManager.getErrorMsg(extResp.getResult()));

			HashMap<String, String> map = new HashMap<>();
			if (extResp.getQueryTransid() != null) {
				map.put("QueryTransID", String.valueOf(extResp.getQueryTransid()));
			}
			if (extResp.getAmount() != null) {
				map.put("QueryAmount", String.valueOf(extResp.getAmount()));
			}
			if (extResp.getDate() != null) {
				map.put("QueryTransDate", String.valueOf(extResp.getDate()));
			}
			if (extResp.getTransResult() != null) {
				map.put("QueryTransResult", String.valueOf(extResp.getTransResult()));
				try {
					map.put("QueryTransResult", dataManager.getErrorMsg(extResp.getTransResult()));
				} catch (Exception ex) {
					map.put("QueryTransResult", "General Error, Ask Support");
				}
			}
			if (extResp.getFee() != null) {
				map.put("QueryFee", String.valueOf(extResp.getFee()));
			}
			response.setDetailTrx(map);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("transQueryExt Found Error: sessionId[" + sessionId + "] extRef[" + extRef + "]");
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}

		return response;
	}

	public Response resetPin(String agent, ApiUser user) {
		Response response = new Response();
		try {
			String sessionId = dataManager.getGlobalSessionId();
			ResetPinRequest resetPinRequest = new ResetPinRequest();
			resetPinRequest.setResetPinRequestType(new ResetPinRequestType());
			resetPinRequest.getResetPinRequestType().setSessionid(sessionId);
			resetPinRequest.getResetPinRequestType().setAgent(agent);
			resetPinRequest.getResetPinRequestType().setSuppressPinExpiry(true);

			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			resetPinRequest.getResetPinRequestType().setExtraTransData(kvpm);

			ResetPinResponse rpr = utibaClient.resetPin(resetPinRequest);
			response.setTrxid(String.valueOf(rpr.getResetPinReturn().getTransid()));
			response.setStatus(rpr.getResetPinReturn().getResult());
			response.setMsg(dataManager.getErrorMsg(rpr.getResetPinReturn().getResult()));
			if (rpr.getResetPinReturn().getResult() == Constant.RC_SESSION_EXPIRED
					&& rpr.getResetPinReturn().getResultNamespace().equalsIgnoreCase("session")) {
				log.info("Session Expired, try to get the new one......");
				dataManager.setGlobalSessionId();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				response = this.resetPin(agent, user);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("resetPin Found Error: agent[" + agent + "]");
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}

		return response;
	}

	public Response unmapAgent(String sessionId, String agent, long agentId, ApiUser user) {
		Response response = new Response();
		try {
			UnmapAgentRequest unmapAgentRequest = new UnmapAgentRequest();
			unmapAgentRequest.setUnmapAgentRequestType(new UnmapAgentRequestType());
			unmapAgentRequest.getUnmapAgentRequestType().setSessionid(sessionId);
			unmapAgentRequest.getUnmapAgentRequestType().setAgid(agentId);
			unmapAgentRequest.getUnmapAgentRequestType().setAgent(agent);
			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			unmapAgentRequest.getUnmapAgentRequestType().setExtraTransData(kvpm);

			UnmapAgentResponse unmapAgentResponse = utibaClient.unmapAgent(unmapAgentRequest);
			response.setTrxid(String.valueOf(unmapAgentResponse.getUnmapAgentReturn().getTransid()));
			response.setStatus(unmapAgentResponse.getUnmapAgentReturn().getResult());
			response.setMsg(dataManager.getErrorMsg(unmapAgentResponse.getUnmapAgentReturn().getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("unmapAgent Found Error: agent[" + agent + "] agentId[" + agentId + "]");
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public Response unmapAgentExtParam(String sessionId, String agent, long agentId, String idno, String type,
			String agentInitiator, ApiUser user) {
		Response response = new Response();
		try {
			UnmapAgentRequest unmapAgentRequest = new UnmapAgentRequest();
			unmapAgentRequest.setUnmapAgentRequestType(new UnmapAgentRequestType());
			unmapAgentRequest.getUnmapAgentRequestType().setSessionid(sessionId);
			unmapAgentRequest.getUnmapAgentRequestType().setAgid(agentId);
			unmapAgentRequest.getUnmapAgentRequestType().setAgent(agent);
			if (!Utils.isNullorEmptyString(type) && !Utils.isNullorEmptyString(idno))
				if (type.equalsIgnoreCase("1"))
					unmapAgentRequest.getUnmapAgentRequestType().setTransExtReference(idno);
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kycReq = new KeyValuePair();
			kycReq.setKey("kyc_request");
			kycReq.setValue("1");
			kvpm.getKeyValuePairs().add(kycReq);

			KeyValuePair kvagent = new KeyValuePair();
			kvagent.setKey("init_agent");
			kvagent.setValue(agentInitiator);
			kvpm.getKeyValuePairs().add(kvagent);

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);

			unmapAgentRequest.getUnmapAgentRequestType().setExtraTransData(kvpm);

			UnmapAgentResponse unmapAgentResponse = utibaClient.unmapAgent(unmapAgentRequest);
			response.setTrxid(String.valueOf(unmapAgentResponse.getUnmapAgentReturn().getTransid()));
			response.setStatus(unmapAgentResponse.getUnmapAgentReturn().getResult());
			response.setMsg(dataManager.getErrorMsg(unmapAgentResponse.getUnmapAgentReturn().getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("unmapAgent Found Error: agent[" + agent + "] agentId[" + agentId + "]");
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public Response getNextId(String sessionId) {
		Response response = new Response();
		try {
			NextidRequestType nextidRequestType = new NextidRequestType();
			nextidRequestType.setSessionid(sessionId);
			StandardBizResponse sbResp = utibaClient.nextid(nextidRequestType);
			response.setTrxid(String.valueOf(sbResp.getTransid()));
			response.setStatus(sbResp.getResult());
			response.setMsg(dataManager.getErrorMsg(sbResp.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}

		return response;
	}

	public Response updateProfile(String sessionId, String initiator, String name, String smsAddress, String email,
			umarketscws.KeyValuePairMap value, ApiUser user) {
		Response response = new Response();
		try {
			ModifyRequestType requestType = new ModifyRequestType();
			requestType.setSessionid(sessionId);
			requestType.setAgent(initiator);
			requestType.setName(name);
			requestType.setSmsAddress(smsAddress);
			if (!Utils.isNullorEmptyString(email)) {
				requestType.setEmailAddress(email);
			}
			requestType.setExtraParams(value);

			// add for channelType
			umarketscws.KeyValuePair kvChannel = new umarketscws.KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			value.getKeyValuePair().add(kvChannel);

			umarketscws.KeyValuePair kvApiUser = new umarketscws.KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			value.getKeyValuePair().add(kvApiUser);

			StandardBizResponse sbResp = utibaClient.modify(requestType);
			response.setStatus(sbResp.getResult());
			response.setMsg(dataManager.getErrorMsg(sbResp.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public Response updateByAgent(String sessionId, String target, String initiator, String name, String smsAddress,
			String email, umarketscws.KeyValuePairMap value, ApiUser user) {
		Response response = new Response();
		try {
			ModifyRequestType requestType = new ModifyRequestType();
			requestType.setSessionid(sessionId);
			requestType.setAgent(target);
			requestType.setName(name);
			requestType.setSmsAddress(smsAddress);

			if (!Utils.isNullorEmptyString(email)) {
				requestType.setEmailAddress(email);
			}
			requestType.setExtraParams(value);
			KeyValuePairMap kvpm = new KeyValuePairMap();

			KeyValuePair supressSMS = new KeyValuePair();
			supressSMS.setKey("suppress_sms");
			supressSMS.setValue("1");
			kvpm.getKeyValuePairs().add(supressSMS);

			KeyValuePair kvagent = new KeyValuePair();
			kvagent.setKey("init_agent");
			kvagent.setValue(initiator);
			kvpm.getKeyValuePairs().add(kvagent);

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);

			requestType.setExtraTransData(kvpm);

			log.debug(requestType);
			StandardBizResponse sbResp = utibaClient.modify(requestType);
			response.setStatus(sbResp.getResult());
			response.setMsg(dataManager.getErrorMsg(sbResp.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public Response updateAgentToModifyAble(String sessionId, String target, int Status, ApiUser user) {
		Response response = new Response();
		try {
			ModifyRequestType requestType = new ModifyRequestType();
			requestType.setSessionid(sessionId);
			requestType.setAgent(target);
			ModifyRequestType.Type type = new ModifyRequestType.Type();
			if (Status == 1) {
				type.setTrusted(true);
			} else {
				type.setTrusted(false);
			}
			requestType.setType(type);

			KeyValuePairMap kvpm = new KeyValuePairMap();

			KeyValuePair supressSMS = new KeyValuePair();
			supressSMS.setKey("suppress_sms");
			supressSMS.setValue("1");
			kvpm.getKeyValuePairs().add(supressSMS);

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);

			requestType.setExtraTransData(kvpm);

			log.debug(requestType);
			StandardBizResponse sbResp = utibaClient.modify(requestType);
			response.setStatus(sbResp.getResult());
			response.setMsg(dataManager.getErrorMsg(sbResp.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public Response changePIN(String sessionId, String initiator, String oldPassword, String newPassword,
			ApiUser user) {
		Response response = new Response();
		try {
			String pin = generatePIN.generate(sessionId, initiator, oldPassword);
			PinRequestType requestType = new PinRequestType();
			requestType.setSessionid(sessionId);
			requestType.setPin(pin);
			requestType.setNewPin(newPassword);

			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			requestType.setExtraTransData(kvpm);

			StandardBizResponse sbResp = utibaClient.pin(requestType);
			response.setStatus(sbResp.getResult());
			response.setMsg(dataManager.getErrorMsg(sbResp.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}

		return response;
	}

	public Response resetPIN(String initiator, String pin, ApiUser user) {
		Response response = new Response();
		try {
			ResetPinRequest resetPinRequest = new ResetPinRequest();
			ResetPinRequestType resetPinRequestType = new ResetPinRequestType();
			resetPinRequest.setResetPinRequestType(resetPinRequestType);
			resetPinRequest.getResetPinRequestType().setSessionid(dataManager.getGlobalSessionId());
			resetPinRequest.getResetPinRequestType().setNewPin(pin);
			resetPinRequest.getResetPinRequestType().setAgent(initiator);
			resetPinRequest.getResetPinRequestType().setSuppressPinExpiry(true);

			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvp = new KeyValuePair();
			kvp.setKey("suppress_sms");
			kvp.setValue("1");
			keyValuePairMap.getKeyValuePairs().add(kvp);

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);

			resetPinRequest.getResetPinRequestType().setExtraTransData(keyValuePairMap);
			ResetPinResponse resetPinResponse = utibaClient.resetPin(resetPinRequest);
			response.setStatus(resetPinResponse.getResetPinReturn().getResult());
			response.setMsg(dataManager.getErrorMsg(resetPinResponse.getResetPinReturn().getResult()));
			response.setTrxid(String.valueOf(resetPinResponse.getResetPinReturn().getTransid()));
			if (resetPinResponse.getResetPinReturn().getResult() == Constant.RC_SESSION_EXPIRED
					&& resetPinResponse.getResetPinReturn().getResultNamespace().equalsIgnoreCase("session")) {
				log.info("Session Expired, try to get the new one......");
				dataManager.setGlobalSessionId();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				response = this.resetPIN(initiator, pin, user);
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}

		return response;
	}

	public Response eRegister(RegisterESubscriberRequestType requestType) {
		Response response = new Response();
		try {

			StandardBizResponse utibaResp = utibaClient.registerESubscriber(requestType);

			response.setStatus(utibaResp.getResult());
			response.setMsg(dataManager.getErrorMsg(utibaResp.getResult()));

			if (utibaResp.getResult() == Constant.RC_SESSION_EXPIRED
					&& utibaResp.getResultNamespace().equalsIgnoreCase("session")) {
				log.info("Session Expired, try to get the new one......");
				dataManager.setGlobalSessionId();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				response = this.eRegister(requestType);
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public Response registerOutlet(String initiator, String name, ApiUser user) {
		Response response = new Response();
		RegisterRequestType requestType = new RegisterRequestType();
		requestType.setSessionid(dataManager.getGlobalSessionId());
		requestType.setAgent(initiator);
		requestType.setLanguage("id");
		requestType.setSmsAddress(initiator);
		requestType.setName(name);
		requestType.setNewPin("123456");
		requestType.setAgentCategory("outlet");

		umarketscws.KeyValuePairMap keyValuePairMap = new umarketscws.KeyValuePairMap();

		umarketscws.KeyValuePair kv3 = new umarketscws.KeyValuePair();
		kv3.setKey("primary_group");
		kv3.setValue("outlet");

		keyValuePairMap.getKeyValuePair().add(kv3);

		requestType.setExtraParams(keyValuePairMap);

		// add for channelType
		KeyValuePairMap kvpm = new KeyValuePairMap();
		KeyValuePair kvChannel = new KeyValuePair();
		kvChannel.setKey("channel_type");
		kvChannel.setValue(String.valueOf(user.getChannelType()));
		kvpm.getKeyValuePairs().add(kvChannel);

		KeyValuePair kvApiUser = new KeyValuePair();
		kvApiUser.setKey("api_user");
		kvApiUser.setValue(String.valueOf(user.getUserId()));
		kvpm.getKeyValuePairs().add(kvApiUser);
		requestType.setExtraTransData(kvpm);

		StandardBizResponse regRes = utibaClient.register(requestType);

		response.setTrxid(String.valueOf(regRes.getTransid()));
		response.setStatus(regRes.getResult());
		response.setMsg(dataManager.getErrorMsg(regRes.getResult()));

		return response;
	}

	public Response simpleReg(String initiator, String name, String dob, ApiUser user) {
		Response response = new Response();
		try {

			RegisterRequestType requestType = new RegisterRequestType();
			requestType.setSessionid(dataManager.getGlobalSessionId());
			requestType.setAgent(initiator);
			requestType.setLanguage("id");
			requestType.setSmsAddress(initiator);
			requestType.setName(name);

			umarketscws.KeyValuePairMap keyValuePairMap = new umarketscws.KeyValuePairMap();
			umarketscws.KeyValuePair kv = new umarketscws.KeyValuePair();
			kv.setKey("suppress_pin_expiry");
			kv.setValue("1");

			umarketscws.KeyValuePair kv3 = new umarketscws.KeyValuePair();
			kv3.setKey("primary_group");
			kv3.setValue("subscribers");

			umarketscws.KeyValuePair kv4 = new umarketscws.KeyValuePair();
			kv4.setKey("gen_new_pin");
			kv4.setValue("1");

			if (!Utils.isNullorEmptyString(dob)) {
				umarketscws.KeyValuePair kv2 = new umarketscws.KeyValuePair();
				kv2.setKey("dob");
				kv2.setValue(dob);
				keyValuePairMap.getKeyValuePair().add(kv2);
			}

			keyValuePairMap.getKeyValuePair().add(kv);
			keyValuePairMap.getKeyValuePair().add(kv3);
			keyValuePairMap.getKeyValuePair().add(kv4);

			requestType.setExtraParams(keyValuePairMap);

			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			requestType.setExtraTransData(kvpm);

			StandardBizResponse regRes = utibaClient.register(requestType);

			response.setTrxid(String.valueOf(regRes.getTransid()));
			response.setStatus(regRes.getResult());
			response.setMsg(dataManager.getErrorMsg(regRes.getResult()));

		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public Response register(String initiator, String pin, String name, String msisdn, String email,
			umarketscws.KeyValuePairMap value, ApiUser user) {

		Response response = new Response();

		try {
			RegisterRequestType requestType = new RegisterRequestType();
			requestType.setSessionid(dataManager.getGlobalSessionId());
			requestType.setAgent(initiator);
			requestType.setLanguage("id");
			requestType.setSmsAddress(msisdn);
			requestType.setName(name);
			requestType.setNewPin(pin);
			requestType.setExtraParams(value);
			if (!Utils.isNullorEmptyString(email))
				requestType.setEmailAddress(email);

			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			requestType.setExtraTransData(kvpm);

			StandardBizResponse sbr = utibaClient.register(requestType);

			response.setStatus(sbr.getResult());
			response.setMsg(dataManager.getErrorMsg(sbr.getResult()));

			if (sbr.getResult() == Constant.RC_SESSION_EXPIRED
					&& sbr.getResultNamespace().equalsIgnoreCase("session")) {
				log.info("Session Expired, try to get the new one......");
				dataManager.setGlobalSessionId();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				response = this.register(initiator, pin, name, msisdn, email, value, user);
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public Response registerGeneratePin(String name, String msisdn, umarketscws.KeyValuePairMap value, ApiUser user) {
		Response response = new Response();
		try {
			RegisterRequestType requestType = new RegisterRequestType();
			requestType.setSessionid(dataManager.getGlobalSessionId());
			requestType.setAgent(msisdn);
			requestType.setLanguage("id");
			requestType.setSmsAddress(msisdn);
			requestType.setName(name);
			requestType.setExtraParams(value);

			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			requestType.setExtraTransData(kvpm);

			StandardBizResponse sbr = utibaClient.register(requestType);

			response.setStatus(sbr.getResult());
			response.setMsg(dataManager.getErrorMsg(sbr.getResult()));

			if (sbr.getResult() == Constant.RC_SESSION_EXPIRED
					&& sbr.getResultNamespace().equalsIgnoreCase("session")) {
				log.info("Session Expired, try to get the new one......");
				dataManager.setGlobalSessionId();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				response = this.registerGeneratePin(name, msisdn, value, user);
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public Response getLastTransaction(String msisdn, int count, ApiUser user) {
		log.info("getLastTransaction: msisdn" + msisdn + " count:" + count);
		String sessionId = dataManager.getGlobalSessionId();
		Response response = new Response();
		try {
			LastTransactionsRequest requestType = new LastTransactionsRequest();
			LastTransactionsRequestType lastTransactionsRequestType = new LastTransactionsRequestType();
			requestType.setLastTransactionsRequestType(lastTransactionsRequestType);
			requestType.getLastTransactionsRequestType().setAgent(msisdn);
			requestType.getLastTransactionsRequestType().setSessionid(sessionId);
			requestType.getLastTransactionsRequestType().setTransCount(count);

			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			requestType.getLastTransactionsRequestType().setExtraTransData(kvpm);

			LastTransactionsResponse ltresp = utibaClient.lastTransactions(requestType);
			List<TransactionSummary> list = ltresp.getLastTransactionsReturn().getTransactionsList();
			List<LastTransaction> lastTrx = new ArrayList<LastTransaction>();
			for (int i = 0; i < list.size(); i++) {
				LastTransaction trx = new LastTransaction();
				StringBuilder sbDate = new StringBuilder();
				sbDate.append(list.get(i).getLastModified().getDay()).append("/")
						.append(list.get(i).getLastModified().getMonth()).append("/")
						.append(list.get(i).getLastModified().getYear()).append(" ")
						.append(list.get(i).getLastModified().getHour()).append(":")
						.append(list.get(i).getLastModified().getMinute());

				trx.setDate(sbDate.toString());
				trx.setAmount(list.get(i).getAmount());
				trx.setTransid(list.get(i).getTransactionId());
				trx.setType(list.get(i).getTransactionType());
				trx.setAgent(list.get(i).getPartiesReferenceIdList().get(1));
				//add remark
				String remark = "";
				for(int j=0; j<list.get(i).getPartiesReferenceIdList().size(); j++) {
					remark +="#"+list.get(i).getPartiesReferenceIdList().get(j);
				}
				trx.setRemark(remark);
				//==========
				lastTrx.add(trx);
			}

			response.setTrxid(String.valueOf(ltresp.getLastTransactionsReturn().getTransid()));
			response.setStatus(ltresp.getLastTransactionsReturn().getResult());
			response.setTrxList(lastTrx);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}

		return response;
	}

	public Response getAgentByReferenceRequest(String msisdn, ApiUser user) {
		log.info("getAgentByReferenceRequest msisdn:" + msisdn);
		Response rs = new Response();
		try {
			String sessionId = dataManager.getGlobalSessionId();
			GetAgentByReferenceRequest getAgentByReferenceRequest = new GetAgentByReferenceRequest();
			getAgentByReferenceRequest.setSessionid(sessionId);
			getAgentByReferenceRequest.setReference(msisdn);

			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			getAgentByReferenceRequest.setExtraTransData(kvpm);

			AgentResponseType response = utibaClient.getAgentByReference(getAgentByReferenceRequest);
			if (response.getResult() == Constant.RC_SUCCESS) {

				if (!Utils.isNullorEmptyString(response.getAgent().getCategory()))
					rs.getAgentData().setCategory(response.getAgent().getCategory());

				GetAgentGroupByAgentIDRequest agentGroupByAgentIDRequest = new GetAgentGroupByAgentIDRequest();
				agentGroupByAgentIDRequest.setSessionid(sessionId);
				agentGroupByAgentIDRequest.setAgentID(response.getAgent().getAgentID());
				agentGroupByAgentIDRequest.setExtraTransData(kvpm);
				AgentGroupsResponseType agentGroupResp = utibaClient.getAgentGroupByAgentID(agentGroupByAgentIDRequest);
				log.debug("response.getAgent().getAgentID(): " + response.getAgent().getAgentID());
				log.debug("agentGroupResp: " + agentGroupResp.getResult());
				if (response.getAgent().getStatus() == Constant.UTIBA_ACCOUNT_STATUS_REGISTERED) {

					rs.setName(response.getAgent().getName() == null ? response.getAgent().getReference()
							: response.getAgent().getName());

					if (!Utils.isNullorEmptyString(response.getAgent().getSMSAddress())) {
						rs.getAgentData().setSmsAddress(response.getAgent().getSMSAddress());
					}

					if (!Utils.isNullorEmptyString(response.getAgent().getEmailAddress())) {
						rs.getAgentData().setEmail(response.getAgent().getEmailAddress());
					}

					List<KeyValuePair> agentDetailsList = response.getAgent().getAgentData();
					for (KeyValuePair obj : agentDetailsList) {
						if (obj.getKey().equalsIgnoreCase("idnumber")) {
							if (!obj.getValue().equals("###"))
								rs.getAgentData().setIdNo(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase("id_num")) {
							if (!obj.getValue().equals("###"))
								rs.getAgentData().setIdNo(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase("idno")) {
							if (!obj.getValue().equals("###"))
								rs.getAgentData().setIdNo(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase("alt_id_type")) {
							if (!obj.getValue().equals("###"))
								rs.getAgentData().setIdNo(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase("idtype")) {
							if (!obj.getValue().equals("###"))
								rs.getAgentData().setIdType(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase("id_type")) {
							if (!obj.getValue().equals("###")) {
								if (obj.getValue().equals("national_id"))
									rs.getAgentData().setIdType("1");
							}
						} else if (obj.getKey().equalsIgnoreCase(Constant.ADDRESS_PARAM)) {
							if (!obj.getValue().equals("###"))
								rs.getAgentData().setAddress(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase("alt_id")) {
							if (!obj.getValue().equals("###"))
								rs.getAgentData().setMotherMaidenName(obj.getValue());
						}
						// else if (obj.getKey().equalsIgnoreCase("mother")) {
						// if (!obj.getValue().equals("###"))
						// rs.getAgentData().setMotherMaidenName(obj.getValue());
						// }
						else if (obj.getKey().equalsIgnoreCase("mothername")) {
							if (!obj.getValue().equals("###"))
								rs.getAgentData().setMotherMaidenName(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase(Constant.GENDER_PARAM)) {
							rs.getAgentData().setGender(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase(Constant.DOB_PARAM)) {
							if (!obj.getValue().equals("###"))
								rs.getAgentData().setDateOfBirth(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase("date_of_birth")) {
							if (!obj.getValue().equals("###"))
								rs.getAgentData().setDateOfBirth(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase("birthdate")) {
							if (!obj.getValue().equals("###"))
								// rs.getAgentData().setDateOfBirth(Utils.formatDate("yyyy-MM-dd'T'HH:mm:ss",
								// "ddMMyyyy", obj.getValue()));
								rs.getAgentData().setDateOfBirth(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase(Constant.FIRSTNAME_PARAM)) {
							rs.getAgentData().setFirstName(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase(Constant.LASTNAME_PARAM)) {
							rs.getAgentData().setLastName(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase(Constant.PROFILEPIC_PARAM)) {
							if (!obj.getValue().equals("###"))
								rs.getAgentData().setProfilePic(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase(Constant.IDPHOTO_PARAM)) {
							rs.getAgentData().setIdPhoto(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase("crn")) {
							rs.getAgentData().setAccountId(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase(Constant.MSISDN_PARAM)) {
							rs.getAgentData().setSmsAddress(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase(Constant.KYC_REQUEST_PARAM)) {
							rs.getAgentData().setWaitingForKyc(true);
						} else if (obj.getKey().equalsIgnoreCase(Constant.EMAIL_PARAM)) {
							rs.getAgentData().setEmail(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase(Constant.COUNTRYCODE_PARAM)) {
							rs.getAgentData().setCountrycode(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase(Constant.CITY_PARAM)) {
							rs.getAgentData().setCity(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase(Constant.PROVINCE_PARAM)) {
							rs.getAgentData().setProvince(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase(Constant.IDPHOTOUSER_PARAM)) {
							rs.getAgentData().setIdPhotoSelfie(obj.getValue());
						} else if (obj.getKey().equalsIgnoreCase(Constant.POB_PARAM)) {
							rs.getAgentData().setPlaceOfBirth(obj.getValue());
						}
					}

					rs.getAgentData().setWalletType(Constant.WALLET_TYPE_PREMIUM);

					// String kycAgentId[] = agentIDallowToKYC.split(",");
					ArrayList<Long> walletArray = new ArrayList<Long>();
					for (AgentGroup obj : agentGroupResp.getAgentGroups()) {
						walletArray.add(obj.getID());
						log.debug("obj.getID(): " + obj.getID() + "  obj.getName(): " + obj.getName());
						if (obj.getID() == nonKycAgentId) {
							rs.getAgentData().setWalletType(Constant.WALLET_TYPE_REG);
							if (rs.getAgentData().isWaitingForKyc())
								rs.getAgentData().setWalletType(Constant.WALLET_TYPE_KYC_REQUEST);
						}

						/*
						 * for(String agentId: kycAgentId){
						 * if(Integer.valueOf(agentId) == obj.getID()){
						 * rs.getAgentData().setAllowToKyc(true); } }
						 */

						if (agentIDallowToKYC.contains(String.valueOf(obj.getID()))) {
							rs.getAgentData().setAllowToKyc(true);
						}

						if (agentIDOutlet.contains(String.valueOf(obj.getID()))) {
							rs.getAgentData().setOutlet(true);
						}

						// rs.getAgentData().setAllowToKyc(true);//temporary
					}

					rs.getAgentData().setWalletGrouping(walletArray.toString());

					GetAccountByReferenceIDAndTypeRequest typeRequest = new GetAccountByReferenceIDAndTypeRequest();
					typeRequest.setSessionid(sessionId);
					typeRequest.setReferenceID(response.getAgent().getReferenceID());
					typeRequest.setType(1);
					typeRequest.setExtraTransData(kvpm);
					AccountResponseType accountResponseType = utibaClient.getAccountByReferenceIDAndType(typeRequest);
					if (accountResponseType.getResult() == Constant.RC_SUCCESS) {
						rs.setBalance(String.valueOf(accountResponseType.getAccount().getAvailBalance()));
						HashMap<String, String> detailTrx = new HashMap<>();
						detailTrx.put("limit", String.valueOf(accountResponseType.getAccount().getLimit()));
						detailTrx.put("upperLimit", String.valueOf(accountResponseType.getAccount().getUpperLimit()));
						detailTrx.put("Threshold", String.valueOf(accountResponseType.getAccount().getThreshold()));
						rs.setDetailTrx(detailTrx);
						rs.setTrxid(String.valueOf(response.getTransid()));
					}

					rs.setStatus(response.getResult());
					rs.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));

				} else if (response.getAgent().getAgentType() == 0 && response.getAgent().getStatus() == 0) {
					rs.setStatus(Constant.RC_AGENT_NOTFOUND);
					rs.setMsg(dataManager.getErrorMsg(Constant.RC_AGENT_NOTFOUND));
				} else {
					rs.setStatus(Constant.RC_AGENT_SUSPENDED);
					rs.setMsg(dataManager.getErrorMsg(Constant.RC_AGENT_SUSPENDED));
				}

			} else if (response.getResult() == Constant.RC_SESSION_EXPIRED
					&& response.getResultNamespace().equalsIgnoreCase("session")) {
				log.info("Session Expired, try to get the new one......");
				dataManager.setGlobalSessionId();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				rs = this.getAgentByReferenceRequest(msisdn, user);
			} else {
				rs.setStatus(Constant.RC_AGENT_NOTFOUND);
				rs.setMsg(dataManager.getErrorMsg(Constant.RC_AGENT_NOTFOUND));
			}
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return rs;
	}

	public Response sellNoConfirm(String sessionId, String to, String amount, String extRef, String serviceType,
			ApiUser user) {
		Response rs = new Response();

		try {
			SellRequestType sellRequestType = new SellRequestType();

			KeyValuePairMap kpm = new KeyValuePairMap();

			KeyValuePair kp = new KeyValuePair();
			kp.setKey("service_type");
			kp.setValue(serviceType);

			KeyValuePair supressSMS = new KeyValuePair();
			supressSMS.setKey("suppress_sms");
			supressSMS.setValue("1");

			if (!Utils.isNullorEmptyString(serviceType) && serviceType.equals("merchant")) {

			} else {
				kpm.getKeyValuePairs().add(supressSMS);
			}

			KeyValuePair supressConfirm = new KeyValuePair();
			supressConfirm.setKey("suppress_confirm");
			supressConfirm.setValue("1");

			kpm.getKeyValuePairs().add(kp);
			kpm.getKeyValuePairs().add(supressConfirm);

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kpm.getKeyValuePairs().add(kvApiUser);

			sellRequestType.setExtraTransData(kpm);
			sellRequestType.setSessionid(sessionId);
			sellRequestType.setTransExtReference(extRef);
			sellRequestType.setTo(to);
			sellRequestType.setAmount(new BigDecimal(amount));

			StandardBizResponse response = utibaClient.sell(sellRequestType);
			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
			rs.setResultNameSpace(response.getResultNamespace());
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
			log.error("Sell request error to[" + to + "] amount[" + amount + "] extRef[" + extRef + "]");
		}

		return rs;
	}

	public Response sellNoConfirmOnebill(String sessionId, String to, String amount, String extRef, String serviceType,
			String billerCustomerID, ApiUser user) {
		Response rs = new Response();

		try {
			SellRequestType sellRequestType = new SellRequestType();

			KeyValuePairMap kpm = new KeyValuePairMap();

			KeyValuePair kp = new KeyValuePair();
			kp.setKey("service_type");
			kp.setValue(serviceType);

			KeyValuePair supressSMS = new KeyValuePair();
			supressSMS.setKey("suppress_sms");
			supressSMS.setValue("1");

			if (!Utils.isNullorEmptyString(billerCustomerID)) {
				// KeyValuePair billerCustID1 = new KeyValuePair();
				// billerCustID1.setKey("idpelanggan");
				// billerCustID1.setValue(billerCustomerID);
				// kpm.getKeyValuePairs().add(billerCustID1);

				KeyValuePair billerCustID2 = new KeyValuePair();
				billerCustID2.setKey("subscriber");
				billerCustID2.setValue(billerCustomerID);
				kpm.getKeyValuePairs().add(billerCustID2);
			}

			if (!Utils.isNullorEmptyString(serviceType) && serviceType.equals("merchant")) {

			} else {
				kpm.getKeyValuePairs().add(supressSMS);
			}

			KeyValuePair supressConfirm = new KeyValuePair();
			supressConfirm.setKey("suppress_confirm");
			supressConfirm.setValue("1");

			kpm.getKeyValuePairs().add(supressConfirm);
			kpm.getKeyValuePairs().add(kp);

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kpm.getKeyValuePairs().add(kvApiUser);

			sellRequestType.setExtraTransData(kpm);

			// log.debug("SellNoConfirm Session["+sessionId+"]");
			sellRequestType.setSessionid(sessionId);
			sellRequestType.setTransExtReference(extRef);
			sellRequestType.setTo(to);
			sellRequestType.setAmount(new BigDecimal(amount));
			// log.debug("SellNoConfirm Session["+sessionId+"]");

			StandardBizResponse response = utibaClient.sell(sellRequestType);
			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
			rs.setResultNameSpace(response.getResultNamespace());
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
			log.error("Sell request error to[" + to + "] amount[" + amount + "] extRef[" + extRef + "]");
		}

		return rs;
	}
	
	public Response sellNoConfirmWithParam(String sessionId, String to, String amount, String extRef, String serviceType,
		   int suppress,Parameter param,ApiUser user) {
		Response rs = new Response();

		try {
			SellRequestType sellRequestType = new SellRequestType();

			KeyValuePairMap kpm = new KeyValuePairMap();

			KeyValuePair kp = new KeyValuePair();
			kp.setKey("service_type");
			kp.setValue(serviceType);
			kpm.getKeyValuePairs().add(kp);

			KeyValuePair supressSMS = new KeyValuePair();
			supressSMS.setKey("suppress_sms");
			supressSMS.setValue("1");
//			kpm.getKeyValuePairs().add(supressSMS);
			if (suppress!=1) {

			} else {
				kpm.getKeyValuePairs().add(supressSMS);
			}

			KeyValuePair supressConfirm = new KeyValuePair();
			supressConfirm.setKey("suppress_confirm");
			supressConfirm.setValue("1");
			kpm.getKeyValuePairs().add(supressConfirm);

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kpm.getKeyValuePairs().add(kvChannel);
			
			KeyValuePair kvp = null;
			Iterator it = param.getKeyValueMap().entrySet().iterator();
			StringBuilder reference = new StringBuilder();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (!pair.getKey().equals("signature") && !pair.getKey().equals("userid")
						&& !pair.getKey().equals("amount") && !pair.getKey().equals("extRef")
						&& !pair.getKey().equals("to")&& !pair.getKey().equals("transactionDigest")) {
					kvp = new KeyValuePair();
					kvp.setKey(String.valueOf(pair.getKey()));
					kvp.setValue(String.valueOf(pair.getValue()));
					kpm.getKeyValuePairs().add(kvp);
					
					reference.append(pair.getValue()+"|");
				}
				it.remove();
			}
			
			if(reference.length()>0){
				KeyValuePair kvAddReference = new KeyValuePair();
				String refStr = reference.toString();
				refStr = StringUtils.removeEnd(refStr, "|");
				kvAddReference.setKey("reference");
				kvAddReference.setValue(String.valueOf(refStr));
				kpm.getKeyValuePairs().add(kvAddReference);
			}

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kpm.getKeyValuePairs().add(kvApiUser);

			sellRequestType.setExtraTransData(kpm);
			sellRequestType.setSessionid(sessionId);
			sellRequestType.setTransExtReference(extRef);
			sellRequestType.setTo(to);
			sellRequestType.setAmount(new BigDecimal(amount));

			StandardBizResponse response = utibaClient.sell(sellRequestType);
			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
			rs.setResultNameSpace(response.getResultNamespace());
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
			log.error("Sell request error to[" + to + "] amount[" + amount + "] extRef[" + extRef + "]");
		}

		return rs;
	}

	public Response sell(String sessionId, String to, String amount, String serviceType, String extRef, ApiUser user) {
		Response rs = new Response();
		try {
			SellRequestType sellRequestType = new SellRequestType();

			KeyValuePairMap kpm = new KeyValuePairMap();

			KeyValuePair kp = new KeyValuePair();
			kp.setKey("service_type");
			kp.setValue(serviceType);
			kpm.getKeyValuePairs().add(kp);

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kpm.getKeyValuePairs().add(kvApiUser);

			sellRequestType.setExtraTransData(kpm);

			sellRequestType.setSessionid(sessionId);
			sellRequestType.setWait(true);
			sellRequestType.setSuppressConfirm(false);
			sellRequestType.setTransExtReference(extRef);
			sellRequestType.setTo(to);
			sellRequestType.setAmount(new BigDecimal(amount));

			StandardBizResponse response = utibaClient.sell(sellRequestType);
			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}
	
	public Response sellWithParam(String sessionId, String to, String amount, String serviceType, String extRef, ApiUser user, Parameter param) {
		Response rs = new Response();

		try {
			SellRequestType sellRequestType = new SellRequestType();

			KeyValuePairMap kpm = new KeyValuePairMap();

			KeyValuePair kp = new KeyValuePair();
			kp.setKey("service_type");
			kp.setValue(serviceType);
			kpm.getKeyValuePairs().add(kp);

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kpm.getKeyValuePairs().add(kvApiUser);
			
			KeyValuePair kvp = null;
			Iterator it = param.getKeyValueMap().entrySet().iterator();
			StringBuilder reference = new StringBuilder();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (!pair.getKey().equals("signature") && !pair.getKey().equals("userid")
						&& !pair.getKey().equals("amount") && !pair.getKey().equals("extRef")
						&& !pair.getKey().equals("to")&& !pair.getKey().equals("transactionDigest")) {
					kvp = new KeyValuePair();
					kvp.setKey(String.valueOf(pair.getKey()));
					kvp.setValue(String.valueOf(pair.getValue()));
					kpm.getKeyValuePairs().add(kvp);
					
					reference.append(pair.getValue()+"|");
				}
				it.remove();
			}

			sellRequestType.setExtraTransData(kpm);
			sellRequestType.setSessionid(sessionId);
			sellRequestType.setWait(true);
			sellRequestType.setSuppressConfirm(false);
			sellRequestType.setTransExtReference(extRef);
			sellRequestType.setTo(to);
			sellRequestType.setAmount(new BigDecimal(amount));

			StandardBizResponse response = utibaClient.sell(sellRequestType);
			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}

	public Response sellWithPoint(String sessionId, String to, String amount, String serviceType, String extRef,
			ApiUser user) {
		Response rs = new Response();

		try {
			SellRequestType sellRequestType = new SellRequestType();

			KeyValuePairMap kpm = new KeyValuePairMap();

			if (!Utils.isNullorEmptyString(serviceType)) {
				KeyValuePair kp = new KeyValuePair();
				kp.setKey("service_type");
				kp.setValue(serviceType);
				kpm.getKeyValuePairs().add(kp);
			}

			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			sellRequestType.setExtraTransData(kpm);

			sellRequestType.setSessionid(sessionId);
			sellRequestType.setWait(true);
			sellRequestType.setSuppressConfirm(false);
			sellRequestType.setTransExtReference(extRef);
			sellRequestType.setTo(to);
			sellRequestType.setAmount(new BigDecimal(amount));
			sellRequestType.setType(3);
			StandardBizResponse response = utibaClient.sell(sellRequestType);
			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}

	public Response checkTransaction(String sessionId, String trxId, ApiUser user) {
		Response rs = new Response();

		try {
			TransqueryextRequestType trxRT = new TransqueryextRequestType();
			trxRT.setSessionid(sessionId);
			trxRT.setTransactionExtRef(trxId);

			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			trxRT.setExtraTransData(kvpm);

			TransqueryextResponseType response = utibaClient.transqueryext(trxRT);
			if (rs.getCommonResponse() == null)
				rs.setCommonResponse(new CommonResponse());
			rs.getCommonResponse().setResponseCode(response.getResult());
			rs.getCommonResponse().setResponseMessage(dataManager.getErrorMsg(response.getResult()));
			rs.getCommonResponse().setTrxid(response.getTransid());
			if (rs.getCommonResponse().getTransactionInfo() == null)
				rs.getCommonResponse().setTransactionInfo(new TransactionInfo());
			rs.getCommonResponse().getTransactionInfo().setTransid(trxId);

			if (response.getResult() == Constant.RC_SESSION_EXPIRED
					&& response.getResultNamespace().equalsIgnoreCase("session"))
				rs.setStatus(Constant.RC_SESSION_EXPIRED);

		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}

	public Response couponTransfer(int service, String sessionId, String subscriber, String serviceType, String extRef,
			String amount, String couponId, String outletId, ApiUser user) {

		Response rs = new Response();

		try {
			CoupontransferRequestType requestType = new CoupontransferRequestType();
			KeyValuePairMap value = new KeyValuePairMap();
			KeyValuePair kv = new KeyValuePair();
			kv.setKey("subscriber");
			kv.setValue(subscriber);

			KeyValuePair kv2 = new KeyValuePair();
			if (service == Constant.SERVICE_CASHOUT) {
				kv2.setKey("service_type");
				kv2.setValue("cashout");
			} else {
				kv2.setKey("service_type");
				kv2.setValue(serviceType);
			}

			value.getKeyValuePairs().add(kv);
			value.getKeyValuePairs().add(kv2);

			if (!Utils.isNullorEmptyString(outletId)) {
				KeyValuePair kv3 = new KeyValuePair();
				kv3.setKey("outletid");
				kv3.setValue(outletId);
				value.getKeyValuePairs().add(kv3);
			}

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			value.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			value.getKeyValuePairs().add(kvApiUser);

			requestType.setExtraTransData(value);
			requestType.setSessionid(sessionId);
			requestType.setTransExtReference(extRef);
			requestType.setAmount(new BigDecimal(amount));
			requestType.setType(1);
			requestType.setCouponid(couponId);
			StandardBizResponse response = utibaClient.coupontransfer(requestType);

			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));

		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}

	public Response cashin(String sessionId, String to, String amount, String userId, String transId,
			boolean supressSms, ApiUser user) {
		Response rs = new Response();
		try {
			CashinRequestType requestType = new CashinRequestType();
			KeyValuePairMap value = new KeyValuePairMap();
			KeyValuePair kv = new KeyValuePair();

			if (supressSms) {
				kv.setKey("suppress_sms");
				kv.setValue("1");
			} else {
				kv.setKey("outletid");
				kv.setValue(userId);
			}

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			value.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			value.getKeyValuePairs().add(kvApiUser);

			value.getKeyValuePairs().add(kv);
			requestType.setTransExtReference(transId);
			requestType.setExtraTransData(value);
			requestType.setSessionid(sessionId);
			requestType.setTo(to);
			requestType.setAmount(new BigDecimal(amount));
			StandardBizResponse response = utibaClient.cashin(requestType);

			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));

		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}
		return rs;
	}

	public Response reversal(String sessionId, String transId, String amount, String recipient, ApiUser user) {
		Response rs = new Response();
		try {

			KeyValuePairMap value = new KeyValuePairMap();
			KeyValuePair kv = new KeyValuePair();
			kv.setKey("biz_command");
			kv.setValue("reversal_request");
			value.getKeyValuePairs().add(kv);

			if (recipient != null) {
				KeyValuePair kvRec = new KeyValuePair();
				kvRec.setKey("subscriber");
				kvRec.setValue(recipient);
				value.getKeyValuePairs().add(kvRec);
			}

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			value.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			value.getKeyValuePairs().add(kvApiUser);

			ReverseRequestType requestType = new ReverseRequestType();
			requestType.setSessionid(sessionId);
			requestType.setTransactionID(Integer.valueOf(transId));
			requestType.setAmount(new BigDecimal(amount));
			requestType.setExtraTransData(value);
			StandardBizResponse response = utibaClient.reverse(requestType);

			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));

		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}
		return rs;
	}
	
	public Response reversal(String sessionId, String transId, String amount, String recipient,Parameter param, ApiUser user) {
		Response rs = new Response();
		try {
			
			KeyValuePairMap value = new KeyValuePairMap();
			KeyValuePair kv = new KeyValuePair();
			kv.setKey("biz_command");
			kv.setValue("reversal_request");
			value.getKeyValuePairs().add(kv);
			
			if (recipient != null) {
				KeyValuePair kvRec = new KeyValuePair();
				kvRec.setKey("subscriber");
				kvRec.setValue(recipient);
				value.getKeyValuePairs().add(kvRec);
			}
			
			KeyValuePair kvp = null;
			Iterator it = param.getKeyValueMap().entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (!pair.getKey().equals("signature") && !pair.getKey().equals("userid")
						&& !pair.getKey().equals("amount") && !pair.getKey().equals("extRef")
						&& !pair.getKey().equals("to")) {
					kvp = new KeyValuePair();
					kvp.setKey(String.valueOf(pair.getKey()));
					kvp.setValue(String.valueOf(pair.getValue()));
					value.getKeyValuePairs().add(kvp);
				}
				it.remove(); // avoids a ConcurrentModificationException
			}

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			value.getKeyValuePairs().add(kvChannel);
			
			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			value.getKeyValuePairs().add(kvApiUser);
			
			ReverseRequestType requestType = new ReverseRequestType();
			requestType.setSessionid(sessionId);
			requestType.setTransactionID(Integer.valueOf(transId));
			requestType.setAmount(new BigDecimal(amount));
			if (value.getKeyValuePairs().size() > 0)
				requestType.setExtraTransData(value);
			
			StandardBizResponse response = utibaClient.reverse(requestType);
			
			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
			
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}
		return rs;
	}

	public Response balance(String sessionId, ApiUser user) {
		Response rs = new Response();
		try {
			BalanceRequestType requestType = new BalanceRequestType();
			requestType.setSessionid(sessionId);

			// add for channelType
			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			requestType.setExtraTransData(kvpm);

			BalanceResponseType response = utibaClient.balance(requestType);
			rs.setBalance(String.valueOf(response.getAvail1()));
			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}

	public Response createCouponRemitance(String sessionId, String amount, String senderPhoneNumber,
			String recipientName, String senderName, String extRef, HashMap<String, String> parameters, ApiUser user) {
		Response rs = new Response();
		KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
		try {

			for (Map.Entry<String, String> entry : parameters.entrySet()) {
				KeyValuePair kv = new KeyValuePair();
				kv.setKey(entry.getKey());
				kv.setValue(entry.getValue());
				keyValuePairMap.getKeyValuePairs().add(kv);
			}

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);

			CreatecouponRequestType rt = new CreatecouponRequestType();
			rt.setSessionid(sessionId);
			rt.setAmount(new BigDecimal(amount));
			rt.setRecipient(senderPhoneNumber);
			rt.setRecipientName(recipientName);
			rt.setSenderName(senderName);
			rt.setCouponType(2);
			rt.setExtraTransData(keyValuePairMap);
			rt.setTransExtReference(extRef);
			CreatecouponResponseType response = utibaClient.createcoupon(rt);
			rs.setStatus(response.getResult());
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
			rs.setCouponId(response.getCouponid());
			rs.setTrxid(String.valueOf(response.getTransid()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
			log.error("failed createCouponRemitance(sessionId[" + sessionId + "])" + e);
		}
		return rs;
	}

	public Response createCouponRemitance(String sessionId, String amount, String recipient, String recipientName,
			String senderName, String sender, String identityNumber, String storeName, ApiUser user) {
		Response rs = new Response();
		try {

			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();

			KeyValuePair suppressSms = new KeyValuePair();
			suppressSms.setKey("suppress_sms");
			suppressSms.setValue("1");
			// keyValuePairMap.getKeyValuePairs().add(suppressSms);

			if (!Utils.isNullorEmptyString(sender)) {
				KeyValuePair senderNumber = new KeyValuePair();
				senderNumber.setKey(Constant.SENDER_PARAM);
				senderNumber.setValue(sender);
				keyValuePairMap.getKeyValuePairs().add(senderNumber);
			}

			if (!Utils.isNullorEmptyString(identityNumber)) {
				KeyValuePair id_no = new KeyValuePair();
				id_no.setKey(Constant.ID_NUMBER_PARAM);
				id_no.setValue(identityNumber);
				keyValuePairMap.getKeyValuePairs().add(id_no);
			}

			if (!Utils.isNullorEmptyString(storeName)) {
				KeyValuePair stName = new KeyValuePair();
				stName.setKey(Constant.STORE_NAME_PARAM);
				stName.setValue(identityNumber);
				keyValuePairMap.getKeyValuePairs().add(stName);
			}

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);

			CreatecouponRequestType rt = new CreatecouponRequestType();
			rt.setSessionid(sessionId);
			rt.setAmount(new BigDecimal(amount));
			rt.setRecipient(recipient);
			rt.setRecipientName(recipientName);
			rt.setSenderName(senderName);
			rt.setExtraTransData(keyValuePairMap);

			CreatecouponResponseType response = utibaClient.createcoupon(rt);
			rs.setStatus(response.getResult());
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
			rs.setCouponId(response.getCouponid());
			rs.setTrxid(String.valueOf(response.getTransid()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
			log.error("failed createCouponRemitance(sessionId[" + sessionId + "])" + e);
		}
		return rs;
	}

	public Response getCouponRemitance(String sessionId, String couponId, ApiUser user) {
		Response rs = new Response();
		try {

			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair suppressSms = new KeyValuePair();
			suppressSms.setKey("suppress_sms");
			suppressSms.setValue("1");
			keyValuePairMap.getKeyValuePairs().add(suppressSms);

			GetcouponRequestType requestType = new GetcouponRequestType();
			requestType.setSessionid(sessionId);
			requestType.setCouponid(couponId);

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);

			requestType.setExtraTransData(keyValuePairMap);
			GetcouponResponseType response = utibaClient.getcoupon(requestType);
			rs.setStatus(response.getResult());
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
			rs.setTrxid(String.valueOf(response.getTransid()));
			if (response.getResult() == Constant.RC_SUCCESS) {
				rs.getCouponDetails().setCouponId(response.getCouponid());
				rs.getCouponDetails().setSenderName(response.getSenderName());
				rs.getCouponDetails().setRecipientName(response.getRecipientName());
				rs.getCouponDetails().setAmount(String.valueOf(response.getAmount()));
				rs.getCouponDetails().setCouponTransId(response.getCoupontransid());
				Response getTransactionByIdResp = this.getTransactionById(sessionId, response.getCoupontransid(),
						Constant.SERVICE_GET_COUPON_DETAILS, user);
				rs.getCouponDetails().setIdentityNo(getTransactionByIdResp.getCouponDetails().getIdentityNo());
				rs.getCouponDetails().setSenderPhoneNumber(response.getRecipient());
				if (response.getRecipient().equals("123456"))
					rs.getCouponDetails()
							.setSenderPhoneNumber(getTransactionByIdResp.getCouponDetails().getSenderPhoneNumber());
				rs.getCouponDetails().setIdType(getTransactionByIdResp.getCouponDetails().getIdType());
				rs.getCouponDetails()
						.setRecipientPhoneNumber(getTransactionByIdResp.getCouponDetails().getRecipientPhoneNumber());
			}
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
			log.error("failed getCoupon(sessionId[" + sessionId + "] couponId[" + couponId + "])" + e);
		}

		return rs;
	}

	public Response cancelCoupon(String sessionId, String couponId, String initiator, ApiUser user) {
		Response rs = new Response();
		try {
			CancelcouponRequestType requestType = new CancelcouponRequestType();
			requestType.setSessionid(sessionId);
			requestType.setCouponid(couponId);

			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvp = new KeyValuePair();
			kvp.setKey("subscriber");
			kvp.setValue(initiator);

			// keyValuePairMap.getKeyValuePairs().add(kvp);

			KeyValuePair suppressSms = new KeyValuePair();
			suppressSms.setKey("suppress_sms");
			suppressSms.setValue("1");
			keyValuePairMap.getKeyValuePairs().add(suppressSms);

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);

			requestType.setExtraTransData(keyValuePairMap);
			StandardBizResponse response = utibaClient.cancelcoupon(requestType);
			rs.setStatus(response.getResult());
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
			rs.setTrxid(String.valueOf(response.getTransid()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
			log.error("failed cancelCoupon(sessionId[" + sessionId + "] couponId[" + couponId + "])" + e);
		}
		return rs;
	}

	public Response cancelCouponRemitance(String sessionId, String couponId, String password, String extRef,
			String storeName, ApiUser user) {
		Response rs = new Response();
		try {

			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair suppressSms = new KeyValuePair();
			suppressSms.setKey("suppress_sms");
			suppressSms.setValue("1");
			keyValuePairMap.getKeyValuePairs().add(suppressSms);

			KeyValuePair storeNameKv = new KeyValuePair();
			storeNameKv.setKey(Constant.STORE_NAME_PARAM_LITE);
			storeNameKv.setValue(storeName);
			keyValuePairMap.getKeyValuePairs().add(storeNameKv);

			CancelcouponRequestType requestType = new CancelcouponRequestType();
			requestType.setSessionid(sessionId);
			requestType.setCouponid(couponId);
			requestType.setPassword(password);
			requestType.setTransExtReference(extRef);

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);

			requestType.setExtraTransData(keyValuePairMap);
			StandardBizResponse response = utibaClient.cancelcoupon(requestType);
			rs.setStatus(response.getResult());
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
			rs.setTrxid(String.valueOf(response.getTransid()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
			log.error("failed cancelCouponRemitance(sessionId[" + sessionId + "] couponId[" + couponId + "])" + e);
		}

		return rs;
	}

	public Response remiteCoupon(String sessionId, String couponId, String extRef, HashMap<String, String> parameters,
			ApiUser user) {
		Response rs = new Response();
		try {

			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();

			for (Map.Entry<String, String> entry : parameters.entrySet()) {
				KeyValuePair kv = new KeyValuePair();
				kv.setKey(entry.getKey());
				kv.setValue(entry.getValue());
				keyValuePairMap.getKeyValuePairs().add(kv);
			}

			/*
			 * KeyValuePair suppressSms = new KeyValuePair();
			 * suppressSms.setKey("suppress_sms"); suppressSms.setValue("1");
			 * keyValuePairMap.getKeyValuePairs().add(suppressSms);
			 */

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);

			RemitcouponRequestType requestType = new RemitcouponRequestType();
			requestType.setSessionid(sessionId);
			requestType.setCouponid(couponId);
			// requestType.setTransExtReference(extRef);
			requestType.setExtraTransData(keyValuePairMap);
			StandardBizResponse response = utibaClient.remitcoupon(requestType);
			rs.setStatus(response.getResult());
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
			rs.setTrxid(String.valueOf(response.getTransid()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
			log.error("failed cancelCouponRemitance(sessionId[" + sessionId + "] couponId[" + couponId + "])" + e);
		}

		return rs;
	}

	public Response createCoupon(String sessionId, ApiUser user) {
		Response rs = new Response();
		try {
			Date date = Utils.getExpiredTokenTime(expiredHour);
			Timestamp timestamp = new Timestamp(date.getTime());
			CreatecouponRequestType rt = new CreatecouponRequestType();
			rt.setCouponType(1);
			rt.setExpiry(timestamp.getTime());
			rt.setSessionid(sessionId);

			// add for channelType
			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			rt.setExtraTransData(keyValuePairMap);

			CreatecouponResponseType response = utibaClient.createcoupon(rt);
			rs.setStatus(response.getResult());
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
			rs.setCouponId(response.getCouponid().substring(0, 6));
			rs.setTrxid(String.valueOf(response.getTransid()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}
		return rs;
	}

	// TEOT
	public Response createCoupon(String sessionId, int hour, String channel, ApiUser user) {
		Response rs = new Response();

		try {
			Date date = Utils.getExpiredTokenTime(hour);
			Timestamp timestamp = new Timestamp(date.getTime());
			CreatecouponRequestType rt = new CreatecouponRequestType();
			rt.setCouponType(1);
			rt.setExpiry(timestamp.getTime());
			rt.setSessionid(sessionId);

			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvp = new KeyValuePair();
			kvp.setKey("suppress_sms");
			kvp.setValue("1");

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey(Constant.CHANNEL_STR);
			kvChannel.setValue(channel);

			keyValuePairMap.getKeyValuePairs().add(kvp);
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			// add for channelType
			KeyValuePair kvChannelType = new KeyValuePair();
			kvChannelType.setKey("channel_type");
			kvChannelType.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannelType);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);

			rt.setExtraTransData(keyValuePairMap);

			CreatecouponResponseType response = utibaClient.createcoupon(rt);
			rs.setStatus(response.getResult());
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
			rs.setCouponId(response.getCouponid());
			rs.setTrxid(String.valueOf(response.getTransid()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}
		return rs;
	}

	public Response merchantTransfer(String sessionId, String amount, String to,
			com.utiba.delirium.ws.misc.KeyValuePairMap kpvmap, ApiUser user) {
		Response rs = new Response();
		try {
			KeyValuePair kvp = new KeyValuePair();
			kvp.setKey("biz_command");
			kvp.setValue("merchant_buy");

			kpvmap.getKeyValuePairs().add(kvp);

			TransferRequestType trt = new TransferRequestType();
			trt.setAmount(new BigDecimal(amount));
			trt.setTo(to);
			trt.setSessionid(sessionId);
			trt.setSuppressConfirm(true);

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kpvmap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kpvmap.getKeyValuePairs().add(kvApiUser);

			trt.setExtraTransData(kpvmap);
			StandardBizResponse result = utibaClient.transfer(trt);
			rs.setStatus(result.getResult());
			rs.setTrxid(String.valueOf(result.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(result.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;

	}

	public Response merchantTransfer(String sessionId, String amount, String to, HashMap<String, String> map,
			ApiUser user) {
		Response rs = new Response();
		try {
			KeyValuePairMap kpvmap = new KeyValuePairMap();
			KeyValuePair kvp = new KeyValuePair();
			kvp.setKey("biz_command");
			kvp.setValue("merchant_buy");
			kpvmap.getKeyValuePairs().add(kvp);

			TransferRequestType trt = new TransferRequestType();
			trt.setAmount(new BigDecimal(amount));
			trt.setTo(to);
			trt.setSessionid(sessionId);
			trt.setSuppressConfirm(true);

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kpvmap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kpvmap.getKeyValuePairs().add(kvApiUser);

			for (Map.Entry<String, String> entry : map.entrySet()) {
				KeyValuePair kvpMap = new KeyValuePair();
				if (entry.getKey().equalsIgnoreCase("suppress_sms")) {
					if (map.get("suppress_sms").equals("1")) {
						kvpMap.setKey("suppress_sms");
						kvpMap.setValue("1");
						kpvmap.getKeyValuePairs().add(kvpMap);
					}
				}

				if (!entry.getKey().equalsIgnoreCase(Constant.SIGNATURE_PARAM)
						&& !entry.getKey().equalsIgnoreCase(Constant.USERID_PARAM)) {
					kvpMap.setKey(entry.getKey());
					kvpMap.setValue(entry.getValue());
					kpvmap.getKeyValuePairs().add(kvpMap);
				}

			}

			trt.setExtraTransData(kpvmap);
			StandardBizResponse result = utibaClient.transfer(trt);
			rs.setStatus(result.getResult());
			rs.setMsg(dataManager.getErrorMsg(result.getResult()));
			rs.setTrxid(String.valueOf(result.getTransid()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;

	}

	public Response nfcSettlement(String sessionId, String amount, String to, String extRef, ApiUser user) {
		Response rs = new Response();
		try {

			TransferRequestType trt = new TransferRequestType();
			trt.setAmount(new BigDecimal(amount));
			trt.setTo(to);
			trt.setSessionid(sessionId);
			trt.setSuppressConfirm(true);
			trt.setTransExtReference(extRef);

			// add for channelType
			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			trt.setExtraTransData(keyValuePairMap);

			StandardBizResponse result = utibaClient.transfer(trt);
			rs.setStatus(result.getResult());
			rs.setTrxid(String.valueOf(result.getTransid()));

			if (result.getResult() == Constant.RC_SESSION_EXPIRED
					&& result.getResultNamespace().equalsIgnoreCase("session")) {
				log.info("Session Expired, try to get the new one......");
				dataManager.setGlobalNFCSessionId();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				rs = this.nfcSettlement(dataManager.getGlobalNFCSessionId(), amount, to, extRef, user);
			}
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}

	public Response moneyTransfer(String sessionId, String amount, String to, String extRef, ApiUser user) {
		Response rs = new Response();
		try {

			TransferRequestType trt = new TransferRequestType();
			trt.setAmount(new BigDecimal(amount));
			trt.setTo(to);
			trt.setSessionid(sessionId);
			trt.setSuppressConfirm(true);
			if (!Utils.isNullorEmptyString(extRef))
				trt.setTransExtReference(extRef);

			// add for channelType
			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			trt.setExtraTransData(keyValuePairMap);

			StandardBizResponse result = utibaClient.transfer(trt);
			rs.setStatus(result.getResult());
			rs.setTrxid(String.valueOf(result.getTransid()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}

	public Response moneyTransfer(String sessionId, String amount, String to, String extRef, Parameter param,
			ApiUser user) {
		Response rs = new Response();
		try {
			TransferRequestType trt = new TransferRequestType();
			trt.setAmount(new BigDecimal(amount));
			trt.setTo(to);
			trt.setSessionid(sessionId);
			trt.setSuppressConfirm(true);
			if (!Utils.isNullorEmptyString(extRef))
				trt.setTransExtReference(extRef);

			com.utiba.delirium.ws.misc.KeyValuePairMap keyValuePairMap = new com.utiba.delirium.ws.misc.KeyValuePairMap();
			KeyValuePair kvp = null;
			Iterator it = param.getKeyValueMap().entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (!pair.getKey().equals("signature") && !pair.getKey().equals("userid")
						&& !pair.getKey().equals("amount") && !pair.getKey().equals("extRef")
						&& !pair.getKey().equals("to")) {
					kvp = new KeyValuePair();
					kvp.setKey(String.valueOf(pair.getKey()));
					kvp.setValue(String.valueOf(pair.getValue()));
					keyValuePairMap.getKeyValuePairs().add(kvp);
				}
				it.remove(); // avoids a ConcurrentModificationException
			}

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);

			if (keyValuePairMap.getKeyValuePairs().size() > 0)
				trt.setExtraTransData(keyValuePairMap);

			StandardBizResponse result = utibaClient.transfer(trt);
			rs.setStatus(result.getResult());
			rs.setMsg(dataManager.getErrorMsg(result.getResult()));
			rs.setTrxid(String.valueOf(result.getTransid()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}

	public Response creditAirtimeInquiry(String initiator, String to, String amount, String transId, ApiUser user) {
		Response rs = new Response();

		try {
			umarketspiws.v1.KeyValuePair voucherType = new umarketspiws.v1.KeyValuePair();
			voucherType.setKey("voucher_type");
			voucherType.setValue("Regular");
			umarketspiws.v1.KeyValuePair recipient = new umarketspiws.v1.KeyValuePair();
			recipient.setKey("recipient");
			recipient.setValue(to);
			umarketspiws.v1.KeyValuePair supressConfirm = new umarketspiws.v1.KeyValuePair();
			supressConfirm.setKey("suppress_confirm");
			supressConfirm.setValue("1");
			umarketspiws.v1.KeyValuePair kvChannel = new umarketspiws.v1.KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));

			umarketspiws.v1.KeyValuePair kvApiUser = new umarketspiws.v1.KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));

			umarketspiws.v1.KeyValuePairMap keyValuePairMap = new umarketspiws.v1.KeyValuePairMap();
			keyValuePairMap.getKeyValuePair().add(kvChannel);
			keyValuePairMap.getKeyValuePair().add(kvApiUser);
			keyValuePairMap.getKeyValuePair().add(voucherType);
			keyValuePairMap.getKeyValuePair().add(recipient);
			keyValuePairMap.getKeyValuePair().add(supressConfirm);

			// do creditAirtime
			CreditAirtimeRequest creditAirtimeRequest = new CreditAirtimeRequest();
			creditAirtimeRequest.setExtraParameters(keyValuePairMap);
			creditAirtimeRequest.setTransactionId(Long.valueOf(transId));
			creditAirtimeRequest.setMsisdn(initiator);
			creditAirtimeRequest.setAmount(new BigDecimal(amount));

			CreditAirtimeResponse crt = isc.creditAirtime(creditAirtimeRequest);

			log.info("CreditAirtimeResponse.getResultCode(): " + crt.getResultCode());

			if (crt.getResultCode().toString().equalsIgnoreCase(Constant.SUCCESS_TEXT)) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// if success, do isAirtimeAccountValid
				umarketspiws.v1.KeyValuePairMap iaMap = new umarketspiws.v1.KeyValuePairMap();
				umarketspiws.v1.KeyValuePair transidKVP = new umarketspiws.v1.KeyValuePair();
				transidKVP.setKey("transid");
				transidKVP.setValue(crt.getServiceTransactionId());
				iaMap.getKeyValuePair().add(kvChannel);
				iaMap.getKeyValuePair().add(kvApiUser);
				iaMap.getKeyValuePair().add(transidKVP);
				IsAirtimeAccountValidRequest iaRequest = new IsAirtimeAccountValidRequest();
				iaRequest.setTransactionId(Long.valueOf(transId));
				iaRequest.setExtraParameters(iaMap);
				iaRequest.setMsisdn(initiator);
				IsAirtimeAccountValidResponse iaResponse = isc.isAirtimeAccountValid(iaRequest);

				log.info("IsAirtimeAccountValidResponse: " + iaResponse.getResultCode().value());

				if (iaResponse.getResultCode().value().trim().toUpperCase().equalsIgnoreCase(Constant.SUCCESS_TEXT)) {
					String price = "0";
					log.info("iaResponse.getServiceTransactionId(): " + iaResponse.getServiceTransactionId());
					log.info("iaResponse.getErrorMessage(): " + iaResponse.getErrorMessage());

					List<umarketspiws.v1.KeyValuePair> list = iaResponse.getExtraParameters().getKeyValuePair();
					log.info("List<umarketspiws.v1.KeyValuePair> list: " + list.size());
					for (umarketspiws.v1.KeyValuePair obj : list) {
						log.info("obj.getKey(): " + obj.getKey() + " obj.getValue(): " + obj.getValue());
						if (obj.getKey().equalsIgnoreCase("price")) {
							// System.out.println("PRICE: "+obj.getValue());
							price = obj.getValue();
							break;
						}
					}

					rs.setPrice(price);
					rs.setStatus(Constant.RC_SUCCESS);
					rs.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
					rs.setTrxid(crt.getServiceTransactionId());

				}

			} else {
				rs.setStatus(Constant.RC_TRANSACTIN_FAILED);
				rs.setMsg(dataManager.getErrorMsg(Constant.RC_TRANSACTIN_FAILED));
			}
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;

	}

	public Response commitCreditAirtime(String initiator, String pin, String serviceTransactionId, String transactionId,
			String operatorName, ApiUser user) {
		log.info("commitCreditAirtime: initiator: " + initiator + " serviceTransactionId: " + serviceTransactionId
				+ " transactionId:" + transactionId);
		Response rs = new Response();

		try {
			CommitRequest cmr = new CommitRequest();
			umarketspiws.v1.KeyValuePairMap keyValuePairMap = new umarketspiws.v1.KeyValuePairMap();

			umarketspiws.v1.KeyValuePair kvp1 = new umarketspiws.v1.KeyValuePair();
			kvp1.setKey("initiator");
			kvp1.setValue(initiator);
			umarketspiws.v1.KeyValuePair kvp2 = new umarketspiws.v1.KeyValuePair();
			kvp2.setKey("pin");
			kvp2.setValue(pin);
			umarketspiws.v1.KeyValuePair kvp3 = new umarketspiws.v1.KeyValuePair();
			kvp3.setKey("transid");
			kvp3.setValue(serviceTransactionId);
			umarketspiws.v1.KeyValuePair kvp4 = new umarketspiws.v1.KeyValuePair();
			kvp4.setKey("action");
			kvp4.setValue("commit");
			umarketspiws.v1.KeyValuePair kvp5 = new umarketspiws.v1.KeyValuePair();
			kvp5.setKey("OPERATOR");
			kvp5.setValue(operatorName);
			umarketspiws.v1.KeyValuePair kvChannel = new umarketspiws.v1.KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));

			umarketspiws.v1.KeyValuePair kvApiUser = new umarketspiws.v1.KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));

			keyValuePairMap.getKeyValuePair().add(kvp1);
			keyValuePairMap.getKeyValuePair().add(kvp2);
			keyValuePairMap.getKeyValuePair().add(kvp3);
			keyValuePairMap.getKeyValuePair().add(kvp4);
			keyValuePairMap.getKeyValuePair().add(kvp5);
			keyValuePairMap.getKeyValuePair().add(kvChannel);
			keyValuePairMap.getKeyValuePair().add(kvApiUser);
			cmr.setExtraParameters(keyValuePairMap);
			cmr.setServiceTransactionId(serviceTransactionId);
			cmr.setTransactionId(Long.valueOf(transactionId));
			CommitResponse cr = isc.commit(cmr);
			log.debug("Commit ResultCode: " + cr.getResultCode().value());
			if (cr.getServiceTransactionId() != null && cr.getResultCode().value().equalsIgnoreCase("Success")) {
				rs.setStatus(Constant.RC_SUCCESS);
				rs.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
				rs.setTrxid(cr.getServiceTransactionId());
			} else {
				rs.setStatus(Constant.RC_TRANSACTIN_FAILED);
				rs.setMsg(dataManager.getErrorMsg(Constant.RC_TRANSACTIN_FAILED));
			}

		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}

	public Response confirm(String sessionId, int transId, ApiUser user) {
		log.info("Confirm Billpay Request with sessionid: " + sessionId + " and transactionid: " + transId);

		Response rs = new Response();
		try {
			ConfirmRequestType confirmRequestType = new ConfirmRequestType();
			confirmRequestType.setSessionid(sessionId);
			confirmRequestType.setTransid(transId);
			confirmRequestType.setAction("commit");
			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			confirmRequestType.setExtraTransData(keyValuePairMap);

			StandardBizResponse sbr = utibaClient.confirm(confirmRequestType);
			rs.setTrxid(String.valueOf(sbr.getTransid()));
			rs.setStatus(sbr.getResult());
			rs.setMsg(dataManager.getErrorMsg(sbr.getResult()));
			log.info("confirm StandardBizResponse: " + sbr.getResult());
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}
	
	public Response confirmNoTransid(String sessionId, ApiUser user) {
		log.info("Confirm Billpay Request with sessionid: " + sessionId );

		Response rs = new Response();
		try {
			ConfirmRequestType confirmRequestType = new ConfirmRequestType();
			confirmRequestType.setSessionid(sessionId);
			confirmRequestType.setAction("commit");
			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			confirmRequestType.setExtraTransData(keyValuePairMap);

			StandardBizResponse sbr = utibaClient.confirm(confirmRequestType);
			rs.setTrxid(String.valueOf(sbr.getTransid()));
			rs.setStatus(sbr.getResult());
			rs.setMsg(dataManager.getErrorMsg(sbr.getResult()));
			log.info("confirm StandardBizResponse: " + sbr.getResult());
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}

	public String getValueFromTransactionById(String sessionId, int id, String key, ApiUser user) {
		String value = "";
		GetTransactionByIDRequest getTransactionByIDRequest = new GetTransactionByIDRequest();
		getTransactionByIDRequest.setSessionid(sessionId);
		getTransactionByIDRequest.setID(id);
		KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
		KeyValuePair kvChannel = new KeyValuePair();
		kvChannel.setKey("channel_type");
		kvChannel.setValue(String.valueOf(user.getChannelType()));
		keyValuePairMap.getKeyValuePairs().add(kvChannel);

		KeyValuePair kvApiUser = new KeyValuePair();
		kvApiUser.setKey("api_user");
		kvApiUser.setValue(String.valueOf(user.getUserId()));
		keyValuePairMap.getKeyValuePairs().add(kvApiUser);
		getTransactionByIDRequest.setExtraTransData(keyValuePairMap);
		TransactionResponseType trst = utibaClient.getTransactionByID(getTransactionByIDRequest);

		List<TransactionData> trxdtList = trst.getTransaction().getParams();
		for (TransactionData obj : trxdtList) {
			log.debug("tdObj.getKey(): " + obj.getKey() + " tdObj.getValue():" + obj.getValue());
			if (obj.getKey().equalsIgnoreCase(key)) {
				value = obj.getValue();
				break;
			}
		}
		return value;
	}

	public Response getTransactionById(String sessionId, int id, int requestType, ApiUser user) {
		Response rs = new Response();
		try {
			GetTransactionByIDRequest getTransactionByIDRequest = new GetTransactionByIDRequest();
			getTransactionByIDRequest.setSessionid(sessionId);
			getTransactionByIDRequest.setID(id);
			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			getTransactionByIDRequest.setExtraTransData(keyValuePairMap);
			rs.setTrxid(String.valueOf(id));
			TransactionResponseType trst = utibaClient.getTransactionByID(getTransactionByIDRequest);
			log.info("GETTRANSACTIONBYID: " + trst.getResult());
//			log.info("trst.getTransaction().getResult(): " + trst.getTransaction().getResult());
			int uMarketResonse = -99;
			if(trst.getTransaction()!=null){
				if (!Utils.isNullorEmptyString(trst.getTransaction().getResult())) {
					uMarketResonse = Integer.valueOf(trst.getTransaction().getResult().trim().replace("umarket:", ""));
					log.info("uMarketResonse: " + uMarketResonse);
					rs.setStatus(uMarketResonse);
					rs.setMsg(dataManager.getErrorMsg(uMarketResonse));
				} else {
					rs.setStatus(Constant.RC_TRANSACTION_NOT_FOUND);
					rs.setMsg(dataManager.getErrorMsg(Constant.RC_TRANSACTION_NOT_FOUND));
				}
			}else{
				rs.setStatus(Constant.RC_TRANSACTION_NOT_FOUND);
				rs.setMsg(dataManager.getErrorMsg(Constant.RC_TRANSACTION_NOT_FOUND));
			}

			if (trst.getResult() == Constant.RC_SUCCESS && uMarketResonse == Constant.RC_SUCCESS) {
				log.info("Service Type ["+requestType+"]");
				if (requestType == Constant.SERVICE_QUERY_BILLPAY) {
					List<TransactionData> trxDateList = trst.getTransaction().getParams();
					for (TransactionData tdObj : trxDateList) {
						// System.out.println("tdList size:
						// "+trxDateList.size());
						// System.out.println("tdObj.getKey(): "
						// +tdObj.getKey()+" tdObj.getValue():
						// "+tdObj.getValue());
						if (tdObj.getKey().equals("price"))
							rs.setPrice(tdObj.getValue());
						else if (tdObj.getKey().equals("recipient"))
							rs.setMsisdn(tdObj.getValue());
						else if (tdObj.getKey().equals("namapelanggan") || tdObj.getKey().equals("nama"))
							rs.setName(tdObj.getValue());
						else if (tdObj.getKey().equals("admin"))
							rs.setFee(tdObj.getValue());
					}
				} else if (requestType == Constant.SERVICE_GET_COUPON_DETAILS) {
					rs.getCouponDetails().setCouponTransId(trst.getTransaction().getParentID());
					List<TransactionData> trxDateList = trst.getTransaction().getParams();
					for (TransactionData tdObj : trxDateList) {
						log.debug("key:" + tdObj.getKey() + " value:" + tdObj.getValue());
						if (tdObj.getKey().equals(Constant.SENDER_ID_NO_PARAM_LITE)) {
							rs.getCouponDetails().setIdentityNo(tdObj.getValue());
						} else if (tdObj.getKey().equals(Constant.RECIPIENT_PHONE_NUMBER_PARAM_LITE)) {
							rs.getCouponDetails().setRecipientPhoneNumber(tdObj.getValue());
						} else if (tdObj.getKey().equals(Constant.SENDER_ID_TYPE_PARAM_LITE)) {
							rs.getCouponDetails().setIdType(tdObj.getValue());
						} else if (tdObj.getKey().equals(Constant.SENDER_PHONE_NUMBER_PARAM_LITE)) {
							rs.getCouponDetails().setSenderPhoneNumber(tdObj.getValue());
						}
					}
				} else if (requestType == 999999) {
					List<TransactionData> extTranslist = trst.getTransaction().getParams();
					rs.setMsisdn(trst.getTransaction().getInitiator());
					String trxdetail = "";
					for (TransactionData obj : extTranslist) {
						if (obj.getKey().equalsIgnoreCase("CustomerMessage"))
							rs.setVoucherCode(obj.getValue());
						if (!obj.getKey().equalsIgnoreCase("api_user")) {
							if (!obj.getKey().equalsIgnoreCase("channel_type")) {
								trxdetail = trxdetail + obj.getKey() + "|" + obj.getValue() + ",";
							}
						}
					}
					if (trxdetail != null && trxdetail.length() > 0
							&& trxdetail.charAt(trxdetail.length() - 1) == 'x') {
						trxdetail = trxdetail.substring(0, trxdetail.length() - 1);
					}
					rs.setName(trst.getTransaction().getType());
					rs.setDetailTagihan(trxdetail);

					if (rs.getName().equals("cashin")) {
						List<TransactionParty> parties = trst.getTransaction().getParties();
						for (TransactionParty party : parties) {
							List<KeyValuePair> datas = party.getParty().getAgent().getAgentData();
							for (KeyValuePair kvp : datas) {
								if (kvp.getValue().equalsIgnoreCase("subscribers")) {
									rs.setMsisdn(
											party.getParty().getMSISDN() + "|" + party.getParty().getAgent().getName());
								}
							}
						}
					}
				} else if (requestType == 999998) {
					HashMap<String, String> map = new HashMap<>();
					map.put("transid", String.valueOf(trst.getTransid()));
					map.put("transidQuery", String.valueOf(trst.getTransaction().getID()));
					map.put("type_trx", trst.getTransaction().getType());
					map.put("initiator", trst.getTransaction().getInitiator());
					map.put("trx_date", trst.getTransaction().getCreated().toString());
					map.put("result", trst.getTransaction().getResult().replace("umarket:", ""));
					for (TransactionData data : trst.getTransaction().getParams()) {
						map.put(data.getKey(), data.getValue());
					}
					rs.setDetailTrx(map);
					rs.setName(trst.getTransaction().getType());
				} else {
					List<ExtTransaction> extTranslist = trst.getTransaction().getExtTransactions();
					rs.setBillpayQueryState(false);
					rs.setBillpayState(false);
					for (ExtTransaction obj : extTranslist) {
						if (obj.getType().equals("billpay_query")) {
							if (obj.getState() != 1 && obj.getState() != 0) {
								rs.setBillpayQueryState(true);
							}
						} else if (obj.getType().equals("billpay")) {
							if (obj.getState() != 1 && obj.getState() != 0) {
								rs.setBillpayState(true);
							}
							List<TransactionData> tdList = obj.getParams();
							for (TransactionData tdObj : tdList) {
								// System.out.println("tdList size:
								// "+tdList.size());
								// System.out.println("tdObj.getKey(): "
								// +tdObj.getKey()+" tdObj.getValue():
								// "+tdObj.getValue());
								if (tdObj.getKey().equals("TransAmt"))
									rs.setPrice(tdObj.getValue());
								else if (tdObj.getKey().equals("MSISDN"))
									rs.setMsisdn(tdObj.getValue());
								else if (tdObj.getKey().equals("CustName"))
									rs.setName(tdObj.getValue());
							}
						}

					}
				}
				log.debug("Result :"+rs.toString());
			}
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}
		return rs;
	}

	public Response getTransactionDetail(String sessionId, int id, ApiUser user) {
		Response rs = new Response();
		Prefix telcoObj = null;
		TransactionResponseType trst = null;
		try {
			GetTransactionByIDRequest getTransactionByIDRequest = new GetTransactionByIDRequest();
			getTransactionByIDRequest.setSessionid(sessionId);
			getTransactionByIDRequest.setID(id);
			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			getTransactionByIDRequest.setExtraTransData(keyValuePairMap);
			rs.setTrxid(String.valueOf(id));
			trst = utibaClient.getTransactionByID(getTransactionByIDRequest);
			int uMarketResonse = -99;
			if (!Utils.isNullorEmptyString(trst.getTransaction().getResult())) {
				uMarketResonse = Integer.valueOf(trst.getTransaction().getResult().trim().replace("umarket:", ""));
				log.info("uMarketResonse: " + uMarketResonse);
				rs.setStatus(uMarketResonse);
				rs.setMsg(dataManager.getErrorMsg(uMarketResonse));
			}
			if (trst.getResult() == Constant.RC_SUCCESS && uMarketResonse == Constant.RC_SUCCESS) {
				List<TransactionData> extTranslist = trst.getTransaction().getParams();
				List<TransactionParty> transParty = trst.getTransaction().getParties();
				HashMap<String, String> utibaDataMap = new HashMap<String, String>();
				HashMap<String, String> detailData = new HashMap<String, String>();

				for (TransactionData obj : extTranslist) {
					utibaDataMap.put(obj.getKey(), obj.getValue());
				}

				// filtering data to show
				switch (trst.getTransaction().getType()) {
				case "coupontransfer":
					if (utibaDataMap.get("service_type").equalsIgnoreCase("banktransfer")) {
						detailData.put("trx_name", "Bank Transfer");
						detailData.put("sender",
								trst.getTransaction().getParties().get(0).getParty().getAgent().getMSISDN());
						detailData.put("sender_name",
								trst.getTransaction().getParties().get(0).getParty().getAgent().getName());
					} else {
						detailData.put("trx_name", "Token Payment");
						detailData.put("token_no", utibaDataMap.get("couponid").substring(0, 6));
						detailData.put("merchant_name", utibaDataMap.get("outletid"));
					}
					detailData.put("amount", utibaDataMap.get("amount"));
					detailData.put("ext_reference", utibaDataMap.get("trans_ext_reference"));
					detailData.put("trx_date", trst.getTransaction().getLastModified().toString());
					break;
				case "transfer":
					if (trst.getTransaction().getInitiator().equalsIgnoreCase("ajcashin")) {
						detailData.put("trx_name", "Bank Cashin");
						detailData.put("sender", trst.getTransaction().getInitiatorEntity().getAgent().getMSISDN());
						detailData.put("sender_name", trst.getTransaction().getInitiatorEntity().getAgent().getName());
					} else {
						detailData.put("trx_name", "P2P Transfer");
						for (TransactionParty obj : transParty) {
							if (obj.getType() == 7) {
								detailData.put("sender", obj.getParty().getMSISDN());
								detailData.put("sender_name", obj.getParty().getAgent().getName());
							}
							if (obj.getType() == 6) {
								detailData.put("receiver", obj.getParty().getMSISDN());
								detailData.put("receiver_name", obj.getParty().getAgent().getName());
								List<KeyValuePair> agentData = obj.getParty().getAgent().getAgentData();
								for (KeyValuePair kp : agentData) {
									if (kp.getKey().equalsIgnoreCase("primary_group")) {
										if (Utils.checkContains(kp.getValue(), "merchants")) {
											detailData.put("trx_name", "Cashout Merchant");
										}
									}
								}
							}
						}
					}
					detailData.put("amount", utibaDataMap.get("amount"));
					detailData.put("ext_reference", utibaDataMap.get("trans_ext_reference"));
					detailData.put("trx_date", trst.getTransaction().getLastModified().toString());
					break;
				case "outletbillpay":
					detailData.put("trx_name", "Billpayment");
					detailData.put("amount", utibaDataMap.get("price"));
					detailData.put("fee", utibaDataMap.get("admin"));
					detailData.put("ext_reference", utibaDataMap.get("trans_ext_reference"));
					// Nexmedia Diff
					String idpel = utibaDataMap.get("idpelanggan");
					try {
						if (Utils.checkContains(detailData.get("payee"), "speedy")) {
							detailData.put("trx_name", "Billpayment");
							detailData.put("payee", "Speedy");
						}

						if (Utils.checkContains(idpel, "Nexmedia:")) {
							idpel = idpel.split("Nexmedia:")[1];
						}
					} catch (Exception ex) {
						log.error("Error Get Details Trx [" + ExceptionUtils.getFullStackTrace(ex) + "]");
					}
					detailData.put("customer_id", idpel);
					detailData.put("customer_name", utibaDataMap.get("namapelanggan"));
					detailData.put("trx_date", trst.getTransaction().getLastModified().toString());
					String token = "";
					// Nexmedia-Firstmedia diff
					if (!Utils.isNullorEmptyString(utibaDataMap.get("service_name"))) {
						detailData.put("payee", utibaDataMap.get("service_name"));
						if (Utils.checkContains(utibaDataMap.get("service_name"), "speedy")) {
							detailData.put("trx_name", "Billpayment");
							detailData.put("payee", "Speedy");
						}

					} else {
						String biller = utibaDataMap.get("payment");
						try {
							if (Utils.checkContains(utibaDataMap.get("payment"), "Firstmedia")) {
								biller = "Firstmedia";
							} else if (Utils.checkContains(utibaDataMap.get("payment"), "NexMediaPay")) {
								biller = "Nexmedia";
							} else if (Utils.checkContains(utibaDataMap.get("payment"), "KCJ")) {
								biller = "KCJ";
								idpel = utibaDataMap.get("subscriber");
								detailData.put("customer_id", idpel);
							}
						} catch (Exception ex) {
							log.error("Error Get Details Trx [" + ExceptionUtils.getFullStackTrace(ex) + "]");
						}
						detailData.put("payee", biller);
					}
					if (!Utils.isNullorEmptyString(utibaDataMap.get("target"))) {
						if (utibaDataMap.get("target").equalsIgnoreCase("orangetv")) {
							token = utibaDataMap.get("CustomerMessage").split("snorangeid: ")[1];
							detailData.put("payee", utibaDataMap.get("target"));
						}
					}
					if (!Utils.isNullorEmptyString(utibaDataMap.get("reference"))) {
						try {
							if (Utils.checkContains(utibaDataMap.get("reference"), "8090")) {
								token = utibaDataMap.get("CustomerMessage").split(",")[1].split(":")[1];
								detailData.put("payee", "Token Listrik");
							} else if (Utils.checkContains(utibaDataMap.get("reference"), "SERVICEID|9002")) {
								token = utibaDataMap.get("CustomerMessage").split("TOKEN No.")[1].split("-Untuk ")[0];
								detailData.put("payee", "Token Listrik");
							} else if (Utils.checkContains(utibaDataMap.get("reference"), "SERVICEID|9003")) {
								detailData.put("payee", "PLN Pascabayar");
							} else if (Utils.checkContains(utibaDataMap.get("reference"), "8089")) {
								detailData.put("payee", "PLN Pascabayar");
							}
						} catch (Exception ex) {
							log.error("Error Get Details Trx [" + ExceptionUtils.getFullStackTrace(ex) + "]");
						}
					}
					if (!Utils.isNullorEmptyString(token)) {
						detailData.put("token", token);
					}

					if (!Utils.isNullorEmptyString(utibaDataMap.get("idpelanggan"))) {
						telcoObj = dataManager.getTelcoIdByPrefix(utibaDataMap.get("idpelanggan"));
						if (!Utils.isNullorEmptyString(telcoObj.getName())) {
							detailData.put("telco", telcoObj.getName());
							detailData.put("trx_name", "Airtime Topup");
							if (detailData.get("payee").equalsIgnoreCase("TELKOM TELEPON")) {
								detailData.put("telco", "Telkom");
								detailData.put("trx_name", "Billpayment");
								detailData.put("payee", "Telkom");
							}
						}
					}
					break;
				case "cashin":
					detailData.put("trx_name", "Cashin");
					detailData.put("sender", trst.getTransaction().getInitiatorEntity().getAgent().getMSISDN());
					detailData.put("sender_name", trst.getTransaction().getInitiatorEntity().getAgent().getName());
					detailData.put("amount", utibaDataMap.get("amount"));
					detailData.put("ext_reference", utibaDataMap.get("trans_ext_reference"));
					detailData.put("trx_date", trst.getTransaction().getLastModified().toString());
					break;
				case "topup":
					detailData.put("trx_name", "Airtime Topup");
					detailData.put("customer_id", utibaDataMap.get("recipient"));
					telcoObj = dataManager.getTelcoIdByPrefix(utibaDataMap.get("recipient"));
					if (!Utils.isNullorEmptyString(telcoObj.getName())) {
						detailData.put("telco", telcoObj.getName());
					}
					int fee = Integer.parseInt(utibaDataMap.get("price"))
							- Integer.parseInt(utibaDataMap.get("amount"));
					detailData.put("fee", String.valueOf(fee));
					detailData.put("amount", utibaDataMap.get("amount"));
					detailData.put("ext_reference", utibaDataMap.get("trans_ext_reference"));
					detailData.put("trx_date", trst.getTransaction().getLastModified().toString());
					break;
				case "createcoupon":
					if (!Utils.isNullorEmptyString(utibaDataMap.get("coupon_type"))) {
						if (utibaDataMap.get("coupon_type").equals("2")) {
							detailData.put("trx_name", "Create MTCN");
							detailData.put("sender_name", utibaDataMap.get("sender_name"));
							detailData.put("receiver_name", utibaDataMap.get("recipient_name"));
							detailData.put("expiry", Utils.convertDateFromMilis("yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
									utibaDataMap.get("expiry")));
							detailData.put("token", utibaDataMap.get("couponid"));
							detailData.put("amount", utibaDataMap.get("amount"));
						} else {
							detailData.put("Token", utibaDataMap.get("couponid").substring(0, 6));
							detailData.put("amount", utibaDataMap.get("price"));
						}
					} else {
						detailData.put("token", utibaDataMap.get("couponid").substring(0, 6));
						detailData.put("amount", utibaDataMap.get("price"));
					}
					detailData.put("ext_reference", utibaDataMap.get("trans_ext_reference"));
					detailData.put("trx_date", trst.getTransaction().getLastModified().toString());
					break;
				case "billpay":
					detailData.put("trx_name", "Billpayment");
					HashMap<String, String> detailDataBillpay = new HashMap<String, String>();
					for (TransactionData datas : trst.getTransaction().getExtTransactions().get(1).getParams()) {
						detailDataBillpay.put(datas.getKey(), datas.getValue());
					}
					detailData.put("amount", detailDataBillpay.get("TransAmt"));
					detailData.put("fee", "0");
					detailData.put("ext_reference", utibaDataMap.get("trans_ext_reference"));
					detailData.put("payee", "Airtime Postpaid");
					detailData.put("customer_id", detailDataBillpay.get("MSISDN"));
					detailData.put("customer_name", detailDataBillpay.get("CustName"));
					detailData.put("trx_date", trst.getTransaction().getLastModified().toString());
					telcoObj = dataManager.getTelcoIdByPrefix(utibaDataMap.get("recipient"));
					if (!Utils.isNullorEmptyString(telcoObj.getName())) {
						detailData.put("telco", telcoObj.getName());
					}
					break;
				case "merchant_buy":
					detailData.put("trx_name", "Merchant Buy");
					if (utibaDataMap.get("to") != null) {
						switch (utibaDataMap.get("to")) {
						case "adira_asuransi":
							detailData.put("trx_name", "Insurance");
							detailData.put("to", "ASURANSI ADIRA");
							break;
						case "car_asuransi":
							detailData.put("trx_name", "Insurance");
							detailData.put("to", "ASURANSI CAR");
							break;
						case "cigna_bayar":
							detailData.put("trx_name", "Insurance");
							detailData.put("to", "ASURANSI CIGNA");
							break;
						case "mnc_life":
							detailData.put("trx_name", "Insurance");
							detailData.put("to", "MNC LIFE");
							break;
						case "dbs_payment":
							detailData.put("trx_name", "Loan");
							detailData.put("to", "Dana Bantuan Sahabat");
							detailData.put("customer_id", utibaDataMap.get("idpelanggan"));
							break;
						default:
							detailData.put("to", utibaDataMap.get("to"));
							break;
						}
					}
					if (utibaDataMap.get("TO") != null) {
						detailData.put("to", utibaDataMap.get("TO"));
					}
					if (utibaDataMap.get("program") != null) {
						switch (utibaDataMap.get("program")) {
						case "Voucher Payment":
							detailData.put("trx_name", "Voucher Payment");
							detailData.put("voucher_code", utibaDataMap.get("voucher_code"));
							// 2017-04-15 23:59:00.0
							String formattedDate = Utils.convertDateFromOtherFormat("yyyy-MM-dd HH:mm:ss.S",
									"yyyy-MM-dd'T'HH:mm:ss.SSSXXX", utibaDataMap.get("expired_date"));
							detailData.put("expired_date", formattedDate);
							detailData.put("to", utibaDataMap.get("NAME"));
							detailData.put("link", utibaDataMap.get("url"));
							break;
						case "QR Payment":
							detailData.put("payee", utibaDataMap.get("program"));
							detailData.put("customer_msisdn", utibaDataMap.get("NAME"));
							detailData.put("qr_type", utibaDataMap.get("qr_type"));
							if (utibaDataMap.get("dimo_merchant") != null)
								detailData.put("to", utibaDataMap.get("dimo_merchant"));
							break;
						default:
							detailData.put("payee", utibaDataMap.get("program"));
							detailData.put("customer_name", utibaDataMap.get("NAME"));
							break;
						}
					}
					detailData.put("amount", utibaDataMap.get("amount"));
					detailData.put("trx_date", trst.getTransaction().getLastModified().toString());
					break;
				case "map_agent":
					switch (utibaDataMap.get("agid")) {
					case "11258":
					case "181162":
						detailData.put("trx_name", "Loan Request");
						detailData.put("payee", "Dana Bantuan Sahabat");
						detailData.put("trx_date", trst.getTransaction().getLastModified().toString());
						break;
					}
					detailData.put("customer_name", utibaDataMap.get("name"));
					break;
				default:
					detailData.put("error", "Trx category not registered");
					break;
				}
				rs.setMsisdn(trst.getTransaction().getInitiator());
				rs.setDetailTrx(detailData);
				rs.setName(trst.getTransaction().getType());
			}
		} catch (Exception e) {
			// TODO: handle exception
			if (trst != null) {
				rs.setStatus(Constant.RC_TRANSACTION_NOT_FOUND);
				rs.setMsg(dataManager.getErrorMsg(Constant.RC_TRANSACTION_NOT_FOUND));
			} else {
				rs.setStatus(Constant.RC_TIMEOUT);
				rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			}
			e.printStackTrace();
		}
		return rs;
	}

	public Response matrixBillPay(String sessionId, String recipient, String target, ApiUser user) {
		log.info("matrixBillPay Request with sessionid: " + sessionId + " and recipient: " + recipient + " target:"
				+ target);
		Response rs = new Response();
		try {

			com.utiba.delirium.ws.misc.KeyValuePairMap keyValuePairMap = new com.utiba.delirium.ws.misc.KeyValuePairMap();
			KeyValuePair kvp1 = new KeyValuePair();
			kvp1.setKey("recipient");
			kvp1.setValue(recipient);

			// KeyValuePair kvp2 = new KeyValuePair();
			// kvp2.setKey("suppress_sms");
			// kvp2.setValue("1");

			KeyValuePair kvp3 = new KeyValuePair();
			kvp3.setKey("biz_command");
			kvp3.setValue("outletbillpay");

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));

			keyValuePairMap.getKeyValuePairs().add(kvChannel);
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			keyValuePairMap.getKeyValuePairs().add(kvp1);
			// keyValuePairMap.getKeyValuePairs().add(kvp2);
			BillpayRequestType billpayRequestType = new BillpayRequestType();
			billpayRequestType.setSessionid(sessionId);
			billpayRequestType.setWait(false);
			billpayRequestType.setTarget(target);
			billpayRequestType.setAmount(new BigDecimal(1000));
			billpayRequestType.setExtraTransData(keyValuePairMap);
			StandardBizResponse bpr = utibaClient.billpay(billpayRequestType);
			log.info("matrikBillPay RESULT; " + bpr.getResult());
			if (bpr.getResult() == Constant.RC_SUCCESS) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				rs = getTransactionById(sessionId, bpr.getTransid(), Constant.SERVICE_BILLPAY, user);
				rs.setTrxid(String.valueOf(bpr.getTransid()));
				// rs.setStatus(bpr.getResult());
				// rs.setMsg(dataManager.getErrorMsg(bpr.getResult()));
			} else {
				rs.setStatus(bpr.getResult());
				rs.setMsg(dataManager.getErrorMsg(bpr.getResult()));
			}

		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}
		return rs;
	}

	public Response queryBillPay(String sessionId, String initiator, String biller, List<ExtraTransData> extList,
			ApiUser user) {
		log.info("queryBillPay initiator:" + sessionId + " initiator:" + initiator + " biller:" + biller
				+ "ExtraTransData: " + extList.size());
		Response rs = new Response();
		try {
			com.utiba.delirium.ws.misc.KeyValuePairMap keyValuePairMap = new com.utiba.delirium.ws.misc.KeyValuePairMap();
			for (ExtraTransData obj : extList) {
				KeyValuePair kv = new KeyValuePair();
				kv.setKey(obj.getExtKey());
				kv.setValue(obj.getExtValue());
				keyValuePairMap.getKeyValuePairs().add(kv);
			}

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);

			QuerybillpayRequestType querybillpayRequestType = new QuerybillpayRequestType();
			querybillpayRequestType.setSessionid(sessionId);
			querybillpayRequestType.setBiller(biller);
			querybillpayRequestType.setRecipient(initiator);
			querybillpayRequestType.setExtraTransData(keyValuePairMap);
			QuerybillpayResponseType qbr = utibaClient.querybillpay(querybillpayRequestType);
			log.info("QuerybillpayResponseType qbr.getResult() :" + qbr.getResult());
			log.info("QuerybillpayResponseType qbr.getTransid() :" + qbr.getTransid());
			log.info("QuerybillpayResponseType qbr.getAmount() :" + qbr.getAmount());
			if (qbr.getResult() == Constant.RC_SUCCESS) {
				rs = getTransactionById(sessionId, qbr.getTransid(), Constant.SERVICE_QUERY_BILLPAY, user);
				rs.setPrice(String.valueOf(qbr.getAmount()));

				// SET AMOUNT
				// String amount = String.valueOf(qbr.getAmount());
				// if(!Utils.isNullorEmptyString(rs.getFee())){
				// int amountInt = Integer.valueOf(rs.getPrice()) -
				// Integer.valueOf(rs.getFee());
				// amount = String.valueOf(amountInt);
				// rs.setAmount(amount);
				// }
			} else {
				rs.setTrxid(String.valueOf(qbr.getTransid()));
				rs.setStatus(qbr.getResult());
				rs.setMsg(dataManager.getErrorMsg(qbr.getResult()));
			}

		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}

	public Response billPay(String sessionId, String target, String extRef, String amount, List<ExtraTransData> extList,
			String key, ApiUser user) {

		log.info("billpay querybillpay sessionid: " + sessionId + " extRef:" + extRef + " target:" + target + " amount:"
				+ amount);

		Response rs = new Response();

		try {

			com.utiba.delirium.ws.misc.KeyValuePairMap keyValuePairMap = new com.utiba.delirium.ws.misc.KeyValuePairMap();

			for (ExtraTransData obj : extList) {
				// System.out.println("obj.getKey(): "+obj.getExtKey()+ "
				// obj.getValue(): "+obj.getExtValue() );
				if (obj.getExtKey().equalsIgnoreCase("reference")) {
					if (obj.getExtValue().contains("SERVICEID")) {
						HashMap<String, String> param = Utils.populate(obj.getExtValue());
						if (!param.isEmpty() || param != null) {
							if (param.containsKey("SERVICEID")) {
								if (!Utils.isNullorEmptyString(param.get("SERVICEID"))) {
									KeyValuePair kv = new KeyValuePair();
									kv.setKey("service_type");
									kv.setValue(param.get("SERVICEID"));
									keyValuePairMap.getKeyValuePairs().add(kv);
								}
							}
						}
					}
				}

				KeyValuePair kv = new KeyValuePair();
				kv.setKey(obj.getExtKey());
				kv.setValue(obj.getExtValue());
				keyValuePairMap.getKeyValuePairs().add(kv);
			}

			BillpayRequestType billpayRequestType = new BillpayRequestType();
			billpayRequestType.setSessionid(sessionId);
			billpayRequestType.setWait(true);
			billpayRequestType.setTransExtReference(extRef);
			billpayRequestType.setTarget(target);
			billpayRequestType.setAmount(new BigDecimal(amount));

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);

			billpayRequestType.setExtraTransData(keyValuePairMap);
			StandardBizResponse bpr = utibaClient.billpay(billpayRequestType);
			log.info("BILLPAY RESULT " + bpr.getResult());

			if (bpr.getResult() == Constant.RC_SUCCESS)
				if (!Utils.isNullorEmptyString(key)) {
					rs.setVoucherCode(getValueFromTransactionById(sessionId, bpr.getTransid(), key, user));
				}
			rs.setTrxid(String.valueOf(bpr.getTransid()));
			rs.setStatus(bpr.getResult());
			rs.setMsg(dataManager.getErrorMsg(bpr.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}
		return rs;
	}

	public Response billPayWithAdminFee(String sessionId, String target, String extRef, String amount, String fee, List<ExtraTransData> extList,
			String key, ApiUser user) {

		log.info("billpay querybillpay sessionid: " + sessionId + " extRef:" + extRef + " target:" + target + " amount:"
				+ amount + " fee:"+fee);

		Response rs = new Response();

		try {

			com.utiba.delirium.ws.misc.KeyValuePairMap keyValuePairMap = new com.utiba.delirium.ws.misc.KeyValuePairMap();

			for (ExtraTransData obj : extList) {
				// System.out.println("obj.getKey(): "+obj.getExtKey()+ "
				// obj.getValue(): "+obj.getExtValue() );
				if (obj.getExtKey().equalsIgnoreCase("reference")) {
					if (obj.getExtValue().contains("SERVICEID")) {
						HashMap<String, String> param = Utils.populate(obj.getExtValue());
						if (!param.isEmpty() || param != null) {
							if (param.containsKey("SERVICEID")) {
								if (!Utils.isNullorEmptyString(param.get("SERVICEID"))) {
									KeyValuePair kv = new KeyValuePair();
									kv.setKey("service_type");
									kv.setValue(param.get("SERVICEID"));
									keyValuePairMap.getKeyValuePairs().add(kv);
								}
							}
						}
					}
				}
				
				KeyValuePair kv = new KeyValuePair();
				kv.setKey(obj.getExtKey());
				kv.setValue(obj.getExtValue());
				keyValuePairMap.getKeyValuePairs().add(kv);
			}

			BillpayRequestType billpayRequestType = new BillpayRequestType();
			billpayRequestType.setSessionid(sessionId);
			billpayRequestType.setWait(true);
			billpayRequestType.setTransExtReference(extRef);
			billpayRequestType.setTarget(target);
			billpayRequestType.setAmount(new BigDecimal(amount));

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			
			KeyValuePair kvAdminFee = new KeyValuePair();
			kvAdminFee.setKey("admin_fee");
			kvAdminFee.setValue(String.valueOf(fee));
			keyValuePairMap.getKeyValuePairs().add(kvAdminFee);
		
			billpayRequestType.setExtraTransData(keyValuePairMap);
			StandardBizResponse bpr = utibaClient.billpay(billpayRequestType);
			log.info("BILLPAY RESULT " + bpr.getResult());

			if (bpr.getResult() == Constant.RC_SUCCESS)
				if (!Utils.isNullorEmptyString(key)) {
					rs.setVoucherCode(getValueFromTransactionById(sessionId, bpr.getTransid(), key, user));
				}
			rs.setTrxid(String.valueOf(bpr.getTransid()));
			rs.setStatus(bpr.getResult());
			rs.setMsg(dataManager.getErrorMsg(bpr.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}
		return rs;
	}

	
	// Added By Ryanbhuled
	public Response reversalNoAmount(String sessionId, String transId, ApiUser user) {
		Response rs = new Response();
		try {

			KeyValuePairMap value = new KeyValuePairMap();
			KeyValuePairMap kpm = new KeyValuePairMap();

			KeyValuePair kv = new KeyValuePair();
			kv.setKey("biz_command");
			kv.setValue("reversal_request");
			value.getKeyValuePairs().add(kv);

			KeyValuePair supressSMS = new KeyValuePair();
			supressSMS.setKey("suppress_sms");
			supressSMS.setValue("1");

			KeyValuePair supressConfirm = new KeyValuePair();
			supressConfirm.setKey("suppress_confirm");
			supressConfirm.setValue("1");

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));

			kpm.getKeyValuePairs().add(kvChannel);
			kpm.getKeyValuePairs().add(kvApiUser);
			kpm.getKeyValuePairs().add(supressConfirm);
			kpm.getKeyValuePairs().add(supressSMS);

			ReverseRequestType requestType = new ReverseRequestType();
			requestType.setSessionid(sessionId);
			requestType.setTransactionID(Integer.valueOf(transId));
			requestType.setExtraTransData(kpm);
			StandardBizResponse response = utibaClient.reverse(requestType);

			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));

		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}
		return rs;
	}

	public Response sellHold(String sessionId, String to, String amount, String biller, String extRef, String type_trx,
			int extParam, ApiUser user) {
		Response rs = new Response();
		try {
			SellRequestType sellRequestType = new SellRequestType();

			KeyValuePairMap kpm = new KeyValuePairMap();

			KeyValuePair kp = new KeyValuePair();
			kp.setKey("type_trx");
			kp.setValue(type_trx);

			KeyValuePair kp1 = new KeyValuePair();
			kp1.setKey("biller");
			kp1.setValue(biller);

			KeyValuePair kp2 = new KeyValuePair();
			kp2.setKey("price");
			kp2.setValue(amount);

			KeyValuePair supressConfirm = new KeyValuePair();
			supressConfirm.setKey("suppress_confirm");
			supressConfirm.setValue("1");

			KeyValuePair supressSMS = new KeyValuePair();
			supressSMS.setKey("suppress_sms");
			supressSMS.setValue("1");

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kpm.getKeyValuePairs().add(kvApiUser);

			if (extParam == 1) {
				kpm.getKeyValuePairs().add(supressSMS);
			}

			kpm.getKeyValuePairs().add(supressConfirm);
			kpm.getKeyValuePairs().add(kp);
			kpm.getKeyValuePairs().add(kp1);
			kpm.getKeyValuePairs().add(kp2);
			sellRequestType.setExtraTransData(kpm);

			sellRequestType.setSessionid(sessionId);
			sellRequestType.setTransExtReference(extRef);
			sellRequestType.setTo(to);
			sellRequestType.setAmount(new BigDecimal(amount));
			StandardBizResponse response = utibaClient.sell(sellRequestType);
			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
			rs.setResultNameSpace(response.getResultNamespace());
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
			log.error("Sell request error to[" + to + "] amount[" + amount + "] extRef[" + extRef + "]");
		}
		return rs;
	}

	public Response mapAgent(String sessionId, String agent, long agentId, String agentInitiator, ApiUser user) {
		Response response = new Response();
		try {
			MapAgentRequest mapAgentRequest = new MapAgentRequest();
			mapAgentRequest.setMapAgentRequestType(new MapAgentRequestType());
			mapAgentRequest.getMapAgentRequestType().setSessionid(sessionId);
			mapAgentRequest.getMapAgentRequestType().setAgid(agentId);
			mapAgentRequest.getMapAgentRequestType().setAgent(agent);

			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair kycReq = new KeyValuePair();
			kycReq.setKey("kyc_request");
			kycReq.setValue("1");
			kvpm.getKeyValuePairs().add(kycReq);

			KeyValuePair kvagent = new KeyValuePair();
			kvagent.setKey("init_agent");
			kvagent.setValue(agentInitiator);
			kvpm.getKeyValuePairs().add(kvagent);

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);

			mapAgentRequest.getMapAgentRequestType().setExtraTransData(kvpm);

			MapAgentResponse mapAgentResponse = utibaClient.mapAgent(mapAgentRequest);
			response.setTrxid(String.valueOf(mapAgentResponse.getMapAgentReturn().getTransid()));
			response.setStatus(mapAgentResponse.getMapAgentReturn().getResult());
			response.setMsg(dataManager.getErrorMsg(mapAgentResponse.getMapAgentReturn().getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("mapAgent Found Error: agent[" + agent + "] agentId[" + agentId + "]");
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public Response mapAgentWithParam(String sessionId, String agent, long agentId, HashMap<String, String> param,
			ApiUser user) {
		Response response = new Response();
		try {
			com.utiba.delirium.ws.misc.KeyValuePairMap keyValuePairMap = new com.utiba.delirium.ws.misc.KeyValuePairMap();

			if (!Utils.isNullorEmptyString(param.get(Constant.NAME_PARAM))) {
				KeyValuePair kv = new KeyValuePair();
				kv.setKey("name");
				kv.setValue(param.get(Constant.NAME_PARAM));
				keyValuePairMap.getKeyValuePairs().add(kv);
			}

			if (!Utils.isNullorEmptyString(param.get(Constant.DOB_PARAM))) {
				KeyValuePair kv2 = new KeyValuePair();
				kv2.setKey("dob");
				kv2.setValue(param.get(Constant.DOB_PARAM));
				keyValuePairMap.getKeyValuePairs().add(kv2);
			}

			if (!Utils.isNullorEmptyString(param.get(Constant.ID_NUMBER_PARAM))) {
				KeyValuePair kv3 = new KeyValuePair();
				kv3.setKey("idnum");
				kv3.setValue(param.get(Constant.ID_NUMBER_PARAM));
				keyValuePairMap.getKeyValuePairs().add(kv3);
			}

			if (!Utils.isNullorEmptyString(param.get(Constant.GENDER_PARAM))) {
				KeyValuePair kv4 = new KeyValuePair();
				kv4.setKey("gender");
				kv4.setValue(param.get(Constant.GENDER_PARAM));
				keyValuePairMap.getKeyValuePairs().add(kv4);
			}

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);

			log.debug("Param:" + param);
			TreeMap<String, String> paramTree = new TreeMap<String, String>();
			paramTree.putAll(param);
			if (!paramTree.isEmpty() || paramTree.size() > 0) {
				try {
					paramTree.remove(Constant.SIGNATURE_PARAM);
					paramTree.remove(Constant.GROUPID_PARAM);
					paramTree.remove(Constant.USERID_PARAM);
					paramTree.remove(Constant.MSISDN_PARAM);
				} catch (Exception ex) {

				}
				Iterator it = paramTree.entrySet().iterator();
				StringBuilder sb = new StringBuilder();
				String paramval = "";
				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry) it.next();
					String pairVal = pair.getValue().toString();
					// sb.append(pair.getKey());
					// sb.append("|");
					if (pairVal.contains(",")) {
						pairVal = pairVal.replace(",", " ");
					}
					if (pair.getKey().equals("address")) {
						if (pairVal.length() > 40) {
							String y = pairVal.substring(0, 40);
							pairVal = y.substring(0, y.lastIndexOf(" "));
						}
					}
					// sb.append(pair.getValue());
					sb.append(pairVal);
					sb.append(",");
				}
				paramval = StringUtils.removeEnd(sb.toString(), ",");
				KeyValuePair kvParam = new KeyValuePair();
				kvParam.setKey("PROGRAM");
				kvParam.setValue(paramval);
				keyValuePairMap.getKeyValuePairs().add(kvParam);
			}

			MapAgentRequest agentRequest = new MapAgentRequest();
			MapAgentRequestType requestType = new MapAgentRequestType();
			requestType.setSessionid(sessionId);
			requestType.setAgid(agentId);
			requestType.setAgent(agent);
			requestType.setExtraTransData(keyValuePairMap);
			agentRequest.setMapAgentRequestType(requestType);
			MapAgentResponse mapAgentResponse = utibaClient.mapAgent(agentRequest);
			response.setTrxid(String.valueOf(mapAgentResponse.getMapAgentReturn().getTransid()));
			response.setStatus(mapAgentResponse.getMapAgentReturn().getResult());
			response.setMsg(dataManager.getErrorMsg(mapAgentResponse.getMapAgentReturn().getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("mapAgent Found Error: agent[" + agent + "] agentId[" + agentId + "]");
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
		// return response.getMapAgentReturn().getResult();
	}

	public Response adjustWallet(String sessionId, BigDecimal amount, String source, String target, String desc,
			int type, ApiUser user) {
		Response response = new Response();
		try {
			KeyValuePairMap kvpm = new KeyValuePairMap();

			KeyValuePair description = new KeyValuePair();
			description.setKey("description");
			description.setValue(desc);

			if (type == 1) {
				KeyValuePair supressSMS = new KeyValuePair();
				supressSMS.setKey("suppress_sms");
				supressSMS.setValue("1");
				kvpm.getKeyValuePairs().add(supressSMS);
			}

			kvpm.getKeyValuePairs().add(description);

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);

			AdjustWalletRequest adjustWalletRequest = new AdjustWalletRequest();
			adjustWalletRequest.setAdjustWalletRequestType(new AdjustWalletRequestType());

			adjustWalletRequest.getAdjustWalletRequestType().setSessionid(sessionId);
			adjustWalletRequest.getAdjustWalletRequestType().setAmount(amount);
			adjustWalletRequest.getAdjustWalletRequestType().setSource(source);
			adjustWalletRequest.getAdjustWalletRequestType().setTarget(target);
			adjustWalletRequest.getAdjustWalletRequestType().setType(1);
			adjustWalletRequest.getAdjustWalletRequestType().setExtraTransData(kvpm);
			AdjustWalletResponse adjustWalletResponse = utibaClient.adjustWallet(adjustWalletRequest);
			response.setTrxid(String.valueOf(adjustWalletResponse.getAdjustWalletReturn().getTransid()));
			response.setStatus(adjustWalletResponse.getAdjustWalletReturn().getResult());
			response.setMsg(dataManager.getErrorMsg(adjustWalletResponse.getAdjustWalletReturn().getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("adjustWallet Found Error: source[" + source + "] target[" + target + "]");
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public Response joinParent(String sessionId, String parent, String child, ApiUser user) {
		Response response = new Response();
		try {

			JoinparentRequestType jp = new JoinparentRequestType();
			jp.setSessionid(sessionId);
			jp.setTarget(parent);
			jp.setSource(child);

			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair supressSMS = new KeyValuePair();
			supressSMS.setKey("suppress_sms");
			supressSMS.setValue("1");
			kvpm.getKeyValuePairs().add(supressSMS);

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);

			jp.setExtraTransData(kvpm);

			StandardBizResponse resp = utibaClient.joinparent(jp);

			response.setTrxid(String.valueOf(resp.getTransid()));
			response.setStatus(resp.getResult());
			response.setMsg(dataManager.getErrorMsg(resp.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("joinParent Found Error: parent[" + parent + "] child[" + child + "]");
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}
	
	public Response joinChild(String sessionId, String parent, String child, ApiUser user) {
		Response response = new Response();
		try {

			JoinchildRequestType jc = new JoinchildRequestType();
			jc.setSessionid(sessionId);
			jc.setTarget(child);
			jc.setSource(parent);

			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair supressSMS = new KeyValuePair();
			supressSMS.setKey("suppress_sms");
			supressSMS.setValue("1");
			kvpm.getKeyValuePairs().add(supressSMS);

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);

			jc.setExtraTransData(kvpm);

			StandardBizResponse resp = utibaClient.joinchild(jc);

			response.setTrxid(String.valueOf(resp.getTransid()));
			response.setStatus(resp.getResult());
			response.setMsg(dataManager.getErrorMsg(resp.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("joinParent Found Error: parent[" + parent + "] child[" + child + "]");
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public Response removeAgent(String sessionId, String target, ApiUser user) {
		Response response = new Response();
		try {

			DeleteRequestType delReq = new DeleteRequestType();
			delReq.setSessionid(sessionId);
			delReq.setAgent(target);

			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			delReq.setExtraTransData(keyValuePairMap);

			StandardBizResponse resp = utibaClient.delete(delReq);
			response.setTrxid(String.valueOf(resp.getTransid()));
			response.setStatus(resp.getResult());
			response.setMsg(dataManager.getErrorMsg(resp.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("removeAgent Found Error: target[" + target + "]");
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public umarketscws.KeyValuePairMap utibaUserMapping(AgentData ag) {
		umarketscws.KeyValuePairMap value = new umarketscws.KeyValuePairMap();
		if (!Utils.isNullorEmptyString(ag.getFirstName())) {
			String namalkp = ag.getFirstName();
			if (!Utils.isNullorEmptyString(ag.getLastName())) {
				umarketscws.KeyValuePair kvFirstName = new umarketscws.KeyValuePair();
				kvFirstName.setKey(Constant.FIRSTNAME_PARAM);
				kvFirstName.setValue(ag.getFirstName());
				value.getKeyValuePair().add(kvFirstName);
				umarketscws.KeyValuePair kvLastName = new umarketscws.KeyValuePair();
				kvLastName.setKey(Constant.LASTNAME_PARAM);
				kvLastName.setValue(ag.getLastName());
				value.getKeyValuePair().add(kvLastName);
				namalkp = ag.getFirstName() + " " + ag.getLastName();
			} else {
				umarketscws.KeyValuePair kvFirstName = new umarketscws.KeyValuePair();
				kvFirstName.setKey(Constant.FIRSTNAME_PARAM);
				kvFirstName.setValue(ag.getFirstName());
				value.getKeyValuePair().add(kvFirstName);
			}
			umarketscws.KeyValuePair kvName = new umarketscws.KeyValuePair();
			kvName.setKey(Constant.NAME_PARAM);
			kvName.setValue(namalkp);
			value.getKeyValuePair().add(kvName);
		}

		umarketscws.KeyValuePair kvIdType = new umarketscws.KeyValuePair();
		kvIdType.setKey("idtype");
		kvIdType.setValue("KTP");
		value.getKeyValuePair().add(kvIdType);

		if (!Utils.isNullorEmptyString(ag.getIdNo())) {
			umarketscws.KeyValuePair kvIdNumber = new umarketscws.KeyValuePair();
			kvIdNumber.setKey("id_num");
			kvIdNumber.setValue(ag.getIdNo());
			value.getKeyValuePair().add(kvIdNumber);
		}

		String addr = "";
		if (!Utils.isNullorEmptyString(ag.getAddress())) {
			addr = ag.getAddress();
			if (!Utils.isNullorEmptyString(ag.getNo_rt()))
				addr = addr + " RT." + ag.getNo_rt();
			if (!Utils.isNullorEmptyString(ag.getNo_rw()))
				addr = addr + "/" + ag.getNo_rw();
			if (!Utils.isNullorEmptyString(ag.getKelurahan()))
				addr = addr + ", " + ag.getKelurahan();
			if (!Utils.isNullorEmptyString(ag.getKecamatan()))
				addr = addr + ", " + ag.getKecamatan();
			if (!Utils.isNullorEmptyString(ag.getCity()))
				addr = addr + ", " + ag.getCity();
			if (!Utils.isNullorEmptyString(ag.getProvince()))
				addr = addr + ", " + ag.getProvince();
		}
		umarketscws.KeyValuePair kvAddress = new umarketscws.KeyValuePair();
		kvAddress.setKey(Constant.ADDRESS_PARAM);
		kvAddress.setValue(addr);
		value.getKeyValuePair().add(kvAddress);

		if (!Utils.isNullorEmptyString(ag.getNo_kk())) {
			umarketscws.KeyValuePair kvnoKK = new umarketscws.KeyValuePair();
			kvnoKK.setKey("id_kk");
			kvnoKK.setValue(ag.getNo_kk());
			value.getKeyValuePair().add(kvnoKK);
		}

		if (!Utils.isNullorEmptyString(ag.getDateOfBirth())) {
			umarketscws.KeyValuePair kvDOB = new umarketscws.KeyValuePair();
			kvDOB.setKey("birthdate");
			kvDOB.setValue(ag.getDateOfBirth());
			value.getKeyValuePair().add(kvDOB);
		}

		if (!Utils.isNullorEmptyString(ag.getMotherMaidenName())) {
			umarketscws.KeyValuePair kvMotherMaidenName = new umarketscws.KeyValuePair();
			kvMotherMaidenName.setKey("mother");
			kvMotherMaidenName.setValue(ag.getMotherMaidenName());
			value.getKeyValuePair().add(kvMotherMaidenName);

			umarketscws.KeyValuePair kvMotherMaidenNameDompetku = new umarketscws.KeyValuePair();
			kvMotherMaidenNameDompetku.setKey("alt_id");
			kvMotherMaidenNameDompetku.setValue(ag.getMotherMaidenName());
			value.getKeyValuePair().add(kvMotherMaidenNameDompetku);
		}

		if (!Utils.isNullorEmptyString(ag.getGender())) {
			umarketscws.KeyValuePair kvGender = new umarketscws.KeyValuePair();
			kvGender.setKey(Constant.GENDER_PARAM);
			kvGender.setValue(ag.getGender());
			value.getKeyValuePair().add(kvGender);
		}

		if (!Utils.isNullorEmptyString(ag.getIdPhoto())) {
			umarketscws.KeyValuePair kvPhotoID = new umarketscws.KeyValuePair();
			kvPhotoID.setKey("photo_id");
			kvPhotoID.setValue(ag.getIdPhoto());
			value.getKeyValuePair().add(kvPhotoID);
		}

		if (!Utils.isNullorEmptyString(ag.getProfilePic())) {
			umarketscws.KeyValuePair kvPhotoIDUser = new umarketscws.KeyValuePair();
			kvPhotoIDUser.setKey("photo_id_user");
			kvPhotoIDUser.setValue(ag.getProfilePic());
			value.getKeyValuePair().add(kvPhotoIDUser);
		}
		return value;
	}

	public Response updateAgentToOperator(String sessionId, String target, String name, String smsAddress,
			String category, String isOutlet, ApiUser user) {
		Response response = new Response();
		try {
			ModifyRequestType requestType = new ModifyRequestType();
			requestType.setSessionid(sessionId);
			requestType.setAgent(target);
			requestType.setName(name);
			requestType.setSmsAddress(smsAddress);

			umarketscws.KeyValuePairMap kvpm = new umarketscws.KeyValuePairMap();

			umarketscws.KeyValuePair supressSMS = new umarketscws.KeyValuePair();
			supressSMS.setKey("suppress_sms");
			supressSMS.setValue("1");
			kvpm.getKeyValuePair().add(supressSMS);

			umarketscws.KeyValuePair agent_category = new umarketscws.KeyValuePair();
			agent_category.setKey("agent_category");
			agent_category.setValue(category);
			kvpm.getKeyValuePair().add(agent_category);

			if (!Utils.isNullorEmptyString(isOutlet)) {
				if (isOutlet.equalsIgnoreCase("1")) {
					umarketscws.KeyValuePair kv3 = new umarketscws.KeyValuePair();
					kv3.setKey("primary_group");
					kv3.setValue("outlet");
					kvpm.getKeyValuePair().add(kv3);
				}
			}

			requestType.setExtraParams(kvpm);

			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			requestType.setExtraTransData(keyValuePairMap);

			log.debug(requestType);
			StandardBizResponse sbResp = utibaClient.modify(requestType);
			response.setStatus(sbResp.getResult());
			response.setMsg(dataManager.getErrorMsg(sbResp.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	public Response checkPoint(String sessionId, ApiUser user) {
		Response rs = new Response();
		try {
			BalanceRequestType requestType = new BalanceRequestType();
			requestType.setSessionid(sessionId);
			requestType.setType(3);

			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			requestType.setExtraTransData(keyValuePairMap);

			BalanceResponseType response = utibaClient.balance(requestType);
			rs.setBalance(String.valueOf(response.getAvail3()));
			rs.setStatus(response.getResult());
			rs.setTrxid(String.valueOf(response.getTransid()));
			rs.setMsg(dataManager.getErrorMsg(response.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}

		return rs;
	}

	public Response getUserGroupingName(String msisdn, ApiUser user) {
		log.info("getAgentByReferenceRequest msisdn:" + msisdn);
		Response rs = new Response();
		try {
			String sessionId = dataManager.getGlobalSessionId();
			GetAgentByReferenceRequest getAgentByReferenceRequest = new GetAgentByReferenceRequest();
			getAgentByReferenceRequest.setSessionid(sessionId);
			getAgentByReferenceRequest.setReference(msisdn);

			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			getAgentByReferenceRequest.setExtraTransData(keyValuePairMap);

			AgentResponseType response = utibaClient.getAgentByReference(getAgentByReferenceRequest);
			if (response.getResult() == Constant.RC_SUCCESS) {
				GetAgentGroupByAgentIDRequest agentGroupByAgentIDRequest = new GetAgentGroupByAgentIDRequest();
				agentGroupByAgentIDRequest.setSessionid(sessionId);
				agentGroupByAgentIDRequest.setAgentID(response.getAgent().getAgentID());
				agentGroupByAgentIDRequest.setExtraTransData(keyValuePairMap);
				AgentGroupsResponseType agentGroupResp = utibaClient.getAgentGroupByAgentID(agentGroupByAgentIDRequest);
				log.debug("response.getAgent().getAgentID(): " + response.getAgent().getAgentID());
				log.debug("agentGroupResp: " + agentGroupResp.getResult());
				if (response.getAgent().getStatus() == Constant.UTIBA_ACCOUNT_STATUS_REGISTERED) {

					rs.setName(response.getAgent().getName() == null ? response.getAgent().getReference()
							: response.getAgent().getName());

					if (!Utils.isNullorEmptyString(response.getAgent().getSMSAddress())) {
						rs.getAgentData().setSmsAddress(response.getAgent().getSMSAddress());
					}
					ArrayList<String> walletArray = new ArrayList<String>();
					for (AgentGroup obj : agentGroupResp.getAgentGroups()) {
						walletArray.add(obj.getID() + ":" + obj.getName());
						log.debug("obj.getID(): " + obj.getID() + "  obj.getName(): " + obj.getName());
					}

					rs.getAgentData().setWalletGrouping(walletArray.toString());
					rs.setStatus(response.getResult());
					rs.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
				} else if (response.getAgent().getAgentType() == 0 && response.getAgent().getStatus() == 0) {
					rs.setStatus(Constant.RC_AGENT_NOTFOUND);
					rs.setMsg(dataManager.getErrorMsg(Constant.RC_AGENT_NOTFOUND));
				} else {
					rs.setStatus(Constant.RC_AGENT_SUSPENDED);
					rs.setMsg(dataManager.getErrorMsg(Constant.RC_AGENT_SUSPENDED));
				}

			} else if (response.getResult() == Constant.RC_SESSION_EXPIRED
					&& response.getResultNamespace().equalsIgnoreCase("session")) {
				log.info("Session Expired, try to get the new one......");
				dataManager.setGlobalSessionId();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				rs = this.getAgentByReferenceRequest(msisdn, user);
			} else {
				rs.setStatus(Constant.RC_AGENT_NOTFOUND);
				rs.setMsg(dataManager.getErrorMsg(Constant.RC_AGENT_NOTFOUND));
			}
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return rs;
	}
	
	//Check Child
	public Response getChildList(String msisdn, ApiUser user) {
		log.info("getChildListRequest msisdn:" + msisdn);
		Response rs = new Response();
		try {
			String sessionId = dataManager.getGlobalSessionId();
			GetAgentByReferenceRequest getAgentByReferenceRequest = new GetAgentByReferenceRequest();
			getAgentByReferenceRequest.setSessionid(sessionId);
			getAgentByReferenceRequest.setReference(msisdn);

			KeyValuePairMap keyValuePairMap = new KeyValuePairMap();
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			keyValuePairMap.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			getAgentByReferenceRequest.setExtraTransData(keyValuePairMap);

			AgentResponseType response = utibaClient.getAgentByReference(getAgentByReferenceRequest);
			if (response.getResult() == Constant.RC_SUCCESS) {
				GetChildListByReferenceIDRequestType getChildParam = new GetChildListByReferenceIDRequestType();
				getChildParam.setSessionid(sessionId);
				getChildParam.setExtraTransData(keyValuePairMap);
				getChildParam.setAgentReferenceID(response.getAgent().getReferenceID());
				GetChildListByReferenceIDRequest getChildList = new GetChildListByReferenceIDRequest();
				getChildList.setGetChildListByReferenceIDRequestType(getChildParam);
				GetChildListByReferenceIDResponse getChild = utibaClient.getChildListByReferenceID(getChildList);
				log.debug("getChildResponse: " + getChild.toString());
				if(getChild.getGetChildListByReferenceIDResponseType().getResult()==Constant.RC_SUCCESS){
					if(getChild.getGetChildListByReferenceIDResponseType().getAgentList().size()<=0){
						rs.setStatus(Constant.RC_AGENT_NOTFOUND);
						rs.setMsg(dataManager.getErrorMsg(Constant.RC_AGENT_NOTFOUND));
						return rs;
					}
					List<Agent> agentList = getChild.getGetChildListByReferenceIDResponseType().getAgentList();
					List<LastTransaction> ltList = new ArrayList<>();
					for (Agent agent : agentList) {
						LastTransaction lt = new LastTransaction();
						lt.setAgent(agent.getName());
						lt.setType(msisdn);
						ltList.add(lt);
					}
					rs.setStatus(Constant.RC_SUCCESS);
					rs.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
					rs.setTrxList(ltList);
				}else{
					rs.setStatus(Constant.RC_SYSTEM_ERROR);
					rs.setMsg(dataManager.getErrorMsg(Constant.RC_SYSTEM_ERROR));
				}
			} else if (response.getResult() == Constant.RC_SESSION_EXPIRED
					&& response.getResultNamespace().equalsIgnoreCase("session")) {
				log.info("Session Expired, try to get the new one......");
				dataManager.setGlobalSessionId();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				rs = this.getAgentByReferenceRequest(msisdn, user);
			} else {
				rs.setStatus(Constant.RC_AGENT_NOTFOUND);
				rs.setMsg(dataManager.getErrorMsg(Constant.RC_AGENT_NOTFOUND));
			}
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return rs;
	}

	// PART Merchant Registration
	// use session id of registrator
	public Response registerMerchant(String sessionId, String msisdn, String name, String pin, String address,
			String bank_acc, String bank_acc_name, String bank_code, String bank_name, ApiUser user) {
		Response response = new Response();
		RegisterRequestType requestType = new RegisterRequestType();
		requestType.setSessionid(sessionId);
		requestType.setAgent(msisdn);
		requestType.setLanguage("id");
		requestType.setSmsAddress(msisdn);
		requestType.setName(name);
		requestType.setNewPin(pin);
		requestType.setAgentCategory("merchant");

		umarketscws.KeyValuePairMap keyValuePairMap = new umarketscws.KeyValuePairMap();
		umarketscws.KeyValuePair kv1 = new umarketscws.KeyValuePair();
		kv1.setKey("primary_group");
		kv1.setValue("merchant");
		keyValuePairMap.getKeyValuePair().add(kv1);

		umarketscws.KeyValuePair kv2 = new umarketscws.KeyValuePair();
		kv2.setKey("address");
		kv2.setValue(address);
		keyValuePairMap.getKeyValuePair().add(kv2);

		umarketscws.KeyValuePair kv3 = new umarketscws.KeyValuePair();
		kv3.setKey("bank_acc");
		kv3.setValue(bank_acc);
		keyValuePairMap.getKeyValuePair().add(kv3);

		umarketscws.KeyValuePair kv4 = new umarketscws.KeyValuePair();
		kv4.setKey("bank_acc_name");
		kv4.setValue(bank_acc_name);
		keyValuePairMap.getKeyValuePair().add(kv4);

		umarketscws.KeyValuePair kv5 = new umarketscws.KeyValuePair();
		kv5.setKey("bank_code");
		kv5.setValue(bank_code);
		keyValuePairMap.getKeyValuePair().add(kv5);

		umarketscws.KeyValuePair kv6 = new umarketscws.KeyValuePair();
		kv6.setKey("bank_name");
		kv6.setValue(bank_name);
		keyValuePairMap.getKeyValuePair().add(kv6);

		umarketscws.KeyValuePair kv7 = new umarketscws.KeyValuePair();
		kv7.setKey("lic_cert_number");
		kv7.setValue("0000000015");
		keyValuePairMap.getKeyValuePair().add(kv7);

		umarketscws.KeyValuePair kv8 = new umarketscws.KeyValuePair();
		kv8.setKey("merchant_id");
		kv8.setValue(msisdn);
		keyValuePairMap.getKeyValuePair().add(kv8);
		requestType.setExtraParams(keyValuePairMap);

		// add for ExtraTransData
		KeyValuePairMap kvpm = new KeyValuePairMap();
		KeyValuePair kvChannel = new KeyValuePair();
		kvChannel.setKey("channel_type");
		kvChannel.setValue(String.valueOf(user.getChannelType()));
		kvpm.getKeyValuePairs().add(kvChannel);

		KeyValuePair kvApiUser = new KeyValuePair();
		kvApiUser.setKey("api_user");
		kvApiUser.setValue(String.valueOf(user.getUserId()));
		kvpm.getKeyValuePairs().add(kvApiUser);

		// KeyValuePair kvSupressSMS = new KeyValuePair();
		// kvSupressSMS.setKey("suppress_sms");
		// kvSupressSMS.setValue(String.valueOf("1"));
		// kvpm.getKeyValuePairs().add(kvSupressSMS);
		requestType.setExtraTransData(kvpm);

		StandardBizResponse regRes = utibaClient.register(requestType);
		response.setTrxid(String.valueOf(regRes.getTransid()));
		response.setStatus(regRes.getResult());
		response.setMsg(dataManager.getErrorMsg(regRes.getResult()));

		return response;
	}
	
	//for bluebird lock and unlock account lower limit
	//for modify account data
	public Response modifyLowerLimit(String sessionId, String msisdn, BigDecimal lowerLimit, ApiUser user) {
		
		Response response = new Response();
		ModifyAccountRequestType requestType = new ModifyAccountRequestType();
		requestType.setSessionid(sessionId);
		requestType.setAgent(msisdn);
		requestType.setLowerLimit(lowerLimit);
		
		ModifyAccountRequest request = new ModifyAccountRequest();
		request.setModifyAccountRequestType(requestType);

		StandardBizResponse regRes = utibaClient.modifyAccount(request).getModifyAccountReturn();
		response.setTrxid(String.valueOf(regRes.getTransid()));
		response.setStatus(regRes.getResult());
		response.setMsg(dataManager.getErrorMsg(regRes.getResult()));

		return response;
	}

	public Response modifyMerchant(String sessionId, String msisdn, String name, String bank_acc, String bank_acc_name,
			String bank_code, String bank_name, ApiUser user) {
		Response response = new Response();
		ModifyRequestType requestType = new ModifyRequestType();
		requestType.setSessionid(sessionId);
		requestType.setAgent(msisdn);
		requestType.setName(name);

		umarketscws.KeyValuePairMap keyValuePairMap = new umarketscws.KeyValuePairMap();
		umarketscws.KeyValuePair kv1 = new umarketscws.KeyValuePair();
		kv1.setKey("primary_group");
		kv1.setValue("merchant");
		keyValuePairMap.getKeyValuePair().add(kv1);

		umarketscws.KeyValuePair kv3 = new umarketscws.KeyValuePair();
		kv3.setKey("bank_acc");
		kv3.setValue(bank_acc);
		keyValuePairMap.getKeyValuePair().add(kv3);

		umarketscws.KeyValuePair kv4 = new umarketscws.KeyValuePair();
		kv4.setKey("bank_acc_name");
		kv4.setValue(bank_acc_name);
		keyValuePairMap.getKeyValuePair().add(kv4);

		umarketscws.KeyValuePair kv5 = new umarketscws.KeyValuePair();
		kv5.setKey("bank_code");
		kv5.setValue(bank_code);
		keyValuePairMap.getKeyValuePair().add(kv5);

		umarketscws.KeyValuePair kv6 = new umarketscws.KeyValuePair();
		kv6.setKey("bank_name");
		kv6.setValue(bank_name);
		keyValuePairMap.getKeyValuePair().add(kv6);
		requestType.setExtraParams(keyValuePairMap);

		// add for ExtraTransData
		KeyValuePairMap kvpm = new KeyValuePairMap();
		KeyValuePair kvChannel = new KeyValuePair();
		kvChannel.setKey("channel_type");
		kvChannel.setValue(String.valueOf(user.getChannelType()));
		kvpm.getKeyValuePairs().add(kvChannel);

		KeyValuePair kvApiUser = new KeyValuePair();
		kvApiUser.setKey("api_user");
		kvApiUser.setValue(String.valueOf(user.getUserId()));
		kvpm.getKeyValuePairs().add(kvApiUser);

		// KeyValuePair kvSupressSMS = new KeyValuePair();
		// kvSupressSMS.setKey("suppress_sms");
		// kvSupressSMS.setValue(String.valueOf("1"));
		// kvpm.getKeyValuePairs().add(kvSupressSMS);
		requestType.setExtraTransData(kvpm);

		StandardBizResponse regRes = utibaClient.modify(requestType);
		response.setTrxid(String.valueOf(regRes.getTransid()));
		response.setStatus(regRes.getResult());
		response.setMsg(dataManager.getErrorMsg(regRes.getResult()));

		return response;
	}

	public Response capset(String sessionId, String msisdn, ApiUser user) {
		Response response = new Response();
		ModifyAccountRequest reqModAcc = new ModifyAccountRequest();
		ModifyAccountRequestType requestType = new ModifyAccountRequestType();
		requestType.setSessionid(sessionId);
		requestType.setAgent(msisdn);
		requestType.setType(1);
		requestType.setCapSet(401l);

		// add for ExtraTransData
		KeyValuePairMap kvpm = new KeyValuePairMap();
		KeyValuePair kvChannel = new KeyValuePair();
		kvChannel.setKey("channel_type");
		kvChannel.setValue(String.valueOf(user.getChannelType()));
		kvpm.getKeyValuePairs().add(kvChannel);

		KeyValuePair kvApiUser = new KeyValuePair();
		kvApiUser.setKey("api_user");
		kvApiUser.setValue(String.valueOf(user.getUserId()));
		kvpm.getKeyValuePairs().add(kvApiUser);

		KeyValuePair upLImit = new KeyValuePair();
		upLImit.setKey("upper_limit");
		upLImit.setValue("");
		kvpm.getKeyValuePairs().add(upLImit);

		KeyValuePair lowLImit = new KeyValuePair();
		lowLImit.setKey("lower_limit");
		lowLImit.setValue("");
		kvpm.getKeyValuePairs().add(lowLImit);

		// KeyValuePair kvSupressSMS = new KeyValuePair();
		// kvSupressSMS.setKey("suppress_sms");
		// kvSupressSMS.setValue(String.valueOf("1"));
		// kvpm.getKeyValuePairs().add(kvSupressSMS);
		requestType.setExtraTransData(kvpm);

		reqModAcc.setModifyAccountRequestType(requestType);
		ModifyAccountResponse regRes = utibaClient.modifyAccount(reqModAcc);
		response.setTrxid(String.valueOf(regRes.getModifyAccountReturn().getTransid()));
		response.setStatus(regRes.getModifyAccountReturn().getResult());
		response.setMsg(dataManager.getErrorMsg(regRes.getModifyAccountReturn().getResult()));

		return response;
	}

	public Response updateMerchantTrusted(String sessionId, String msisdn, ApiUser user) {
		Response response = new Response();
		try {
			ModifyRequestType requestType = new ModifyRequestType();
			requestType.setSessionid(sessionId);
			requestType.setAgent(msisdn);
			ModifyRequestType.Type type = new ModifyRequestType.Type();
			type.setTrusted(true);
			type.setMerchant(true);
			requestType.setType(type);

			KeyValuePairMap kvpm = new KeyValuePairMap();
			KeyValuePair supressSMS = new KeyValuePair();
			supressSMS.setKey("suppress_sms");
			supressSMS.setValue("1");
			kvpm.getKeyValuePairs().add(supressSMS);

			// add for channelType
			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));
			kvpm.getKeyValuePairs().add(kvChannel);

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));
			kvpm.getKeyValuePairs().add(kvApiUser);
			requestType.setExtraTransData(kvpm);
			log.debug(requestType);

			StandardBizResponse sbResp = utibaClient.modify(requestType);
			response.setStatus(sbResp.getResult());
			response.setMsg(dataManager.getErrorMsg(sbResp.getResult()));
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
			response.setStatus(Constant.RC_TIMEOUT);
			response.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
		}
		return response;
	}

	// creditAirtimeInquiry(String userId, String signature, String to, String
	// amount, String transId)
	// public Response creditAirtimeInquiry(String initiator, String to, String
	// amount, String transId, ApiUser user)
//	public Response isatCreditInquiry(String sessionId, String to, String amount, String target, ApiUser user) {
//		log.info("isatCreditInquiry Request with sessionid: " + sessionId + " and recipient: " + to + " target:"
//				+ target);
//		Response rs = new Response();
//		try {
//			com.utiba.delirium.ws.misc.KeyValuePairMap keyValuePairMap = new com.utiba.delirium.ws.misc.KeyValuePairMap();
//			KeyValuePair kvp1 = new KeyValuePair();
//			kvp1.setKey("recipient");
//			kvp1.setValue(to);
//
//			// KeyValuePair kvp2 = new KeyValuePair();
//			// kvp2.setKey("suppress_sms");
//			// kvp2.setValue("1");
//
//			// KeyValuePair kvp3 = new KeyValuePair();
//			// kvp3.setKey("biz_command");
//			// kvp3.setValue("outletbillpay");
//
//			KeyValuePair kvp4 = new KeyValuePair();
//			kvp4.setKey("ugaml_content_type");
//			kvp4.setValue("topup");
//
//			KeyValuePair kvp5 = new KeyValuePair();
//			kvp5.setKey("voucher_type");
//			kvp5.setValue("Regular");
//
//			KeyValuePair kvp6 = new KeyValuePair();
//			kvp6.setKey("denomination");
//			kvp6.setValue(amount);
//
//			KeyValuePair kvChannel = new KeyValuePair();
//			kvChannel.setKey("channel_type");
//			kvChannel.setValue(String.valueOf(user.getChannelType()));
//
//			KeyValuePair kvApiUser = new KeyValuePair();
//			kvApiUser.setKey("api_user");
//			kvApiUser.setValue(String.valueOf(user.getUserId()));
//
//			keyValuePairMap.getKeyValuePairs().add(kvChannel);
//			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
//			keyValuePairMap.getKeyValuePairs().add(kvp1);
//			// keyValuePairMap.getKeyValuePairs().add(kvp2);
//			// keyValuePairMap.getKeyValuePairs().add(kvp3);
//			keyValuePairMap.getKeyValuePairs().add(kvp4);
//			keyValuePairMap.getKeyValuePairs().add(kvp5);
//			keyValuePairMap.getKeyValuePairs().add(kvp6);
//
//			BillpayRequestType billpayRequestType = new BillpayRequestType();
//			billpayRequestType.setSessionid(sessionId);
//			billpayRequestType.setWait(false);
//			billpayRequestType.setTarget(target);
//			HashMap<String, String> priceList = dataManager.getDenomMap(8);
//			billpayRequestType.setAmount(BigDecimal.valueOf(Long.valueOf(priceList.get(amount))));
//			billpayRequestType.setExtraTransData(keyValuePairMap);
//
//			StandardBizResponse bpr = utibaClient.billpay(billpayRequestType);
//			log.info("isatCreditInquiry RESULT; " + bpr.getResult());
//			if (bpr.getResult() == Constant.RC_SUCCESS) {
//				rs.setPrice(priceList.get(amount));
//				rs.setStatus(Constant.RC_SUCCESS);
//				rs.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
//				rs.setTrxid(String.valueOf(bpr.getTransid()));
//			} else {
//				rs.setStatus(Constant.RC_TRANSACTIN_FAILED);
//				rs.setMsg(dataManager.getErrorMsg(Constant.RC_TRANSACTIN_FAILED));
//			}
//			// if (bpr.getResult() == Constant.RC_SUCCESS) {
//			// try {
//			// Thread.sleep(1000);
//			// } catch (InterruptedException e) {
//			// // TODO Auto-generated catch block
//			// e.printStackTrace();
//			// }
//			// rs = getTransactionById(sessionId, bpr.getTransid(),
//			// Constant.SERVICE_BILLPAY, user);
//			// rs.setTrxid(String.valueOf(bpr.getTransid()));
//			// // rs.setStatus(bpr.getResult());
//			// // rs.setMsg(dataManager.getErrorMsg(bpr.getResult()));
//			// } else {
//			// rs.setStatus(bpr.getResult());
//			// rs.setMsg(dataManager.getErrorMsg(bpr.getResult()));
//			// }
//		} catch (Exception e) {
//			// TODO: handle exception
//			rs.setStatus(Constant.RC_TIMEOUT);
//			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
//			e.printStackTrace();
//		}
//		return rs;
//	}
	
	public Response airtimeBillpayCreditInquiry(String sessionId, String to, String amount, String target,Prefix prefix, ApiUser user) {
		log.info("isatCreditInquiry Request with sessionid: " + sessionId + " and recipient: " + to + " target:"
				+ target);
		Response rs = new Response();
		try {
			com.utiba.delirium.ws.misc.KeyValuePairMap keyValuePairMap = new com.utiba.delirium.ws.misc.KeyValuePairMap();
			KeyValuePair kvp1 = new KeyValuePair();
			kvp1.setKey("recipient");
			kvp1.setValue(to);

			// KeyValuePair kvp2 = new KeyValuePair();
			// kvp2.setKey("suppress_sms");
			// kvp2.setValue("1");

			// KeyValuePair kvp3 = new KeyValuePair();
			// kvp3.setKey("biz_command");
			// kvp3.setValue("outletbillpay");

			KeyValuePair kvp4 = new KeyValuePair();
			kvp4.setKey("ugaml_content_type");
			kvp4.setValue("topup");

			KeyValuePair kvp5 = new KeyValuePair();
			kvp5.setKey("voucher_type");
			kvp5.setValue("Regular");

			KeyValuePair kvp6 = new KeyValuePair();
			kvp6.setKey("denomination");
			kvp6.setValue(amount);

			KeyValuePair kvp7 = new KeyValuePair();
			kvp7.setKey("operator");
			kvp7.setValue(prefix.getName());

			KeyValuePair kvChannel = new KeyValuePair();
			kvChannel.setKey("channel_type");
			kvChannel.setValue(String.valueOf(user.getChannelType()));

			KeyValuePair kvApiUser = new KeyValuePair();
			kvApiUser.setKey("api_user");
			kvApiUser.setValue(String.valueOf(user.getUserId()));

			keyValuePairMap.getKeyValuePairs().add(kvChannel);
			keyValuePairMap.getKeyValuePairs().add(kvApiUser);
			keyValuePairMap.getKeyValuePairs().add(kvp1);
			// keyValuePairMap.getKeyValuePairs().add(kvp2);
			// keyValuePairMap.getKeyValuePairs().add(kvp3);
			keyValuePairMap.getKeyValuePairs().add(kvp4);
			keyValuePairMap.getKeyValuePairs().add(kvp5);
			keyValuePairMap.getKeyValuePairs().add(kvp6);
			keyValuePairMap.getKeyValuePairs().add(kvp7);

			BillpayRequestType billpayRequestType = new BillpayRequestType();
			billpayRequestType.setSessionid(sessionId);
			billpayRequestType.setWait(false);
			billpayRequestType.setTarget(target);
			HashMap<String, String> priceList = dataManager.getDenomMap(prefix.getId());
			billpayRequestType.setAmount(BigDecimal.valueOf(Long.valueOf(priceList.get(amount))));
			billpayRequestType.setExtraTransData(keyValuePairMap);

			StandardBizResponse bpr = utibaClient.billpay(billpayRequestType);
			log.info("airtimeBillpayCreditInquiry RESULT; " + bpr.getResult());
			if (bpr.getResult() == Constant.RC_SUCCESS) {
				rs.setPrice(priceList.get(amount));
				rs.setStatus(Constant.RC_SUCCESS);
				rs.setMsg(dataManager.getErrorMsg(Constant.RC_SUCCESS));
				rs.setTrxid(String.valueOf(bpr.getTransid()));
			} else {
				rs.setStatus(Constant.RC_TRANSACTIN_FAILED);
				rs.setMsg(dataManager.getErrorMsg(Constant.RC_TRANSACTIN_FAILED));
			}
			// if (bpr.getResult() == Constant.RC_SUCCESS) {
			// try {
			// Thread.sleep(1000);
			// } catch (InterruptedException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// rs = getTransactionById(sessionId, bpr.getTransid(),
			// Constant.SERVICE_BILLPAY, user);
			// rs.setTrxid(String.valueOf(bpr.getTransid()));
			// // rs.setStatus(bpr.getResult());
			// // rs.setMsg(dataManager.getErrorMsg(bpr.getResult()));
			// } else {
			// rs.setStatus(bpr.getResult());
			// rs.setMsg(dataManager.getErrorMsg(bpr.getResult()));
			// }
		} catch (Exception e) {
			// TODO: handle exception
			rs.setStatus(Constant.RC_TIMEOUT);
			rs.setMsg(dataManager.getErrorMsg(Constant.RC_TIMEOUT));
			e.printStackTrace();
		}
		return rs;
	}
}
