/**
 * Project    : Mobile Financial Service Middleware  
 * Created By : Megi Jaka Permana
 * Date       : Jun 26, 2014 
 * Time       : 3:13:18 PM 
 */
package com.indosat.kloc.mfsmw.soap;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.indosat.kloc.mfsmw.sender.MailSenderHandler;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.GeneratePIN;
import com.indosat.kloc.mfsmw.util.Utils;

import umarketscws.CreatesessionResponseType;
import umarketscws.LoginRequestType;
import umarketscws.StandardBizResponse;
import umarketscws.UMarketSC;


@Component
public class SessionHandler {
	
	private static Log log = LogFactory.getLog(SessionHandler.class);
	
	@Autowired
	private UMarketSC utibaClient;
	@Autowired
	private GeneratePIN generatePIN;
	
	@Value("${app.utiba.initiator}")
	private String userAdmin;
	@Value("${app.utiba.pin}")
	private String passAdmin;
	
	@Value("${app.utiba.nfc.wallet.initiator}")
	private String nfcAdmin;
	@Value("${app.utiba.nfc.wallet.pin}")
	private String nfcPass;
	@Autowired
	private MailSenderHandler mailSenderHandler;
	@Value("${app.alert.email}")
	private String emailList;
	
	public String getValidGlobalNFCSession(){
		log.info("GET VALID GLOBAL NFC SESSIONID");
		String sessionId = this.getSessionId();
		String pin = generatePIN.generate(sessionId, nfcAdmin, nfcPass);
		LoginRequestType requestType = new LoginRequestType();
		requestType.setSessionid(sessionId);
	    requestType.setInitiator(nfcAdmin);
	    requestType.setPin(pin);
	    StandardBizResponse resp =  utibaClient.login(requestType);
	   // System.out.println("validateSessionResponse: "+resp.getResult());
	    int result = resp.getResult();
	    if(result != Constant.RC_SUCCESS)
	    	log.info("FAILED TO VERIFY SESSION, WRONG GLOBAL NFC USERNAME OR PASSWORD RC:"+result);
		return sessionId;
	}
	
	public String getValidGlobalSessionId(){
		log.info("GET VALID GLOBAL SESSIONID");
		String sessionId = this.getSessionId();
		String pin = generatePIN.generate(sessionId, userAdmin, passAdmin);
		LoginRequestType requestType = new LoginRequestType();
		requestType.setSessionid(sessionId);
	    requestType.setInitiator(userAdmin);
	    requestType.setPin(pin);
	    StandardBizResponse resp =  utibaClient.login(requestType);
	   // System.out.println("validateSessionResponse: "+resp.getResult());
	    int result = resp.getResult();
	    if(result != Constant.RC_SUCCESS)
	    	log.info("FAILED TO VERIFY SESSION, WRONG GLOBAL USERNAME OR PASSWORD RC:"+result);
		return sessionId;
	}
	
	public String getSessionId(){
		//org.apache.cxf.endpoint.Client client = ClientProxy.getClient(sc);
	    //HTTPConduit conduit = (HTTPConduit) client.getConduit();
	    //TLSClientParameters parameters = new TLSClientParameters();
	    //parameters.setDisableCNCheck(true);
	    //conduit.setTlsClientParameters(parameters);
		try{
			CreatesessionResponseType createsessionResponseType =  utibaClient.createsession();
			return createsessionResponseType.getSessionid();
		}catch(Exception ex){
			try {
				log.debug("RETRY CREATE SESSIONID ***********************");
				CreatesessionResponseType createsessionResponseType =  utibaClient.createsession();
				return createsessionResponseType.getSessionid();
			} catch (Exception e) {
				// TODO: handle exception
				mailSenderHandler.sendEmailNotification(this.emailList, "Middleware ERROR ALERT", "CREATE SESSIONID | TIME["+Utils.getCurrentDate("dd/MM/yyyy hh:mm:ss")+"]"
						+ "<br />"
						+ "Causes:<br />"
						+ ExceptionUtils.getCause(ex)
						+"<br />"
						+ ExceptionUtils.getRootCause(ex));
			}
			return null;
		}
	}
	
	public int validateSession(String sessionId, String username, String password){
		    String pin = generatePIN.generate(sessionId, username, password);
		    LoginRequestType requestType = new LoginRequestType();
		    requestType.setSessionid(sessionId);
		    requestType.setInitiator(username);
		    requestType.setPin(pin);
		    StandardBizResponse resp =  utibaClient.login(requestType);
		    return resp.getResult();
	}
	
	public int validateSessionMerchant(String sessionId){
	    String pin = generatePIN.generate(sessionId, userAdmin, passAdmin);
	    LoginRequestType requestType = new LoginRequestType();
	    requestType.setSessionid(sessionId);
	    requestType.setInitiator(userAdmin);
	    requestType.setPin(pin);
	    StandardBizResponse resp =  utibaClient.login(requestType);
	    return resp.getResult();
}

}
