/**
 * Project    : Mobile Financial Service Middleware
 * Created By : Megi Jaka Permana
 * Date       : Mar 27, 2015 
 * Time       : 9:52:36 AM 
 */
package com.indosat.kloc.mfsmw.soap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.indosat.kloc.mfsmw.dao.BillerDao;
import com.indosat.kloc.mfsmw.model.MBiller;
import com.indosat.kloc.mfsmw.util.Constant;
import com.indosat.kloc.mfsmw.util.DESedeEncryption;
import com.indosat.kloc.mfsmw.util.GeneratePIN;
import com.indosat.kloc.mfsmw.util.SignatureBuilder;

import umarketscws.UMarketSC;

@Component
public class BillerSessionHandler {

	private static Log log = LogFactory.getLog(BillerSessionHandler.class);
	
	@Autowired
	private UMarketSC utibaClient;
	@Autowired
	private GeneratePIN generatePIN;
	@Autowired
	private SessionHandler sessionHandler;
	@Autowired
	private BillerDao billerDao;	
	
	@Value("${app.utiba.obill.wallet.initiator}")
	private String obInit;
	@Value("${app.utiba.obill.wallet.pin}")
	private String obPinEnc;
	private String encKey = "0n3b1ll_R3ff3rnC3_K3yMf5";
	private Map<String, String> billerSessionMap = new HashMap<String, String>();
	
	@PostConstruct
	public void init(){
		List<MBiller> list = billerDao.getAllMBiller();
		
		//AddForOnebill
		MBiller obBiller = new MBiller();
		DESedeEncryption encryptionKey = new DESedeEncryption(encKey);
		obBiller.setInitiator(obInit);
		obBiller.setPin(encryptionKey.decrypt(obPinEnc));
		list.add(obBiller);
		
		for(MBiller mbiller : list){
			this.validateSession(mbiller.getInitiator(), mbiller.getPin());
		}
	}
	
	public String getSession(String initiator){
		return billerSessionMap.get(initiator);
	}
	
	public int validateSession(String initiator, String pin){
		String sessionId = sessionHandler.getSessionId();
		int result = sessionHandler.validateSession(sessionId, initiator, pin);
		if(result == Constant.RC_SUCCESS){
			this.billerSessionMap.put(initiator, sessionId);
			log.info("Success validate session for biller "+initiator);
		}else{
			log.info("Fail to validate session for biller "+initiator);
		}
		return result;
	}
	
}
