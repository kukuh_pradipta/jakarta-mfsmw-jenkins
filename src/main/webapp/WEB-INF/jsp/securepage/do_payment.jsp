
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Review Payment</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Loading Bootstrap -->
    <link href="${pageContext.request.contextPath}/ott/css/vendor/bootstrap.min.css" rel="stylesheet">

    <!-- Loading Flat UI Pro -->
    <link href="${pageContext.request.contextPath}/ott/css/flat-ui-pro.css" rel="stylesheet">


    <link rel="shortcut icon" href="${pageContext.request.contextPath}/ott/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background-color:#F5D2A4">
	<div class="container">
		<div style="min-height:50px;"></div>
	</div>
    <div class="container">
        <div class="row">
        	<div class="col-lg-12">
        		<img alt="" src="${pageContext.request.contextPath}/ott/img/logo_ott.png" width="250px">
        		<p>
        	</div>
        </div>
        <div class="row">
        	<div class="col-lg-5">
        		<div class="boxed">
				<div class="table-responsive">
					<table class="table" style="background-color:#FFFFFF"> 
						<thead>
							<tr>
								<td>My Account</td>
							</tr>
						</thead>
						<tbody>
							<tr>  
								<td>
									Email/Mobile Number:
								</td>
								<td>${username}</td>
							</tr>
							<tr>
								<td>Account ID</td>
								<td>${response.accountId}</td>
							</tr>
							<tr>
								<td>Balance</td>
								<td>Rp ${response.balance}</td>
							</tr>
						</tbody>
					</table>
				</div>
				</div>
        	</div>
        	<div class="col-lg-7">
        	<form action="${pageContext.request.contextPath}/securepage/do_payment" method="post">
				<div class="boxed" style="background-color:#f9f9f9;padding: 30px 50px;">
					<strong><span class="fui-arrow-right"></span>&nbsp;&nbsp;Cipika Store Order Summary</strong><br />
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Produk</th>
										<th>Jumlah</th>
										<th>Harga</th>
										<th>Discount(%)</th>
										<th>Sub Total</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Ovomaltine Crunchy Cream</td>
										<td>1</td>
										<td>53,000</td>
										<td>0</td>
										<td>53,000</td>
									</tr>
								</tbody>
								<thead>
									<tr>
										<th colspan=3></th>
										<th>Grand Total</th>
										<th colspan=2>53,000</th>
									</tr>
								</thead>
							</table>
						</div>

						<p>
						<strong><span class="fui-arrow-right"></span>&nbsp;&nbsp;Payment method</strong><br />
						<span class="small">select your payment method</span>
					</p> 
					<div class="btn-group">
						<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
	                		PayWizz<span class="caret"></span>
	              		</button>
						<ul class="dropdown-menu" role="menu">
			                <li><a href="#">Credit Card 55xx xxxx xxxx xx41</a></li>
			                <li><a href="#" class="selected">PayWizz</a></li>
		              	</ul>
	              	</div>
		            <button class="btn btn-warning btn-embossed" type="submit">Pay Now</button>
		            <input type="text" name="signature" value="${signature}" style="display: none">
				</div>
			</form>	
        	</div>
        </div>
    </div>

    <!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
    <script src="${pageContext.request.contextPath}/ott/js/vendor/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/ott/js/vendor/video.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${pageContext.request.contextPath}/ott/js/flat-ui-pro.js"></script>

  </body>
</html>
