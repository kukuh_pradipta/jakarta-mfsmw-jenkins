
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Loading Bootstrap -->
    <link href="${pageContext.request.contextPath}/ott/css/vendor/bootstrap.min.css" rel="stylesheet">

    <!-- Loading Flat UI Pro -->
    <link href="${pageContext.request.contextPath}/ott/css/flat-ui-pro.css" rel="stylesheet">


    <link rel="shortcut icon" href="${pageContext.request.contextPath}/ott/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background-color:#F5D2A4">
	<div class="container">
		<div style="min-height:50px;"></div>
	</div>
    <div class="container">
        <div class="row">
        	<div class="col-lg-3"></div>
        	<div class="col-lg-6">
        		<h4>Register</h4>
        	</div>
        	<div class="col-lg-3"></div>
        </div>
        <div class="row">
        	<div class="col-lg-3"></div>
        	<div class="col-lg-6">
        	<form action="${pageContext.request.contextPath}/securepage/register" method="post">
        		<div class="boxed" style="background-color:#f9f9f9;padding: 30px 50px;">
					<p>
						<strong><span class="fui-arrow-right"></span>&nbsp;&nbsp;Create your PayWizz account, it's free to sign up</strong><br />
						<span class="small">Enter your information</span>
					</p>
					<div class="form-group">
						<input class="form-control input-group-lg flat" name="name" type="text" placeholder="Your name">
						<span class="form-control-feedback"></span>
					</div>
					<div class="form-group">
						<input class="form-control input-group-lg flat" name="username" type="email" placeholder="Email">
						<span class="form-control-feedback fui-user"></span>
					</div>
					<div class="form-group">
						<input class="form-control input-group-lg flat" name="password" type="password" placeholder="Password" maxlength="6">
						<span class="form-control-feedback fui-lock"></span>
					</div>
					<div class="form-group">
						<input class="form-control input-group-lg flat" name="" type="password" placeholder="Confirm Password" maxlength="6">
						<span class="form-control-feedback fui-lock"></span>
					</div>
					<div class="form-group">
						<input class="form-control input-group-lg flat" name="msisdn" type="number" required="required" placeholder="Phone Number (08XXXXXXXXX)">
						<span class="form-control-feedback"></span>
					</div>
				<p><span class="small">By clicking the button, I agree to the PayWizz <a href='#' style="color:#084482;text-decoration: underline;">User Agreement</a> and <a href='#' style="color:#084482;text-decoration: underline;">Privacy Policy</a></span></p>
	            	<button class="btn btn-warning btn-embossed">Agree and create account</button>
				</div>
        	</form>
				
        	</div>
        	<div class="col-lg-3"></div>
        </div>
    </div>

    <!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
    <script src="${pageContext.request.contextPath}/ott/js/vendor/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/ott/js/vendor/video.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${pageContext.request.contextPath}/ott/js/flat-ui-pro.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function() {
		
		if('${message}' != ''){
			alert('${message}');
		}
		
	});
	</script>
	
  </body>
</html>
