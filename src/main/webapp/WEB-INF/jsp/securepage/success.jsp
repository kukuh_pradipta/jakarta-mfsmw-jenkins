
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Success</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Loading Bootstrap -->
    <link href="${pageContext.request.contextPath}/ott/css/vendor/bootstrap.min.css" rel="stylesheet">

    <!-- Loading Flat UI Pro -->
    <link href="${pageContext.request.contextPath}/ott/css/flat-ui-pro.css" rel="stylesheet">


    <link rel="shortcut icon" href="${pageContext.request.contextPath}/ott/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<div class="container">
		<div style="min-height:50px;"></div>
	</div>
    <div class="container">
        <div class="row">
        	<div class="col-lg-3"></div>
        	<div class="col-lg-6">
        		<h4>Success</h4>
        	</div>
        	<div class="col-lg-3"></div>
        </div>
        <div class="row">
        	<div class="col-lg-3"></div>
        	<div class="col-lg-6">
				<div class="boxed" style="background-color:#f9f9f9;padding: 30px 50px;">
					<p>
						<strong><span class="fui-arrow-right"></span>&nbsp;&nbsp;Payment completed</strong><br />
						<span class="small">please wait, we will redirect you back to merchant page</span>
					</p>
		            <a class="btn btn-warning btn-embossed" href="${pageContext.request.contextPath}/checkout">Redirect</a>
				</div>
        	</div>
        	<div class="col-lg-3"></div>
        </div>
    </div>

    <!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
    <script src="${pageContext.request.contextPath}/ott/js/vendor/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/ott/js/vendor/video.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${pageContext.request.contextPath}/ott/js/flat-ui-pro.js"></script>

  </body>
</html>
