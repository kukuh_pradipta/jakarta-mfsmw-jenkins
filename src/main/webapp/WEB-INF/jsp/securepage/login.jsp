
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Payment</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Loading Bootstrap -->
    <link href="${pageContext.request.contextPath}/ott/css/vendor/bootstrap.min.css" rel="stylesheet">

    <!-- Loading Flat UI Pro -->
    <link href="${pageContext.request.contextPath}/ott/css/flat-ui-pro.css" rel="stylesheet">


    <link rel="shortcut icon" href="${pageContext.request.contextPath}/ott/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background-color:#F5D2A4">
	<div class="container">
		<div style="min-height:50px;"></div>
	</div>
    <div class="container">
        <div class="row">
        	<div class="col-lg-12">
        		<h4>Cipika Store</h4>
        	</div>
        </div>
        <div class="row">
        	<div class="col-lg-4">
        		<div class="boxed">
        		<p align="center">Your Order Summary</p>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr style="background-color: #f1c40f;">
								<th style="color:#fff;">Description</th>
								<th style="color:#fff;">Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									Ovomaltine Crunchy Cream<br />
									<span class="small">Discount : 0%</span>
								</td>
								<td>53,000</td>
							</tr>
						</tbody>
						<thead>
							<tr>
								<th>Grand Total</th>
								<th>53,000</th>
							</tr> 
						</thead>
					</table>
				</div>
				</div>
        	</div>
        	<div class="col-lg-8">
        		<form action="${pageContext.request.contextPath}/securepage/login" method="post">
					<div class="boxed" style="background-color:#f9f9f9;padding: 30px 50px;">
						<p>
							<strong><span class="fui-arrow-right"></span>&nbsp;&nbsp;Pay with my PayWizz account</strong><br />
							<span class="small">Login to your account to complete the purchases</span>
						</p>
						
							<div class="form-group">
							<input class="form-control input-group-lg flat" id="appendedInputButton-04" type="text" placeholder="Email Address/Mobile Number" name="username">
							<span class="form-control-feedback fui-user"></span>
							</div>
							<div class="form-group">
								<input class="form-control input-group-lg flat" id="appendedInputButton-04" type="password" placeholder="Password" name="password">
								<span class="form-control-feedback fui-lock"></span>
							</div>					
						
					<button class="btn btn-warning btn-embossed" name="login" type="submit">Log in</button>
					</div>
				</form>
				<div class="boxed" style="background-color:#f9f9f9;padding: 30px 50px;">
					<p>
						<strong><span class="fui-arrow-right"></span>&nbsp;&nbsp;Don't have PayWizz account?</strong><br />
					</p>
					<a href="${pageContext.request.contextPath}/securepage/register" class="btn btn-info btn-embossed">Register</a>
				</div>
        	</div>
        </div>
    </div>

    <!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
    <script src="${pageContext.request.contextPath}/ott/js/vendor/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/ott/js/vendor/video.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${pageContext.request.contextPath}/ott/js/flat-ui-pro.js"></script>

  </body>
</html>
