<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Check Out</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Loading Bootstrap -->
    <link href="${pageContext.request.contextPath}/ott/css/vendor/bootstrap.min.css" rel="stylesheet">

    <!-- Loading Flat UI Pro -->
    <link href="${pageContext.request.contextPath}/ott/css/flat-ui-pro.css" rel="stylesheet">


    <link rel="shortcut icon" href="${pageContext.request.contextPath}/ott/img/favicon.ico">
    
     <!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
    <script src="${pageContext.request.contextPath}/ott/js/vendor/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/ott/js/vendor/video.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${pageContext.request.contextPath}/ott/s/flat-ui-pro.js"></script> 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<div class="container">
		<div style="min-height:50px;"></div>
	</div>
    <div class="container">
        <div class="row">
        	<div class="col-lg-2"></div>
        	<div class="col-lg-8">
        		<h4>Check out</h4>
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Produk</th>
								<th>Jumlah</th>
								<th>Harga</th>
								<th>Discount(%)</th>
								<th>Sub Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Ovomaltine Crunchy Cream</td>
								<td>1</td>
								<td>53,000</td>
								<td>0</td>
								<td>53,000</td>
							</tr>
						</tbody>
						<thead>
							<tr>
								<th colspan=3></th>
								<th>Grand Total</th>  
								<th colspan=2>53,000</th>
							</tr>
						</thead>
					</table>
				</div>
				<p><span class="small"><strong>Metode pembayaran</strong></span></p>
        		<a href="securepage/login" class="btn btn-warning btn-embossed">PayWizz</a>
        	</div>
        	<div class="col-lg-2"></div>
        </div>
    </div>


  </body>
</html>
